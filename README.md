fixpolyglot
=====

An OTP application

Build
-----

    $ rebar3 compile
ShortIntro
----------
* Vaasi in Hindi means inhabitant.. 
As the inhabitants know the nitty gritty details of a language.
So does this application knows how to speak fix language of finance ----> This is going to play a pivotal role in at least two of our projects. 


Conventions
-----------

1. Fix protocol is made of definitions. These definitions are 
   a. Fix Body messages/components/groups
   b. Fix header/trailer definitions
   c. Other's --> Basically user defined things (could be anything messages, new fields, new groups).

2. The two most important functions that any fix parser should provide are
   a. encode--> (given a fix structure provide you with a valid fix string)
   b. decode--> (given a valid fix string convert it to the parse's internal structure.)

3. This fix parser is an ambitious project. But for basics this provides these two functionalies.


Decode
------
    Reads input fix binary stream and decode it to internal representation
    signature: decode(FixBinstream, DefinitionMap, {to, map/xml/json})

Encode
------
    Takes a form encode(Message, <Definition_Map>)
    This Definition map expects three keys and this is the only standard that we define here:
         1. fix_version
         2. fix_transport
         3. user_defined_fix

         This is the search order in which tags are parsed as well.
         ** encode function has to have a fix_version key in the definition map **
         The priority order for conflicting definitions(tags/values/messages/fields/groups/components) is :
         1. user_defined_fix --> Top priority
         2. fix_transport --> Next
         3. fix_version ---> lowest

The combination of parser is saved in src/parsers directory with a naming convention:
    <udef>_<transport>_<version>.erl
and once created is used from here next time

For more information please check the "test.erl" file

Issues face
----------
Rebar3 release did not build anything
Make sure that the app name is correct

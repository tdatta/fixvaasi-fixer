#!/usr/bin/python
'''
The program that takes xml that quickfix has and convert them to fix maps

'''

import sys
from string import Template
from bs4 import BeautifulSoup
import pdb

ERL_FIX_Map = Template('''-module($ModuleName).
-export([messages/0,
         fields/0,
         components/0,
         groups/0,
         header/0,
         trailer/0]).

messages() ->
 #{$MsgList}.


fields() ->
#{$FldList}.



components() ->
 #{$CmpList}.



groups() ->
 $GrpList.



header()->
#{$HeaderList}.



trailer()->
#{$TrailerList}.

''')

GrpTmp = Template('''
                              "$GrpName" => #{
                              "Fields" => $F
                              ,"Groups" => $G
                              ,"Components" => $Cmp
}
''')


MsgTmp = Template('''"$MsgName" => #{
                              "Category" => "$C"
                              ,"Type" => "$T"
                              ,"Fields" => $F
                              ,"Groups" => $G
                              ,"Components" => $Cmp
}
,"$T" => #{"Category"=>"$C"
           ,"Name" => "$MsgName"}
''')


MsgFld = Template('''"$N" =>#{"Required" => "$R", "Sequence" => undefined}''')

FldTmp = Template('''
"$Name" => #{"TagNum" => "$N" ,"Type" => "$T" ,"ValidValues" =>$V}
, "$N" => #{"Name"=>"$Name" ,"Type"=>"$T" ,"ValidValues"=>$V, "TagNum" => "$N"}

''')

CmpTmp = Template('''"$N" => #{
                              "Fields" => $F
                              ,"Groups" => $G
                              ,"Components" => $C}''')

ListTmp = Template("#{$List}")
PLTmp = Template('{"$Value", "$Desc"}')

HeaderTmp = Template('''
"Fields" => $F, \n"Groups" => $G, \n "Components" => $Cmp\n
''')


def get_all_fields(row):
    """ Get all the fields from a row

    :param row: soup (row from quickfix)
    :returns: Erlang dictionary
    :rtype: erl map

    """
    fl = row.findAll('field')
    AllFields =''
    for f in fl:
        AllFields += MsgFld.substitute(N=f['name'],
                                       R=f['required']) + "\n,"

    return ListTmp.substitute(List=AllFields[:-2])
    

def get_all_component(row):
    fl = row.findAll('component', recursive=False)
    AllFields =''
    for f in fl:
        AllFields += MsgFld.substitute(N=f['name'],
                                       R=f['required']) + "\n,"

    return ListTmp.substitute(List=AllFields[:-1])


def get_messages(soup):
    messages = soup.find("messages")
    AllMsg = ''
    for row in messages.findAll("message"):
        AllMsg += get_message(row)
    return AllMsg[:-1]


def get_message(row):
    msgName = row['name']
    category = row['msgcat']
    types  = row['msgtype']
    fields = get_all_fields(row)
    components = get_all_component(row)
    groups = get_all_groups(row)
    return MsgTmp.substitute(MsgName=msgName,
                             C=category,
                             T=types,
                             F=fields,
                             G=groups,
                             Cmp=components) + "\n,"

def get_header_trailer(row):
    # pdb.set_trace ()
    fields = get_all_fields(row)
    components = get_all_component(row)
    groups = get_all_groups(row)
    return HeaderTmp.safe_substitute (
        F=fields,
        G=groups,
        Cmp=components
        )

def get_fields(soup):
    fields = soup.find('fields')
    AllFields = ''
    for fld in fields.findAll("field", recursive=False):
        number = fld['number']
        name = fld['name']
        types = fld['type']
        valid_values = fld.findAll("value", recursive=False)
        value_des = [PLTmp.substitute(Desc = str(x['description']),
                                      Value=str(x['enum']))
                     for x in valid_values]
        value_des = "[" + ",".join(value_des) + "]"
        AllFields += FldTmp.substitute(N=number,
                                       Name=name,
                                       T=types,
                                       V=value_des) + "\n,"
    return AllFields[:-1]


def get_components(soup):
    components = soup.find('components')
    # import pdb
    # pdb.set_trace()
    AllComponents = ''
    for c in components.findAll("component", recursive=False):
        if c.text <> '':
            [fields, groups, components]= get_component(c)
            name = c['name']
            AllComponents += CmpTmp.substitute(N=name,
                                               F=fields,
                                               G=groups,
                                               C=components) + "\n,"
        else:
            continue
    return AllComponents[:-1]


def get_component(component):

    if len(component.contents)==0:
        return [u'#{}', u'#{}', u'#{}']
    else:
        AllFields= get_all_fields(component)
        AllGroups = get_all_groups(component)
        AllComponents= get_all_component(component)
        return [AllFields, AllGroups, AllComponents]

def get_all_groups(row):
    grplst = row.findAll ("group", recursive=False)
    group_item = ""
    for g in grplst:
        (gF, gG, gC) = get_group(g)
        group_item += GrpTmp.safe_substitute (GrpName = g['name'],
                                                   F=gF,
                                                   G=gG,
                                                   Cmp=gC) + "\n,"
    return ListTmp.safe_substitute (List=group_item[:-1])


def get_group(g):
    if len (g.contents)==0:
        return [u'#{}', u'#{}', u'#{}']
    else:
        AllFields= get_all_fields(g)
        AllGroups = get_all_groups(g)
        AllComponents= get_all_component(g)
        return [AllFields, AllGroups, AllComponents]
        
        

if __name__ == '__main__':
    src = sys.argv[1] 
    dest = sys.argv[2] 
    module_name = dest.split("/")[-1].split(".")[0].lower()
    # BeautifulSoup.NESTABLE_TAGS['component']= ['component']
    # BeautifulSoup.RESET_NESTING_TAGS.has_key('component')
    soup = BeautifulSoup(open(src, 'r').read(), 'html.parser')
    # soup = BeautifulSoup(open(src, 'r').read())
    table = soup.find("table", { "class" : "ListTable" })

    MsgList = ''
    FldList = ''
    CmpList = ''
    MsgList=get_messages(soup)[:-1]
    MsgList  = MsgList.encode("utf8")

    FldList = get_fields(soup)
    FldList = FldList.encode("utf8")

    CmpList = get_components(soup)
    CmpList = CmpList.encode("utf8")

    GrpList = get_all_groups(soup)
    GrpList = GrpList.encode("utf8")

    HeaderList = get_header_trailer(soup.find("header"))
    HeaderList = HeaderList.encode("utf8")

    TrailerList = get_header_trailer(soup.find("trailer"))
    TrailerList = TrailerList.encode("utf8")

    with open(dest.lower(), 'w') as destfile:
        destfile.write(ERL_FIX_Map.substitute(ModuleName=module_name,
                                              MsgList=MsgList,
                                              CmpList=CmpList,
                                              FldList=FldList,
                                              GrpList=GrpList,
                                              HeaderList=HeaderList,
                                              TrailerList=TrailerList))
    print "done"

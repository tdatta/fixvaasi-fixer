-module(fix41).
-export([messages/0,
         fields/0,
         components/0,
         groups/0,
         header/0,
         trailer/0]).

messages() ->
 #{"Heartbeat" => #{
                              "Category" => "admin"
                              ,"Type" => "0"
                              ,"Fields" => #{"TestReqID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"0" => #{"Category"=>"admin"
           ,"Name" => "Heartbeat"}

,"TestRequest" => #{
                              "Category" => "admin"
                              ,"Type" => "1"
                              ,"Fields" => #{"TestReqID" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"1" => #{"Category"=>"admin"
           ,"Name" => "TestRequest"}

,"ResendRequest" => #{
                              "Category" => "admin"
                              ,"Type" => "2"
                              ,"Fields" => #{"BeginSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}
,"EndSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"2" => #{"Category"=>"admin"
           ,"Name" => "ResendRequest"}

,"Reject" => #{
                              "Category" => "admin"
                              ,"Type" => "3"
                              ,"Fields" => #{"RefSeqNum" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"3" => #{"Category"=>"admin"
           ,"Name" => "Reject"}

,"SequenceReset" => #{
                              "Category" => "admin"
                              ,"Type" => "4"
                              ,"Fields" => #{"GapFillFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"NewSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"4" => #{"Category"=>"admin"
           ,"Name" => "SequenceReset"}

,"Logout" => #{
                              "Category" => "admin"
                              ,"Type" => "5"
                              ,"Fields" => #{"Text" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"5" => #{"Category"=>"admin"
           ,"Name" => "Logout"}

,"IOI" => #{
                              "Category" => "app"
                              ,"Type" => "6"
                              ,"Fields" => #{"IOIid" =>#{"Required" => "Y", "Sequence" => undefined}
,"IOITransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"IOIRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"IOIShares" =>#{"Required" => "Y", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIQltyInd" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIOthSvc" =>#{"Required" => "N", "Sequence" => undefined}
,"IOINaturalFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIQualifier" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"URLLink" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoIOIQualifiers" => #{
                              "Fields" => #{"IOIQualifier" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"6" => #{"Category"=>"app"
           ,"Name" => "IOI"}

,"Advertisement" => #{
                              "Category" => "app"
                              ,"Type" => "7"
                              ,"Fields" => #{"AdvId" =>#{"Required" => "Y", "Sequence" => undefined}
,"AdvTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"AdvRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"AdvSide" =>#{"Required" => "Y", "Sequence" => undefined}
,"Shares" =>#{"Required" => "Y", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"URLLink" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"7" => #{"Category"=>"app"
           ,"Name" => "Advertisement"}

,"ExecutionReport" => #{
                              "Category" => "app"
                              ,"Type" => "8"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExecTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExecRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecType" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"PegDifference" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"Rule80A" =>#{"Required" => "N", "Sequence" => undefined}
,"LastShares" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"LastForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"LastCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"LeavesQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"CumQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ReportToExch" =>#{"Required" => "N", "Sequence" => undefined}
,"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"8" => #{"Category"=>"app"
           ,"Name" => "ExecutionReport"}

,"OrderCancelReject" => #{
                              "Category" => "app"
                              ,"Type" => "9"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"CxlRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"9" => #{"Category"=>"app"
           ,"Name" => "OrderCancelReject"}

,"Logon" => #{
                              "Category" => "admin"
                              ,"Type" => "A"
                              ,"Fields" => #{"EncryptMethod" =>#{"Required" => "Y", "Sequence" => undefined}
,"HeartBtInt" =>#{"Required" => "Y", "Sequence" => undefined}
,"RawDataLength" =>#{"Required" => "N", "Sequence" => undefined}
,"RawData" =>#{"Required" => "N", "Sequence" => undefined}
,"ResetSeqNumFlag" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"A" => #{"Category"=>"admin"
           ,"Name" => "Logon"}

,"News" => #{
                              "Category" => "app"
                              ,"Type" => "B"
                              ,"Fields" => #{"OrigTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Urgency" =>#{"Required" => "N", "Sequence" => undefined}
,"Headline" =>#{"Required" => "Y", "Sequence" => undefined}
,"RelatdSym" =>#{"Required" => "N", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "Y", "Sequence" => undefined}
,"URLLink" =>#{"Required" => "N", "Sequence" => undefined}
,"RawDataLength" =>#{"Required" => "N", "Sequence" => undefined}
,"RawData" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"RelatdSym" =>#{"Required" => "N", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "LinesOfText" => #{
                              "Fields" => #{"Text" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"B" => #{"Category"=>"app"
           ,"Name" => "News"}

,"Email" => #{
                              "Category" => "app"
                              ,"Type" => "C"
                              ,"Fields" => #{"EmailThreadID" =>#{"Required" => "Y", "Sequence" => undefined}
,"EmailType" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Subject" =>#{"Required" => "Y", "Sequence" => undefined}
,"RelatdSym" =>#{"Required" => "N", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "Y", "Sequence" => undefined}
,"RawDataLength" =>#{"Required" => "N", "Sequence" => undefined}
,"RawData" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"RelatdSym" =>#{"Required" => "N", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "LinesOfText" => #{
                              "Fields" => #{"Text" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"C" => #{"Category"=>"app"
           ,"Name" => "Email"}

,"NewOrderSingle" => #{
                              "Category" => "app"
                              ,"Type" => "D"
                              ,"Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIid" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"Rule80A" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenClose" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"CustomerOrFirm" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"PegDifference" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"D" => #{"Category"=>"app"
           ,"Name" => "NewOrderSingle"}

,"NewOrderList" => #{
                              "Category" => "app"
                              ,"Type" => "E"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"WaveNo" =>#{"Required" => "N", "Sequence" => undefined}
,"ListSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListNoOrds" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"PegDifference" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"Rule80A" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenClose" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"CustomerOrFirm" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"E" => #{"Category"=>"app"
           ,"Name" => "NewOrderList"}

,"OrderCancelRequest" => #{
                              "Category" => "app"
                              ,"Type" => "F"
                              ,"Fields" => #{"OrigClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"F" => #{"Category"=>"app"
           ,"Name" => "OrderCancelRequest"}

,"OrderCancelReplaceRequest" => #{
                              "Category" => "app"
                              ,"Type" => "G"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"PegDifference" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"Rule80A" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenClose" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"CustomerOrFirm" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"G" => #{"Category"=>"app"
           ,"Name" => "OrderCancelReplaceRequest"}

,"OrderStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "H"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"H" => #{"Category"=>"app"
           ,"Name" => "OrderStatusRequest"}

,"Allocation" => #{
                              "Category" => "app"
                              ,"Type" => "J"
                              ,"Fields" => #{"AllocID" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"RefAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocLinkType" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"WaveNo" =>#{"Required" => "N", "Sequence" => undefined}
,"LastShares" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Shares" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPrxPrecision" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenClose" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"NumDaysInterest" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocShares" =>#{"Required" => "Y", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"BrokerOfCredit" =>#{"Required" => "N", "Sequence" => undefined}
,"NotifyBrokerOfCredit" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocHandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocNetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstMode" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoOrders" => #{
                              "Fields" => #{"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"WaveNo" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoExecs" => #{
                              "Fields" => #{"LastShares" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastCapacity" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocShares" =>#{"Required" => "Y", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"BrokerOfCredit" =>#{"Required" => "N", "Sequence" => undefined}
,"NotifyBrokerOfCredit" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocHandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocNetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstMode" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMiscFees" => #{
                              "Fields" => #{"MiscFeeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"J" => #{"Category"=>"app"
           ,"Name" => "Allocation"}

,"ListCancelRequest" => #{
                              "Category" => "app"
                              ,"Type" => "K"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"WaveNo" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"K" => #{"Category"=>"app"
           ,"Name" => "ListCancelRequest"}

,"ListExecute" => #{
                              "Category" => "app"
                              ,"Type" => "L"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"WaveNo" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"L" => #{"Category"=>"app"
           ,"Name" => "ListExecute"}

,"ListStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "M"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"WaveNo" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"M" => #{"Category"=>"app"
           ,"Name" => "ListStatusRequest"}

,"ListStatus" => #{
                              "Category" => "app"
                              ,"Type" => "N"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"WaveNo" =>#{"Required" => "N", "Sequence" => undefined}
,"NoRpts" =>#{"Required" => "Y", "Sequence" => undefined}
,"RptSeq" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"CumQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"LeavesQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"CxlQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoOrders" => #{
                              "Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"CumQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"LeavesQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"CxlQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"N" => #{"Category"=>"app"
           ,"Name" => "ListStatus"}

,"AllocationInstructionAck" => #{
                              "Category" => "app"
                              ,"Type" => "P"
                              ,"Fields" => #{"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocRejCode" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"P" => #{"Category"=>"app"
           ,"Name" => "AllocationInstructionAck"}

,"DontKnowTrade" => #{
                              "Category" => "app"
                              ,"Type" => "Q"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"DKReason" =>#{"Required" => "Y", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LastShares" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"Q" => #{"Category"=>"app"
           ,"Name" => "DontKnowTrade"}

,"QuoteRequest" => #{
                              "Category" => "app"
                              ,"Type" => "R"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"R" => #{"Category"=>"app"
           ,"Name" => "QuoteRequest"}

,"Quote" => #{
                              "Category" => "app"
                              ,"Type" => "S"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "Y", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"S" => #{"Category"=>"app"
           ,"Name" => "Quote"}

,"SettlementInstructions" => #{
                              "Category" => "app"
                              ,"Type" => "T"
                              ,"Fields" => #{"SettlInstID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlInstTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlInstMode" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlInstSource" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlLocation" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"StandInstDbType" =>#{"Required" => "N", "Sequence" => undefined}
,"StandInstDbName" =>#{"Required" => "N", "Sequence" => undefined}
,"StandInstDbID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDeliveryType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDepositoryCode" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlBrkrCode" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstCode" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySettlAgentName" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySettlAgentCode" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySettlAgentAcctNum" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySettlAgentAcctName" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySettlAgentContactName" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySettlAgentContactPhone" =>#{"Required" => "N", "Sequence" => undefined}
,"CashSettlAgentName" =>#{"Required" => "N", "Sequence" => undefined}
,"CashSettlAgentCode" =>#{"Required" => "N", "Sequence" => undefined}
,"CashSettlAgentAcctNum" =>#{"Required" => "N", "Sequence" => undefined}
,"CashSettlAgentAcctName" =>#{"Required" => "N", "Sequence" => undefined}
,"CashSettlAgentContactName" =>#{"Required" => "N", "Sequence" => undefined}
,"CashSettlAgentContactPhone" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"T" => #{"Category"=>"app"
           ,"Name" => "SettlementInstructions"}
}.


fields() ->
#{
"Account" => #{"TagNum" => "1" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "1" => #{"Name"=>"Account" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "1"}


,
"AdvId" => #{"TagNum" => "2" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "2" => #{"Name"=>"AdvId" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "2"}


,
"AdvRefID" => #{"TagNum" => "3" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "3" => #{"Name"=>"AdvRefID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "3"}


,
"AdvSide" => #{"TagNum" => "4" ,"Type" => "CHAR" ,"ValidValues" =>[{"B", "BUY"},{"S", "SELL"},{"T", "TRADE"},{"X", "CROSS"}]}
, "4" => #{"Name"=>"AdvSide" ,"Type"=>"CHAR" ,"ValidValues"=>[{"B", "BUY"},{"S", "SELL"},{"T", "TRADE"},{"X", "CROSS"}], "TagNum" => "4"}


,
"AdvTransType" => #{"TagNum" => "5" ,"Type" => "CHAR" ,"ValidValues" =>[{"C", "CANCEL"},{"N", "NEW"},{"R", "REPLACE"}]}
, "5" => #{"Name"=>"AdvTransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"C", "CANCEL"},{"N", "NEW"},{"R", "REPLACE"}], "TagNum" => "5"}


,
"AvgPx" => #{"TagNum" => "6" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "6" => #{"Name"=>"AvgPx" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "6"}


,
"BeginSeqNo" => #{"TagNum" => "7" ,"Type" => "INT" ,"ValidValues" =>[]}
, "7" => #{"Name"=>"BeginSeqNo" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "7"}


,
"BeginString" => #{"TagNum" => "8" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "8" => #{"Name"=>"BeginString" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "8"}


,
"BodyLength" => #{"TagNum" => "9" ,"Type" => "INT" ,"ValidValues" =>[]}
, "9" => #{"Name"=>"BodyLength" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "9"}


,
"CheckSum" => #{"TagNum" => "10" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "10" => #{"Name"=>"CheckSum" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "10"}


,
"ClOrdID" => #{"TagNum" => "11" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "11" => #{"Name"=>"ClOrdID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "11"}


,
"Commission" => #{"TagNum" => "12" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "12" => #{"Name"=>"Commission" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "12"}


,
"CommType" => #{"TagNum" => "13" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "PER_SHARE"},{"2", "PERCENTAGE"},{"3", "ABSOLUTE"}]}
, "13" => #{"Name"=>"CommType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "PER_SHARE"},{"2", "PERCENTAGE"},{"3", "ABSOLUTE"}], "TagNum" => "13"}


,
"CumQty" => #{"TagNum" => "14" ,"Type" => "INT" ,"ValidValues" =>[]}
, "14" => #{"Name"=>"CumQty" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "14"}


,
"Currency" => #{"TagNum" => "15" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "15" => #{"Name"=>"Currency" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "15"}


,
"EndSeqNo" => #{"TagNum" => "16" ,"Type" => "INT" ,"ValidValues" =>[]}
, "16" => #{"Name"=>"EndSeqNo" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "16"}


,
"ExecID" => #{"TagNum" => "17" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "17" => #{"Name"=>"ExecID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "17"}


,
"ExecInst" => #{"TagNum" => "18" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "STAY_ON_OFFERSIDE"},{"1", "NOT_HELD"},{"2", "WORK"},{"3", "GO_ALONG"},{"4", "OVER_THE_DAY"},{"5", "HELD"},{"6", "PARTICIPATE_DONT_INITIATE"},{"7", "STRICT_SCALE"},{"8", "TRY_TO_SCALE"},{"9", "STAY_ON_BIDSIDE"},{"A", "NO_CROSS"},{"B", "OK_TO_CROSS"},{"C", "CALL_FIRST"},{"D", "PERCENT_OF_VOLUME"},{"E", "DO_NOT_INCREASE"},{"F", "DO_NOT_REDUCE"},{"G", "ALL_OR_NONE"},{"I", "INSTITUTIONS_ONLY"},{"L", "LAST_PEG"},{"M", "MID_PRICE_PEG"},{"N", "NON_NEGOTIABLE"},{"O", "OPENING_PEG"},{"P", "MARKET_PEG"},{"R", "PRIMARY_PEG"},{"S", "SUSPEND"},{"U", "CUSTOMER_DISPLAY_INSTRUCTION"},{"V", "NETTING"}]}
, "18" => #{"Name"=>"ExecInst" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "STAY_ON_OFFERSIDE"},{"1", "NOT_HELD"},{"2", "WORK"},{"3", "GO_ALONG"},{"4", "OVER_THE_DAY"},{"5", "HELD"},{"6", "PARTICIPATE_DONT_INITIATE"},{"7", "STRICT_SCALE"},{"8", "TRY_TO_SCALE"},{"9", "STAY_ON_BIDSIDE"},{"A", "NO_CROSS"},{"B", "OK_TO_CROSS"},{"C", "CALL_FIRST"},{"D", "PERCENT_OF_VOLUME"},{"E", "DO_NOT_INCREASE"},{"F", "DO_NOT_REDUCE"},{"G", "ALL_OR_NONE"},{"I", "INSTITUTIONS_ONLY"},{"L", "LAST_PEG"},{"M", "MID_PRICE_PEG"},{"N", "NON_NEGOTIABLE"},{"O", "OPENING_PEG"},{"P", "MARKET_PEG"},{"R", "PRIMARY_PEG"},{"S", "SUSPEND"},{"U", "CUSTOMER_DISPLAY_INSTRUCTION"},{"V", "NETTING"}], "TagNum" => "18"}


,
"ExecRefID" => #{"TagNum" => "19" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "19" => #{"Name"=>"ExecRefID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "19"}


,
"ExecTransType" => #{"TagNum" => "20" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "CANCEL"},{"2", "CORRECT"},{"3", "STATUS"}]}
, "20" => #{"Name"=>"ExecTransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "CANCEL"},{"2", "CORRECT"},{"3", "STATUS"}], "TagNum" => "20"}


,
"HandlInst" => #{"TagNum" => "21" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "AUTOMATED_EXECUTION_ORDER_PRIVATE_NO_BROKER_INTERVENTION"},{"2", "AUTOMATED_EXECUTION_ORDER_PUBLIC_BROKER_INTERVENTION_OK"},{"3", "MANUAL_ORDER_BEST_EXECUTION"}]}
, "21" => #{"Name"=>"HandlInst" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "AUTOMATED_EXECUTION_ORDER_PRIVATE_NO_BROKER_INTERVENTION"},{"2", "AUTOMATED_EXECUTION_ORDER_PUBLIC_BROKER_INTERVENTION_OK"},{"3", "MANUAL_ORDER_BEST_EXECUTION"}], "TagNum" => "21"}


,
"IDSource" => #{"TagNum" => "22" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "CUSIP"},{"2", "SEDOL"},{"3", "QUIK"},{"4", "ISIN_NUMBER"},{"5", "RIC_CODE"},{"6", "ISO_CURRENCY_CODE"},{"7", "ISO_COUNTRY_CODE"}]}
, "22" => #{"Name"=>"IDSource" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "CUSIP"},{"2", "SEDOL"},{"3", "QUIK"},{"4", "ISIN_NUMBER"},{"5", "RIC_CODE"},{"6", "ISO_CURRENCY_CODE"},{"7", "ISO_COUNTRY_CODE"}], "TagNum" => "22"}


,
"IOIid" => #{"TagNum" => "23" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "23" => #{"Name"=>"IOIid" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "23"}


,
"IOIOthSvc" => #{"TagNum" => "24" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "AUTEX"},{"B", "BRIDGE"}]}
, "24" => #{"Name"=>"IOIOthSvc" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "AUTEX"},{"B", "BRIDGE"}], "TagNum" => "24"}


,
"IOIQltyInd" => #{"TagNum" => "25" ,"Type" => "CHAR" ,"ValidValues" =>[{"H", "HIGH"},{"L", "LOW"},{"M", "MEDIUM"}]}
, "25" => #{"Name"=>"IOIQltyInd" ,"Type"=>"CHAR" ,"ValidValues"=>[{"H", "HIGH"},{"L", "LOW"},{"M", "MEDIUM"}], "TagNum" => "25"}


,
"IOIRefID" => #{"TagNum" => "26" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "26" => #{"Name"=>"IOIRefID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "26"}


,
"IOIShares" => #{"TagNum" => "27" ,"Type" => "CHAR" ,"ValidValues" =>[{"L", "LARGE"},{"M", "MEDIUM"},{"S", "SMALL"}]}
, "27" => #{"Name"=>"IOIShares" ,"Type"=>"CHAR" ,"ValidValues"=>[{"L", "LARGE"},{"M", "MEDIUM"},{"S", "SMALL"}], "TagNum" => "27"}


,
"IOITransType" => #{"TagNum" => "28" ,"Type" => "CHAR" ,"ValidValues" =>[{"C", "CANCEL"},{"N", "NEW"},{"R", "REPLACE"}]}
, "28" => #{"Name"=>"IOITransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"C", "CANCEL"},{"N", "NEW"},{"R", "REPLACE"}], "TagNum" => "28"}


,
"LastCapacity" => #{"TagNum" => "29" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "AGENT"},{"2", "CROSS_AS_AGENT"},{"3", "CROSS_AS_PRINCIPAL"},{"4", "PRINCIPAL"}]}
, "29" => #{"Name"=>"LastCapacity" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "AGENT"},{"2", "CROSS_AS_AGENT"},{"3", "CROSS_AS_PRINCIPAL"},{"4", "PRINCIPAL"}], "TagNum" => "29"}


,
"LastMkt" => #{"TagNum" => "30" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "30" => #{"Name"=>"LastMkt" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "30"}


,
"LastPx" => #{"TagNum" => "31" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "31" => #{"Name"=>"LastPx" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "31"}


,
"LastShares" => #{"TagNum" => "32" ,"Type" => "INT" ,"ValidValues" =>[]}
, "32" => #{"Name"=>"LastShares" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "32"}


,
"LinesOfText" => #{"TagNum" => "33" ,"Type" => "INT" ,"ValidValues" =>[]}
, "33" => #{"Name"=>"LinesOfText" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "33"}


,
"MsgSeqNum" => #{"TagNum" => "34" ,"Type" => "INT" ,"ValidValues" =>[]}
, "34" => #{"Name"=>"MsgSeqNum" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "34"}


,
"MsgType" => #{"TagNum" => "35" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "HEARTBEAT"},{"1", "TEST_REQUEST"},{"2", "RESEND_REQUEST"},{"3", "REJECT"},{"4", "SEQUENCE_RESET"},{"5", "LOGOUT"},{"6", "INDICATION_OF_INTEREST"},{"7", "ADVERTISEMENT"},{"8", "EXECUTION_REPORT"},{"9", "ORDER_CANCEL_REJECT"},{"A", "LOGON"},{"B", "NEWS"},{"C", "EMAIL"},{"D", "ORDER_D"},{"E", "ORDER_E"},{"F", "ORDER_CANCEL_REQUEST"},{"G", "ORDER_CANCEL_REPLACE_REQUEST"},{"H", "ORDER_STATUS_REQUEST"},{"J", "ALLOCATION"},{"K", "LIST_CANCEL_REQUEST"},{"L", "LIST_EXECUTE"},{"M", "LIST_STATUS_REQUEST"},{"N", "LIST_STATUS"},{"P", "ALLOCATION_ACK"},{"Q", "DONT_KNOW_TRADE"},{"R", "QUOTE_REQUEST"},{"S", "QUOTE"},{"T", "SETTLEMENT_INSTRUCTIONS"}]}
, "35" => #{"Name"=>"MsgType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "HEARTBEAT"},{"1", "TEST_REQUEST"},{"2", "RESEND_REQUEST"},{"3", "REJECT"},{"4", "SEQUENCE_RESET"},{"5", "LOGOUT"},{"6", "INDICATION_OF_INTEREST"},{"7", "ADVERTISEMENT"},{"8", "EXECUTION_REPORT"},{"9", "ORDER_CANCEL_REJECT"},{"A", "LOGON"},{"B", "NEWS"},{"C", "EMAIL"},{"D", "ORDER_D"},{"E", "ORDER_E"},{"F", "ORDER_CANCEL_REQUEST"},{"G", "ORDER_CANCEL_REPLACE_REQUEST"},{"H", "ORDER_STATUS_REQUEST"},{"J", "ALLOCATION"},{"K", "LIST_CANCEL_REQUEST"},{"L", "LIST_EXECUTE"},{"M", "LIST_STATUS_REQUEST"},{"N", "LIST_STATUS"},{"P", "ALLOCATION_ACK"},{"Q", "DONT_KNOW_TRADE"},{"R", "QUOTE_REQUEST"},{"S", "QUOTE"},{"T", "SETTLEMENT_INSTRUCTIONS"}], "TagNum" => "35"}


,
"NewSeqNo" => #{"TagNum" => "36" ,"Type" => "INT" ,"ValidValues" =>[]}
, "36" => #{"Name"=>"NewSeqNo" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "36"}


,
"OrderID" => #{"TagNum" => "37" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "37" => #{"Name"=>"OrderID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "37"}


,
"OrderQty" => #{"TagNum" => "38" ,"Type" => "INT" ,"ValidValues" =>[]}
, "38" => #{"Name"=>"OrderQty" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "38"}


,
"OrdStatus" => #{"TagNum" => "39" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "PARTIALLY_FILLED"},{"2", "FILLED"},{"3", "DONE_FOR_DAY"},{"4", "CANCELED"},{"5", "REPLACED"},{"6", "PENDING_CANCEL_REPLACE"},{"7", "STOPPED"},{"8", "REJECTED"},{"9", "SUSPENDED"},{"A", "PENDING_NEW"},{"B", "CALCULATED"},{"C", "EXPIRED"}]}
, "39" => #{"Name"=>"OrdStatus" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "PARTIALLY_FILLED"},{"2", "FILLED"},{"3", "DONE_FOR_DAY"},{"4", "CANCELED"},{"5", "REPLACED"},{"6", "PENDING_CANCEL_REPLACE"},{"7", "STOPPED"},{"8", "REJECTED"},{"9", "SUSPENDED"},{"A", "PENDING_NEW"},{"B", "CALCULATED"},{"C", "EXPIRED"}], "TagNum" => "39"}


,
"OrdType" => #{"TagNum" => "40" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "MARKET"},{"2", "LIMIT"},{"3", "STOP"},{"4", "STOP_LIMIT"},{"5", "MARKET_ON_CLOSE"},{"6", "WITH_OR_WITHOUT"},{"7", "LIMIT_OR_BETTER"},{"8", "LIMIT_WITH_OR_WITHOUT"},{"9", "ON_BASIS"},{"A", "ON_CLOSE"},{"B", "LIMIT_ON_CLOSE"},{"C", "FOREX_C"},{"D", "PREVIOUSLY_QUOTED"},{"E", "PREVIOUSLY_INDICATED"},{"F", "FOREX_F"},{"G", "FOREX_G"},{"H", "FOREX_H"},{"P", "PEGGED"}]}
, "40" => #{"Name"=>"OrdType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "MARKET"},{"2", "LIMIT"},{"3", "STOP"},{"4", "STOP_LIMIT"},{"5", "MARKET_ON_CLOSE"},{"6", "WITH_OR_WITHOUT"},{"7", "LIMIT_OR_BETTER"},{"8", "LIMIT_WITH_OR_WITHOUT"},{"9", "ON_BASIS"},{"A", "ON_CLOSE"},{"B", "LIMIT_ON_CLOSE"},{"C", "FOREX_C"},{"D", "PREVIOUSLY_QUOTED"},{"E", "PREVIOUSLY_INDICATED"},{"F", "FOREX_F"},{"G", "FOREX_G"},{"H", "FOREX_H"},{"P", "PEGGED"}], "TagNum" => "40"}


,
"OrigClOrdID" => #{"TagNum" => "41" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "41" => #{"Name"=>"OrigClOrdID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "41"}


,
"OrigTime" => #{"TagNum" => "42" ,"Type" => "TIME" ,"ValidValues" =>[]}
, "42" => #{"Name"=>"OrigTime" ,"Type"=>"TIME" ,"ValidValues"=>[], "TagNum" => "42"}


,
"PossDupFlag" => #{"TagNum" => "43" ,"Type" => "CHAR" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "43" => #{"Name"=>"PossDupFlag" ,"Type"=>"CHAR" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "43"}


,
"Price" => #{"TagNum" => "44" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "44" => #{"Name"=>"Price" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "44"}


,
"RefSeqNum" => #{"TagNum" => "45" ,"Type" => "INT" ,"ValidValues" =>[]}
, "45" => #{"Name"=>"RefSeqNum" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "45"}


,
"RelatdSym" => #{"TagNum" => "46" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "46" => #{"Name"=>"RelatdSym" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "46"}


,
"Rule80A" => #{"TagNum" => "47" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "AGENCY_SINGLE_ORDER"},{"B", "SHORT_EXEMPT_TRANSACTION_B"},{"C", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_MEMBER_FIRM_ORG"},{"D", "PROGRAM_ORDER_INDEX_ARB_FOR_MEMBER_FIRM_ORG"},{"E", "REGISTERED_EQUITY_MARKET_MAKER_TRADES"},{"F", "SHORT_EXEMPT_TRANSACTION_F"},{"H", "SHORT_EXEMPT_TRANSACTION_H"},{"I", "INDIVIDUAL_INVESTOR_SINGLE_ORDER"},{"J", "PROGRAM_ORDER_INDEX_ARB_FOR_INDIVIDUAL_CUSTOMER"},{"K", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_INDIVIDUAL_CUSTOMER"},{"L", "SHORT_EXEMPT_TRANSACTION_FOR_MEMBER_COMPETING_MARKET_MAKER_AFFILIATED_WITH_THE_FIRM_CLEARING_THE_TRADE"},{"M", "PROGRAM_ORDER_INDEX_ARB_FOR_OTHER_MEMBER"},{"N", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_OTHER_MEMBER"},{"O", "COMPETING_DEALER_TRADES_O"},{"P", "PRINCIPAL"},{"R", "COMPETING_DEALER_TRADES_R"},{"S", "SPECIALIST_TRADES"},{"T", "COMPETING_DEALER_TRADES_T"},{"U", "PROGRAM_ORDER_INDEX_ARB_FOR_OTHER_AGENCY"},{"W", "ALL_OTHER_ORDERS_AS_AGENT_FOR_OTHER_MEMBER"},{"X", "SHORT_EXEMPT_TRANSACTION_FOR_MEMBER_COMPETING_MARKET_MAKER_NOT_AFFILIATED_WITH_THE_FIRM_CLEARING_THE_TRADE"},{"Y", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_OTHER_AGENCY"},{"Z", "SHORT_EXEMPT_TRANSACTION_FOR_NON_MEMBER_COMPETING_MARKET_MAKER"}]}
, "47" => #{"Name"=>"Rule80A" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "AGENCY_SINGLE_ORDER"},{"B", "SHORT_EXEMPT_TRANSACTION_B"},{"C", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_MEMBER_FIRM_ORG"},{"D", "PROGRAM_ORDER_INDEX_ARB_FOR_MEMBER_FIRM_ORG"},{"E", "REGISTERED_EQUITY_MARKET_MAKER_TRADES"},{"F", "SHORT_EXEMPT_TRANSACTION_F"},{"H", "SHORT_EXEMPT_TRANSACTION_H"},{"I", "INDIVIDUAL_INVESTOR_SINGLE_ORDER"},{"J", "PROGRAM_ORDER_INDEX_ARB_FOR_INDIVIDUAL_CUSTOMER"},{"K", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_INDIVIDUAL_CUSTOMER"},{"L", "SHORT_EXEMPT_TRANSACTION_FOR_MEMBER_COMPETING_MARKET_MAKER_AFFILIATED_WITH_THE_FIRM_CLEARING_THE_TRADE"},{"M", "PROGRAM_ORDER_INDEX_ARB_FOR_OTHER_MEMBER"},{"N", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_OTHER_MEMBER"},{"O", "COMPETING_DEALER_TRADES_O"},{"P", "PRINCIPAL"},{"R", "COMPETING_DEALER_TRADES_R"},{"S", "SPECIALIST_TRADES"},{"T", "COMPETING_DEALER_TRADES_T"},{"U", "PROGRAM_ORDER_INDEX_ARB_FOR_OTHER_AGENCY"},{"W", "ALL_OTHER_ORDERS_AS_AGENT_FOR_OTHER_MEMBER"},{"X", "SHORT_EXEMPT_TRANSACTION_FOR_MEMBER_COMPETING_MARKET_MAKER_NOT_AFFILIATED_WITH_THE_FIRM_CLEARING_THE_TRADE"},{"Y", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_OTHER_AGENCY"},{"Z", "SHORT_EXEMPT_TRANSACTION_FOR_NON_MEMBER_COMPETING_MARKET_MAKER"}], "TagNum" => "47"}


,
"SecurityID" => #{"TagNum" => "48" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "48" => #{"Name"=>"SecurityID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "48"}


,
"SenderCompID" => #{"TagNum" => "49" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "49" => #{"Name"=>"SenderCompID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "49"}


,
"SenderSubID" => #{"TagNum" => "50" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "50" => #{"Name"=>"SenderSubID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "50"}


,
"SendingTime" => #{"TagNum" => "52" ,"Type" => "TIME" ,"ValidValues" =>[]}
, "52" => #{"Name"=>"SendingTime" ,"Type"=>"TIME" ,"ValidValues"=>[], "TagNum" => "52"}


,
"Shares" => #{"TagNum" => "53" ,"Type" => "INT" ,"ValidValues" =>[]}
, "53" => #{"Name"=>"Shares" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "53"}


,
"Side" => #{"TagNum" => "54" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "BUY"},{"2", "SELL"},{"3", "BUY_MINUS"},{"4", "SELL_PLUS"},{"5", "SELL_SHORT"},{"6", "SELL_SHORT_EXEMPT"},{"7", "UNDISCLOSED"},{"8", "CROSS"}]}
, "54" => #{"Name"=>"Side" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "BUY"},{"2", "SELL"},{"3", "BUY_MINUS"},{"4", "SELL_PLUS"},{"5", "SELL_SHORT"},{"6", "SELL_SHORT_EXEMPT"},{"7", "UNDISCLOSED"},{"8", "CROSS"}], "TagNum" => "54"}


,
"Symbol" => #{"TagNum" => "55" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "55" => #{"Name"=>"Symbol" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "55"}


,
"TargetCompID" => #{"TagNum" => "56" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "56" => #{"Name"=>"TargetCompID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "56"}


,
"TargetSubID" => #{"TagNum" => "57" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "57" => #{"Name"=>"TargetSubID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "57"}


,
"Text" => #{"TagNum" => "58" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "58" => #{"Name"=>"Text" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "58"}


,
"TimeInForce" => #{"TagNum" => "59" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "DAY"},{"1", "GOOD_TILL_CANCEL"},{"2", "AT_THE_OPENING"},{"3", "IMMEDIATE_OR_CANCEL"},{"4", "FILL_OR_KILL"},{"5", "GOOD_TILL_CROSSING"},{"6", "GOOD_TILL_DATE"}]}
, "59" => #{"Name"=>"TimeInForce" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "DAY"},{"1", "GOOD_TILL_CANCEL"},{"2", "AT_THE_OPENING"},{"3", "IMMEDIATE_OR_CANCEL"},{"4", "FILL_OR_KILL"},{"5", "GOOD_TILL_CROSSING"},{"6", "GOOD_TILL_DATE"}], "TagNum" => "59"}


,
"TransactTime" => #{"TagNum" => "60" ,"Type" => "TIME" ,"ValidValues" =>[]}
, "60" => #{"Name"=>"TransactTime" ,"Type"=>"TIME" ,"ValidValues"=>[], "TagNum" => "60"}


,
"Urgency" => #{"TagNum" => "61" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NORMAL"},{"1", "FLASH"},{"2", "BACKGROUND"}]}
, "61" => #{"Name"=>"Urgency" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NORMAL"},{"1", "FLASH"},{"2", "BACKGROUND"}], "TagNum" => "61"}


,
"ValidUntilTime" => #{"TagNum" => "62" ,"Type" => "TIME" ,"ValidValues" =>[]}
, "62" => #{"Name"=>"ValidUntilTime" ,"Type"=>"TIME" ,"ValidValues"=>[], "TagNum" => "62"}


,
"SettlmntTyp" => #{"TagNum" => "63" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "REGULAR"},{"1", "CASH"},{"2", "NEXT_DAY"},{"3", "T_PLUS_2"},{"4", "T_PLUS_3"},{"5", "T_PLUS_4"},{"6", "FUTURE"},{"7", "WHEN_ISSUED"},{"8", "SELLERS_OPTION"},{"9", "T_PLUS_5"}]}
, "63" => #{"Name"=>"SettlmntTyp" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "REGULAR"},{"1", "CASH"},{"2", "NEXT_DAY"},{"3", "T_PLUS_2"},{"4", "T_PLUS_3"},{"5", "T_PLUS_4"},{"6", "FUTURE"},{"7", "WHEN_ISSUED"},{"8", "SELLERS_OPTION"},{"9", "T_PLUS_5"}], "TagNum" => "63"}


,
"FutSettDate" => #{"TagNum" => "64" ,"Type" => "DATE" ,"ValidValues" =>[]}
, "64" => #{"Name"=>"FutSettDate" ,"Type"=>"DATE" ,"ValidValues"=>[], "TagNum" => "64"}


,
"SymbolSfx" => #{"TagNum" => "65" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "65" => #{"Name"=>"SymbolSfx" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "65"}


,
"ListID" => #{"TagNum" => "66" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "66" => #{"Name"=>"ListID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "66"}


,
"ListSeqNo" => #{"TagNum" => "67" ,"Type" => "INT" ,"ValidValues" =>[]}
, "67" => #{"Name"=>"ListSeqNo" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "67"}


,
"ListNoOrds" => #{"TagNum" => "68" ,"Type" => "INT" ,"ValidValues" =>[]}
, "68" => #{"Name"=>"ListNoOrds" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "68"}


,
"ListExecInst" => #{"TagNum" => "69" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "69" => #{"Name"=>"ListExecInst" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "69"}


,
"AllocID" => #{"TagNum" => "70" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "70" => #{"Name"=>"AllocID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "70"}


,
"AllocTransType" => #{"TagNum" => "71" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "REPLACE"},{"2", "CANCEL"},{"3", "PRELIMINARY"},{"4", "CALCULATED"}]}
, "71" => #{"Name"=>"AllocTransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "REPLACE"},{"2", "CANCEL"},{"3", "PRELIMINARY"},{"4", "CALCULATED"}], "TagNum" => "71"}


,
"RefAllocID" => #{"TagNum" => "72" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "72" => #{"Name"=>"RefAllocID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "72"}


,
"NoOrders" => #{"TagNum" => "73" ,"Type" => "INT" ,"ValidValues" =>[]}
, "73" => #{"Name"=>"NoOrders" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "73"}


,
"AvgPrxPrecision" => #{"TagNum" => "74" ,"Type" => "INT" ,"ValidValues" =>[]}
, "74" => #{"Name"=>"AvgPrxPrecision" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "74"}


,
"TradeDate" => #{"TagNum" => "75" ,"Type" => "DATE" ,"ValidValues" =>[]}
, "75" => #{"Name"=>"TradeDate" ,"Type"=>"DATE" ,"ValidValues"=>[], "TagNum" => "75"}


,
"ExecBroker" => #{"TagNum" => "76" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "76" => #{"Name"=>"ExecBroker" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "76"}


,
"OpenClose" => #{"TagNum" => "77" ,"Type" => "CHAR" ,"ValidValues" =>[{"C", "CLOSE"},{"O", "OPEN"}]}
, "77" => #{"Name"=>"OpenClose" ,"Type"=>"CHAR" ,"ValidValues"=>[{"C", "CLOSE"},{"O", "OPEN"}], "TagNum" => "77"}


,
"NoAllocs" => #{"TagNum" => "78" ,"Type" => "INT" ,"ValidValues" =>[]}
, "78" => #{"Name"=>"NoAllocs" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "78"}


,
"AllocAccount" => #{"TagNum" => "79" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "79" => #{"Name"=>"AllocAccount" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "79"}


,
"AllocShares" => #{"TagNum" => "80" ,"Type" => "INT" ,"ValidValues" =>[]}
, "80" => #{"Name"=>"AllocShares" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "80"}


,
"ProcessCode" => #{"TagNum" => "81" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "REGULAR"},{"1", "SOFT_DOLLAR"},{"2", "STEP_IN"},{"3", "STEP_OUT"},{"4", "SOFT_DOLLAR_STEP_IN"},{"5", "SOFT_DOLLAR_STEP_OUT"},{"6", "PLAN_SPONSOR"}]}
, "81" => #{"Name"=>"ProcessCode" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "REGULAR"},{"1", "SOFT_DOLLAR"},{"2", "STEP_IN"},{"3", "STEP_OUT"},{"4", "SOFT_DOLLAR_STEP_IN"},{"5", "SOFT_DOLLAR_STEP_OUT"},{"6", "PLAN_SPONSOR"}], "TagNum" => "81"}


,
"NoRpts" => #{"TagNum" => "82" ,"Type" => "INT" ,"ValidValues" =>[]}
, "82" => #{"Name"=>"NoRpts" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "82"}


,
"RptSeq" => #{"TagNum" => "83" ,"Type" => "INT" ,"ValidValues" =>[]}
, "83" => #{"Name"=>"RptSeq" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "83"}


,
"CxlQty" => #{"TagNum" => "84" ,"Type" => "INT" ,"ValidValues" =>[]}
, "84" => #{"Name"=>"CxlQty" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "84"}


,
"AllocStatus" => #{"TagNum" => "87" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ACCEPTED"},{"1", "REJECTED"},{"2", "PARTIAL_ACCEPT"},{"3", "RECEIVED"}]}
, "87" => #{"Name"=>"AllocStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ACCEPTED"},{"1", "REJECTED"},{"2", "PARTIAL_ACCEPT"},{"3", "RECEIVED"}], "TagNum" => "87"}


,
"AllocRejCode" => #{"TagNum" => "88" ,"Type" => "INT" ,"ValidValues" =>[{"0", "UNKNOWN_ACCOUNT"},{"1", "INCORRECT_QUANTITY"},{"2", "INCORRECT_AVERAGE_PRICE"},{"3", "UNKNOWN_EXECUTING_BROKER_MNEMONIC"},{"4", "COMMISSION_DIFFERENCE"},{"5", "UNKNOWN_ORDERID"},{"6", "UNKNOWN_LISTID"},{"7", "OTHER"}]}
, "88" => #{"Name"=>"AllocRejCode" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "UNKNOWN_ACCOUNT"},{"1", "INCORRECT_QUANTITY"},{"2", "INCORRECT_AVERAGE_PRICE"},{"3", "UNKNOWN_EXECUTING_BROKER_MNEMONIC"},{"4", "COMMISSION_DIFFERENCE"},{"5", "UNKNOWN_ORDERID"},{"6", "UNKNOWN_LISTID"},{"7", "OTHER"}], "TagNum" => "88"}


,
"Signature" => #{"TagNum" => "89" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "89" => #{"Name"=>"Signature" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "89"}


,
"SecureDataLen" => #{"TagNum" => "90" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "90" => #{"Name"=>"SecureDataLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "90"}


,
"SecureData" => #{"TagNum" => "91" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "91" => #{"Name"=>"SecureData" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "91"}


,
"BrokerOfCredit" => #{"TagNum" => "92" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "92" => #{"Name"=>"BrokerOfCredit" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "92"}


,
"SignatureLength" => #{"TagNum" => "93" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "93" => #{"Name"=>"SignatureLength" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "93"}


,
"EmailType" => #{"TagNum" => "94" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "REPLY"},{"2", "ADMIN_REPLY"}]}
, "94" => #{"Name"=>"EmailType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "REPLY"},{"2", "ADMIN_REPLY"}], "TagNum" => "94"}


,
"RawDataLength" => #{"TagNum" => "95" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "95" => #{"Name"=>"RawDataLength" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "95"}


,
"RawData" => #{"TagNum" => "96" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "96" => #{"Name"=>"RawData" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "96"}


,
"PossResend" => #{"TagNum" => "97" ,"Type" => "CHAR" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "97" => #{"Name"=>"PossResend" ,"Type"=>"CHAR" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "97"}


,
"EncryptMethod" => #{"TagNum" => "98" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NONE"},{"1", "PKCS"},{"2", "DES"},{"3", "PKCS_DES"},{"4", "PGP_DES"},{"5", "PGP_DES_MD5"},{"6", "PEM_DES_MD5"}]}
, "98" => #{"Name"=>"EncryptMethod" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NONE"},{"1", "PKCS"},{"2", "DES"},{"3", "PKCS_DES"},{"4", "PGP_DES"},{"5", "PGP_DES_MD5"},{"6", "PEM_DES_MD5"}], "TagNum" => "98"}


,
"StopPx" => #{"TagNum" => "99" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "99" => #{"Name"=>"StopPx" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "99"}


,
"ExDestination" => #{"TagNum" => "100" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "100" => #{"Name"=>"ExDestination" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "100"}


,
"CxlRejReason" => #{"TagNum" => "102" ,"Type" => "INT" ,"ValidValues" =>[{"0", "TOO_LATE_TO_CANCEL"},{"1", "UNKNOWN_ORDER"}]}
, "102" => #{"Name"=>"CxlRejReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "TOO_LATE_TO_CANCEL"},{"1", "UNKNOWN_ORDER"}], "TagNum" => "102"}


,
"OrdRejReason" => #{"TagNum" => "103" ,"Type" => "INT" ,"ValidValues" =>[{"0", "BROKER_OPTION"},{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE_CLOSED"},{"3", "ORDER_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "UNKNOWN_ORDER"},{"6", "DUPLICATE_ORDER"}]}
, "103" => #{"Name"=>"OrdRejReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "BROKER_OPTION"},{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE_CLOSED"},{"3", "ORDER_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "UNKNOWN_ORDER"},{"6", "DUPLICATE_ORDER"}], "TagNum" => "103"}


,
"IOIQualifier" => #{"TagNum" => "104" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "ALL_OR_NONE"},{"C", "AT_THE_CLOSE"},{"I", "IN_TOUCH_WITH"},{"L", "LIMIT"},{"M", "MORE_BEHIND"},{"O", "AT_THE_OPEN"},{"P", "TAKING_A_POSITION"},{"Q", "AT_THE_MARKET"},{"S", "PORTFOLIO_SHOW_N"},{"T", "THROUGH_THE_DAY"},{"V", "VERSUS"},{"W", "INDICATION"},{"X", "CROSSING_OPPORTUNITY"},{"Y", "AT_THE_MIDPOINT"},{"Z", "PRE_OPEN"}]}
, "104" => #{"Name"=>"IOIQualifier" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "ALL_OR_NONE"},{"C", "AT_THE_CLOSE"},{"I", "IN_TOUCH_WITH"},{"L", "LIMIT"},{"M", "MORE_BEHIND"},{"O", "AT_THE_OPEN"},{"P", "TAKING_A_POSITION"},{"Q", "AT_THE_MARKET"},{"S", "PORTFOLIO_SHOW_N"},{"T", "THROUGH_THE_DAY"},{"V", "VERSUS"},{"W", "INDICATION"},{"X", "CROSSING_OPPORTUNITY"},{"Y", "AT_THE_MIDPOINT"},{"Z", "PRE_OPEN"}], "TagNum" => "104"}


,
"WaveNo" => #{"TagNum" => "105" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "105" => #{"Name"=>"WaveNo" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "105"}


,
"Issuer" => #{"TagNum" => "106" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "106" => #{"Name"=>"Issuer" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "106"}


,
"SecurityDesc" => #{"TagNum" => "107" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "107" => #{"Name"=>"SecurityDesc" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "107"}


,
"HeartBtInt" => #{"TagNum" => "108" ,"Type" => "INT" ,"ValidValues" =>[]}
, "108" => #{"Name"=>"HeartBtInt" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "108"}


,
"ClientID" => #{"TagNum" => "109" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "109" => #{"Name"=>"ClientID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "109"}


,
"MinQty" => #{"TagNum" => "110" ,"Type" => "INT" ,"ValidValues" =>[]}
, "110" => #{"Name"=>"MinQty" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "110"}


,
"MaxFloor" => #{"TagNum" => "111" ,"Type" => "INT" ,"ValidValues" =>[]}
, "111" => #{"Name"=>"MaxFloor" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "111"}


,
"TestReqID" => #{"TagNum" => "112" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "112" => #{"Name"=>"TestReqID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "112"}


,
"ReportToExch" => #{"TagNum" => "113" ,"Type" => "CHAR" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "113" => #{"Name"=>"ReportToExch" ,"Type"=>"CHAR" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "113"}


,
"LocateReqd" => #{"TagNum" => "114" ,"Type" => "CHAR" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "114" => #{"Name"=>"LocateReqd" ,"Type"=>"CHAR" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "114"}


,
"OnBehalfOfCompID" => #{"TagNum" => "115" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "115" => #{"Name"=>"OnBehalfOfCompID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "115"}


,
"OnBehalfOfSubID" => #{"TagNum" => "116" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "116" => #{"Name"=>"OnBehalfOfSubID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "116"}


,
"QuoteID" => #{"TagNum" => "117" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "117" => #{"Name"=>"QuoteID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "117"}


,
"NetMoney" => #{"TagNum" => "118" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "118" => #{"Name"=>"NetMoney" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "118"}


,
"SettlCurrAmt" => #{"TagNum" => "119" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "119" => #{"Name"=>"SettlCurrAmt" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "119"}


,
"SettlCurrency" => #{"TagNum" => "120" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "120" => #{"Name"=>"SettlCurrency" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "120"}


,
"ForexReq" => #{"TagNum" => "121" ,"Type" => "CHAR" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "121" => #{"Name"=>"ForexReq" ,"Type"=>"CHAR" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "121"}


,
"OrigSendingTime" => #{"TagNum" => "122" ,"Type" => "TIME" ,"ValidValues" =>[]}
, "122" => #{"Name"=>"OrigSendingTime" ,"Type"=>"TIME" ,"ValidValues"=>[], "TagNum" => "122"}


,
"GapFillFlag" => #{"TagNum" => "123" ,"Type" => "CHAR" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "123" => #{"Name"=>"GapFillFlag" ,"Type"=>"CHAR" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "123"}


,
"NoExecs" => #{"TagNum" => "124" ,"Type" => "INT" ,"ValidValues" =>[]}
, "124" => #{"Name"=>"NoExecs" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "124"}


,
"ExpireTime" => #{"TagNum" => "126" ,"Type" => "TIME" ,"ValidValues" =>[]}
, "126" => #{"Name"=>"ExpireTime" ,"Type"=>"TIME" ,"ValidValues"=>[], "TagNum" => "126"}


,
"DKReason" => #{"TagNum" => "127" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "UNKNOWN_SYMBOL"},{"B", "WRONG_SIDE"},{"C", "QUANTITY_EXCEEDS_ORDER"},{"D", "NO_MATCHING_ORDER"},{"E", "PRICE_EXCEEDS_LIMIT"},{"Z", "OTHER"}]}
, "127" => #{"Name"=>"DKReason" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "UNKNOWN_SYMBOL"},{"B", "WRONG_SIDE"},{"C", "QUANTITY_EXCEEDS_ORDER"},{"D", "NO_MATCHING_ORDER"},{"E", "PRICE_EXCEEDS_LIMIT"},{"Z", "OTHER"}], "TagNum" => "127"}


,
"DeliverToCompID" => #{"TagNum" => "128" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "128" => #{"Name"=>"DeliverToCompID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "128"}


,
"DeliverToSubID" => #{"TagNum" => "129" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "129" => #{"Name"=>"DeliverToSubID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "129"}


,
"IOINaturalFlag" => #{"TagNum" => "130" ,"Type" => "CHAR" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "130" => #{"Name"=>"IOINaturalFlag" ,"Type"=>"CHAR" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "130"}


,
"QuoteReqID" => #{"TagNum" => "131" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "131" => #{"Name"=>"QuoteReqID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "131"}


,
"BidPx" => #{"TagNum" => "132" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "132" => #{"Name"=>"BidPx" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "132"}


,
"OfferPx" => #{"TagNum" => "133" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "133" => #{"Name"=>"OfferPx" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "133"}


,
"BidSize" => #{"TagNum" => "134" ,"Type" => "INT" ,"ValidValues" =>[]}
, "134" => #{"Name"=>"BidSize" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "134"}


,
"OfferSize" => #{"TagNum" => "135" ,"Type" => "INT" ,"ValidValues" =>[]}
, "135" => #{"Name"=>"OfferSize" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "135"}


,
"NoMiscFees" => #{"TagNum" => "136" ,"Type" => "INT" ,"ValidValues" =>[]}
, "136" => #{"Name"=>"NoMiscFees" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "136"}


,
"MiscFeeAmt" => #{"TagNum" => "137" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "137" => #{"Name"=>"MiscFeeAmt" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "137"}


,
"MiscFeeCurr" => #{"TagNum" => "138" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "138" => #{"Name"=>"MiscFeeCurr" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "138"}


,
"MiscFeeType" => #{"TagNum" => "139" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "REGULATORY"},{"2", "TAX"},{"3", "LOCAL_COMMISSION"},{"4", "EXCHANGE_FEES"},{"5", "STAMP"},{"6", "LEVY"},{"7", "OTHER"},{"8", "MARKUP"}]}
, "139" => #{"Name"=>"MiscFeeType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "REGULATORY"},{"2", "TAX"},{"3", "LOCAL_COMMISSION"},{"4", "EXCHANGE_FEES"},{"5", "STAMP"},{"6", "LEVY"},{"7", "OTHER"},{"8", "MARKUP"}], "TagNum" => "139"}


,
"PrevClosePx" => #{"TagNum" => "140" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "140" => #{"Name"=>"PrevClosePx" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "140"}


,
"ResetSeqNumFlag" => #{"TagNum" => "141" ,"Type" => "CHAR" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "141" => #{"Name"=>"ResetSeqNumFlag" ,"Type"=>"CHAR" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "141"}


,
"SenderLocationID" => #{"TagNum" => "142" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "142" => #{"Name"=>"SenderLocationID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "142"}


,
"TargetLocationID" => #{"TagNum" => "143" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "143" => #{"Name"=>"TargetLocationID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "143"}


,
"OnBehalfOfLocationID" => #{"TagNum" => "144" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "144" => #{"Name"=>"OnBehalfOfLocationID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "144"}


,
"DeliverToLocationID" => #{"TagNum" => "145" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "145" => #{"Name"=>"DeliverToLocationID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "145"}


,
"NoRelatedSym" => #{"TagNum" => "146" ,"Type" => "INT" ,"ValidValues" =>[]}
, "146" => #{"Name"=>"NoRelatedSym" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "146"}


,
"Subject" => #{"TagNum" => "147" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "147" => #{"Name"=>"Subject" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "147"}


,
"Headline" => #{"TagNum" => "148" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "148" => #{"Name"=>"Headline" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "148"}


,
"URLLink" => #{"TagNum" => "149" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "149" => #{"Name"=>"URLLink" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "149"}


,
"ExecType" => #{"TagNum" => "150" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "PARTIAL_FILL"},{"2", "FILL"},{"3", "DONE_FOR_DAY"},{"4", "CANCELLED"},{"5", "REPLACE"},{"6", "PENDING_CANCEL_REPLACE"},{"7", "STOPPED"},{"8", "REJECTED"},{"9", "SUSPENDED"},{"A", "PENDING_NEW"},{"B", "CALCULATED"},{"C", "EXPIRED"}]}
, "150" => #{"Name"=>"ExecType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "PARTIAL_FILL"},{"2", "FILL"},{"3", "DONE_FOR_DAY"},{"4", "CANCELLED"},{"5", "REPLACE"},{"6", "PENDING_CANCEL_REPLACE"},{"7", "STOPPED"},{"8", "REJECTED"},{"9", "SUSPENDED"},{"A", "PENDING_NEW"},{"B", "CALCULATED"},{"C", "EXPIRED"}], "TagNum" => "150"}


,
"LeavesQty" => #{"TagNum" => "151" ,"Type" => "INT" ,"ValidValues" =>[]}
, "151" => #{"Name"=>"LeavesQty" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "151"}


,
"CashOrderQty" => #{"TagNum" => "152" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "152" => #{"Name"=>"CashOrderQty" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "152"}


,
"AllocAvgPx" => #{"TagNum" => "153" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "153" => #{"Name"=>"AllocAvgPx" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "153"}


,
"AllocNetMoney" => #{"TagNum" => "154" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "154" => #{"Name"=>"AllocNetMoney" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "154"}


,
"SettlCurrFxRate" => #{"TagNum" => "155" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "155" => #{"Name"=>"SettlCurrFxRate" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "155"}


,
"SettlCurrFxRateCalc" => #{"TagNum" => "156" ,"Type" => "CHAR" ,"ValidValues" =>[{"M", "MULTIPLY"},{"D", "DIVIDE"}]}
, "156" => #{"Name"=>"SettlCurrFxRateCalc" ,"Type"=>"CHAR" ,"ValidValues"=>[{"M", "MULTIPLY"},{"D", "DIVIDE"}], "TagNum" => "156"}


,
"NumDaysInterest" => #{"TagNum" => "157" ,"Type" => "INT" ,"ValidValues" =>[]}
, "157" => #{"Name"=>"NumDaysInterest" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "157"}


,
"AccruedInterestRate" => #{"TagNum" => "158" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "158" => #{"Name"=>"AccruedInterestRate" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "158"}


,
"AccruedInterestAmt" => #{"TagNum" => "159" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "159" => #{"Name"=>"AccruedInterestAmt" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "159"}


,
"SettlInstMode" => #{"TagNum" => "160" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "DEFAULT"},{"1", "STANDING_INSTRUCTIONS_PROVIDED"},{"2", "SPECIFIC_ALLOCATION_ACCOUNT_OVERRIDING"},{"3", "SPECIFIC_ALLOCATION_ACCOUNT_STANDING"}]}
, "160" => #{"Name"=>"SettlInstMode" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "DEFAULT"},{"1", "STANDING_INSTRUCTIONS_PROVIDED"},{"2", "SPECIFIC_ALLOCATION_ACCOUNT_OVERRIDING"},{"3", "SPECIFIC_ALLOCATION_ACCOUNT_STANDING"}], "TagNum" => "160"}


,
"AllocText" => #{"TagNum" => "161" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "161" => #{"Name"=>"AllocText" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "161"}


,
"SettlInstID" => #{"TagNum" => "162" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "162" => #{"Name"=>"SettlInstID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "162"}


,
"SettlInstTransType" => #{"TagNum" => "163" ,"Type" => "CHAR" ,"ValidValues" =>[{"C", "CANCEL"},{"N", "NEW"},{"R", "REPLACE"}]}
, "163" => #{"Name"=>"SettlInstTransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"C", "CANCEL"},{"N", "NEW"},{"R", "REPLACE"}], "TagNum" => "163"}


,
"EmailThreadID" => #{"TagNum" => "164" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "164" => #{"Name"=>"EmailThreadID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "164"}


,
"SettlInstSource" => #{"TagNum" => "165" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "BROKERS_INSTRUCTIONS"},{"2", "INSTITUTIONS_INSTRUCTIONS"}]}
, "165" => #{"Name"=>"SettlInstSource" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "BROKERS_INSTRUCTIONS"},{"2", "INSTITUTIONS_INSTRUCTIONS"}], "TagNum" => "165"}


,
"SettlLocation" => #{"TagNum" => "166" ,"Type" => "CHAR" ,"ValidValues" =>[{"CED", "CEDEL"},{"DTC", "DEPOSITORY_TRUST_COMPANY"},{"EUR", "EUROCLEAR"},{"FED", "FEDERAL_BOOK_ENTRY"},{"ISO Country Code", "LOCAL_MARKET_SETTLE_LOCATION"},{"PNY", "PHYSICAL"},{"PTC", "PARTICIPANT_TRUST_COMPANY"}]}
, "166" => #{"Name"=>"SettlLocation" ,"Type"=>"CHAR" ,"ValidValues"=>[{"CED", "CEDEL"},{"DTC", "DEPOSITORY_TRUST_COMPANY"},{"EUR", "EUROCLEAR"},{"FED", "FEDERAL_BOOK_ENTRY"},{"ISO Country Code", "LOCAL_MARKET_SETTLE_LOCATION"},{"PNY", "PHYSICAL"},{"PTC", "PARTICIPANT_TRUST_COMPANY"}], "TagNum" => "166"}


,
"SecurityType" => #{"TagNum" => "167" ,"Type" => "CHAR" ,"ValidValues" =>[{"BA", "BANKERS_ACCEPTANCE"},{"CD", "CERTIFICATE_OF_DEPOSIT"},{"CMO", "COLLATERALIZE_MORTGAGE_OBLIGATION"},{"CORP", "CORPORATE_BOND"},{"CP", "COMMERCIAL_PAPER"},{"CPP", "CORPORATE_PRIVATE_PLACEMENT"},{"CS", "COMMON_STOCK"},{"FHA", "FEDERAL_HOUSING_AUTHORITY"},{"FHL", "FEDERAL_HOME_LOAN"},{"FN", "FEDERAL_NATIONAL_MORTGAGE_ASSOCIATION"},{"FOR", "FOREIGN_EXCHANGE_CONTRACT"},{"FUT", "FUTURE"},{"GN", "GOVERNMENT_NATIONAL_MORTGAGE_ASSOCIATION"},{"GOVT", "TREASURIES_PLUS_AGENCY_DEBENTURE"},{"MF", "MUTUAL_FUND"},{"MIO", "MORTGAGE_INTEREST_ONLY"},{"MPO", "MORTGAGE_PRINCIPLE_ONLY"},{"MPP", "MORTGAGE_PRIVATE_PLACEMENT"},{"MPT", "MISCELLANEOUS_PASS_THRU"},{"MUNI", "MUNICIPAL_BOND"},{"NONE", "NO_ISITC_SECURITY_TYPE"},{"OPT", "OPTION"},{"PS", "PREFERRED_STOCK"},{"RP", "REPURCHASE_AGREEMENT"},{"RVRP", "REVERSE_REPURCHASE_AGREEMENT"},{"SL", "STUDENT_LOAN_MARKETING_ASSOCIATION"},{"TD", "TIME_DEPOSIT"},{"USTB", "US_TREASURY_BILL"},{"WAR", "WARRANT"},{"ZOO", "CATS_TIGERS_LIONS"}]}
, "167" => #{"Name"=>"SecurityType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"BA", "BANKERS_ACCEPTANCE"},{"CD", "CERTIFICATE_OF_DEPOSIT"},{"CMO", "COLLATERALIZE_MORTGAGE_OBLIGATION"},{"CORP", "CORPORATE_BOND"},{"CP", "COMMERCIAL_PAPER"},{"CPP", "CORPORATE_PRIVATE_PLACEMENT"},{"CS", "COMMON_STOCK"},{"FHA", "FEDERAL_HOUSING_AUTHORITY"},{"FHL", "FEDERAL_HOME_LOAN"},{"FN", "FEDERAL_NATIONAL_MORTGAGE_ASSOCIATION"},{"FOR", "FOREIGN_EXCHANGE_CONTRACT"},{"FUT", "FUTURE"},{"GN", "GOVERNMENT_NATIONAL_MORTGAGE_ASSOCIATION"},{"GOVT", "TREASURIES_PLUS_AGENCY_DEBENTURE"},{"MF", "MUTUAL_FUND"},{"MIO", "MORTGAGE_INTEREST_ONLY"},{"MPO", "MORTGAGE_PRINCIPLE_ONLY"},{"MPP", "MORTGAGE_PRIVATE_PLACEMENT"},{"MPT", "MISCELLANEOUS_PASS_THRU"},{"MUNI", "MUNICIPAL_BOND"},{"NONE", "NO_ISITC_SECURITY_TYPE"},{"OPT", "OPTION"},{"PS", "PREFERRED_STOCK"},{"RP", "REPURCHASE_AGREEMENT"},{"RVRP", "REVERSE_REPURCHASE_AGREEMENT"},{"SL", "STUDENT_LOAN_MARKETING_ASSOCIATION"},{"TD", "TIME_DEPOSIT"},{"USTB", "US_TREASURY_BILL"},{"WAR", "WARRANT"},{"ZOO", "CATS_TIGERS_LIONS"}], "TagNum" => "167"}


,
"EffectiveTime" => #{"TagNum" => "168" ,"Type" => "TIME" ,"ValidValues" =>[]}
, "168" => #{"Name"=>"EffectiveTime" ,"Type"=>"TIME" ,"ValidValues"=>[], "TagNum" => "168"}


,
"StandInstDbType" => #{"TagNum" => "169" ,"Type" => "INT" ,"ValidValues" =>[{"0", "OTHER"},{"1", "DTC_SID"},{"2", "THOMSON_ALERT"},{"3", "A_GLOBAL_CUSTODIAN"}]}
, "169" => #{"Name"=>"StandInstDbType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "OTHER"},{"1", "DTC_SID"},{"2", "THOMSON_ALERT"},{"3", "A_GLOBAL_CUSTODIAN"}], "TagNum" => "169"}


,
"StandInstDbName" => #{"TagNum" => "170" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "170" => #{"Name"=>"StandInstDbName" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "170"}


,
"StandInstDbID" => #{"TagNum" => "171" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "171" => #{"Name"=>"StandInstDbID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "171"}


,
"SettlDeliveryType" => #{"TagNum" => "172" ,"Type" => "INT" ,"ValidValues" =>[]}
, "172" => #{"Name"=>"SettlDeliveryType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "172"}


,
"SettlDepositoryCode" => #{"TagNum" => "173" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "173" => #{"Name"=>"SettlDepositoryCode" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "173"}


,
"SettlBrkrCode" => #{"TagNum" => "174" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "174" => #{"Name"=>"SettlBrkrCode" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "174"}


,
"SettlInstCode" => #{"TagNum" => "175" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "175" => #{"Name"=>"SettlInstCode" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "175"}


,
"SecuritySettlAgentName" => #{"TagNum" => "176" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "176" => #{"Name"=>"SecuritySettlAgentName" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "176"}


,
"SecuritySettlAgentCode" => #{"TagNum" => "177" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "177" => #{"Name"=>"SecuritySettlAgentCode" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "177"}


,
"SecuritySettlAgentAcctNum" => #{"TagNum" => "178" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "178" => #{"Name"=>"SecuritySettlAgentAcctNum" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "178"}


,
"SecuritySettlAgentAcctName" => #{"TagNum" => "179" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "179" => #{"Name"=>"SecuritySettlAgentAcctName" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "179"}


,
"SecuritySettlAgentContactName" => #{"TagNum" => "180" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "180" => #{"Name"=>"SecuritySettlAgentContactName" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "180"}


,
"SecuritySettlAgentContactPhone" => #{"TagNum" => "181" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "181" => #{"Name"=>"SecuritySettlAgentContactPhone" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "181"}


,
"CashSettlAgentName" => #{"TagNum" => "182" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "182" => #{"Name"=>"CashSettlAgentName" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "182"}


,
"CashSettlAgentCode" => #{"TagNum" => "183" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "183" => #{"Name"=>"CashSettlAgentCode" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "183"}


,
"CashSettlAgentAcctNum" => #{"TagNum" => "184" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "184" => #{"Name"=>"CashSettlAgentAcctNum" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "184"}


,
"CashSettlAgentAcctName" => #{"TagNum" => "185" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "185" => #{"Name"=>"CashSettlAgentAcctName" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "185"}


,
"CashSettlAgentContactName" => #{"TagNum" => "186" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "186" => #{"Name"=>"CashSettlAgentContactName" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "186"}


,
"CashSettlAgentContactPhone" => #{"TagNum" => "187" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "187" => #{"Name"=>"CashSettlAgentContactPhone" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "187"}


,
"BidSpotRate" => #{"TagNum" => "188" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "188" => #{"Name"=>"BidSpotRate" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "188"}


,
"BidForwardPoints" => #{"TagNum" => "189" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "189" => #{"Name"=>"BidForwardPoints" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "189"}


,
"OfferSpotRate" => #{"TagNum" => "190" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "190" => #{"Name"=>"OfferSpotRate" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "190"}


,
"OfferForwardPoints" => #{"TagNum" => "191" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "191" => #{"Name"=>"OfferForwardPoints" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "191"}


,
"OrderQty2" => #{"TagNum" => "192" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "192" => #{"Name"=>"OrderQty2" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "192"}


,
"FutSettDate2" => #{"TagNum" => "193" ,"Type" => "DATE" ,"ValidValues" =>[]}
, "193" => #{"Name"=>"FutSettDate2" ,"Type"=>"DATE" ,"ValidValues"=>[], "TagNum" => "193"}


,
"LastSpotRate" => #{"TagNum" => "194" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "194" => #{"Name"=>"LastSpotRate" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "194"}


,
"LastForwardPoints" => #{"TagNum" => "195" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "195" => #{"Name"=>"LastForwardPoints" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "195"}


,
"AllocLinkID" => #{"TagNum" => "196" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "196" => #{"Name"=>"AllocLinkID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "196"}


,
"AllocLinkType" => #{"TagNum" => "197" ,"Type" => "INT" ,"ValidValues" =>[{"0", "F_X_NETTING"},{"1", "F_X_SWAP"}]}
, "197" => #{"Name"=>"AllocLinkType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "F_X_NETTING"},{"1", "F_X_SWAP"}], "TagNum" => "197"}


,
"SecondaryOrderID" => #{"TagNum" => "198" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "198" => #{"Name"=>"SecondaryOrderID" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "198"}


,
"NoIOIQualifiers" => #{"TagNum" => "199" ,"Type" => "INT" ,"ValidValues" =>[]}
, "199" => #{"Name"=>"NoIOIQualifiers" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "199"}


,
"MaturityMonthYear" => #{"TagNum" => "200" ,"Type" => "MONTHYEAR" ,"ValidValues" =>[]}
, "200" => #{"Name"=>"MaturityMonthYear" ,"Type"=>"MONTHYEAR" ,"ValidValues"=>[], "TagNum" => "200"}


,
"PutOrCall" => #{"TagNum" => "201" ,"Type" => "INT" ,"ValidValues" =>[{"0", "PUT"},{"1", "CALL"}]}
, "201" => #{"Name"=>"PutOrCall" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "PUT"},{"1", "CALL"}], "TagNum" => "201"}


,
"StrikePrice" => #{"TagNum" => "202" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "202" => #{"Name"=>"StrikePrice" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "202"}


,
"CoveredOrUncovered" => #{"TagNum" => "203" ,"Type" => "INT" ,"ValidValues" =>[{"0", "COVERED"},{"1", "UNCOVERED"}]}
, "203" => #{"Name"=>"CoveredOrUncovered" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "COVERED"},{"1", "UNCOVERED"}], "TagNum" => "203"}


,
"CustomerOrFirm" => #{"TagNum" => "204" ,"Type" => "INT" ,"ValidValues" =>[{"0", "CUSTOMER"},{"1", "FIRM"}]}
, "204" => #{"Name"=>"CustomerOrFirm" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "CUSTOMER"},{"1", "FIRM"}], "TagNum" => "204"}


,
"MaturityDay" => #{"TagNum" => "205" ,"Type" => "DAYOFMONTH" ,"ValidValues" =>[]}
, "205" => #{"Name"=>"MaturityDay" ,"Type"=>"DAYOFMONTH" ,"ValidValues"=>[], "TagNum" => "205"}


,
"OptAttribute" => #{"TagNum" => "206" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "206" => #{"Name"=>"OptAttribute" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "206"}


,
"SecurityExchange" => #{"TagNum" => "207" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "207" => #{"Name"=>"SecurityExchange" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "207"}


,
"NotifyBrokerOfCredit" => #{"TagNum" => "208" ,"Type" => "CHAR" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "208" => #{"Name"=>"NotifyBrokerOfCredit" ,"Type"=>"CHAR" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "208"}


,
"AllocHandlInst" => #{"TagNum" => "209" ,"Type" => "INT" ,"ValidValues" =>[{"1", "MATCH"},{"2", "FORWARD"},{"3", "FORWARD_AND_MATCH"}]}
, "209" => #{"Name"=>"AllocHandlInst" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "MATCH"},{"2", "FORWARD"},{"3", "FORWARD_AND_MATCH"}], "TagNum" => "209"}


,
"MaxShow" => #{"TagNum" => "210" ,"Type" => "INT" ,"ValidValues" =>[]}
, "210" => #{"Name"=>"MaxShow" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "210"}


,
"PegDifference" => #{"TagNum" => "211" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "211" => #{"Name"=>"PegDifference" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "211"}


}.



components() ->
 #{}.



groups() ->
 #{}.



header()->
#{
"Fields" => #{"BeginString" =>#{"Required" => "Y", "Sequence" => undefined}
,"BodyLength" =>#{"Required" => "Y", "Sequence" => undefined}
,"MsgType" =>#{"Required" => "Y", "Sequence" => undefined}
,"SenderCompID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TargetCompID" =>#{"Required" => "Y", "Sequence" => undefined}
,"OnBehalfOfCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeliverToCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecureDataLen" =>#{"Required" => "N", "Sequence" => undefined}
,"SecureData" =>#{"Required" => "N", "Sequence" => undefined}
,"MsgSeqNum" =>#{"Required" => "Y", "Sequence" => undefined}
,"SenderSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SenderLocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetLocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"OnBehalfOfSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"OnBehalfOfLocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeliverToSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeliverToLocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"PossDupFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"PossResend" =>#{"Required" => "N", "Sequence" => undefined}
,"SendingTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigSendingTime" =>#{"Required" => "N", "Sequence" => undefined}}, 
"Groups" => #{}, 
 "Components" => #{}

}.



trailer()->
#{
"Fields" => #{"SignatureLength" =>#{"Required" => "N", "Sequence" => undefined}
,"Signature" =>#{"Required" => "N", "Sequence" => undefined}
,"CheckSum" =>#{"Required" => "Y", "Sequence" => undefined}}, 
"Groups" => #{}, 
 "Components" => #{}

}.


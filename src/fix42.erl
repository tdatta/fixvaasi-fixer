-module(fix42).
-export([messages/0,
         fields/0,
         components/0,
         groups/0,
         header/0,
         trailer/0]).

messages() ->
 #{"Heartbeat" => #{
                              "Category" => "admin"
                              ,"Type" => "0"
                              ,"Fields" => #{"TestReqID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"0" => #{"Category"=>"admin"
           ,"Name" => "Heartbeat"}

,"TestRequest" => #{
                              "Category" => "admin"
                              ,"Type" => "1"
                              ,"Fields" => #{"TestReqID" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"1" => #{"Category"=>"admin"
           ,"Name" => "TestRequest"}

,"ResendRequest" => #{
                              "Category" => "admin"
                              ,"Type" => "2"
                              ,"Fields" => #{"BeginSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}
,"EndSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"2" => #{"Category"=>"admin"
           ,"Name" => "ResendRequest"}

,"Reject" => #{
                              "Category" => "admin"
                              ,"Type" => "3"
                              ,"Fields" => #{"RefSeqNum" =>#{"Required" => "Y", "Sequence" => undefined}
,"RefTagID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefMsgType" =>#{"Required" => "N", "Sequence" => undefined}
,"SessionRejectReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"3" => #{"Category"=>"admin"
           ,"Name" => "Reject"}

,"SequenceReset" => #{
                              "Category" => "admin"
                              ,"Type" => "4"
                              ,"Fields" => #{"GapFillFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"NewSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"4" => #{"Category"=>"admin"
           ,"Name" => "SequenceReset"}

,"Logout" => #{
                              "Category" => "admin"
                              ,"Type" => "5"
                              ,"Fields" => #{"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"5" => #{"Category"=>"admin"
           ,"Name" => "Logout"}

,"IOI" => #{
                              "Category" => "app"
                              ,"Type" => "6"
                              ,"Fields" => #{"IOIid" =>#{"Required" => "Y", "Sequence" => undefined}
,"IOITransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"IOIRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"IOIShares" =>#{"Required" => "Y", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIQltyInd" =>#{"Required" => "N", "Sequence" => undefined}
,"IOINaturalFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIQualifier" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"URLLink" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingType" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingID" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadToBenchmark" =>#{"Required" => "N", "Sequence" => undefined}
,"Benchmark" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoIOIQualifiers" => #{
                              "Fields" => #{"IOIQualifier" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoRoutingIDs" => #{
                              "Fields" => #{"RoutingType" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"6" => #{"Category"=>"app"
           ,"Name" => "IOI"}

,"Advertisement" => #{
                              "Category" => "app"
                              ,"Type" => "7"
                              ,"Fields" => #{"AdvId" =>#{"Required" => "Y", "Sequence" => undefined}
,"AdvTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"AdvRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"AdvSide" =>#{"Required" => "Y", "Sequence" => undefined}
,"Shares" =>#{"Required" => "Y", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"URLLink" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"7" => #{"Category"=>"app"
           ,"Name" => "Advertisement"}

,"ExecutionReport" => #{
                              "Category" => "app"
                              ,"Type" => "8"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTrader" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTradeQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTradeTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExecTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExecRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecType" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecRestatementReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"PegDifference" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInst" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionOffset" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"Rule80A" =>#{"Required" => "N", "Sequence" => undefined}
,"LastShares" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"LastForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"LastCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"LeavesQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"CumQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"DayOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"DayCumQty" =>#{"Required" => "N", "Sequence" => undefined}
,"DayAvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ReportToExch" =>#{"Required" => "N", "Sequence" => undefined}
,"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"GrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenClose" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFirm" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"MultiLegReportingType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoContraBrokers" => #{
                              "Fields" => #{"ContraBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTrader" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTradeQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTradeTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"8" => #{"Category"=>"app"
           ,"Name" => "ExecutionReport"}

,"OrderCancelReject" => #{
                              "Category" => "app"
                              ,"Type" => "9"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"CxlRejResponseTo" =>#{"Required" => "Y", "Sequence" => undefined}
,"CxlRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"9" => #{"Category"=>"app"
           ,"Name" => "OrderCancelReject"}

,"Logon" => #{
                              "Category" => "admin"
                              ,"Type" => "A"
                              ,"Fields" => #{"EncryptMethod" =>#{"Required" => "Y", "Sequence" => undefined}
,"HeartBtInt" =>#{"Required" => "Y", "Sequence" => undefined}
,"RawDataLength" =>#{"Required" => "N", "Sequence" => undefined}
,"RawData" =>#{"Required" => "N", "Sequence" => undefined}
,"ResetSeqNumFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxMessageSize" =>#{"Required" => "N", "Sequence" => undefined}
,"RefMsgType" =>#{"Required" => "N", "Sequence" => undefined}
,"MsgDirection" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMsgTypes" => #{
                              "Fields" => #{"RefMsgType" =>#{"Required" => "N", "Sequence" => undefined}
,"MsgDirection" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"A" => #{"Category"=>"admin"
           ,"Name" => "Logon"}

,"News" => #{
                              "Category" => "app"
                              ,"Type" => "B"
                              ,"Fields" => #{"OrigTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Urgency" =>#{"Required" => "N", "Sequence" => undefined}
,"Headline" =>#{"Required" => "Y", "Sequence" => undefined}
,"EncodedHeadlineLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedHeadline" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingType" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingID" =>#{"Required" => "N", "Sequence" => undefined}
,"RelatdSym" =>#{"Required" => "N", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "Y", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"URLLink" =>#{"Required" => "N", "Sequence" => undefined}
,"RawDataLength" =>#{"Required" => "N", "Sequence" => undefined}
,"RawData" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRoutingIDs" => #{
                              "Fields" => #{"RoutingType" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoRelatedSym" => #{
                              "Fields" => #{"RelatdSym" =>#{"Required" => "N", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "LinesOfText" => #{
                              "Fields" => #{"Text" =>#{"Required" => "Y", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"B" => #{"Category"=>"app"
           ,"Name" => "News"}

,"Email" => #{
                              "Category" => "app"
                              ,"Type" => "C"
                              ,"Fields" => #{"EmailThreadID" =>#{"Required" => "Y", "Sequence" => undefined}
,"EmailType" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Subject" =>#{"Required" => "Y", "Sequence" => undefined}
,"EncodedSubjectLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSubject" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingType" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingID" =>#{"Required" => "N", "Sequence" => undefined}
,"RelatdSym" =>#{"Required" => "N", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "Y", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"RawDataLength" =>#{"Required" => "N", "Sequence" => undefined}
,"RawData" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRoutingIDs" => #{
                              "Fields" => #{"RoutingType" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoRelatedSym" => #{
                              "Fields" => #{"RelatdSym" =>#{"Required" => "N", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "LinesOfText" => #{
                              "Fields" => #{"Text" =>#{"Required" => "Y", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"C" => #{"Category"=>"app"
           ,"Name" => "Email"}

,"NewOrderSingle" => #{
                              "Category" => "app"
                              ,"Type" => "D"
                              ,"Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocShares" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIid" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"Rule80A" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenClose" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"CustomerOrFirm" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"PegDifference" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInst" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionOffset" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFirm" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingAccount" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocShares" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoTradingSessions" => #{
                              "Fields" => #{"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"D" => #{"Category"=>"app"
           ,"Name" => "NewOrderSingle"}

,"NewOrderList" => #{
                              "Category" => "app"
                              ,"Type" => "E"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"BidID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientBidID" =>#{"Required" => "N", "Sequence" => undefined}
,"ProgRptReqs" =>#{"Required" => "N", "Sequence" => undefined}
,"BidType" =>#{"Required" => "Y", "Sequence" => undefined}
,"ProgPeriodInterval" =>#{"Required" => "N", "Sequence" => undefined}
,"ListExecInstType" =>#{"Required" => "N", "Sequence" => undefined}
,"ListExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedListExecInstLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedListExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoOrders" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlInstMode" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocShares" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"SideValueInd" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIid" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"Rule80A" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenClose" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"CustomerOrFirm" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"PegDifference" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInst" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionOffset" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFirm" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingAccount" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoOrders" => #{
                              "Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlInstMode" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocShares" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"SideValueInd" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIid" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"Rule80A" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenClose" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"CustomerOrFirm" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"PegDifference" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInst" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionOffset" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFirm" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingAccount" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocShares" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoTradingSessions" => #{
                              "Fields" => #{"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"E" => #{"Category"=>"app"
           ,"Name" => "NewOrderList"}

,"OrderCancelRequest" => #{
                              "Category" => "app"
                              ,"Type" => "F"
                              ,"Fields" => #{"OrigClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"F" => #{"Category"=>"app"
           ,"Name" => "OrderCancelRequest"}

,"OrderCancelReplaceRequest" => #{
                              "Category" => "app"
                              ,"Type" => "G"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocShares" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"PegDifference" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInst" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionOffset" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"Rule80A" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenClose" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"CustomerOrFirm" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFirm" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingAccount" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocShares" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoTradingSessions" => #{
                              "Fields" => #{"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"G" => #{"Category"=>"app"
           ,"Name" => "OrderCancelReplaceRequest"}

,"OrderStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "H"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"H" => #{"Category"=>"app"
           ,"Name" => "OrderStatusRequest"}

,"Allocation" => #{
                              "Category" => "app"
                              ,"Type" => "J"
                              ,"Fields" => #{"AllocID" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"RefAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocLinkType" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"WaveNo" =>#{"Required" => "N", "Sequence" => undefined}
,"LastShares" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Shares" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPrxPrecision" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"GrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenClose" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"NumDaysInterest" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocShares" =>#{"Required" => "Y", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"BrokerOfCredit" =>#{"Required" => "N", "Sequence" => undefined}
,"NotifyBrokerOfCredit" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocHandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocNetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstMode" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoOrders" => #{
                              "Fields" => #{"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"WaveNo" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoExecs" => #{
                              "Fields" => #{"LastShares" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastCapacity" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocShares" =>#{"Required" => "Y", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"BrokerOfCredit" =>#{"Required" => "N", "Sequence" => undefined}
,"NotifyBrokerOfCredit" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocHandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocNetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstMode" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMiscFees" => #{
                              "Fields" => #{"MiscFeeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"J" => #{"Category"=>"app"
           ,"Name" => "Allocation"}

,"ListCancelRequest" => #{
                              "Category" => "app"
                              ,"Type" => "K"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"K" => #{"Category"=>"app"
           ,"Name" => "ListCancelRequest"}

,"ListExecute" => #{
                              "Category" => "app"
                              ,"Type" => "L"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClientBidID" =>#{"Required" => "N", "Sequence" => undefined}
,"BidID" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"L" => #{"Category"=>"app"
           ,"Name" => "ListExecute"}

,"ListStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "M"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"M" => #{"Category"=>"app"
           ,"Name" => "ListStatusRequest"}

,"ListStatus" => #{
                              "Category" => "app"
                              ,"Type" => "N"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListStatusType" =>#{"Required" => "Y", "Sequence" => undefined}
,"NoRpts" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListOrderStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"RptSeq" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListStatusText" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedListStatusTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedListStatusText" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoOrders" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"CumQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"LeavesQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"CxlQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoOrders" => #{
                              "Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"CumQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"LeavesQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"CxlQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"N" => #{"Category"=>"app"
           ,"Name" => "ListStatus"}

,"AllocationInstructionAck" => #{
                              "Category" => "app"
                              ,"Type" => "P"
                              ,"Fields" => #{"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocRejCode" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"P" => #{"Category"=>"app"
           ,"Name" => "AllocationInstructionAck"}

,"DontKnowTrade" => #{
                              "Category" => "app"
                              ,"Type" => "Q"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "Y", "Sequence" => undefined}
,"DKReason" =>#{"Required" => "Y", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LastShares" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"Q" => #{"Category"=>"app"
           ,"Name" => "DontKnowTrade"}

,"QuoteRequest" => #{
                              "Category" => "app"
                              ,"Type" => "R"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"R" => #{"Category"=>"app"
           ,"Name" => "QuoteRequest"}

,"Quote" => #{
                              "Category" => "app"
                              ,"Type" => "S"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteResponseLevel" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"S" => #{"Category"=>"app"
           ,"Name" => "Quote"}

,"SettlementInstructions" => #{
                              "Category" => "app"
                              ,"Type" => "T"
                              ,"Fields" => #{"SettlInstID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlInstTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlInstRefID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlInstMode" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlInstSource" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlLocation" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClientID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"StandInstDbType" =>#{"Required" => "N", "Sequence" => undefined}
,"StandInstDbName" =>#{"Required" => "N", "Sequence" => undefined}
,"StandInstDbID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDeliveryType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDepositoryCode" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlBrkrCode" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstCode" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySettlAgentName" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySettlAgentCode" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySettlAgentAcctNum" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySettlAgentAcctName" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySettlAgentContactName" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySettlAgentContactPhone" =>#{"Required" => "N", "Sequence" => undefined}
,"CashSettlAgentName" =>#{"Required" => "N", "Sequence" => undefined}
,"CashSettlAgentCode" =>#{"Required" => "N", "Sequence" => undefined}
,"CashSettlAgentAcctNum" =>#{"Required" => "N", "Sequence" => undefined}
,"CashSettlAgentAcctName" =>#{"Required" => "N", "Sequence" => undefined}
,"CashSettlAgentContactName" =>#{"Required" => "N", "Sequence" => undefined}
,"CashSettlAgentContactPhone" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"T" => #{"Category"=>"app"
           ,"Name" => "SettlementInstructions"}

,"MarketDataRequest" => #{
                              "Category" => "app"
                              ,"Type" => "V"
                              ,"Fields" => #{"MDReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MarketDepth" =>#{"Required" => "Y", "Sequence" => undefined}
,"MDUpdateType" =>#{"Required" => "N", "Sequence" => undefined}
,"AggregatedBook" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMDEntryTypes" => #{
                              "Fields" => #{"MDEntryType" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoRelatedSym" => #{
                              "Fields" => #{"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"V" => #{"Category"=>"app"
           ,"Name" => "MarketDataRequest"}

,"MarketDataSnapshotFullRefresh" => #{
                              "Category" => "app"
                              ,"Type" => "W"
                              ,"Fields" => #{"MDReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancialStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalVolumeTraded" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MDEntryPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySize" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TickDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"MDMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryOriginator" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenCloseSettleFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"SellerDays" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryBuyer" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySeller" =>#{"Required" => "N", "Sequence" => undefined}
,"NumberOfOrders" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPositionNo" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMDEntries" => #{
                              "Fields" => #{"MDEntryType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MDEntryPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySize" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TickDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"MDMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryOriginator" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenCloseSettleFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"SellerDays" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryBuyer" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySeller" =>#{"Required" => "N", "Sequence" => undefined}
,"NumberOfOrders" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPositionNo" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"W" => #{"Category"=>"app"
           ,"Name" => "MarketDataSnapshotFullRefresh"}

,"MarketDataIncrementalRefresh" => #{
                              "Category" => "app"
                              ,"Type" => "X"
                              ,"Fields" => #{"MDReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDUpdateAction" =>#{"Required" => "Y", "Sequence" => undefined}
,"DeleteReason" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryType" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "N", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancialStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySize" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TickDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"MDMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryOriginator" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenCloseSettleFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"SellerDays" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryBuyer" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySeller" =>#{"Required" => "N", "Sequence" => undefined}
,"NumberOfOrders" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPositionNo" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalVolumeTraded" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMDEntries" => #{
                              "Fields" => #{"MDUpdateAction" =>#{"Required" => "Y", "Sequence" => undefined}
,"DeleteReason" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryType" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "N", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancialStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySize" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TickDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"MDMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryOriginator" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenCloseSettleFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"SellerDays" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryBuyer" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySeller" =>#{"Required" => "N", "Sequence" => undefined}
,"NumberOfOrders" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPositionNo" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalVolumeTraded" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"X" => #{"Category"=>"app"
           ,"Name" => "MarketDataIncrementalRefresh"}

,"MarketDataRequestReject" => #{
                              "Category" => "app"
                              ,"Type" => "Y"
                              ,"Fields" => #{"MDReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"MDReqRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"Y" => #{"Category"=>"app"
           ,"Name" => "MarketDataRequestReject"}

,"QuoteCancel" => #{
                              "Category" => "app"
                              ,"Type" => "Z"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteCancelType" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteResponseLevel" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSymbol" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoQuoteEntries" => #{
                              "Fields" => #{"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSymbol" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"Z" => #{"Category"=>"app"
           ,"Name" => "QuoteCancel"}

,"QuoteStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "a"
                              ,"Fields" => #{"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"a" => #{"Category"=>"app"
           ,"Name" => "QuoteStatusRequest"}

,"QuoteAcknowledgement" => #{
                              "Category" => "app"
                              ,"Type" => "b"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteAckStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteRejectReason" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteResponseLevel" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteSetID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSymbol" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingPutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingOptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"TotQuoteEntries" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "N", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryRejectReason" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoQuoteSets" => #{
                              "Fields" => #{"QuoteSetID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSymbol" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingPutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingOptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"TotQuoteEntries" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "N", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryRejectReason" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoQuoteEntries" => #{
                              "Fields" => #{"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "N", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryRejectReason" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"b" => #{"Category"=>"app"
           ,"Name" => "QuoteAcknowledgement"}

,"SecurityDefinitionRequest" => #{
                              "Category" => "app"
                              ,"Type" => "c"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "N", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSymbol" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingPutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingOptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"RatioQty" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCurrency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"UnderlyingSymbol" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingPutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingOptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"RatioQty" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCurrency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"c" => #{"Category"=>"app"
           ,"Name" => "SecurityDefinitionRequest"}

,"SecurityDefinition" => #{
                              "Category" => "app"
                              ,"Type" => "d"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityResponseID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityResponseType" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalNumSecurities" =>#{"Required" => "Y", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "N", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSymbol" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingPutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingOptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"RatioQty" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCurrency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"UnderlyingSymbol" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingPutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingOptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"RatioQty" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCurrency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"d" => #{"Category"=>"app"
           ,"Name" => "SecurityDefinition"}

,"SecurityStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "e"
                              ,"Fields" => #{"SecurityStatusReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"e" => #{"Category"=>"app"
           ,"Name" => "SecurityStatusRequest"}

,"SecurityStatus" => #{
                              "Category" => "app"
                              ,"Type" => "f"
                              ,"Fields" => #{"SecurityStatusReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnsolicitedIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityTradingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancialStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"HaltReasonChar" =>#{"Required" => "N", "Sequence" => undefined}
,"InViewOfCommon" =>#{"Required" => "N", "Sequence" => undefined}
,"DueToRelated" =>#{"Required" => "N", "Sequence" => undefined}
,"BuyVolume" =>#{"Required" => "N", "Sequence" => undefined}
,"SellVolume" =>#{"Required" => "N", "Sequence" => undefined}
,"HighPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LowPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Adjustment" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"f" => #{"Category"=>"app"
           ,"Name" => "SecurityStatus"}

,"TradingSessionStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "g"
                              ,"Fields" => #{"TradSesReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesMode" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"g" => #{"Category"=>"app"
           ,"Name" => "TradingSessionStatusRequest"}

,"TradingSessionStatus" => #{
                              "Category" => "app"
                              ,"Type" => "h"
                              ,"Fields" => #{"TradSesReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradSesMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesMode" =>#{"Required" => "N", "Sequence" => undefined}
,"UnsolicitedIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradSesStartTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesOpenTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesPreCloseTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesCloseTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesEndTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalVolumeTraded" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"h" => #{"Category"=>"app"
           ,"Name" => "TradingSessionStatus"}

,"MassQuote" => #{
                              "Category" => "app"
                              ,"Type" => "i"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteResponseLevel" =>#{"Required" => "N", "Sequence" => undefined}
,"DefBidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"DefOfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteSetID" =>#{"Required" => "Y", "Sequence" => undefined}
,"UnderlyingSymbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"UnderlyingSymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingPutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingOptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteSetValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TotQuoteEntries" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "Y", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "N", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoQuoteSets" => #{
                              "Fields" => #{"QuoteSetID" =>#{"Required" => "Y", "Sequence" => undefined}
,"UnderlyingSymbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"UnderlyingSymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingPutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingOptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteSetValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TotQuoteEntries" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "Y", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "N", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoQuoteEntries" => #{
                              "Fields" => #{"QuoteEntryID" =>#{"Required" => "Y", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "N", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"i" => #{"Category"=>"app"
           ,"Name" => "MassQuote"}

,"BusinessMessageReject" => #{
                              "Category" => "app"
                              ,"Type" => "j"
                              ,"Fields" => #{"RefSeqNum" =>#{"Required" => "N", "Sequence" => undefined}
,"RefMsgType" =>#{"Required" => "Y", "Sequence" => undefined}
,"BusinessRejectRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"BusinessRejectReason" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"j" => #{"Category"=>"app"
           ,"Name" => "BusinessMessageReject"}

,"BidRequest" => #{
                              "Category" => "app"
                              ,"Type" => "k"
                              ,"Fields" => #{"BidID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientBidID" =>#{"Required" => "Y", "Sequence" => undefined}
,"BidRequestTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListName" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalNumSecurities" =>#{"Required" => "Y", "Sequence" => undefined}
,"BidType" =>#{"Required" => "Y", "Sequence" => undefined}
,"NumTickets" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"SideValue1" =>#{"Required" => "N", "Sequence" => undefined}
,"SideValue2" =>#{"Required" => "N", "Sequence" => undefined}
,"BidDescriptorType" =>#{"Required" => "N", "Sequence" => undefined}
,"BidDescriptor" =>#{"Required" => "N", "Sequence" => undefined}
,"SideValueInd" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityValue" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityNumSecurities" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityPctLow" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityPctHigh" =>#{"Required" => "N", "Sequence" => undefined}
,"EFPTrackingError" =>#{"Required" => "N", "Sequence" => undefined}
,"FairValue" =>#{"Required" => "N", "Sequence" => undefined}
,"OutsideIndexPct" =>#{"Required" => "N", "Sequence" => undefined}
,"ValueOfFutures" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"NetGrossInd" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityIndType" =>#{"Required" => "N", "Sequence" => undefined}
,"WtAverageLiquidity" =>#{"Required" => "N", "Sequence" => undefined}
,"ExchangeForPhysical" =>#{"Required" => "N", "Sequence" => undefined}
,"OutMainCntryUIndex" =>#{"Required" => "N", "Sequence" => undefined}
,"CrossPercent" =>#{"Required" => "N", "Sequence" => undefined}
,"ProgRptReqs" =>#{"Required" => "N", "Sequence" => undefined}
,"ProgPeriodInterval" =>#{"Required" => "N", "Sequence" => undefined}
,"IncTaxInd" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"NumBidders" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeType" =>#{"Required" => "Y", "Sequence" => undefined}
,"BasisPxType" =>#{"Required" => "Y", "Sequence" => undefined}
,"StrikeTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoBidDescriptors" => #{
                              "Fields" => #{"BidDescriptorType" =>#{"Required" => "N", "Sequence" => undefined}
,"BidDescriptor" =>#{"Required" => "N", "Sequence" => undefined}
,"SideValueInd" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityValue" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityNumSecurities" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityPctLow" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityPctHigh" =>#{"Required" => "N", "Sequence" => undefined}
,"EFPTrackingError" =>#{"Required" => "N", "Sequence" => undefined}
,"FairValue" =>#{"Required" => "N", "Sequence" => undefined}
,"OutsideIndexPct" =>#{"Required" => "N", "Sequence" => undefined}
,"ValueOfFutures" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoBidComponents" => #{
                              "Fields" => #{"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"NetGrossInd" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"k" => #{"Category"=>"app"
           ,"Name" => "BidRequest"}

,"BidResponse" => #{
                              "Category" => "app"
                              ,"Type" => "l"
                              ,"Fields" => #{"BidID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientBidID" =>#{"Required" => "N", "Sequence" => undefined}
,"Commission" =>#{"Required" => "Y", "Sequence" => undefined}
,"CommType" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Country" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"FairValue" =>#{"Required" => "N", "Sequence" => undefined}
,"NetGrossInd" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoBidComponents" => #{
                              "Fields" => #{"Commission" =>#{"Required" => "Y", "Sequence" => undefined}
,"CommType" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Country" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"FairValue" =>#{"Required" => "N", "Sequence" => undefined}
,"NetGrossInd" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"l" => #{"Category"=>"app"
           ,"Name" => "BidResponse"}

,"ListStrikePrice" => #{
                              "Category" => "app"
                              ,"Type" => "m"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TotNoStrikes" =>#{"Required" => "Y", "Sequence" => undefined}
,"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoStrikes" => #{
                              "Fields" => #{"Symbol" =>#{"Required" => "Y", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"IDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDay" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"m" => #{"Category"=>"app"
           ,"Name" => "ListStrikePrice"}
}.


fields() ->
#{
"Account" => #{"TagNum" => "1" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1" => #{"Name"=>"Account" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1"}


,
"AdvId" => #{"TagNum" => "2" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "2" => #{"Name"=>"AdvId" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "2"}


,
"AdvRefID" => #{"TagNum" => "3" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "3" => #{"Name"=>"AdvRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "3"}


,
"AdvSide" => #{"TagNum" => "4" ,"Type" => "CHAR" ,"ValidValues" =>[{"B", "BUY"},{"S", "SELL"},{"T", "TRADE"},{"X", "CROSS"}]}
, "4" => #{"Name"=>"AdvSide" ,"Type"=>"CHAR" ,"ValidValues"=>[{"B", "BUY"},{"S", "SELL"},{"T", "TRADE"},{"X", "CROSS"}], "TagNum" => "4"}


,
"AdvTransType" => #{"TagNum" => "5" ,"Type" => "STRING" ,"ValidValues" =>[{"C", "CANCEL"},{"N", "NEW"},{"R", "REPLACE"}]}
, "5" => #{"Name"=>"AdvTransType" ,"Type"=>"STRING" ,"ValidValues"=>[{"C", "CANCEL"},{"N", "NEW"},{"R", "REPLACE"}], "TagNum" => "5"}


,
"AvgPx" => #{"TagNum" => "6" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "6" => #{"Name"=>"AvgPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "6"}


,
"BeginSeqNo" => #{"TagNum" => "7" ,"Type" => "INT" ,"ValidValues" =>[]}
, "7" => #{"Name"=>"BeginSeqNo" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "7"}


,
"BeginString" => #{"TagNum" => "8" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "8" => #{"Name"=>"BeginString" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "8"}


,
"BodyLength" => #{"TagNum" => "9" ,"Type" => "INT" ,"ValidValues" =>[]}
, "9" => #{"Name"=>"BodyLength" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "9"}


,
"CheckSum" => #{"TagNum" => "10" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "10" => #{"Name"=>"CheckSum" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "10"}


,
"ClOrdID" => #{"TagNum" => "11" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "11" => #{"Name"=>"ClOrdID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "11"}


,
"Commission" => #{"TagNum" => "12" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "12" => #{"Name"=>"Commission" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "12"}


,
"CommType" => #{"TagNum" => "13" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "PER_SHARE"},{"2", "PERCENTAGE"},{"3", "ABSOLUTE"}]}
, "13" => #{"Name"=>"CommType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "PER_SHARE"},{"2", "PERCENTAGE"},{"3", "ABSOLUTE"}], "TagNum" => "13"}


,
"CumQty" => #{"TagNum" => "14" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "14" => #{"Name"=>"CumQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "14"}


,
"Currency" => #{"TagNum" => "15" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "15" => #{"Name"=>"Currency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "15"}


,
"EndSeqNo" => #{"TagNum" => "16" ,"Type" => "INT" ,"ValidValues" =>[]}
, "16" => #{"Name"=>"EndSeqNo" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "16"}


,
"ExecID" => #{"TagNum" => "17" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "17" => #{"Name"=>"ExecID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "17"}


,
"ExecInst" => #{"TagNum" => "18" ,"Type" => "MULTIPLEVALUESTRING" ,"ValidValues" =>[{"0", "STAY_ON_OFFERSIDE"},{"1", "NOT_HELD"},{"2", "WORK"},{"3", "GO_ALONG"},{"4", "OVER_THE_DAY"},{"5", "HELD"},{"6", "PARTICIPATE_DONT_INITIATE"},{"7", "STRICT_SCALE"},{"8", "TRY_TO_SCALE"},{"9", "STAY_ON_BIDSIDE"},{"A", "NO_CROSS"},{"B", "OK_TO_CROSS"},{"C", "CALL_FIRST"},{"D", "PERCENT_OF_VOLUME"},{"E", "DO_NOT_INCREASE"},{"F", "DO_NOT_REDUCE"},{"G", "ALL_OR_NONE"},{"I", "INSTITUTIONS_ONLY"},{"L", "LAST_PEG"},{"M", "MID_PRICE_PEG"},{"N", "NON_NEGOTIABLE"},{"O", "OPENING_PEG"},{"P", "MARKET_PEG"},{"R", "PRIMARY_PEG"},{"S", "SUSPEND"},{"T", "FIXED_PEG_TO_LOCAL_BEST_BID_OR_OFFER_AT_TIME_OF_ORDER"},{"U", "CUSTOMER_DISPLAY_INSTRUCTION"},{"V", "NETTING"},{"W", "PEG_TO_VWAP"}]}
, "18" => #{"Name"=>"ExecInst" ,"Type"=>"MULTIPLEVALUESTRING" ,"ValidValues"=>[{"0", "STAY_ON_OFFERSIDE"},{"1", "NOT_HELD"},{"2", "WORK"},{"3", "GO_ALONG"},{"4", "OVER_THE_DAY"},{"5", "HELD"},{"6", "PARTICIPATE_DONT_INITIATE"},{"7", "STRICT_SCALE"},{"8", "TRY_TO_SCALE"},{"9", "STAY_ON_BIDSIDE"},{"A", "NO_CROSS"},{"B", "OK_TO_CROSS"},{"C", "CALL_FIRST"},{"D", "PERCENT_OF_VOLUME"},{"E", "DO_NOT_INCREASE"},{"F", "DO_NOT_REDUCE"},{"G", "ALL_OR_NONE"},{"I", "INSTITUTIONS_ONLY"},{"L", "LAST_PEG"},{"M", "MID_PRICE_PEG"},{"N", "NON_NEGOTIABLE"},{"O", "OPENING_PEG"},{"P", "MARKET_PEG"},{"R", "PRIMARY_PEG"},{"S", "SUSPEND"},{"T", "FIXED_PEG_TO_LOCAL_BEST_BID_OR_OFFER_AT_TIME_OF_ORDER"},{"U", "CUSTOMER_DISPLAY_INSTRUCTION"},{"V", "NETTING"},{"W", "PEG_TO_VWAP"}], "TagNum" => "18"}


,
"ExecRefID" => #{"TagNum" => "19" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "19" => #{"Name"=>"ExecRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "19"}


,
"ExecTransType" => #{"TagNum" => "20" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "CANCEL"},{"2", "CORRECT"},{"3", "STATUS"}]}
, "20" => #{"Name"=>"ExecTransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "CANCEL"},{"2", "CORRECT"},{"3", "STATUS"}], "TagNum" => "20"}


,
"HandlInst" => #{"TagNum" => "21" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "AUTOMATED_EXECUTION_ORDER_PRIVATE_NO_BROKER_INTERVENTION"},{"2", "AUTOMATED_EXECUTION_ORDER_PUBLIC_BROKER_INTERVENTION_OK"},{"3", "MANUAL_ORDER_BEST_EXECUTION"}]}
, "21" => #{"Name"=>"HandlInst" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "AUTOMATED_EXECUTION_ORDER_PRIVATE_NO_BROKER_INTERVENTION"},{"2", "AUTOMATED_EXECUTION_ORDER_PUBLIC_BROKER_INTERVENTION_OK"},{"3", "MANUAL_ORDER_BEST_EXECUTION"}], "TagNum" => "21"}


,
"IDSource" => #{"TagNum" => "22" ,"Type" => "STRING" ,"ValidValues" =>[{"1", "CUSIP"},{"2", "SEDOL"},{"3", "QUIK"},{"4", "ISIN_NUMBER"},{"5", "RIC_CODE"},{"6", "ISO_CURRENCY_CODE"},{"7", "ISO_COUNTRY_CODE"},{"8", "EXCHANGE_SYMBOL"},{"9", "CONSOLIDATED_TAPE_ASSOCIATION"}]}
, "22" => #{"Name"=>"IDSource" ,"Type"=>"STRING" ,"ValidValues"=>[{"1", "CUSIP"},{"2", "SEDOL"},{"3", "QUIK"},{"4", "ISIN_NUMBER"},{"5", "RIC_CODE"},{"6", "ISO_CURRENCY_CODE"},{"7", "ISO_COUNTRY_CODE"},{"8", "EXCHANGE_SYMBOL"},{"9", "CONSOLIDATED_TAPE_ASSOCIATION"}], "TagNum" => "22"}


,
"IOIid" => #{"TagNum" => "23" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "23" => #{"Name"=>"IOIid" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "23"}


,
"IOIOthSvc" => #{"TagNum" => "24" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "24" => #{"Name"=>"IOIOthSvc" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "24"}


,
"IOIQltyInd" => #{"TagNum" => "25" ,"Type" => "CHAR" ,"ValidValues" =>[{"H", "HIGH"},{"L", "LOW"},{"M", "MEDIUM"}]}
, "25" => #{"Name"=>"IOIQltyInd" ,"Type"=>"CHAR" ,"ValidValues"=>[{"H", "HIGH"},{"L", "LOW"},{"M", "MEDIUM"}], "TagNum" => "25"}


,
"IOIRefID" => #{"TagNum" => "26" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "26" => #{"Name"=>"IOIRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "26"}


,
"IOIShares" => #{"TagNum" => "27" ,"Type" => "STRING" ,"ValidValues" =>[{"L", "LARGE"},{"M", "MEDIUM"},{"S", "SMALL"}]}
, "27" => #{"Name"=>"IOIShares" ,"Type"=>"STRING" ,"ValidValues"=>[{"L", "LARGE"},{"M", "MEDIUM"},{"S", "SMALL"}], "TagNum" => "27"}


,
"IOITransType" => #{"TagNum" => "28" ,"Type" => "CHAR" ,"ValidValues" =>[{"C", "CANCEL"},{"N", "NEW"},{"R", "REPLACE"}]}
, "28" => #{"Name"=>"IOITransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"C", "CANCEL"},{"N", "NEW"},{"R", "REPLACE"}], "TagNum" => "28"}


,
"LastCapacity" => #{"TagNum" => "29" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "AGENT"},{"2", "CROSS_AS_AGENT"},{"3", "CROSS_AS_PRINCIPAL"},{"4", "PRINCIPAL"}]}
, "29" => #{"Name"=>"LastCapacity" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "AGENT"},{"2", "CROSS_AS_AGENT"},{"3", "CROSS_AS_PRINCIPAL"},{"4", "PRINCIPAL"}], "TagNum" => "29"}


,
"LastMkt" => #{"TagNum" => "30" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "30" => #{"Name"=>"LastMkt" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "30"}


,
"LastPx" => #{"TagNum" => "31" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "31" => #{"Name"=>"LastPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "31"}


,
"LastShares" => #{"TagNum" => "32" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "32" => #{"Name"=>"LastShares" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "32"}


,
"LinesOfText" => #{"TagNum" => "33" ,"Type" => "INT" ,"ValidValues" =>[]}
, "33" => #{"Name"=>"LinesOfText" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "33"}


,
"MsgSeqNum" => #{"TagNum" => "34" ,"Type" => "INT" ,"ValidValues" =>[]}
, "34" => #{"Name"=>"MsgSeqNum" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "34"}


,
"MsgType" => #{"TagNum" => "35" ,"Type" => "STRING" ,"ValidValues" =>[{"0", "HEARTBEAT"},{"1", "TEST_REQUEST"},{"2", "RESEND_REQUEST"},{"3", "REJECT"},{"4", "SEQUENCE_RESET"},{"5", "LOGOUT"},{"6", "INDICATION_OF_INTEREST"},{"7", "ADVERTISEMENT"},{"8", "EXECUTION_REPORT"},{"9", "ORDER_CANCEL_REJECT"},{"a", "QUOTE_STATUS_REQUEST"},{"A", "LOGON"},{"B", "NEWS"},{"b", "QUOTE_ACKNOWLEDGEMENT"},{"C", "EMAIL"},{"c", "SECURITY_DEFINITION_REQUEST"},{"D", "ORDER_SINGLE"},{"d", "SECURITY_DEFINITION"},{"E", "ORDER_LIST"},{"e", "SECURITY_STATUS_REQUEST"},{"f", "SECURITY_STATUS"},{"F", "ORDER_CANCEL_REQUEST"},{"G", "ORDER_CANCEL_REPLACE_REQUEST"},{"g", "TRADING_SESSION_STATUS_REQUEST"},{"H", "ORDER_STATUS_REQUEST"},{"h", "TRADING_SESSION_STATUS"},{"i", "MASS_QUOTE"},{"j", "BUSINESS_MESSAGE_REJECT"},{"J", "ALLOCATION"},{"K", "LIST_CANCEL_REQUEST"},{"k", "BID_REQUEST"},{"l", "BID_RESPONSE"},{"L", "LIST_EXECUTE"},{"m", "LIST_STRIKE_PRICE"},{"M", "LIST_STATUS_REQUEST"},{"N", "LIST_STATUS"},{"P", "ALLOCATION_ACK"},{"Q", "DONT_KNOW_TRADE"},{"R", "QUOTE_REQUEST"},{"S", "QUOTE"},{"T", "SETTLEMENT_INSTRUCTIONS"},{"V", "MARKET_DATA_REQUEST"},{"W", "MARKET_DATA_SNAPSHOT_FULL_REFRESH"},{"X", "MARKET_DATA_INCREMENTAL_REFRESH"},{"Y", "MARKET_DATA_REQUEST_REJECT"},{"Z", "QUOTE_CANCEL"}]}
, "35" => #{"Name"=>"MsgType" ,"Type"=>"STRING" ,"ValidValues"=>[{"0", "HEARTBEAT"},{"1", "TEST_REQUEST"},{"2", "RESEND_REQUEST"},{"3", "REJECT"},{"4", "SEQUENCE_RESET"},{"5", "LOGOUT"},{"6", "INDICATION_OF_INTEREST"},{"7", "ADVERTISEMENT"},{"8", "EXECUTION_REPORT"},{"9", "ORDER_CANCEL_REJECT"},{"a", "QUOTE_STATUS_REQUEST"},{"A", "LOGON"},{"B", "NEWS"},{"b", "QUOTE_ACKNOWLEDGEMENT"},{"C", "EMAIL"},{"c", "SECURITY_DEFINITION_REQUEST"},{"D", "ORDER_SINGLE"},{"d", "SECURITY_DEFINITION"},{"E", "ORDER_LIST"},{"e", "SECURITY_STATUS_REQUEST"},{"f", "SECURITY_STATUS"},{"F", "ORDER_CANCEL_REQUEST"},{"G", "ORDER_CANCEL_REPLACE_REQUEST"},{"g", "TRADING_SESSION_STATUS_REQUEST"},{"H", "ORDER_STATUS_REQUEST"},{"h", "TRADING_SESSION_STATUS"},{"i", "MASS_QUOTE"},{"j", "BUSINESS_MESSAGE_REJECT"},{"J", "ALLOCATION"},{"K", "LIST_CANCEL_REQUEST"},{"k", "BID_REQUEST"},{"l", "BID_RESPONSE"},{"L", "LIST_EXECUTE"},{"m", "LIST_STRIKE_PRICE"},{"M", "LIST_STATUS_REQUEST"},{"N", "LIST_STATUS"},{"P", "ALLOCATION_ACK"},{"Q", "DONT_KNOW_TRADE"},{"R", "QUOTE_REQUEST"},{"S", "QUOTE"},{"T", "SETTLEMENT_INSTRUCTIONS"},{"V", "MARKET_DATA_REQUEST"},{"W", "MARKET_DATA_SNAPSHOT_FULL_REFRESH"},{"X", "MARKET_DATA_INCREMENTAL_REFRESH"},{"Y", "MARKET_DATA_REQUEST_REJECT"},{"Z", "QUOTE_CANCEL"}], "TagNum" => "35"}


,
"NewSeqNo" => #{"TagNum" => "36" ,"Type" => "INT" ,"ValidValues" =>[]}
, "36" => #{"Name"=>"NewSeqNo" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "36"}


,
"OrderID" => #{"TagNum" => "37" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "37" => #{"Name"=>"OrderID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "37"}


,
"OrderQty" => #{"TagNum" => "38" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "38" => #{"Name"=>"OrderQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "38"}


,
"OrdStatus" => #{"TagNum" => "39" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "PARTIALLY_FILLED"},{"2", "FILLED"},{"3", "DONE_FOR_DAY"},{"4", "CANCELED"},{"5", "REPLACED"},{"6", "PENDING_CANCEL"},{"7", "STOPPED"},{"8", "REJECTED"},{"9", "SUSPENDED"},{"A", "PENDING_NEW"},{"B", "CALCULATED"},{"C", "EXPIRED"},{"D", "ACCEPTED_FOR_BIDDING"},{"E", "PENDING_REPLACE"}]}
, "39" => #{"Name"=>"OrdStatus" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "PARTIALLY_FILLED"},{"2", "FILLED"},{"3", "DONE_FOR_DAY"},{"4", "CANCELED"},{"5", "REPLACED"},{"6", "PENDING_CANCEL"},{"7", "STOPPED"},{"8", "REJECTED"},{"9", "SUSPENDED"},{"A", "PENDING_NEW"},{"B", "CALCULATED"},{"C", "EXPIRED"},{"D", "ACCEPTED_FOR_BIDDING"},{"E", "PENDING_REPLACE"}], "TagNum" => "39"}


,
"OrdType" => #{"TagNum" => "40" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "MARKET"},{"2", "LIMIT"},{"3", "STOP"},{"4", "STOP_LIMIT"},{"5", "MARKET_ON_CLOSE"},{"6", "WITH_OR_WITHOUT"},{"7", "LIMIT_OR_BETTER"},{"8", "LIMIT_WITH_OR_WITHOUT"},{"9", "ON_BASIS"},{"A", "ON_CLOSE"},{"B", "LIMIT_ON_CLOSE"},{"C", "FOREX_C"},{"D", "PREVIOUSLY_QUOTED"},{"E", "PREVIOUSLY_INDICATED"},{"F", "FOREX_F"},{"G", "FOREX_G"},{"H", "FOREX_H"},{"I", "FUNARI"},{"P", "PEGGED"}]}
, "40" => #{"Name"=>"OrdType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "MARKET"},{"2", "LIMIT"},{"3", "STOP"},{"4", "STOP_LIMIT"},{"5", "MARKET_ON_CLOSE"},{"6", "WITH_OR_WITHOUT"},{"7", "LIMIT_OR_BETTER"},{"8", "LIMIT_WITH_OR_WITHOUT"},{"9", "ON_BASIS"},{"A", "ON_CLOSE"},{"B", "LIMIT_ON_CLOSE"},{"C", "FOREX_C"},{"D", "PREVIOUSLY_QUOTED"},{"E", "PREVIOUSLY_INDICATED"},{"F", "FOREX_F"},{"G", "FOREX_G"},{"H", "FOREX_H"},{"I", "FUNARI"},{"P", "PEGGED"}], "TagNum" => "40"}


,
"OrigClOrdID" => #{"TagNum" => "41" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "41" => #{"Name"=>"OrigClOrdID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "41"}


,
"OrigTime" => #{"TagNum" => "42" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "42" => #{"Name"=>"OrigTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "42"}


,
"PossDupFlag" => #{"TagNum" => "43" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "43" => #{"Name"=>"PossDupFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "43"}


,
"Price" => #{"TagNum" => "44" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "44" => #{"Name"=>"Price" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "44"}


,
"RefSeqNum" => #{"TagNum" => "45" ,"Type" => "INT" ,"ValidValues" =>[]}
, "45" => #{"Name"=>"RefSeqNum" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "45"}


,
"RelatdSym" => #{"TagNum" => "46" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "46" => #{"Name"=>"RelatdSym" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "46"}


,
"Rule80A" => #{"TagNum" => "47" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "AGENCY_SINGLE_ORDER"},{"B", "SHORT_EXEMPT_TRANSACTION_B"},{"C", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_MEMBER_FIRM_ORG"},{"D", "PROGRAM_ORDER_INDEX_ARB_FOR_MEMBER_FIRM_ORG"},{"E", "REGISTERED_EQUITY_MARKET_MAKER_TRADES"},{"F", "SHORT_EXEMPT_TRANSACTION_F"},{"H", "SHORT_EXEMPT_TRANSACTION_H"},{"I", "INDIVIDUAL_INVESTOR_SINGLE_ORDER"},{"J", "PROGRAM_ORDER_INDEX_ARB_FOR_INDIVIDUAL_CUSTOMER"},{"K", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_INDIVIDUAL_CUSTOMER"},{"L", "SHORT_EXEMPT_TRANSACTION_FOR_MEMBER_COMPETING_MARKET_MAKER_AFFILIATED_WITH_THE_FIRM_CLEARING_THE_TRADE"},{"M", "PROGRAM_ORDER_INDEX_ARB_FOR_OTHER_MEMBER"},{"N", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_OTHER_MEMBER"},{"O", "COMPETING_DEALER_TRADES_O"},{"P", "PRINCIPAL"},{"R", "COMPETING_DEALER_TRADES_R"},{"S", "SPECIALIST_TRADES"},{"T", "COMPETING_DEALER_TRADES_T"},{"U", "PROGRAM_ORDER_INDEX_ARB_FOR_OTHER_AGENCY"},{"W", "ALL_OTHER_ORDERS_AS_AGENT_FOR_OTHER_MEMBER"},{"X", "SHORT_EXEMPT_TRANSACTION_FOR_MEMBER_COMPETING_MARKET_MAKER_NOT_AFFILIATED_WITH_THE_FIRM_CLEARING_THE_TRADE"},{"Y", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_OTHER_AGENCY"},{"Z", "SHORT_EXEMPT_TRANSACTION_FOR_NON_MEMBER_COMPETING_MARKET_MAKER"}]}
, "47" => #{"Name"=>"Rule80A" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "AGENCY_SINGLE_ORDER"},{"B", "SHORT_EXEMPT_TRANSACTION_B"},{"C", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_MEMBER_FIRM_ORG"},{"D", "PROGRAM_ORDER_INDEX_ARB_FOR_MEMBER_FIRM_ORG"},{"E", "REGISTERED_EQUITY_MARKET_MAKER_TRADES"},{"F", "SHORT_EXEMPT_TRANSACTION_F"},{"H", "SHORT_EXEMPT_TRANSACTION_H"},{"I", "INDIVIDUAL_INVESTOR_SINGLE_ORDER"},{"J", "PROGRAM_ORDER_INDEX_ARB_FOR_INDIVIDUAL_CUSTOMER"},{"K", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_INDIVIDUAL_CUSTOMER"},{"L", "SHORT_EXEMPT_TRANSACTION_FOR_MEMBER_COMPETING_MARKET_MAKER_AFFILIATED_WITH_THE_FIRM_CLEARING_THE_TRADE"},{"M", "PROGRAM_ORDER_INDEX_ARB_FOR_OTHER_MEMBER"},{"N", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_OTHER_MEMBER"},{"O", "COMPETING_DEALER_TRADES_O"},{"P", "PRINCIPAL"},{"R", "COMPETING_DEALER_TRADES_R"},{"S", "SPECIALIST_TRADES"},{"T", "COMPETING_DEALER_TRADES_T"},{"U", "PROGRAM_ORDER_INDEX_ARB_FOR_OTHER_AGENCY"},{"W", "ALL_OTHER_ORDERS_AS_AGENT_FOR_OTHER_MEMBER"},{"X", "SHORT_EXEMPT_TRANSACTION_FOR_MEMBER_COMPETING_MARKET_MAKER_NOT_AFFILIATED_WITH_THE_FIRM_CLEARING_THE_TRADE"},{"Y", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_OTHER_AGENCY"},{"Z", "SHORT_EXEMPT_TRANSACTION_FOR_NON_MEMBER_COMPETING_MARKET_MAKER"}], "TagNum" => "47"}


,
"SecurityID" => #{"TagNum" => "48" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "48" => #{"Name"=>"SecurityID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "48"}


,
"SenderCompID" => #{"TagNum" => "49" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "49" => #{"Name"=>"SenderCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "49"}


,
"SenderSubID" => #{"TagNum" => "50" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "50" => #{"Name"=>"SenderSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "50"}


,
"SendingDate" => #{"TagNum" => "51" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "51" => #{"Name"=>"SendingDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "51"}


,
"SendingTime" => #{"TagNum" => "52" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "52" => #{"Name"=>"SendingTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "52"}


,
"Shares" => #{"TagNum" => "53" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "53" => #{"Name"=>"Shares" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "53"}


,
"Side" => #{"TagNum" => "54" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "BUY"},{"2", "SELL"},{"3", "BUY_MINUS"},{"4", "SELL_PLUS"},{"5", "SELL_SHORT"},{"6", "SELL_SHORT_EXEMPT"},{"7", "UNDISCLOSED"},{"8", "CROSS"},{"9", "CROSS_SHORT"}]}
, "54" => #{"Name"=>"Side" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "BUY"},{"2", "SELL"},{"3", "BUY_MINUS"},{"4", "SELL_PLUS"},{"5", "SELL_SHORT"},{"6", "SELL_SHORT_EXEMPT"},{"7", "UNDISCLOSED"},{"8", "CROSS"},{"9", "CROSS_SHORT"}], "TagNum" => "54"}


,
"Symbol" => #{"TagNum" => "55" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "55" => #{"Name"=>"Symbol" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "55"}


,
"TargetCompID" => #{"TagNum" => "56" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "56" => #{"Name"=>"TargetCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "56"}


,
"TargetSubID" => #{"TagNum" => "57" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "57" => #{"Name"=>"TargetSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "57"}


,
"Text" => #{"TagNum" => "58" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "58" => #{"Name"=>"Text" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "58"}


,
"TimeInForce" => #{"TagNum" => "59" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "DAY"},{"1", "GOOD_TILL_CANCEL"},{"2", "AT_THE_OPENING"},{"3", "IMMEDIATE_OR_CANCEL"},{"4", "FILL_OR_KILL"},{"5", "GOOD_TILL_CROSSING"},{"6", "GOOD_TILL_DATE"}]}
, "59" => #{"Name"=>"TimeInForce" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "DAY"},{"1", "GOOD_TILL_CANCEL"},{"2", "AT_THE_OPENING"},{"3", "IMMEDIATE_OR_CANCEL"},{"4", "FILL_OR_KILL"},{"5", "GOOD_TILL_CROSSING"},{"6", "GOOD_TILL_DATE"}], "TagNum" => "59"}


,
"TransactTime" => #{"TagNum" => "60" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "60" => #{"Name"=>"TransactTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "60"}


,
"Urgency" => #{"TagNum" => "61" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NORMAL"},{"1", "FLASH"},{"2", "BACKGROUND"}]}
, "61" => #{"Name"=>"Urgency" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NORMAL"},{"1", "FLASH"},{"2", "BACKGROUND"}], "TagNum" => "61"}


,
"ValidUntilTime" => #{"TagNum" => "62" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "62" => #{"Name"=>"ValidUntilTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "62"}


,
"SettlmntTyp" => #{"TagNum" => "63" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "REGULAR"},{"1", "CASH"},{"2", "NEXT_DAY"},{"3", "T_PLUS_2"},{"4", "T_PLUS_3"},{"5", "T_PLUS_4"},{"6", "FUTURE"},{"7", "WHEN_ISSUED"},{"8", "SELLERS_OPTION"},{"9", "T_PLUS_5"}]}
, "63" => #{"Name"=>"SettlmntTyp" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "REGULAR"},{"1", "CASH"},{"2", "NEXT_DAY"},{"3", "T_PLUS_2"},{"4", "T_PLUS_3"},{"5", "T_PLUS_4"},{"6", "FUTURE"},{"7", "WHEN_ISSUED"},{"8", "SELLERS_OPTION"},{"9", "T_PLUS_5"}], "TagNum" => "63"}


,
"FutSettDate" => #{"TagNum" => "64" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "64" => #{"Name"=>"FutSettDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "64"}


,
"SymbolSfx" => #{"TagNum" => "65" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "65" => #{"Name"=>"SymbolSfx" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "65"}


,
"ListID" => #{"TagNum" => "66" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "66" => #{"Name"=>"ListID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "66"}


,
"ListSeqNo" => #{"TagNum" => "67" ,"Type" => "INT" ,"ValidValues" =>[]}
, "67" => #{"Name"=>"ListSeqNo" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "67"}


,
"TotNoOrders" => #{"TagNum" => "68" ,"Type" => "INT" ,"ValidValues" =>[]}
, "68" => #{"Name"=>"TotNoOrders" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "68"}


,
"ListExecInst" => #{"TagNum" => "69" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "69" => #{"Name"=>"ListExecInst" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "69"}


,
"AllocID" => #{"TagNum" => "70" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "70" => #{"Name"=>"AllocID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "70"}


,
"AllocTransType" => #{"TagNum" => "71" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "REPLACE"},{"2", "CANCEL"},{"3", "PRELIMINARY"},{"4", "CALCULATED"},{"5", "CALCULATED_WITHOUT_PRELIMINARY"}]}
, "71" => #{"Name"=>"AllocTransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "REPLACE"},{"2", "CANCEL"},{"3", "PRELIMINARY"},{"4", "CALCULATED"},{"5", "CALCULATED_WITHOUT_PRELIMINARY"}], "TagNum" => "71"}


,
"RefAllocID" => #{"TagNum" => "72" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "72" => #{"Name"=>"RefAllocID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "72"}


,
"NoOrders" => #{"TagNum" => "73" ,"Type" => "INT" ,"ValidValues" =>[]}
, "73" => #{"Name"=>"NoOrders" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "73"}


,
"AvgPrxPrecision" => #{"TagNum" => "74" ,"Type" => "INT" ,"ValidValues" =>[]}
, "74" => #{"Name"=>"AvgPrxPrecision" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "74"}


,
"TradeDate" => #{"TagNum" => "75" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "75" => #{"Name"=>"TradeDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "75"}


,
"ExecBroker" => #{"TagNum" => "76" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "76" => #{"Name"=>"ExecBroker" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "76"}


,
"OpenClose" => #{"TagNum" => "77" ,"Type" => "CHAR" ,"ValidValues" =>[{"C", "CLOSE"},{"O", "OPEN"}]}
, "77" => #{"Name"=>"OpenClose" ,"Type"=>"CHAR" ,"ValidValues"=>[{"C", "CLOSE"},{"O", "OPEN"}], "TagNum" => "77"}


,
"NoAllocs" => #{"TagNum" => "78" ,"Type" => "INT" ,"ValidValues" =>[]}
, "78" => #{"Name"=>"NoAllocs" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "78"}


,
"AllocAccount" => #{"TagNum" => "79" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "79" => #{"Name"=>"AllocAccount" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "79"}


,
"AllocShares" => #{"TagNum" => "80" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "80" => #{"Name"=>"AllocShares" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "80"}


,
"ProcessCode" => #{"TagNum" => "81" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "REGULAR"},{"1", "SOFT_DOLLAR"},{"2", "STEP_IN"},{"3", "STEP_OUT"},{"4", "SOFT_DOLLAR_STEP_IN"},{"5", "SOFT_DOLLAR_STEP_OUT"},{"6", "PLAN_SPONSOR"}]}
, "81" => #{"Name"=>"ProcessCode" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "REGULAR"},{"1", "SOFT_DOLLAR"},{"2", "STEP_IN"},{"3", "STEP_OUT"},{"4", "SOFT_DOLLAR_STEP_IN"},{"5", "SOFT_DOLLAR_STEP_OUT"},{"6", "PLAN_SPONSOR"}], "TagNum" => "81"}


,
"NoRpts" => #{"TagNum" => "82" ,"Type" => "INT" ,"ValidValues" =>[]}
, "82" => #{"Name"=>"NoRpts" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "82"}


,
"RptSeq" => #{"TagNum" => "83" ,"Type" => "INT" ,"ValidValues" =>[]}
, "83" => #{"Name"=>"RptSeq" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "83"}


,
"CxlQty" => #{"TagNum" => "84" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "84" => #{"Name"=>"CxlQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "84"}


,
"NoDlvyInst" => #{"TagNum" => "85" ,"Type" => "INT" ,"ValidValues" =>[]}
, "85" => #{"Name"=>"NoDlvyInst" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "85"}


,
"DlvyInst" => #{"TagNum" => "86" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "86" => #{"Name"=>"DlvyInst" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "86"}


,
"AllocStatus" => #{"TagNum" => "87" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ACCEPTED"},{"1", "REJECTED"},{"2", "PARTIAL_ACCEPT"},{"3", "RECEIVED"}]}
, "87" => #{"Name"=>"AllocStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ACCEPTED"},{"1", "REJECTED"},{"2", "PARTIAL_ACCEPT"},{"3", "RECEIVED"}], "TagNum" => "87"}


,
"AllocRejCode" => #{"TagNum" => "88" ,"Type" => "INT" ,"ValidValues" =>[{"0", "UNKNOWN_ACCOUNT"},{"1", "INCORRECT_QUANTITY"},{"2", "INCORRECT_AVERAGE_PRICE"},{"3", "UNKNOWN_EXECUTING_BROKER_MNEMONIC"},{"4", "COMMISSION_DIFFERENCE"},{"5", "UNKNOWN_ORDERID"},{"6", "UNKNOWN_LISTID"},{"7", "OTHER"}]}
, "88" => #{"Name"=>"AllocRejCode" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "UNKNOWN_ACCOUNT"},{"1", "INCORRECT_QUANTITY"},{"2", "INCORRECT_AVERAGE_PRICE"},{"3", "UNKNOWN_EXECUTING_BROKER_MNEMONIC"},{"4", "COMMISSION_DIFFERENCE"},{"5", "UNKNOWN_ORDERID"},{"6", "UNKNOWN_LISTID"},{"7", "OTHER"}], "TagNum" => "88"}


,
"Signature" => #{"TagNum" => "89" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "89" => #{"Name"=>"Signature" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "89"}


,
"SecureDataLen" => #{"TagNum" => "90" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "90" => #{"Name"=>"SecureDataLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "90"}


,
"SecureData" => #{"TagNum" => "91" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "91" => #{"Name"=>"SecureData" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "91"}


,
"BrokerOfCredit" => #{"TagNum" => "92" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "92" => #{"Name"=>"BrokerOfCredit" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "92"}


,
"SignatureLength" => #{"TagNum" => "93" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "93" => #{"Name"=>"SignatureLength" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "93"}


,
"EmailType" => #{"TagNum" => "94" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "REPLY"},{"2", "ADMIN_REPLY"}]}
, "94" => #{"Name"=>"EmailType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "REPLY"},{"2", "ADMIN_REPLY"}], "TagNum" => "94"}


,
"RawDataLength" => #{"TagNum" => "95" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "95" => #{"Name"=>"RawDataLength" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "95"}


,
"RawData" => #{"TagNum" => "96" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "96" => #{"Name"=>"RawData" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "96"}


,
"PossResend" => #{"TagNum" => "97" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "97" => #{"Name"=>"PossResend" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "97"}


,
"EncryptMethod" => #{"TagNum" => "98" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NONE"},{"1", "PKCS"},{"2", "DES"},{"3", "PKCS_DES"},{"4", "PGP_DES"},{"5", "PGP_DES_MD5"},{"6", "PEM_DES_MD5"}]}
, "98" => #{"Name"=>"EncryptMethod" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NONE"},{"1", "PKCS"},{"2", "DES"},{"3", "PKCS_DES"},{"4", "PGP_DES"},{"5", "PGP_DES_MD5"},{"6", "PEM_DES_MD5"}], "TagNum" => "98"}


,
"StopPx" => #{"TagNum" => "99" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "99" => #{"Name"=>"StopPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "99"}


,
"ExDestination" => #{"TagNum" => "100" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "100" => #{"Name"=>"ExDestination" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "100"}


,
"CxlRejReason" => #{"TagNum" => "102" ,"Type" => "INT" ,"ValidValues" =>[{"0", "TOO_LATE_TO_CANCEL"},{"1", "UNKNOWN_ORDER"},{"2", "BROKER_OPTION"},{"3", "ORDER_ALREADY_IN_PENDING_CANCEL_OR_PENDING_REPLACE_STATUS"}]}
, "102" => #{"Name"=>"CxlRejReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "TOO_LATE_TO_CANCEL"},{"1", "UNKNOWN_ORDER"},{"2", "BROKER_OPTION"},{"3", "ORDER_ALREADY_IN_PENDING_CANCEL_OR_PENDING_REPLACE_STATUS"}], "TagNum" => "102"}


,
"OrdRejReason" => #{"TagNum" => "103" ,"Type" => "INT" ,"ValidValues" =>[{"0", "BROKER_OPTION"},{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE_CLOSED"},{"3", "ORDER_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "UNKNOWN_ORDER"},{"6", "DUPLICATE_ORDER"},{"7", "DUPLICATE_OF_A_VERBALLY_COMMUNICATED_ORDER"},{"8", "STALE_ORDER"}]}
, "103" => #{"Name"=>"OrdRejReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "BROKER_OPTION"},{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE_CLOSED"},{"3", "ORDER_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "UNKNOWN_ORDER"},{"6", "DUPLICATE_ORDER"},{"7", "DUPLICATE_OF_A_VERBALLY_COMMUNICATED_ORDER"},{"8", "STALE_ORDER"}], "TagNum" => "103"}


,
"IOIQualifier" => #{"TagNum" => "104" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "ALL_OR_NONE"},{"C", "AT_THE_CLOSE"},{"I", "IN_TOUCH_WITH"},{"L", "LIMIT"},{"M", "MORE_BEHIND"},{"O", "AT_THE_OPEN"},{"P", "TAKING_A_POSITION"},{"Q", "AT_THE_MARKET"},{"R", "READY_TO_TRADE"},{"S", "PORTFOLIO_SHOW_N"},{"T", "THROUGH_THE_DAY"},{"V", "VERSUS"},{"W", "INDICATION"},{"X", "CROSSING_OPPORTUNITY"},{"Y", "AT_THE_MIDPOINT"},{"Z", "PRE_OPEN"}]}
, "104" => #{"Name"=>"IOIQualifier" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "ALL_OR_NONE"},{"C", "AT_THE_CLOSE"},{"I", "IN_TOUCH_WITH"},{"L", "LIMIT"},{"M", "MORE_BEHIND"},{"O", "AT_THE_OPEN"},{"P", "TAKING_A_POSITION"},{"Q", "AT_THE_MARKET"},{"R", "READY_TO_TRADE"},{"S", "PORTFOLIO_SHOW_N"},{"T", "THROUGH_THE_DAY"},{"V", "VERSUS"},{"W", "INDICATION"},{"X", "CROSSING_OPPORTUNITY"},{"Y", "AT_THE_MIDPOINT"},{"Z", "PRE_OPEN"}], "TagNum" => "104"}


,
"WaveNo" => #{"TagNum" => "105" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "105" => #{"Name"=>"WaveNo" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "105"}


,
"Issuer" => #{"TagNum" => "106" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "106" => #{"Name"=>"Issuer" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "106"}


,
"SecurityDesc" => #{"TagNum" => "107" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "107" => #{"Name"=>"SecurityDesc" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "107"}


,
"HeartBtInt" => #{"TagNum" => "108" ,"Type" => "INT" ,"ValidValues" =>[]}
, "108" => #{"Name"=>"HeartBtInt" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "108"}


,
"ClientID" => #{"TagNum" => "109" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "109" => #{"Name"=>"ClientID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "109"}


,
"MinQty" => #{"TagNum" => "110" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "110" => #{"Name"=>"MinQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "110"}


,
"MaxFloor" => #{"TagNum" => "111" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "111" => #{"Name"=>"MaxFloor" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "111"}


,
"TestReqID" => #{"TagNum" => "112" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "112" => #{"Name"=>"TestReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "112"}


,
"ReportToExch" => #{"TagNum" => "113" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "113" => #{"Name"=>"ReportToExch" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "113"}


,
"LocateReqd" => #{"TagNum" => "114" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "114" => #{"Name"=>"LocateReqd" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "114"}


,
"OnBehalfOfCompID" => #{"TagNum" => "115" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "115" => #{"Name"=>"OnBehalfOfCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "115"}


,
"OnBehalfOfSubID" => #{"TagNum" => "116" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "116" => #{"Name"=>"OnBehalfOfSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "116"}


,
"QuoteID" => #{"TagNum" => "117" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "117" => #{"Name"=>"QuoteID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "117"}


,
"NetMoney" => #{"TagNum" => "118" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "118" => #{"Name"=>"NetMoney" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "118"}


,
"SettlCurrAmt" => #{"TagNum" => "119" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "119" => #{"Name"=>"SettlCurrAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "119"}


,
"SettlCurrency" => #{"TagNum" => "120" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "120" => #{"Name"=>"SettlCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "120"}


,
"ForexReq" => #{"TagNum" => "121" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "121" => #{"Name"=>"ForexReq" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "121"}


,
"OrigSendingTime" => #{"TagNum" => "122" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "122" => #{"Name"=>"OrigSendingTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "122"}


,
"GapFillFlag" => #{"TagNum" => "123" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "123" => #{"Name"=>"GapFillFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "123"}


,
"NoExecs" => #{"TagNum" => "124" ,"Type" => "INT" ,"ValidValues" =>[]}
, "124" => #{"Name"=>"NoExecs" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "124"}


,
"CxlType" => #{"TagNum" => "125" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "125" => #{"Name"=>"CxlType" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "125"}


,
"ExpireTime" => #{"TagNum" => "126" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "126" => #{"Name"=>"ExpireTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "126"}


,
"DKReason" => #{"TagNum" => "127" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "UNKNOWN_SYMBOL"},{"B", "WRONG_SIDE"},{"C", "QUANTITY_EXCEEDS_ORDER"},{"D", "NO_MATCHING_ORDER"},{"E", "PRICE_EXCEEDS_LIMIT"},{"Z", "OTHER"}]}
, "127" => #{"Name"=>"DKReason" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "UNKNOWN_SYMBOL"},{"B", "WRONG_SIDE"},{"C", "QUANTITY_EXCEEDS_ORDER"},{"D", "NO_MATCHING_ORDER"},{"E", "PRICE_EXCEEDS_LIMIT"},{"Z", "OTHER"}], "TagNum" => "127"}


,
"DeliverToCompID" => #{"TagNum" => "128" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "128" => #{"Name"=>"DeliverToCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "128"}


,
"DeliverToSubID" => #{"TagNum" => "129" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "129" => #{"Name"=>"DeliverToSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "129"}


,
"IOINaturalFlag" => #{"TagNum" => "130" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "130" => #{"Name"=>"IOINaturalFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "130"}


,
"QuoteReqID" => #{"TagNum" => "131" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "131" => #{"Name"=>"QuoteReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "131"}


,
"BidPx" => #{"TagNum" => "132" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "132" => #{"Name"=>"BidPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "132"}


,
"OfferPx" => #{"TagNum" => "133" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "133" => #{"Name"=>"OfferPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "133"}


,
"BidSize" => #{"TagNum" => "134" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "134" => #{"Name"=>"BidSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "134"}


,
"OfferSize" => #{"TagNum" => "135" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "135" => #{"Name"=>"OfferSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "135"}


,
"NoMiscFees" => #{"TagNum" => "136" ,"Type" => "INT" ,"ValidValues" =>[]}
, "136" => #{"Name"=>"NoMiscFees" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "136"}


,
"MiscFeeAmt" => #{"TagNum" => "137" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "137" => #{"Name"=>"MiscFeeAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "137"}


,
"MiscFeeCurr" => #{"TagNum" => "138" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "138" => #{"Name"=>"MiscFeeCurr" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "138"}


,
"MiscFeeType" => #{"TagNum" => "139" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "REGULATORY"},{"2", "TAX"},{"3", "LOCAL_COMMISSION"},{"4", "EXCHANGE_FEES"},{"5", "STAMP"},{"6", "LEVY"},{"7", "OTHER"},{"8", "MARKUP"},{"9", "CONSUMPTION_TAX"}]}
, "139" => #{"Name"=>"MiscFeeType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "REGULATORY"},{"2", "TAX"},{"3", "LOCAL_COMMISSION"},{"4", "EXCHANGE_FEES"},{"5", "STAMP"},{"6", "LEVY"},{"7", "OTHER"},{"8", "MARKUP"},{"9", "CONSUMPTION_TAX"}], "TagNum" => "139"}


,
"PrevClosePx" => #{"TagNum" => "140" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "140" => #{"Name"=>"PrevClosePx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "140"}


,
"ResetSeqNumFlag" => #{"TagNum" => "141" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "141" => #{"Name"=>"ResetSeqNumFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "141"}


,
"SenderLocationID" => #{"TagNum" => "142" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "142" => #{"Name"=>"SenderLocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "142"}


,
"TargetLocationID" => #{"TagNum" => "143" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "143" => #{"Name"=>"TargetLocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "143"}


,
"OnBehalfOfLocationID" => #{"TagNum" => "144" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "144" => #{"Name"=>"OnBehalfOfLocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "144"}


,
"DeliverToLocationID" => #{"TagNum" => "145" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "145" => #{"Name"=>"DeliverToLocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "145"}


,
"NoRelatedSym" => #{"TagNum" => "146" ,"Type" => "INT" ,"ValidValues" =>[]}
, "146" => #{"Name"=>"NoRelatedSym" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "146"}


,
"Subject" => #{"TagNum" => "147" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "147" => #{"Name"=>"Subject" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "147"}


,
"Headline" => #{"TagNum" => "148" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "148" => #{"Name"=>"Headline" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "148"}


,
"URLLink" => #{"TagNum" => "149" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "149" => #{"Name"=>"URLLink" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "149"}


,
"ExecType" => #{"TagNum" => "150" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "PARTIAL_FILL"},{"2", "FILL"},{"3", "DONE_FOR_DAY"},{"4", "CANCELED"},{"5", "REPLACE"},{"6", "PENDING_CANCEL"},{"7", "STOPPED"},{"8", "REJECTED"},{"9", "SUSPENDED"},{"A", "PENDING_NEW"},{"B", "CALCULATED"},{"C", "EXPIRED"},{"D", "RESTATED"},{"E", "PENDING_REPLACE"}]}
, "150" => #{"Name"=>"ExecType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "PARTIAL_FILL"},{"2", "FILL"},{"3", "DONE_FOR_DAY"},{"4", "CANCELED"},{"5", "REPLACE"},{"6", "PENDING_CANCEL"},{"7", "STOPPED"},{"8", "REJECTED"},{"9", "SUSPENDED"},{"A", "PENDING_NEW"},{"B", "CALCULATED"},{"C", "EXPIRED"},{"D", "RESTATED"},{"E", "PENDING_REPLACE"}], "TagNum" => "150"}


,
"LeavesQty" => #{"TagNum" => "151" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "151" => #{"Name"=>"LeavesQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "151"}


,
"CashOrderQty" => #{"TagNum" => "152" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "152" => #{"Name"=>"CashOrderQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "152"}


,
"AllocAvgPx" => #{"TagNum" => "153" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "153" => #{"Name"=>"AllocAvgPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "153"}


,
"AllocNetMoney" => #{"TagNum" => "154" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "154" => #{"Name"=>"AllocNetMoney" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "154"}


,
"SettlCurrFxRate" => #{"TagNum" => "155" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "155" => #{"Name"=>"SettlCurrFxRate" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "155"}


,
"SettlCurrFxRateCalc" => #{"TagNum" => "156" ,"Type" => "CHAR" ,"ValidValues" =>[{"M", "MULTIPLY"},{"D", "DIVIDE"}]}
, "156" => #{"Name"=>"SettlCurrFxRateCalc" ,"Type"=>"CHAR" ,"ValidValues"=>[{"M", "MULTIPLY"},{"D", "DIVIDE"}], "TagNum" => "156"}


,
"NumDaysInterest" => #{"TagNum" => "157" ,"Type" => "INT" ,"ValidValues" =>[]}
, "157" => #{"Name"=>"NumDaysInterest" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "157"}


,
"AccruedInterestRate" => #{"TagNum" => "158" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "158" => #{"Name"=>"AccruedInterestRate" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "158"}


,
"AccruedInterestAmt" => #{"TagNum" => "159" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "159" => #{"Name"=>"AccruedInterestAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "159"}


,
"SettlInstMode" => #{"TagNum" => "160" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "DEFAULT"},{"1", "STANDING_INSTRUCTIONS_PROVIDED"},{"2", "SPECIFIC_ALLOCATION_ACCOUNT_OVERRIDING"},{"3", "SPECIFIC_ALLOCATION_ACCOUNT_STANDING"}]}
, "160" => #{"Name"=>"SettlInstMode" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "DEFAULT"},{"1", "STANDING_INSTRUCTIONS_PROVIDED"},{"2", "SPECIFIC_ALLOCATION_ACCOUNT_OVERRIDING"},{"3", "SPECIFIC_ALLOCATION_ACCOUNT_STANDING"}], "TagNum" => "160"}


,
"AllocText" => #{"TagNum" => "161" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "161" => #{"Name"=>"AllocText" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "161"}


,
"SettlInstID" => #{"TagNum" => "162" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "162" => #{"Name"=>"SettlInstID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "162"}


,
"SettlInstTransType" => #{"TagNum" => "163" ,"Type" => "CHAR" ,"ValidValues" =>[{"C", "CANCEL"},{"N", "NEW"},{"R", "REPLACE"}]}
, "163" => #{"Name"=>"SettlInstTransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"C", "CANCEL"},{"N", "NEW"},{"R", "REPLACE"}], "TagNum" => "163"}


,
"EmailThreadID" => #{"TagNum" => "164" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "164" => #{"Name"=>"EmailThreadID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "164"}


,
"SettlInstSource" => #{"TagNum" => "165" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "BROKERS_INSTRUCTIONS"},{"2", "INSTITUTIONS_INSTRUCTIONS"}]}
, "165" => #{"Name"=>"SettlInstSource" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "BROKERS_INSTRUCTIONS"},{"2", "INSTITUTIONS_INSTRUCTIONS"}], "TagNum" => "165"}


,
"SettlLocation" => #{"TagNum" => "166" ,"Type" => "STRING" ,"ValidValues" =>[{"CED", "CEDEL"},{"DTC", "DEPOSITORY_TRUST_COMPANY"},{"EUR", "EUROCLEAR"},{"FED", "FEDERAL_BOOK_ENTRY"},{"ISO Country Code", "LOCAL_MARKET_SETTLE_LOCATION"},{"PNY", "PHYSICAL"},{"PTC", "PARTICIPANT_TRUST_COMPANY"}]}
, "166" => #{"Name"=>"SettlLocation" ,"Type"=>"STRING" ,"ValidValues"=>[{"CED", "CEDEL"},{"DTC", "DEPOSITORY_TRUST_COMPANY"},{"EUR", "EUROCLEAR"},{"FED", "FEDERAL_BOOK_ENTRY"},{"ISO Country Code", "LOCAL_MARKET_SETTLE_LOCATION"},{"PNY", "PHYSICAL"},{"PTC", "PARTICIPANT_TRUST_COMPANY"}], "TagNum" => "166"}


,
"SecurityType" => #{"TagNum" => "167" ,"Type" => "STRING" ,"ValidValues" =>[{"?", "WILDCARD_ENTRY"},{"BA", "BANKERS_ACCEPTANCE"},{"CB", "CONVERTIBLE_BOND"},{"CD", "CERTIFICATE_OF_DEPOSIT"},{"CMO", "COLLATERALIZE_MORTGAGE_OBLIGATION"},{"CORP", "CORPORATE_BOND"},{"CP", "COMMERCIAL_PAPER"},{"CPP", "CORPORATE_PRIVATE_PLACEMENT"},{"CS", "COMMON_STOCK"},{"FHA", "FEDERAL_HOUSING_AUTHORITY"},{"FHL", "FEDERAL_HOME_LOAN"},{"FN", "FEDERAL_NATIONAL_MORTGAGE_ASSOCIATION"},{"FOR", "FOREIGN_EXCHANGE_CONTRACT"},{"FUT", "FUTURE"},{"GN", "GOVERNMENT_NATIONAL_MORTGAGE_ASSOCIATION"},{"GOVT", "TREASURIES_PLUS_AGENCY_DEBENTURE"},{"IET", "MORTGAGE_IOETTE"},{"MF", "MUTUAL_FUND"},{"MIO", "MORTGAGE_INTEREST_ONLY"},{"MPO", "MORTGAGE_PRINCIPAL_ONLY"},{"MPP", "MORTGAGE_PRIVATE_PLACEMENT"},{"MPT", "MISCELLANEOUS_PASS_THRU"},{"MUNI", "MUNICIPAL_BOND"},{"NONE", "NO_ISITC_SECURITY_TYPE"},{"OPT", "OPTION"},{"PS", "PREFERRED_STOCK"},{"RP", "REPURCHASE_AGREEMENT"},{"RVRP", "REVERSE_REPURCHASE_AGREEMENT"},{"SL", "STUDENT_LOAN_MARKETING_ASSOCIATION"},{"TD", "TIME_DEPOSIT"},{"USTB", "US_TREASURY_BILL"},{"WAR", "WARRANT"},{"ZOO", "CATS_TIGERS_LIONS"}]}
, "167" => #{"Name"=>"SecurityType" ,"Type"=>"STRING" ,"ValidValues"=>[{"?", "WILDCARD_ENTRY"},{"BA", "BANKERS_ACCEPTANCE"},{"CB", "CONVERTIBLE_BOND"},{"CD", "CERTIFICATE_OF_DEPOSIT"},{"CMO", "COLLATERALIZE_MORTGAGE_OBLIGATION"},{"CORP", "CORPORATE_BOND"},{"CP", "COMMERCIAL_PAPER"},{"CPP", "CORPORATE_PRIVATE_PLACEMENT"},{"CS", "COMMON_STOCK"},{"FHA", "FEDERAL_HOUSING_AUTHORITY"},{"FHL", "FEDERAL_HOME_LOAN"},{"FN", "FEDERAL_NATIONAL_MORTGAGE_ASSOCIATION"},{"FOR", "FOREIGN_EXCHANGE_CONTRACT"},{"FUT", "FUTURE"},{"GN", "GOVERNMENT_NATIONAL_MORTGAGE_ASSOCIATION"},{"GOVT", "TREASURIES_PLUS_AGENCY_DEBENTURE"},{"IET", "MORTGAGE_IOETTE"},{"MF", "MUTUAL_FUND"},{"MIO", "MORTGAGE_INTEREST_ONLY"},{"MPO", "MORTGAGE_PRINCIPAL_ONLY"},{"MPP", "MORTGAGE_PRIVATE_PLACEMENT"},{"MPT", "MISCELLANEOUS_PASS_THRU"},{"MUNI", "MUNICIPAL_BOND"},{"NONE", "NO_ISITC_SECURITY_TYPE"},{"OPT", "OPTION"},{"PS", "PREFERRED_STOCK"},{"RP", "REPURCHASE_AGREEMENT"},{"RVRP", "REVERSE_REPURCHASE_AGREEMENT"},{"SL", "STUDENT_LOAN_MARKETING_ASSOCIATION"},{"TD", "TIME_DEPOSIT"},{"USTB", "US_TREASURY_BILL"},{"WAR", "WARRANT"},{"ZOO", "CATS_TIGERS_LIONS"}], "TagNum" => "167"}


,
"EffectiveTime" => #{"TagNum" => "168" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "168" => #{"Name"=>"EffectiveTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "168"}


,
"StandInstDbType" => #{"TagNum" => "169" ,"Type" => "INT" ,"ValidValues" =>[{"0", "OTHER"},{"1", "DTC_SID"},{"2", "THOMSON_ALERT"},{"3", "A_GLOBAL_CUSTODIAN"}]}
, "169" => #{"Name"=>"StandInstDbType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "OTHER"},{"1", "DTC_SID"},{"2", "THOMSON_ALERT"},{"3", "A_GLOBAL_CUSTODIAN"}], "TagNum" => "169"}


,
"StandInstDbName" => #{"TagNum" => "170" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "170" => #{"Name"=>"StandInstDbName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "170"}


,
"StandInstDbID" => #{"TagNum" => "171" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "171" => #{"Name"=>"StandInstDbID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "171"}


,
"SettlDeliveryType" => #{"TagNum" => "172" ,"Type" => "INT" ,"ValidValues" =>[]}
, "172" => #{"Name"=>"SettlDeliveryType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "172"}


,
"SettlDepositoryCode" => #{"TagNum" => "173" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "173" => #{"Name"=>"SettlDepositoryCode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "173"}


,
"SettlBrkrCode" => #{"TagNum" => "174" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "174" => #{"Name"=>"SettlBrkrCode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "174"}


,
"SettlInstCode" => #{"TagNum" => "175" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "175" => #{"Name"=>"SettlInstCode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "175"}


,
"SecuritySettlAgentName" => #{"TagNum" => "176" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "176" => #{"Name"=>"SecuritySettlAgentName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "176"}


,
"SecuritySettlAgentCode" => #{"TagNum" => "177" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "177" => #{"Name"=>"SecuritySettlAgentCode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "177"}


,
"SecuritySettlAgentAcctNum" => #{"TagNum" => "178" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "178" => #{"Name"=>"SecuritySettlAgentAcctNum" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "178"}


,
"SecuritySettlAgentAcctName" => #{"TagNum" => "179" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "179" => #{"Name"=>"SecuritySettlAgentAcctName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "179"}


,
"SecuritySettlAgentContactName" => #{"TagNum" => "180" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "180" => #{"Name"=>"SecuritySettlAgentContactName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "180"}


,
"SecuritySettlAgentContactPhone" => #{"TagNum" => "181" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "181" => #{"Name"=>"SecuritySettlAgentContactPhone" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "181"}


,
"CashSettlAgentName" => #{"TagNum" => "182" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "182" => #{"Name"=>"CashSettlAgentName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "182"}


,
"CashSettlAgentCode" => #{"TagNum" => "183" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "183" => #{"Name"=>"CashSettlAgentCode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "183"}


,
"CashSettlAgentAcctNum" => #{"TagNum" => "184" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "184" => #{"Name"=>"CashSettlAgentAcctNum" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "184"}


,
"CashSettlAgentAcctName" => #{"TagNum" => "185" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "185" => #{"Name"=>"CashSettlAgentAcctName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "185"}


,
"CashSettlAgentContactName" => #{"TagNum" => "186" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "186" => #{"Name"=>"CashSettlAgentContactName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "186"}


,
"CashSettlAgentContactPhone" => #{"TagNum" => "187" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "187" => #{"Name"=>"CashSettlAgentContactPhone" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "187"}


,
"BidSpotRate" => #{"TagNum" => "188" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "188" => #{"Name"=>"BidSpotRate" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "188"}


,
"BidForwardPoints" => #{"TagNum" => "189" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "189" => #{"Name"=>"BidForwardPoints" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "189"}


,
"OfferSpotRate" => #{"TagNum" => "190" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "190" => #{"Name"=>"OfferSpotRate" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "190"}


,
"OfferForwardPoints" => #{"TagNum" => "191" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "191" => #{"Name"=>"OfferForwardPoints" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "191"}


,
"OrderQty2" => #{"TagNum" => "192" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "192" => #{"Name"=>"OrderQty2" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "192"}


,
"FutSettDate2" => #{"TagNum" => "193" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "193" => #{"Name"=>"FutSettDate2" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "193"}


,
"LastSpotRate" => #{"TagNum" => "194" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "194" => #{"Name"=>"LastSpotRate" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "194"}


,
"LastForwardPoints" => #{"TagNum" => "195" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "195" => #{"Name"=>"LastForwardPoints" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "195"}


,
"AllocLinkID" => #{"TagNum" => "196" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "196" => #{"Name"=>"AllocLinkID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "196"}


,
"AllocLinkType" => #{"TagNum" => "197" ,"Type" => "INT" ,"ValidValues" =>[{"0", "F_X_NETTING"},{"1", "F_X_SWAP"}]}
, "197" => #{"Name"=>"AllocLinkType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "F_X_NETTING"},{"1", "F_X_SWAP"}], "TagNum" => "197"}


,
"SecondaryOrderID" => #{"TagNum" => "198" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "198" => #{"Name"=>"SecondaryOrderID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "198"}


,
"NoIOIQualifiers" => #{"TagNum" => "199" ,"Type" => "INT" ,"ValidValues" =>[]}
, "199" => #{"Name"=>"NoIOIQualifiers" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "199"}


,
"MaturityMonthYear" => #{"TagNum" => "200" ,"Type" => "MONTHYEAR" ,"ValidValues" =>[]}
, "200" => #{"Name"=>"MaturityMonthYear" ,"Type"=>"MONTHYEAR" ,"ValidValues"=>[], "TagNum" => "200"}


,
"PutOrCall" => #{"TagNum" => "201" ,"Type" => "INT" ,"ValidValues" =>[{"0", "PUT"},{"1", "CALL"}]}
, "201" => #{"Name"=>"PutOrCall" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "PUT"},{"1", "CALL"}], "TagNum" => "201"}


,
"StrikePrice" => #{"TagNum" => "202" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "202" => #{"Name"=>"StrikePrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "202"}


,
"CoveredOrUncovered" => #{"TagNum" => "203" ,"Type" => "INT" ,"ValidValues" =>[{"0", "COVERED"},{"1", "UNCOVERED"}]}
, "203" => #{"Name"=>"CoveredOrUncovered" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "COVERED"},{"1", "UNCOVERED"}], "TagNum" => "203"}


,
"CustomerOrFirm" => #{"TagNum" => "204" ,"Type" => "INT" ,"ValidValues" =>[{"0", "CUSTOMER"},{"1", "FIRM"}]}
, "204" => #{"Name"=>"CustomerOrFirm" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "CUSTOMER"},{"1", "FIRM"}], "TagNum" => "204"}


,
"MaturityDay" => #{"TagNum" => "205" ,"Type" => "DAYOFMONTH" ,"ValidValues" =>[]}
, "205" => #{"Name"=>"MaturityDay" ,"Type"=>"DAYOFMONTH" ,"ValidValues"=>[], "TagNum" => "205"}


,
"OptAttribute" => #{"TagNum" => "206" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "206" => #{"Name"=>"OptAttribute" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "206"}


,
"SecurityExchange" => #{"TagNum" => "207" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "207" => #{"Name"=>"SecurityExchange" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "207"}


,
"NotifyBrokerOfCredit" => #{"TagNum" => "208" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "208" => #{"Name"=>"NotifyBrokerOfCredit" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "208"}


,
"AllocHandlInst" => #{"TagNum" => "209" ,"Type" => "INT" ,"ValidValues" =>[{"1", "MATCH"},{"2", "FORWARD"},{"3", "FORWARD_AND_MATCH"}]}
, "209" => #{"Name"=>"AllocHandlInst" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "MATCH"},{"2", "FORWARD"},{"3", "FORWARD_AND_MATCH"}], "TagNum" => "209"}


,
"MaxShow" => #{"TagNum" => "210" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "210" => #{"Name"=>"MaxShow" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "210"}


,
"PegDifference" => #{"TagNum" => "211" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "211" => #{"Name"=>"PegDifference" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "211"}


,
"XmlDataLen" => #{"TagNum" => "212" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "212" => #{"Name"=>"XmlDataLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "212"}


,
"XmlData" => #{"TagNum" => "213" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "213" => #{"Name"=>"XmlData" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "213"}


,
"SettlInstRefID" => #{"TagNum" => "214" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "214" => #{"Name"=>"SettlInstRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "214"}


,
"NoRoutingIDs" => #{"TagNum" => "215" ,"Type" => "INT" ,"ValidValues" =>[]}
, "215" => #{"Name"=>"NoRoutingIDs" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "215"}


,
"RoutingType" => #{"TagNum" => "216" ,"Type" => "INT" ,"ValidValues" =>[{"1", "TARGET_FIRM"},{"2", "TARGET_LIST"},{"3", "BLOCK_FIRM"},{"4", "BLOCK_LIST"}]}
, "216" => #{"Name"=>"RoutingType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "TARGET_FIRM"},{"2", "TARGET_LIST"},{"3", "BLOCK_FIRM"},{"4", "BLOCK_LIST"}], "TagNum" => "216"}


,
"RoutingID" => #{"TagNum" => "217" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "217" => #{"Name"=>"RoutingID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "217"}


,
"SpreadToBenchmark" => #{"TagNum" => "218" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "218" => #{"Name"=>"SpreadToBenchmark" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "218"}


,
"Benchmark" => #{"TagNum" => "219" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "CURVE"},{"2", "5_YR"},{"3", "OLD_5"},{"4", "10_YR"},{"5", "OLD_10"},{"6", "30_YR"},{"7", "OLD_30"},{"8", "3_MO_LIBOR"},{"9", "6_MO_LIBOR"}]}
, "219" => #{"Name"=>"Benchmark" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "CURVE"},{"2", "5_YR"},{"3", "OLD_5"},{"4", "10_YR"},{"5", "OLD_10"},{"6", "30_YR"},{"7", "OLD_30"},{"8", "3_MO_LIBOR"},{"9", "6_MO_LIBOR"}], "TagNum" => "219"}


,
"CouponRate" => #{"TagNum" => "223" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "223" => #{"Name"=>"CouponRate" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "223"}


,
"ContractMultiplier" => #{"TagNum" => "231" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "231" => #{"Name"=>"ContractMultiplier" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "231"}


,
"MDReqID" => #{"TagNum" => "262" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "262" => #{"Name"=>"MDReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "262"}


,
"SubscriptionRequestType" => #{"TagNum" => "263" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "SNAPSHOT"},{"1", "SNAPSHOT_PLUS_UPDATES"},{"2", "DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST"}]}
, "263" => #{"Name"=>"SubscriptionRequestType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "SNAPSHOT"},{"1", "SNAPSHOT_PLUS_UPDATES"},{"2", "DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST"}], "TagNum" => "263"}


,
"MarketDepth" => #{"TagNum" => "264" ,"Type" => "INT" ,"ValidValues" =>[]}
, "264" => #{"Name"=>"MarketDepth" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "264"}


,
"MDUpdateType" => #{"TagNum" => "265" ,"Type" => "INT" ,"ValidValues" =>[{"0", "FULL_REFRESH"},{"1", "INCREMENTAL_REFRESH"}]}
, "265" => #{"Name"=>"MDUpdateType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "FULL_REFRESH"},{"1", "INCREMENTAL_REFRESH"}], "TagNum" => "265"}


,
"AggregatedBook" => #{"TagNum" => "266" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "266" => #{"Name"=>"AggregatedBook" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "266"}


,
"NoMDEntryTypes" => #{"TagNum" => "267" ,"Type" => "INT" ,"ValidValues" =>[]}
, "267" => #{"Name"=>"NoMDEntryTypes" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "267"}


,
"NoMDEntries" => #{"TagNum" => "268" ,"Type" => "INT" ,"ValidValues" =>[]}
, "268" => #{"Name"=>"NoMDEntries" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "268"}


,
"MDEntryType" => #{"TagNum" => "269" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "BID"},{"1", "OFFER"},{"2", "TRADE"},{"3", "INDEX_VALUE"},{"4", "OPENING_PRICE"},{"5", "CLOSING_PRICE"},{"6", "SETTLEMENT_PRICE"},{"7", "TRADING_SESSION_HIGH_PRICE"},{"8", "TRADING_SESSION_LOW_PRICE"},{"9", "TRADING_SESSION_VWAP_PRICE"}]}
, "269" => #{"Name"=>"MDEntryType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "BID"},{"1", "OFFER"},{"2", "TRADE"},{"3", "INDEX_VALUE"},{"4", "OPENING_PRICE"},{"5", "CLOSING_PRICE"},{"6", "SETTLEMENT_PRICE"},{"7", "TRADING_SESSION_HIGH_PRICE"},{"8", "TRADING_SESSION_LOW_PRICE"},{"9", "TRADING_SESSION_VWAP_PRICE"}], "TagNum" => "269"}


,
"MDEntryPx" => #{"TagNum" => "270" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "270" => #{"Name"=>"MDEntryPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "270"}


,
"MDEntrySize" => #{"TagNum" => "271" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "271" => #{"Name"=>"MDEntrySize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "271"}


,
"MDEntryDate" => #{"TagNum" => "272" ,"Type" => "UTCDATE" ,"ValidValues" =>[]}
, "272" => #{"Name"=>"MDEntryDate" ,"Type"=>"UTCDATE" ,"ValidValues"=>[], "TagNum" => "272"}


,
"MDEntryTime" => #{"TagNum" => "273" ,"Type" => "UTCTIMEONLY" ,"ValidValues" =>[]}
, "273" => #{"Name"=>"MDEntryTime" ,"Type"=>"UTCTIMEONLY" ,"ValidValues"=>[], "TagNum" => "273"}


,
"TickDirection" => #{"TagNum" => "274" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "PLUS_TICK"},{"1", "ZERO_PLUS_TICK"},{"2", "MINUS_TICK"},{"3", "ZERO_MINUS_TICK"}]}
, "274" => #{"Name"=>"TickDirection" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "PLUS_TICK"},{"1", "ZERO_PLUS_TICK"},{"2", "MINUS_TICK"},{"3", "ZERO_MINUS_TICK"}], "TagNum" => "274"}


,
"MDMkt" => #{"TagNum" => "275" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "275" => #{"Name"=>"MDMkt" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "275"}


,
"QuoteCondition" => #{"TagNum" => "276" ,"Type" => "MULTIPLEVALUESTRING" ,"ValidValues" =>[{"A", "OPEN"},{"B", "CLOSED"},{"C", "EXCHANGE_BEST"},{"D", "CONSOLIDATED_BEST"},{"E", "LOCKED"},{"F", "CROSSED"},{"G", "DEPTH"},{"H", "FAST_TRADING"},{"I", "NON_FIRM"}]}
, "276" => #{"Name"=>"QuoteCondition" ,"Type"=>"MULTIPLEVALUESTRING" ,"ValidValues"=>[{"A", "OPEN"},{"B", "CLOSED"},{"C", "EXCHANGE_BEST"},{"D", "CONSOLIDATED_BEST"},{"E", "LOCKED"},{"F", "CROSSED"},{"G", "DEPTH"},{"H", "FAST_TRADING"},{"I", "NON_FIRM"}], "TagNum" => "276"}


,
"TradeCondition" => #{"TagNum" => "277" ,"Type" => "MULTIPLEVALUESTRING" ,"ValidValues" =>[{"A", "CASH"},{"B", "AVERAGE_PRICE_TRADE"},{"C", "CASH_TRADE"},{"D", "NEXT_DAY"},{"E", "OPENING"},{"F", "INTRADAY_TRADE_DETAIL"},{"G", "RULE_127_TRADE"},{"H", "RULE_155_TRADE"},{"I", "SOLD_LAST"},{"J", "NEXT_DAY_TRADE"},{"K", "OPENED"},{"L", "SELLER"},{"M", "SOLD"},{"N", "STOPPED_STOCK"}]}
, "277" => #{"Name"=>"TradeCondition" ,"Type"=>"MULTIPLEVALUESTRING" ,"ValidValues"=>[{"A", "CASH"},{"B", "AVERAGE_PRICE_TRADE"},{"C", "CASH_TRADE"},{"D", "NEXT_DAY"},{"E", "OPENING"},{"F", "INTRADAY_TRADE_DETAIL"},{"G", "RULE_127_TRADE"},{"H", "RULE_155_TRADE"},{"I", "SOLD_LAST"},{"J", "NEXT_DAY_TRADE"},{"K", "OPENED"},{"L", "SELLER"},{"M", "SOLD"},{"N", "STOPPED_STOCK"}], "TagNum" => "277"}


,
"MDEntryID" => #{"TagNum" => "278" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "278" => #{"Name"=>"MDEntryID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "278"}


,
"MDUpdateAction" => #{"TagNum" => "279" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "CHANGE"},{"2", "DELETE"}]}
, "279" => #{"Name"=>"MDUpdateAction" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "CHANGE"},{"2", "DELETE"}], "TagNum" => "279"}


,
"MDEntryRefID" => #{"TagNum" => "280" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "280" => #{"Name"=>"MDEntryRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "280"}


,
"MDReqRejReason" => #{"TagNum" => "281" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "UNKNOWN_SYMBOL"},{"1", "DUPLICATE_MDREQID"},{"2", "INSUFFICIENT_BANDWIDTH"},{"3", "INSUFFICIENT_PERMISSIONS"},{"4", "UNSUPPORTED_SUBSCRIPTIONREQUESTTYPE"},{"5", "UNSUPPORTED_MARKETDEPTH"},{"6", "UNSUPPORTED_MDUPDATETYPE"},{"7", "UNSUPPORTED_AGGREGATEDBOOK"},{"8", "UNSUPPORTED_MDENTRYTYPE"}]}
, "281" => #{"Name"=>"MDReqRejReason" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "UNKNOWN_SYMBOL"},{"1", "DUPLICATE_MDREQID"},{"2", "INSUFFICIENT_BANDWIDTH"},{"3", "INSUFFICIENT_PERMISSIONS"},{"4", "UNSUPPORTED_SUBSCRIPTIONREQUESTTYPE"},{"5", "UNSUPPORTED_MARKETDEPTH"},{"6", "UNSUPPORTED_MDUPDATETYPE"},{"7", "UNSUPPORTED_AGGREGATEDBOOK"},{"8", "UNSUPPORTED_MDENTRYTYPE"}], "TagNum" => "281"}


,
"MDEntryOriginator" => #{"TagNum" => "282" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "282" => #{"Name"=>"MDEntryOriginator" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "282"}


,
"LocationID" => #{"TagNum" => "283" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "283" => #{"Name"=>"LocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "283"}


,
"DeskID" => #{"TagNum" => "284" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "284" => #{"Name"=>"DeskID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "284"}


,
"DeleteReason" => #{"TagNum" => "285" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "CANCELATION"},{"1", "ERROR"}]}
, "285" => #{"Name"=>"DeleteReason" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "CANCELATION"},{"1", "ERROR"}], "TagNum" => "285"}


,
"OpenCloseSettleFlag" => #{"TagNum" => "286" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "DAILY_OPEN"},{"1", "SESSION_OPEN"},{"2", "DELIVERY_SETTLEMENT_PRICE"}]}
, "286" => #{"Name"=>"OpenCloseSettleFlag" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "DAILY_OPEN"},{"1", "SESSION_OPEN"},{"2", "DELIVERY_SETTLEMENT_PRICE"}], "TagNum" => "286"}


,
"SellerDays" => #{"TagNum" => "287" ,"Type" => "INT" ,"ValidValues" =>[]}
, "287" => #{"Name"=>"SellerDays" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "287"}


,
"MDEntryBuyer" => #{"TagNum" => "288" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "288" => #{"Name"=>"MDEntryBuyer" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "288"}


,
"MDEntrySeller" => #{"TagNum" => "289" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "289" => #{"Name"=>"MDEntrySeller" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "289"}


,
"MDEntryPositionNo" => #{"TagNum" => "290" ,"Type" => "INT" ,"ValidValues" =>[]}
, "290" => #{"Name"=>"MDEntryPositionNo" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "290"}


,
"FinancialStatus" => #{"TagNum" => "291" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "BANKRUPT"}]}
, "291" => #{"Name"=>"FinancialStatus" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "BANKRUPT"}], "TagNum" => "291"}


,
"CorporateAction" => #{"TagNum" => "292" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "EX_DIVIDEND"},{"B", "EX_DISTRIBUTION"},{"C", "EX_RIGHTS"},{"D", "NEW"},{"E", "EX_INTEREST"}]}
, "292" => #{"Name"=>"CorporateAction" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "EX_DIVIDEND"},{"B", "EX_DISTRIBUTION"},{"C", "EX_RIGHTS"},{"D", "NEW"},{"E", "EX_INTEREST"}], "TagNum" => "292"}


,
"DefBidSize" => #{"TagNum" => "293" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "293" => #{"Name"=>"DefBidSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "293"}


,
"DefOfferSize" => #{"TagNum" => "294" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "294" => #{"Name"=>"DefOfferSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "294"}


,
"NoQuoteEntries" => #{"TagNum" => "295" ,"Type" => "INT" ,"ValidValues" =>[]}
, "295" => #{"Name"=>"NoQuoteEntries" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "295"}


,
"NoQuoteSets" => #{"TagNum" => "296" ,"Type" => "INT" ,"ValidValues" =>[]}
, "296" => #{"Name"=>"NoQuoteSets" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "296"}


,
"QuoteAckStatus" => #{"TagNum" => "297" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ACCEPTED"},{"1", "CANCELED_FOR_SYMBOL"},{"2", "CANCELED_FOR_SECURITY_TYPE"},{"3", "CANCELED_FOR_UNDERLYING"},{"4", "CANCELED_ALL"},{"5", "REJECTED"}]}
, "297" => #{"Name"=>"QuoteAckStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ACCEPTED"},{"1", "CANCELED_FOR_SYMBOL"},{"2", "CANCELED_FOR_SECURITY_TYPE"},{"3", "CANCELED_FOR_UNDERLYING"},{"4", "CANCELED_ALL"},{"5", "REJECTED"}], "TagNum" => "297"}


,
"QuoteCancelType" => #{"TagNum" => "298" ,"Type" => "INT" ,"ValidValues" =>[{"1", "CANCEL_FOR_SYMBOL"},{"2", "CANCEL_FOR_SECURITY_TYPE"},{"3", "CANCEL_FOR_UNDERLYING_SYMBOL"},{"4", "CANCEL_FOR_ALL_QUOTES"}]}
, "298" => #{"Name"=>"QuoteCancelType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "CANCEL_FOR_SYMBOL"},{"2", "CANCEL_FOR_SECURITY_TYPE"},{"3", "CANCEL_FOR_UNDERLYING_SYMBOL"},{"4", "CANCEL_FOR_ALL_QUOTES"}], "TagNum" => "298"}


,
"QuoteEntryID" => #{"TagNum" => "299" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "299" => #{"Name"=>"QuoteEntryID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "299"}


,
"QuoteRejectReason" => #{"TagNum" => "300" ,"Type" => "INT" ,"ValidValues" =>[{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE"},{"3", "QUOTE_REQUEST_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "UNKNOWN_QUOTE"},{"6", "DUPLICATE_QUOTE"},{"7", "INVALID_BID_ASK_SPREAD"},{"8", "INVALID_PRICE"},{"9", "NOT_AUTHORIZED_TO_QUOTE_SECURITY"}]}
, "300" => #{"Name"=>"QuoteRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE"},{"3", "QUOTE_REQUEST_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "UNKNOWN_QUOTE"},{"6", "DUPLICATE_QUOTE"},{"7", "INVALID_BID_ASK_SPREAD"},{"8", "INVALID_PRICE"},{"9", "NOT_AUTHORIZED_TO_QUOTE_SECURITY"}], "TagNum" => "300"}


,
"QuoteResponseLevel" => #{"TagNum" => "301" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NO_ACKNOWLEDGEMENT"},{"1", "ACKNOWLEDGE_ONLY_NEGATIVE_OR_ERRONEOUS_QUOTES"},{"2", "ACKNOWLEDGE_EACH_QUOTE_MESSAGES"}]}
, "301" => #{"Name"=>"QuoteResponseLevel" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NO_ACKNOWLEDGEMENT"},{"1", "ACKNOWLEDGE_ONLY_NEGATIVE_OR_ERRONEOUS_QUOTES"},{"2", "ACKNOWLEDGE_EACH_QUOTE_MESSAGES"}], "TagNum" => "301"}


,
"QuoteSetID" => #{"TagNum" => "302" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "302" => #{"Name"=>"QuoteSetID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "302"}


,
"QuoteRequestType" => #{"TagNum" => "303" ,"Type" => "INT" ,"ValidValues" =>[{"1", "MANUAL"},{"2", "AUTOMATIC"}]}
, "303" => #{"Name"=>"QuoteRequestType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "MANUAL"},{"2", "AUTOMATIC"}], "TagNum" => "303"}


,
"TotQuoteEntries" => #{"TagNum" => "304" ,"Type" => "INT" ,"ValidValues" =>[]}
, "304" => #{"Name"=>"TotQuoteEntries" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "304"}


,
"UnderlyingIDSource" => #{"TagNum" => "305" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "305" => #{"Name"=>"UnderlyingIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "305"}


,
"UnderlyingIssuer" => #{"TagNum" => "306" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "306" => #{"Name"=>"UnderlyingIssuer" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "306"}


,
"UnderlyingSecurityDesc" => #{"TagNum" => "307" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "307" => #{"Name"=>"UnderlyingSecurityDesc" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "307"}


,
"UnderlyingSecurityExchange" => #{"TagNum" => "308" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "308" => #{"Name"=>"UnderlyingSecurityExchange" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "308"}


,
"UnderlyingSecurityID" => #{"TagNum" => "309" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "309" => #{"Name"=>"UnderlyingSecurityID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "309"}


,
"UnderlyingSecurityType" => #{"TagNum" => "310" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "310" => #{"Name"=>"UnderlyingSecurityType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "310"}


,
"UnderlyingSymbol" => #{"TagNum" => "311" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "311" => #{"Name"=>"UnderlyingSymbol" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "311"}


,
"UnderlyingSymbolSfx" => #{"TagNum" => "312" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "312" => #{"Name"=>"UnderlyingSymbolSfx" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "312"}


,
"UnderlyingMaturityMonthYear" => #{"TagNum" => "313" ,"Type" => "MONTHYEAR" ,"ValidValues" =>[]}
, "313" => #{"Name"=>"UnderlyingMaturityMonthYear" ,"Type"=>"MONTHYEAR" ,"ValidValues"=>[], "TagNum" => "313"}


,
"UnderlyingMaturityDay" => #{"TagNum" => "314" ,"Type" => "DAYOFMONTH" ,"ValidValues" =>[]}
, "314" => #{"Name"=>"UnderlyingMaturityDay" ,"Type"=>"DAYOFMONTH" ,"ValidValues"=>[], "TagNum" => "314"}


,
"UnderlyingPutOrCall" => #{"TagNum" => "315" ,"Type" => "INT" ,"ValidValues" =>[]}
, "315" => #{"Name"=>"UnderlyingPutOrCall" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "315"}


,
"UnderlyingStrikePrice" => #{"TagNum" => "316" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "316" => #{"Name"=>"UnderlyingStrikePrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "316"}


,
"UnderlyingOptAttribute" => #{"TagNum" => "317" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "317" => #{"Name"=>"UnderlyingOptAttribute" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "317"}


,
"UnderlyingCurrency" => #{"TagNum" => "318" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "318" => #{"Name"=>"UnderlyingCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "318"}


,
"RatioQty" => #{"TagNum" => "319" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "319" => #{"Name"=>"RatioQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "319"}


,
"SecurityReqID" => #{"TagNum" => "320" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "320" => #{"Name"=>"SecurityReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "320"}


,
"SecurityRequestType" => #{"TagNum" => "321" ,"Type" => "INT" ,"ValidValues" =>[{"0", "REQUEST_SECURITY_IDENTITY_AND_SPECIFICATIONS"},{"1", "REQUEST_SECURITY_IDENTITY_FOR_THE_SPECIFICATIONS_PROVIDED"},{"2", "REQUEST_LIST_SECURITY_TYPES"},{"3", "REQUEST_LIST_SECURITIES"}]}
, "321" => #{"Name"=>"SecurityRequestType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "REQUEST_SECURITY_IDENTITY_AND_SPECIFICATIONS"},{"1", "REQUEST_SECURITY_IDENTITY_FOR_THE_SPECIFICATIONS_PROVIDED"},{"2", "REQUEST_LIST_SECURITY_TYPES"},{"3", "REQUEST_LIST_SECURITIES"}], "TagNum" => "321"}


,
"SecurityResponseID" => #{"TagNum" => "322" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "322" => #{"Name"=>"SecurityResponseID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "322"}


,
"SecurityResponseType" => #{"TagNum" => "323" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ACCEPT_SECURITY_PROPOSAL_AS_IS"},{"2", "ACCEPT_SECURITY_PROPOSAL_WITH_REVISIONS_AS_INDICATED_IN_THE_MESSAGE"},{"3", "LIST_OF_SECURITY_TYPES_RETURNED_PER_REQUEST"},{"4", "LIST_OF_SECURITIES_RETURNED_PER_REQUEST"},{"5", "REJECT_SECURITY_PROPOSAL"},{"6", "CAN_NOT_MATCH_SELECTION_CRITERIA"}]}
, "323" => #{"Name"=>"SecurityResponseType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ACCEPT_SECURITY_PROPOSAL_AS_IS"},{"2", "ACCEPT_SECURITY_PROPOSAL_WITH_REVISIONS_AS_INDICATED_IN_THE_MESSAGE"},{"3", "LIST_OF_SECURITY_TYPES_RETURNED_PER_REQUEST"},{"4", "LIST_OF_SECURITIES_RETURNED_PER_REQUEST"},{"5", "REJECT_SECURITY_PROPOSAL"},{"6", "CAN_NOT_MATCH_SELECTION_CRITERIA"}], "TagNum" => "323"}


,
"SecurityStatusReqID" => #{"TagNum" => "324" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "324" => #{"Name"=>"SecurityStatusReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "324"}


,
"UnsolicitedIndicator" => #{"TagNum" => "325" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "325" => #{"Name"=>"UnsolicitedIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "325"}


,
"SecurityTradingStatus" => #{"TagNum" => "326" ,"Type" => "INT" ,"ValidValues" =>[{"1", "OPENING_DELAY"},{"10", "MARKET_ON_CLOSE_IMBALANCE_SELL"},{"12", "NO_MARKET_IMBALANCE"},{"13", "NO_MARKET_ON_CLOSE_IMBALANCE"},{"14", "ITS_PRE_OPENING"},{"15", "NEW_PRICE_INDICATION"},{"16", "TRADE_DISSEMINATION_TIME"},{"17", "READY_TO_TRADE"},{"18", "NOT_AVAILABLE_FOR_TRADING"},{"19", "NOT_TRADED_ON_THIS_MARKET"},{"2", "TRADING_HALT"},{"20", "UNKNOWN_OR_INVALID"},{"3", "RESUME"},{"4", "NO_OPEN_NO_RESUME"},{"5", "PRICE_INDICATION"},{"6", "TRADING_RANGE_INDICATION"},{"7", "MARKET_IMBALANCE_BUY"},{"8", "MARKET_IMBALANCE_SELL"},{"9", "MARKET_ON_CLOSE_IMBALANCE_BUY"}]}
, "326" => #{"Name"=>"SecurityTradingStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "OPENING_DELAY"},{"10", "MARKET_ON_CLOSE_IMBALANCE_SELL"},{"12", "NO_MARKET_IMBALANCE"},{"13", "NO_MARKET_ON_CLOSE_IMBALANCE"},{"14", "ITS_PRE_OPENING"},{"15", "NEW_PRICE_INDICATION"},{"16", "TRADE_DISSEMINATION_TIME"},{"17", "READY_TO_TRADE"},{"18", "NOT_AVAILABLE_FOR_TRADING"},{"19", "NOT_TRADED_ON_THIS_MARKET"},{"2", "TRADING_HALT"},{"20", "UNKNOWN_OR_INVALID"},{"3", "RESUME"},{"4", "NO_OPEN_NO_RESUME"},{"5", "PRICE_INDICATION"},{"6", "TRADING_RANGE_INDICATION"},{"7", "MARKET_IMBALANCE_BUY"},{"8", "MARKET_IMBALANCE_SELL"},{"9", "MARKET_ON_CLOSE_IMBALANCE_BUY"}], "TagNum" => "326"}


,
"HaltReasonChar" => #{"TagNum" => "327" ,"Type" => "CHAR" ,"ValidValues" =>[{"D", "NEWS_DISSEMINATION"},{"E", "ORDER_INFLUX"},{"I", "ORDER_IMBALANCE"},{"M", "ADDITIONAL_INFORMATION"},{"P", "NEWS_PENDING"},{"X", "EQUIPMENT_CHANGEOVER"}]}
, "327" => #{"Name"=>"HaltReasonChar" ,"Type"=>"CHAR" ,"ValidValues"=>[{"D", "NEWS_DISSEMINATION"},{"E", "ORDER_INFLUX"},{"I", "ORDER_IMBALANCE"},{"M", "ADDITIONAL_INFORMATION"},{"P", "NEWS_PENDING"},{"X", "EQUIPMENT_CHANGEOVER"}], "TagNum" => "327"}


,
"InViewOfCommon" => #{"TagNum" => "328" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "328" => #{"Name"=>"InViewOfCommon" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "328"}


,
"DueToRelated" => #{"TagNum" => "329" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "329" => #{"Name"=>"DueToRelated" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "329"}


,
"BuyVolume" => #{"TagNum" => "330" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "330" => #{"Name"=>"BuyVolume" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "330"}


,
"SellVolume" => #{"TagNum" => "331" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "331" => #{"Name"=>"SellVolume" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "331"}


,
"HighPx" => #{"TagNum" => "332" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "332" => #{"Name"=>"HighPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "332"}


,
"LowPx" => #{"TagNum" => "333" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "333" => #{"Name"=>"LowPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "333"}


,
"Adjustment" => #{"TagNum" => "334" ,"Type" => "INT" ,"ValidValues" =>[{"1", "CANCEL"},{"2", "ERROR"},{"3", "CORRECTION"}]}
, "334" => #{"Name"=>"Adjustment" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "CANCEL"},{"2", "ERROR"},{"3", "CORRECTION"}], "TagNum" => "334"}


,
"TradSesReqID" => #{"TagNum" => "335" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "335" => #{"Name"=>"TradSesReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "335"}


,
"TradingSessionID" => #{"TagNum" => "336" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "336" => #{"Name"=>"TradingSessionID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "336"}


,
"ContraTrader" => #{"TagNum" => "337" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "337" => #{"Name"=>"ContraTrader" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "337"}


,
"TradSesMethod" => #{"TagNum" => "338" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ELECTRONIC"},{"2", "OPEN_OUTCRY"},{"3", "TWO_PARTY"}]}
, "338" => #{"Name"=>"TradSesMethod" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ELECTRONIC"},{"2", "OPEN_OUTCRY"},{"3", "TWO_PARTY"}], "TagNum" => "338"}


,
"TradSesMode" => #{"TagNum" => "339" ,"Type" => "INT" ,"ValidValues" =>[{"1", "TESTING"},{"2", "SIMULATED"},{"3", "PRODUCTION"}]}
, "339" => #{"Name"=>"TradSesMode" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "TESTING"},{"2", "SIMULATED"},{"3", "PRODUCTION"}], "TagNum" => "339"}


,
"TradSesStatus" => #{"TagNum" => "340" ,"Type" => "INT" ,"ValidValues" =>[{"1", "HALTED"},{"2", "OPEN"},{"3", "CLOSED"},{"4", "PRE_OPEN"},{"5", "PRE_CLOSE"}]}
, "340" => #{"Name"=>"TradSesStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "HALTED"},{"2", "OPEN"},{"3", "CLOSED"},{"4", "PRE_OPEN"},{"5", "PRE_CLOSE"}], "TagNum" => "340"}


,
"TradSesStartTime" => #{"TagNum" => "341" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "341" => #{"Name"=>"TradSesStartTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "341"}


,
"TradSesOpenTime" => #{"TagNum" => "342" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "342" => #{"Name"=>"TradSesOpenTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "342"}


,
"TradSesPreCloseTime" => #{"TagNum" => "343" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "343" => #{"Name"=>"TradSesPreCloseTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "343"}


,
"TradSesCloseTime" => #{"TagNum" => "344" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "344" => #{"Name"=>"TradSesCloseTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "344"}


,
"TradSesEndTime" => #{"TagNum" => "345" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "345" => #{"Name"=>"TradSesEndTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "345"}


,
"NumberOfOrders" => #{"TagNum" => "346" ,"Type" => "INT" ,"ValidValues" =>[]}
, "346" => #{"Name"=>"NumberOfOrders" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "346"}


,
"MessageEncoding" => #{"TagNum" => "347" ,"Type" => "STRING" ,"ValidValues" =>[{"EUC-JP", "EUC_JP"},{"ISO-2022-JP", "ISO_2022_JP"},{"SHIFT_JIS", "SHIFT_JIS"},{"UTF-8", "UTF_8"}]}
, "347" => #{"Name"=>"MessageEncoding" ,"Type"=>"STRING" ,"ValidValues"=>[{"EUC-JP", "EUC_JP"},{"ISO-2022-JP", "ISO_2022_JP"},{"SHIFT_JIS", "SHIFT_JIS"},{"UTF-8", "UTF_8"}], "TagNum" => "347"}


,
"EncodedIssuerLen" => #{"TagNum" => "348" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "348" => #{"Name"=>"EncodedIssuerLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "348"}


,
"EncodedIssuer" => #{"TagNum" => "349" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "349" => #{"Name"=>"EncodedIssuer" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "349"}


,
"EncodedSecurityDescLen" => #{"TagNum" => "350" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "350" => #{"Name"=>"EncodedSecurityDescLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "350"}


,
"EncodedSecurityDesc" => #{"TagNum" => "351" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "351" => #{"Name"=>"EncodedSecurityDesc" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "351"}


,
"EncodedListExecInstLen" => #{"TagNum" => "352" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "352" => #{"Name"=>"EncodedListExecInstLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "352"}


,
"EncodedListExecInst" => #{"TagNum" => "353" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "353" => #{"Name"=>"EncodedListExecInst" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "353"}


,
"EncodedTextLen" => #{"TagNum" => "354" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "354" => #{"Name"=>"EncodedTextLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "354"}


,
"EncodedText" => #{"TagNum" => "355" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "355" => #{"Name"=>"EncodedText" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "355"}


,
"EncodedSubjectLen" => #{"TagNum" => "356" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "356" => #{"Name"=>"EncodedSubjectLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "356"}


,
"EncodedSubject" => #{"TagNum" => "357" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "357" => #{"Name"=>"EncodedSubject" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "357"}


,
"EncodedHeadlineLen" => #{"TagNum" => "358" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "358" => #{"Name"=>"EncodedHeadlineLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "358"}


,
"EncodedHeadline" => #{"TagNum" => "359" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "359" => #{"Name"=>"EncodedHeadline" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "359"}


,
"EncodedAllocTextLen" => #{"TagNum" => "360" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "360" => #{"Name"=>"EncodedAllocTextLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "360"}


,
"EncodedAllocText" => #{"TagNum" => "361" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "361" => #{"Name"=>"EncodedAllocText" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "361"}


,
"EncodedUnderlyingIssuerLen" => #{"TagNum" => "362" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "362" => #{"Name"=>"EncodedUnderlyingIssuerLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "362"}


,
"EncodedUnderlyingIssuer" => #{"TagNum" => "363" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "363" => #{"Name"=>"EncodedUnderlyingIssuer" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "363"}


,
"EncodedUnderlyingSecurityDescLen" => #{"TagNum" => "364" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "364" => #{"Name"=>"EncodedUnderlyingSecurityDescLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "364"}


,
"EncodedUnderlyingSecurityDesc" => #{"TagNum" => "365" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "365" => #{"Name"=>"EncodedUnderlyingSecurityDesc" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "365"}


,
"AllocPrice" => #{"TagNum" => "366" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "366" => #{"Name"=>"AllocPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "366"}


,
"QuoteSetValidUntilTime" => #{"TagNum" => "367" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "367" => #{"Name"=>"QuoteSetValidUntilTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "367"}


,
"QuoteEntryRejectReason" => #{"TagNum" => "368" ,"Type" => "INT" ,"ValidValues" =>[{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE"},{"3", "QUOTE_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "UNKNOWN_QUOTE"},{"6", "DUPLICATE_QUOTE"},{"7", "INVALID_BID_ASK_SPREAD"},{"8", "INVALID_PRICE"},{"9", "NOT_AUTHORIZED_TO_QUOTE_SECURITY"}]}
, "368" => #{"Name"=>"QuoteEntryRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE"},{"3", "QUOTE_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "UNKNOWN_QUOTE"},{"6", "DUPLICATE_QUOTE"},{"7", "INVALID_BID_ASK_SPREAD"},{"8", "INVALID_PRICE"},{"9", "NOT_AUTHORIZED_TO_QUOTE_SECURITY"}], "TagNum" => "368"}


,
"LastMsgSeqNumProcessed" => #{"TagNum" => "369" ,"Type" => "INT" ,"ValidValues" =>[]}
, "369" => #{"Name"=>"LastMsgSeqNumProcessed" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "369"}


,
"OnBehalfOfSendingTime" => #{"TagNum" => "370" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "370" => #{"Name"=>"OnBehalfOfSendingTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "370"}


,
"RefTagID" => #{"TagNum" => "371" ,"Type" => "INT" ,"ValidValues" =>[]}
, "371" => #{"Name"=>"RefTagID" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "371"}


,
"RefMsgType" => #{"TagNum" => "372" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "372" => #{"Name"=>"RefMsgType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "372"}


,
"SessionRejectReason" => #{"TagNum" => "373" ,"Type" => "INT" ,"ValidValues" =>[{"0", "INVALID_TAG_NUMBER"},{"1", "REQUIRED_TAG_MISSING"},{"10", "SENDINGTIME_ACCURACY_PROBLEM"},{"11", "INVALID_MSGTYPE"},{"2", "TAG_NOT_DEFINED_FOR_THIS_MESSAGE_TYPE"},{"3", "UNDEFINED_TAG"},{"4", "TAG_SPECIFIED_WITHOUT_A_VALUE"},{"5", "VALUE_IS_INCORRECT"},{"6", "INCORRECT_DATA_FORMAT_FOR_VALUE"},{"7", "DECRYPTION_PROBLEM"},{"8", "SIGNATURE_PROBLEM"},{"9", "COMPID_PROBLEM"}]}
, "373" => #{"Name"=>"SessionRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "INVALID_TAG_NUMBER"},{"1", "REQUIRED_TAG_MISSING"},{"10", "SENDINGTIME_ACCURACY_PROBLEM"},{"11", "INVALID_MSGTYPE"},{"2", "TAG_NOT_DEFINED_FOR_THIS_MESSAGE_TYPE"},{"3", "UNDEFINED_TAG"},{"4", "TAG_SPECIFIED_WITHOUT_A_VALUE"},{"5", "VALUE_IS_INCORRECT"},{"6", "INCORRECT_DATA_FORMAT_FOR_VALUE"},{"7", "DECRYPTION_PROBLEM"},{"8", "SIGNATURE_PROBLEM"},{"9", "COMPID_PROBLEM"}], "TagNum" => "373"}


,
"BidRequestTransType" => #{"TagNum" => "374" ,"Type" => "CHAR" ,"ValidValues" =>[{"C", "CANCEL"},{"N", "NO"}]}
, "374" => #{"Name"=>"BidRequestTransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"C", "CANCEL"},{"N", "NO"}], "TagNum" => "374"}


,
"ContraBroker" => #{"TagNum" => "375" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "375" => #{"Name"=>"ContraBroker" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "375"}


,
"ComplianceID" => #{"TagNum" => "376" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "376" => #{"Name"=>"ComplianceID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "376"}


,
"SolicitedFlag" => #{"TagNum" => "377" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "377" => #{"Name"=>"SolicitedFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "377"}


,
"ExecRestatementReason" => #{"TagNum" => "378" ,"Type" => "INT" ,"ValidValues" =>[{"0", "GT_CORPORATE_ACTION"},{"1", "GT_RENEWAL"},{"2", "VERBAL_CHANGE"},{"3", "REPRICING_OF_ORDER"},{"4", "BROKER_OPTION"},{"5", "PARTIAL_DECLINE_OF_ORDERQTY"}]}
, "378" => #{"Name"=>"ExecRestatementReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "GT_CORPORATE_ACTION"},{"1", "GT_RENEWAL"},{"2", "VERBAL_CHANGE"},{"3", "REPRICING_OF_ORDER"},{"4", "BROKER_OPTION"},{"5", "PARTIAL_DECLINE_OF_ORDERQTY"}], "TagNum" => "378"}


,
"BusinessRejectRefID" => #{"TagNum" => "379" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "379" => #{"Name"=>"BusinessRejectRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "379"}


,
"BusinessRejectReason" => #{"TagNum" => "380" ,"Type" => "INT" ,"ValidValues" =>[{"0", "OTHER"},{"1", "UNKOWN_ID"},{"2", "UNKNOWN_SECURITY"},{"3", "UNSUPPORTED_MESSAGE_TYPE"},{"4", "APPLICATION_NOT_AVAILABLE"},{"5", "CONDITIONALLY_REQUIRED_FIELD_MISSING"}]}
, "380" => #{"Name"=>"BusinessRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "OTHER"},{"1", "UNKOWN_ID"},{"2", "UNKNOWN_SECURITY"},{"3", "UNSUPPORTED_MESSAGE_TYPE"},{"4", "APPLICATION_NOT_AVAILABLE"},{"5", "CONDITIONALLY_REQUIRED_FIELD_MISSING"}], "TagNum" => "380"}


,
"GrossTradeAmt" => #{"TagNum" => "381" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "381" => #{"Name"=>"GrossTradeAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "381"}


,
"NoContraBrokers" => #{"TagNum" => "382" ,"Type" => "INT" ,"ValidValues" =>[]}
, "382" => #{"Name"=>"NoContraBrokers" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "382"}


,
"MaxMessageSize" => #{"TagNum" => "383" ,"Type" => "INT" ,"ValidValues" =>[]}
, "383" => #{"Name"=>"MaxMessageSize" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "383"}


,
"NoMsgTypes" => #{"TagNum" => "384" ,"Type" => "INT" ,"ValidValues" =>[]}
, "384" => #{"Name"=>"NoMsgTypes" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "384"}


,
"MsgDirection" => #{"TagNum" => "385" ,"Type" => "CHAR" ,"ValidValues" =>[{"R", "RECEIVE"},{"S", "SEND"}]}
, "385" => #{"Name"=>"MsgDirection" ,"Type"=>"CHAR" ,"ValidValues"=>[{"R", "RECEIVE"},{"S", "SEND"}], "TagNum" => "385"}


,
"NoTradingSessions" => #{"TagNum" => "386" ,"Type" => "INT" ,"ValidValues" =>[]}
, "386" => #{"Name"=>"NoTradingSessions" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "386"}


,
"TotalVolumeTraded" => #{"TagNum" => "387" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "387" => #{"Name"=>"TotalVolumeTraded" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "387"}


,
"DiscretionInst" => #{"TagNum" => "388" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "RELATED_TO_DISPLAYED_PRICE"},{"1", "RELATED_TO_MARKET_PRICE"},{"2", "RELATED_TO_PRIMARY_PRICE"},{"3", "RELATED_TO_LOCAL_PRIMARY_PRICE"},{"4", "RELATED_TO_MIDPOINT_PRICE"},{"5", "RELATED_TO_LAST_TRADE_PRICE"}]}
, "388" => #{"Name"=>"DiscretionInst" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "RELATED_TO_DISPLAYED_PRICE"},{"1", "RELATED_TO_MARKET_PRICE"},{"2", "RELATED_TO_PRIMARY_PRICE"},{"3", "RELATED_TO_LOCAL_PRIMARY_PRICE"},{"4", "RELATED_TO_MIDPOINT_PRICE"},{"5", "RELATED_TO_LAST_TRADE_PRICE"}], "TagNum" => "388"}


,
"DiscretionOffset" => #{"TagNum" => "389" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "389" => #{"Name"=>"DiscretionOffset" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "389"}


,
"BidID" => #{"TagNum" => "390" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "390" => #{"Name"=>"BidID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "390"}


,
"ClientBidID" => #{"TagNum" => "391" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "391" => #{"Name"=>"ClientBidID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "391"}


,
"ListName" => #{"TagNum" => "392" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "392" => #{"Name"=>"ListName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "392"}


,
"TotalNumSecurities" => #{"TagNum" => "393" ,"Type" => "INT" ,"ValidValues" =>[]}
, "393" => #{"Name"=>"TotalNumSecurities" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "393"}


,
"BidType" => #{"TagNum" => "394" ,"Type" => "INT" ,"ValidValues" =>[]}
, "394" => #{"Name"=>"BidType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "394"}


,
"NumTickets" => #{"TagNum" => "395" ,"Type" => "INT" ,"ValidValues" =>[]}
, "395" => #{"Name"=>"NumTickets" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "395"}


,
"SideValue1" => #{"TagNum" => "396" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "396" => #{"Name"=>"SideValue1" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "396"}


,
"SideValue2" => #{"TagNum" => "397" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "397" => #{"Name"=>"SideValue2" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "397"}


,
"NoBidDescriptors" => #{"TagNum" => "398" ,"Type" => "INT" ,"ValidValues" =>[]}
, "398" => #{"Name"=>"NoBidDescriptors" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "398"}


,
"BidDescriptorType" => #{"TagNum" => "399" ,"Type" => "INT" ,"ValidValues" =>[]}
, "399" => #{"Name"=>"BidDescriptorType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "399"}


,
"BidDescriptor" => #{"TagNum" => "400" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "400" => #{"Name"=>"BidDescriptor" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "400"}


,
"SideValueInd" => #{"TagNum" => "401" ,"Type" => "INT" ,"ValidValues" =>[]}
, "401" => #{"Name"=>"SideValueInd" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "401"}


,
"LiquidityPctLow" => #{"TagNum" => "402" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "402" => #{"Name"=>"LiquidityPctLow" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "402"}


,
"LiquidityPctHigh" => #{"TagNum" => "403" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "403" => #{"Name"=>"LiquidityPctHigh" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "403"}


,
"LiquidityValue" => #{"TagNum" => "404" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "404" => #{"Name"=>"LiquidityValue" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "404"}


,
"EFPTrackingError" => #{"TagNum" => "405" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "405" => #{"Name"=>"EFPTrackingError" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "405"}


,
"FairValue" => #{"TagNum" => "406" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "406" => #{"Name"=>"FairValue" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "406"}


,
"OutsideIndexPct" => #{"TagNum" => "407" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "407" => #{"Name"=>"OutsideIndexPct" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "407"}


,
"ValueOfFutures" => #{"TagNum" => "408" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "408" => #{"Name"=>"ValueOfFutures" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "408"}


,
"LiquidityIndType" => #{"TagNum" => "409" ,"Type" => "INT" ,"ValidValues" =>[{"1", "5_DAY_MOVING_AVERAGE"},{"2", "20_DAY_MOVING_AVERAGE"},{"3", "NORMAL_MARKET_SIZE"},{"4", "OTHER"}]}
, "409" => #{"Name"=>"LiquidityIndType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "5_DAY_MOVING_AVERAGE"},{"2", "20_DAY_MOVING_AVERAGE"},{"3", "NORMAL_MARKET_SIZE"},{"4", "OTHER"}], "TagNum" => "409"}


,
"WtAverageLiquidity" => #{"TagNum" => "410" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "410" => #{"Name"=>"WtAverageLiquidity" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "410"}


,
"ExchangeForPhysical" => #{"TagNum" => "411" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "411" => #{"Name"=>"ExchangeForPhysical" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "411"}


,
"OutMainCntryUIndex" => #{"TagNum" => "412" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "412" => #{"Name"=>"OutMainCntryUIndex" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "412"}


,
"CrossPercent" => #{"TagNum" => "413" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "413" => #{"Name"=>"CrossPercent" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "413"}


,
"ProgRptReqs" => #{"TagNum" => "414" ,"Type" => "INT" ,"ValidValues" =>[{"1", "BUYSIDE_EXPLICITLY_REQUESTS_STATUS_USING_STATUSREQUEST"},{"2", "SELLSIDE_PERIODICALLY_SENDS_STATUS_USING_LISTSTATUS_PERIOD_OPTIONALLY_SPECIFIED_IN_PROGRESSPERIOD"},{"3", "REAL_TIME_EXECUTION_REPORTS"}]}
, "414" => #{"Name"=>"ProgRptReqs" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "BUYSIDE_EXPLICITLY_REQUESTS_STATUS_USING_STATUSREQUEST"},{"2", "SELLSIDE_PERIODICALLY_SENDS_STATUS_USING_LISTSTATUS_PERIOD_OPTIONALLY_SPECIFIED_IN_PROGRESSPERIOD"},{"3", "REAL_TIME_EXECUTION_REPORTS"}], "TagNum" => "414"}


,
"ProgPeriodInterval" => #{"TagNum" => "415" ,"Type" => "INT" ,"ValidValues" =>[]}
, "415" => #{"Name"=>"ProgPeriodInterval" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "415"}


,
"IncTaxInd" => #{"TagNum" => "416" ,"Type" => "INT" ,"ValidValues" =>[{"1", "NET"},{"2", "GROSS"}]}
, "416" => #{"Name"=>"IncTaxInd" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "NET"},{"2", "GROSS"}], "TagNum" => "416"}


,
"NumBidders" => #{"TagNum" => "417" ,"Type" => "INT" ,"ValidValues" =>[]}
, "417" => #{"Name"=>"NumBidders" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "417"}


,
"TradeType" => #{"TagNum" => "418" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "AGENCY"},{"G", "VWAP_GUARANTEE"},{"J", "GUARANTEED_CLOSE"},{"R", "RISK_TRADE"}]}
, "418" => #{"Name"=>"TradeType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "AGENCY"},{"G", "VWAP_GUARANTEE"},{"J", "GUARANTEED_CLOSE"},{"R", "RISK_TRADE"}], "TagNum" => "418"}


,
"BasisPxType" => #{"TagNum" => "419" ,"Type" => "CHAR" ,"ValidValues" =>[{"2", "CLOSING_PRICE_AT_MORNING_SESSION"},{"3", "CLOSING_PRICE"},{"4", "CURRENT_PRICE"},{"5", "SQ"},{"6", "VWAP_THROUGH_A_DAY"},{"7", "VWAP_THROUGH_A_MORNING_SESSION"},{"8", "VWAP_THROUGH_AN_AFTERNOON_SESSION"},{"9", "VWAP_THROUGH_A_DAY_EXCEPT_YORI"},{"A", "VWAP_THROUGH_A_MORNING_SESSION_EXCEPT_YORI"},{"B", "VWAP_THROUGH_AN_AFTERNOON_SESSION_EXCEPT_YORI"},{"C", "STRIKE"},{"D", "OPEN"},{"Z", "OTHERS"}]}
, "419" => #{"Name"=>"BasisPxType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"2", "CLOSING_PRICE_AT_MORNING_SESSION"},{"3", "CLOSING_PRICE"},{"4", "CURRENT_PRICE"},{"5", "SQ"},{"6", "VWAP_THROUGH_A_DAY"},{"7", "VWAP_THROUGH_A_MORNING_SESSION"},{"8", "VWAP_THROUGH_AN_AFTERNOON_SESSION"},{"9", "VWAP_THROUGH_A_DAY_EXCEPT_YORI"},{"A", "VWAP_THROUGH_A_MORNING_SESSION_EXCEPT_YORI"},{"B", "VWAP_THROUGH_AN_AFTERNOON_SESSION_EXCEPT_YORI"},{"C", "STRIKE"},{"D", "OPEN"},{"Z", "OTHERS"}], "TagNum" => "419"}


,
"NoBidComponents" => #{"TagNum" => "420" ,"Type" => "INT" ,"ValidValues" =>[]}
, "420" => #{"Name"=>"NoBidComponents" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "420"}


,
"Country" => #{"TagNum" => "421" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "421" => #{"Name"=>"Country" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "421"}


,
"TotNoStrikes" => #{"TagNum" => "422" ,"Type" => "INT" ,"ValidValues" =>[]}
, "422" => #{"Name"=>"TotNoStrikes" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "422"}


,
"PriceType" => #{"TagNum" => "423" ,"Type" => "INT" ,"ValidValues" =>[{"1", "PERCENTAGE"},{"2", "PER_SHARE"},{"3", "FIXED_AMOUNT"}]}
, "423" => #{"Name"=>"PriceType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "PERCENTAGE"},{"2", "PER_SHARE"},{"3", "FIXED_AMOUNT"}], "TagNum" => "423"}


,
"DayOrderQty" => #{"TagNum" => "424" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "424" => #{"Name"=>"DayOrderQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "424"}


,
"DayCumQty" => #{"TagNum" => "425" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "425" => #{"Name"=>"DayCumQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "425"}


,
"DayAvgPx" => #{"TagNum" => "426" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "426" => #{"Name"=>"DayAvgPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "426"}


,
"GTBookingInst" => #{"TagNum" => "427" ,"Type" => "INT" ,"ValidValues" =>[{"0", "BOOK_OUT_ALL_TRADES_ON_DAY_OF_EXECUTION"},{"1", "ACCUMULATE_EXECUTIONS_UNTIL_ORDER_IS_FILLED_OR_EXPIRES"},{"2", "ACCUMULATE_UNTIL_VERBALLY_NOTIFIED_OTHERWISE"}]}
, "427" => #{"Name"=>"GTBookingInst" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "BOOK_OUT_ALL_TRADES_ON_DAY_OF_EXECUTION"},{"1", "ACCUMULATE_EXECUTIONS_UNTIL_ORDER_IS_FILLED_OR_EXPIRES"},{"2", "ACCUMULATE_UNTIL_VERBALLY_NOTIFIED_OTHERWISE"}], "TagNum" => "427"}


,
"NoStrikes" => #{"TagNum" => "428" ,"Type" => "INT" ,"ValidValues" =>[]}
, "428" => #{"Name"=>"NoStrikes" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "428"}


,
"ListStatusType" => #{"TagNum" => "429" ,"Type" => "INT" ,"ValidValues" =>[]}
, "429" => #{"Name"=>"ListStatusType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "429"}


,
"NetGrossInd" => #{"TagNum" => "430" ,"Type" => "INT" ,"ValidValues" =>[{"1", "NET"},{"2", "GROSS"}]}
, "430" => #{"Name"=>"NetGrossInd" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "NET"},{"2", "GROSS"}], "TagNum" => "430"}


,
"ListOrderStatus" => #{"TagNum" => "431" ,"Type" => "INT" ,"ValidValues" =>[]}
, "431" => #{"Name"=>"ListOrderStatus" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "431"}


,
"ExpireDate" => #{"TagNum" => "432" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "432" => #{"Name"=>"ExpireDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "432"}


,
"ListExecInstType" => #{"TagNum" => "433" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "IMMEDIATE"},{"2", "WAIT_FOR_EXECUTE_INSTRUCTION"}]}
, "433" => #{"Name"=>"ListExecInstType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "IMMEDIATE"},{"2", "WAIT_FOR_EXECUTE_INSTRUCTION"}], "TagNum" => "433"}


,
"CxlRejResponseTo" => #{"TagNum" => "434" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "ORDER_CANCEL_REQUEST"},{"2", "ORDER_CANCEL_REPLACE_REQUEST"}]}
, "434" => #{"Name"=>"CxlRejResponseTo" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "ORDER_CANCEL_REQUEST"},{"2", "ORDER_CANCEL_REPLACE_REQUEST"}], "TagNum" => "434"}


,
"UnderlyingCouponRate" => #{"TagNum" => "435" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "435" => #{"Name"=>"UnderlyingCouponRate" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "435"}


,
"UnderlyingContractMultiplier" => #{"TagNum" => "436" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "436" => #{"Name"=>"UnderlyingContractMultiplier" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "436"}


,
"ContraTradeQty" => #{"TagNum" => "437" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "437" => #{"Name"=>"ContraTradeQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "437"}


,
"ContraTradeTime" => #{"TagNum" => "438" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "438" => #{"Name"=>"ContraTradeTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "438"}


,
"ClearingFirm" => #{"TagNum" => "439" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "439" => #{"Name"=>"ClearingFirm" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "439"}


,
"ClearingAccount" => #{"TagNum" => "440" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "440" => #{"Name"=>"ClearingAccount" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "440"}


,
"LiquidityNumSecurities" => #{"TagNum" => "441" ,"Type" => "INT" ,"ValidValues" =>[]}
, "441" => #{"Name"=>"LiquidityNumSecurities" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "441"}


,
"MultiLegReportingType" => #{"TagNum" => "442" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "SINGLE_SECURITY"},{"2", "INDIVIDUAL_LEG_OF_A_MULTI_LEG_SECURITY"},{"3", "MULTI_LEG_SECURITY"}]}
, "442" => #{"Name"=>"MultiLegReportingType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "SINGLE_SECURITY"},{"2", "INDIVIDUAL_LEG_OF_A_MULTI_LEG_SECURITY"},{"3", "MULTI_LEG_SECURITY"}], "TagNum" => "442"}


,
"StrikeTime" => #{"TagNum" => "443" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "443" => #{"Name"=>"StrikeTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "443"}


,
"ListStatusText" => #{"TagNum" => "444" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "444" => #{"Name"=>"ListStatusText" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "444"}


,
"EncodedListStatusTextLen" => #{"TagNum" => "445" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "445" => #{"Name"=>"EncodedListStatusTextLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "445"}


,
"EncodedListStatusText" => #{"TagNum" => "446" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "446" => #{"Name"=>"EncodedListStatusText" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "446"}


}.



components() ->
 #{}.



groups() ->
 #{}.



header()->
#{
"Fields" => #{"BeginString" =>#{"Required" => "Y", "Sequence" => undefined}
,"BodyLength" =>#{"Required" => "Y", "Sequence" => undefined}
,"MsgType" =>#{"Required" => "Y", "Sequence" => undefined}
,"SenderCompID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TargetCompID" =>#{"Required" => "Y", "Sequence" => undefined}
,"OnBehalfOfCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeliverToCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecureDataLen" =>#{"Required" => "N", "Sequence" => undefined}
,"SecureData" =>#{"Required" => "N", "Sequence" => undefined}
,"MsgSeqNum" =>#{"Required" => "Y", "Sequence" => undefined}
,"SenderSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SenderLocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetLocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"OnBehalfOfSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"OnBehalfOfLocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeliverToSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeliverToLocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"PossDupFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"PossResend" =>#{"Required" => "N", "Sequence" => undefined}
,"SendingTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigSendingTime" =>#{"Required" => "N", "Sequence" => undefined}
,"XmlDataLen" =>#{"Required" => "N", "Sequence" => undefined}
,"XmlData" =>#{"Required" => "N", "Sequence" => undefined}
,"MessageEncoding" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMsgSeqNumProcessed" =>#{"Required" => "N", "Sequence" => undefined}
,"OnBehalfOfSendingTime" =>#{"Required" => "N", "Sequence" => undefined}}, 
"Groups" => #{}, 
 "Components" => #{}

}.



trailer()->
#{
"Fields" => #{"SignatureLength" =>#{"Required" => "N", "Sequence" => undefined}
,"Signature" =>#{"Required" => "N", "Sequence" => undefined}
,"CheckSum" =>#{"Required" => "Y", "Sequence" => undefined}}, 
"Groups" => #{}, 
 "Components" => #{}

}.


-module(fix43).
-export([messages/0,
         fields/0,
         components/0,
         groups/0,
         header/0,
         trailer/0]).

messages() ->
 #{"Heartbeat" => #{
                              "Category" => "admin"
                              ,"Type" => "0"
                              ,"Fields" => #{"TestReqID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"0" => #{"Category"=>"admin"
           ,"Name" => "Heartbeat"}

,"TestRequest" => #{
                              "Category" => "admin"
                              ,"Type" => "1"
                              ,"Fields" => #{"TestReqID" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"1" => #{"Category"=>"admin"
           ,"Name" => "TestRequest"}

,"ResendRequest" => #{
                              "Category" => "admin"
                              ,"Type" => "2"
                              ,"Fields" => #{"BeginSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}
,"EndSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"2" => #{"Category"=>"admin"
           ,"Name" => "ResendRequest"}

,"Reject" => #{
                              "Category" => "admin"
                              ,"Type" => "3"
                              ,"Fields" => #{"RefSeqNum" =>#{"Required" => "Y", "Sequence" => undefined}
,"RefTagID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefMsgType" =>#{"Required" => "N", "Sequence" => undefined}
,"SessionRejectReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"3" => #{"Category"=>"admin"
           ,"Name" => "Reject"}

,"SequenceReset" => #{
                              "Category" => "admin"
                              ,"Type" => "4"
                              ,"Fields" => #{"GapFillFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"NewSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"4" => #{"Category"=>"admin"
           ,"Name" => "SequenceReset"}

,"Logout" => #{
                              "Category" => "admin"
                              ,"Type" => "5"
                              ,"Fields" => #{"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"5" => #{"Category"=>"admin"
           ,"Name" => "Logout"}

,"IOI" => #{
                              "Category" => "app"
                              ,"Type" => "6"
                              ,"Fields" => #{"IOIid" =>#{"Required" => "Y", "Sequence" => undefined}
,"IOITransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"IOIRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuantityType" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIQltyInd" =>#{"Required" => "N", "Sequence" => undefined}
,"IOINaturalFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIQualifier" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"URLLink" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingType" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingID" =>#{"Required" => "N", "Sequence" => undefined}
,"Benchmark" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoIOIQualifiers" => #{
                              "Fields" => #{"IOIQualifier" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoRoutingIDs" => #{
                              "Fields" => #{"RoutingType" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"6" => #{"Category"=>"app"
           ,"Name" => "IOI"}

,"Advertisement" => #{
                              "Category" => "app"
                              ,"Type" => "7"
                              ,"Fields" => #{"AdvId" =>#{"Required" => "Y", "Sequence" => undefined}
,"AdvTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"AdvRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"AdvSide" =>#{"Required" => "Y", "Sequence" => undefined}
,"Quantity" =>#{"Required" => "Y", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"URLLink" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"7" => #{"Category"=>"app"
           ,"Name" => "Advertisement"}

,"ExecutionReport" => #{
                              "Category" => "app"
                              ,"Type" => "8"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTrader" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTradeQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTradeTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraLegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"CrossID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigCrossID" =>#{"Required" => "N", "Sequence" => undefined}
,"CrossType" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExecRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecType" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"WorkingIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecRestatementReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuantityType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"PegDifference" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInst" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionOffset" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"Rule80A" =>#{"Required" => "N", "Sequence" => undefined}
,"LastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"LastForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"LastCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"LeavesQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"CumQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"DayOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"DayCumQty" =>#{"Required" => "N", "Sequence" => undefined}
,"DayAvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ReportToExch" =>#{"Required" => "N", "Sequence" => undefined}
,"GrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"NumDaysInterest" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradedFlatSwitch" =>#{"Required" => "N", "Sequence" => undefined}
,"BasisFeatureDate" =>#{"Required" => "N", "Sequence" => undefined}
,"BasisFeaturePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"Concession" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalTakedown" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"LastForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"MultiLegReportingType" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}
,"TransBkdTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecValuationPoint" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecPriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecPriceAdjustment" =>#{"Required" => "N", "Sequence" => undefined}
,"PriorityIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceImprovement" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtType" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtValue" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"LegFutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegLastPx" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoContraBrokers" => #{
                              "Fields" => #{"ContraBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTrader" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTradeQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTradeTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraLegRefID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoContAmts" => #{
                              "Fields" => #{"ContAmtType" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtValue" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtCurr" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoLegs" => #{
                              "Fields" => #{"LegPositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"LegFutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegLastPx" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"8" => #{"Category"=>"app"
           ,"Name" => "ExecutionReport"}

,"OrderCancelReject" => #{
                              "Category" => "app"
                              ,"Type" => "9"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"WorkingIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigOrdModTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"CxlRejResponseTo" =>#{"Required" => "Y", "Sequence" => undefined}
,"CxlRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"9" => #{"Category"=>"app"
           ,"Name" => "OrderCancelReject"}

,"Logon" => #{
                              "Category" => "admin"
                              ,"Type" => "A"
                              ,"Fields" => #{"EncryptMethod" =>#{"Required" => "Y", "Sequence" => undefined}
,"HeartBtInt" =>#{"Required" => "Y", "Sequence" => undefined}
,"RawDataLength" =>#{"Required" => "N", "Sequence" => undefined}
,"RawData" =>#{"Required" => "N", "Sequence" => undefined}
,"ResetSeqNumFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxMessageSize" =>#{"Required" => "N", "Sequence" => undefined}
,"RefMsgType" =>#{"Required" => "N", "Sequence" => undefined}
,"MsgDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"TestMessageIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"Username" =>#{"Required" => "N", "Sequence" => undefined}
,"Password" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMsgTypes" => #{
                              "Fields" => #{"RefMsgType" =>#{"Required" => "N", "Sequence" => undefined}
,"MsgDirection" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"A" => #{"Category"=>"admin"
           ,"Name" => "Logon"}

,"News" => #{
                              "Category" => "app"
                              ,"Type" => "B"
                              ,"Fields" => #{"OrigTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Urgency" =>#{"Required" => "N", "Sequence" => undefined}
,"Headline" =>#{"Required" => "Y", "Sequence" => undefined}
,"EncodedHeadlineLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedHeadline" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingType" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "Y", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"URLLink" =>#{"Required" => "N", "Sequence" => undefined}
,"RawDataLength" =>#{"Required" => "N", "Sequence" => undefined}
,"RawData" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRoutingIDs" => #{
                              "Fields" => #{"RoutingType" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoRelatedSym" => #{
                              "Fields" => #{}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}

,
                              "LinesOfText" => #{
                              "Fields" => #{"Text" =>#{"Required" => "Y", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"B" => #{"Category"=>"app"
           ,"Name" => "News"}

,"Email" => #{
                              "Category" => "app"
                              ,"Type" => "C"
                              ,"Fields" => #{"EmailThreadID" =>#{"Required" => "Y", "Sequence" => undefined}
,"EmailType" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Subject" =>#{"Required" => "Y", "Sequence" => undefined}
,"EncodedSubjectLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSubject" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingType" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "Y", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"RawDataLength" =>#{"Required" => "N", "Sequence" => undefined}
,"RawData" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRoutingIDs" => #{
                              "Fields" => #{"RoutingType" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoRelatedSym" => #{
                              "Fields" => #{}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}

,
                              "LinesOfText" => #{
                              "Fields" => #{"Text" =>#{"Required" => "Y", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"C" => #{"Category"=>"app"
           ,"Name" => "Email"}

,"NewOrderSingle" => #{
                              "Category" => "app"
                              ,"Type" => "D"
                              ,"Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuantityType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIid" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"Rule80A" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"PegDifference" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInst" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionOffset" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

,
                              "NoTradingSessions" => #{
                              "Fields" => #{"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"D" => #{"Category"=>"app"
           ,"Name" => "NewOrderSingle"}

,"NewOrderList" => #{
                              "Category" => "app"
                              ,"Type" => "E"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"BidID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientBidID" =>#{"Required" => "N", "Sequence" => undefined}
,"ProgRptReqs" =>#{"Required" => "N", "Sequence" => undefined}
,"BidType" =>#{"Required" => "Y", "Sequence" => undefined}
,"ProgPeriodInterval" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListExecInstType" =>#{"Required" => "N", "Sequence" => undefined}
,"ListExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedListExecInstLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedListExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoOrders" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstMode" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"SideValueInd" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"QuantityType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIid" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"Rule80A" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"PegDifference" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInst" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionOffset" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoOrders" => #{
                              "Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstMode" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"SideValueInd" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"QuantityType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIid" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"Rule80A" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"PegDifference" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInst" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionOffset" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

,
                              "NoTradingSessions" => #{
                              "Fields" => #{"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}
}
,"E" => #{"Category"=>"app"
           ,"Name" => "NewOrderList"}

,"OrderCancelRequest" => #{
                              "Category" => "app"
                              ,"Type" => "F"
                              ,"Fields" => #{"OrigClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigOrdModTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"F" => #{"Category"=>"app"
           ,"Name" => "OrderCancelRequest"}

,"OrderCancelReplaceRequest" => #{
                              "Category" => "app"
                              ,"Type" => "G"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigOrdModTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuantityType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"PegDifference" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInst" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionOffset" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"Rule80A" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

,
                              "NoTradingSessions" => #{
                              "Fields" => #{"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"G" => #{"Category"=>"app"
           ,"Name" => "OrderCancelReplaceRequest"}

,"OrderStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "H"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"H" => #{"Category"=>"app"
           ,"Name" => "OrderStatusRequest"}

,"Allocation" => #{
                              "Category" => "app"
                              ,"Type" => "J"
                              ,"Fields" => #{"AllocID" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocType" =>#{"Required" => "Y", "Sequence" => undefined}
,"RefAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocLinkType" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"LastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"Quantity" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPrxPrecision" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"GrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"Concession" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalTakedown" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"NumDaysInterest" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"LegalConfirm" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"NotifyBrokerOfCredit" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocHandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocNetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstMode" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoOrders" => #{
                              "Fields" => #{"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoExecs" => #{
                              "Fields" => #{"LastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastCapacity" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"NotifyBrokerOfCredit" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocHandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocNetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstMode" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMiscFees" => #{
                              "Fields" => #{"MiscFeeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"J" => #{"Category"=>"app"
           ,"Name" => "Allocation"}

,"ListCancelRequest" => #{
                              "Category" => "app"
                              ,"Type" => "K"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"K" => #{"Category"=>"app"
           ,"Name" => "ListCancelRequest"}

,"ListExecute" => #{
                              "Category" => "app"
                              ,"Type" => "L"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClientBidID" =>#{"Required" => "N", "Sequence" => undefined}
,"BidID" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"L" => #{"Category"=>"app"
           ,"Name" => "ListExecute"}

,"ListStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "M"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"M" => #{"Category"=>"app"
           ,"Name" => "ListStatusRequest"}

,"ListStatus" => #{
                              "Category" => "app"
                              ,"Type" => "N"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListStatusType" =>#{"Required" => "Y", "Sequence" => undefined}
,"NoRpts" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListOrderStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"RptSeq" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListStatusText" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedListStatusTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedListStatusText" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoOrders" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"CumQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"WorkingIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"LeavesQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"CxlQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoOrders" => #{
                              "Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"CumQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"WorkingIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"LeavesQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"CxlQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"N" => #{"Category"=>"app"
           ,"Name" => "ListStatus"}

,"AllocationAck" => #{
                              "Category" => "app"
                              ,"Type" => "P"
                              ,"Fields" => #{"AllocID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocRejCode" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"LegalConfirm" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"P" => #{"Category"=>"app"
           ,"Name" => "AllocationAck"}

,"DontKnowTrade" => #{
                              "Category" => "app"
                              ,"Type" => "Q"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "Y", "Sequence" => undefined}
,"DKReason" =>#{"Required" => "Y", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"Q" => #{"Category"=>"app"
           ,"Name" => "DontKnowTrade"}

,"QuoteRequest" => #{
                              "Category" => "app"
                              ,"Type" => "R"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"RFQReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"QuantityType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"QuantityType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}
}
,"R" => #{"Category"=>"app"
           ,"Name" => "QuoteRequest"}

,"Quote" => #{
                              "Category" => "app"
                              ,"Type" => "S"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteResponseLevel" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MktBidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MktOfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MinBidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"MinOfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"MidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"MidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferYield" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrBidFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrOfferFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"S" => #{"Category"=>"app"
           ,"Name" => "Quote"}

,"SettlementInstructions" => #{
                              "Category" => "app"
                              ,"Type" => "T"
                              ,"Fields" => #{"SettlInstID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlInstTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlInstRefID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlInstMode" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlInstSource" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "Y", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"StandInstDbType" =>#{"Required" => "N", "Sequence" => undefined}
,"StandInstDbName" =>#{"Required" => "N", "Sequence" => undefined}
,"StandInstDbID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDeliveryType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDepositoryCode" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlBrkrCode" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstCode" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySettlAgentName" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySettlAgentCode" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySettlAgentAcctNum" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySettlAgentAcctName" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySettlAgentContactName" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySettlAgentContactPhone" =>#{"Required" => "N", "Sequence" => undefined}
,"CashSettlAgentName" =>#{"Required" => "N", "Sequence" => undefined}
,"CashSettlAgentCode" =>#{"Required" => "N", "Sequence" => undefined}
,"CashSettlAgentAcctNum" =>#{"Required" => "N", "Sequence" => undefined}
,"CashSettlAgentAcctName" =>#{"Required" => "N", "Sequence" => undefined}
,"CashSettlAgentContactName" =>#{"Required" => "N", "Sequence" => undefined}
,"CashSettlAgentContactPhone" =>#{"Required" => "N", "Sequence" => undefined}
,"PaymentMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"PaymentRef" =>#{"Required" => "N", "Sequence" => undefined}
,"CardHolderName" =>#{"Required" => "N", "Sequence" => undefined}
,"CardNumber" =>#{"Required" => "N", "Sequence" => undefined}
,"CardStartDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CardExpDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CardIssNo" =>#{"Required" => "N", "Sequence" => undefined}
,"PaymentDate" =>#{"Required" => "N", "Sequence" => undefined}
,"PaymentRemitterID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"T" => #{"Category"=>"app"
           ,"Name" => "SettlementInstructions"}

,"MarketDataRequest" => #{
                              "Category" => "app"
                              ,"Type" => "V"
                              ,"Fields" => #{"MDReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MarketDepth" =>#{"Required" => "Y", "Sequence" => undefined}
,"MDUpdateType" =>#{"Required" => "N", "Sequence" => undefined}
,"AggregatedBook" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenCloseSettleFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"Scope" =>#{"Required" => "N", "Sequence" => undefined}
,"MDImplicitDelete" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryType" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMDEntryTypes" => #{
                              "Fields" => #{"MDEntryType" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoRelatedSym" => #{
                              "Fields" => #{}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
}
}

,
                              "NoTradingSessions" => #{
                              "Fields" => #{"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"V" => #{"Category"=>"app"
           ,"Name" => "MarketDataRequest"}

,"MarketDataSnapshotFullRefresh" => #{
                              "Category" => "app"
                              ,"Type" => "W"
                              ,"Fields" => #{"MDReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancialStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalVolumeTraded" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalVolumeTradedDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalVolumeTradedTime" =>#{"Required" => "N", "Sequence" => undefined}
,"NetChgPrevDay" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MDEntryPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySize" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TickDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"MDMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryOriginator" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenCloseSettleFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"SellerDays" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryBuyer" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySeller" =>#{"Required" => "N", "Sequence" => undefined}
,"NumberOfOrders" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPositionNo" =>#{"Required" => "N", "Sequence" => undefined}
,"Scope" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMDEntries" => #{
                              "Fields" => #{"MDEntryType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MDEntryPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySize" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TickDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"MDMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryOriginator" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenCloseSettleFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"SellerDays" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryBuyer" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySeller" =>#{"Required" => "N", "Sequence" => undefined}
,"NumberOfOrders" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPositionNo" =>#{"Required" => "N", "Sequence" => undefined}
,"Scope" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"W" => #{"Category"=>"app"
           ,"Name" => "MarketDataSnapshotFullRefresh"}

,"MarketDataIncrementalRefresh" => #{
                              "Category" => "app"
                              ,"Type" => "X"
                              ,"Fields" => #{"MDReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDUpdateAction" =>#{"Required" => "Y", "Sequence" => undefined}
,"DeleteReason" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryType" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancialStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySize" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TickDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"MDMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryOriginator" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenCloseSettleFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"SellerDays" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryBuyer" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySeller" =>#{"Required" => "N", "Sequence" => undefined}
,"NumberOfOrders" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPositionNo" =>#{"Required" => "N", "Sequence" => undefined}
,"Scope" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalVolumeTraded" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalVolumeTradedDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalVolumeTradedTime" =>#{"Required" => "N", "Sequence" => undefined}
,"NetChgPrevDay" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMDEntries" => #{
                              "Fields" => #{"MDUpdateAction" =>#{"Required" => "Y", "Sequence" => undefined}
,"DeleteReason" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryType" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancialStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySize" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TickDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"MDMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryOriginator" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenCloseSettleFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"SellerDays" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryBuyer" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySeller" =>#{"Required" => "N", "Sequence" => undefined}
,"NumberOfOrders" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPositionNo" =>#{"Required" => "N", "Sequence" => undefined}
,"Scope" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalVolumeTraded" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalVolumeTradedDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalVolumeTradedTime" =>#{"Required" => "N", "Sequence" => undefined}
,"NetChgPrevDay" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}
}
,"X" => #{"Category"=>"app"
           ,"Name" => "MarketDataIncrementalRefresh"}

,"MarketDataRequestReject" => #{
                              "Category" => "app"
                              ,"Type" => "Y"
                              ,"Fields" => #{"MDReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"MDReqRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"Y" => #{"Category"=>"app"
           ,"Name" => "MarketDataRequestReject"}

,"QuoteCancel" => #{
                              "Category" => "app"
                              ,"Type" => "Z"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteCancelType" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteResponseLevel" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoQuoteEntries" => #{
                              "Fields" => #{}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"Z" => #{"Category"=>"app"
           ,"Name" => "QuoteCancel"}

,"QuoteStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "a"
                              ,"Fields" => #{"QuoteStatusReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"a" => #{"Category"=>"app"
           ,"Name" => "QuoteStatusRequest"}

,"MassQuoteAcknowledgement" => #{
                              "Category" => "app"
                              ,"Type" => "b"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteRejectReason" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteResponseLevel" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteSetID" =>#{"Required" => "N", "Sequence" => undefined}
,"TotQuoteEntries" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"MidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"MidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferYield" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryRejectReason" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoQuoteSets" => #{
                              "Fields" => #{"QuoteSetID" =>#{"Required" => "N", "Sequence" => undefined}
,"TotQuoteEntries" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"MidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"MidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferYield" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryRejectReason" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoQuoteEntries" => #{
                              "Fields" => #{"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"MidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"MidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferYield" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryRejectReason" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"b" => #{"Category"=>"app"
           ,"Name" => "MassQuoteAcknowledgement"}

,"SecurityDefinitionRequest" => #{
                              "Category" => "app"
                              ,"Type" => "c"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{"LegCurrency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"c" => #{"Category"=>"app"
           ,"Name" => "SecurityDefinitionRequest"}

,"SecurityDefinition" => #{
                              "Category" => "app"
                              ,"Type" => "d"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityResponseID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityResponseType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"RoundLot" =>#{"Required" => "N", "Sequence" => undefined}
,"MinTradeVol" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{"LegCurrency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"d" => #{"Category"=>"app"
           ,"Name" => "SecurityDefinition"}

,"SecurityStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "e"
                              ,"Fields" => #{"SecurityStatusReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"e" => #{"Category"=>"app"
           ,"Name" => "SecurityStatusRequest"}

,"SecurityStatus" => #{
                              "Category" => "app"
                              ,"Type" => "f"
                              ,"Fields" => #{"SecurityStatusReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnsolicitedIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityTradingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancialStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"HaltReasonChar" =>#{"Required" => "N", "Sequence" => undefined}
,"InViewOfCommon" =>#{"Required" => "N", "Sequence" => undefined}
,"DueToRelated" =>#{"Required" => "N", "Sequence" => undefined}
,"BuyVolume" =>#{"Required" => "N", "Sequence" => undefined}
,"SellVolume" =>#{"Required" => "N", "Sequence" => undefined}
,"HighPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LowPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Adjustment" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"f" => #{"Category"=>"app"
           ,"Name" => "SecurityStatus"}

,"TradingSessionStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "g"
                              ,"Fields" => #{"TradSesReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesMode" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"g" => #{"Category"=>"app"
           ,"Name" => "TradingSessionStatusRequest"}

,"TradingSessionStatus" => #{
                              "Category" => "app"
                              ,"Type" => "h"
                              ,"Fields" => #{"TradSesReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesMode" =>#{"Required" => "N", "Sequence" => undefined}
,"UnsolicitedIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradSesStatusRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesStartTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesOpenTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesPreCloseTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesCloseTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesEndTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalVolumeTraded" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"h" => #{"Category"=>"app"
           ,"Name" => "TradingSessionStatus"}

,"MassQuote" => #{
                              "Category" => "app"
                              ,"Type" => "i"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteResponseLevel" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DefBidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"DefOfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteSetID" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteSetValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TotQuoteEntries" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "Y", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"MidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"MidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferYield" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoQuoteSets" => #{
                              "Fields" => #{"QuoteSetID" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteSetValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TotQuoteEntries" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "Y", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"MidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"MidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferYield" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoQuoteEntries" => #{
                              "Fields" => #{"QuoteEntryID" =>#{"Required" => "Y", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"MidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"MidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferYield" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"i" => #{"Category"=>"app"
           ,"Name" => "MassQuote"}

,"BusinessMessageReject" => #{
                              "Category" => "app"
                              ,"Type" => "j"
                              ,"Fields" => #{"RefSeqNum" =>#{"Required" => "N", "Sequence" => undefined}
,"RefMsgType" =>#{"Required" => "Y", "Sequence" => undefined}
,"BusinessRejectRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"BusinessRejectReason" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"j" => #{"Category"=>"app"
           ,"Name" => "BusinessMessageReject"}

,"BidRequest" => #{
                              "Category" => "app"
                              ,"Type" => "k"
                              ,"Fields" => #{"BidID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientBidID" =>#{"Required" => "Y", "Sequence" => undefined}
,"BidRequestTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListName" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalNumSecurities" =>#{"Required" => "Y", "Sequence" => undefined}
,"BidType" =>#{"Required" => "Y", "Sequence" => undefined}
,"NumTickets" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"SideValue1" =>#{"Required" => "N", "Sequence" => undefined}
,"SideValue2" =>#{"Required" => "N", "Sequence" => undefined}
,"BidDescriptorType" =>#{"Required" => "N", "Sequence" => undefined}
,"BidDescriptor" =>#{"Required" => "N", "Sequence" => undefined}
,"SideValueInd" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityValue" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityNumSecurities" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityPctLow" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityPctHigh" =>#{"Required" => "N", "Sequence" => undefined}
,"EFPTrackingError" =>#{"Required" => "N", "Sequence" => undefined}
,"FairValue" =>#{"Required" => "N", "Sequence" => undefined}
,"OutsideIndexPct" =>#{"Required" => "N", "Sequence" => undefined}
,"ValueOfFutures" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"NetGrossInd" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityIndType" =>#{"Required" => "N", "Sequence" => undefined}
,"WtAverageLiquidity" =>#{"Required" => "N", "Sequence" => undefined}
,"ExchangeForPhysical" =>#{"Required" => "N", "Sequence" => undefined}
,"OutMainCntryUIndex" =>#{"Required" => "N", "Sequence" => undefined}
,"CrossPercent" =>#{"Required" => "N", "Sequence" => undefined}
,"ProgRptReqs" =>#{"Required" => "N", "Sequence" => undefined}
,"ProgPeriodInterval" =>#{"Required" => "N", "Sequence" => undefined}
,"IncTaxInd" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"NumBidders" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeType" =>#{"Required" => "Y", "Sequence" => undefined}
,"BasisPxType" =>#{"Required" => "Y", "Sequence" => undefined}
,"StrikeTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoBidDescriptors" => #{
                              "Fields" => #{"BidDescriptorType" =>#{"Required" => "N", "Sequence" => undefined}
,"BidDescriptor" =>#{"Required" => "N", "Sequence" => undefined}
,"SideValueInd" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityValue" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityNumSecurities" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityPctLow" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityPctHigh" =>#{"Required" => "N", "Sequence" => undefined}
,"EFPTrackingError" =>#{"Required" => "N", "Sequence" => undefined}
,"FairValue" =>#{"Required" => "N", "Sequence" => undefined}
,"OutsideIndexPct" =>#{"Required" => "N", "Sequence" => undefined}
,"ValueOfFutures" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoBidComponents" => #{
                              "Fields" => #{"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"NetGrossInd" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"k" => #{"Category"=>"app"
           ,"Name" => "BidRequest"}

,"BidResponse" => #{
                              "Category" => "app"
                              ,"Type" => "l"
                              ,"Fields" => #{"BidID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientBidID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Country" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"FairValue" =>#{"Required" => "N", "Sequence" => undefined}
,"NetGrossInd" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoBidComponents" => #{
                              "Fields" => #{"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Country" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"FairValue" =>#{"Required" => "N", "Sequence" => undefined}
,"NetGrossInd" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"CommissionData" =>#{"Required" => "Y", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}
}
,"l" => #{"Category"=>"app"
           ,"Name" => "BidResponse"}

,"ListStrikePrice" => #{
                              "Category" => "app"
                              ,"Type" => "m"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TotNoStrikes" =>#{"Required" => "Y", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoStrikes" => #{
                              "Fields" => #{"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}
}
,"m" => #{"Category"=>"app"
           ,"Name" => "ListStrikePrice"}

,"XMLnonFIX" => #{
                              "Category" => "admin"
                              ,"Type" => "n"
                              ,"Fields" => #{}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"n" => #{"Category"=>"admin"
           ,"Name" => "XMLnonFIX"}

,"RegistrationInstructions" => #{
                              "Category" => "app"
                              ,"Type" => "o"
                              ,"Fields" => #{"RegistID" =>#{"Required" => "Y", "Sequence" => undefined}
,"RegistTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"RegistRefID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistAcctType" =>#{"Required" => "N", "Sequence" => undefined}
,"TaxAdvantageType" =>#{"Required" => "N", "Sequence" => undefined}
,"OwnershipType" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistDetls" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistEmail" =>#{"Required" => "N", "Sequence" => undefined}
,"MailingDtls" =>#{"Required" => "N", "Sequence" => undefined}
,"MailingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OwnerType" =>#{"Required" => "N", "Sequence" => undefined}
,"DateOfBirth" =>#{"Required" => "N", "Sequence" => undefined}
,"InvestorCountryOfResidence" =>#{"Required" => "N", "Sequence" => undefined}
,"DistribPaymentMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"DistribPercentage" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribAgentName" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribAgentCode" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribAgentAcctNumber" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribPayRef" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRegistDtls" => #{
                              "Fields" => #{"RegistDetls" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistEmail" =>#{"Required" => "N", "Sequence" => undefined}
,"MailingDtls" =>#{"Required" => "N", "Sequence" => undefined}
,"MailingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OwnerType" =>#{"Required" => "N", "Sequence" => undefined}
,"DateOfBirth" =>#{"Required" => "N", "Sequence" => undefined}
,"InvestorCountryOfResidence" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

,
                              "NoDistribInsts" => #{
                              "Fields" => #{"DistribPaymentMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"DistribPercentage" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribAgentName" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribAgentCode" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribAgentAcctNumber" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribPayRef" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"o" => #{"Category"=>"app"
           ,"Name" => "RegistrationInstructions"}

,"RegistrationInstructionsResponse" => #{
                              "Category" => "app"
                              ,"Type" => "p"
                              ,"Fields" => #{"RegistID" =>#{"Required" => "Y", "Sequence" => undefined}
,"RegistTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"RegistRefID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"RegistRejReasonCode" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistRejReasonText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"p" => #{"Category"=>"app"
           ,"Name" => "RegistrationInstructionsResponse"}

,"OrderMassCancelRequest" => #{
                              "Category" => "app"
                              ,"Type" => "q"
                              ,"Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"MassCancelRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"q" => #{"Category"=>"app"
           ,"Name" => "OrderMassCancelRequest"}

,"OrderMassCancelReport" => #{
                              "Category" => "app"
                              ,"Type" => "r"
                              ,"Fields" => #{"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"MassCancelRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MassCancelResponse" =>#{"Required" => "Y", "Sequence" => undefined}
,"MassCancelRejectReason" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalAffectedOrders" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"AffectedOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"AffectedSecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAffectedOrders" => #{
                              "Fields" => #{"OrigClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"AffectedOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"AffectedSecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"r" => #{"Category"=>"app"
           ,"Name" => "OrderMassCancelReport"}

,"NewOrderCross" => #{
                              "Category" => "app"
                              ,"Type" => "s"
                              ,"Fields" => #{"CrossID" =>#{"Required" => "Y", "Sequence" => undefined}
,"CrossType" =>#{"Required" => "Y", "Sequence" => undefined}
,"CrossPrioritization" =>#{"Required" => "Y", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}
,"QuantityType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"SideComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIid" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"PegDifference" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInst" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionOffset" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSides" => #{
                              "Fields" => #{"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}
,"QuantityType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"SideComplianceID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

,
                              "NoTradingSessions" => #{
                              "Fields" => #{"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"s" => #{"Category"=>"app"
           ,"Name" => "NewOrderCross"}

,"CrossOrderCancelRequest" => #{
                              "Category" => "app"
                              ,"Type" => "u"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"CrossID" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigCrossID" =>#{"Required" => "Y", "Sequence" => undefined}
,"CrossType" =>#{"Required" => "Y", "Sequence" => undefined}
,"CrossPrioritization" =>#{"Required" => "Y", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigOrdModTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSides" => #{
                              "Fields" => #{"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigOrdModTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"u" => #{"Category"=>"app"
           ,"Name" => "CrossOrderCancelRequest"}

,"CrossOrderCancelReplaceRequest" => #{
                              "Category" => "app"
                              ,"Type" => "t"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"CrossID" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigCrossID" =>#{"Required" => "Y", "Sequence" => undefined}
,"CrossType" =>#{"Required" => "Y", "Sequence" => undefined}
,"CrossPrioritization" =>#{"Required" => "Y", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigOrdModTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}
,"QuantityType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"SideComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIid" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"PegDifference" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInst" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionOffset" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSides" => #{
                              "Fields" => #{"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigOrdModTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}
,"QuantityType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"SideComplianceID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

,
                              "NoTradingSessions" => #{
                              "Fields" => #{"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"t" => #{"Category"=>"app"
           ,"Name" => "CrossOrderCancelReplaceRequest"}

,"SecurityTypeRequest" => #{
                              "Category" => "app"
                              ,"Type" => "v"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"v" => #{"Category"=>"app"
           ,"Name" => "SecurityTypeRequest"}

,"SecurityTypes" => #{
                              "Category" => "app"
                              ,"Type" => "w"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityResponseID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityResponseType" =>#{"Required" => "Y", "Sequence" => undefined}
,"TotalNumSecurityTypes" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"Product" =>#{"Required" => "N", "Sequence" => undefined}
,"CFICode" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSecurityTypes" => #{
                              "Fields" => #{"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"Product" =>#{"Required" => "N", "Sequence" => undefined}
,"CFICode" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"w" => #{"Category"=>"app"
           ,"Name" => "SecurityTypes"}

,"SecurityListRequest" => #{
                              "Category" => "app"
                              ,"Type" => "x"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityListRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"x" => #{"Category"=>"app"
           ,"Name" => "SecurityListRequest"}

,"SecurityList" => #{
                              "Category" => "app"
                              ,"Type" => "y"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityResponseID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityRequestResult" =>#{"Required" => "Y", "Sequence" => undefined}
,"TotalNumSecurities" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"RoundLot" =>#{"Required" => "N", "Sequence" => undefined}
,"MinTradeVol" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"RoundLot" =>#{"Required" => "N", "Sequence" => undefined}
,"MinTradeVol" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{"LegCurrency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}
}
,"y" => #{"Category"=>"app"
           ,"Name" => "SecurityList"}

,"DerivativeSecurityListRequest" => #{
                              "Category" => "app"
                              ,"Type" => "z"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityListRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"z" => #{"Category"=>"app"
           ,"Name" => "DerivativeSecurityListRequest"}

,"DerivativeSecurityList" => #{
                              "Category" => "app"
                              ,"Type" => "AA"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityResponseID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityRequestResult" =>#{"Required" => "Y", "Sequence" => undefined}
,"TotalNumSecurities" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{"LegCurrency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AA" => #{"Category"=>"app"
           ,"Name" => "DerivativeSecurityList"}

,"NewOrderMultileg" => #{
                              "Category" => "app"
                              ,"Type" => "AB"
                              ,"Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"LegFutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuantityType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIid" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"PegDifference" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInst" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionOffset" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}
,"MultiLegRptTypeReq" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoTradingSessions" => #{
                              "Fields" => #{"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoLegs" => #{
                              "Fields" => #{"LegPositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"LegFutSettDate" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AB" => #{"Category"=>"app"
           ,"Name" => "NewOrderMultileg"}

,"MultilegOrderCancelReplaceRequest" => #{
                              "Category" => "app"
                              ,"Type" => "AC"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigOrdModTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"LegFutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuantityType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIid" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"PegDifference" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInst" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionOffset" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}
,"MultiLegRptTypeReq" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoTradingSessions" => #{
                              "Fields" => #{"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoLegs" => #{
                              "Fields" => #{"LegPositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"LegFutSettDate" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AC" => #{"Category"=>"app"
           ,"Name" => "MultilegOrderCancelReplaceRequest"}

,"TradeCaptureReportRequest" => #{
                              "Category" => "app"
                              ,"Type" => "AD"
                              ,"Fields" => #{"TradeRequestID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputSource" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputDevice" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoDates" => #{
                              "Fields" => #{"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AD" => #{"Category"=>"app"
           ,"Name" => "TradeCaptureReportRequest"}

,"TradeCaptureReport" => #{
                              "Category" => "app"
                              ,"Type" => "AE"
                              ,"Fields" => #{"TradeReportID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeReportTransType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeRequestID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecType" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeReportRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecRestatementReason" =>#{"Required" => "N", "Sequence" => undefined}
,"PreviouslyReported" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"LastForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchType" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"OddLot" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingInstruction" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputSource" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputDevice" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"TransBkdTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"GrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"NumDaysInterest" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"Concession" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalTakedown" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"MultiLegReportingType" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtType" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtValue" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSides" => #{
                              "Fields" => #{"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"OddLot" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingInstruction" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputSource" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputDevice" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"TransBkdTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"GrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"NumDaysInterest" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"Concession" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalTakedown" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"MultiLegReportingType" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtType" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtValue" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoClearingInstructions" => #{
                              "Fields" => #{"ClearingInstruction" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoContAmts" => #{
                              "Fields" => #{"ContAmtType" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtValue" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtCurr" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

,
                              "NoMiscFees" => #{
                              "Fields" => #{"MiscFeeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AE" => #{"Category"=>"app"
           ,"Name" => "TradeCaptureReport"}

,"OrderMassStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "AF"
                              ,"Fields" => #{"MassStatusReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"MassStatusReqType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AF" => #{"Category"=>"app"
           ,"Name" => "OrderMassStatusRequest"}

,"QuoteRequestReject" => #{
                              "Category" => "app"
                              ,"Type" => "AG"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"RFQReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestRejectReason" =>#{"Required" => "Y", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"QuantityType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"QuantityType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlmntTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}
}
,"AG" => #{"Category"=>"app"
           ,"Name" => "QuoteRequestReject"}

,"RFQRequest" => #{
                              "Category" => "app"
                              ,"Type" => "AH"
                              ,"Fields" => #{"RFQReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}
}
,"AH" => #{"Category"=>"app"
           ,"Name" => "RFQRequest"}

,"QuoteStatusReport" => #{
                              "Category" => "app"
                              ,"Type" => "AI"
                              ,"Fields" => #{"QuoteStatusReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MktBidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MktOfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MinBidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"MinOfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"MidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"MidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferYield" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"FutSettDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrBidFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrOfferFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteStatus" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"AI" => #{"Category"=>"app"
           ,"Name" => "QuoteStatusReport"}
}.


fields() ->
#{
"Account" => #{"TagNum" => "1" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1" => #{"Name"=>"Account" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1"}


,
"AdvId" => #{"TagNum" => "2" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "2" => #{"Name"=>"AdvId" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "2"}


,
"AdvRefID" => #{"TagNum" => "3" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "3" => #{"Name"=>"AdvRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "3"}


,
"AdvSide" => #{"TagNum" => "4" ,"Type" => "CHAR" ,"ValidValues" =>[{"B", "BUY"},{"S", "SELL"},{"X", "CROSS"},{"T", "TRADE"}]}
, "4" => #{"Name"=>"AdvSide" ,"Type"=>"CHAR" ,"ValidValues"=>[{"B", "BUY"},{"S", "SELL"},{"X", "CROSS"},{"T", "TRADE"}], "TagNum" => "4"}


,
"AdvTransType" => #{"TagNum" => "5" ,"Type" => "STRING" ,"ValidValues" =>[{"N", "NEW"},{"C", "CANCEL"},{"R", "REPLACE"}]}
, "5" => #{"Name"=>"AdvTransType" ,"Type"=>"STRING" ,"ValidValues"=>[{"N", "NEW"},{"C", "CANCEL"},{"R", "REPLACE"}], "TagNum" => "5"}


,
"AvgPx" => #{"TagNum" => "6" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "6" => #{"Name"=>"AvgPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "6"}


,
"BeginSeqNo" => #{"TagNum" => "7" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "7" => #{"Name"=>"BeginSeqNo" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "7"}


,
"BeginString" => #{"TagNum" => "8" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "8" => #{"Name"=>"BeginString" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "8"}


,
"BodyLength" => #{"TagNum" => "9" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "9" => #{"Name"=>"BodyLength" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "9"}


,
"CheckSum" => #{"TagNum" => "10" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "10" => #{"Name"=>"CheckSum" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "10"}


,
"ClOrdID" => #{"TagNum" => "11" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "11" => #{"Name"=>"ClOrdID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "11"}


,
"Commission" => #{"TagNum" => "12" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "12" => #{"Name"=>"Commission" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "12"}


,
"CommType" => #{"TagNum" => "13" ,"Type" => "CHAR" ,"ValidValues" =>[{"6", "PER_BOND"},{"1", "PER_SHARE"},{"2", "PERCENTAGE"},{"3", "ABSOLUTE"},{"5", "5"},{"4", "4"}]}
, "13" => #{"Name"=>"CommType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"6", "PER_BOND"},{"1", "PER_SHARE"},{"2", "PERCENTAGE"},{"3", "ABSOLUTE"},{"5", "5"},{"4", "4"}], "TagNum" => "13"}


,
"CumQty" => #{"TagNum" => "14" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "14" => #{"Name"=>"CumQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "14"}


,
"Currency" => #{"TagNum" => "15" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "15" => #{"Name"=>"Currency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "15"}


,
"EndSeqNo" => #{"TagNum" => "16" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "16" => #{"Name"=>"EndSeqNo" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "16"}


,
"ExecID" => #{"TagNum" => "17" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "17" => #{"Name"=>"ExecID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "17"}


,
"ExecInst" => #{"TagNum" => "18" ,"Type" => "MULTIPLEVALUESTRING" ,"ValidValues" =>[{"Y", "TRYTOSTOP"},{"M", "MIDPRCPEG"},{"P", "MARKPEG"},{"Q", "CANCELONSYSFAIL"},{"R", "PRIMPEG"},{"S", "SUSPEND"},{"U", "CUSTDISPINST"},{"V", "NETTING"},{"W", "PEGVWAP"},{"X", "TRADEALONG"},{"D", "PERCVOL"},{"0", "STAYOFFER"},{"2", "WORK"},{"4", "OVERDAY"},{"5", "HELD"},{"6", "PARTNOTINIT"},{"7", "STRICTSCALE"},{"8", "TRYTOSCALE"},{"9", "STAYBID"},{"A", "NOCROSS"},{"O", "OPENPEG"},{"C", "CALLFIRST"},{"N", "NONNEGO"},{"E", "DNI"},{"F", "DNR"},{"G", "AON"},{"H", "RESTATEONSYSFAIL"},{"I", "INSTITONLY"},{"J", "RESTATEONTRADINGHALT"},{"K", "CANCELONTRADINGHALT"},{"L", "LASTPEG"},{"3", "GOALONG"},{"B", "OKCROSS"},{"1", "NOTHELD"}]}
, "18" => #{"Name"=>"ExecInst" ,"Type"=>"MULTIPLEVALUESTRING" ,"ValidValues"=>[{"Y", "TRYTOSTOP"},{"M", "MIDPRCPEG"},{"P", "MARKPEG"},{"Q", "CANCELONSYSFAIL"},{"R", "PRIMPEG"},{"S", "SUSPEND"},{"U", "CUSTDISPINST"},{"V", "NETTING"},{"W", "PEGVWAP"},{"X", "TRADEALONG"},{"D", "PERCVOL"},{"0", "STAYOFFER"},{"2", "WORK"},{"4", "OVERDAY"},{"5", "HELD"},{"6", "PARTNOTINIT"},{"7", "STRICTSCALE"},{"8", "TRYTOSCALE"},{"9", "STAYBID"},{"A", "NOCROSS"},{"O", "OPENPEG"},{"C", "CALLFIRST"},{"N", "NONNEGO"},{"E", "DNI"},{"F", "DNR"},{"G", "AON"},{"H", "RESTATEONSYSFAIL"},{"I", "INSTITONLY"},{"J", "RESTATEONTRADINGHALT"},{"K", "CANCELONTRADINGHALT"},{"L", "LASTPEG"},{"3", "GOALONG"},{"B", "OKCROSS"},{"1", "NOTHELD"}], "TagNum" => "18"}


,
"ExecRefID" => #{"TagNum" => "19" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "19" => #{"Name"=>"ExecRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "19"}


,
"HandlInst" => #{"TagNum" => "21" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "AUTOMATED_EXECUTION_ORDER_PRIVATE_NO_BROKER_INTERVENTION"},{"2", "AUTOMATED_EXECUTION_ORDER_PUBLIC_BROKER_INTERVENTION_OK"},{"3", "MANUAL_ORDER_BEST_EXECUTION"}]}
, "21" => #{"Name"=>"HandlInst" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "AUTOMATED_EXECUTION_ORDER_PRIVATE_NO_BROKER_INTERVENTION"},{"2", "AUTOMATED_EXECUTION_ORDER_PUBLIC_BROKER_INTERVENTION_OK"},{"3", "MANUAL_ORDER_BEST_EXECUTION"}], "TagNum" => "21"}


,
"SecurityIDSource" => #{"TagNum" => "22" ,"Type" => "STRING" ,"ValidValues" =>[{"E", "SICOVAM"},{"2", "SEDOL"},{"1", "CUSIP"},{"3", "QUIK"},{"F", "BELGIAN"},{"D", "VALOREN"},{"C", "DUTCH"},{"B", "WERTPAPIER"},{"A", "BLOOMBERG_SYMBOL"},{"9", "CONSOLIDATED_TAPE_ASSOCIATION"},{"8", "EXCHANGE_SYMBOL"},{"7", "ISO_COUNTRY_CODE"},{"6", "ISO_CURRENCY_CODE"},{"5", "RIC_CODE"},{"4", "ISIN_NUMBER"},{"G", "COMMON"}]}
, "22" => #{"Name"=>"SecurityIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[{"E", "SICOVAM"},{"2", "SEDOL"},{"1", "CUSIP"},{"3", "QUIK"},{"F", "BELGIAN"},{"D", "VALOREN"},{"C", "DUTCH"},{"B", "WERTPAPIER"},{"A", "BLOOMBERG_SYMBOL"},{"9", "CONSOLIDATED_TAPE_ASSOCIATION"},{"8", "EXCHANGE_SYMBOL"},{"7", "ISO_COUNTRY_CODE"},{"6", "ISO_CURRENCY_CODE"},{"5", "RIC_CODE"},{"4", "ISIN_NUMBER"},{"G", "COMMON"}], "TagNum" => "22"}


,
"IOIid" => #{"TagNum" => "23" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "23" => #{"Name"=>"IOIid" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "23"}


,
"IOIQltyInd" => #{"TagNum" => "25" ,"Type" => "CHAR" ,"ValidValues" =>[{"M", "MEDIUM"},{"H", "HIGH"},{"L", "LOW"}]}
, "25" => #{"Name"=>"IOIQltyInd" ,"Type"=>"CHAR" ,"ValidValues"=>[{"M", "MEDIUM"},{"H", "HIGH"},{"L", "LOW"}], "TagNum" => "25"}


,
"IOIRefID" => #{"TagNum" => "26" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "26" => #{"Name"=>"IOIRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "26"}


,
"IOIQty" => #{"TagNum" => "27" ,"Type" => "STRING" ,"ValidValues" =>[{"L", "LARGE"},{"M", "MEDIUM"},{"S", "SMALL"}]}
, "27" => #{"Name"=>"IOIQty" ,"Type"=>"STRING" ,"ValidValues"=>[{"L", "LARGE"},{"M", "MEDIUM"},{"S", "SMALL"}], "TagNum" => "27"}


,
"IOITransType" => #{"TagNum" => "28" ,"Type" => "CHAR" ,"ValidValues" =>[{"C", "CANCEL"},{"N", "NEW"},{"R", "REPLACE"}]}
, "28" => #{"Name"=>"IOITransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"C", "CANCEL"},{"N", "NEW"},{"R", "REPLACE"}], "TagNum" => "28"}


,
"LastCapacity" => #{"TagNum" => "29" ,"Type" => "CHAR" ,"ValidValues" =>[{"4", "PRINCIPAL"},{"3", "CROSS_AS_PRINCIPAL"},{"1", "AGENT"},{"2", "CROSS_AS_AGENT"}]}
, "29" => #{"Name"=>"LastCapacity" ,"Type"=>"CHAR" ,"ValidValues"=>[{"4", "PRINCIPAL"},{"3", "CROSS_AS_PRINCIPAL"},{"1", "AGENT"},{"2", "CROSS_AS_AGENT"}], "TagNum" => "29"}


,
"LastMkt" => #{"TagNum" => "30" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "30" => #{"Name"=>"LastMkt" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "30"}


,
"LastPx" => #{"TagNum" => "31" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "31" => #{"Name"=>"LastPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "31"}


,
"LastQty" => #{"TagNum" => "32" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "32" => #{"Name"=>"LastQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "32"}


,
"LinesOfText" => #{"TagNum" => "33" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "33" => #{"Name"=>"LinesOfText" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "33"}


,
"MsgSeqNum" => #{"TagNum" => "34" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "34" => #{"Name"=>"MsgSeqNum" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "34"}


,
"MsgType" => #{"TagNum" => "35" ,"Type" => "STRING" ,"ValidValues" =>[{"0", "HEARTBEAT"},{"1", "TEST_REQUEST"},{"2", "RESEND_REQUEST"},{"3", "REJECT"},{"4", "SEQUENCE_RESET"},{"5", "LOGOUT"},{"6", "INDICATION_OF_INTEREST"},{"7", "ADVERTISEMENT"},{"8", "EXECUTION_REPORT"},{"9", "ORDER_CANCEL_REJECT"},{"a", "QUOTE_STATUS_REQUEST"},{"A", "LOGON"},{"AA", "DERIVATIVE_SECURITY_LIST"},{"AB", "NEW_ORDER_AB"},{"AC", "MULTILEG_ORDER_CANCEL_REPLACE"},{"AD", "TRADE_CAPTURE_REPORT_REQUEST"},{"AE", "TRADE_CAPTURE_REPORT"},{"AF", "ORDER_MASS_STATUS_REQUEST"},{"AG", "QUOTE_REQUEST_REJECT"},{"AH", "RFQ_REQUEST"},{"AI", "QUOTE_STATUS_REPORT"},{"b", "MASS_QUOTE_ACKNOWLEDGEMENT"},{"B", "NEWS"},{"c", "SECURITY_DEFINITION_REQUEST"},{"C", "EMAIL"},{"d", "SECURITY_DEFINITION"},{"D", "ORDER_SINGLE"},{"e", "SECURITY_STATUS_REQUEST"},{"E", "ORDER_LIST"},{"f", "SECURITY_STATUS"},{"F", "ORDER_CANCEL_REQUEST"},{"G", "ORDER_CANCEL_REPLACE_REQUEST"},{"g", "TRADING_SESSION_STATUS_REQUEST"},{"h", "TRADING_SESSION_STATUS"},{"H", "ORDER_STATUS_REQUEST"},{"i", "MASS_QUOTE"},{"j", "BUSINESS_MESSAGE_REJECT"},{"J", "ALLOCATION"},{"K", "LIST_CANCEL_REQUEST"},{"k", "BID_REQUEST"},{"l", "BID_RESPONSE"},{"L", "LIST_EXECUTE"},{"m", "LIST_STRIKE_PRICE"},{"M", "LIST_STATUS_REQUEST"},{"N", "LIST_STATUS"},{"n", "XML_MESSAGE"},{"o", "REGISTRATION_INSTRUCTIONS"},{"P", "ALLOCATION_ACK"},{"p", "REGISTRATION_INSTRUCTIONS_RESPONSE"},{"q", "ORDER_MASS_CANCEL_REQUEST"},{"Q", "DONT_KNOW_TRADE"},{"r", "ORDER_MASS_CANCEL_REPORT"},{"R", "QUOTE_REQUEST"},{"s", "NEW_ORDER_s"},{"S", "QUOTE"},{"t", "CROSS_ORDER_CANCEL_REPLACE_REQUEST"},{"T", "SETTLEMENT_INSTRUCTIONS"},{"u", "CROSS_ORDER_CANCEL_REQUEST"},{"v", "SECURITY_TYPE_REQUEST"},{"V", "MARKET_DATA_REQUEST"},{"w", "SECURITY_TYPES"},{"W", "MARKET_DATA_SNAPSHOT_FULL_REFRESH"},{"x", "SECURITY_LIST_REQUEST"},{"X", "MARKET_DATA_INCREMENTAL_REFRESH"},{"y", "SECURITY_LIST"},{"Y", "MARKET_DATA_REQUEST_REJECT"},{"z", "DERIVATIVE_SECURITY_LIST_REQUEST"},{"Z", "QUOTE_CANCEL"}]}
, "35" => #{"Name"=>"MsgType" ,"Type"=>"STRING" ,"ValidValues"=>[{"0", "HEARTBEAT"},{"1", "TEST_REQUEST"},{"2", "RESEND_REQUEST"},{"3", "REJECT"},{"4", "SEQUENCE_RESET"},{"5", "LOGOUT"},{"6", "INDICATION_OF_INTEREST"},{"7", "ADVERTISEMENT"},{"8", "EXECUTION_REPORT"},{"9", "ORDER_CANCEL_REJECT"},{"a", "QUOTE_STATUS_REQUEST"},{"A", "LOGON"},{"AA", "DERIVATIVE_SECURITY_LIST"},{"AB", "NEW_ORDER_AB"},{"AC", "MULTILEG_ORDER_CANCEL_REPLACE"},{"AD", "TRADE_CAPTURE_REPORT_REQUEST"},{"AE", "TRADE_CAPTURE_REPORT"},{"AF", "ORDER_MASS_STATUS_REQUEST"},{"AG", "QUOTE_REQUEST_REJECT"},{"AH", "RFQ_REQUEST"},{"AI", "QUOTE_STATUS_REPORT"},{"b", "MASS_QUOTE_ACKNOWLEDGEMENT"},{"B", "NEWS"},{"c", "SECURITY_DEFINITION_REQUEST"},{"C", "EMAIL"},{"d", "SECURITY_DEFINITION"},{"D", "ORDER_SINGLE"},{"e", "SECURITY_STATUS_REQUEST"},{"E", "ORDER_LIST"},{"f", "SECURITY_STATUS"},{"F", "ORDER_CANCEL_REQUEST"},{"G", "ORDER_CANCEL_REPLACE_REQUEST"},{"g", "TRADING_SESSION_STATUS_REQUEST"},{"h", "TRADING_SESSION_STATUS"},{"H", "ORDER_STATUS_REQUEST"},{"i", "MASS_QUOTE"},{"j", "BUSINESS_MESSAGE_REJECT"},{"J", "ALLOCATION"},{"K", "LIST_CANCEL_REQUEST"},{"k", "BID_REQUEST"},{"l", "BID_RESPONSE"},{"L", "LIST_EXECUTE"},{"m", "LIST_STRIKE_PRICE"},{"M", "LIST_STATUS_REQUEST"},{"N", "LIST_STATUS"},{"n", "XML_MESSAGE"},{"o", "REGISTRATION_INSTRUCTIONS"},{"P", "ALLOCATION_ACK"},{"p", "REGISTRATION_INSTRUCTIONS_RESPONSE"},{"q", "ORDER_MASS_CANCEL_REQUEST"},{"Q", "DONT_KNOW_TRADE"},{"r", "ORDER_MASS_CANCEL_REPORT"},{"R", "QUOTE_REQUEST"},{"s", "NEW_ORDER_s"},{"S", "QUOTE"},{"t", "CROSS_ORDER_CANCEL_REPLACE_REQUEST"},{"T", "SETTLEMENT_INSTRUCTIONS"},{"u", "CROSS_ORDER_CANCEL_REQUEST"},{"v", "SECURITY_TYPE_REQUEST"},{"V", "MARKET_DATA_REQUEST"},{"w", "SECURITY_TYPES"},{"W", "MARKET_DATA_SNAPSHOT_FULL_REFRESH"},{"x", "SECURITY_LIST_REQUEST"},{"X", "MARKET_DATA_INCREMENTAL_REFRESH"},{"y", "SECURITY_LIST"},{"Y", "MARKET_DATA_REQUEST_REJECT"},{"z", "DERIVATIVE_SECURITY_LIST_REQUEST"},{"Z", "QUOTE_CANCEL"}], "TagNum" => "35"}


,
"NewSeqNo" => #{"TagNum" => "36" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "36" => #{"Name"=>"NewSeqNo" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "36"}


,
"OrderID" => #{"TagNum" => "37" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "37" => #{"Name"=>"OrderID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "37"}


,
"OrderQty" => #{"TagNum" => "38" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "38" => #{"Name"=>"OrderQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "38"}


,
"OrdStatus" => #{"TagNum" => "39" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "PARTIALLY_FILLED"},{"5", "REPLACED"},{"2", "FILLED"},{"6", "PENDING_CANCEL"},{"7", "STOPPED"},{"8", "REJECTED"},{"9", "SUSPENDED"},{"A", "PENDING_NEW"},{"B", "CALCULATED"},{"C", "EXPIRED"},{"D", "ACCEPTED_FOR_BIDDING"},{"E", "PENDING_REPLACE"},{"3", "DONE_FOR_DAY"},{"4", "CANCELED"}]}
, "39" => #{"Name"=>"OrdStatus" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "PARTIALLY_FILLED"},{"5", "REPLACED"},{"2", "FILLED"},{"6", "PENDING_CANCEL"},{"7", "STOPPED"},{"8", "REJECTED"},{"9", "SUSPENDED"},{"A", "PENDING_NEW"},{"B", "CALCULATED"},{"C", "EXPIRED"},{"D", "ACCEPTED_FOR_BIDDING"},{"E", "PENDING_REPLACE"},{"3", "DONE_FOR_DAY"},{"4", "CANCELED"}], "TagNum" => "39"}


,
"OrdType" => #{"TagNum" => "40" ,"Type" => "CHAR" ,"ValidValues" =>[{"D", "PREVIOUSLY_QUOTED"},{"2", "LIMIT"},{"3", "STOP"},{"4", "STOP_LIMIT"},{"5", "MARKET_ON_CLOSE"},{"6", "WITH_OR_WITHOUT"},{"7", "LIMIT_OR_BETTER"},{"8", "LIMIT_WITH_OR_WITHOUT"},{"9", "ON_BASIS"},{"A", "ON_CLOSE"},{"1", "MARKET"},{"C", "FOREX_C"},{"F", "FOREX_F"},{"E", "PREVIOUSLY_INDICATED"},{"G", "FOREX_G"},{"I", "FUNARI"},{"J", "MARKET_IF_TOUCHED"},{"K", "MARKET_WITH_LEFTOVER_AS_LIMIT"},{"L", "PREVIOUS_FUND_VALUATION_POINT"},{"M", "NEXT_FUND_VALUATION_POINT"},{"P", "PEGGED"},{"B", "LIMIT_ON_CLOSE"},{"H", "FOREX_H"}]}
, "40" => #{"Name"=>"OrdType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"D", "PREVIOUSLY_QUOTED"},{"2", "LIMIT"},{"3", "STOP"},{"4", "STOP_LIMIT"},{"5", "MARKET_ON_CLOSE"},{"6", "WITH_OR_WITHOUT"},{"7", "LIMIT_OR_BETTER"},{"8", "LIMIT_WITH_OR_WITHOUT"},{"9", "ON_BASIS"},{"A", "ON_CLOSE"},{"1", "MARKET"},{"C", "FOREX_C"},{"F", "FOREX_F"},{"E", "PREVIOUSLY_INDICATED"},{"G", "FOREX_G"},{"I", "FUNARI"},{"J", "MARKET_IF_TOUCHED"},{"K", "MARKET_WITH_LEFTOVER_AS_LIMIT"},{"L", "PREVIOUS_FUND_VALUATION_POINT"},{"M", "NEXT_FUND_VALUATION_POINT"},{"P", "PEGGED"},{"B", "LIMIT_ON_CLOSE"},{"H", "FOREX_H"}], "TagNum" => "40"}


,
"OrigClOrdID" => #{"TagNum" => "41" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "41" => #{"Name"=>"OrigClOrdID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "41"}


,
"OrigTime" => #{"TagNum" => "42" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "42" => #{"Name"=>"OrigTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "42"}


,
"PossDupFlag" => #{"TagNum" => "43" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "43" => #{"Name"=>"PossDupFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "43"}


,
"Price" => #{"TagNum" => "44" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "44" => #{"Name"=>"Price" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "44"}


,
"RefSeqNum" => #{"TagNum" => "45" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "45" => #{"Name"=>"RefSeqNum" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "45"}


,
"Rule80A" => #{"TagNum" => "47" ,"Type" => "CHAR" ,"ValidValues" =>[{"N", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_OTHER_MEMBER"},{"B", "SHORT_EXEMPT_TRANSACTION_B"},{"D", "PROGRAM_ORDER_INDEX_ARB_FOR_MEMBER_FIRM_ORG"},{"E", "SHORT_EXEMPT_TRANSACTION_FOR_PRINCIPAL"},{"F", "SHORT_EXEMPT_TRANSACTION_F"},{"H", "SHORT_EXEMPT_TRANSACTION_H"},{"I", "INDIVIDUAL_INVESTOR_SINGLE_ORDER"},{"J", "PROGRAM_ORDER_INDEX_ARB_FOR_INDIVIDUAL_CUSTOMER"},{"K", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_INDIVIDUAL_CUSTOMER"},{"M", "PROGRAM_ORDER_INDEX_ARB_FOR_OTHER_MEMBER"},{"A", "AGENCY_SINGLE_ORDER"},{"O", "PROPRIETARY_TRANSACTIONS_FOR_COMPETING_MARKET_MAKER_THAT_IS_AFFILIATED_WITH_THE_CLEARING_MEMBER"},{"P", "PRINCIPAL"},{"R", "TRANSACTIONS_FOR_THE_ACCOUNT_OF_A_NON_MEMBER_COMPETING_MARKET_MAKER"},{"S", "SPECIALIST_TRADES"},{"T", "TRANSACTIONS_FOR_THE_ACCOUNT_OF_AN_UNAFFILIATED_MEMBERS_COMPETING_MARKET_MAKER"},{"U", "PROGRAM_ORDER_INDEX_ARB_FOR_OTHER_AGENCY"},{"W", "ALL_OTHER_ORDERS_AS_AGENT_FOR_OTHER_MEMBER"},{"X", "SHORT_EXEMPT_TRANSACTION_FOR_MEMBER_COMPETING_MARKET_MAKER_NOT_AFFILIATED_WITH_THE_FIRM_CLEARING_THE_TRADE"},{"Y", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_OTHER_AGENCY"},{"Z", "SHORT_EXEMPT_TRANSACTION_FOR_NON_MEMBER_COMPETING_MARKET_MAKER"},{"L", "SHORT_EXEMPT_TRANSACTION_FOR_MEMBER_COMPETING_MARKET_MAKER_AFFILIATED_WITH_THE_FIRM_CLEARING_THE_TRADE"},{"C", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_MEMBER_FIRM_ORG"}]}
, "47" => #{"Name"=>"Rule80A" ,"Type"=>"CHAR" ,"ValidValues"=>[{"N", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_OTHER_MEMBER"},{"B", "SHORT_EXEMPT_TRANSACTION_B"},{"D", "PROGRAM_ORDER_INDEX_ARB_FOR_MEMBER_FIRM_ORG"},{"E", "SHORT_EXEMPT_TRANSACTION_FOR_PRINCIPAL"},{"F", "SHORT_EXEMPT_TRANSACTION_F"},{"H", "SHORT_EXEMPT_TRANSACTION_H"},{"I", "INDIVIDUAL_INVESTOR_SINGLE_ORDER"},{"J", "PROGRAM_ORDER_INDEX_ARB_FOR_INDIVIDUAL_CUSTOMER"},{"K", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_INDIVIDUAL_CUSTOMER"},{"M", "PROGRAM_ORDER_INDEX_ARB_FOR_OTHER_MEMBER"},{"A", "AGENCY_SINGLE_ORDER"},{"O", "PROPRIETARY_TRANSACTIONS_FOR_COMPETING_MARKET_MAKER_THAT_IS_AFFILIATED_WITH_THE_CLEARING_MEMBER"},{"P", "PRINCIPAL"},{"R", "TRANSACTIONS_FOR_THE_ACCOUNT_OF_A_NON_MEMBER_COMPETING_MARKET_MAKER"},{"S", "SPECIALIST_TRADES"},{"T", "TRANSACTIONS_FOR_THE_ACCOUNT_OF_AN_UNAFFILIATED_MEMBERS_COMPETING_MARKET_MAKER"},{"U", "PROGRAM_ORDER_INDEX_ARB_FOR_OTHER_AGENCY"},{"W", "ALL_OTHER_ORDERS_AS_AGENT_FOR_OTHER_MEMBER"},{"X", "SHORT_EXEMPT_TRANSACTION_FOR_MEMBER_COMPETING_MARKET_MAKER_NOT_AFFILIATED_WITH_THE_FIRM_CLEARING_THE_TRADE"},{"Y", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_OTHER_AGENCY"},{"Z", "SHORT_EXEMPT_TRANSACTION_FOR_NON_MEMBER_COMPETING_MARKET_MAKER"},{"L", "SHORT_EXEMPT_TRANSACTION_FOR_MEMBER_COMPETING_MARKET_MAKER_AFFILIATED_WITH_THE_FIRM_CLEARING_THE_TRADE"},{"C", "PROGRAM_ORDER_NON_INDEX_ARB_FOR_MEMBER_FIRM_ORG"}], "TagNum" => "47"}


,
"SecurityID" => #{"TagNum" => "48" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "48" => #{"Name"=>"SecurityID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "48"}


,
"SenderCompID" => #{"TagNum" => "49" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "49" => #{"Name"=>"SenderCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "49"}


,
"SenderSubID" => #{"TagNum" => "50" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "50" => #{"Name"=>"SenderSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "50"}


,
"SendingTime" => #{"TagNum" => "52" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "52" => #{"Name"=>"SendingTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "52"}


,
"Quantity" => #{"TagNum" => "53" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "53" => #{"Name"=>"Quantity" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "53"}


,
"Side" => #{"TagNum" => "54" ,"Type" => "CHAR" ,"ValidValues" =>[{"6", "SELL_SHORT_EXEMPT"},{"B", "AS_DEFINED"},{"C", "OPPOSITE"},{"8", "CROSS"},{"9", "CROSS_SHORT"},{"1", "BUY"},{"2", "SELL"},{"3", "BUY_MINUS"},{"4", "SELL_PLUS"},{"A", "CROSS_SHORT_EXEMPT"},{"5", "SELL_SHORT"},{"7", "UNDISCLOSED"}]}
, "54" => #{"Name"=>"Side" ,"Type"=>"CHAR" ,"ValidValues"=>[{"6", "SELL_SHORT_EXEMPT"},{"B", "AS_DEFINED"},{"C", "OPPOSITE"},{"8", "CROSS"},{"9", "CROSS_SHORT"},{"1", "BUY"},{"2", "SELL"},{"3", "BUY_MINUS"},{"4", "SELL_PLUS"},{"A", "CROSS_SHORT_EXEMPT"},{"5", "SELL_SHORT"},{"7", "UNDISCLOSED"}], "TagNum" => "54"}


,
"Symbol" => #{"TagNum" => "55" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "55" => #{"Name"=>"Symbol" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "55"}


,
"TargetCompID" => #{"TagNum" => "56" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "56" => #{"Name"=>"TargetCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "56"}


,
"TargetSubID" => #{"TagNum" => "57" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "57" => #{"Name"=>"TargetSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "57"}


,
"Text" => #{"TagNum" => "58" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "58" => #{"Name"=>"Text" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "58"}


,
"TimeInForce" => #{"TagNum" => "59" ,"Type" => "CHAR" ,"ValidValues" =>[{"7", "AT_THE_CLOSE"},{"0", "DAY"},{"1", "GOOD_TILL_CANCEL"},{"2", "AT_THE_OPENING"},{"3", "IMMEDIATE_OR_CANCEL"},{"4", "FILL_OR_KILL"},{"5", "GOOD_TILL_CROSSING"},{"6", "GOOD_TILL_DATE"}]}
, "59" => #{"Name"=>"TimeInForce" ,"Type"=>"CHAR" ,"ValidValues"=>[{"7", "AT_THE_CLOSE"},{"0", "DAY"},{"1", "GOOD_TILL_CANCEL"},{"2", "AT_THE_OPENING"},{"3", "IMMEDIATE_OR_CANCEL"},{"4", "FILL_OR_KILL"},{"5", "GOOD_TILL_CROSSING"},{"6", "GOOD_TILL_DATE"}], "TagNum" => "59"}


,
"TransactTime" => #{"TagNum" => "60" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "60" => #{"Name"=>"TransactTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "60"}


,
"Urgency" => #{"TagNum" => "61" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "FLASH"},{"2", "BACKGROUND"},{"0", "NORMAL"}]}
, "61" => #{"Name"=>"Urgency" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "FLASH"},{"2", "BACKGROUND"},{"0", "NORMAL"}], "TagNum" => "61"}


,
"ValidUntilTime" => #{"TagNum" => "62" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "62" => #{"Name"=>"ValidUntilTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "62"}


,
"SettlmntTyp" => #{"TagNum" => "63" ,"Type" => "CHAR" ,"ValidValues" =>[{"5", "T_PLUS_4"},{"A", "T_PLUS_1"},{"6", "FUTURE"},{"3", "T_PLUS_2"},{"2", "NEXT_DAY"},{"8", "SELLERS_OPTION"},{"1", "CASH"},{"7", "WHEN_AND_IF_ISSUED"},{"0", "REGULAR"},{"9", "T_PLUS_5"},{"4", "T_PLUS_3"}]}
, "63" => #{"Name"=>"SettlmntTyp" ,"Type"=>"CHAR" ,"ValidValues"=>[{"5", "T_PLUS_4"},{"A", "T_PLUS_1"},{"6", "FUTURE"},{"3", "T_PLUS_2"},{"2", "NEXT_DAY"},{"8", "SELLERS_OPTION"},{"1", "CASH"},{"7", "WHEN_AND_IF_ISSUED"},{"0", "REGULAR"},{"9", "T_PLUS_5"},{"4", "T_PLUS_3"}], "TagNum" => "63"}


,
"FutSettDate" => #{"TagNum" => "64" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "64" => #{"Name"=>"FutSettDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "64"}


,
"SymbolSfx" => #{"TagNum" => "65" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "65" => #{"Name"=>"SymbolSfx" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "65"}


,
"ListID" => #{"TagNum" => "66" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "66" => #{"Name"=>"ListID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "66"}


,
"ListSeqNo" => #{"TagNum" => "67" ,"Type" => "INT" ,"ValidValues" =>[]}
, "67" => #{"Name"=>"ListSeqNo" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "67"}


,
"TotNoOrders" => #{"TagNum" => "68" ,"Type" => "INT" ,"ValidValues" =>[]}
, "68" => #{"Name"=>"TotNoOrders" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "68"}


,
"ListExecInst" => #{"TagNum" => "69" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "69" => #{"Name"=>"ListExecInst" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "69"}


,
"AllocID" => #{"TagNum" => "70" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "70" => #{"Name"=>"AllocID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "70"}


,
"AllocTransType" => #{"TagNum" => "71" ,"Type" => "CHAR" ,"ValidValues" =>[{"5", "CALCULATED_WITHOUT_PRELIMINARY"},{"4", "CALCULATED"},{"3", "PRELIMINARY"},{"2", "CANCEL"},{"1", "REPLACE"},{"0", "NEW"}]}
, "71" => #{"Name"=>"AllocTransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"5", "CALCULATED_WITHOUT_PRELIMINARY"},{"4", "CALCULATED"},{"3", "PRELIMINARY"},{"2", "CANCEL"},{"1", "REPLACE"},{"0", "NEW"}], "TagNum" => "71"}


,
"RefAllocID" => #{"TagNum" => "72" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "72" => #{"Name"=>"RefAllocID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "72"}


,
"NoOrders" => #{"TagNum" => "73" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "73" => #{"Name"=>"NoOrders" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "73"}


,
"AvgPrxPrecision" => #{"TagNum" => "74" ,"Type" => "INT" ,"ValidValues" =>[]}
, "74" => #{"Name"=>"AvgPrxPrecision" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "74"}


,
"TradeDate" => #{"TagNum" => "75" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "75" => #{"Name"=>"TradeDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "75"}


,
"PositionEffect" => #{"TagNum" => "77" ,"Type" => "CHAR" ,"ValidValues" =>[{"F", "FIFO"},{"R", "ROLLED"},{"C", "CLOSE"},{"O", "OPEN"}]}
, "77" => #{"Name"=>"PositionEffect" ,"Type"=>"CHAR" ,"ValidValues"=>[{"F", "FIFO"},{"R", "ROLLED"},{"C", "CLOSE"},{"O", "OPEN"}], "TagNum" => "77"}


,
"NoAllocs" => #{"TagNum" => "78" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "78" => #{"Name"=>"NoAllocs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "78"}


,
"AllocAccount" => #{"TagNum" => "79" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "79" => #{"Name"=>"AllocAccount" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "79"}


,
"AllocQty" => #{"TagNum" => "80" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "80" => #{"Name"=>"AllocQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "80"}


,
"ProcessCode" => #{"TagNum" => "81" ,"Type" => "CHAR" ,"ValidValues" =>[{"6", "PLAN_SPONSOR"},{"0", "REGULAR"},{"1", "SOFT_DOLLAR"},{"2", "STEP_IN"},{"3", "STEP_OUT"},{"4", "SOFT_DOLLAR_STEP_IN"},{"5", "SOFT_DOLLAR_STEP_OUT"}]}
, "81" => #{"Name"=>"ProcessCode" ,"Type"=>"CHAR" ,"ValidValues"=>[{"6", "PLAN_SPONSOR"},{"0", "REGULAR"},{"1", "SOFT_DOLLAR"},{"2", "STEP_IN"},{"3", "STEP_OUT"},{"4", "SOFT_DOLLAR_STEP_IN"},{"5", "SOFT_DOLLAR_STEP_OUT"}], "TagNum" => "81"}


,
"NoRpts" => #{"TagNum" => "82" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "82" => #{"Name"=>"NoRpts" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "82"}


,
"RptSeq" => #{"TagNum" => "83" ,"Type" => "INT" ,"ValidValues" =>[]}
, "83" => #{"Name"=>"RptSeq" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "83"}


,
"CxlQty" => #{"TagNum" => "84" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "84" => #{"Name"=>"CxlQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "84"}


,
"AllocStatus" => #{"TagNum" => "87" ,"Type" => "INT" ,"ValidValues" =>[{"1", "REJECTED"},{"2", "PARTIAL_ACCEPT"},{"3", "RECEIVED"},{"0", "ACCEPTED"}]}
, "87" => #{"Name"=>"AllocStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "REJECTED"},{"2", "PARTIAL_ACCEPT"},{"3", "RECEIVED"},{"0", "ACCEPTED"}], "TagNum" => "87"}


,
"AllocRejCode" => #{"TagNum" => "88" ,"Type" => "INT" ,"ValidValues" =>[{"0", "UNKNOWN_ACCOUNT"},{"6", "UNKNOWN_LISTID"},{"3", "UNKNOWN_EXECUTING_BROKER_MNEMONIC"},{"5", "UNKNOWN_ORDERID"},{"7", "OTHER"},{"4", "COMMISSION_DIFFERENCE"},{"1", "INCORRECT_QUANTITY"},{"2", "INCORRECT_AVERAGE_PRICE"}]}
, "88" => #{"Name"=>"AllocRejCode" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "UNKNOWN_ACCOUNT"},{"6", "UNKNOWN_LISTID"},{"3", "UNKNOWN_EXECUTING_BROKER_MNEMONIC"},{"5", "UNKNOWN_ORDERID"},{"7", "OTHER"},{"4", "COMMISSION_DIFFERENCE"},{"1", "INCORRECT_QUANTITY"},{"2", "INCORRECT_AVERAGE_PRICE"}], "TagNum" => "88"}


,
"Signature" => #{"TagNum" => "89" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "89" => #{"Name"=>"Signature" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "89"}


,
"SecureDataLen" => #{"TagNum" => "90" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "90" => #{"Name"=>"SecureDataLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "90"}


,
"SecureData" => #{"TagNum" => "91" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "91" => #{"Name"=>"SecureData" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "91"}


,
"SignatureLength" => #{"TagNum" => "93" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "93" => #{"Name"=>"SignatureLength" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "93"}


,
"EmailType" => #{"TagNum" => "94" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "REPLY"},{"2", "ADMIN_REPLY"}]}
, "94" => #{"Name"=>"EmailType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "REPLY"},{"2", "ADMIN_REPLY"}], "TagNum" => "94"}


,
"RawDataLength" => #{"TagNum" => "95" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "95" => #{"Name"=>"RawDataLength" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "95"}


,
"RawData" => #{"TagNum" => "96" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "96" => #{"Name"=>"RawData" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "96"}


,
"PossResend" => #{"TagNum" => "97" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "97" => #{"Name"=>"PossResend" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "97"}


,
"EncryptMethod" => #{"TagNum" => "98" ,"Type" => "INT" ,"ValidValues" =>[{"2", "DES"},{"6", "PEM_DES_MD5"},{"5", "PGP_DES_MD5"},{"3", "PKCS_DES"},{"0", "NONE"},{"1", "PKCS"},{"4", "PGP_DES"}]}
, "98" => #{"Name"=>"EncryptMethod" ,"Type"=>"INT" ,"ValidValues"=>[{"2", "DES"},{"6", "PEM_DES_MD5"},{"5", "PGP_DES_MD5"},{"3", "PKCS_DES"},{"0", "NONE"},{"1", "PKCS"},{"4", "PGP_DES"}], "TagNum" => "98"}


,
"StopPx" => #{"TagNum" => "99" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "99" => #{"Name"=>"StopPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "99"}


,
"ExDestination" => #{"TagNum" => "100" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "100" => #{"Name"=>"ExDestination" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "100"}


,
"CxlRejReason" => #{"TagNum" => "102" ,"Type" => "INT" ,"ValidValues" =>[{"1", "UNKNOWN_ORDER"},{"0", "TOO_LATE_TO_CANCEL"},{"6", "DUPLICATE_CLORDID_RECEIVED"},{"5", "ORIGORDMODTIME_DID_NOT_MATCH_LAST_TRANSACTTIME_OF_ORDER"},{"4", "UNABLE_TO_PROCESS_ORDER_MASS_CANCEL_REQUEST"},{"3", "ORDER_ALREADY_IN_PENDING_CANCEL_OR_PENDING_REPLACE_STATUS"},{"2", "BROKER"}]}
, "102" => #{"Name"=>"CxlRejReason" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "UNKNOWN_ORDER"},{"0", "TOO_LATE_TO_CANCEL"},{"6", "DUPLICATE_CLORDID_RECEIVED"},{"5", "ORIGORDMODTIME_DID_NOT_MATCH_LAST_TRANSACTTIME_OF_ORDER"},{"4", "UNABLE_TO_PROCESS_ORDER_MASS_CANCEL_REQUEST"},{"3", "ORDER_ALREADY_IN_PENDING_CANCEL_OR_PENDING_REPLACE_STATUS"},{"2", "BROKER"}], "TagNum" => "102"}


,
"OrdRejReason" => #{"TagNum" => "103" ,"Type" => "INT" ,"ValidValues" =>[{"2", "EXCHANGE_CLOSED"},{"1", "UNKNOWN_SYMBOL"},{"3", "ORDER_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "UNKNOWN_ORDER"},{"7", "DUPLICATE_OF_A_VERBALLY_COMMUNICATED_ORDER"},{"9", "TRADE_ALONG_REQUIRED"},{"10", "INVALID_INVESTOR_ID"},{"6", "DUPLICATE_ORDER"},{"11", "UNSUPPORTED_ORDER_CHARACTERISTIC"},{"12", "SURVEILLENCE_OPTION"},{"0", "BROKER"},{"8", "STALE_ORDER"}]}
, "103" => #{"Name"=>"OrdRejReason" ,"Type"=>"INT" ,"ValidValues"=>[{"2", "EXCHANGE_CLOSED"},{"1", "UNKNOWN_SYMBOL"},{"3", "ORDER_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "UNKNOWN_ORDER"},{"7", "DUPLICATE_OF_A_VERBALLY_COMMUNICATED_ORDER"},{"9", "TRADE_ALONG_REQUIRED"},{"10", "INVALID_INVESTOR_ID"},{"6", "DUPLICATE_ORDER"},{"11", "UNSUPPORTED_ORDER_CHARACTERISTIC"},{"12", "SURVEILLENCE_OPTION"},{"0", "BROKER"},{"8", "STALE_ORDER"}], "TagNum" => "103"}


,
"IOIQualifier" => #{"TagNum" => "104" ,"Type" => "CHAR" ,"ValidValues" =>[{"O", "AT_THE_OPEN"},{"X", "CROSSING_OPPORTUNITY"},{"W", "INDICATION"},{"V", "VERSUS"},{"T", "THROUGH_THE_DAY"},{"S", "PORTFOLIO_SHOWN"},{"R", "READY_TO_TRADE"},{"A", "ALL_OR_NONE"},{"P", "TAKING_A_POSITION"},{"M", "MORE_BEHIND"},{"L", "LIMIT"},{"I", "IN_TOUCH_WITH"},{"D", "VWAP"},{"C", "AT_THE_CLOSE"},{"B", "MARKET_ON_CLOSE"},{"Q", "AT_THE_MARKET"},{"Y", "AT_THE_MIDPOINT"},{"Z", "PRE_OPEN"}]}
, "104" => #{"Name"=>"IOIQualifier" ,"Type"=>"CHAR" ,"ValidValues"=>[{"O", "AT_THE_OPEN"},{"X", "CROSSING_OPPORTUNITY"},{"W", "INDICATION"},{"V", "VERSUS"},{"T", "THROUGH_THE_DAY"},{"S", "PORTFOLIO_SHOWN"},{"R", "READY_TO_TRADE"},{"A", "ALL_OR_NONE"},{"P", "TAKING_A_POSITION"},{"M", "MORE_BEHIND"},{"L", "LIMIT"},{"I", "IN_TOUCH_WITH"},{"D", "VWAP"},{"C", "AT_THE_CLOSE"},{"B", "MARKET_ON_CLOSE"},{"Q", "AT_THE_MARKET"},{"Y", "AT_THE_MIDPOINT"},{"Z", "PRE_OPEN"}], "TagNum" => "104"}


,
"Issuer" => #{"TagNum" => "106" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "106" => #{"Name"=>"Issuer" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "106"}


,
"SecurityDesc" => #{"TagNum" => "107" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "107" => #{"Name"=>"SecurityDesc" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "107"}


,
"HeartBtInt" => #{"TagNum" => "108" ,"Type" => "INT" ,"ValidValues" =>[]}
, "108" => #{"Name"=>"HeartBtInt" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "108"}


,
"MinQty" => #{"TagNum" => "110" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "110" => #{"Name"=>"MinQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "110"}


,
"MaxFloor" => #{"TagNum" => "111" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "111" => #{"Name"=>"MaxFloor" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "111"}


,
"TestReqID" => #{"TagNum" => "112" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "112" => #{"Name"=>"TestReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "112"}


,
"ReportToExch" => #{"TagNum" => "113" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "113" => #{"Name"=>"ReportToExch" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "113"}


,
"LocateReqd" => #{"TagNum" => "114" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "114" => #{"Name"=>"LocateReqd" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "114"}


,
"OnBehalfOfCompID" => #{"TagNum" => "115" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "115" => #{"Name"=>"OnBehalfOfCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "115"}


,
"OnBehalfOfSubID" => #{"TagNum" => "116" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "116" => #{"Name"=>"OnBehalfOfSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "116"}


,
"QuoteID" => #{"TagNum" => "117" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "117" => #{"Name"=>"QuoteID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "117"}


,
"NetMoney" => #{"TagNum" => "118" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "118" => #{"Name"=>"NetMoney" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "118"}


,
"SettlCurrAmt" => #{"TagNum" => "119" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "119" => #{"Name"=>"SettlCurrAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "119"}


,
"SettlCurrency" => #{"TagNum" => "120" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "120" => #{"Name"=>"SettlCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "120"}


,
"ForexReq" => #{"TagNum" => "121" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "121" => #{"Name"=>"ForexReq" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "121"}


,
"OrigSendingTime" => #{"TagNum" => "122" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "122" => #{"Name"=>"OrigSendingTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "122"}


,
"GapFillFlag" => #{"TagNum" => "123" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "123" => #{"Name"=>"GapFillFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "123"}


,
"NoExecs" => #{"TagNum" => "124" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "124" => #{"Name"=>"NoExecs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "124"}


,
"ExpireTime" => #{"TagNum" => "126" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "126" => #{"Name"=>"ExpireTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "126"}


,
"DKReason" => #{"TagNum" => "127" ,"Type" => "CHAR" ,"ValidValues" =>[{"B", "WRONG_SIDE"},{"C", "QUANTITY_EXCEEDS_ORDER"},{"D", "NO_MATCHING_ORDER"},{"E", "PRICE_EXCEEDS_LIMIT"},{"Z", "OTHER"},{"A", "UNKNOWN_SYMBOL"}]}
, "127" => #{"Name"=>"DKReason" ,"Type"=>"CHAR" ,"ValidValues"=>[{"B", "WRONG_SIDE"},{"C", "QUANTITY_EXCEEDS_ORDER"},{"D", "NO_MATCHING_ORDER"},{"E", "PRICE_EXCEEDS_LIMIT"},{"Z", "OTHER"},{"A", "UNKNOWN_SYMBOL"}], "TagNum" => "127"}


,
"DeliverToCompID" => #{"TagNum" => "128" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "128" => #{"Name"=>"DeliverToCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "128"}


,
"DeliverToSubID" => #{"TagNum" => "129" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "129" => #{"Name"=>"DeliverToSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "129"}


,
"IOINaturalFlag" => #{"TagNum" => "130" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "130" => #{"Name"=>"IOINaturalFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "130"}


,
"QuoteReqID" => #{"TagNum" => "131" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "131" => #{"Name"=>"QuoteReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "131"}


,
"BidPx" => #{"TagNum" => "132" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "132" => #{"Name"=>"BidPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "132"}


,
"OfferPx" => #{"TagNum" => "133" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "133" => #{"Name"=>"OfferPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "133"}


,
"BidSize" => #{"TagNum" => "134" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "134" => #{"Name"=>"BidSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "134"}


,
"OfferSize" => #{"TagNum" => "135" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "135" => #{"Name"=>"OfferSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "135"}


,
"NoMiscFees" => #{"TagNum" => "136" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "136" => #{"Name"=>"NoMiscFees" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "136"}


,
"MiscFeeAmt" => #{"TagNum" => "137" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "137" => #{"Name"=>"MiscFeeAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "137"}


,
"MiscFeeCurr" => #{"TagNum" => "138" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "138" => #{"Name"=>"MiscFeeCurr" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "138"}


,
"MiscFeeType" => #{"TagNum" => "139" ,"Type" => "CHAR" ,"ValidValues" =>[{"3", "LOCAL_COMMISSION"},{"4", "EXCHANGE_FEES"},{"5", "STAMP"},{"6", "LEVY"},{"7", "OTHER"},{"8", "MARKUP"},{"9", "CONSUMPTION_TAX"},{"1", "REGULATORY"},{"2", "TAX"}]}
, "139" => #{"Name"=>"MiscFeeType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"3", "LOCAL_COMMISSION"},{"4", "EXCHANGE_FEES"},{"5", "STAMP"},{"6", "LEVY"},{"7", "OTHER"},{"8", "MARKUP"},{"9", "CONSUMPTION_TAX"},{"1", "REGULATORY"},{"2", "TAX"}], "TagNum" => "139"}


,
"PrevClosePx" => #{"TagNum" => "140" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "140" => #{"Name"=>"PrevClosePx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "140"}


,
"ResetSeqNumFlag" => #{"TagNum" => "141" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "141" => #{"Name"=>"ResetSeqNumFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "141"}


,
"SenderLocationID" => #{"TagNum" => "142" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "142" => #{"Name"=>"SenderLocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "142"}


,
"TargetLocationID" => #{"TagNum" => "143" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "143" => #{"Name"=>"TargetLocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "143"}


,
"OnBehalfOfLocationID" => #{"TagNum" => "144" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "144" => #{"Name"=>"OnBehalfOfLocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "144"}


,
"DeliverToLocationID" => #{"TagNum" => "145" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "145" => #{"Name"=>"DeliverToLocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "145"}


,
"NoRelatedSym" => #{"TagNum" => "146" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "146" => #{"Name"=>"NoRelatedSym" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "146"}


,
"Subject" => #{"TagNum" => "147" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "147" => #{"Name"=>"Subject" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "147"}


,
"Headline" => #{"TagNum" => "148" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "148" => #{"Name"=>"Headline" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "148"}


,
"URLLink" => #{"TagNum" => "149" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "149" => #{"Name"=>"URLLink" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "149"}


,
"ExecType" => #{"TagNum" => "150" ,"Type" => "CHAR" ,"ValidValues" =>[{"6", "PENDING_CANCEL"},{"0", "NEW"},{"1", "PARTIAL_FILL"},{"2", "FILL"},{"4", "CANCELED"},{"5", "REPLACE"},{"8", "REJECTED"},{"9", "SUSPENDED"},{"A", "PENDING_NEW"},{"B", "CALCULATED"},{"C", "EXPIRED"},{"D", "RESTATED"},{"E", "PENDING_REPLACE"},{"F", "TRADE"},{"G", "TRADE_CORRECT"},{"H", "TRADE_CANCEL"},{"I", "ORDER_STATUS"},{"3", "DONE_FOR_DAY"},{"7", "STOPPED"}]}
, "150" => #{"Name"=>"ExecType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"6", "PENDING_CANCEL"},{"0", "NEW"},{"1", "PARTIAL_FILL"},{"2", "FILL"},{"4", "CANCELED"},{"5", "REPLACE"},{"8", "REJECTED"},{"9", "SUSPENDED"},{"A", "PENDING_NEW"},{"B", "CALCULATED"},{"C", "EXPIRED"},{"D", "RESTATED"},{"E", "PENDING_REPLACE"},{"F", "TRADE"},{"G", "TRADE_CORRECT"},{"H", "TRADE_CANCEL"},{"I", "ORDER_STATUS"},{"3", "DONE_FOR_DAY"},{"7", "STOPPED"}], "TagNum" => "150"}


,
"LeavesQty" => #{"TagNum" => "151" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "151" => #{"Name"=>"LeavesQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "151"}


,
"CashOrderQty" => #{"TagNum" => "152" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "152" => #{"Name"=>"CashOrderQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "152"}


,
"AllocAvgPx" => #{"TagNum" => "153" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "153" => #{"Name"=>"AllocAvgPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "153"}


,
"AllocNetMoney" => #{"TagNum" => "154" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "154" => #{"Name"=>"AllocNetMoney" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "154"}


,
"SettlCurrFxRate" => #{"TagNum" => "155" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "155" => #{"Name"=>"SettlCurrFxRate" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "155"}


,
"SettlCurrFxRateCalc" => #{"TagNum" => "156" ,"Type" => "CHAR" ,"ValidValues" =>[{"D", "DIVIDE"},{"M", "MULTIPLY"}]}
, "156" => #{"Name"=>"SettlCurrFxRateCalc" ,"Type"=>"CHAR" ,"ValidValues"=>[{"D", "DIVIDE"},{"M", "MULTIPLY"}], "TagNum" => "156"}


,
"NumDaysInterest" => #{"TagNum" => "157" ,"Type" => "INT" ,"ValidValues" =>[]}
, "157" => #{"Name"=>"NumDaysInterest" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "157"}


,
"AccruedInterestRate" => #{"TagNum" => "158" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "158" => #{"Name"=>"AccruedInterestRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "158"}


,
"AccruedInterestAmt" => #{"TagNum" => "159" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "159" => #{"Name"=>"AccruedInterestAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "159"}


,
"SettlInstMode" => #{"TagNum" => "160" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "DEFAULT"},{"4", "SPECIFIC_ORDER_FOR_A_SINGLE_ACCOUNT"},{"3", "SPECIFIC_ALLOCATION_ACCOUNT_STANDING"},{"1", "STANDING_INSTRUCTIONS_PROVIDED"},{"2", "SPECIFIC_ALLOCATION_ACCOUNT_OVERRIDING"}]}
, "160" => #{"Name"=>"SettlInstMode" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "DEFAULT"},{"4", "SPECIFIC_ORDER_FOR_A_SINGLE_ACCOUNT"},{"3", "SPECIFIC_ALLOCATION_ACCOUNT_STANDING"},{"1", "STANDING_INSTRUCTIONS_PROVIDED"},{"2", "SPECIFIC_ALLOCATION_ACCOUNT_OVERRIDING"}], "TagNum" => "160"}


,
"AllocText" => #{"TagNum" => "161" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "161" => #{"Name"=>"AllocText" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "161"}


,
"SettlInstID" => #{"TagNum" => "162" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "162" => #{"Name"=>"SettlInstID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "162"}


,
"SettlInstTransType" => #{"TagNum" => "163" ,"Type" => "CHAR" ,"ValidValues" =>[{"N", "NEW"},{"R", "REPLACE"},{"C", "CANCEL"}]}
, "163" => #{"Name"=>"SettlInstTransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"N", "NEW"},{"R", "REPLACE"},{"C", "CANCEL"}], "TagNum" => "163"}


,
"EmailThreadID" => #{"TagNum" => "164" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "164" => #{"Name"=>"EmailThreadID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "164"}


,
"SettlInstSource" => #{"TagNum" => "165" ,"Type" => "CHAR" ,"ValidValues" =>[{"2", "INSTITUTIONS_INSTRUCTIONS"},{"3", "INVESTOR"},{"1", "BROKERS_INSTRUCTIONS"}]}
, "165" => #{"Name"=>"SettlInstSource" ,"Type"=>"CHAR" ,"ValidValues"=>[{"2", "INSTITUTIONS_INSTRUCTIONS"},{"3", "INVESTOR"},{"1", "BROKERS_INSTRUCTIONS"}], "TagNum" => "165"}


,
"SecurityType" => #{"TagNum" => "167" ,"Type" => "STRING" ,"ValidValues" =>[{"CP", "COMMERCIAL_PAPER"},{"VRDN", "VARIABLE_RATE_DEMAND_NOTE"},{"PZFJ", "PLAZOS_FIJOS"},{"PN", "PROMISSORY_NOTE"},{"ONITE", "OVERNIGHT"},{"MTN", "MEDIUM_TERM_NOTES"},{"TECP", "TAX_EXEMPT_COMMERCIAL_PAPER"},{"AMENDED", "AMENDED_RESTATED"},{"BRIDGE", "BRIDGE_LOAN"},{"LOFC", "LETTER_OF_CREDIT"},{"SWING", "SWING_LINE_FACILITY"},{"DINP", "DEBTOR_IN_POSSESSION"},{"DEFLTED", "DEFAULTED"},{"WITHDRN", "WITHDRAWN"},{"LQN", "LIQUIDITY_NOTE"},{"MATURED", "MATURED"},{"DN", "DEPOSIT_NOTES"},{"RETIRED", "RETIRED"},{"BA", "BANKERS_ACCEPTANCE"},{"BN", "BANK_NOTES"},{"BOX", "BILL_OF_EXCHANGES"},{"CD", "CERTIFICATE_OF_DEPOSIT"},{"CL", "CALL_LOANS"},{"REPLACD", "REPLACED"},{"MT", "MANDATORY_TENDER"},{"RVLVTRM", "REVOLVER_TERM_LOAN"},{"MPP", "MORTGAGE_PRIVATE_PLACEMENT"},{"STN", "SHORT_TERM_LOAN_NOTE"},{"MPT", "MISCELLANEOUS_PASS_THROUGH"},{"TBA", "TO_BE_ANNOUNCED"},{"AN", "OTHER_ANTICIPATION_NOTES_BAN_GAN_ETC"},{"MIO", "MORTGAGE_INTEREST_ONLY"},{"COFP", "CERTIFICATE_OF_PARTICIPATION"},{"MBS", "MORTGAGE_BACKED_SECURITIES"},{"REV", "REVENUE_BONDS"},{"SPCLA", "SPECIAL_ASSESSMENT"},{"SPCLO", "SPECIAL_OBLIGATION"},{"SPCLT", "SPECIAL_TAX"},{"TAN", "TAX_ANTICIPATION_NOTE"},{"TAXA", "TAX_ALLOCATION"},{"COFO", "CERTIFICATE_OF_OBLIGATION"},{"TD", "TIME_DEPOSIT"},{"GO", "GENERAL_OBLIGATION_BONDS"},{"?", "WILDCARD_ENTRY"},{"WAR", "WARRANT"},{"MF", "MUTUAL_FUND"},{"MLEG", "MULTI_LEG_INSTRUMENT"},{"TRAN", "TAX_REVENUE_ANTICIPATION_NOTE"},{"MPO", "MORTGAGE_PRINCIPAL_ONLY"},{"RP", "REPURCHASE_AGREEMENT"},{"NONE", "NO_SECURITY_TYPE"},{"XCN", "EXTENDED_COMM_NOTE"},{"POOL", "AGENCY_POOLS"},{"ABS", "ASSET_BACKED_SECURITIES"},{"CMBS", "CORP_MORTGAGE_BACKED_SECURITIES"},{"CMO", "COLLATERALIZED_MORTGAGE_OBLIGATION"},{"IET", "IOETTE_MORTGAGE"},{"RVRP", "REVERSE_REPURCHASE_AGREEMENT"},{"FOR", "FOREIGN_EXCHANGE_CONTRACT"},{"RAN", "REVENUE_ANTICIPATION_NOTE"},{"RVLV", "REVOLVER_LOAN"},{"FAC", "FEDERAL_AGENCY_COUPON"},{"FADN", "FEDERAL_AGENCY_DISCOUNT_NOTE"},{"PEF", "PRIVATE_EXPORT_FUNDING"},{"CORP", "CORPORATE_BOND"},{"CPP", "CORPORATE_PRIVATE_PLACEMENT"},{"CB", "CONVERTIBLE_BOND"},{"DUAL", "DUAL_CURRENCY"},{"XLINKD", "INDEXED_LINKED"},{"YANK", "YANKEE_CORPORATE_BOND"},{"CS", "COMMON_STOCK"},{"PS", "PREFERRED_STOCK"},{"BRADY", "BRADY_BOND"},{"TBOND", "US_TREASURY_BOND"},{"TINT", "INTEREST_STRIP_FROM_ANY_BOND_OR_NOTE"},{"TIPS", "TREASURY_INFLATION_PROTECTED_SECURITIES"},{"TCAL", "PRINCIPAL_STRIP_OF_A_CALLABLE_BOND_OR_NOTE"},{"TPRN", "PRINCIPAL_STRIP_FROM_A_NON_CALLABLE_BOND_OR_NOTE"},{"UST", "US_TREASURY_NOTE_BOND"},{"USTB", "US_TREASURY_BILL"},{"TERM", "TERM_LOAN"},{"STRUCT", "STRUCTURED_NOTES"}]}
, "167" => #{"Name"=>"SecurityType" ,"Type"=>"STRING" ,"ValidValues"=>[{"CP", "COMMERCIAL_PAPER"},{"VRDN", "VARIABLE_RATE_DEMAND_NOTE"},{"PZFJ", "PLAZOS_FIJOS"},{"PN", "PROMISSORY_NOTE"},{"ONITE", "OVERNIGHT"},{"MTN", "MEDIUM_TERM_NOTES"},{"TECP", "TAX_EXEMPT_COMMERCIAL_PAPER"},{"AMENDED", "AMENDED_RESTATED"},{"BRIDGE", "BRIDGE_LOAN"},{"LOFC", "LETTER_OF_CREDIT"},{"SWING", "SWING_LINE_FACILITY"},{"DINP", "DEBTOR_IN_POSSESSION"},{"DEFLTED", "DEFAULTED"},{"WITHDRN", "WITHDRAWN"},{"LQN", "LIQUIDITY_NOTE"},{"MATURED", "MATURED"},{"DN", "DEPOSIT_NOTES"},{"RETIRED", "RETIRED"},{"BA", "BANKERS_ACCEPTANCE"},{"BN", "BANK_NOTES"},{"BOX", "BILL_OF_EXCHANGES"},{"CD", "CERTIFICATE_OF_DEPOSIT"},{"CL", "CALL_LOANS"},{"REPLACD", "REPLACED"},{"MT", "MANDATORY_TENDER"},{"RVLVTRM", "REVOLVER_TERM_LOAN"},{"MPP", "MORTGAGE_PRIVATE_PLACEMENT"},{"STN", "SHORT_TERM_LOAN_NOTE"},{"MPT", "MISCELLANEOUS_PASS_THROUGH"},{"TBA", "TO_BE_ANNOUNCED"},{"AN", "OTHER_ANTICIPATION_NOTES_BAN_GAN_ETC"},{"MIO", "MORTGAGE_INTEREST_ONLY"},{"COFP", "CERTIFICATE_OF_PARTICIPATION"},{"MBS", "MORTGAGE_BACKED_SECURITIES"},{"REV", "REVENUE_BONDS"},{"SPCLA", "SPECIAL_ASSESSMENT"},{"SPCLO", "SPECIAL_OBLIGATION"},{"SPCLT", "SPECIAL_TAX"},{"TAN", "TAX_ANTICIPATION_NOTE"},{"TAXA", "TAX_ALLOCATION"},{"COFO", "CERTIFICATE_OF_OBLIGATION"},{"TD", "TIME_DEPOSIT"},{"GO", "GENERAL_OBLIGATION_BONDS"},{"?", "WILDCARD_ENTRY"},{"WAR", "WARRANT"},{"MF", "MUTUAL_FUND"},{"MLEG", "MULTI_LEG_INSTRUMENT"},{"TRAN", "TAX_REVENUE_ANTICIPATION_NOTE"},{"MPO", "MORTGAGE_PRINCIPAL_ONLY"},{"RP", "REPURCHASE_AGREEMENT"},{"NONE", "NO_SECURITY_TYPE"},{"XCN", "EXTENDED_COMM_NOTE"},{"POOL", "AGENCY_POOLS"},{"ABS", "ASSET_BACKED_SECURITIES"},{"CMBS", "CORP_MORTGAGE_BACKED_SECURITIES"},{"CMO", "COLLATERALIZED_MORTGAGE_OBLIGATION"},{"IET", "IOETTE_MORTGAGE"},{"RVRP", "REVERSE_REPURCHASE_AGREEMENT"},{"FOR", "FOREIGN_EXCHANGE_CONTRACT"},{"RAN", "REVENUE_ANTICIPATION_NOTE"},{"RVLV", "REVOLVER_LOAN"},{"FAC", "FEDERAL_AGENCY_COUPON"},{"FADN", "FEDERAL_AGENCY_DISCOUNT_NOTE"},{"PEF", "PRIVATE_EXPORT_FUNDING"},{"CORP", "CORPORATE_BOND"},{"CPP", "CORPORATE_PRIVATE_PLACEMENT"},{"CB", "CONVERTIBLE_BOND"},{"DUAL", "DUAL_CURRENCY"},{"XLINKD", "INDEXED_LINKED"},{"YANK", "YANKEE_CORPORATE_BOND"},{"CS", "COMMON_STOCK"},{"PS", "PREFERRED_STOCK"},{"BRADY", "BRADY_BOND"},{"TBOND", "US_TREASURY_BOND"},{"TINT", "INTEREST_STRIP_FROM_ANY_BOND_OR_NOTE"},{"TIPS", "TREASURY_INFLATION_PROTECTED_SECURITIES"},{"TCAL", "PRINCIPAL_STRIP_OF_A_CALLABLE_BOND_OR_NOTE"},{"TPRN", "PRINCIPAL_STRIP_FROM_A_NON_CALLABLE_BOND_OR_NOTE"},{"UST", "US_TREASURY_NOTE_BOND"},{"USTB", "US_TREASURY_BILL"},{"TERM", "TERM_LOAN"},{"STRUCT", "STRUCTURED_NOTES"}], "TagNum" => "167"}


,
"EffectiveTime" => #{"TagNum" => "168" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "168" => #{"Name"=>"EffectiveTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "168"}


,
"StandInstDbType" => #{"TagNum" => "169" ,"Type" => "INT" ,"ValidValues" =>[{"0", "OTHER"},{"1", "DTC_SID"},{"3", "A_GLOBAL_CUSTODIAN"},{"2", "THOMSON_ALERT"}]}
, "169" => #{"Name"=>"StandInstDbType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "OTHER"},{"1", "DTC_SID"},{"3", "A_GLOBAL_CUSTODIAN"},{"2", "THOMSON_ALERT"}], "TagNum" => "169"}


,
"StandInstDbName" => #{"TagNum" => "170" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "170" => #{"Name"=>"StandInstDbName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "170"}


,
"StandInstDbID" => #{"TagNum" => "171" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "171" => #{"Name"=>"StandInstDbID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "171"}


,
"SettlDeliveryType" => #{"TagNum" => "172" ,"Type" => "INT" ,"ValidValues" =>[{"1", "FREE"},{"0", "VERSUS_PAYMENT"}]}
, "172" => #{"Name"=>"SettlDeliveryType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "FREE"},{"0", "VERSUS_PAYMENT"}], "TagNum" => "172"}


,
"SettlDepositoryCode" => #{"TagNum" => "173" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "173" => #{"Name"=>"SettlDepositoryCode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "173"}


,
"SettlBrkrCode" => #{"TagNum" => "174" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "174" => #{"Name"=>"SettlBrkrCode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "174"}


,
"SettlInstCode" => #{"TagNum" => "175" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "175" => #{"Name"=>"SettlInstCode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "175"}


,
"SecuritySettlAgentName" => #{"TagNum" => "176" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "176" => #{"Name"=>"SecuritySettlAgentName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "176"}


,
"SecuritySettlAgentCode" => #{"TagNum" => "177" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "177" => #{"Name"=>"SecuritySettlAgentCode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "177"}


,
"SecuritySettlAgentAcctNum" => #{"TagNum" => "178" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "178" => #{"Name"=>"SecuritySettlAgentAcctNum" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "178"}


,
"SecuritySettlAgentAcctName" => #{"TagNum" => "179" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "179" => #{"Name"=>"SecuritySettlAgentAcctName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "179"}


,
"SecuritySettlAgentContactName" => #{"TagNum" => "180" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "180" => #{"Name"=>"SecuritySettlAgentContactName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "180"}


,
"SecuritySettlAgentContactPhone" => #{"TagNum" => "181" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "181" => #{"Name"=>"SecuritySettlAgentContactPhone" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "181"}


,
"CashSettlAgentName" => #{"TagNum" => "182" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "182" => #{"Name"=>"CashSettlAgentName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "182"}


,
"CashSettlAgentCode" => #{"TagNum" => "183" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "183" => #{"Name"=>"CashSettlAgentCode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "183"}


,
"CashSettlAgentAcctNum" => #{"TagNum" => "184" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "184" => #{"Name"=>"CashSettlAgentAcctNum" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "184"}


,
"CashSettlAgentAcctName" => #{"TagNum" => "185" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "185" => #{"Name"=>"CashSettlAgentAcctName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "185"}


,
"CashSettlAgentContactName" => #{"TagNum" => "186" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "186" => #{"Name"=>"CashSettlAgentContactName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "186"}


,
"CashSettlAgentContactPhone" => #{"TagNum" => "187" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "187" => #{"Name"=>"CashSettlAgentContactPhone" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "187"}


,
"BidSpotRate" => #{"TagNum" => "188" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "188" => #{"Name"=>"BidSpotRate" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "188"}


,
"BidForwardPoints" => #{"TagNum" => "189" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "189" => #{"Name"=>"BidForwardPoints" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "189"}


,
"OfferSpotRate" => #{"TagNum" => "190" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "190" => #{"Name"=>"OfferSpotRate" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "190"}


,
"OfferForwardPoints" => #{"TagNum" => "191" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "191" => #{"Name"=>"OfferForwardPoints" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "191"}


,
"OrderQty2" => #{"TagNum" => "192" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "192" => #{"Name"=>"OrderQty2" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "192"}


,
"FutSettDate2" => #{"TagNum" => "193" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "193" => #{"Name"=>"FutSettDate2" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "193"}


,
"LastSpotRate" => #{"TagNum" => "194" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "194" => #{"Name"=>"LastSpotRate" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "194"}


,
"LastForwardPoints" => #{"TagNum" => "195" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "195" => #{"Name"=>"LastForwardPoints" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "195"}


,
"AllocLinkID" => #{"TagNum" => "196" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "196" => #{"Name"=>"AllocLinkID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "196"}


,
"AllocLinkType" => #{"TagNum" => "197" ,"Type" => "INT" ,"ValidValues" =>[{"0", "F_X_NETTING"},{"1", "F_X_SWAP"}]}
, "197" => #{"Name"=>"AllocLinkType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "F_X_NETTING"},{"1", "F_X_SWAP"}], "TagNum" => "197"}


,
"SecondaryOrderID" => #{"TagNum" => "198" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "198" => #{"Name"=>"SecondaryOrderID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "198"}


,
"NoIOIQualifiers" => #{"TagNum" => "199" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "199" => #{"Name"=>"NoIOIQualifiers" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "199"}


,
"MaturityMonthYear" => #{"TagNum" => "200" ,"Type" => "MONTHYEAR" ,"ValidValues" =>[]}
, "200" => #{"Name"=>"MaturityMonthYear" ,"Type"=>"MONTHYEAR" ,"ValidValues"=>[], "TagNum" => "200"}


,
"StrikePrice" => #{"TagNum" => "202" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "202" => #{"Name"=>"StrikePrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "202"}


,
"CoveredOrUncovered" => #{"TagNum" => "203" ,"Type" => "INT" ,"ValidValues" =>[{"1", "UNCOVERED"},{"0", "COVERED"}]}
, "203" => #{"Name"=>"CoveredOrUncovered" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "UNCOVERED"},{"0", "COVERED"}], "TagNum" => "203"}


,
"OptAttribute" => #{"TagNum" => "206" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "206" => #{"Name"=>"OptAttribute" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "206"}


,
"SecurityExchange" => #{"TagNum" => "207" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "207" => #{"Name"=>"SecurityExchange" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "207"}


,
"NotifyBrokerOfCredit" => #{"TagNum" => "208" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "208" => #{"Name"=>"NotifyBrokerOfCredit" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "208"}


,
"AllocHandlInst" => #{"TagNum" => "209" ,"Type" => "INT" ,"ValidValues" =>[{"3", "FORWARD_AND_MATCH"},{"2", "FORWARD"},{"1", "MATCH"}]}
, "209" => #{"Name"=>"AllocHandlInst" ,"Type"=>"INT" ,"ValidValues"=>[{"3", "FORWARD_AND_MATCH"},{"2", "FORWARD"},{"1", "MATCH"}], "TagNum" => "209"}


,
"MaxShow" => #{"TagNum" => "210" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "210" => #{"Name"=>"MaxShow" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "210"}


,
"PegDifference" => #{"TagNum" => "211" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "211" => #{"Name"=>"PegDifference" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "211"}


,
"XmlDataLen" => #{"TagNum" => "212" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "212" => #{"Name"=>"XmlDataLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "212"}


,
"XmlData" => #{"TagNum" => "213" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "213" => #{"Name"=>"XmlData" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "213"}


,
"SettlInstRefID" => #{"TagNum" => "214" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "214" => #{"Name"=>"SettlInstRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "214"}


,
"NoRoutingIDs" => #{"TagNum" => "215" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "215" => #{"Name"=>"NoRoutingIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "215"}


,
"RoutingType" => #{"TagNum" => "216" ,"Type" => "INT" ,"ValidValues" =>[{"1", "TARGET_FIRM"},{"2", "TARGET_LIST"},{"3", "BLOCK_FIRM"},{"4", "BLOCK_LIST"}]}
, "216" => #{"Name"=>"RoutingType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "TARGET_FIRM"},{"2", "TARGET_LIST"},{"3", "BLOCK_FIRM"},{"4", "BLOCK_LIST"}], "TagNum" => "216"}


,
"RoutingID" => #{"TagNum" => "217" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "217" => #{"Name"=>"RoutingID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "217"}


,
"Spread" => #{"TagNum" => "218" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "218" => #{"Name"=>"Spread" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "218"}


,
"Benchmark" => #{"TagNum" => "219" ,"Type" => "CHAR" ,"ValidValues" =>[{"5", "OLD_10"},{"1", "CURVE"},{"2", "5_YR"},{"4", "10_YR"},{"6", "30_YR"},{"7", "OLD_30"},{"8", "3_MO_LIBOR"},{"9", "6_MO_LIBOR"},{"3", "OLD_5"}]}
, "219" => #{"Name"=>"Benchmark" ,"Type"=>"CHAR" ,"ValidValues"=>[{"5", "OLD_10"},{"1", "CURVE"},{"2", "5_YR"},{"4", "10_YR"},{"6", "30_YR"},{"7", "OLD_30"},{"8", "3_MO_LIBOR"},{"9", "6_MO_LIBOR"},{"3", "OLD_5"}], "TagNum" => "219"}


,
"BenchmarkCurveCurrency" => #{"TagNum" => "220" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "220" => #{"Name"=>"BenchmarkCurveCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "220"}


,
"BenchmarkCurveName" => #{"TagNum" => "221" ,"Type" => "STRING" ,"ValidValues" =>[{"SWAP", "SWAP"},{"LIBID", "LIBID"},{"OTHER", "OTHER"},{"Treasury", "TREASURY"},{"Euribor", "EURIBOR"},{"Pfandbriefe", "PFANDBRIEFE"},{"FutureSWAP", "FUTURESWAP"},{"MuniAAA", "MUNIAAA"},{"LIBOR", "LIBOR"}]}
, "221" => #{"Name"=>"BenchmarkCurveName" ,"Type"=>"STRING" ,"ValidValues"=>[{"SWAP", "SWAP"},{"LIBID", "LIBID"},{"OTHER", "OTHER"},{"Treasury", "TREASURY"},{"Euribor", "EURIBOR"},{"Pfandbriefe", "PFANDBRIEFE"},{"FutureSWAP", "FUTURESWAP"},{"MuniAAA", "MUNIAAA"},{"LIBOR", "LIBOR"}], "TagNum" => "221"}


,
"BenchmarkCurvePoint" => #{"TagNum" => "222" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "222" => #{"Name"=>"BenchmarkCurvePoint" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "222"}


,
"CouponRate" => #{"TagNum" => "223" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "223" => #{"Name"=>"CouponRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "223"}


,
"CouponPaymentDate" => #{"TagNum" => "224" ,"Type" => "UTCDATE" ,"ValidValues" =>[]}
, "224" => #{"Name"=>"CouponPaymentDate" ,"Type"=>"UTCDATE" ,"ValidValues"=>[], "TagNum" => "224"}


,
"IssueDate" => #{"TagNum" => "225" ,"Type" => "UTCDATE" ,"ValidValues" =>[]}
, "225" => #{"Name"=>"IssueDate" ,"Type"=>"UTCDATE" ,"ValidValues"=>[], "TagNum" => "225"}


,
"RepurchaseTerm" => #{"TagNum" => "226" ,"Type" => "INT" ,"ValidValues" =>[]}
, "226" => #{"Name"=>"RepurchaseTerm" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "226"}


,
"RepurchaseRate" => #{"TagNum" => "227" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "227" => #{"Name"=>"RepurchaseRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "227"}


,
"Factor" => #{"TagNum" => "228" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "228" => #{"Name"=>"Factor" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "228"}


,
"TradeOriginationDate" => #{"TagNum" => "229" ,"Type" => "UTCDATE" ,"ValidValues" =>[]}
, "229" => #{"Name"=>"TradeOriginationDate" ,"Type"=>"UTCDATE" ,"ValidValues"=>[], "TagNum" => "229"}


,
"ExDate" => #{"TagNum" => "230" ,"Type" => "UTCDATE" ,"ValidValues" =>[]}
, "230" => #{"Name"=>"ExDate" ,"Type"=>"UTCDATE" ,"ValidValues"=>[], "TagNum" => "230"}


,
"ContractMultiplier" => #{"TagNum" => "231" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "231" => #{"Name"=>"ContractMultiplier" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "231"}


,
"NoStipulations" => #{"TagNum" => "232" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "232" => #{"Name"=>"NoStipulations" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "232"}


,
"StipulationType" => #{"TagNum" => "233" ,"Type" => "STRING" ,"ValidValues" =>[{"ABS", "ABSOLUTE_PREPAYMENT_SPEED"},{"WALA", "WEIGHTED_AVERAGE_LOAN_AGE"},{"WAM", "WEIGHTED_AVERAGE_MATURITY"},{"CPR", "CONSTANT_PREPAYMENT_RATE"},{"HEP", "FINAL_CPR_OF_HOME_EQUITY_PREPAYMENT_CURVE"},{"WAL", "WEIGHTED_AVERAGE_LIFE"},{"MHP", "OF_MANUFACTURED_HOUSING_PREPAYMENT_CURVE"},{"SMM", "SINGLE_MONTHLY_MORTALITY"},{"MPR", "MONTHLY_PREPAYMENT_RATE"},{"PSA", "OF_BMA_PREPAYMENT_CURVE"},{"PPC", "OF_PROSPECTUS_PREPAYMENT_CURVE"},{"CPP", "CONSTANT_PREPAYMENT_PENALTY"},{"LOTVAR", "LOT_VARIANCE"},{"CPY", "CONSTANT_PREPAYMENT_YIELD"},{"WAC", "WEIGHTED_AVERAGE_COUPON"},{"ISSUE", "YEAR_OF_ISSUE"},{"MAT", "MATURITY_YEAR"},{"PIECES", "NUMBER_OF_PIECES"},{"PMAX", "POOLS_MAXIMUM"},{"PPM", "POOLS_PER_MILLION"},{"PPL", "POOLS_PER_LOT"},{"PPT", "POOLS_PER_TRADE"},{"PROD", "PRODUCTION_YEAR"},{"TRDVAR", "TRADE_VARIANCE"},{"GEOG", "GEOGRAPHICS"}]}
, "233" => #{"Name"=>"StipulationType" ,"Type"=>"STRING" ,"ValidValues"=>[{"ABS", "ABSOLUTE_PREPAYMENT_SPEED"},{"WALA", "WEIGHTED_AVERAGE_LOAN_AGE"},{"WAM", "WEIGHTED_AVERAGE_MATURITY"},{"CPR", "CONSTANT_PREPAYMENT_RATE"},{"HEP", "FINAL_CPR_OF_HOME_EQUITY_PREPAYMENT_CURVE"},{"WAL", "WEIGHTED_AVERAGE_LIFE"},{"MHP", "OF_MANUFACTURED_HOUSING_PREPAYMENT_CURVE"},{"SMM", "SINGLE_MONTHLY_MORTALITY"},{"MPR", "MONTHLY_PREPAYMENT_RATE"},{"PSA", "OF_BMA_PREPAYMENT_CURVE"},{"PPC", "OF_PROSPECTUS_PREPAYMENT_CURVE"},{"CPP", "CONSTANT_PREPAYMENT_PENALTY"},{"LOTVAR", "LOT_VARIANCE"},{"CPY", "CONSTANT_PREPAYMENT_YIELD"},{"WAC", "WEIGHTED_AVERAGE_COUPON"},{"ISSUE", "YEAR_OF_ISSUE"},{"MAT", "MATURITY_YEAR"},{"PIECES", "NUMBER_OF_PIECES"},{"PMAX", "POOLS_MAXIMUM"},{"PPM", "POOLS_PER_MILLION"},{"PPL", "POOLS_PER_LOT"},{"PPT", "POOLS_PER_TRADE"},{"PROD", "PRODUCTION_YEAR"},{"TRDVAR", "TRADE_VARIANCE"},{"GEOG", "GEOGRAPHICS"}], "TagNum" => "233"}


,
"StipulationValue" => #{"TagNum" => "234" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "234" => #{"Name"=>"StipulationValue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "234"}


,
"YieldType" => #{"TagNum" => "235" ,"Type" => "STRING" ,"ValidValues" =>[{"TRUE", "TRUE_YIELD_THE_YIELD_CALCULATED_WITH_COUPON_DATES_MOVED_FROM_A_WEEKEND_OR_HOLIDAY_TO_THE_NEXT_VALID_SETTLEMENT_DATE"},{"PREVCLOSE", "PREVIOUS_CLOSE_YIELD_THE_YIELD_OF_A_BOND_BASED_ON_THE_CLOSING_PRICE_1_DAY_AGO"},{"LONGEST", "YIELD_TO_LONGEST_AVERAGE"},{"LONGAVGLIFE", "YIELD_TO_LONGEST_AVERAGE_LIFE_THE_YIELD_ASSUMING_ONLY_MANDATORY_SINKS_ARE_TAKEN_THIS_RESULTS_IN_A_LOWER_PAYDOWN_OF_DEBT_THE_YIELD_IS_THEN_CALCULATED_TO_THE_FINAL_PAYMENT_DATE"},{"MATURITY", "YIELD_TO_MATURITY_THE_YIELD_OF_A_BOND_TO_ITS_MATURITY_DATE"},{"MARK", "MARK_TO_MARKET_YIELD_AN_ADJUSTMENT_IN_THE_VALUATION_OF_A_SECURITIES_PORTFOLIO_TO_REFLECT_THE_CURRENT_MARKET_VALUES_OF_THE_RESPECTIVE_SECURITIES_IN_THE_PORTFOLIO"},{"OPENAVG", "OPEN_AVERAGE_YIELD_THE_AVERAGE_YIELD_OF_THE_RESPECTIVE_SECURITIES_IN_THE_PORTFOLIO"},{"PUT", "YIELD_TO_NEXT_PUT_THE_YIELD_TO_THE_DATE_AT_WHICH_THE_BOND_HOLDER_CAN_NEXT_PUT_THE_BOND_TO_THE_ISSUER"},{"PROCEEDS", "PROCEEDS_YIELD_THE_CD_EQUIVALENT_YIELD_WHEN_THE_REMAINING_TIME_TO_MATURITY_IS_LESS_THAN_TWO_YEARS"},{"SEMIANNUAL", "SEMI_ANNUAL_YIELD_THE_YIELD_OF_A_BOND_WHOSE_COUPON_PAYMENTS_ARE_REINVESTED_SEMI_ANNUALLY"},{"SHORTAVGLIFE", "YIELD_TO_SHORTEST_AVERAGE_LIFE_SAME_AS_AVGLIFE_ABOVE"},{"SHORTEST", "YIELD_TO_SHORTEST_AVERAGE"},{"SIMPLE", "SIMPLE_YIELD_THE_YIELD_OF_A_BOND_ASSUMING_NO_REINVESTMENT_OF_COUPON_PAYMENTS"},{"TENDER", "YIELD_TO_TENDER_DATE_THE_YIELD_ON_A_MUNICIPAL_BOND_TO_ITS_MANDATORY_TENDER_DATE"},{"VALUE1/32", "YIELD_VALUE_OF_1_32_THE_AMOUNT_THAT_THE_YIELD_WILL_CHANGE_FOR_A_1_32ND_CHANGE_IN_PRICE"},{"WORST", "YIELD_TO_WORST_CONVENTION_THE_LOWEST_YIELD_TO_ALL_POSSIBLE_REDEMPTION_DATE_SCENARIOS"},{"TAXEQUIV", "TAX_EQUIVALENT_YIELD_THE_AFTER_TAX_YIELD_GROSSED_UP_BY_THE_MAXIMUM_FEDERAL_TAX_RATE_OF_396_FOR_COMPARISON_TO_TAXABLE_YIELDS"},{"ANNUAL", "ANNUAL_YIELD_THE_ANNUAL_INTEREST_OR_DIVIDEND_INCOME_AN_INVESTMENT_EARNS_EXPRESSED_AS_A_PERCENTAGE_OF_THE_INVESTMENTS_TOTAL_VALUE"},{"LASTYEAR", "CLOSING_YIELD_MOST_RECENT_YEAR_THE_YIELD_OF_A_BOND_BASED_ON_THE_CLOSING_PRICE_AS_OF_THE_MOST_RECENT_YEARS_END"},{"NEXTREFUND", "YIELD_TO_NEXT_REFUND"},{"AFTERTAX", "AFTER_TAX_YIELD"},{"ATISSUE", "YIELD_AT_ISSUE"},{"AVGLIFE", "YIELD_TO_AVERAGE_LIFE_THE_YIELD_ASSUMING_THAT_ALL_SINKS"},{"AVGMATURITY", "YIELD_TO_AVERAGE_MATURITY_THE_YIELD_ACHIEVED_BY_SUBSTITUTING_A_BONDS_AVERAGE_MATURITY_FOR_THE_ISSUES_FINAL_MATURITY_DATE"},{"BOOK", "BOOK_YIELD_THE_YIELD_OF_A_SECURITY_CALCULATED_BY_USING_ITS_BOOK_VALUE_INSTEAD_OF_THE_CURRENT_MARKET_PRICE_THIS_TERM_IS_TYPICALLY_USED_IN_THE_US_DOMESTIC_MARKET"},{"CALL", "YIELD_TO_NEXT_CALL_THE_YIELD_OF_A_BOND_TO_THE_NEXT_POSSIBLE_CALL_DATE"},{"CHANGE", "YIELD_CHANGE_SINCE_CLOSE_THE_CHANGE_IN_THE_YIELD_SINCE_THE_PREVIOUS_DAYS_CLOSING_YIELD"},{"COMPOUND", "COMPOUND_YIELD_THE_YIELD_OF_CERTAIN_JAPANESE_BONDS_BASED_ON_ITS_PRICE_CERTAIN_JAPANESE_BONDS_HAVE_IRREGULAR_FIRST_OR_LAST_COUPONS_AND_THE_YIELD_IS_CALCULATED_COMPOUND_FOR_THESE_IRREGULAR_PERIODS"},{"CURRENT", "CURRENT_YIELD_ANNUAL_INTEREST_ON_A_BOND_DIVIDED_BY_THE_MARKET_VALUE_THE_ACTUAL_INCOME_RATE_OF_RETURN_AS_OPPOSED_TO_THE_COUPON_RATE_EXPRESSED_AS_A_PERCENTAGE"},{"GROSS", "TRUE_GROSS_YIELD_YIELD_CALCULATED_USING_THE_PRICE_INCLUDING_ACCRUED_INTEREST_WHERE_COUPON_DATES_ARE_MOVED_FROM_HOLIDAYS_AND_WEEKENDS_TO_THE_NEXT_TRADING_DAY"},{"GOVTEQUIV", "GOVERNMENT_EQUIVALENT_YIELD_ASK_YIELD_BASED_ON_SEMI_ANNUAL_COUPONS_COMPOUNDING_IN_ALL_PERIODS_AND_ACTUAL_ACTUAL_CALENDAR"},{"INFLATION", "YIELD_WITH_INFLATION_ASSUMPTION_BASED_ON_PRICE_THE_RETURN_AN_INVESTOR_WOULD_REQUIRE_ON_A_NORMAL_BOND_THAT_WOULD_MAKE_THE_REAL_RETURN_EQUAL_TO_THAT_OF_THE_INFLATION_INDEXED_BOND_ASSUMING_A_CONSTANT_INFLATION_RATE"},{"INVERSEFLOATER", "INVERSE_FLOATER_BOND_YIELD_INVERSE_FLOATER_SEMI_ANNUAL_BOND_EQUIVALENT_RATE"},{"LASTQUARTER", "CLOSING_YIELD_MOST_RECENT_QUARTER_THE_YIELD_OF_A_BOND_BASED_ON_THE_CLOSING_PRICE_AS_OF_THE_MOST_RECENT_QUARTERS_END"},{"LASTCLOSE", "MOST_RECENT_CLOSING_YIELD_THE_LAST_AVAILABLE_YIELD_STORED_IN_HISTORY_COMPUTED_USING_PRICE"},{"LASTMONTH", "CLOSING_YIELD_MOST_RECENT_MONTH_THE_YIELD_OF_A_BOND_BASED_ON_THE_CLOSING_PRICE_AS_OF_THE_MOST_RECENT_MONTHS_END"},{"CLOSE", "CLOSING_YIELD_THE_YIELD_OF_A_BOND_BASED_ON_THE_CLOSING_PRICE"}]}
, "235" => #{"Name"=>"YieldType" ,"Type"=>"STRING" ,"ValidValues"=>[{"TRUE", "TRUE_YIELD_THE_YIELD_CALCULATED_WITH_COUPON_DATES_MOVED_FROM_A_WEEKEND_OR_HOLIDAY_TO_THE_NEXT_VALID_SETTLEMENT_DATE"},{"PREVCLOSE", "PREVIOUS_CLOSE_YIELD_THE_YIELD_OF_A_BOND_BASED_ON_THE_CLOSING_PRICE_1_DAY_AGO"},{"LONGEST", "YIELD_TO_LONGEST_AVERAGE"},{"LONGAVGLIFE", "YIELD_TO_LONGEST_AVERAGE_LIFE_THE_YIELD_ASSUMING_ONLY_MANDATORY_SINKS_ARE_TAKEN_THIS_RESULTS_IN_A_LOWER_PAYDOWN_OF_DEBT_THE_YIELD_IS_THEN_CALCULATED_TO_THE_FINAL_PAYMENT_DATE"},{"MATURITY", "YIELD_TO_MATURITY_THE_YIELD_OF_A_BOND_TO_ITS_MATURITY_DATE"},{"MARK", "MARK_TO_MARKET_YIELD_AN_ADJUSTMENT_IN_THE_VALUATION_OF_A_SECURITIES_PORTFOLIO_TO_REFLECT_THE_CURRENT_MARKET_VALUES_OF_THE_RESPECTIVE_SECURITIES_IN_THE_PORTFOLIO"},{"OPENAVG", "OPEN_AVERAGE_YIELD_THE_AVERAGE_YIELD_OF_THE_RESPECTIVE_SECURITIES_IN_THE_PORTFOLIO"},{"PUT", "YIELD_TO_NEXT_PUT_THE_YIELD_TO_THE_DATE_AT_WHICH_THE_BOND_HOLDER_CAN_NEXT_PUT_THE_BOND_TO_THE_ISSUER"},{"PROCEEDS", "PROCEEDS_YIELD_THE_CD_EQUIVALENT_YIELD_WHEN_THE_REMAINING_TIME_TO_MATURITY_IS_LESS_THAN_TWO_YEARS"},{"SEMIANNUAL", "SEMI_ANNUAL_YIELD_THE_YIELD_OF_A_BOND_WHOSE_COUPON_PAYMENTS_ARE_REINVESTED_SEMI_ANNUALLY"},{"SHORTAVGLIFE", "YIELD_TO_SHORTEST_AVERAGE_LIFE_SAME_AS_AVGLIFE_ABOVE"},{"SHORTEST", "YIELD_TO_SHORTEST_AVERAGE"},{"SIMPLE", "SIMPLE_YIELD_THE_YIELD_OF_A_BOND_ASSUMING_NO_REINVESTMENT_OF_COUPON_PAYMENTS"},{"TENDER", "YIELD_TO_TENDER_DATE_THE_YIELD_ON_A_MUNICIPAL_BOND_TO_ITS_MANDATORY_TENDER_DATE"},{"VALUE1/32", "YIELD_VALUE_OF_1_32_THE_AMOUNT_THAT_THE_YIELD_WILL_CHANGE_FOR_A_1_32ND_CHANGE_IN_PRICE"},{"WORST", "YIELD_TO_WORST_CONVENTION_THE_LOWEST_YIELD_TO_ALL_POSSIBLE_REDEMPTION_DATE_SCENARIOS"},{"TAXEQUIV", "TAX_EQUIVALENT_YIELD_THE_AFTER_TAX_YIELD_GROSSED_UP_BY_THE_MAXIMUM_FEDERAL_TAX_RATE_OF_396_FOR_COMPARISON_TO_TAXABLE_YIELDS"},{"ANNUAL", "ANNUAL_YIELD_THE_ANNUAL_INTEREST_OR_DIVIDEND_INCOME_AN_INVESTMENT_EARNS_EXPRESSED_AS_A_PERCENTAGE_OF_THE_INVESTMENTS_TOTAL_VALUE"},{"LASTYEAR", "CLOSING_YIELD_MOST_RECENT_YEAR_THE_YIELD_OF_A_BOND_BASED_ON_THE_CLOSING_PRICE_AS_OF_THE_MOST_RECENT_YEARS_END"},{"NEXTREFUND", "YIELD_TO_NEXT_REFUND"},{"AFTERTAX", "AFTER_TAX_YIELD"},{"ATISSUE", "YIELD_AT_ISSUE"},{"AVGLIFE", "YIELD_TO_AVERAGE_LIFE_THE_YIELD_ASSUMING_THAT_ALL_SINKS"},{"AVGMATURITY", "YIELD_TO_AVERAGE_MATURITY_THE_YIELD_ACHIEVED_BY_SUBSTITUTING_A_BONDS_AVERAGE_MATURITY_FOR_THE_ISSUES_FINAL_MATURITY_DATE"},{"BOOK", "BOOK_YIELD_THE_YIELD_OF_A_SECURITY_CALCULATED_BY_USING_ITS_BOOK_VALUE_INSTEAD_OF_THE_CURRENT_MARKET_PRICE_THIS_TERM_IS_TYPICALLY_USED_IN_THE_US_DOMESTIC_MARKET"},{"CALL", "YIELD_TO_NEXT_CALL_THE_YIELD_OF_A_BOND_TO_THE_NEXT_POSSIBLE_CALL_DATE"},{"CHANGE", "YIELD_CHANGE_SINCE_CLOSE_THE_CHANGE_IN_THE_YIELD_SINCE_THE_PREVIOUS_DAYS_CLOSING_YIELD"},{"COMPOUND", "COMPOUND_YIELD_THE_YIELD_OF_CERTAIN_JAPANESE_BONDS_BASED_ON_ITS_PRICE_CERTAIN_JAPANESE_BONDS_HAVE_IRREGULAR_FIRST_OR_LAST_COUPONS_AND_THE_YIELD_IS_CALCULATED_COMPOUND_FOR_THESE_IRREGULAR_PERIODS"},{"CURRENT", "CURRENT_YIELD_ANNUAL_INTEREST_ON_A_BOND_DIVIDED_BY_THE_MARKET_VALUE_THE_ACTUAL_INCOME_RATE_OF_RETURN_AS_OPPOSED_TO_THE_COUPON_RATE_EXPRESSED_AS_A_PERCENTAGE"},{"GROSS", "TRUE_GROSS_YIELD_YIELD_CALCULATED_USING_THE_PRICE_INCLUDING_ACCRUED_INTEREST_WHERE_COUPON_DATES_ARE_MOVED_FROM_HOLIDAYS_AND_WEEKENDS_TO_THE_NEXT_TRADING_DAY"},{"GOVTEQUIV", "GOVERNMENT_EQUIVALENT_YIELD_ASK_YIELD_BASED_ON_SEMI_ANNUAL_COUPONS_COMPOUNDING_IN_ALL_PERIODS_AND_ACTUAL_ACTUAL_CALENDAR"},{"INFLATION", "YIELD_WITH_INFLATION_ASSUMPTION_BASED_ON_PRICE_THE_RETURN_AN_INVESTOR_WOULD_REQUIRE_ON_A_NORMAL_BOND_THAT_WOULD_MAKE_THE_REAL_RETURN_EQUAL_TO_THAT_OF_THE_INFLATION_INDEXED_BOND_ASSUMING_A_CONSTANT_INFLATION_RATE"},{"INVERSEFLOATER", "INVERSE_FLOATER_BOND_YIELD_INVERSE_FLOATER_SEMI_ANNUAL_BOND_EQUIVALENT_RATE"},{"LASTQUARTER", "CLOSING_YIELD_MOST_RECENT_QUARTER_THE_YIELD_OF_A_BOND_BASED_ON_THE_CLOSING_PRICE_AS_OF_THE_MOST_RECENT_QUARTERS_END"},{"LASTCLOSE", "MOST_RECENT_CLOSING_YIELD_THE_LAST_AVAILABLE_YIELD_STORED_IN_HISTORY_COMPUTED_USING_PRICE"},{"LASTMONTH", "CLOSING_YIELD_MOST_RECENT_MONTH_THE_YIELD_OF_A_BOND_BASED_ON_THE_CLOSING_PRICE_AS_OF_THE_MOST_RECENT_MONTHS_END"},{"CLOSE", "CLOSING_YIELD_THE_YIELD_OF_A_BOND_BASED_ON_THE_CLOSING_PRICE"}], "TagNum" => "235"}


,
"Yield" => #{"TagNum" => "236" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "236" => #{"Name"=>"Yield" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "236"}


,
"TotalTakedown" => #{"TagNum" => "237" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "237" => #{"Name"=>"TotalTakedown" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "237"}


,
"Concession" => #{"TagNum" => "238" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "238" => #{"Name"=>"Concession" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "238"}


,
"RepoCollateralSecurityType" => #{"TagNum" => "239" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "239" => #{"Name"=>"RepoCollateralSecurityType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "239"}


,
"RedemptionDate" => #{"TagNum" => "240" ,"Type" => "UTCDATE" ,"ValidValues" =>[]}
, "240" => #{"Name"=>"RedemptionDate" ,"Type"=>"UTCDATE" ,"ValidValues"=>[], "TagNum" => "240"}


,
"UnderlyingCouponPaymentDate" => #{"TagNum" => "241" ,"Type" => "UTCDATE" ,"ValidValues" =>[]}
, "241" => #{"Name"=>"UnderlyingCouponPaymentDate" ,"Type"=>"UTCDATE" ,"ValidValues"=>[], "TagNum" => "241"}


,
"UnderlyingIssueDate" => #{"TagNum" => "242" ,"Type" => "UTCDATE" ,"ValidValues" =>[]}
, "242" => #{"Name"=>"UnderlyingIssueDate" ,"Type"=>"UTCDATE" ,"ValidValues"=>[], "TagNum" => "242"}


,
"UnderlyingRepoCollateralSecurityType" => #{"TagNum" => "243" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "243" => #{"Name"=>"UnderlyingRepoCollateralSecurityType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "243"}


,
"UnderlyingRepurchaseTerm" => #{"TagNum" => "244" ,"Type" => "INT" ,"ValidValues" =>[]}
, "244" => #{"Name"=>"UnderlyingRepurchaseTerm" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "244"}


,
"UnderlyingRepurchaseRate" => #{"TagNum" => "245" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "245" => #{"Name"=>"UnderlyingRepurchaseRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "245"}


,
"UnderlyingFactor" => #{"TagNum" => "246" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "246" => #{"Name"=>"UnderlyingFactor" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "246"}


,
"UnderlyingRedemptionDate" => #{"TagNum" => "247" ,"Type" => "UTCDATE" ,"ValidValues" =>[]}
, "247" => #{"Name"=>"UnderlyingRedemptionDate" ,"Type"=>"UTCDATE" ,"ValidValues"=>[], "TagNum" => "247"}


,
"LegCouponPaymentDate" => #{"TagNum" => "248" ,"Type" => "UTCDATE" ,"ValidValues" =>[]}
, "248" => #{"Name"=>"LegCouponPaymentDate" ,"Type"=>"UTCDATE" ,"ValidValues"=>[], "TagNum" => "248"}


,
"LegIssueDate" => #{"TagNum" => "249" ,"Type" => "UTCDATE" ,"ValidValues" =>[]}
, "249" => #{"Name"=>"LegIssueDate" ,"Type"=>"UTCDATE" ,"ValidValues"=>[], "TagNum" => "249"}


,
"LegRepoCollateralSecurityType" => #{"TagNum" => "250" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "250" => #{"Name"=>"LegRepoCollateralSecurityType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "250"}


,
"LegRepurchaseTerm" => #{"TagNum" => "251" ,"Type" => "INT" ,"ValidValues" =>[]}
, "251" => #{"Name"=>"LegRepurchaseTerm" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "251"}


,
"LegRepurchaseRate" => #{"TagNum" => "252" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "252" => #{"Name"=>"LegRepurchaseRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "252"}


,
"LegFactor" => #{"TagNum" => "253" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "253" => #{"Name"=>"LegFactor" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "253"}


,
"LegRedemptionDate" => #{"TagNum" => "254" ,"Type" => "UTCDATE" ,"ValidValues" =>[]}
, "254" => #{"Name"=>"LegRedemptionDate" ,"Type"=>"UTCDATE" ,"ValidValues"=>[], "TagNum" => "254"}


,
"CreditRating" => #{"TagNum" => "255" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "255" => #{"Name"=>"CreditRating" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "255"}


,
"UnderlyingCreditRating" => #{"TagNum" => "256" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "256" => #{"Name"=>"UnderlyingCreditRating" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "256"}


,
"LegCreditRating" => #{"TagNum" => "257" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "257" => #{"Name"=>"LegCreditRating" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "257"}


,
"TradedFlatSwitch" => #{"TagNum" => "258" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "258" => #{"Name"=>"TradedFlatSwitch" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "258"}


,
"BasisFeatureDate" => #{"TagNum" => "259" ,"Type" => "UTCDATE" ,"ValidValues" =>[]}
, "259" => #{"Name"=>"BasisFeatureDate" ,"Type"=>"UTCDATE" ,"ValidValues"=>[], "TagNum" => "259"}


,
"BasisFeaturePrice" => #{"TagNum" => "260" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "260" => #{"Name"=>"BasisFeaturePrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "260"}


,
"MDReqID" => #{"TagNum" => "262" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "262" => #{"Name"=>"MDReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "262"}


,
"SubscriptionRequestType" => #{"TagNum" => "263" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "SNAPSHOT_PLUS_UPDATES"},{"2", "DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST"},{"0", "SNAPSHOT"}]}
, "263" => #{"Name"=>"SubscriptionRequestType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "SNAPSHOT_PLUS_UPDATES"},{"2", "DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST"},{"0", "SNAPSHOT"}], "TagNum" => "263"}


,
"MarketDepth" => #{"TagNum" => "264" ,"Type" => "INT" ,"ValidValues" =>[]}
, "264" => #{"Name"=>"MarketDepth" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "264"}


,
"MDUpdateType" => #{"TagNum" => "265" ,"Type" => "INT" ,"ValidValues" =>[{"0", "FULL_REFRESH"},{"1", "INCREMENTAL_REFRESH"}]}
, "265" => #{"Name"=>"MDUpdateType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "FULL_REFRESH"},{"1", "INCREMENTAL_REFRESH"}], "TagNum" => "265"}


,
"AggregatedBook" => #{"TagNum" => "266" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "266" => #{"Name"=>"AggregatedBook" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "266"}


,
"NoMDEntryTypes" => #{"TagNum" => "267" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "267" => #{"Name"=>"NoMDEntryTypes" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "267"}


,
"NoMDEntries" => #{"TagNum" => "268" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "268" => #{"Name"=>"NoMDEntries" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "268"}


,
"MDEntryType" => #{"TagNum" => "269" ,"Type" => "CHAR" ,"ValidValues" =>[{"7", "TRADING_SESSION_HIGH_PRICE"},{"1", "OFFER"},{"A", "IMBALANCE"},{"9", "TRADING_SESSION_VWAP_PRICE"},{"8", "TRADING_SESSION_LOW_PRICE"},{"5", "CLOSING_PRICE"},{"4", "OPENING_PRICE"},{"0", "BID"},{"2", "TRADE"},{"3", "INDEX_VALUE"},{"6", "SETTLEMENT_PRICE"}]}
, "269" => #{"Name"=>"MDEntryType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"7", "TRADING_SESSION_HIGH_PRICE"},{"1", "OFFER"},{"A", "IMBALANCE"},{"9", "TRADING_SESSION_VWAP_PRICE"},{"8", "TRADING_SESSION_LOW_PRICE"},{"5", "CLOSING_PRICE"},{"4", "OPENING_PRICE"},{"0", "BID"},{"2", "TRADE"},{"3", "INDEX_VALUE"},{"6", "SETTLEMENT_PRICE"}], "TagNum" => "269"}


,
"MDEntryPx" => #{"TagNum" => "270" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "270" => #{"Name"=>"MDEntryPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "270"}


,
"MDEntrySize" => #{"TagNum" => "271" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "271" => #{"Name"=>"MDEntrySize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "271"}


,
"MDEntryDate" => #{"TagNum" => "272" ,"Type" => "UTCDATE" ,"ValidValues" =>[]}
, "272" => #{"Name"=>"MDEntryDate" ,"Type"=>"UTCDATE" ,"ValidValues"=>[], "TagNum" => "272"}


,
"MDEntryTime" => #{"TagNum" => "273" ,"Type" => "UTCTIMEONLY" ,"ValidValues" =>[]}
, "273" => #{"Name"=>"MDEntryTime" ,"Type"=>"UTCTIMEONLY" ,"ValidValues"=>[], "TagNum" => "273"}


,
"TickDirection" => #{"TagNum" => "274" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "PLUS_TICK"},{"1", "ZERO_PLUS_TICK"},{"2", "MINUS_TICK"},{"3", "ZERO_MINUS_TICK"}]}
, "274" => #{"Name"=>"TickDirection" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "PLUS_TICK"},{"1", "ZERO_PLUS_TICK"},{"2", "MINUS_TICK"},{"3", "ZERO_MINUS_TICK"}], "TagNum" => "274"}


,
"MDMkt" => #{"TagNum" => "275" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "275" => #{"Name"=>"MDMkt" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "275"}


,
"QuoteCondition" => #{"TagNum" => "276" ,"Type" => "MULTIPLEVALUESTRING" ,"ValidValues" =>[{"E", "LOCKED"},{"I", "NON_FIRM"},{"H", "FAST_TRADING"},{"F", "CROSSED"},{"D", "CONSOLIDATED_BEST"},{"C", "EXCHANGE_BEST"},{"B", "CLOSED"},{"A", "OPEN"},{"G", "DEPTH"}]}
, "276" => #{"Name"=>"QuoteCondition" ,"Type"=>"MULTIPLEVALUESTRING" ,"ValidValues"=>[{"E", "LOCKED"},{"I", "NON_FIRM"},{"H", "FAST_TRADING"},{"F", "CROSSED"},{"D", "CONSOLIDATED_BEST"},{"C", "EXCHANGE_BEST"},{"B", "CLOSED"},{"A", "OPEN"},{"G", "DEPTH"}], "TagNum" => "276"}


,
"TradeCondition" => #{"TagNum" => "277" ,"Type" => "MULTIPLEVALUESTRING" ,"ValidValues" =>[{"J", "NEXT_DAY_TRADE"},{"K", "OPENED"},{"L", "SELLER"},{"B", "AVERAGE_PRICE_TRADE"},{"M", "SOLD"},{"H", "RULE_155_TRADE"},{"N", "STOPPED_STOCK"},{"P", "IMBALANCE_MORE_BUYERS"},{"Q", "IMBALANCE_MORE_SELLERS"},{"R", "OPENING_PRICE"},{"I", "SOLD_LAST"},{"A", "CASH"},{"C", "CASH_TRADE"},{"E", "OPENING"},{"F", "INTRADAY_TRADE_DETAIL"},{"G", "RULE_127_TRADE"},{"D", "NEXT_DAY"}]}
, "277" => #{"Name"=>"TradeCondition" ,"Type"=>"MULTIPLEVALUESTRING" ,"ValidValues"=>[{"J", "NEXT_DAY_TRADE"},{"K", "OPENED"},{"L", "SELLER"},{"B", "AVERAGE_PRICE_TRADE"},{"M", "SOLD"},{"H", "RULE_155_TRADE"},{"N", "STOPPED_STOCK"},{"P", "IMBALANCE_MORE_BUYERS"},{"Q", "IMBALANCE_MORE_SELLERS"},{"R", "OPENING_PRICE"},{"I", "SOLD_LAST"},{"A", "CASH"},{"C", "CASH_TRADE"},{"E", "OPENING"},{"F", "INTRADAY_TRADE_DETAIL"},{"G", "RULE_127_TRADE"},{"D", "NEXT_DAY"}], "TagNum" => "277"}


,
"MDEntryID" => #{"TagNum" => "278" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "278" => #{"Name"=>"MDEntryID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "278"}


,
"MDUpdateAction" => #{"TagNum" => "279" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "CHANGE"},{"2", "DELETE"}]}
, "279" => #{"Name"=>"MDUpdateAction" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "CHANGE"},{"2", "DELETE"}], "TagNum" => "279"}


,
"MDEntryRefID" => #{"TagNum" => "280" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "280" => #{"Name"=>"MDEntryRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "280"}


,
"MDReqRejReason" => #{"TagNum" => "281" ,"Type" => "CHAR" ,"ValidValues" =>[{"7", "UNSUPPORTED_AGGREGATEDBOOK"},{"1", "DUPLICATE_MDREQID"},{"C", "UNSUPPORTED_MDIMPLICITDELETE"},{"B", "UNSUPPORTED_OPENCLOSESETTLEFLAG"},{"A", "UNSUPPORTED_SCOPE"},{"9", "UNSUPPORTED_TRADINGSESSIONID"},{"8", "UNSUPPORTED_MDENTRYTYPE"},{"6", "UNSUPPORTED_MDUPDATETYPE"},{"5", "UNSUPPORTED_MARKETDEPTH"},{"4", "UNSUPPORTED_SUBSCRIPTIONREQUESTTYPE"},{"2", "INSUFFICIENT_BANDWIDTH"},{"0", "UNKNOWN_SYMBOL"},{"3", "INSUFFICIENT_PERMISSIONS"}]}
, "281" => #{"Name"=>"MDReqRejReason" ,"Type"=>"CHAR" ,"ValidValues"=>[{"7", "UNSUPPORTED_AGGREGATEDBOOK"},{"1", "DUPLICATE_MDREQID"},{"C", "UNSUPPORTED_MDIMPLICITDELETE"},{"B", "UNSUPPORTED_OPENCLOSESETTLEFLAG"},{"A", "UNSUPPORTED_SCOPE"},{"9", "UNSUPPORTED_TRADINGSESSIONID"},{"8", "UNSUPPORTED_MDENTRYTYPE"},{"6", "UNSUPPORTED_MDUPDATETYPE"},{"5", "UNSUPPORTED_MARKETDEPTH"},{"4", "UNSUPPORTED_SUBSCRIPTIONREQUESTTYPE"},{"2", "INSUFFICIENT_BANDWIDTH"},{"0", "UNKNOWN_SYMBOL"},{"3", "INSUFFICIENT_PERMISSIONS"}], "TagNum" => "281"}


,
"MDEntryOriginator" => #{"TagNum" => "282" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "282" => #{"Name"=>"MDEntryOriginator" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "282"}


,
"LocationID" => #{"TagNum" => "283" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "283" => #{"Name"=>"LocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "283"}


,
"DeskID" => #{"TagNum" => "284" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "284" => #{"Name"=>"DeskID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "284"}


,
"DeleteReason" => #{"TagNum" => "285" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "CANCELATION"},{"1", "ERROR"}]}
, "285" => #{"Name"=>"DeleteReason" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "CANCELATION"},{"1", "ERROR"}], "TagNum" => "285"}


,
"OpenCloseSettleFlag" => #{"TagNum" => "286" ,"Type" => "MULTIPLEVALUESTRING" ,"ValidValues" =>[{"1", "SESSION_OPEN"},{"2", "DELIVERY_SETTLEMENT_PRICE"},{"3", "EXPECTED_PRICE"},{"4", "PRICE_FROM_PREVIOUS_BUSINESS_DAY"},{"0", "DAILY_OPEN"}]}
, "286" => #{"Name"=>"OpenCloseSettleFlag" ,"Type"=>"MULTIPLEVALUESTRING" ,"ValidValues"=>[{"1", "SESSION_OPEN"},{"2", "DELIVERY_SETTLEMENT_PRICE"},{"3", "EXPECTED_PRICE"},{"4", "PRICE_FROM_PREVIOUS_BUSINESS_DAY"},{"0", "DAILY_OPEN"}], "TagNum" => "286"}


,
"SellerDays" => #{"TagNum" => "287" ,"Type" => "INT" ,"ValidValues" =>[]}
, "287" => #{"Name"=>"SellerDays" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "287"}


,
"MDEntryBuyer" => #{"TagNum" => "288" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "288" => #{"Name"=>"MDEntryBuyer" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "288"}


,
"MDEntrySeller" => #{"TagNum" => "289" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "289" => #{"Name"=>"MDEntrySeller" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "289"}


,
"MDEntryPositionNo" => #{"TagNum" => "290" ,"Type" => "INT" ,"ValidValues" =>[]}
, "290" => #{"Name"=>"MDEntryPositionNo" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "290"}


,
"FinancialStatus" => #{"TagNum" => "291" ,"Type" => "MULTIPLEVALUESTRING" ,"ValidValues" =>[{"1", "BANKRUPT"},{"2", "PENDING_DELISTING"}]}
, "291" => #{"Name"=>"FinancialStatus" ,"Type"=>"MULTIPLEVALUESTRING" ,"ValidValues"=>[{"1", "BANKRUPT"},{"2", "PENDING_DELISTING"}], "TagNum" => "291"}


,
"CorporateAction" => #{"TagNum" => "292" ,"Type" => "MULTIPLEVALUESTRING" ,"ValidValues" =>[{"B", "EX_DISTRIBUTION"},{"E", "EX_INTEREST"},{"C", "EX_RIGHTS"},{"A", "EX_DIVIDEND"},{"D", "NEW"}]}
, "292" => #{"Name"=>"CorporateAction" ,"Type"=>"MULTIPLEVALUESTRING" ,"ValidValues"=>[{"B", "EX_DISTRIBUTION"},{"E", "EX_INTEREST"},{"C", "EX_RIGHTS"},{"A", "EX_DIVIDEND"},{"D", "NEW"}], "TagNum" => "292"}


,
"DefBidSize" => #{"TagNum" => "293" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "293" => #{"Name"=>"DefBidSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "293"}


,
"DefOfferSize" => #{"TagNum" => "294" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "294" => #{"Name"=>"DefOfferSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "294"}


,
"NoQuoteEntries" => #{"TagNum" => "295" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "295" => #{"Name"=>"NoQuoteEntries" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "295"}


,
"NoQuoteSets" => #{"TagNum" => "296" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "296" => #{"Name"=>"NoQuoteSets" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "296"}


,
"QuoteStatus" => #{"TagNum" => "297" ,"Type" => "INT" ,"ValidValues" =>[{"6", "REMOVED_FROM_MARKET"},{"1", "CANCELED_FOR_SYMBOL"},{"10", "PENDING"},{"9", "QUOTE_NOT_FOUND"},{"8", "QUERY"},{"7", "EXPIRED"},{"5", "REJECTED"},{"4", "CANCELED_ALL"},{"3", "CANCELED_FOR_UNDERLYING"},{"2", "CANCELED_FOR_SECURITY_TYPE"},{"0", "ACCEPTED"}]}
, "297" => #{"Name"=>"QuoteStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"6", "REMOVED_FROM_MARKET"},{"1", "CANCELED_FOR_SYMBOL"},{"10", "PENDING"},{"9", "QUOTE_NOT_FOUND"},{"8", "QUERY"},{"7", "EXPIRED"},{"5", "REJECTED"},{"4", "CANCELED_ALL"},{"3", "CANCELED_FOR_UNDERLYING"},{"2", "CANCELED_FOR_SECURITY_TYPE"},{"0", "ACCEPTED"}], "TagNum" => "297"}


,
"QuoteCancelType" => #{"TagNum" => "298" ,"Type" => "INT" ,"ValidValues" =>[{"4", "CANCEL_ALL_QUOTES"},{"2", "CANCEL_FOR_SECURITY_TYPE"},{"1", "CANCEL_FOR_SYMBOL"},{"3", "CANCEL_FOR_UNDERLYING_SYMBOL"}]}
, "298" => #{"Name"=>"QuoteCancelType" ,"Type"=>"INT" ,"ValidValues"=>[{"4", "CANCEL_ALL_QUOTES"},{"2", "CANCEL_FOR_SECURITY_TYPE"},{"1", "CANCEL_FOR_SYMBOL"},{"3", "CANCEL_FOR_UNDERLYING_SYMBOL"}], "TagNum" => "298"}


,
"QuoteEntryID" => #{"TagNum" => "299" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "299" => #{"Name"=>"QuoteEntryID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "299"}


,
"QuoteRejectReason" => #{"TagNum" => "300" ,"Type" => "INT" ,"ValidValues" =>[{"9", "NOT_AUTHORIZED_TO_QUOTE_SECURITY"},{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE"},{"3", "QUOTE_REQUEST_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "UNKNOWN_QUOTE"},{"6", "DUPLICATE_QUOTE"},{"7", "INVALID_BID_ASK_SPREAD"},{"8", "INVALID_PRICE"}]}
, "300" => #{"Name"=>"QuoteRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"9", "NOT_AUTHORIZED_TO_QUOTE_SECURITY"},{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE"},{"3", "QUOTE_REQUEST_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "UNKNOWN_QUOTE"},{"6", "DUPLICATE_QUOTE"},{"7", "INVALID_BID_ASK_SPREAD"},{"8", "INVALID_PRICE"}], "TagNum" => "300"}


,
"QuoteResponseLevel" => #{"TagNum" => "301" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ACKNOWLEDGE_ONLY_NEGATIVE_OR_ERRONEOUS_QUOTES"},{"0", "NO_ACKNOWLEDGEMENT"},{"2", "ACKNOWLEDGE_EACH_QUOTE_MESSAGES"}]}
, "301" => #{"Name"=>"QuoteResponseLevel" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ACKNOWLEDGE_ONLY_NEGATIVE_OR_ERRONEOUS_QUOTES"},{"0", "NO_ACKNOWLEDGEMENT"},{"2", "ACKNOWLEDGE_EACH_QUOTE_MESSAGES"}], "TagNum" => "301"}


,
"QuoteSetID" => #{"TagNum" => "302" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "302" => #{"Name"=>"QuoteSetID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "302"}


,
"QuoteRequestType" => #{"TagNum" => "303" ,"Type" => "INT" ,"ValidValues" =>[{"2", "AUTOMATIC"},{"1", "MANUAL"}]}
, "303" => #{"Name"=>"QuoteRequestType" ,"Type"=>"INT" ,"ValidValues"=>[{"2", "AUTOMATIC"},{"1", "MANUAL"}], "TagNum" => "303"}


,
"TotQuoteEntries" => #{"TagNum" => "304" ,"Type" => "INT" ,"ValidValues" =>[]}
, "304" => #{"Name"=>"TotQuoteEntries" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "304"}


,
"UnderlyingSecurityIDSource" => #{"TagNum" => "305" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "305" => #{"Name"=>"UnderlyingSecurityIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "305"}


,
"UnderlyingIssuer" => #{"TagNum" => "306" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "306" => #{"Name"=>"UnderlyingIssuer" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "306"}


,
"UnderlyingSecurityDesc" => #{"TagNum" => "307" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "307" => #{"Name"=>"UnderlyingSecurityDesc" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "307"}


,
"UnderlyingSecurityExchange" => #{"TagNum" => "308" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "308" => #{"Name"=>"UnderlyingSecurityExchange" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "308"}


,
"UnderlyingSecurityID" => #{"TagNum" => "309" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "309" => #{"Name"=>"UnderlyingSecurityID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "309"}


,
"UnderlyingSecurityType" => #{"TagNum" => "310" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "310" => #{"Name"=>"UnderlyingSecurityType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "310"}


,
"UnderlyingSymbol" => #{"TagNum" => "311" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "311" => #{"Name"=>"UnderlyingSymbol" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "311"}


,
"UnderlyingSymbolSfx" => #{"TagNum" => "312" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "312" => #{"Name"=>"UnderlyingSymbolSfx" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "312"}


,
"UnderlyingMaturityMonthYear" => #{"TagNum" => "313" ,"Type" => "MONTHYEAR" ,"ValidValues" =>[]}
, "313" => #{"Name"=>"UnderlyingMaturityMonthYear" ,"Type"=>"MONTHYEAR" ,"ValidValues"=>[], "TagNum" => "313"}


,
"UnderlyingPutOrCall" => #{"TagNum" => "315" ,"Type" => "INT" ,"ValidValues" =>[]}
, "315" => #{"Name"=>"UnderlyingPutOrCall" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "315"}


,
"UnderlyingStrikePrice" => #{"TagNum" => "316" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "316" => #{"Name"=>"UnderlyingStrikePrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "316"}


,
"UnderlyingOptAttribute" => #{"TagNum" => "317" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "317" => #{"Name"=>"UnderlyingOptAttribute" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "317"}


,
"SecurityReqID" => #{"TagNum" => "320" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "320" => #{"Name"=>"SecurityReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "320"}


,
"SecurityRequestType" => #{"TagNum" => "321" ,"Type" => "INT" ,"ValidValues" =>[{"0", "REQUEST_SECURITY_IDENTITY_AND_SPECIFICATIONS"},{"1", "REQUEST_SECURITY_IDENTITY_FOR_THE_SPECIFICATIONS_PROVIDED"},{"2", "REQUEST_LIST_SECURITY_TYPES"},{"3", "REQUEST_LIST_SECURITIES"}]}
, "321" => #{"Name"=>"SecurityRequestType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "REQUEST_SECURITY_IDENTITY_AND_SPECIFICATIONS"},{"1", "REQUEST_SECURITY_IDENTITY_FOR_THE_SPECIFICATIONS_PROVIDED"},{"2", "REQUEST_LIST_SECURITY_TYPES"},{"3", "REQUEST_LIST_SECURITIES"}], "TagNum" => "321"}


,
"SecurityResponseID" => #{"TagNum" => "322" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "322" => #{"Name"=>"SecurityResponseID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "322"}


,
"SecurityResponseType" => #{"TagNum" => "323" ,"Type" => "INT" ,"ValidValues" =>[{"5", "REJECT_SECURITY_PROPOSAL"},{"1", "ACCEPT_SECURITY_PROPOSAL_AS_IS"},{"6", "CAN_NOT_MATCH_SELECTION_CRITERIA"},{"2", "ACCEPT_SECURITY_PROPOSAL_WITH_REVISIONS_AS_INDICATED_IN_THE_MESSAGE"},{"4", "LIST_OF_SECURITIES_RETURNED_PER_REQUEST"},{"3", "LIST_OF_SECURITY_TYPES_RETURNED_PER_REQUEST"}]}
, "323" => #{"Name"=>"SecurityResponseType" ,"Type"=>"INT" ,"ValidValues"=>[{"5", "REJECT_SECURITY_PROPOSAL"},{"1", "ACCEPT_SECURITY_PROPOSAL_AS_IS"},{"6", "CAN_NOT_MATCH_SELECTION_CRITERIA"},{"2", "ACCEPT_SECURITY_PROPOSAL_WITH_REVISIONS_AS_INDICATED_IN_THE_MESSAGE"},{"4", "LIST_OF_SECURITIES_RETURNED_PER_REQUEST"},{"3", "LIST_OF_SECURITY_TYPES_RETURNED_PER_REQUEST"}], "TagNum" => "323"}


,
"SecurityStatusReqID" => #{"TagNum" => "324" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "324" => #{"Name"=>"SecurityStatusReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "324"}


,
"UnsolicitedIndicator" => #{"TagNum" => "325" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "325" => #{"Name"=>"UnsolicitedIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "325"}


,
"SecurityTradingStatus" => #{"TagNum" => "326" ,"Type" => "INT" ,"ValidValues" =>[{"20", "UNKNOWN_OR_INVALID"},{"13", "NO_MARKET_ON_CLOSE_IMBALANCE"},{"14", "ITS_PRE_OPENING"},{"15", "NEW_PRICE_INDICATION"},{"16", "TRADE_DISSEMINATION_TIME"},{"17", "READY_TO_TRADE"},{"19", "NOT_TRADED_ON_THIS_MARKET"},{"22", "OPENING_ROTATION"},{"21", "PRE_OPEN"},{"12", "NO_MARKET_IMBALANCE"},{"18", "NOT_AVAILABLE_FOR_TRADING"},{"10", "MARKET_ON_CLOSE_IMBALANCE_SELL"},{"9", "MARKET_ON_CLOSE_IMBALANCE_BUY"},{"8", "MARKET_IMBALANCE_SELL"},{"7", "MARKET_IMBALANCE_BUY"},{"6", "TRADING_RANGE_INDICATION"},{"5", "PRICE_INDICATION"},{"4", "NO_OPEN_NO_RESUME"},{"3", "RESUME"},{"1", "OPENING_DELAY"},{"2", "TRADING_HALT"},{"23", "FAST_MARKET"}]}
, "326" => #{"Name"=>"SecurityTradingStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"20", "UNKNOWN_OR_INVALID"},{"13", "NO_MARKET_ON_CLOSE_IMBALANCE"},{"14", "ITS_PRE_OPENING"},{"15", "NEW_PRICE_INDICATION"},{"16", "TRADE_DISSEMINATION_TIME"},{"17", "READY_TO_TRADE"},{"19", "NOT_TRADED_ON_THIS_MARKET"},{"22", "OPENING_ROTATION"},{"21", "PRE_OPEN"},{"12", "NO_MARKET_IMBALANCE"},{"18", "NOT_AVAILABLE_FOR_TRADING"},{"10", "MARKET_ON_CLOSE_IMBALANCE_SELL"},{"9", "MARKET_ON_CLOSE_IMBALANCE_BUY"},{"8", "MARKET_IMBALANCE_SELL"},{"7", "MARKET_IMBALANCE_BUY"},{"6", "TRADING_RANGE_INDICATION"},{"5", "PRICE_INDICATION"},{"4", "NO_OPEN_NO_RESUME"},{"3", "RESUME"},{"1", "OPENING_DELAY"},{"2", "TRADING_HALT"},{"23", "FAST_MARKET"}], "TagNum" => "326"}


,
"HaltReasonChar" => #{"TagNum" => "327" ,"Type" => "CHAR" ,"ValidValues" =>[{"X", "EQUIPMENT_CHANGEOVER"},{"M", "ADDITIONAL_INFORMATION"},{"E", "ORDER_INFLUX"},{"P", "NEWS_PENDING"},{"I", "ORDER_IMBALANCE"},{"D", "NEWS_DISSEMINATION"}]}
, "327" => #{"Name"=>"HaltReasonChar" ,"Type"=>"CHAR" ,"ValidValues"=>[{"X", "EQUIPMENT_CHANGEOVER"},{"M", "ADDITIONAL_INFORMATION"},{"E", "ORDER_INFLUX"},{"P", "NEWS_PENDING"},{"I", "ORDER_IMBALANCE"},{"D", "NEWS_DISSEMINATION"}], "TagNum" => "327"}


,
"InViewOfCommon" => #{"TagNum" => "328" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "328" => #{"Name"=>"InViewOfCommon" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "328"}


,
"DueToRelated" => #{"TagNum" => "329" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "329" => #{"Name"=>"DueToRelated" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "329"}


,
"BuyVolume" => #{"TagNum" => "330" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "330" => #{"Name"=>"BuyVolume" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "330"}


,
"SellVolume" => #{"TagNum" => "331" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "331" => #{"Name"=>"SellVolume" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "331"}


,
"HighPx" => #{"TagNum" => "332" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "332" => #{"Name"=>"HighPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "332"}


,
"LowPx" => #{"TagNum" => "333" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "333" => #{"Name"=>"LowPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "333"}


,
"Adjustment" => #{"TagNum" => "334" ,"Type" => "INT" ,"ValidValues" =>[{"1", "CANCEL"},{"2", "ERROR"},{"3", "CORRECTION"}]}
, "334" => #{"Name"=>"Adjustment" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "CANCEL"},{"2", "ERROR"},{"3", "CORRECTION"}], "TagNum" => "334"}


,
"TradSesReqID" => #{"TagNum" => "335" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "335" => #{"Name"=>"TradSesReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "335"}


,
"TradingSessionID" => #{"TagNum" => "336" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "336" => #{"Name"=>"TradingSessionID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "336"}


,
"ContraTrader" => #{"TagNum" => "337" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "337" => #{"Name"=>"ContraTrader" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "337"}


,
"TradSesMethod" => #{"TagNum" => "338" ,"Type" => "INT" ,"ValidValues" =>[{"3", "TWO_PARTY"},{"1", "ELECTRONIC"},{"2", "OPEN_OUTCRY"}]}
, "338" => #{"Name"=>"TradSesMethod" ,"Type"=>"INT" ,"ValidValues"=>[{"3", "TWO_PARTY"},{"1", "ELECTRONIC"},{"2", "OPEN_OUTCRY"}], "TagNum" => "338"}


,
"TradSesMode" => #{"TagNum" => "339" ,"Type" => "INT" ,"ValidValues" =>[{"3", "PRODUCTION"},{"1", "TESTING"},{"2", "SIMULATED"}]}
, "339" => #{"Name"=>"TradSesMode" ,"Type"=>"INT" ,"ValidValues"=>[{"3", "PRODUCTION"},{"1", "TESTING"},{"2", "SIMULATED"}], "TagNum" => "339"}


,
"TradSesStatus" => #{"TagNum" => "340" ,"Type" => "INT" ,"ValidValues" =>[{"5", "PRE_CLOSE"},{"6", "REQUEST_REJECTED"},{"4", "PRE_OPEN"},{"3", "CLOSED"},{"2", "OPEN"},{"1", "HALTED"},{"0", "UNKNOWN"}]}
, "340" => #{"Name"=>"TradSesStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"5", "PRE_CLOSE"},{"6", "REQUEST_REJECTED"},{"4", "PRE_OPEN"},{"3", "CLOSED"},{"2", "OPEN"},{"1", "HALTED"},{"0", "UNKNOWN"}], "TagNum" => "340"}


,
"TradSesStartTime" => #{"TagNum" => "341" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "341" => #{"Name"=>"TradSesStartTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "341"}


,
"TradSesOpenTime" => #{"TagNum" => "342" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "342" => #{"Name"=>"TradSesOpenTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "342"}


,
"TradSesPreCloseTime" => #{"TagNum" => "343" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "343" => #{"Name"=>"TradSesPreCloseTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "343"}


,
"TradSesCloseTime" => #{"TagNum" => "344" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "344" => #{"Name"=>"TradSesCloseTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "344"}


,
"TradSesEndTime" => #{"TagNum" => "345" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "345" => #{"Name"=>"TradSesEndTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "345"}


,
"NumberOfOrders" => #{"TagNum" => "346" ,"Type" => "INT" ,"ValidValues" =>[]}
, "346" => #{"Name"=>"NumberOfOrders" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "346"}


,
"MessageEncoding" => #{"TagNum" => "347" ,"Type" => "STRING" ,"ValidValues" =>[{"UTF-8", "UTF_8"},{"ISO-2022-JP", "ISO_2022_JP"},{"EUC-JP", "EUC_JP"},{"SHIFT_JIS", "SHIFT_JIS"}]}
, "347" => #{"Name"=>"MessageEncoding" ,"Type"=>"STRING" ,"ValidValues"=>[{"UTF-8", "UTF_8"},{"ISO-2022-JP", "ISO_2022_JP"},{"EUC-JP", "EUC_JP"},{"SHIFT_JIS", "SHIFT_JIS"}], "TagNum" => "347"}


,
"EncodedIssuerLen" => #{"TagNum" => "348" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "348" => #{"Name"=>"EncodedIssuerLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "348"}


,
"EncodedIssuer" => #{"TagNum" => "349" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "349" => #{"Name"=>"EncodedIssuer" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "349"}


,
"EncodedSecurityDescLen" => #{"TagNum" => "350" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "350" => #{"Name"=>"EncodedSecurityDescLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "350"}


,
"EncodedSecurityDesc" => #{"TagNum" => "351" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "351" => #{"Name"=>"EncodedSecurityDesc" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "351"}


,
"EncodedListExecInstLen" => #{"TagNum" => "352" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "352" => #{"Name"=>"EncodedListExecInstLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "352"}


,
"EncodedListExecInst" => #{"TagNum" => "353" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "353" => #{"Name"=>"EncodedListExecInst" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "353"}


,
"EncodedTextLen" => #{"TagNum" => "354" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "354" => #{"Name"=>"EncodedTextLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "354"}


,
"EncodedText" => #{"TagNum" => "355" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "355" => #{"Name"=>"EncodedText" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "355"}


,
"EncodedSubjectLen" => #{"TagNum" => "356" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "356" => #{"Name"=>"EncodedSubjectLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "356"}


,
"EncodedSubject" => #{"TagNum" => "357" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "357" => #{"Name"=>"EncodedSubject" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "357"}


,
"EncodedHeadlineLen" => #{"TagNum" => "358" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "358" => #{"Name"=>"EncodedHeadlineLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "358"}


,
"EncodedHeadline" => #{"TagNum" => "359" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "359" => #{"Name"=>"EncodedHeadline" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "359"}


,
"EncodedAllocTextLen" => #{"TagNum" => "360" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "360" => #{"Name"=>"EncodedAllocTextLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "360"}


,
"EncodedAllocText" => #{"TagNum" => "361" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "361" => #{"Name"=>"EncodedAllocText" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "361"}


,
"EncodedUnderlyingIssuerLen" => #{"TagNum" => "362" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "362" => #{"Name"=>"EncodedUnderlyingIssuerLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "362"}


,
"EncodedUnderlyingIssuer" => #{"TagNum" => "363" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "363" => #{"Name"=>"EncodedUnderlyingIssuer" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "363"}


,
"EncodedUnderlyingSecurityDescLen" => #{"TagNum" => "364" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "364" => #{"Name"=>"EncodedUnderlyingSecurityDescLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "364"}


,
"EncodedUnderlyingSecurityDesc" => #{"TagNum" => "365" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "365" => #{"Name"=>"EncodedUnderlyingSecurityDesc" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "365"}


,
"AllocPrice" => #{"TagNum" => "366" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "366" => #{"Name"=>"AllocPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "366"}


,
"QuoteSetValidUntilTime" => #{"TagNum" => "367" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "367" => #{"Name"=>"QuoteSetValidUntilTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "367"}


,
"QuoteEntryRejectReason" => #{"TagNum" => "368" ,"Type" => "INT" ,"ValidValues" =>[]}
, "368" => #{"Name"=>"QuoteEntryRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "368"}


,
"LastMsgSeqNumProcessed" => #{"TagNum" => "369" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "369" => #{"Name"=>"LastMsgSeqNumProcessed" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "369"}


,
"OnBehalfOfSendingTime" => #{"TagNum" => "370" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "370" => #{"Name"=>"OnBehalfOfSendingTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "370"}


,
"RefTagID" => #{"TagNum" => "371" ,"Type" => "INT" ,"ValidValues" =>[]}
, "371" => #{"Name"=>"RefTagID" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "371"}


,
"RefMsgType" => #{"TagNum" => "372" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "372" => #{"Name"=>"RefMsgType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "372"}


,
"SessionRejectReason" => #{"TagNum" => "373" ,"Type" => "INT" ,"ValidValues" =>[{"12", "XML_VALIDATION_ERROR"},{"17", "NON_DATA_VALUE_INCLUDES_FIELD_DELIMITER"},{"16", "INCORRECT_NUMINGROUP_COUNT_FOR_REPEATING_GROUP"},{"15", "REPEATING_GROUP_FIELDS_OUT_OF_ORDER"},{"14", "TAG_SPECIFIED_OUT_OF_REQUIRED_ORDER"},{"11", "INVALID_MSGTYPE"},{"0", "INVALID_TAG_NUMBER"},{"9", "COMPID_PROBLEM"},{"8", "SIGNATURE_PROBLEM"},{"7", "DECRYPTION_PROBLEM"},{"6", "INCORRECT_DATA_FORMAT_FOR_VALUE"},{"5", "VALUE_IS_INCORRECT"},{"4", "TAG_SPECIFIED_WITHOUT_A_VALUE"},{"3", "UNDEFINED_TAG"},{"10", "SENDINGTIME_ACCURACY_PROBLEM"},{"13", "TAG_APPEARS_MORE_THAN_ONCE"},{"2", "TAG_NOT_DEFINED_FOR_THIS_MESSAGE_TYPE"},{"1", "REQUIRED_TAG_MISSING"}]}
, "373" => #{"Name"=>"SessionRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"12", "XML_VALIDATION_ERROR"},{"17", "NON_DATA_VALUE_INCLUDES_FIELD_DELIMITER"},{"16", "INCORRECT_NUMINGROUP_COUNT_FOR_REPEATING_GROUP"},{"15", "REPEATING_GROUP_FIELDS_OUT_OF_ORDER"},{"14", "TAG_SPECIFIED_OUT_OF_REQUIRED_ORDER"},{"11", "INVALID_MSGTYPE"},{"0", "INVALID_TAG_NUMBER"},{"9", "COMPID_PROBLEM"},{"8", "SIGNATURE_PROBLEM"},{"7", "DECRYPTION_PROBLEM"},{"6", "INCORRECT_DATA_FORMAT_FOR_VALUE"},{"5", "VALUE_IS_INCORRECT"},{"4", "TAG_SPECIFIED_WITHOUT_A_VALUE"},{"3", "UNDEFINED_TAG"},{"10", "SENDINGTIME_ACCURACY_PROBLEM"},{"13", "TAG_APPEARS_MORE_THAN_ONCE"},{"2", "TAG_NOT_DEFINED_FOR_THIS_MESSAGE_TYPE"},{"1", "REQUIRED_TAG_MISSING"}], "TagNum" => "373"}


,
"BidRequestTransType" => #{"TagNum" => "374" ,"Type" => "CHAR" ,"ValidValues" =>[{"N", "NEW"},{"C", "CANCEL"}]}
, "374" => #{"Name"=>"BidRequestTransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"N", "NEW"},{"C", "CANCEL"}], "TagNum" => "374"}


,
"ContraBroker" => #{"TagNum" => "375" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "375" => #{"Name"=>"ContraBroker" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "375"}


,
"ComplianceID" => #{"TagNum" => "376" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "376" => #{"Name"=>"ComplianceID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "376"}


,
"SolicitedFlag" => #{"TagNum" => "377" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "377" => #{"Name"=>"SolicitedFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "377"}


,
"ExecRestatementReason" => #{"TagNum" => "378" ,"Type" => "INT" ,"ValidValues" =>[{"7", "CANCEL_ON_SYSTEM_FAILURE"},{"0", "GT_CORPORATE_ACTION"},{"8", "MARKET"},{"6", "CANCEL_ON_TRADING_HALT"},{"5", "PARTIAL_DECLINE_OF_ORDERQTY"},{"4", "BROKER_OPTION"},{"3", "REPRICING_OF_ORDER"},{"1", "GT_RENEWAL"},{"2", "VERBAL_CHANGE"}]}
, "378" => #{"Name"=>"ExecRestatementReason" ,"Type"=>"INT" ,"ValidValues"=>[{"7", "CANCEL_ON_SYSTEM_FAILURE"},{"0", "GT_CORPORATE_ACTION"},{"8", "MARKET"},{"6", "CANCEL_ON_TRADING_HALT"},{"5", "PARTIAL_DECLINE_OF_ORDERQTY"},{"4", "BROKER_OPTION"},{"3", "REPRICING_OF_ORDER"},{"1", "GT_RENEWAL"},{"2", "VERBAL_CHANGE"}], "TagNum" => "378"}


,
"BusinessRejectRefID" => #{"TagNum" => "379" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "379" => #{"Name"=>"BusinessRejectRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "379"}


,
"BusinessRejectReason" => #{"TagNum" => "380" ,"Type" => "INT" ,"ValidValues" =>[{"3", "UNSUPPORTED_MESSAGE_TYPE"},{"7", "DELIVERTO_FIRM_NOT_AVAILABLE_AT_THIS_TIME"},{"4", "APPLICATION_NOT_AVAILABLE"},{"6", "NOT_AUTHORIZED"},{"0", "OTHER"},{"5", "CONDITIONALLY_REQUIRED_FIELD_MISSING"},{"1", "UNKOWN_ID"},{"2", "UNKNOWN_SECURITY"}]}
, "380" => #{"Name"=>"BusinessRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"3", "UNSUPPORTED_MESSAGE_TYPE"},{"7", "DELIVERTO_FIRM_NOT_AVAILABLE_AT_THIS_TIME"},{"4", "APPLICATION_NOT_AVAILABLE"},{"6", "NOT_AUTHORIZED"},{"0", "OTHER"},{"5", "CONDITIONALLY_REQUIRED_FIELD_MISSING"},{"1", "UNKOWN_ID"},{"2", "UNKNOWN_SECURITY"}], "TagNum" => "380"}


,
"GrossTradeAmt" => #{"TagNum" => "381" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "381" => #{"Name"=>"GrossTradeAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "381"}


,
"NoContraBrokers" => #{"TagNum" => "382" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "382" => #{"Name"=>"NoContraBrokers" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "382"}


,
"MaxMessageSize" => #{"TagNum" => "383" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "383" => #{"Name"=>"MaxMessageSize" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "383"}


,
"NoMsgTypes" => #{"TagNum" => "384" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "384" => #{"Name"=>"NoMsgTypes" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "384"}


,
"MsgDirection" => #{"TagNum" => "385" ,"Type" => "CHAR" ,"ValidValues" =>[{"S", "SEND"},{"R", "RECEIVE"}]}
, "385" => #{"Name"=>"MsgDirection" ,"Type"=>"CHAR" ,"ValidValues"=>[{"S", "SEND"},{"R", "RECEIVE"}], "TagNum" => "385"}


,
"NoTradingSessions" => #{"TagNum" => "386" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "386" => #{"Name"=>"NoTradingSessions" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "386"}


,
"TotalVolumeTraded" => #{"TagNum" => "387" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "387" => #{"Name"=>"TotalVolumeTraded" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "387"}


,
"DiscretionInst" => #{"TagNum" => "388" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "RELATED_TO_DISPLAYED_PRICE"},{"1", "RELATED_TO_MARKET_PRICE"},{"2", "RELATED_TO_PRIMARY_PRICE"},{"3", "RELATED_TO_LOCAL_PRIMARY_PRICE"},{"4", "RELATED_TO_MIDPOINT_PRICE"},{"5", "RELATED_TO_LAST_TRADE_PRICE"}]}
, "388" => #{"Name"=>"DiscretionInst" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "RELATED_TO_DISPLAYED_PRICE"},{"1", "RELATED_TO_MARKET_PRICE"},{"2", "RELATED_TO_PRIMARY_PRICE"},{"3", "RELATED_TO_LOCAL_PRIMARY_PRICE"},{"4", "RELATED_TO_MIDPOINT_PRICE"},{"5", "RELATED_TO_LAST_TRADE_PRICE"}], "TagNum" => "388"}


,
"DiscretionOffset" => #{"TagNum" => "389" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "389" => #{"Name"=>"DiscretionOffset" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "389"}


,
"BidID" => #{"TagNum" => "390" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "390" => #{"Name"=>"BidID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "390"}


,
"ClientBidID" => #{"TagNum" => "391" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "391" => #{"Name"=>"ClientBidID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "391"}


,
"ListName" => #{"TagNum" => "392" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "392" => #{"Name"=>"ListName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "392"}


,
"TotalNumSecurities" => #{"TagNum" => "393" ,"Type" => "INT" ,"ValidValues" =>[]}
, "393" => #{"Name"=>"TotalNumSecurities" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "393"}


,
"BidType" => #{"TagNum" => "394" ,"Type" => "INT" ,"ValidValues" =>[{"1", "NON_DISCLOSED_STYLE"},{"2", "DISCLOSED_STYLE"},{"3", "NO_BIDDING_PROCESS"}]}
, "394" => #{"Name"=>"BidType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "NON_DISCLOSED_STYLE"},{"2", "DISCLOSED_STYLE"},{"3", "NO_BIDDING_PROCESS"}], "TagNum" => "394"}


,
"NumTickets" => #{"TagNum" => "395" ,"Type" => "INT" ,"ValidValues" =>[]}
, "395" => #{"Name"=>"NumTickets" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "395"}


,
"SideValue1" => #{"TagNum" => "396" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "396" => #{"Name"=>"SideValue1" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "396"}


,
"SideValue2" => #{"TagNum" => "397" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "397" => #{"Name"=>"SideValue2" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "397"}


,
"NoBidDescriptors" => #{"TagNum" => "398" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "398" => #{"Name"=>"NoBidDescriptors" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "398"}


,
"BidDescriptorType" => #{"TagNum" => "399" ,"Type" => "INT" ,"ValidValues" =>[{"3", "INDEX"},{"2", "COUNTRY"},{"1", "SECTOR"}]}
, "399" => #{"Name"=>"BidDescriptorType" ,"Type"=>"INT" ,"ValidValues"=>[{"3", "INDEX"},{"2", "COUNTRY"},{"1", "SECTOR"}], "TagNum" => "399"}


,
"BidDescriptor" => #{"TagNum" => "400" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "400" => #{"Name"=>"BidDescriptor" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "400"}


,
"SideValueInd" => #{"TagNum" => "401" ,"Type" => "INT" ,"ValidValues" =>[{"1", "SIDEVALUE1"},{"2", "SIDEVALUE_2"}]}
, "401" => #{"Name"=>"SideValueInd" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "SIDEVALUE1"},{"2", "SIDEVALUE_2"}], "TagNum" => "401"}


,
"LiquidityPctLow" => #{"TagNum" => "402" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "402" => #{"Name"=>"LiquidityPctLow" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "402"}


,
"LiquidityPctHigh" => #{"TagNum" => "403" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "403" => #{"Name"=>"LiquidityPctHigh" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "403"}


,
"LiquidityValue" => #{"TagNum" => "404" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "404" => #{"Name"=>"LiquidityValue" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "404"}


,
"EFPTrackingError" => #{"TagNum" => "405" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "405" => #{"Name"=>"EFPTrackingError" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "405"}


,
"FairValue" => #{"TagNum" => "406" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "406" => #{"Name"=>"FairValue" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "406"}


,
"OutsideIndexPct" => #{"TagNum" => "407" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "407" => #{"Name"=>"OutsideIndexPct" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "407"}


,
"ValueOfFutures" => #{"TagNum" => "408" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "408" => #{"Name"=>"ValueOfFutures" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "408"}


,
"LiquidityIndType" => #{"TagNum" => "409" ,"Type" => "INT" ,"ValidValues" =>[{"3", "NORMAL_MARKET_SIZE"},{"4", "OTHER"},{"2", "20_DAY_MOVING_AVERAGE"},{"1", "5DAY_MOVING_AVERAGE"}]}
, "409" => #{"Name"=>"LiquidityIndType" ,"Type"=>"INT" ,"ValidValues"=>[{"3", "NORMAL_MARKET_SIZE"},{"4", "OTHER"},{"2", "20_DAY_MOVING_AVERAGE"},{"1", "5DAY_MOVING_AVERAGE"}], "TagNum" => "409"}


,
"WtAverageLiquidity" => #{"TagNum" => "410" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "410" => #{"Name"=>"WtAverageLiquidity" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "410"}


,
"ExchangeForPhysical" => #{"TagNum" => "411" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "411" => #{"Name"=>"ExchangeForPhysical" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "411"}


,
"OutMainCntryUIndex" => #{"TagNum" => "412" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "412" => #{"Name"=>"OutMainCntryUIndex" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "412"}


,
"CrossPercent" => #{"TagNum" => "413" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "413" => #{"Name"=>"CrossPercent" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "413"}


,
"ProgRptReqs" => #{"TagNum" => "414" ,"Type" => "INT" ,"ValidValues" =>[{"3", "REAL_TIME_EXECUTION_REPORTS"},{"2", "SELLSIDE_PERIODICALLY_SENDS_STATUS_USING_LISTSTATUS_PERIOD_OPTIONALLY_SPECIFIED_IN_PROGRESSPERIOD"},{"1", "BUYSIDE_EXPLICITLY_REQUESTS_STATUS_USING_STATUSREQUEST"}]}
, "414" => #{"Name"=>"ProgRptReqs" ,"Type"=>"INT" ,"ValidValues"=>[{"3", "REAL_TIME_EXECUTION_REPORTS"},{"2", "SELLSIDE_PERIODICALLY_SENDS_STATUS_USING_LISTSTATUS_PERIOD_OPTIONALLY_SPECIFIED_IN_PROGRESSPERIOD"},{"1", "BUYSIDE_EXPLICITLY_REQUESTS_STATUS_USING_STATUSREQUEST"}], "TagNum" => "414"}


,
"ProgPeriodInterval" => #{"TagNum" => "415" ,"Type" => "INT" ,"ValidValues" =>[]}
, "415" => #{"Name"=>"ProgPeriodInterval" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "415"}


,
"IncTaxInd" => #{"TagNum" => "416" ,"Type" => "INT" ,"ValidValues" =>[{"2", "GROSS"},{"1", "NET"}]}
, "416" => #{"Name"=>"IncTaxInd" ,"Type"=>"INT" ,"ValidValues"=>[{"2", "GROSS"},{"1", "NET"}], "TagNum" => "416"}


,
"NumBidders" => #{"TagNum" => "417" ,"Type" => "INT" ,"ValidValues" =>[]}
, "417" => #{"Name"=>"NumBidders" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "417"}


,
"TradeType" => #{"TagNum" => "418" ,"Type" => "CHAR" ,"ValidValues" =>[{"G", "VWAP_GUARANTEE"},{"A", "AGENCY"},{"J", "GUARANTEED_CLOSE"},{"R", "RISK_TRADE"}]}
, "418" => #{"Name"=>"TradeType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"G", "VWAP_GUARANTEE"},{"A", "AGENCY"},{"J", "GUARANTEED_CLOSE"},{"R", "RISK_TRADE"}], "TagNum" => "418"}


,
"BasisPxType" => #{"TagNum" => "419" ,"Type" => "CHAR" ,"ValidValues" =>[{"8", "VWAP_THROUGH_AN_AFTERNOON_SESSION"},{"D", "OPEN"},{"Z", "OTHERS"},{"C", "STRIKE"},{"B", "VWAP_THROUGH_AN_AFTERNOON_SESSION_EXCEPT_YORI"},{"9", "VWAP_THROUGH_A_DAY_EXCEPT_YORI"},{"7", "VWAP_THROUGH_A_MORNING_SESSION"},{"6", "VWAP_THROUGH_A_DAY"},{"5", "SQ"},{"4", "CURRENT_PRICE"},{"3", "CLOSING_PRICE"},{"2", "CLOSING_PRICE_AT_MORNING_SESSION"},{"A", "VWAP_THROUGH_A_MORNING_SESSION_EXCEPT_YORI"}]}
, "419" => #{"Name"=>"BasisPxType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"8", "VWAP_THROUGH_AN_AFTERNOON_SESSION"},{"D", "OPEN"},{"Z", "OTHERS"},{"C", "STRIKE"},{"B", "VWAP_THROUGH_AN_AFTERNOON_SESSION_EXCEPT_YORI"},{"9", "VWAP_THROUGH_A_DAY_EXCEPT_YORI"},{"7", "VWAP_THROUGH_A_MORNING_SESSION"},{"6", "VWAP_THROUGH_A_DAY"},{"5", "SQ"},{"4", "CURRENT_PRICE"},{"3", "CLOSING_PRICE"},{"2", "CLOSING_PRICE_AT_MORNING_SESSION"},{"A", "VWAP_THROUGH_A_MORNING_SESSION_EXCEPT_YORI"}], "TagNum" => "419"}


,
"NoBidComponents" => #{"TagNum" => "420" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "420" => #{"Name"=>"NoBidComponents" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "420"}


,
"Country" => #{"TagNum" => "421" ,"Type" => "COUNTRY" ,"ValidValues" =>[]}
, "421" => #{"Name"=>"Country" ,"Type"=>"COUNTRY" ,"ValidValues"=>[], "TagNum" => "421"}


,
"TotNoStrikes" => #{"TagNum" => "422" ,"Type" => "INT" ,"ValidValues" =>[]}
, "422" => #{"Name"=>"TotNoStrikes" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "422"}


,
"PriceType" => #{"TagNum" => "423" ,"Type" => "INT" ,"ValidValues" =>[{"3", "FIXED_AMOUNT"},{"1", "PERCENTAGE"},{"4", "DISCOUNT"},{"6", "BASIS_POINTS_RELATIVE_TO_BENCHMARK"},{"7", "TED_PRICE"},{"8", "TED_YIELD"},{"5", "PREMIUM"},{"2", "PER_SHARE"}]}
, "423" => #{"Name"=>"PriceType" ,"Type"=>"INT" ,"ValidValues"=>[{"3", "FIXED_AMOUNT"},{"1", "PERCENTAGE"},{"4", "DISCOUNT"},{"6", "BASIS_POINTS_RELATIVE_TO_BENCHMARK"},{"7", "TED_PRICE"},{"8", "TED_YIELD"},{"5", "PREMIUM"},{"2", "PER_SHARE"}], "TagNum" => "423"}


,
"DayOrderQty" => #{"TagNum" => "424" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "424" => #{"Name"=>"DayOrderQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "424"}


,
"DayCumQty" => #{"TagNum" => "425" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "425" => #{"Name"=>"DayCumQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "425"}


,
"DayAvgPx" => #{"TagNum" => "426" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "426" => #{"Name"=>"DayAvgPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "426"}


,
"GTBookingInst" => #{"TagNum" => "427" ,"Type" => "INT" ,"ValidValues" =>[{"0", "BOOK_OUT_ALL_TRADES_ON_DAY_OF_EXECUTION"},{"2", "ACCUMULATE_UNTIL_VERBALLY_NOTIFIED_OTHERWISE"},{"1", "ACCUMULATE_EXECUTIONS_UNTIL_ORDER_IS_FILLED_OR_EXPIRES"}]}
, "427" => #{"Name"=>"GTBookingInst" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "BOOK_OUT_ALL_TRADES_ON_DAY_OF_EXECUTION"},{"2", "ACCUMULATE_UNTIL_VERBALLY_NOTIFIED_OTHERWISE"},{"1", "ACCUMULATE_EXECUTIONS_UNTIL_ORDER_IS_FILLED_OR_EXPIRES"}], "TagNum" => "427"}


,
"NoStrikes" => #{"TagNum" => "428" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "428" => #{"Name"=>"NoStrikes" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "428"}


,
"ListStatusType" => #{"TagNum" => "429" ,"Type" => "INT" ,"ValidValues" =>[{"6", "ALERT"},{"4", "EXECSTARTED"},{"3", "TIMED"},{"2", "RESPONSE"},{"1", "ACK"},{"5", "ALLDONE"}]}
, "429" => #{"Name"=>"ListStatusType" ,"Type"=>"INT" ,"ValidValues"=>[{"6", "ALERT"},{"4", "EXECSTARTED"},{"3", "TIMED"},{"2", "RESPONSE"},{"1", "ACK"},{"5", "ALLDONE"}], "TagNum" => "429"}


,
"NetGrossInd" => #{"TagNum" => "430" ,"Type" => "INT" ,"ValidValues" =>[{"1", "NET"},{"2", "GROSS"}]}
, "430" => #{"Name"=>"NetGrossInd" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "NET"},{"2", "GROSS"}], "TagNum" => "430"}


,
"ListOrderStatus" => #{"TagNum" => "431" ,"Type" => "INT" ,"ValidValues" =>[{"4", "CANCELING"},{"3", "EXECUTING"},{"7", "REJECT"},{"6", "ALL_DONE"},{"5", "ALERT"},{"2", "RECEIVEDFOREXECUTION"},{"1", "INBIDDINGPROCESS"}]}
, "431" => #{"Name"=>"ListOrderStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"4", "CANCELING"},{"3", "EXECUTING"},{"7", "REJECT"},{"6", "ALL_DONE"},{"5", "ALERT"},{"2", "RECEIVEDFOREXECUTION"},{"1", "INBIDDINGPROCESS"}], "TagNum" => "431"}


,
"ExpireDate" => #{"TagNum" => "432" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "432" => #{"Name"=>"ExpireDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "432"}


,
"ListExecInstType" => #{"TagNum" => "433" ,"Type" => "CHAR" ,"ValidValues" =>[{"5", "EXCHANGE_SWITCH_CIV_ORDER_BUY_DRIVEN_CASH_WITHDRAW"},{"4", "EXCHANGE_SWITCH_CIV_ORDER_BUY_DRIVEN_CASH_TOP_UP"},{"2", "WAIT_FOR_EXECUTE_INSTRUCTION"},{"1", "IMMEDIATE"},{"3", "EXCHANGE_SWITCH_CIV_ORDER_SELL_DRIVEN"}]}
, "433" => #{"Name"=>"ListExecInstType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"5", "EXCHANGE_SWITCH_CIV_ORDER_BUY_DRIVEN_CASH_WITHDRAW"},{"4", "EXCHANGE_SWITCH_CIV_ORDER_BUY_DRIVEN_CASH_TOP_UP"},{"2", "WAIT_FOR_EXECUTE_INSTRUCTION"},{"1", "IMMEDIATE"},{"3", "EXCHANGE_SWITCH_CIV_ORDER_SELL_DRIVEN"}], "TagNum" => "433"}


,
"CxlRejResponseTo" => #{"TagNum" => "434" ,"Type" => "CHAR" ,"ValidValues" =>[{"2", "ORDER_CANCEL_REPLACE_REQUEST"},{"1", "ORDER_CANCEL_REQUEST"}]}
, "434" => #{"Name"=>"CxlRejResponseTo" ,"Type"=>"CHAR" ,"ValidValues"=>[{"2", "ORDER_CANCEL_REPLACE_REQUEST"},{"1", "ORDER_CANCEL_REQUEST"}], "TagNum" => "434"}


,
"UnderlyingCouponRate" => #{"TagNum" => "435" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "435" => #{"Name"=>"UnderlyingCouponRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "435"}


,
"UnderlyingContractMultiplier" => #{"TagNum" => "436" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "436" => #{"Name"=>"UnderlyingContractMultiplier" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "436"}


,
"ContraTradeQty" => #{"TagNum" => "437" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "437" => #{"Name"=>"ContraTradeQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "437"}


,
"ContraTradeTime" => #{"TagNum" => "438" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "438" => #{"Name"=>"ContraTradeTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "438"}


,
"LiquidityNumSecurities" => #{"TagNum" => "441" ,"Type" => "INT" ,"ValidValues" =>[]}
, "441" => #{"Name"=>"LiquidityNumSecurities" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "441"}


,
"MultiLegReportingType" => #{"TagNum" => "442" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "SINGLE_SECURITY"},{"2", "INDIVIDUAL_LEG_OF_A_MULTI_LEG_SECURITY"},{"3", "MULTI_LEG_SECURITY"}]}
, "442" => #{"Name"=>"MultiLegReportingType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "SINGLE_SECURITY"},{"2", "INDIVIDUAL_LEG_OF_A_MULTI_LEG_SECURITY"},{"3", "MULTI_LEG_SECURITY"}], "TagNum" => "442"}


,
"StrikeTime" => #{"TagNum" => "443" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "443" => #{"Name"=>"StrikeTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "443"}


,
"ListStatusText" => #{"TagNum" => "444" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "444" => #{"Name"=>"ListStatusText" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "444"}


,
"EncodedListStatusTextLen" => #{"TagNum" => "445" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "445" => #{"Name"=>"EncodedListStatusTextLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "445"}


,
"EncodedListStatusText" => #{"TagNum" => "446" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "446" => #{"Name"=>"EncodedListStatusText" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "446"}


,
"PartyIDSource" => #{"TagNum" => "447" ,"Type" => "CHAR" ,"ValidValues" =>[{"5", "CHINESE_B_SHARE"},{"8", "US_EMPLOYER_IDENTIFICATION_NUMBER"},{"A", "AUSTRALIAN_TAX_FILE_NUMBER"},{"9", "AUSTRALIAN_BUSINESS_NUMBER"},{"E", "ISO_COUNTRY_CODE"},{"B", "BIC"},{"7", "US_SOCIAL_SECURITY_NUMBER"},{"D", "PROPRIETARY_CUSTOM_CODE"},{"F", "SETTLEMENT_ENTITY_LOCATION"},{"1", "KOREAN_INVESTOR_ID"},{"2", "TAIWANESE_QUALIFIED_FOREIGN_INVESTOR_ID_QFII"},{"3", "TAIWANESE_TRADING_ACCOUNT"},{"4", "MALAYSIAN_CENTRAL_DEPOSITORY"},{"6", "UK_NATIONAL_INSURANCE_OR_PENSION_NUMBER"},{"C", "GENERALLY_ACCEPTED_MARKET_PARTICIPANT_IDENTIFIER"}]}
, "447" => #{"Name"=>"PartyIDSource" ,"Type"=>"CHAR" ,"ValidValues"=>[{"5", "CHINESE_B_SHARE"},{"8", "US_EMPLOYER_IDENTIFICATION_NUMBER"},{"A", "AUSTRALIAN_TAX_FILE_NUMBER"},{"9", "AUSTRALIAN_BUSINESS_NUMBER"},{"E", "ISO_COUNTRY_CODE"},{"B", "BIC"},{"7", "US_SOCIAL_SECURITY_NUMBER"},{"D", "PROPRIETARY_CUSTOM_CODE"},{"F", "SETTLEMENT_ENTITY_LOCATION"},{"1", "KOREAN_INVESTOR_ID"},{"2", "TAIWANESE_QUALIFIED_FOREIGN_INVESTOR_ID_QFII"},{"3", "TAIWANESE_TRADING_ACCOUNT"},{"4", "MALAYSIAN_CENTRAL_DEPOSITORY"},{"6", "UK_NATIONAL_INSURANCE_OR_PENSION_NUMBER"},{"C", "GENERALLY_ACCEPTED_MARKET_PARTICIPANT_IDENTIFIER"}], "TagNum" => "447"}


,
"PartyID" => #{"TagNum" => "448" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "448" => #{"Name"=>"PartyID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "448"}


,
"TotalVolumeTradedDate" => #{"TagNum" => "449" ,"Type" => "UTCDATE" ,"ValidValues" =>[]}
, "449" => #{"Name"=>"TotalVolumeTradedDate" ,"Type"=>"UTCDATE" ,"ValidValues"=>[], "TagNum" => "449"}


,
"TotalVolumeTradedTime" => #{"TagNum" => "450" ,"Type" => "UTCTIMEONLY" ,"ValidValues" =>[]}
, "450" => #{"Name"=>"TotalVolumeTradedTime" ,"Type"=>"UTCTIMEONLY" ,"ValidValues"=>[], "TagNum" => "450"}


,
"NetChgPrevDay" => #{"TagNum" => "451" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "451" => #{"Name"=>"NetChgPrevDay" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "451"}


,
"PartyRole" => #{"TagNum" => "452" ,"Type" => "INT" ,"ValidValues" =>[{"15", "CORRESPONDANT_CLEARING_FIRM"},{"3", "CLIENT_ID"},{"20", "UNDERLYING_CONTRA_FIRM"},{"19", "SPONSORING_FIRM"},{"18", "CONTRA_CLEARING_FIRM"},{"17", "CONTRA_FIRM"},{"16", "EXECUTING_SYSTEM"},{"7", "ENTERING_FIRM"},{"1", "EXECUTING_FIRM"},{"2", "BROKER_OF_CREDIT"},{"5", "INVESTOR_ID"},{"6", "INTRODUCING_FIRM"},{"14", "GIVEUP_CLEARING_FIRM"},{"8", "LOCATE_LENDING_FIRM"},{"9", "FUND_MANAGER_CLIENT_ID"},{"10", "SETTLEMENT_LOCATION"},{"11", "ORDER_ORIGINATION_TRADER"},{"12", "EXECUTING_TRADER"},{"13", "ORDER_ORIGINATION_FIRM"},{"4", "CLEARING_FIRM"}]}
, "452" => #{"Name"=>"PartyRole" ,"Type"=>"INT" ,"ValidValues"=>[{"15", "CORRESPONDANT_CLEARING_FIRM"},{"3", "CLIENT_ID"},{"20", "UNDERLYING_CONTRA_FIRM"},{"19", "SPONSORING_FIRM"},{"18", "CONTRA_CLEARING_FIRM"},{"17", "CONTRA_FIRM"},{"16", "EXECUTING_SYSTEM"},{"7", "ENTERING_FIRM"},{"1", "EXECUTING_FIRM"},{"2", "BROKER_OF_CREDIT"},{"5", "INVESTOR_ID"},{"6", "INTRODUCING_FIRM"},{"14", "GIVEUP_CLEARING_FIRM"},{"8", "LOCATE_LENDING_FIRM"},{"9", "FUND_MANAGER_CLIENT_ID"},{"10", "SETTLEMENT_LOCATION"},{"11", "ORDER_ORIGINATION_TRADER"},{"12", "EXECUTING_TRADER"},{"13", "ORDER_ORIGINATION_FIRM"},{"4", "CLEARING_FIRM"}], "TagNum" => "452"}


,
"NoPartyIDs" => #{"TagNum" => "453" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "453" => #{"Name"=>"NoPartyIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "453"}


,
"NoSecurityAltID" => #{"TagNum" => "454" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "454" => #{"Name"=>"NoSecurityAltID" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "454"}


,
"SecurityAltID" => #{"TagNum" => "455" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "455" => #{"Name"=>"SecurityAltID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "455"}


,
"SecurityAltIDSource" => #{"TagNum" => "456" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "456" => #{"Name"=>"SecurityAltIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "456"}


,
"NoUnderlyingSecurityAltID" => #{"TagNum" => "457" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "457" => #{"Name"=>"NoUnderlyingSecurityAltID" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "457"}


,
"UnderlyingSecurityAltID" => #{"TagNum" => "458" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "458" => #{"Name"=>"UnderlyingSecurityAltID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "458"}


,
"UnderlyingSecurityAltIDSource" => #{"TagNum" => "459" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "459" => #{"Name"=>"UnderlyingSecurityAltIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "459"}


,
"Product" => #{"TagNum" => "460" ,"Type" => "INT" ,"ValidValues" =>[{"8", "LOAN"},{"12", "OTHER"},{"11", "MUNICIPAL"},{"1", "AGENCY"},{"3", "CORPORATE"},{"4", "CURRENCY"},{"2", "COMMODITY"},{"6", "GOVERNMENT"},{"10", "MORTGAGE"},{"7", "INDEX"},{"9", "MONEYMARKET"},{"5", "EQUITY"}]}
, "460" => #{"Name"=>"Product" ,"Type"=>"INT" ,"ValidValues"=>[{"8", "LOAN"},{"12", "OTHER"},{"11", "MUNICIPAL"},{"1", "AGENCY"},{"3", "CORPORATE"},{"4", "CURRENCY"},{"2", "COMMODITY"},{"6", "GOVERNMENT"},{"10", "MORTGAGE"},{"7", "INDEX"},{"9", "MONEYMARKET"},{"5", "EQUITY"}], "TagNum" => "460"}


,
"CFICode" => #{"TagNum" => "461" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "461" => #{"Name"=>"CFICode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "461"}


,
"UnderlyingProduct" => #{"TagNum" => "462" ,"Type" => "INT" ,"ValidValues" =>[]}
, "462" => #{"Name"=>"UnderlyingProduct" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "462"}


,
"UnderlyingCFICode" => #{"TagNum" => "463" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "463" => #{"Name"=>"UnderlyingCFICode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "463"}


,
"TestMessageIndicator" => #{"TagNum" => "464" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "464" => #{"Name"=>"TestMessageIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "464"}


,
"QuantityType" => #{"TagNum" => "465" ,"Type" => "INT" ,"ValidValues" =>[{"6", "CONTRACTS"},{"7", "OTHER"},{"5", "CURRENCY"},{"4", "ORIGINALFACE"},{"3", "CURRENTFACE"},{"2", "BONDS"},{"1", "SHARES"},{"8", "PAR"}]}
, "465" => #{"Name"=>"QuantityType" ,"Type"=>"INT" ,"ValidValues"=>[{"6", "CONTRACTS"},{"7", "OTHER"},{"5", "CURRENCY"},{"4", "ORIGINALFACE"},{"3", "CURRENTFACE"},{"2", "BONDS"},{"1", "SHARES"},{"8", "PAR"}], "TagNum" => "465"}


,
"BookingRefID" => #{"TagNum" => "466" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "466" => #{"Name"=>"BookingRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "466"}


,
"IndividualAllocID" => #{"TagNum" => "467" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "467" => #{"Name"=>"IndividualAllocID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "467"}


,
"RoundingDirection" => #{"TagNum" => "468" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "ROUND_TO_NEAREST"},{"1", "ROUND_DOWN"},{"2", "ROUND_UP"}]}
, "468" => #{"Name"=>"RoundingDirection" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "ROUND_TO_NEAREST"},{"1", "ROUND_DOWN"},{"2", "ROUND_UP"}], "TagNum" => "468"}


,
"RoundingModulus" => #{"TagNum" => "469" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "469" => #{"Name"=>"RoundingModulus" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "469"}


,
"CountryOfIssue" => #{"TagNum" => "470" ,"Type" => "COUNTRY" ,"ValidValues" =>[]}
, "470" => #{"Name"=>"CountryOfIssue" ,"Type"=>"COUNTRY" ,"ValidValues"=>[], "TagNum" => "470"}


,
"StateOrProvinceOfIssue" => #{"TagNum" => "471" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "471" => #{"Name"=>"StateOrProvinceOfIssue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "471"}


,
"LocaleOfIssue" => #{"TagNum" => "472" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "472" => #{"Name"=>"LocaleOfIssue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "472"}


,
"NoRegistDtls" => #{"TagNum" => "473" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "473" => #{"Name"=>"NoRegistDtls" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "473"}


,
"MailingDtls" => #{"TagNum" => "474" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "474" => #{"Name"=>"MailingDtls" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "474"}


,
"InvestorCountryOfResidence" => #{"TagNum" => "475" ,"Type" => "COUNTRY" ,"ValidValues" =>[]}
, "475" => #{"Name"=>"InvestorCountryOfResidence" ,"Type"=>"COUNTRY" ,"ValidValues"=>[], "TagNum" => "475"}


,
"PaymentRef" => #{"TagNum" => "476" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "476" => #{"Name"=>"PaymentRef" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "476"}


,
"DistribPaymentMethod" => #{"TagNum" => "477" ,"Type" => "INT" ,"ValidValues" =>[]}
, "477" => #{"Name"=>"DistribPaymentMethod" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "477"}


,
"CashDistribCurr" => #{"TagNum" => "478" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "478" => #{"Name"=>"CashDistribCurr" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "478"}


,
"CommCurrency" => #{"TagNum" => "479" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "479" => #{"Name"=>"CommCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "479"}


,
"CancellationRights" => #{"TagNum" => "480" ,"Type" => "CHAR" ,"ValidValues" =>[{"M", "NO_WAIVER_AGREEMENT"},{"N", "NO_EXECUTION_ONLY"},{"Y", "YES"},{"O", "NO_INSTITUTIONAL"}]}
, "480" => #{"Name"=>"CancellationRights" ,"Type"=>"CHAR" ,"ValidValues"=>[{"M", "NO_WAIVER_AGREEMENT"},{"N", "NO_EXECUTION_ONLY"},{"Y", "YES"},{"O", "NO_INSTITUTIONAL"}], "TagNum" => "480"}


,
"MoneyLaunderingStatus" => #{"TagNum" => "481" ,"Type" => "CHAR" ,"ValidValues" =>[{"3", "EXEMPT_AUTHORISED_CREDIT_OR_FINANCIAL_INSTITUTION"},{"2", "EXEMPT_CLIENT_MONEY_TYPE_EXEMPTION"},{"1", "EXEMPT_BELOW_THE_LIMIT"},{"Y", "PASSED"},{"N", "NOT_CHECKED"}]}
, "481" => #{"Name"=>"MoneyLaunderingStatus" ,"Type"=>"CHAR" ,"ValidValues"=>[{"3", "EXEMPT_AUTHORISED_CREDIT_OR_FINANCIAL_INSTITUTION"},{"2", "EXEMPT_CLIENT_MONEY_TYPE_EXEMPTION"},{"1", "EXEMPT_BELOW_THE_LIMIT"},{"Y", "PASSED"},{"N", "NOT_CHECKED"}], "TagNum" => "481"}


,
"MailingInst" => #{"TagNum" => "482" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "482" => #{"Name"=>"MailingInst" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "482"}


,
"TransBkdTime" => #{"TagNum" => "483" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "483" => #{"Name"=>"TransBkdTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "483"}


,
"ExecPriceType" => #{"TagNum" => "484" ,"Type" => "CHAR" ,"ValidValues" =>[{"S", "SINGLE_PRICE"},{"Q", "OFFER_PRICE_MINUS_ADJUSTMENT_AMOUNT"},{"P", "OFFER_PRICE_MINUS_ADJUSTMENT"},{"O", "OFFER_PRICE"},{"E", "CREATION_PRICE_PLUS_ADJUSTMENT_AMOUNT"},{"D", "CREATION_PRICE_PLUS_ADJUSTMENT"},{"C", "CREATION_PRICE"},{"B", "BID_PRICE"}]}
, "484" => #{"Name"=>"ExecPriceType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"S", "SINGLE_PRICE"},{"Q", "OFFER_PRICE_MINUS_ADJUSTMENT_AMOUNT"},{"P", "OFFER_PRICE_MINUS_ADJUSTMENT"},{"O", "OFFER_PRICE"},{"E", "CREATION_PRICE_PLUS_ADJUSTMENT_AMOUNT"},{"D", "CREATION_PRICE_PLUS_ADJUSTMENT"},{"C", "CREATION_PRICE"},{"B", "BID_PRICE"}], "TagNum" => "484"}


,
"ExecPriceAdjustment" => #{"TagNum" => "485" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "485" => #{"Name"=>"ExecPriceAdjustment" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "485"}


,
"DateOfBirth" => #{"TagNum" => "486" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "486" => #{"Name"=>"DateOfBirth" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "486"}


,
"TradeReportTransType" => #{"TagNum" => "487" ,"Type" => "CHAR" ,"ValidValues" =>[{"N", "NEW"},{"R", "REPLACE"},{"C", "CANCEL"}]}
, "487" => #{"Name"=>"TradeReportTransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"N", "NEW"},{"R", "REPLACE"},{"C", "CANCEL"}], "TagNum" => "487"}


,
"CardHolderName" => #{"TagNum" => "488" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "488" => #{"Name"=>"CardHolderName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "488"}


,
"CardNumber" => #{"TagNum" => "489" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "489" => #{"Name"=>"CardNumber" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "489"}


,
"CardExpDate" => #{"TagNum" => "490" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "490" => #{"Name"=>"CardExpDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "490"}


,
"CardIssNo" => #{"TagNum" => "491" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "491" => #{"Name"=>"CardIssNo" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "491"}


,
"PaymentMethod" => #{"TagNum" => "492" ,"Type" => "INT" ,"ValidValues" =>[{"14", "BPAY"},{"13", "ACH_CREDIT"},{"12", "ACH_DEBIT"},{"11", "CREDIT_CARD"},{"10", "DIRECT_CREDIT"},{"9", "DIRECT_DEBIT"},{"8", "DEBIT_CARD"},{"7", "FEDWIRE"},{"15", "HIGH_VALUE_CLEARING_SYSTEM"},{"3", "EUROCLEAR"},{"6", "TELEGRAPHIC_TRANSFER"},{"4", "CLEARSTREAM"},{"1", "CREST"},{"2", "NSCC"},{"5", "CHEQUE"}]}
, "492" => #{"Name"=>"PaymentMethod" ,"Type"=>"INT" ,"ValidValues"=>[{"14", "BPAY"},{"13", "ACH_CREDIT"},{"12", "ACH_DEBIT"},{"11", "CREDIT_CARD"},{"10", "DIRECT_CREDIT"},{"9", "DIRECT_DEBIT"},{"8", "DEBIT_CARD"},{"7", "FEDWIRE"},{"15", "HIGH_VALUE_CLEARING_SYSTEM"},{"3", "EUROCLEAR"},{"6", "TELEGRAPHIC_TRANSFER"},{"4", "CLEARSTREAM"},{"1", "CREST"},{"2", "NSCC"},{"5", "CHEQUE"}], "TagNum" => "492"}


,
"RegistAcctType" => #{"TagNum" => "493" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "493" => #{"Name"=>"RegistAcctType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "493"}


,
"Designation" => #{"TagNum" => "494" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "494" => #{"Name"=>"Designation" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "494"}


,
"TaxAdvantageType" => #{"TagNum" => "495" ,"Type" => "INT" ,"ValidValues" =>[{"19", "PROFIT_SHARING_PLAN"},{"11", "EMPLOYER"},{"12", "EMPLOYER_CURRENT_YEAR"},{"13", "NON_FUND_PROTOTYPE_IRA"},{"14", "NON_FUND_QUALIFIED_PLAN"},{"15", "DEFINED_CONTRIBUTION_PLAN"},{"10", "EMPLOYEE_CURRENT_YEAR"},{"17", "INDIVIDUAL_RETIREMENT_ACCOUNT_ROLLOVER"},{"5", "MINI_INSURANCE_ISA"},{"16", "INDIVIDUAL_RETIREMENT_ACCOUNT"},{"9", "EMPLOYEE"},{"8", "ASSET_TRANSFER"},{"21", "SELF_DIRECTED_IRA"},{"6", "CURRENT_YEAR_PAYMENT"},{"20", "401K"},{"4", "MINI_STOCKS_AND_SHARES_ISA"},{"3", "MINI_CASH_ISA"},{"2", "TESSA"},{"1", "MAXI_ISA"},{"0", "NONE_NOT_APPLICABLE"},{"7", "PRIOR_YEAR_PAYMENT"},{"23", "457"},{"24", "ROTH_IRA_24"},{"25", "ROTH_IRA_25"},{"26", "ROTH_CONVERSION_IRA_26"},{"27", "ROTH_CONVERSION_IRA_27"},{"28", "EDUCATION_IRA_28"},{"29", "EDUCATION_IRA_29"},{"18", "KEOGH"},{"22", "403"}]}
, "495" => #{"Name"=>"TaxAdvantageType" ,"Type"=>"INT" ,"ValidValues"=>[{"19", "PROFIT_SHARING_PLAN"},{"11", "EMPLOYER"},{"12", "EMPLOYER_CURRENT_YEAR"},{"13", "NON_FUND_PROTOTYPE_IRA"},{"14", "NON_FUND_QUALIFIED_PLAN"},{"15", "DEFINED_CONTRIBUTION_PLAN"},{"10", "EMPLOYEE_CURRENT_YEAR"},{"17", "INDIVIDUAL_RETIREMENT_ACCOUNT_ROLLOVER"},{"5", "MINI_INSURANCE_ISA"},{"16", "INDIVIDUAL_RETIREMENT_ACCOUNT"},{"9", "EMPLOYEE"},{"8", "ASSET_TRANSFER"},{"21", "SELF_DIRECTED_IRA"},{"6", "CURRENT_YEAR_PAYMENT"},{"20", "401K"},{"4", "MINI_STOCKS_AND_SHARES_ISA"},{"3", "MINI_CASH_ISA"},{"2", "TESSA"},{"1", "MAXI_ISA"},{"0", "NONE_NOT_APPLICABLE"},{"7", "PRIOR_YEAR_PAYMENT"},{"23", "457"},{"24", "ROTH_IRA_24"},{"25", "ROTH_IRA_25"},{"26", "ROTH_CONVERSION_IRA_26"},{"27", "ROTH_CONVERSION_IRA_27"},{"28", "EDUCATION_IRA_28"},{"29", "EDUCATION_IRA_29"},{"18", "KEOGH"},{"22", "403"}], "TagNum" => "495"}


,
"RegistRejReasonText" => #{"TagNum" => "496" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "496" => #{"Name"=>"RegistRejReasonText" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "496"}


,
"FundRenewWaiv" => #{"TagNum" => "497" ,"Type" => "CHAR" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "497" => #{"Name"=>"FundRenewWaiv" ,"Type"=>"CHAR" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "497"}


,
"CashDistribAgentName" => #{"TagNum" => "498" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "498" => #{"Name"=>"CashDistribAgentName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "498"}


,
"CashDistribAgentCode" => #{"TagNum" => "499" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "499" => #{"Name"=>"CashDistribAgentCode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "499"}


,
"CashDistribAgentAcctNumber" => #{"TagNum" => "500" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "500" => #{"Name"=>"CashDistribAgentAcctNumber" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "500"}


,
"CashDistribPayRef" => #{"TagNum" => "501" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "501" => #{"Name"=>"CashDistribPayRef" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "501"}


,
"CardStartDate" => #{"TagNum" => "503" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "503" => #{"Name"=>"CardStartDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "503"}


,
"PaymentDate" => #{"TagNum" => "504" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "504" => #{"Name"=>"PaymentDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "504"}


,
"PaymentRemitterID" => #{"TagNum" => "505" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "505" => #{"Name"=>"PaymentRemitterID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "505"}


,
"RegistStatus" => #{"TagNum" => "506" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "ACCEPT"},{"N", "REMINDER"},{"R", "REJECT"},{"H", "HELD"}]}
, "506" => #{"Name"=>"RegistStatus" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "ACCEPT"},{"N", "REMINDER"},{"R", "REJECT"},{"H", "HELD"}], "TagNum" => "506"}


,
"RegistRejReasonCode" => #{"TagNum" => "507" ,"Type" => "INT" ,"ValidValues" =>[{"13", "INVALID_UNACCEPTABLE_NODISTRIBINSTNS"},{"17", "INVALID_UNACCEPTABLE_CASH_DISTRIB_AGENT_CODE"},{"16", "INVALID_UNACCEPTABLE_CASH_DISTRIB_AGENT_ACCT_NAME"},{"4", "INVALID_UNACCEPTABLE_NO_REG_DETLS"},{"15", "INVALID_UNACCEPTABLE_DISTRIB_PAYMENT_METHOD"},{"14", "INVALID_UNACCEPTABLE_DISTRIB_PERCENTAGE"},{"3", "INVALID_UNACCEPTABLE_OWNERSHIP_TYPE"},{"2", "INVALID_UNACCEPTABLE_TAX_EXEMPT_TYPE"},{"12", "INVALID_UNACCEPTABLE_INVESTOR_COUNTRY_OF_RESIDENCE"},{"11", "INVALID_UNACCEPTABLE_DATE_OF_BIRTH"},{"10", "INVALID_UNACCEPTABLE_INVESTOR_ID_SOURCE"},{"9", "INVALID_UNACCEPTABLE_INVESTOR_ID"},{"8", "INVALID_UNACCEPTABLE_MAILING_INST"},{"7", "INVALID_UNACCEPTABLE_MAILING_DTLS"},{"5", "INVALID_UNACCEPTABLE_REG_SEQ_NO"},{"1", "INVALID_UNACCEPTABLE_ACCOUNT_TYPE"},{"18", "INVALID_UNACCEPTABLE_CASH_DISTRIB_AGENT_ACCT_NUM"},{"6", "INVALID_UNACCEPTABLE_REG_DTLS"}]}
, "507" => #{"Name"=>"RegistRejReasonCode" ,"Type"=>"INT" ,"ValidValues"=>[{"13", "INVALID_UNACCEPTABLE_NODISTRIBINSTNS"},{"17", "INVALID_UNACCEPTABLE_CASH_DISTRIB_AGENT_CODE"},{"16", "INVALID_UNACCEPTABLE_CASH_DISTRIB_AGENT_ACCT_NAME"},{"4", "INVALID_UNACCEPTABLE_NO_REG_DETLS"},{"15", "INVALID_UNACCEPTABLE_DISTRIB_PAYMENT_METHOD"},{"14", "INVALID_UNACCEPTABLE_DISTRIB_PERCENTAGE"},{"3", "INVALID_UNACCEPTABLE_OWNERSHIP_TYPE"},{"2", "INVALID_UNACCEPTABLE_TAX_EXEMPT_TYPE"},{"12", "INVALID_UNACCEPTABLE_INVESTOR_COUNTRY_OF_RESIDENCE"},{"11", "INVALID_UNACCEPTABLE_DATE_OF_BIRTH"},{"10", "INVALID_UNACCEPTABLE_INVESTOR_ID_SOURCE"},{"9", "INVALID_UNACCEPTABLE_INVESTOR_ID"},{"8", "INVALID_UNACCEPTABLE_MAILING_INST"},{"7", "INVALID_UNACCEPTABLE_MAILING_DTLS"},{"5", "INVALID_UNACCEPTABLE_REG_SEQ_NO"},{"1", "INVALID_UNACCEPTABLE_ACCOUNT_TYPE"},{"18", "INVALID_UNACCEPTABLE_CASH_DISTRIB_AGENT_ACCT_NUM"},{"6", "INVALID_UNACCEPTABLE_REG_DTLS"}], "TagNum" => "507"}


,
"RegistRefID" => #{"TagNum" => "508" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "508" => #{"Name"=>"RegistRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "508"}


,
"RegistDetls" => #{"TagNum" => "509" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "509" => #{"Name"=>"RegistDetls" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "509"}


,
"NoDistribInsts" => #{"TagNum" => "510" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "510" => #{"Name"=>"NoDistribInsts" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "510"}


,
"RegistEmail" => #{"TagNum" => "511" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "511" => #{"Name"=>"RegistEmail" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "511"}


,
"DistribPercentage" => #{"TagNum" => "512" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "512" => #{"Name"=>"DistribPercentage" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "512"}


,
"RegistID" => #{"TagNum" => "513" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "513" => #{"Name"=>"RegistID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "513"}


,
"RegistTransType" => #{"TagNum" => "514" ,"Type" => "CHAR" ,"ValidValues" =>[{"2", "CANCEL"},{"0", "NEW"},{"1", "REPLACE"}]}
, "514" => #{"Name"=>"RegistTransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"2", "CANCEL"},{"0", "NEW"},{"1", "REPLACE"}], "TagNum" => "514"}


,
"ExecValuationPoint" => #{"TagNum" => "515" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "515" => #{"Name"=>"ExecValuationPoint" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "515"}


,
"OrderPercent" => #{"TagNum" => "516" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "516" => #{"Name"=>"OrderPercent" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "516"}


,
"OwnershipType" => #{"TagNum" => "517" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "517" => #{"Name"=>"OwnershipType" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "517"}


,
"NoContAmts" => #{"TagNum" => "518" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "518" => #{"Name"=>"NoContAmts" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "518"}


,
"ContAmtType" => #{"TagNum" => "519" ,"Type" => "INT" ,"ValidValues" =>[{"15", "NET_SETTLEMENT_AMOUNT"},{"1", "COMMISSION_AMOUNT"},{"2", "COMMISSION"},{"3", "INITIAL_CHARGE_AMOUNT"},{"4", "INITIAL_CHARGE"},{"5", "DISCOUNT_AMOUNT"},{"6", "DISCOUNT"},{"7", "DILUTION_LEVY_AMOUNT"},{"8", "DILUTION_LEVY"},{"9", "EXIT_CHARGE_AMOUNT"},{"10", "EXIT_CHARGE"},{"11", "FUND_BASED_RENEWAL_COMMISSION"},{"12", "PROJECTED_FUND_VALUE"},{"14", "FUND_BASED_RENEWAL_COMMISSION_AMOUNT_14"},{"13", "FUND_BASED_RENEWAL_COMMISSION_AMOUNT_13"}]}
, "519" => #{"Name"=>"ContAmtType" ,"Type"=>"INT" ,"ValidValues"=>[{"15", "NET_SETTLEMENT_AMOUNT"},{"1", "COMMISSION_AMOUNT"},{"2", "COMMISSION"},{"3", "INITIAL_CHARGE_AMOUNT"},{"4", "INITIAL_CHARGE"},{"5", "DISCOUNT_AMOUNT"},{"6", "DISCOUNT"},{"7", "DILUTION_LEVY_AMOUNT"},{"8", "DILUTION_LEVY"},{"9", "EXIT_CHARGE_AMOUNT"},{"10", "EXIT_CHARGE"},{"11", "FUND_BASED_RENEWAL_COMMISSION"},{"12", "PROJECTED_FUND_VALUE"},{"14", "FUND_BASED_RENEWAL_COMMISSION_AMOUNT_14"},{"13", "FUND_BASED_RENEWAL_COMMISSION_AMOUNT_13"}], "TagNum" => "519"}


,
"ContAmtValue" => #{"TagNum" => "520" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "520" => #{"Name"=>"ContAmtValue" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "520"}


,
"ContAmtCurr" => #{"TagNum" => "521" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "521" => #{"Name"=>"ContAmtCurr" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "521"}


,
"OwnerType" => #{"TagNum" => "522" ,"Type" => "INT" ,"ValidValues" =>[{"5", "COMPANY_TRUSTEE"},{"13", "NOMINEE"},{"12", "CORPORATE_BODY"},{"11", "NON_PROFIT_ORGANIZATION"},{"10", "NETWORKING_SUB_ACCOUNT"},{"9", "FIDUCIARIES"},{"8", "TRUSTS"},{"6", "PENSION_PLAN"},{"4", "INDIVIDUAL_TRUSTEE"},{"2", "PUBLIC_COMPANY"},{"3", "PRIVATE_COMPANY"},{"1", "INDIVIDUAL_INVESTOR"},{"7", "CUSTODIAN_UNDER_GIFTS_TO_MINORS_ACT"}]}
, "522" => #{"Name"=>"OwnerType" ,"Type"=>"INT" ,"ValidValues"=>[{"5", "COMPANY_TRUSTEE"},{"13", "NOMINEE"},{"12", "CORPORATE_BODY"},{"11", "NON_PROFIT_ORGANIZATION"},{"10", "NETWORKING_SUB_ACCOUNT"},{"9", "FIDUCIARIES"},{"8", "TRUSTS"},{"6", "PENSION_PLAN"},{"4", "INDIVIDUAL_TRUSTEE"},{"2", "PUBLIC_COMPANY"},{"3", "PRIVATE_COMPANY"},{"1", "INDIVIDUAL_INVESTOR"},{"7", "CUSTODIAN_UNDER_GIFTS_TO_MINORS_ACT"}], "TagNum" => "522"}


,
"PartySubID" => #{"TagNum" => "523" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "523" => #{"Name"=>"PartySubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "523"}


,
"NestedPartyID" => #{"TagNum" => "524" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "524" => #{"Name"=>"NestedPartyID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "524"}


,
"NestedPartyIDSource" => #{"TagNum" => "525" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "525" => #{"Name"=>"NestedPartyIDSource" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "525"}


,
"SecondaryClOrdID" => #{"TagNum" => "526" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "526" => #{"Name"=>"SecondaryClOrdID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "526"}


,
"SecondaryExecID" => #{"TagNum" => "527" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "527" => #{"Name"=>"SecondaryExecID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "527"}


,
"OrderCapacity" => #{"TagNum" => "528" ,"Type" => "CHAR" ,"ValidValues" =>[{"R", "RISKLESS_PRINCIPAL"},{"I", "INDIVIDUAL"},{"P", "PRINCIPAL"},{"W", "AGENT_FOR_OTHER_MEMBER"},{"A", "AGENCY"},{"G", "PROPRIETARY"}]}
, "528" => #{"Name"=>"OrderCapacity" ,"Type"=>"CHAR" ,"ValidValues"=>[{"R", "RISKLESS_PRINCIPAL"},{"I", "INDIVIDUAL"},{"P", "PRINCIPAL"},{"W", "AGENT_FOR_OTHER_MEMBER"},{"A", "AGENCY"},{"G", "PROPRIETARY"}], "TagNum" => "528"}


,
"OrderRestrictions" => #{"TagNum" => "529" ,"Type" => "MULTIPLEVALUESTRING" ,"ValidValues" =>[{"7", "FOREIGN_ENTITY"},{"A", "RISKLESS_ARBITRAGE"},{"1", "PROGRAM_TRADE"},{"8", "EXTERNAL_MARKET_PARTICIPANT"},{"6", "ACTING_AS_MARKET_MAKER_OR_SPECIALIST_IN_THE_UNDERLYING_SECURITY_OF_A_DERIVATIVE_SECURITY"},{"5", "ACTING_AS_MARKET_MAKER_OR_SPECIALIST_IN_THE_SECURITY"},{"3", "NON_INDEX_ARBITRAGE"},{"2", "INDEX_ARBITRAGE"},{"4", "COMPETING_MARKET_MAKER"},{"9", "EXTERNAL_INTER_CONNECTED_MARKET_LINKAGE"}]}
, "529" => #{"Name"=>"OrderRestrictions" ,"Type"=>"MULTIPLEVALUESTRING" ,"ValidValues"=>[{"7", "FOREIGN_ENTITY"},{"A", "RISKLESS_ARBITRAGE"},{"1", "PROGRAM_TRADE"},{"8", "EXTERNAL_MARKET_PARTICIPANT"},{"6", "ACTING_AS_MARKET_MAKER_OR_SPECIALIST_IN_THE_UNDERLYING_SECURITY_OF_A_DERIVATIVE_SECURITY"},{"5", "ACTING_AS_MARKET_MAKER_OR_SPECIALIST_IN_THE_SECURITY"},{"3", "NON_INDEX_ARBITRAGE"},{"2", "INDEX_ARBITRAGE"},{"4", "COMPETING_MARKET_MAKER"},{"9", "EXTERNAL_INTER_CONNECTED_MARKET_LINKAGE"}], "TagNum" => "529"}


,
"MassCancelRequestType" => #{"TagNum" => "530" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "CANCEL_ORDERS_FOR_A_SECURITY"},{"7", "CANCEL_ALL_ORDERS"},{"6", "CANCEL_ORDERS_FOR_A_TRADING_SESSION"},{"5", "CANCEL_ORDERS_FOR_A_SECURITYTYPE"},{"4", "CANCEL_ORDERS_FOR_A_CFICODE"},{"2", "CANCEL_ORDERS_FOR_AN_UNDERLYING_SECURITY"},{"3", "CANCEL_ORDERS_FOR_A_PRODUCT"}]}
, "530" => #{"Name"=>"MassCancelRequestType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "CANCEL_ORDERS_FOR_A_SECURITY"},{"7", "CANCEL_ALL_ORDERS"},{"6", "CANCEL_ORDERS_FOR_A_TRADING_SESSION"},{"5", "CANCEL_ORDERS_FOR_A_SECURITYTYPE"},{"4", "CANCEL_ORDERS_FOR_A_CFICODE"},{"2", "CANCEL_ORDERS_FOR_AN_UNDERLYING_SECURITY"},{"3", "CANCEL_ORDERS_FOR_A_PRODUCT"}], "TagNum" => "530"}


,
"MassCancelResponse" => #{"TagNum" => "531" ,"Type" => "CHAR" ,"ValidValues" =>[{"6", "CANCEL_ORDERS_FOR_A_TRADING_SESSION"},{"0", "CANCEL_REQUEST_REJECTED"},{"7", "CANCEL_ALL_ORDERS"},{"3", "CANCEL_ORDERS_FOR_A_PRODUCT"},{"5", "CANCEL_ORDERS_FOR_A_SECURITYTYPE"},{"4", "CANCEL_ORDERS_FOR_A_CFICODE"},{"1", "CANCEL_ORDERS_FOR_A_SECURITY"},{"2", "CANCEL_ORDERS_FOR_AN_UNDERLYING_SECURITY"}]}
, "531" => #{"Name"=>"MassCancelResponse" ,"Type"=>"CHAR" ,"ValidValues"=>[{"6", "CANCEL_ORDERS_FOR_A_TRADING_SESSION"},{"0", "CANCEL_REQUEST_REJECTED"},{"7", "CANCEL_ALL_ORDERS"},{"3", "CANCEL_ORDERS_FOR_A_PRODUCT"},{"5", "CANCEL_ORDERS_FOR_A_SECURITYTYPE"},{"4", "CANCEL_ORDERS_FOR_A_CFICODE"},{"1", "CANCEL_ORDERS_FOR_A_SECURITY"},{"2", "CANCEL_ORDERS_FOR_AN_UNDERLYING_SECURITY"}], "TagNum" => "531"}


,
"MassCancelRejectReason" => #{"TagNum" => "532" ,"Type" => "CHAR" ,"ValidValues" =>[{"2", "INVALID_OR_UNKNOWN_UNDERLYING"},{"6", "INVALID_OR_UNKNOWN_TRADING_SESSION"},{"5", "INVALID_OR_UNKNOWN_SECURITY_TYPE"},{"3", "INVALID_OR_UNKNOWN_PRODUCT"},{"1", "INVALID_OR_UNKNOWN_SECURITY"},{"0", "MASS_CANCEL_NOT_SUPPORTED"},{"4", "INVALID_OR_UNKNOWN_CFICODE"}]}
, "532" => #{"Name"=>"MassCancelRejectReason" ,"Type"=>"CHAR" ,"ValidValues"=>[{"2", "INVALID_OR_UNKNOWN_UNDERLYING"},{"6", "INVALID_OR_UNKNOWN_TRADING_SESSION"},{"5", "INVALID_OR_UNKNOWN_SECURITY_TYPE"},{"3", "INVALID_OR_UNKNOWN_PRODUCT"},{"1", "INVALID_OR_UNKNOWN_SECURITY"},{"0", "MASS_CANCEL_NOT_SUPPORTED"},{"4", "INVALID_OR_UNKNOWN_CFICODE"}], "TagNum" => "532"}


,
"TotalAffectedOrders" => #{"TagNum" => "533" ,"Type" => "INT" ,"ValidValues" =>[]}
, "533" => #{"Name"=>"TotalAffectedOrders" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "533"}


,
"NoAffectedOrders" => #{"TagNum" => "534" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "534" => #{"Name"=>"NoAffectedOrders" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "534"}


,
"AffectedOrderID" => #{"TagNum" => "535" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "535" => #{"Name"=>"AffectedOrderID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "535"}


,
"AffectedSecondaryOrderID" => #{"TagNum" => "536" ,"Type" => "STIRNG" ,"ValidValues" =>[]}
, "536" => #{"Name"=>"AffectedSecondaryOrderID" ,"Type"=>"STIRNG" ,"ValidValues"=>[], "TagNum" => "536"}


,
"QuoteType" => #{"TagNum" => "537" ,"Type" => "INT" ,"ValidValues" =>[{"0", "INDICATIVE"},{"1", "TRADEABLE"},{"2", "RESTRICTED_TRADEABLE"}]}
, "537" => #{"Name"=>"QuoteType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "INDICATIVE"},{"1", "TRADEABLE"},{"2", "RESTRICTED_TRADEABLE"}], "TagNum" => "537"}


,
"NestedPartyRole" => #{"TagNum" => "538" ,"Type" => "INT" ,"ValidValues" =>[]}
, "538" => #{"Name"=>"NestedPartyRole" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "538"}


,
"NoNestedPartyIDs" => #{"TagNum" => "539" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "539" => #{"Name"=>"NoNestedPartyIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "539"}


,
"TotalAccruedInterestAmt" => #{"TagNum" => "540" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "540" => #{"Name"=>"TotalAccruedInterestAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "540"}


,
"MaturityDate" => #{"TagNum" => "541" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "541" => #{"Name"=>"MaturityDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "541"}


,
"UnderlyingMaturityDate" => #{"TagNum" => "542" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "542" => #{"Name"=>"UnderlyingMaturityDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "542"}


,
"InstrRegistry" => #{"TagNum" => "543" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "543" => #{"Name"=>"InstrRegistry" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "543"}


,
"CashMargin" => #{"TagNum" => "544" ,"Type" => "CHAR" ,"ValidValues" =>[{"2", "MARGIN_OPEN"},{"3", "MARGIN_CLOSE"},{"1", "CASH"}]}
, "544" => #{"Name"=>"CashMargin" ,"Type"=>"CHAR" ,"ValidValues"=>[{"2", "MARGIN_OPEN"},{"3", "MARGIN_CLOSE"},{"1", "CASH"}], "TagNum" => "544"}


,
"NestedPartySubID" => #{"TagNum" => "545" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "545" => #{"Name"=>"NestedPartySubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "545"}


,
"Scope" => #{"TagNum" => "546" ,"Type" => "MULTIPLEVALUESTRING" ,"ValidValues" =>[{"1", "LOCAL"},{"2", "NATIONAL"},{"3", "GLOBAL"}]}
, "546" => #{"Name"=>"Scope" ,"Type"=>"MULTIPLEVALUESTRING" ,"ValidValues"=>[{"1", "LOCAL"},{"2", "NATIONAL"},{"3", "GLOBAL"}], "TagNum" => "546"}


,
"MDImplicitDelete" => #{"TagNum" => "547" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "547" => #{"Name"=>"MDImplicitDelete" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "547"}


,
"CrossID" => #{"TagNum" => "548" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "548" => #{"Name"=>"CrossID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "548"}


,
"CrossType" => #{"TagNum" => "549" ,"Type" => "INT" ,"ValidValues" =>[{"1", "CROSS_TRADE_WHICH_IS_EXECUTED_COMPLETELY_OR_NOT_BOTH_SIDES_ARE_TREATED_IN_THE_SAME_MANNER_THIS_IS_EQUIVALENT_TO_AN_ALL_OR_NONE"},{"2", "CROSS_TRADE_WHICH_IS_EXECUTED_PARTIALLY_AND_THE_REST_IS_CANCELLED_ONE_SIDE_IS_FULLY_EXECUTED_THE_OTHER_SIDE_IS_PARTIALLY_EXECUTED_WITH_THE_REMAINDER_BEING_CANCELLED_THIS_IS_EQUIVALENT_TO_AN_IMMEDIATE_OR_CANCEL_ON_THE_OTHER_SIDE"},{"3", "CROSS_TRADE_WHICH_IS_PARTIALLY_EXECUTED_WITH_THE_UNFILLED_PORTIONS_REMAINING_ACTIVE_ONE_SIDE_OF_THE_CROSS_IS_FULLY_EXECUTED"},{"4", "CROSS_TRADE_IS_EXECUTED_WITH_EXISTING_ORDERS_WITH_THE_SAME_PRICE"}]}
, "549" => #{"Name"=>"CrossType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "CROSS_TRADE_WHICH_IS_EXECUTED_COMPLETELY_OR_NOT_BOTH_SIDES_ARE_TREATED_IN_THE_SAME_MANNER_THIS_IS_EQUIVALENT_TO_AN_ALL_OR_NONE"},{"2", "CROSS_TRADE_WHICH_IS_EXECUTED_PARTIALLY_AND_THE_REST_IS_CANCELLED_ONE_SIDE_IS_FULLY_EXECUTED_THE_OTHER_SIDE_IS_PARTIALLY_EXECUTED_WITH_THE_REMAINDER_BEING_CANCELLED_THIS_IS_EQUIVALENT_TO_AN_IMMEDIATE_OR_CANCEL_ON_THE_OTHER_SIDE"},{"3", "CROSS_TRADE_WHICH_IS_PARTIALLY_EXECUTED_WITH_THE_UNFILLED_PORTIONS_REMAINING_ACTIVE_ONE_SIDE_OF_THE_CROSS_IS_FULLY_EXECUTED"},{"4", "CROSS_TRADE_IS_EXECUTED_WITH_EXISTING_ORDERS_WITH_THE_SAME_PRICE"}], "TagNum" => "549"}


,
"CrossPrioritization" => #{"TagNum" => "550" ,"Type" => "INT" ,"ValidValues" =>[{"2", "SELLSIDE_PRIORITIZED"},{"0", "NONE"},{"1", "BUYSIDE_PRIORITIZED"}]}
, "550" => #{"Name"=>"CrossPrioritization" ,"Type"=>"INT" ,"ValidValues"=>[{"2", "SELLSIDE_PRIORITIZED"},{"0", "NONE"},{"1", "BUYSIDE_PRIORITIZED"}], "TagNum" => "550"}


,
"OrigCrossID" => #{"TagNum" => "551" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "551" => #{"Name"=>"OrigCrossID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "551"}


,
"NoSides" => #{"TagNum" => "552" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[{"1", "ONE_SIDE"},{"2", "BOTH_SIDES"}]}
, "552" => #{"Name"=>"NoSides" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[{"1", "ONE_SIDE"},{"2", "BOTH_SIDES"}], "TagNum" => "552"}


,
"Username" => #{"TagNum" => "553" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "553" => #{"Name"=>"Username" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "553"}


,
"Password" => #{"TagNum" => "554" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "554" => #{"Name"=>"Password" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "554"}


,
"NoLegs" => #{"TagNum" => "555" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "555" => #{"Name"=>"NoLegs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "555"}


,
"LegCurrency" => #{"TagNum" => "556" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "556" => #{"Name"=>"LegCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "556"}


,
"TotalNumSecurityTypes" => #{"TagNum" => "557" ,"Type" => "INT" ,"ValidValues" =>[]}
, "557" => #{"Name"=>"TotalNumSecurityTypes" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "557"}


,
"NoSecurityTypes" => #{"TagNum" => "558" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "558" => #{"Name"=>"NoSecurityTypes" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "558"}


,
"SecurityListRequestType" => #{"TagNum" => "559" ,"Type" => "INT" ,"ValidValues" =>[{"1", "SECURITYTYPE_AND_OR_CFICODE"},{"2", "PRODUCT"},{"3", "TRADINGSESSIONID"},{"4", "ALL_SECURITIES"},{"0", "SYMBOL"}]}
, "559" => #{"Name"=>"SecurityListRequestType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "SECURITYTYPE_AND_OR_CFICODE"},{"2", "PRODUCT"},{"3", "TRADINGSESSIONID"},{"4", "ALL_SECURITIES"},{"0", "SYMBOL"}], "TagNum" => "559"}


,
"SecurityRequestResult" => #{"TagNum" => "560" ,"Type" => "INT" ,"ValidValues" =>[{"4", "INSTRUMENT_DATA_TEMPORARILY_UNAVAILABLE"},{"0", "VALID_REQUEST"},{"1", "INVALID_OR_UNSUPPORTED_REQUEST"},{"5", "REQUEST_FOR_INSTRUMENT_DATA_NOT_SUPPORTED"},{"3", "NOT_AUTHORIZED_TO_RETRIEVE_INSTRUMENT_DATA"},{"2", "NO_INSTRUMENTS_FOUND_THAT_MATCH_SELECTION_CRITERIA"}]}
, "560" => #{"Name"=>"SecurityRequestResult" ,"Type"=>"INT" ,"ValidValues"=>[{"4", "INSTRUMENT_DATA_TEMPORARILY_UNAVAILABLE"},{"0", "VALID_REQUEST"},{"1", "INVALID_OR_UNSUPPORTED_REQUEST"},{"5", "REQUEST_FOR_INSTRUMENT_DATA_NOT_SUPPORTED"},{"3", "NOT_AUTHORIZED_TO_RETRIEVE_INSTRUMENT_DATA"},{"2", "NO_INSTRUMENTS_FOUND_THAT_MATCH_SELECTION_CRITERIA"}], "TagNum" => "560"}


,
"RoundLot" => #{"TagNum" => "561" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "561" => #{"Name"=>"RoundLot" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "561"}


,
"MinTradeVol" => #{"TagNum" => "562" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "562" => #{"Name"=>"MinTradeVol" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "562"}


,
"MultiLegRptTypeReq" => #{"TagNum" => "563" ,"Type" => "INT" ,"ValidValues" =>[]}
, "563" => #{"Name"=>"MultiLegRptTypeReq" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "563"}


,
"LegPositionEffect" => #{"TagNum" => "564" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "564" => #{"Name"=>"LegPositionEffect" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "564"}


,
"LegCoveredOrUncovered" => #{"TagNum" => "565" ,"Type" => "INT" ,"ValidValues" =>[]}
, "565" => #{"Name"=>"LegCoveredOrUncovered" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "565"}


,
"LegPrice" => #{"TagNum" => "566" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "566" => #{"Name"=>"LegPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "566"}


,
"TradSesStatusRejReason" => #{"TagNum" => "567" ,"Type" => "INT" ,"ValidValues" =>[{"1", "UNKNOWN_OR_INVALID_TRADINGSESSIONID"}]}
, "567" => #{"Name"=>"TradSesStatusRejReason" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "UNKNOWN_OR_INVALID_TRADINGSESSIONID"}], "TagNum" => "567"}


,
"TradeRequestID" => #{"TagNum" => "568" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "568" => #{"Name"=>"TradeRequestID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "568"}


,
"TradeRequestType" => #{"TagNum" => "569" ,"Type" => "INT" ,"ValidValues" =>[{"4", "ADVISORIES_THAT_MATCH_CRITERIA"},{"3", "UNREPORTED_TRADES_THAT_MATCH_CRITERIA"},{"2", "UNMATCHED_TRADES_THAT_MATCH_CRITERIA"},{"1", "MATCHED_TRADES_MATCHING_CRITERIA_PROVIDED_ON_REQUEST"},{"0", "ALL_TRADES"}]}
, "569" => #{"Name"=>"TradeRequestType" ,"Type"=>"INT" ,"ValidValues"=>[{"4", "ADVISORIES_THAT_MATCH_CRITERIA"},{"3", "UNREPORTED_TRADES_THAT_MATCH_CRITERIA"},{"2", "UNMATCHED_TRADES_THAT_MATCH_CRITERIA"},{"1", "MATCHED_TRADES_MATCHING_CRITERIA_PROVIDED_ON_REQUEST"},{"0", "ALL_TRADES"}], "TagNum" => "569"}


,
"PreviouslyReported" => #{"TagNum" => "570" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "570" => #{"Name"=>"PreviouslyReported" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "570"}


,
"TradeReportID" => #{"TagNum" => "571" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "571" => #{"Name"=>"TradeReportID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "571"}


,
"TradeReportRefID" => #{"TagNum" => "572" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "572" => #{"Name"=>"TradeReportRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "572"}


,
"MatchStatus" => #{"TagNum" => "573" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "COMPARED_MATCHED_OR_AFFIRMED"},{"1", "UNCOMPARED_UNMATCHED_OR_UNAFFIRMED"},{"2", "ADVISORY_OR_ALERT"}]}
, "573" => #{"Name"=>"MatchStatus" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "COMPARED_MATCHED_OR_AFFIRMED"},{"1", "UNCOMPARED_UNMATCHED_OR_UNAFFIRMED"},{"2", "ADVISORY_OR_ALERT"}], "TagNum" => "573"}


,
"MatchType" => #{"TagNum" => "574" ,"Type" => "STRING" ,"ValidValues" =>[{"S5", "SUMMARIZED_MATCH_USING_A1_TO_A5_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED_S5"},{"M1", "ACT_M1_MATCH"},{"M6", "ACT_M6_MATCH"},{"M5", "ACT_DEFAULT_AFTER_M2"},{"M3", "ACT_ACCEPTED_TRADE"},{"S2", "SUMMARIZED_MATCH_USING_A1_TO_A5_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED_S2"},{"S3", "SUMMARIZED_MATCH_USING_A1_TO_A5_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED_S3"},{"S4", "SUMMARIZED_MATCH_USING_A1_TO_A5_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED_S4"},{"M2", "ACT_M2_MATCH"},{"A2", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_FOUR_BADGES"},{"A3", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_TWO_BADGES_AND_EXECUTION_TIME"},{"A4", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND"},{"AQ", "COMPARED_RECORDS_RESULTING_FROM_STAMPED_ADVISORIES_OR_SPECIALIST"},{"MT", "NON_ACT"},{"M4", "ACT_DEFAULT_TRADE"},{"A1", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_FOUR_BADGES_AND_EXECUTION_TIME"},{"S1", "SUMMARIZED_MATCH_USING_A1_TO_A5_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED_S1"},{"A5", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_EXECUTION_TIME"}]}
, "574" => #{"Name"=>"MatchType" ,"Type"=>"STRING" ,"ValidValues"=>[{"S5", "SUMMARIZED_MATCH_USING_A1_TO_A5_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED_S5"},{"M1", "ACT_M1_MATCH"},{"M6", "ACT_M6_MATCH"},{"M5", "ACT_DEFAULT_AFTER_M2"},{"M3", "ACT_ACCEPTED_TRADE"},{"S2", "SUMMARIZED_MATCH_USING_A1_TO_A5_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED_S2"},{"S3", "SUMMARIZED_MATCH_USING_A1_TO_A5_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED_S3"},{"S4", "SUMMARIZED_MATCH_USING_A1_TO_A5_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED_S4"},{"M2", "ACT_M2_MATCH"},{"A2", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_FOUR_BADGES"},{"A3", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_TWO_BADGES_AND_EXECUTION_TIME"},{"A4", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND"},{"AQ", "COMPARED_RECORDS_RESULTING_FROM_STAMPED_ADVISORIES_OR_SPECIALIST"},{"MT", "NON_ACT"},{"M4", "ACT_DEFAULT_TRADE"},{"A1", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_FOUR_BADGES_AND_EXECUTION_TIME"},{"S1", "SUMMARIZED_MATCH_USING_A1_TO_A5_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED_S1"},{"A5", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_EXECUTION_TIME"}], "TagNum" => "574"}


,
"OddLot" => #{"TagNum" => "575" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "575" => #{"Name"=>"OddLot" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "575"}


,
"NoClearingInstructions" => #{"TagNum" => "576" ,"Type" => "INT" ,"ValidValues" =>[]}
, "576" => #{"Name"=>"NoClearingInstructions" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "576"}


,
"ClearingInstruction" => #{"TagNum" => "577" ,"Type" => "INT" ,"ValidValues" =>[{"8", "MANUAL_MODE"},{"5", "MULTILATERAL_NETTING"},{"9", "AUTOMATIC_POSTING_MODE"},{"2", "BILATERAL_NETTING_ONLY"},{"6", "CLEAR_AGAINST_CENTRAL_COUNTERPARTY"},{"10", "AUTOMATIC_GIVE_UP_MODE"},{"4", "SPECIAL_TRADE"},{"3", "EX_CLEARING"},{"0", "PROCESS_NORMALLY"},{"7", "EXCLUDE_FROM_CENTRAL_COUNTERPARTY"},{"1", "EXCLUDE_FROM_ALL_NETTING"}]}
, "577" => #{"Name"=>"ClearingInstruction" ,"Type"=>"INT" ,"ValidValues"=>[{"8", "MANUAL_MODE"},{"5", "MULTILATERAL_NETTING"},{"9", "AUTOMATIC_POSTING_MODE"},{"2", "BILATERAL_NETTING_ONLY"},{"6", "CLEAR_AGAINST_CENTRAL_COUNTERPARTY"},{"10", "AUTOMATIC_GIVE_UP_MODE"},{"4", "SPECIAL_TRADE"},{"3", "EX_CLEARING"},{"0", "PROCESS_NORMALLY"},{"7", "EXCLUDE_FROM_CENTRAL_COUNTERPARTY"},{"1", "EXCLUDE_FROM_ALL_NETTING"}], "TagNum" => "577"}


,
"TradeInputSource" => #{"TagNum" => "578" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "578" => #{"Name"=>"TradeInputSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "578"}


,
"TradeInputDevice" => #{"TagNum" => "579" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "579" => #{"Name"=>"TradeInputDevice" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "579"}


,
"NoDates" => #{"TagNum" => "580" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "580" => #{"Name"=>"NoDates" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "580"}


,
"AccountType" => #{"TagNum" => "581" ,"Type" => "INT" ,"ValidValues" =>[{"3", "HOUSE_TRADER"},{"7", "ACCOUNT_IS_HOUSE_TRADER_AND_IS_CROSS_MARGINED"},{"6", "ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS_AND_IS_CROSS_MARGINED"},{"4", "FLOOR_TRADER"},{"2", "ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS"},{"1", "ACCOUNT_IS_CARRIED_ON_CUSTOMER_SIDE_OF_BOOKS"},{"8", "JOINT_BACKOFFICE_ACCOUNT"}]}
, "581" => #{"Name"=>"AccountType" ,"Type"=>"INT" ,"ValidValues"=>[{"3", "HOUSE_TRADER"},{"7", "ACCOUNT_IS_HOUSE_TRADER_AND_IS_CROSS_MARGINED"},{"6", "ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS_AND_IS_CROSS_MARGINED"},{"4", "FLOOR_TRADER"},{"2", "ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS"},{"1", "ACCOUNT_IS_CARRIED_ON_CUSTOMER_SIDE_OF_BOOKS"},{"8", "JOINT_BACKOFFICE_ACCOUNT"}], "TagNum" => "581"}


,
"CustOrderCapacity" => #{"TagNum" => "582" ,"Type" => "INT" ,"ValidValues" =>[]}
, "582" => #{"Name"=>"CustOrderCapacity" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "582"}


,
"ClOrdLinkID" => #{"TagNum" => "583" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "583" => #{"Name"=>"ClOrdLinkID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "583"}


,
"MassStatusReqID" => #{"TagNum" => "584" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "584" => #{"Name"=>"MassStatusReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "584"}


,
"MassStatusReqType" => #{"TagNum" => "585" ,"Type" => "INT" ,"ValidValues" =>[{"1", "STATUS_FOR_ORDERS_FOR_A_SECURITY"},{"2", "STATUS_FOR_ORDERS_FOR_AN_UNDERLYING_SECURITY"},{"3", "STATUS_FOR_ORDERS_FOR_A_PRODUCT"},{"4", "STATUS_FOR_ORDERS_FOR_A_CFICODE"},{"5", "STATUS_FOR_ORDERS_FOR_A_SECURITYTYPE"},{"6", "STATUS_FOR_ORDERS_FOR_A_TRADING_SESSION"},{"8", "STATUS_FOR_ORDERS_FOR_A_PARTYID"},{"7", "STATUS_FOR_ALL_ORDERS"}]}
, "585" => #{"Name"=>"MassStatusReqType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "STATUS_FOR_ORDERS_FOR_A_SECURITY"},{"2", "STATUS_FOR_ORDERS_FOR_AN_UNDERLYING_SECURITY"},{"3", "STATUS_FOR_ORDERS_FOR_A_PRODUCT"},{"4", "STATUS_FOR_ORDERS_FOR_A_CFICODE"},{"5", "STATUS_FOR_ORDERS_FOR_A_SECURITYTYPE"},{"6", "STATUS_FOR_ORDERS_FOR_A_TRADING_SESSION"},{"8", "STATUS_FOR_ORDERS_FOR_A_PARTYID"},{"7", "STATUS_FOR_ALL_ORDERS"}], "TagNum" => "585"}


,
"OrigOrdModTime" => #{"TagNum" => "586" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "586" => #{"Name"=>"OrigOrdModTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "586"}


,
"LegSettlmntTyp" => #{"TagNum" => "587" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "587" => #{"Name"=>"LegSettlmntTyp" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "587"}


,
"LegFutSettDate" => #{"TagNum" => "588" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "588" => #{"Name"=>"LegFutSettDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "588"}


,
"DayBookingInst" => #{"TagNum" => "589" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "CAN_TRIGGER_BOOKING_WITHOUT_REFERENCE_TO_THE_ORDER_INITIATOR"},{"1", "SPEAK_WITH_ORDER_INITIATOR_BEFORE_BOOKING"}]}
, "589" => #{"Name"=>"DayBookingInst" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "CAN_TRIGGER_BOOKING_WITHOUT_REFERENCE_TO_THE_ORDER_INITIATOR"},{"1", "SPEAK_WITH_ORDER_INITIATOR_BEFORE_BOOKING"}], "TagNum" => "589"}


,
"BookingUnit" => #{"TagNum" => "590" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "AGGREGATE_PARTIAL_EXECUTIONS_ON_THIS_ORDER_AND_BOOK_ONE_TRADE_PER_ORDER"},{"2", "AGGREGATE_EXECUTIONS_FOR_THIS_SYMBOL_SIDE_AND_SETTLEMENT_DATE"},{"0", "EACH_PARTIAL_EXECUTION_IS_A_BOOKABLE_UNIT"}]}
, "590" => #{"Name"=>"BookingUnit" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "AGGREGATE_PARTIAL_EXECUTIONS_ON_THIS_ORDER_AND_BOOK_ONE_TRADE_PER_ORDER"},{"2", "AGGREGATE_EXECUTIONS_FOR_THIS_SYMBOL_SIDE_AND_SETTLEMENT_DATE"},{"0", "EACH_PARTIAL_EXECUTION_IS_A_BOOKABLE_UNIT"}], "TagNum" => "590"}


,
"PreallocMethod" => #{"TagNum" => "591" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "PRO_RATA"},{"1", "DO_NOT_PRO_RATA_DISCUSS_FIRST"}]}
, "591" => #{"Name"=>"PreallocMethod" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "PRO_RATA"},{"1", "DO_NOT_PRO_RATA_DISCUSS_FIRST"}], "TagNum" => "591"}


,
"UnderlyingCountryOfIssue" => #{"TagNum" => "592" ,"Type" => "COUNTRY" ,"ValidValues" =>[]}
, "592" => #{"Name"=>"UnderlyingCountryOfIssue" ,"Type"=>"COUNTRY" ,"ValidValues"=>[], "TagNum" => "592"}


,
"UnderlyingStateOrProvinceOfIssue" => #{"TagNum" => "593" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "593" => #{"Name"=>"UnderlyingStateOrProvinceOfIssue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "593"}


,
"UnderlyingLocaleOfIssue" => #{"TagNum" => "594" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "594" => #{"Name"=>"UnderlyingLocaleOfIssue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "594"}


,
"UnderlyingInstrRegistry" => #{"TagNum" => "595" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "595" => #{"Name"=>"UnderlyingInstrRegistry" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "595"}


,
"LegCountryOfIssue" => #{"TagNum" => "596" ,"Type" => "COUNTRY" ,"ValidValues" =>[]}
, "596" => #{"Name"=>"LegCountryOfIssue" ,"Type"=>"COUNTRY" ,"ValidValues"=>[], "TagNum" => "596"}


,
"LegStateOrProvinceOfIssue" => #{"TagNum" => "597" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "597" => #{"Name"=>"LegStateOrProvinceOfIssue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "597"}


,
"LegLocaleOfIssue" => #{"TagNum" => "598" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "598" => #{"Name"=>"LegLocaleOfIssue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "598"}


,
"LegInstrRegistry" => #{"TagNum" => "599" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "599" => #{"Name"=>"LegInstrRegistry" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "599"}


,
"LegSymbol" => #{"TagNum" => "600" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "600" => #{"Name"=>"LegSymbol" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "600"}


,
"LegSymbolSfx" => #{"TagNum" => "601" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "601" => #{"Name"=>"LegSymbolSfx" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "601"}


,
"LegSecurityID" => #{"TagNum" => "602" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "602" => #{"Name"=>"LegSecurityID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "602"}


,
"LegSecurityIDSource" => #{"TagNum" => "603" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "603" => #{"Name"=>"LegSecurityIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "603"}


,
"NoLegSecurityAltID" => #{"TagNum" => "604" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "604" => #{"Name"=>"NoLegSecurityAltID" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "604"}


,
"LegSecurityAltID" => #{"TagNum" => "605" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "605" => #{"Name"=>"LegSecurityAltID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "605"}


,
"LegSecurityAltIDSource" => #{"TagNum" => "606" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "606" => #{"Name"=>"LegSecurityAltIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "606"}


,
"LegProduct" => #{"TagNum" => "607" ,"Type" => "INT" ,"ValidValues" =>[]}
, "607" => #{"Name"=>"LegProduct" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "607"}


,
"LegCFICode" => #{"TagNum" => "608" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "608" => #{"Name"=>"LegCFICode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "608"}


,
"LegSecurityType" => #{"TagNum" => "609" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "609" => #{"Name"=>"LegSecurityType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "609"}


,
"LegMaturityMonthYear" => #{"TagNum" => "610" ,"Type" => "MONTHYEAR" ,"ValidValues" =>[]}
, "610" => #{"Name"=>"LegMaturityMonthYear" ,"Type"=>"MONTHYEAR" ,"ValidValues"=>[], "TagNum" => "610"}


,
"LegMaturityDate" => #{"TagNum" => "611" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "611" => #{"Name"=>"LegMaturityDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "611"}


,
"LegStrikePrice" => #{"TagNum" => "612" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "612" => #{"Name"=>"LegStrikePrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "612"}


,
"LegOptAttribute" => #{"TagNum" => "613" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "613" => #{"Name"=>"LegOptAttribute" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "613"}


,
"LegContractMultiplier" => #{"TagNum" => "614" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "614" => #{"Name"=>"LegContractMultiplier" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "614"}


,
"LegCouponRate" => #{"TagNum" => "615" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "615" => #{"Name"=>"LegCouponRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "615"}


,
"LegSecurityExchange" => #{"TagNum" => "616" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "616" => #{"Name"=>"LegSecurityExchange" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "616"}


,
"LegIssuer" => #{"TagNum" => "617" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "617" => #{"Name"=>"LegIssuer" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "617"}


,
"EncodedLegIssuerLen" => #{"TagNum" => "618" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "618" => #{"Name"=>"EncodedLegIssuerLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "618"}


,
"EncodedLegIssuer" => #{"TagNum" => "619" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "619" => #{"Name"=>"EncodedLegIssuer" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "619"}


,
"LegSecurityDesc" => #{"TagNum" => "620" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "620" => #{"Name"=>"LegSecurityDesc" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "620"}


,
"EncodedLegSecurityDescLen" => #{"TagNum" => "621" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "621" => #{"Name"=>"EncodedLegSecurityDescLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "621"}


,
"EncodedLegSecurityDesc" => #{"TagNum" => "622" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "622" => #{"Name"=>"EncodedLegSecurityDesc" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "622"}


,
"LegRatioQty" => #{"TagNum" => "623" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "623" => #{"Name"=>"LegRatioQty" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "623"}


,
"LegSide" => #{"TagNum" => "624" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "624" => #{"Name"=>"LegSide" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "624"}


,
"TradingSessionSubID" => #{"TagNum" => "625" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "625" => #{"Name"=>"TradingSessionSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "625"}


,
"AllocType" => #{"TagNum" => "626" ,"Type" => "INT" ,"ValidValues" =>[{"6", "BUYSIDE_READY_TO_BOOK_6"},{"2", "BUYSIDE_PRELIMINARY"},{"3", "SELLSIDE_CALCULATED_USING_PRELIMINARY"},{"5", "BUYSIDE_READY_TO_BOOK_5"},{"1", "BUYSIDE_CALCULATED"},{"4", "SELLSIDE_CALCULATED_WITHOUT_PRELIMINARY"}]}
, "626" => #{"Name"=>"AllocType" ,"Type"=>"INT" ,"ValidValues"=>[{"6", "BUYSIDE_READY_TO_BOOK_6"},{"2", "BUYSIDE_PRELIMINARY"},{"3", "SELLSIDE_CALCULATED_USING_PRELIMINARY"},{"5", "BUYSIDE_READY_TO_BOOK_5"},{"1", "BUYSIDE_CALCULATED"},{"4", "SELLSIDE_CALCULATED_WITHOUT_PRELIMINARY"}], "TagNum" => "626"}


,
"NoHops" => #{"TagNum" => "627" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "627" => #{"Name"=>"NoHops" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "627"}


,
"HopCompID" => #{"TagNum" => "628" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "628" => #{"Name"=>"HopCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "628"}


,
"HopSendingTime" => #{"TagNum" => "629" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "629" => #{"Name"=>"HopSendingTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "629"}


,
"HopRefID" => #{"TagNum" => "630" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "630" => #{"Name"=>"HopRefID" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "630"}


,
"MidPx" => #{"TagNum" => "631" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "631" => #{"Name"=>"MidPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "631"}


,
"BidYield" => #{"TagNum" => "632" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "632" => #{"Name"=>"BidYield" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "632"}


,
"MidYield" => #{"TagNum" => "633" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "633" => #{"Name"=>"MidYield" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "633"}


,
"OfferYield" => #{"TagNum" => "634" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "634" => #{"Name"=>"OfferYield" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "634"}


,
"ClearingFeeIndicator" => #{"TagNum" => "635" ,"Type" => "STRING" ,"ValidValues" =>[{"H", "106H_AND_106J_FIRMS"},{"5", "5TH_YEAR_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"},{"4", "4TH_YEAR_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"},{"3", "3RD_YEAR_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"},{"2", "2ND_YEAR_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"},{"1", "1ST_YEAR_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"},{"M", "ALL_OTHER_OWNERSHIP_TYPES"},{"I", "GIM_IDEM_AND_COM_MEMBERSHIP_INTEREST_HOLDERS"},{"9", "6TH_YEAR_AND_BEYOND_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"},{"F", "FULL_AND_ASSOCIATE_MEMBER_TRADING_FOR_OWN_ACCOUNT_AND_AS_FLOOR"},{"E", "EQUITY_MEMBER_AND_CLEARING_MEMBER"},{"C", "NON_MEMBER_AND_CUSTOMER"},{"B", "CBOE_MEMBER"},{"L", "LESSEE_AND_106F_EMPLOYEES"}]}
, "635" => #{"Name"=>"ClearingFeeIndicator" ,"Type"=>"STRING" ,"ValidValues"=>[{"H", "106H_AND_106J_FIRMS"},{"5", "5TH_YEAR_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"},{"4", "4TH_YEAR_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"},{"3", "3RD_YEAR_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"},{"2", "2ND_YEAR_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"},{"1", "1ST_YEAR_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"},{"M", "ALL_OTHER_OWNERSHIP_TYPES"},{"I", "GIM_IDEM_AND_COM_MEMBERSHIP_INTEREST_HOLDERS"},{"9", "6TH_YEAR_AND_BEYOND_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"},{"F", "FULL_AND_ASSOCIATE_MEMBER_TRADING_FOR_OWN_ACCOUNT_AND_AS_FLOOR"},{"E", "EQUITY_MEMBER_AND_CLEARING_MEMBER"},{"C", "NON_MEMBER_AND_CUSTOMER"},{"B", "CBOE_MEMBER"},{"L", "LESSEE_AND_106F_EMPLOYEES"}], "TagNum" => "635"}


,
"WorkingIndicator" => #{"TagNum" => "636" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "636" => #{"Name"=>"WorkingIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "636"}


,
"LegLastPx" => #{"TagNum" => "637" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "637" => #{"Name"=>"LegLastPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "637"}


,
"PriorityIndicator" => #{"TagNum" => "638" ,"Type" => "INT" ,"ValidValues" =>[{"0", "PRIORITY_UNCHANGED"},{"1", "LOST_PRIORITY_AS_RESULT_OF_ORDER_CHANGE"}]}
, "638" => #{"Name"=>"PriorityIndicator" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "PRIORITY_UNCHANGED"},{"1", "LOST_PRIORITY_AS_RESULT_OF_ORDER_CHANGE"}], "TagNum" => "638"}


,
"PriceImprovement" => #{"TagNum" => "639" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "639" => #{"Name"=>"PriceImprovement" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "639"}


,
"Price2" => #{"TagNum" => "640" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "640" => #{"Name"=>"Price2" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "640"}


,
"LastForwardPoints2" => #{"TagNum" => "641" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "641" => #{"Name"=>"LastForwardPoints2" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "641"}


,
"BidForwardPoints2" => #{"TagNum" => "642" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "642" => #{"Name"=>"BidForwardPoints2" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "642"}


,
"OfferForwardPoints2" => #{"TagNum" => "643" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "643" => #{"Name"=>"OfferForwardPoints2" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "643"}


,
"RFQReqID" => #{"TagNum" => "644" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "644" => #{"Name"=>"RFQReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "644"}


,
"MktBidPx" => #{"TagNum" => "645" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "645" => #{"Name"=>"MktBidPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "645"}


,
"MktOfferPx" => #{"TagNum" => "646" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "646" => #{"Name"=>"MktOfferPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "646"}


,
"MinBidSize" => #{"TagNum" => "647" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "647" => #{"Name"=>"MinBidSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "647"}


,
"MinOfferSize" => #{"TagNum" => "648" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "648" => #{"Name"=>"MinOfferSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "648"}


,
"QuoteStatusReqID" => #{"TagNum" => "649" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "649" => #{"Name"=>"QuoteStatusReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "649"}


,
"LegalConfirm" => #{"TagNum" => "650" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "650" => #{"Name"=>"LegalConfirm" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "650"}


,
"UnderlyingLastPx" => #{"TagNum" => "651" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "651" => #{"Name"=>"UnderlyingLastPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "651"}


,
"UnderlyingLastQty" => #{"TagNum" => "652" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "652" => #{"Name"=>"UnderlyingLastQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "652"}


,
"LegRefID" => #{"TagNum" => "654" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "654" => #{"Name"=>"LegRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "654"}


,
"ContraLegRefID" => #{"TagNum" => "655" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "655" => #{"Name"=>"ContraLegRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "655"}


,
"SettlCurrBidFxRate" => #{"TagNum" => "656" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "656" => #{"Name"=>"SettlCurrBidFxRate" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "656"}


,
"SettlCurrOfferFxRate" => #{"TagNum" => "657" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "657" => #{"Name"=>"SettlCurrOfferFxRate" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "657"}


,
"QuoteRequestRejectReason" => #{"TagNum" => "658" ,"Type" => "INT" ,"ValidValues" =>[{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE"},{"3", "QUOTE_REQUEST_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "INVALID_PRICE"},{"6", "NOT_AUTHORIZED_TO_REQUEST_QUOTE"}]}
, "658" => #{"Name"=>"QuoteRequestRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE"},{"3", "QUOTE_REQUEST_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "INVALID_PRICE"},{"6", "NOT_AUTHORIZED_TO_REQUEST_QUOTE"}], "TagNum" => "658"}


,
"SideComplianceID" => #{"TagNum" => "659" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "659" => #{"Name"=>"SideComplianceID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "659"}


}.



components() ->
 #{"SpreadOrBenchmarkCurveData" => #{
                              "Fields" => #{"Spread" =>#{"Required" => "N", "Sequence" => undefined}
,"BenchmarkCurveCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"BenchmarkCurveName" =>#{"Required" => "N", "Sequence" => undefined}
,"BenchmarkCurvePoint" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"YieldData" => #{
                              "Fields" => #{"YieldType" =>#{"Required" => "N", "Sequence" => undefined}
,"Yield" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"CommissionData" => #{
                              "Fields" => #{"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"CommCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"FundRenewWaiv" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"UnderlyingInstrument" => #{
                              "Fields" => #{"UnderlyingSymbol" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityAltID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityAltIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingProduct" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCFICode" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityDate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingPutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCouponPaymentDate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingIssueDate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingRepoCollateralSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingRepurchaseTerm" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingRepurchaseRate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingFactor" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCreditRating" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingInstrRegistry" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCountryOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStateOrProvinceOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLocaleOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingRedemptionDate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingOptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoUnderlyingSecurityAltID" => #{
                              "Fields" => #{"UnderlyingSecurityAltID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityAltIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"Instrument" => #{
                              "Fields" => #{"Symbol" =>#{"Required" => "N", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityAltID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityAltIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"Product" =>#{"Required" => "N", "Sequence" => undefined}
,"CFICode" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponPaymentDate" =>#{"Required" => "N", "Sequence" => undefined}
,"IssueDate" =>#{"Required" => "N", "Sequence" => undefined}
,"RepoCollateralSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"RepurchaseTerm" =>#{"Required" => "N", "Sequence" => undefined}
,"RepurchaseRate" =>#{"Required" => "N", "Sequence" => undefined}
,"Factor" =>#{"Required" => "N", "Sequence" => undefined}
,"CreditRating" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrRegistry" =>#{"Required" => "N", "Sequence" => undefined}
,"CountryOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"StateOrProvinceOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"LocaleOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"RedemptionDate" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSecurityAltID" => #{
                              "Fields" => #{"SecurityAltID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityAltIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"NestedParties" => #{
                              "Fields" => #{"NestedPartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedPartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedPartyRole" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedPartySubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoNestedPartyIDs" => #{
                              "Fields" => #{"NestedPartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedPartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedPartyRole" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedPartySubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"Stipulations" => #{
                              "Fields" => #{"StipulationType" =>#{"Required" => "N", "Sequence" => undefined}
,"StipulationValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoStipulations" => #{
                              "Fields" => #{"StipulationType" =>#{"Required" => "N", "Sequence" => undefined}
,"StipulationValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"Parties" => #{
                              "Fields" => #{"PartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"PartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"PartyRole" =>#{"Required" => "N", "Sequence" => undefined}
,"PartySubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoPartyIDs" => #{
                              "Fields" => #{"PartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"PartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"PartyRole" =>#{"Required" => "N", "Sequence" => undefined}
,"PartySubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"OrderQtyData" => #{
                              "Fields" => #{"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderPercent" =>#{"Required" => "N", "Sequence" => undefined}
,"RoundingDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"RoundingModulus" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"InstrumentLeg" => #{
                              "Fields" => #{"LegSymbol" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecurityIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecurityAltID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecurityAltIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"LegProduct" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCFICode" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegMaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"LegMaturityDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCouponPaymentDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegIssueDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRepoCollateralSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRepurchaseTerm" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRepurchaseRate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegFactor" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCreditRating" =>#{"Required" => "N", "Sequence" => undefined}
,"LegInstrRegistry" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCountryOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStateOrProvinceOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"LegLocaleOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRedemptionDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"LegContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"LegIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedLegIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedLegIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedLegSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedLegSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRatioQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSide" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegSecurityAltID" => #{
                              "Fields" => #{"LegSecurityAltID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecurityAltIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
}.



groups() ->
 #{}.



header()->
#{
"Fields" => #{"BeginString" =>#{"Required" => "Y", "Sequence" => undefined}
,"BodyLength" =>#{"Required" => "Y", "Sequence" => undefined}
,"MsgType" =>#{"Required" => "Y", "Sequence" => undefined}
,"SenderCompID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TargetCompID" =>#{"Required" => "Y", "Sequence" => undefined}
,"OnBehalfOfCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeliverToCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecureDataLen" =>#{"Required" => "N", "Sequence" => undefined}
,"SecureData" =>#{"Required" => "N", "Sequence" => undefined}
,"MsgSeqNum" =>#{"Required" => "Y", "Sequence" => undefined}
,"SenderSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SenderLocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetLocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"OnBehalfOfSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"OnBehalfOfLocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeliverToSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeliverToLocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"PossDupFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"PossResend" =>#{"Required" => "N", "Sequence" => undefined}
,"SendingTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigSendingTime" =>#{"Required" => "N", "Sequence" => undefined}
,"XmlDataLen" =>#{"Required" => "N", "Sequence" => undefined}
,"XmlData" =>#{"Required" => "N", "Sequence" => undefined}
,"MessageEncoding" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMsgSeqNumProcessed" =>#{"Required" => "N", "Sequence" => undefined}
,"OnBehalfOfSendingTime" =>#{"Required" => "N", "Sequence" => undefined}
,"HopCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"HopSendingTime" =>#{"Required" => "N", "Sequence" => undefined}
,"HopRefID" =>#{"Required" => "N", "Sequence" => undefined}}, 
"Groups" => #{
                              "NoHops" => #{
                              "Fields" => #{"HopCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"HopSendingTime" =>#{"Required" => "N", "Sequence" => undefined}
,"HopRefID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}, 
 "Components" => #{}

}.



trailer()->
#{
"Fields" => #{"SignatureLength" =>#{"Required" => "N", "Sequence" => undefined}
,"Signature" =>#{"Required" => "N", "Sequence" => undefined}
,"CheckSum" =>#{"Required" => "Y", "Sequence" => undefined}}, 
"Groups" => #{}, 
 "Components" => #{}

}.


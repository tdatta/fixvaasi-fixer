-module(fix44).
-export([messages/0,
         fields/0,
         components/0,
         groups/0,
         header/0,
         trailer/0]).

messages() ->
 #{"Heartbeat" => #{
                              "Category" => "admin"
                              ,"Type" => "0"
                              ,"Fields" => #{"TestReqID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"0" => #{"Category"=>"admin"
           ,"Name" => "Heartbeat"}

,"TestRequest" => #{
                              "Category" => "admin"
                              ,"Type" => "1"
                              ,"Fields" => #{"TestReqID" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"1" => #{"Category"=>"admin"
           ,"Name" => "TestRequest"}

,"ResendRequest" => #{
                              "Category" => "admin"
                              ,"Type" => "2"
                              ,"Fields" => #{"BeginSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}
,"EndSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"2" => #{"Category"=>"admin"
           ,"Name" => "ResendRequest"}

,"Reject" => #{
                              "Category" => "admin"
                              ,"Type" => "3"
                              ,"Fields" => #{"RefSeqNum" =>#{"Required" => "Y", "Sequence" => undefined}
,"RefTagID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefMsgType" =>#{"Required" => "N", "Sequence" => undefined}
,"SessionRejectReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"3" => #{"Category"=>"admin"
           ,"Name" => "Reject"}

,"SequenceReset" => #{
                              "Category" => "admin"
                              ,"Type" => "4"
                              ,"Fields" => #{"GapFillFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"NewSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"4" => #{"Category"=>"admin"
           ,"Name" => "SequenceReset"}

,"Logout" => #{
                              "Category" => "admin"
                              ,"Type" => "5"
                              ,"Fields" => #{"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"5" => #{"Category"=>"admin"
           ,"Name" => "Logout"}

,"IOI" => #{
                              "Category" => "app"
                              ,"Type" => "6"
                              ,"Fields" => #{"IOIID" =>#{"Required" => "Y", "Sequence" => undefined}
,"IOITransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"IOIRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIQltyInd" =>#{"Required" => "N", "Sequence" => undefined}
,"IOINaturalFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"URLLink" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegIOIGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIQualGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"6" => #{"Category"=>"app"
           ,"Name" => "IOI"}

,"Advertisement" => #{
                              "Category" => "app"
                              ,"Type" => "7"
                              ,"Fields" => #{"AdvId" =>#{"Required" => "Y", "Sequence" => undefined}
,"AdvTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"AdvRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"AdvSide" =>#{"Required" => "Y", "Sequence" => undefined}
,"Quantity" =>#{"Required" => "Y", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"URLLink" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"7" => #{"Category"=>"app"
           ,"Name" => "Advertisement"}

,"ExecutionReport" => #{
                              "Category" => "app"
                              ,"Type" => "8"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRespID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdStatusReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"MassStatusReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNumReports" =>#{"Required" => "N", "Sequence" => undefined}
,"LastRptRequested" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"CrossID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigCrossID" =>#{"Required" => "N", "Sequence" => undefined}
,"CrossType" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExecRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecType" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"WorkingIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecRestatementReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"PeggedPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategy" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategyParameters" =>#{"Required" => "N", "Sequence" => undefined}
,"ParticipationRate" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategyPerformance" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"LastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastParPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"LastForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeBracket" =>#{"Required" => "N", "Sequence" => undefined}
,"LastCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"LeavesQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"CumQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"DayOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"DayCumQty" =>#{"Required" => "N", "Sequence" => undefined}
,"DayAvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ReportToExch" =>#{"Required" => "N", "Sequence" => undefined}
,"GrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"NumDaysInterest" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"InterestAtMaturity" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"TradedFlatSwitch" =>#{"Required" => "N", "Sequence" => undefined}
,"BasisFeatureDate" =>#{"Required" => "N", "Sequence" => undefined}
,"BasisFeaturePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"Concession" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalTakedown" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"LastForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"MultiLegReportingType" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}
,"TransBkdTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecValuationPoint" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecPriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecPriceAdjustment" =>#{"Required" => "N", "Sequence" => undefined}
,"PriorityIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceImprovement" =>#{"Required" => "N", "Sequence" => undefined}
,"LastLiquidityInd" =>#{"Required" => "N", "Sequence" => undefined}
,"CopyMsgIndicator" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "N", "Sequence" => undefined}
,"PegInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegExecGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeesGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"8" => #{"Category"=>"app"
           ,"Name" => "ExecutionReport"}

,"OrderCancelReject" => #{
                              "Category" => "app"
                              ,"Type" => "9"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"WorkingIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigOrdModTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"CxlRejResponseTo" =>#{"Required" => "Y", "Sequence" => undefined}
,"CxlRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"9" => #{"Category"=>"app"
           ,"Name" => "OrderCancelReject"}

,"Logon" => #{
                              "Category" => "admin"
                              ,"Type" => "A"
                              ,"Fields" => #{"EncryptMethod" =>#{"Required" => "Y", "Sequence" => undefined}
,"HeartBtInt" =>#{"Required" => "Y", "Sequence" => undefined}
,"RawDataLength" =>#{"Required" => "N", "Sequence" => undefined}
,"RawData" =>#{"Required" => "N", "Sequence" => undefined}
,"ResetSeqNumFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"NextExpectedMsgSeqNum" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxMessageSize" =>#{"Required" => "N", "Sequence" => undefined}
,"RefMsgType" =>#{"Required" => "N", "Sequence" => undefined}
,"MsgDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"TestMessageIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"Username" =>#{"Required" => "N", "Sequence" => undefined}
,"Password" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMsgTypes" => #{
                              "Fields" => #{"RefMsgType" =>#{"Required" => "N", "Sequence" => undefined}
,"MsgDirection" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}
}
,"A" => #{"Category"=>"admin"
           ,"Name" => "Logon"}

,"News" => #{
                              "Category" => "app"
                              ,"Type" => "B"
                              ,"Fields" => #{"OrigTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Urgency" =>#{"Required" => "N", "Sequence" => undefined}
,"Headline" =>#{"Required" => "Y", "Sequence" => undefined}
,"EncodedHeadlineLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedHeadline" =>#{"Required" => "N", "Sequence" => undefined}
,"URLLink" =>#{"Required" => "N", "Sequence" => undefined}
,"RawDataLength" =>#{"Required" => "N", "Sequence" => undefined}
,"RawData" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"RoutingGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"LinesOfTextGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"B" => #{"Category"=>"app"
           ,"Name" => "News"}

,"Email" => #{
                              "Category" => "app"
                              ,"Type" => "C"
                              ,"Fields" => #{"EmailThreadID" =>#{"Required" => "Y", "Sequence" => undefined}
,"EmailType" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Subject" =>#{"Required" => "Y", "Sequence" => undefined}
,"EncodedSubjectLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSubject" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"RawDataLength" =>#{"Required" => "N", "Sequence" => undefined}
,"RawData" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"RoutingGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"LinesOfTextGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"C" => #{"Category"=>"app"
           ,"Name" => "Email"}

,"NewOrderSingle" => #{
                              "Category" => "app"
                              ,"Type" => "D"
                              ,"Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategy" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategyParameters" =>#{"Required" => "N", "Sequence" => undefined}
,"ParticipationRate" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"PreAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdgSesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
,"PegInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInstructions" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"D" => #{"Category"=>"app"
           ,"Name" => "NewOrderSingle"}

,"NewOrderList" => #{
                              "Category" => "app"
                              ,"Type" => "E"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"BidID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientBidID" =>#{"Required" => "N", "Sequence" => undefined}
,"ProgRptReqs" =>#{"Required" => "N", "Sequence" => undefined}
,"BidType" =>#{"Required" => "Y", "Sequence" => undefined}
,"ProgPeriodInterval" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListExecInstType" =>#{"Required" => "N", "Sequence" => undefined}
,"ListExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedListExecInstLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedListExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"AllowableOneSidednessPct" =>#{"Required" => "N", "Sequence" => undefined}
,"AllowableOneSidednessValue" =>#{"Required" => "N", "Sequence" => undefined}
,"AllowableOneSidednessCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoOrders" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ListOrdGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"E" => #{"Category"=>"app"
           ,"Name" => "NewOrderList"}

,"OrderCancelRequest" => #{
                              "Category" => "app"
                              ,"Type" => "F"
                              ,"Fields" => #{"OrigClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigOrdModTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"F" => #{"Category"=>"app"
           ,"Name" => "OrderCancelRequest"}

,"OrderCancelReplaceRequest" => #{
                              "Category" => "app"
                              ,"Type" => "G"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigOrdModTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategy" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategyParameters" =>#{"Required" => "N", "Sequence" => undefined}
,"ParticipationRate" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"PreAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdgSesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"PegInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"G" => #{"Category"=>"app"
           ,"Name" => "OrderCancelReplaceRequest"}

,"OrderStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "H"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdStatusReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"H" => #{"Category"=>"app"
           ,"Name" => "OrderStatusRequest"}

,"AllocationInstruction" => #{
                              "Category" => "app"
                              ,"Type" => "J"
                              ,"Fields" => #{"AllocID" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocType" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocCancReplaceReason" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocIntermedReqType" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocLinkType" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocNoOrdersType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PreviouslyReported" =>#{"Required" => "N", "Sequence" => undefined}
,"ReversalIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchType" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"Quantity" =>#{"Required" => "Y", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"AvgParPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPxPrecision" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"GrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"Concession" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalTakedown" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"AutoAcceptIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"NumDaysInterest" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"InterestAtMaturity" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"LegalConfirm" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoAllocs" =>#{"Required" => "N", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"OrdAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"J" => #{"Category"=>"app"
           ,"Name" => "AllocationInstruction"}

,"ListCancelRequest" => #{
                              "Category" => "app"
                              ,"Type" => "K"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"K" => #{"Category"=>"app"
           ,"Name" => "ListCancelRequest"}

,"ListExecute" => #{
                              "Category" => "app"
                              ,"Type" => "L"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClientBidID" =>#{"Required" => "N", "Sequence" => undefined}
,"BidID" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"L" => #{"Category"=>"app"
           ,"Name" => "ListExecute"}

,"ListStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "M"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"M" => #{"Category"=>"app"
           ,"Name" => "ListStatusRequest"}

,"ListStatus" => #{
                              "Category" => "app"
                              ,"Type" => "N"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListStatusType" =>#{"Required" => "Y", "Sequence" => undefined}
,"NoRpts" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListOrderStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"RptSeq" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListStatusText" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedListStatusTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedListStatusText" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoOrders" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"OrdListStatGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"N" => #{"Category"=>"app"
           ,"Name" => "ListStatus"}

,"AllocationInstructionAck" => #{
                              "Category" => "app"
                              ,"Type" => "P"
                              ,"Fields" => #{"AllocID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocRejCode" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocType" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocIntermedReqType" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"Product" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAckGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"P" => #{"Category"=>"app"
           ,"Name" => "AllocationInstructionAck"}

,"DontKnowTrade" => #{
                              "Category" => "app"
                              ,"Type" => "Q"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "Y", "Sequence" => undefined}
,"DKReason" =>#{"Required" => "Y", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"Q" => #{"Category"=>"app"
           ,"Name" => "DontKnowTrade"}

,"QuoteRequest" => #{
                              "Category" => "app"
                              ,"Type" => "R"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"RFQReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"QuotReqGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"R" => #{"Category"=>"app"
           ,"Name" => "QuoteRequest"}

,"Quote" => #{
                              "Category" => "app"
                              ,"Type" => "S"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteRespID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteResponseLevel" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MktBidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MktOfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MinBidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"MinOfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"MidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"MidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferYield" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrBidFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrOfferFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"QuotQualGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"LegQuotGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"S" => #{"Category"=>"app"
           ,"Name" => "Quote"}

,"SettlementInstructions" => #{
                              "Category" => "app"
                              ,"Type" => "T"
                              ,"Fields" => #{"SettlInstMsgID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlInstReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstMode" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlInstReqRejCode" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"SettlInstGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"T" => #{"Category"=>"app"
           ,"Name" => "SettlementInstructions"}

,"MarketDataRequest" => #{
                              "Category" => "app"
                              ,"Type" => "V"
                              ,"Fields" => #{"MDReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MarketDepth" =>#{"Required" => "Y", "Sequence" => undefined}
,"MDUpdateType" =>#{"Required" => "N", "Sequence" => undefined}
,"AggregatedBook" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenCloseSettlFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"Scope" =>#{"Required" => "N", "Sequence" => undefined}
,"MDImplicitDelete" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplQueueAction" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplQueueMax" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"MDReqGrp" =>#{"Required" => "Y", "Sequence" => undefined}
,"InstrmtMDReqGrp" =>#{"Required" => "Y", "Sequence" => undefined}
,"TrdgSesGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"V" => #{"Category"=>"app"
           ,"Name" => "MarketDataRequest"}

,"MarketDataSnapshotFullRefresh" => #{
                              "Category" => "app"
                              ,"Type" => "W"
                              ,"Fields" => #{"MDReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancialStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"NetChgPrevDay" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplQueueDepth" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplQueueResolution" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"MDFullGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"W" => #{"Category"=>"app"
           ,"Name" => "MarketDataSnapshotFullRefresh"}

,"MarketDataIncrementalRefresh" => #{
                              "Category" => "app"
                              ,"Type" => "X"
                              ,"Fields" => #{"MDReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplQueueDepth" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplQueueResolution" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"MDIncGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"X" => #{"Category"=>"app"
           ,"Name" => "MarketDataIncrementalRefresh"}

,"MarketDataRequestReject" => #{
                              "Category" => "app"
                              ,"Type" => "Y"
                              ,"Fields" => #{"MDReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"MDReqRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"MDRjctGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"Y" => #{"Category"=>"app"
           ,"Name" => "MarketDataRequestReject"}

,"QuoteCancel" => #{
                              "Category" => "app"
                              ,"Type" => "Z"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteCancelType" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteResponseLevel" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotCxlEntriesGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"Z" => #{"Category"=>"app"
           ,"Name" => "QuoteCancel"}

,"QuoteStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "a"
                              ,"Fields" => #{"QuoteStatusReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"a" => #{"Category"=>"app"
           ,"Name" => "QuoteStatusRequest"}

,"MassQuoteAcknowledgement" => #{
                              "Category" => "app"
                              ,"Type" => "b"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteRejectReason" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteResponseLevel" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotSetAckGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"b" => #{"Category"=>"app"
           ,"Name" => "MassQuoteAcknowledgement"}

,"SecurityDefinitionRequest" => #{
                              "Category" => "app"
                              ,"Type" => "c"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpirationCycle" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"c" => #{"Category"=>"app"
           ,"Name" => "SecurityDefinitionRequest"}

,"SecurityDefinition" => #{
                              "Category" => "app"
                              ,"Type" => "d"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityResponseID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityResponseType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpirationCycle" =>#{"Required" => "N", "Sequence" => undefined}
,"RoundLot" =>#{"Required" => "N", "Sequence" => undefined}
,"MinTradeVol" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"d" => #{"Category"=>"app"
           ,"Name" => "SecurityDefinition"}

,"SecurityStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "e"
                              ,"Fields" => #{"SecurityStatusReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"e" => #{"Category"=>"app"
           ,"Name" => "SecurityStatusRequest"}

,"SecurityStatus" => #{
                              "Category" => "app"
                              ,"Type" => "f"
                              ,"Fields" => #{"SecurityStatusReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnsolicitedIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityTradingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancialStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"HaltReasonChar" =>#{"Required" => "N", "Sequence" => undefined}
,"InViewOfCommon" =>#{"Required" => "N", "Sequence" => undefined}
,"DueToRelated" =>#{"Required" => "N", "Sequence" => undefined}
,"BuyVolume" =>#{"Required" => "N", "Sequence" => undefined}
,"SellVolume" =>#{"Required" => "N", "Sequence" => undefined}
,"HighPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LowPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Adjustment" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"f" => #{"Category"=>"app"
           ,"Name" => "SecurityStatus"}

,"TradingSessionStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "g"
                              ,"Fields" => #{"TradSesReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesMode" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"g" => #{"Category"=>"app"
           ,"Name" => "TradingSessionStatusRequest"}

,"TradingSessionStatus" => #{
                              "Category" => "app"
                              ,"Type" => "h"
                              ,"Fields" => #{"TradSesReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesMode" =>#{"Required" => "N", "Sequence" => undefined}
,"UnsolicitedIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradSesStatusRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesStartTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesOpenTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesPreCloseTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesCloseTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesEndTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalVolumeTraded" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"h" => #{"Category"=>"app"
           ,"Name" => "TradingSessionStatus"}

,"MassQuote" => #{
                              "Category" => "app"
                              ,"Type" => "i"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteResponseLevel" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DefBidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"DefOfferSize" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotSetGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"i" => #{"Category"=>"app"
           ,"Name" => "MassQuote"}

,"BusinessMessageReject" => #{
                              "Category" => "app"
                              ,"Type" => "j"
                              ,"Fields" => #{"RefSeqNum" =>#{"Required" => "N", "Sequence" => undefined}
,"RefMsgType" =>#{"Required" => "Y", "Sequence" => undefined}
,"BusinessRejectRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"BusinessRejectReason" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"j" => #{"Category"=>"app"
           ,"Name" => "BusinessMessageReject"}

,"BidRequest" => #{
                              "Category" => "app"
                              ,"Type" => "k"
                              ,"Fields" => #{"BidID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientBidID" =>#{"Required" => "Y", "Sequence" => undefined}
,"BidRequestTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListName" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoRelatedSym" =>#{"Required" => "Y", "Sequence" => undefined}
,"BidType" =>#{"Required" => "Y", "Sequence" => undefined}
,"NumTickets" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"SideValue1" =>#{"Required" => "N", "Sequence" => undefined}
,"SideValue2" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityIndType" =>#{"Required" => "N", "Sequence" => undefined}
,"WtAverageLiquidity" =>#{"Required" => "N", "Sequence" => undefined}
,"ExchangeForPhysical" =>#{"Required" => "N", "Sequence" => undefined}
,"OutMainCntryUIndex" =>#{"Required" => "N", "Sequence" => undefined}
,"CrossPercent" =>#{"Required" => "N", "Sequence" => undefined}
,"ProgRptReqs" =>#{"Required" => "N", "Sequence" => undefined}
,"ProgPeriodInterval" =>#{"Required" => "N", "Sequence" => undefined}
,"IncTaxInd" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"NumBidders" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidTradeType" =>#{"Required" => "Y", "Sequence" => undefined}
,"BasisPxType" =>#{"Required" => "Y", "Sequence" => undefined}
,"StrikeTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"BidDescReqGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"BidCompReqGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"k" => #{"Category"=>"app"
           ,"Name" => "BidRequest"}

,"BidResponse" => #{
                              "Category" => "app"
                              ,"Type" => "l"
                              ,"Fields" => #{"BidID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientBidID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"BidCompRspGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"l" => #{"Category"=>"app"
           ,"Name" => "BidResponse"}

,"ListStrikePrice" => #{
                              "Category" => "app"
                              ,"Type" => "m"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TotNoStrikes" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrmtStrkPxGrp" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtStrkPxGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"m" => #{"Category"=>"app"
           ,"Name" => "ListStrikePrice"}

,"XMLnonFIX" => #{
                              "Category" => "admin"
                              ,"Type" => "n"
                              ,"Fields" => #{}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"n" => #{"Category"=>"admin"
           ,"Name" => "XMLnonFIX"}

,"RegistrationInstructions" => #{
                              "Category" => "app"
                              ,"Type" => "o"
                              ,"Fields" => #{"RegistID" =>#{"Required" => "Y", "Sequence" => undefined}
,"RegistTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"RegistRefID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistAcctType" =>#{"Required" => "N", "Sequence" => undefined}
,"TaxAdvantageType" =>#{"Required" => "N", "Sequence" => undefined}
,"OwnershipType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"RgstDtlsGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"RgstDistInstGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"o" => #{"Category"=>"app"
           ,"Name" => "RegistrationInstructions"}

,"RegistrationInstructionsResponse" => #{
                              "Category" => "app"
                              ,"Type" => "p"
                              ,"Fields" => #{"RegistID" =>#{"Required" => "Y", "Sequence" => undefined}
,"RegistTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"RegistRefID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"RegistRejReasonCode" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistRejReasonText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"p" => #{"Category"=>"app"
           ,"Name" => "RegistrationInstructionsResponse"}

,"OrderMassCancelRequest" => #{
                              "Category" => "app"
                              ,"Type" => "q"
                              ,"Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"MassCancelRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"q" => #{"Category"=>"app"
           ,"Name" => "OrderMassCancelRequest"}

,"OrderMassCancelReport" => #{
                              "Category" => "app"
                              ,"Type" => "r"
                              ,"Fields" => #{"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"MassCancelRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MassCancelResponse" =>#{"Required" => "Y", "Sequence" => undefined}
,"MassCancelRejectReason" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalAffectedOrders" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"AffectedOrdGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"r" => #{"Category"=>"app"
           ,"Name" => "OrderMassCancelReport"}

,"NewOrderCross" => #{
                              "Category" => "app"
                              ,"Type" => "s"
                              ,"Fields" => #{"CrossID" =>#{"Required" => "Y", "Sequence" => undefined}
,"CrossType" =>#{"Required" => "Y", "Sequence" => undefined}
,"CrossPrioritization" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategy" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategyParameters" =>#{"Required" => "N", "Sequence" => undefined}
,"ParticipationRate" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"SideCrossOrdModGrp" =>#{"Required" => "Y", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdgSesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"PegInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInstructions" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"s" => #{"Category"=>"app"
           ,"Name" => "NewOrderCross"}

,"CrossOrderCancelReplaceRequest" => #{
                              "Category" => "app"
                              ,"Type" => "t"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"CrossID" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigCrossID" =>#{"Required" => "Y", "Sequence" => undefined}
,"CrossType" =>#{"Required" => "Y", "Sequence" => undefined}
,"CrossPrioritization" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategy" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategyParameters" =>#{"Required" => "N", "Sequence" => undefined}
,"ParticipationRate" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"SideCrossOrdModGrp" =>#{"Required" => "Y", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdgSesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"PegInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInstructions" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"t" => #{"Category"=>"app"
           ,"Name" => "CrossOrderCancelReplaceRequest"}

,"CrossOrderCancelRequest" => #{
                              "Category" => "app"
                              ,"Type" => "u"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"CrossID" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigCrossID" =>#{"Required" => "Y", "Sequence" => undefined}
,"CrossType" =>#{"Required" => "Y", "Sequence" => undefined}
,"CrossPrioritization" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"SideCrossOrdCxlGrp" =>#{"Required" => "Y", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"u" => #{"Category"=>"app"
           ,"Name" => "CrossOrderCancelRequest"}

,"SecurityTypeRequest" => #{
                              "Category" => "app"
                              ,"Type" => "v"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Product" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySubType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"v" => #{"Category"=>"app"
           ,"Name" => "SecurityTypeRequest"}

,"SecurityTypes" => #{
                              "Category" => "app"
                              ,"Type" => "w"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityResponseID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityResponseType" =>#{"Required" => "Y", "Sequence" => undefined}
,"TotNoSecurityTypes" =>#{"Required" => "N", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"SecTypesGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"w" => #{"Category"=>"app"
           ,"Name" => "SecurityTypes"}

,"SecurityListRequest" => #{
                              "Category" => "app"
                              ,"Type" => "x"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityListRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"x" => #{"Category"=>"app"
           ,"Name" => "SecurityListRequest"}

,"SecurityList" => #{
                              "Category" => "app"
                              ,"Type" => "y"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityResponseID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityRequestResult" =>#{"Required" => "Y", "Sequence" => undefined}
,"TotNoRelatedSym" =>#{"Required" => "N", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"SecListGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"y" => #{"Category"=>"app"
           ,"Name" => "SecurityList"}

,"DerivativeSecurityListRequest" => #{
                              "Category" => "app"
                              ,"Type" => "z"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityListRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecuritySubType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"z" => #{"Category"=>"app"
           ,"Name" => "DerivativeSecurityListRequest"}

,"DerivativeSecurityList" => #{
                              "Category" => "app"
                              ,"Type" => "AA"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityResponseID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityRequestResult" =>#{"Required" => "Y", "Sequence" => undefined}
,"TotNoRelatedSym" =>#{"Required" => "N", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
,"RelSymDerivSecGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AA" => #{"Category"=>"app"
           ,"Name" => "DerivativeSecurityList"}

,"NewOrderMultileg" => #{
                              "Category" => "app"
                              ,"Type" => "AB"
                              ,"Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategy" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategyParameters" =>#{"Required" => "N", "Sequence" => undefined}
,"ParticipationRate" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}
,"MultiLegRptTypeReq" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"PreAllocMlegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdgSesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOrdGrp" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
,"PegInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInstructions" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AB" => #{"Category"=>"app"
           ,"Name" => "NewOrderMultileg"}

,"MultilegOrderCancelReplace" => #{
                              "Category" => "app"
                              ,"Type" => "AC"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigOrdModTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategy" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategyParameters" =>#{"Required" => "N", "Sequence" => undefined}
,"ParticipationRate" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}
,"MultiLegRptTypeReq" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"PreAllocMlegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdgSesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOrdGrp" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
,"PegInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInstructions" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AC" => #{"Category"=>"app"
           ,"Name" => "MultilegOrderCancelReplace"}

,"TradeCaptureReportRequest" => #{
                              "Category" => "app"
                              ,"Type" => "AD"
                              ,"Fields" => #{"TradeRequestID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTradeReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdSubType" =>#{"Required" => "N", "Sequence" => undefined}
,"TransferReason" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdMatchID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeBracket" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"MultiLegReportingType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputSource" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputDevice" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseTransportType" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdCapDtGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AD" => #{"Category"=>"app"
           ,"Name" => "TradeCaptureReportRequest"}

,"TradeCaptureReport" => #{
                              "Category" => "app"
                              ,"Type" => "AE"
                              ,"Fields" => #{"TradeReportID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeReportTransType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeReportType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeRequestID" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdSubType" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"TransferReason" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecType" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNumTradeReports" =>#{"Required" => "N", "Sequence" => undefined}
,"LastRptRequested" =>#{"Required" => "N", "Sequence" => undefined}
,"UnsolicitedIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeReportRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTradeReportRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTradeReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdMatchID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecRestatementReason" =>#{"Required" => "N", "Sequence" => undefined}
,"PreviouslyReported" =>#{"Required" => "Y", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingTradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingTradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"LastQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastParPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"LastForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPxIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"MultiLegReportingType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeLegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchType" =>#{"Required" => "N", "Sequence" => undefined}
,"CopyMsgIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"PublishTrdIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"ShortSaleReason" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionAmountData" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdInstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestamps" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdCapRptSideGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"AE" => #{"Category"=>"app"
           ,"Name" => "TradeCaptureReport"}

,"OrderMassStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "AF"
                              ,"Fields" => #{"MassStatusReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"MassStatusReqType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AF" => #{"Category"=>"app"
           ,"Name" => "OrderMassStatusRequest"}

,"QuoteRequestReject" => #{
                              "Category" => "app"
                              ,"Type" => "AG"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"RFQReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestRejectReason" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"QuotReqRjctGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"AG" => #{"Category"=>"app"
           ,"Name" => "QuoteRequestReject"}

,"RFQRequest" => #{
                              "Category" => "app"
                              ,"Type" => "AH"
                              ,"Fields" => #{"RFQReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"RFQReqGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"AH" => #{"Category"=>"app"
           ,"Name" => "RFQRequest"}

,"QuoteStatusReport" => #{
                              "Category" => "app"
                              ,"Type" => "AI"
                              ,"Fields" => #{"QuoteStatusReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteRespID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MktBidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MktOfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MinBidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"MinOfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"MidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"MidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferYield" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrBidFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrOfferFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"LegQuotStatGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotQualGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AI" => #{"Category"=>"app"
           ,"Name" => "QuoteStatusReport"}

,"QuoteResponse" => #{
                              "Category" => "app"
                              ,"Type" => "AJ"
                              ,"Fields" => #{"QuoteRespID" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRespType" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MktBidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MktOfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MinBidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"MinOfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"MidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"MidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferYield" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrBidFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrOfferFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"QuotQualGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"LegQuotGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AJ" => #{"Category"=>"app"
           ,"Name" => "QuoteResponse"}

,"Confirmation" => #{
                              "Category" => "app"
                              ,"Type" => "AK"
                              ,"Fields" => #{"ConfirmID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ConfirmRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"ConfirmReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"ConfirmTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"ConfirmType" =>#{"Required" => "Y", "Sequence" => undefined}
,"CopyMsgIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"LegalConfirm" =>#{"Required" => "N", "Sequence" => undefined}
,"ConfirmStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"AvgPxPrecision" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgParPx" =>#{"Required" => "N", "Sequence" => undefined}
,"ReportedPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"GrossTradeAmt" =>#{"Required" => "Y", "Sequence" => undefined}
,"NumDaysInterest" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"InterestAtMaturity" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"Concession" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalTakedown" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "Y", "Sequence" => undefined}
,"MaturityNetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"SharedCommission" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestamps" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "Y", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "Y", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"CpctyConfGrp" =>#{"Required" => "Y", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstructionsData" =>#{"Required" => "N", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeesGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AK" => #{"Category"=>"app"
           ,"Name" => "Confirmation"}

,"PositionMaintenanceRequest" => #{
                              "Category" => "app"
                              ,"Type" => "AL"
                              ,"Fields" => #{"PosReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"PosTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PosMaintAction" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigPosReqRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"PosMaintRptRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "Y", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"AdjustmentType" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraryInstructionIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"PriorSpreadIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"ThresholdAmount" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "Y", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdgSesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionQty" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"AL" => #{"Category"=>"app"
           ,"Name" => "PositionMaintenanceRequest"}

,"PositionMaintenanceReport" => #{
                              "Category" => "app"
                              ,"Type" => "AM"
                              ,"Fields" => #{"PosMaintRptID" =>#{"Required" => "Y", "Sequence" => undefined}
,"PosTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PosReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"PosMaintAction" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigPosReqRefID" =>#{"Required" => "Y", "Sequence" => undefined}
,"PosMaintStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"PosMaintResult" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "Y", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"AdjustmentType" =>#{"Required" => "N", "Sequence" => undefined}
,"ThresholdAmount" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdgSesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"PositionAmountData" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"AM" => #{"Category"=>"app"
           ,"Name" => "PositionMaintenanceReport"}

,"RequestForPositions" => #{
                              "Category" => "app"
                              ,"Type" => "AN"
                              ,"Fields" => #{"PosReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"PosReqType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MatchStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "Y", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"ResponseTransportType" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "Y", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdgSesGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AN" => #{"Category"=>"app"
           ,"Name" => "RequestForPositions"}

,"RequestForPositionsAck" => #{
                              "Category" => "app"
                              ,"Type" => "AO"
                              ,"Fields" => #{"PosMaintRptID" =>#{"Required" => "Y", "Sequence" => undefined}
,"PosReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalNumPosReports" =>#{"Required" => "N", "Sequence" => undefined}
,"UnsolicitedIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"PosReqResult" =>#{"Required" => "Y", "Sequence" => undefined}
,"PosReqStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"Account" =>#{"Required" => "Y", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseTransportType" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "Y", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AO" => #{"Category"=>"app"
           ,"Name" => "RequestForPositionsAck"}

,"PositionReport" => #{
                              "Category" => "app"
                              ,"Type" => "AP"
                              ,"Fields" => #{"PosMaintRptID" =>#{"Required" => "Y", "Sequence" => undefined}
,"PosReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"PosReqType" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalNumPosReports" =>#{"Required" => "N", "Sequence" => undefined}
,"UnsolicitedIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"PosReqResult" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "Y", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlPrice" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlPriceType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PriorSettlPrice" =>#{"Required" => "Y", "Sequence" => undefined}
,"RegistStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"DeliveryDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "Y", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"PosUndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"PositionAmountData" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"AP" => #{"Category"=>"app"
           ,"Name" => "PositionReport"}

,"TradeCaptureReportRequestAck" => #{
                              "Category" => "app"
                              ,"Type" => "AQ"
                              ,"Fields" => #{"TradeRequestID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNumTradeReports" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeRequestResult" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeRequestStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"MultiLegReportingType" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseTransportType" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AQ" => #{"Category"=>"app"
           ,"Name" => "TradeCaptureReportRequestAck"}

,"TradeCaptureReportAck" => #{
                              "Category" => "app"
                              ,"Type" => "AR"
                              ,"Fields" => #{"TradeReportID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeReportTransType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeReportType" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdSubType" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"TransferReason" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecType" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeReportRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTradeReportRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRptStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeReportRejectReason" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTradeReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdMatchID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseTransportType" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"TrdRegTimestamps" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdInstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AR" => #{"Category"=>"app"
           ,"Name" => "TradeCaptureReportAck"}

,"AllocationReport" => #{
                              "Category" => "app"
                              ,"Type" => "AS"
                              ,"Fields" => #{"AllocReportID" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocReportRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocCancReplaceReason" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocReportType" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocRejCode" =>#{"Required" => "N", "Sequence" => undefined}
,"RefAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocIntermedReqType" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocLinkType" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocNoOrdersType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PreviouslyReported" =>#{"Required" => "N", "Sequence" => undefined}
,"ReversalIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchType" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"Quantity" =>#{"Required" => "Y", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"AvgParPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPxPrecision" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"GrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"Concession" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalTakedown" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"AutoAcceptIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"NumDaysInterest" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"InterestAtMaturity" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"LegalConfirm" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoAllocs" =>#{"Required" => "N", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"OrdAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AS" => #{"Category"=>"app"
           ,"Name" => "AllocationReport"}

,"AllocationReportAck" => #{
                              "Category" => "app"
                              ,"Type" => "AT"
                              ,"Fields" => #{"AllocReportID" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocRejCode" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocReportType" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocIntermedReqType" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"Product" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAckGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AT" => #{"Category"=>"app"
           ,"Name" => "AllocationReportAck"}

,"ConfirmationAck" => #{
                              "Category" => "app"
                              ,"Type" => "AU"
                              ,"Fields" => #{"ConfirmID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"AffirmStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"ConfirmRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"AU" => #{"Category"=>"app"
           ,"Name" => "ConfirmationAck"}

,"SettlementInstructionRequest" => #{
                              "Category" => "app"
                              ,"Type" => "AV"
                              ,"Fields" => #{"SettlInstReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Product" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"CFICode" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"LastUpdateTime" =>#{"Required" => "N", "Sequence" => undefined}
,"StandInstDbType" =>#{"Required" => "N", "Sequence" => undefined}
,"StandInstDbName" =>#{"Required" => "N", "Sequence" => undefined}
,"StandInstDbID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AV" => #{"Category"=>"app"
           ,"Name" => "SettlementInstructionRequest"}

,"AssignmentReport" => #{
                              "Category" => "app"
                              ,"Type" => "AW"
                              ,"Fields" => #{"AsgnRptID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TotNumAssignmentReports" =>#{"Required" => "N", "Sequence" => undefined}
,"LastRptRequested" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ThresholdAmount" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlPrice" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlPriceType" =>#{"Required" => "Y", "Sequence" => undefined}
,"UnderlyingSettlPrice" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AssignmentMethod" =>#{"Required" => "Y", "Sequence" => undefined}
,"AssignmentUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenInterest" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExerciseMethod" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "Y", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"PositionAmountData" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"AW" => #{"Category"=>"app"
           ,"Name" => "AssignmentReport"}

,"CollateralRequest" => #{
                              "Category" => "app"
                              ,"Type" => "AX"
                              ,"Fields" => #{"CollReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"CollAsgnReason" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Quantity" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MarginExcess" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalNetValue" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOutstanding" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestamps" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AX" => #{"Category"=>"app"
           ,"Name" => "CollateralRequest"}

,"CollateralAssignment" => #{
                              "Category" => "app"
                              ,"Type" => "AY"
                              ,"Fields" => #{"CollAsgnID" =>#{"Required" => "Y", "Sequence" => undefined}
,"CollReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"CollAsgnReason" =>#{"Required" => "Y", "Sequence" => undefined}
,"CollAsgnTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"CollAsgnRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Quantity" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MarginExcess" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalNetValue" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOutstanding" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestamps" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstructionsData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AY" => #{"Category"=>"app"
           ,"Name" => "CollateralAssignment"}

,"CollateralResponse" => #{
                              "Category" => "app"
                              ,"Type" => "AZ"
                              ,"Fields" => #{"CollRespID" =>#{"Required" => "Y", "Sequence" => undefined}
,"CollAsgnID" =>#{"Required" => "Y", "Sequence" => undefined}
,"CollReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"CollAsgnReason" =>#{"Required" => "Y", "Sequence" => undefined}
,"CollAsgnTransType" =>#{"Required" => "N", "Sequence" => undefined}
,"CollAsgnRespType" =>#{"Required" => "Y", "Sequence" => undefined}
,"CollAsgnRejectReason" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Quantity" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MarginExcess" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalNetValue" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOutstanding" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestamps" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AZ" => #{"Category"=>"app"
           ,"Name" => "CollateralResponse"}

,"CollateralReport" => #{
                              "Category" => "app"
                              ,"Type" => "BA"
                              ,"Fields" => #{"CollRptID" =>#{"Required" => "Y", "Sequence" => undefined}
,"CollInquiryID" =>#{"Required" => "N", "Sequence" => undefined}
,"CollStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"TotNumReports" =>#{"Required" => "N", "Sequence" => undefined}
,"LastRptRequested" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Quantity" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MarginExcess" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalNetValue" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOutstanding" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestamps" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstructionsData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"BA" => #{"Category"=>"app"
           ,"Name" => "CollateralReport"}

,"CollateralInquiry" => #{
                              "Category" => "app"
                              ,"Type" => "BB"
                              ,"Fields" => #{"CollInquiryID" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseTransportType" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Quantity" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MarginExcess" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalNetValue" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOutstanding" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"CollInqQualGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestamps" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstructionsData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"BB" => #{"Category"=>"app"
           ,"Name" => "CollateralInquiry"}

,"NetworkCounterpartySystemStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "BC"
                              ,"Fields" => #{"NetworkRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"NetworkRequestID" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"CompIDReqGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"BC" => #{"Category"=>"app"
           ,"Name" => "NetworkCounterpartySystemStatusRequest"}

,"NetworkCounterpartySystemStatusResponse" => #{
                              "Category" => "app"
                              ,"Type" => "BD"
                              ,"Fields" => #{"NetworkStatusResponseType" =>#{"Required" => "Y", "Sequence" => undefined}
,"NetworkRequestID" =>#{"Required" => "N", "Sequence" => undefined}
,"NetworkResponseID" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastNetworkResponseID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"CompIDStatGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"BD" => #{"Category"=>"app"
           ,"Name" => "NetworkCounterpartySystemStatusResponse"}

,"UserRequest" => #{
                              "Category" => "app"
                              ,"Type" => "BE"
                              ,"Fields" => #{"UserRequestID" =>#{"Required" => "Y", "Sequence" => undefined}
,"UserRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Username" =>#{"Required" => "Y", "Sequence" => undefined}
,"Password" =>#{"Required" => "N", "Sequence" => undefined}
,"NewPassword" =>#{"Required" => "N", "Sequence" => undefined}
,"RawDataLength" =>#{"Required" => "N", "Sequence" => undefined}
,"RawData" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"BE" => #{"Category"=>"app"
           ,"Name" => "UserRequest"}

,"UserResponse" => #{
                              "Category" => "app"
                              ,"Type" => "BF"
                              ,"Fields" => #{"UserRequestID" =>#{"Required" => "Y", "Sequence" => undefined}
,"Username" =>#{"Required" => "Y", "Sequence" => undefined}
,"UserStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"UserStatusText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"BF" => #{"Category"=>"app"
           ,"Name" => "UserResponse"}

,"CollateralInquiryAck" => #{
                              "Category" => "app"
                              ,"Type" => "BG"
                              ,"Fields" => #{"CollInquiryID" =>#{"Required" => "Y", "Sequence" => undefined}
,"CollInquiryStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"CollInquiryResult" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNumReports" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Quantity" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseTransportType" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"CollInqQualGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"BG" => #{"Category"=>"app"
           ,"Name" => "CollateralInquiryAck"}

,"ConfirmationRequest" => #{
                              "Category" => "app"
                              ,"Type" => "BH"
                              ,"Fields" => #{"ConfirmReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ConfirmType" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"OrdAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"BH" => #{"Category"=>"app"
           ,"Name" => "ConfirmationRequest"}
}.


fields() ->
#{
"Account" => #{"TagNum" => "1" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1" => #{"Name"=>"Account" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1"}


,
"AdvId" => #{"TagNum" => "2" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "2" => #{"Name"=>"AdvId" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "2"}


,
"AdvRefID" => #{"TagNum" => "3" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "3" => #{"Name"=>"AdvRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "3"}


,
"AdvSide" => #{"TagNum" => "4" ,"Type" => "CHAR" ,"ValidValues" =>[{"B", "BUY"},{"S", "SELL"},{"X", "CROSS"},{"T", "TRADE"}]}
, "4" => #{"Name"=>"AdvSide" ,"Type"=>"CHAR" ,"ValidValues"=>[{"B", "BUY"},{"S", "SELL"},{"X", "CROSS"},{"T", "TRADE"}], "TagNum" => "4"}


,
"AdvTransType" => #{"TagNum" => "5" ,"Type" => "STRING" ,"ValidValues" =>[{"N", "NEW"},{"C", "CANCEL"},{"R", "REPLACE"}]}
, "5" => #{"Name"=>"AdvTransType" ,"Type"=>"STRING" ,"ValidValues"=>[{"N", "NEW"},{"C", "CANCEL"},{"R", "REPLACE"}], "TagNum" => "5"}


,
"AvgPx" => #{"TagNum" => "6" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "6" => #{"Name"=>"AvgPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "6"}


,
"BeginSeqNo" => #{"TagNum" => "7" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "7" => #{"Name"=>"BeginSeqNo" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "7"}


,
"BeginString" => #{"TagNum" => "8" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "8" => #{"Name"=>"BeginString" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "8"}


,
"BodyLength" => #{"TagNum" => "9" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "9" => #{"Name"=>"BodyLength" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "9"}


,
"CheckSum" => #{"TagNum" => "10" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "10" => #{"Name"=>"CheckSum" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "10"}


,
"ClOrdID" => #{"TagNum" => "11" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "11" => #{"Name"=>"ClOrdID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "11"}


,
"Commission" => #{"TagNum" => "12" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "12" => #{"Name"=>"Commission" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "12"}


,
"CommType" => #{"TagNum" => "13" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "PER_UNIT"},{"2", "PERCENTAGE"},{"3", "ABSOLUTE"},{"4", "4"},{"5", "5"},{"6", "POINTS_PER_BOND_OR_CONTRACT_SUPPLY_CONTRACTMULTIPLIER"}]}
, "13" => #{"Name"=>"CommType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "PER_UNIT"},{"2", "PERCENTAGE"},{"3", "ABSOLUTE"},{"4", "4"},{"5", "5"},{"6", "POINTS_PER_BOND_OR_CONTRACT_SUPPLY_CONTRACTMULTIPLIER"}], "TagNum" => "13"}


,
"CumQty" => #{"TagNum" => "14" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "14" => #{"Name"=>"CumQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "14"}


,
"Currency" => #{"TagNum" => "15" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "15" => #{"Name"=>"Currency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "15"}


,
"EndSeqNo" => #{"TagNum" => "16" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "16" => #{"Name"=>"EndSeqNo" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "16"}


,
"ExecID" => #{"TagNum" => "17" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "17" => #{"Name"=>"ExecID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "17"}


,
"ExecInst" => #{"TagNum" => "18" ,"Type" => "MULTIPLEVALUESTRING" ,"ValidValues" =>[{"1", "NOT_HELD"},{"2", "WORK"},{"3", "GO_ALONG"},{"4", "OVER_THE_DAY"},{"5", "HELD"},{"6", "PARTICIPATE_DONT_INITIATE"},{"7", "STRICT_SCALE"},{"8", "TRY_TO_SCALE"},{"9", "STAY_ON_BIDSIDE"},{"0", "STAY_ON_OFFERSIDE"},{"A", "NO_CROSS"},{"B", "OK_TO_CROSS"},{"C", "CALL_FIRST"},{"D", "PERCENT_OF_VOLUME"},{"E", "DO_NOT_INCREASE"},{"F", "DO_NOT_REDUCE"},{"G", "ALL_OR_NONE"},{"H", "REINSTATE_ON_SYSTEM_FAILURE"},{"I", "INSTITUTIONS_ONLY"},{"J", "REINSTATE_ON_TRADING_HALT"},{"K", "CANCEL_ON_TRADING_HALT"},{"L", "LAST_PEG"},{"M", "MID_PRICE_PEG"},{"N", "NON_NEGOTIABLE"},{"O", "OPENING_PEG"},{"P", "MARKET_PEG"},{"Q", "CANCEL_ON_SYSTEM_FAILURE"},{"R", "PRIMARY_PEG"},{"S", "SUSPEND"},{"U", "CUSTOMER_DISPLAY_INSTRUCTION"},{"V", "NETTING"},{"W", "PEG_TO_VWAP"},{"X", "TRADE_ALONG"},{"Y", "TRY_TO_STOP"},{"Z", "CANCEL_IF_NOT_BEST"},{"a", "TRAILING_STOP_PEG"},{"b", "STRICT_LIMIT"},{"c", "IGNORE_PRICE_VALIDITY_CHECKS"},{"d", "PEG_TO_LIMIT_PRICE"},{"e", "WORK_TO_TARGET_STRATEGY"}]}
, "18" => #{"Name"=>"ExecInst" ,"Type"=>"MULTIPLEVALUESTRING" ,"ValidValues"=>[{"1", "NOT_HELD"},{"2", "WORK"},{"3", "GO_ALONG"},{"4", "OVER_THE_DAY"},{"5", "HELD"},{"6", "PARTICIPATE_DONT_INITIATE"},{"7", "STRICT_SCALE"},{"8", "TRY_TO_SCALE"},{"9", "STAY_ON_BIDSIDE"},{"0", "STAY_ON_OFFERSIDE"},{"A", "NO_CROSS"},{"B", "OK_TO_CROSS"},{"C", "CALL_FIRST"},{"D", "PERCENT_OF_VOLUME"},{"E", "DO_NOT_INCREASE"},{"F", "DO_NOT_REDUCE"},{"G", "ALL_OR_NONE"},{"H", "REINSTATE_ON_SYSTEM_FAILURE"},{"I", "INSTITUTIONS_ONLY"},{"J", "REINSTATE_ON_TRADING_HALT"},{"K", "CANCEL_ON_TRADING_HALT"},{"L", "LAST_PEG"},{"M", "MID_PRICE_PEG"},{"N", "NON_NEGOTIABLE"},{"O", "OPENING_PEG"},{"P", "MARKET_PEG"},{"Q", "CANCEL_ON_SYSTEM_FAILURE"},{"R", "PRIMARY_PEG"},{"S", "SUSPEND"},{"U", "CUSTOMER_DISPLAY_INSTRUCTION"},{"V", "NETTING"},{"W", "PEG_TO_VWAP"},{"X", "TRADE_ALONG"},{"Y", "TRY_TO_STOP"},{"Z", "CANCEL_IF_NOT_BEST"},{"a", "TRAILING_STOP_PEG"},{"b", "STRICT_LIMIT"},{"c", "IGNORE_PRICE_VALIDITY_CHECKS"},{"d", "PEG_TO_LIMIT_PRICE"},{"e", "WORK_TO_TARGET_STRATEGY"}], "TagNum" => "18"}


,
"ExecRefID" => #{"TagNum" => "19" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "19" => #{"Name"=>"ExecRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "19"}


,
"HandlInst" => #{"TagNum" => "21" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "AUTOMATED_EXECUTION_ORDER_PRIVATE_NO_BROKER_INTERVENTION"},{"2", "AUTOMATED_EXECUTION_ORDER_PUBLIC_BROKER_INTERVENTION_OK"},{"3", "MANUAL_ORDER_BEST_EXECUTION"}]}
, "21" => #{"Name"=>"HandlInst" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "AUTOMATED_EXECUTION_ORDER_PRIVATE_NO_BROKER_INTERVENTION"},{"2", "AUTOMATED_EXECUTION_ORDER_PUBLIC_BROKER_INTERVENTION_OK"},{"3", "MANUAL_ORDER_BEST_EXECUTION"}], "TagNum" => "21"}


,
"SecurityIDSource" => #{"TagNum" => "22" ,"Type" => "STRING" ,"ValidValues" =>[{"1", "CUSIP"},{"2", "SEDOL"},{"3", "QUIK"},{"4", "ISIN_NUMBER"},{"5", "RIC_CODE"},{"6", "ISO_CURRENCY_CODE"},{"7", "ISO_COUNTRY_CODE"},{"8", "EXCHANGE_SYMBOL"},{"9", "CONSOLIDATED_TAPE_ASSOCIATION"},{"A", "BLOOMBERG_SYMBOL"},{"B", "WERTPAPIER"},{"C", "DUTCH"},{"D", "VALOREN"},{"E", "SICOVAM"},{"F", "BELGIAN"},{"G", "COMMON"},{"H", "CLEARING_HOUSE"},{"I", "ISDA_FPML_PRODUCT_SPECIFICATION"},{"J", "OPTIONS_PRICE_REPORTING_AUTHORITY"}]}
, "22" => #{"Name"=>"SecurityIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[{"1", "CUSIP"},{"2", "SEDOL"},{"3", "QUIK"},{"4", "ISIN_NUMBER"},{"5", "RIC_CODE"},{"6", "ISO_CURRENCY_CODE"},{"7", "ISO_COUNTRY_CODE"},{"8", "EXCHANGE_SYMBOL"},{"9", "CONSOLIDATED_TAPE_ASSOCIATION"},{"A", "BLOOMBERG_SYMBOL"},{"B", "WERTPAPIER"},{"C", "DUTCH"},{"D", "VALOREN"},{"E", "SICOVAM"},{"F", "BELGIAN"},{"G", "COMMON"},{"H", "CLEARING_HOUSE"},{"I", "ISDA_FPML_PRODUCT_SPECIFICATION"},{"J", "OPTIONS_PRICE_REPORTING_AUTHORITY"}], "TagNum" => "22"}


,
"IOIID" => #{"TagNum" => "23" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "23" => #{"Name"=>"IOIID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "23"}


,
"IOIQltyInd" => #{"TagNum" => "25" ,"Type" => "CHAR" ,"ValidValues" =>[{"L", "LOW"},{"M", "MEDIUM"},{"H", "HIGH"}]}
, "25" => #{"Name"=>"IOIQltyInd" ,"Type"=>"CHAR" ,"ValidValues"=>[{"L", "LOW"},{"M", "MEDIUM"},{"H", "HIGH"}], "TagNum" => "25"}


,
"IOIRefID" => #{"TagNum" => "26" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "26" => #{"Name"=>"IOIRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "26"}


,
"IOIQty" => #{"TagNum" => "27" ,"Type" => "STRING" ,"ValidValues" =>[{"S", "SMALL"},{"M", "MEDIUM"},{"L", "LARGE"}]}
, "27" => #{"Name"=>"IOIQty" ,"Type"=>"STRING" ,"ValidValues"=>[{"S", "SMALL"},{"M", "MEDIUM"},{"L", "LARGE"}], "TagNum" => "27"}


,
"IOITransType" => #{"TagNum" => "28" ,"Type" => "CHAR" ,"ValidValues" =>[{"N", "NEW"},{"C", "CANCEL"},{"R", "REPLACE"}]}
, "28" => #{"Name"=>"IOITransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"N", "NEW"},{"C", "CANCEL"},{"R", "REPLACE"}], "TagNum" => "28"}


,
"LastCapacity" => #{"TagNum" => "29" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "AGENT"},{"2", "CROSS_AS_AGENT"},{"3", "CROSS_AS_PRINCIPAL"},{"4", "PRINCIPAL"}]}
, "29" => #{"Name"=>"LastCapacity" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "AGENT"},{"2", "CROSS_AS_AGENT"},{"3", "CROSS_AS_PRINCIPAL"},{"4", "PRINCIPAL"}], "TagNum" => "29"}


,
"LastMkt" => #{"TagNum" => "30" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "30" => #{"Name"=>"LastMkt" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "30"}


,
"LastPx" => #{"TagNum" => "31" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "31" => #{"Name"=>"LastPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "31"}


,
"LastQty" => #{"TagNum" => "32" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "32" => #{"Name"=>"LastQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "32"}


,
"NoLinesOfText" => #{"TagNum" => "33" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "33" => #{"Name"=>"NoLinesOfText" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "33"}


,
"MsgSeqNum" => #{"TagNum" => "34" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "34" => #{"Name"=>"MsgSeqNum" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "34"}


,
"MsgType" => #{"TagNum" => "35" ,"Type" => "STRING" ,"ValidValues" =>[{"0", "HEARTBEAT"},{"1", "TEST_REQUEST"},{"2", "RESEND_REQUEST"},{"3", "REJECT"},{"4", "SEQUENCE_RESET"},{"5", "LOGOUT"},{"6", "INDICATION_OF_INTEREST"},{"7", "ADVERTISEMENT"},{"8", "EXECUTION_REPORT"},{"9", "ORDER_CANCEL_REJECT"},{"A", "LOGON"},{"B", "NEWS"},{"C", "EMAIL"},{"D", "ORDER_SINGLE"},{"E", "ORDER_LIST"},{"F", "ORDER_CANCEL_REQUEST"},{"G", "ORDER_CANCEL_REPLACE_REQUEST"},{"H", "ORDER_STATUS_REQUEST"},{"J", "ALLOCATION_INSTRUCTION"},{"K", "LIST_CANCEL_REQUEST"},{"L", "LIST_EXECUTE"},{"M", "LIST_STATUS_REQUEST"},{"N", "LIST_STATUS"},{"P", "ALLOCATION_INSTRUCTION_ACK"},{"Q", "DONT_KNOW_TRADE"},{"R", "QUOTE_REQUEST"},{"S", "QUOTE"},{"T", "SETTLEMENT_INSTRUCTIONS"},{"V", "MARKET_DATA_REQUEST"},{"W", "MARKET_DATA_SNAPSHOT_FULL_REFRESH"},{"X", "MARKET_DATA_INCREMENTAL_REFRESH"},{"Y", "MARKET_DATA_REQUEST_REJECT"},{"Z", "QUOTE_CANCEL"},{"a", "QUOTE_STATUS_REQUEST"},{"b", "MASS_QUOTE_ACKNOWLEDGEMENT"},{"c", "SECURITY_DEFINITION_REQUEST"},{"d", "SECURITY_DEFINITION"},{"e", "SECURITY_STATUS_REQUEST"},{"f", "SECURITY_STATUS"},{"g", "TRADING_SESSION_STATUS_REQUEST"},{"h", "TRADING_SESSION_STATUS"},{"i", "MASS_QUOTE"},{"j", "BUSINESS_MESSAGE_REJECT"},{"k", "BID_REQUEST"},{"l", "BID_RESPONSE"},{"m", "LIST_STRIKE_PRICE"},{"n", "XML_MESSAGE"},{"o", "REGISTRATION_INSTRUCTIONS"},{"p", "REGISTRATION_INSTRUCTIONS_RESPONSE"},{"q", "ORDER_MASS_CANCEL_REQUEST"},{"r", "ORDER_MASS_CANCEL_REPORT"},{"s", "NEW_ORDER_s"},{"t", "CROSS_ORDER_CANCEL_REPLACE_REQUEST"},{"u", "CROSS_ORDER_CANCEL_REQUEST"},{"v", "SECURITY_TYPE_REQUEST"},{"w", "SECURITY_TYPES"},{"x", "SECURITY_LIST_REQUEST"},{"y", "SECURITY_LIST"},{"z", "DERIVATIVE_SECURITY_LIST_REQUEST"},{"AA", "DERIVATIVE_SECURITY_LIST"},{"AB", "NEW_ORDER_AB"},{"AC", "MULTILEG_ORDER_CANCEL_REPLACE"},{"AD", "TRADE_CAPTURE_REPORT_REQUEST"},{"AE", "TRADE_CAPTURE_REPORT"},{"AF", "ORDER_MASS_STATUS_REQUEST"},{"AG", "QUOTE_REQUEST_REJECT"},{"AH", "RFQ_REQUEST"},{"AI", "QUOTE_STATUS_REPORT"},{"AJ", "QUOTE_RESPONSE"},{"AK", "CONFIRMATION"},{"AL", "POSITION_MAINTENANCE_REQUEST"},{"AM", "POSITION_MAINTENANCE_REPORT"},{"AN", "REQUEST_FOR_POSITIONS"},{"AO", "REQUEST_FOR_POSITIONS_ACK"},{"AP", "POSITION_REPORT"},{"AQ", "TRADE_CAPTURE_REPORT_REQUEST_ACK"},{"AR", "TRADE_CAPTURE_REPORT_ACK"},{"AS", "ALLOCATION_REPORT"},{"AT", "ALLOCATION_REPORT_ACK"},{"AU", "CONFIRMATION_ACK"},{"AV", "SETTLEMENT_INSTRUCTION_REQUEST"},{"AW", "ASSIGNMENT_REPORT"},{"AX", "COLLATERAL_REQUEST"},{"AY", "COLLATERAL_ASSIGNMENT"},{"AZ", "COLLATERAL_RESPONSE"},{"BA", "COLLATERAL_REPORT"},{"BB", "COLLATERAL_INQUIRY"},{"BC", "NETWORK_BC"},{"BD", "NETWORK_BD"},{"BE", "USER_REQUEST"},{"BF", "USER_RESPONSE"},{"BG", "COLLATERAL_INQUIRY_ACK"},{"BH", "CONFIRMATION_REQUEST"}]}
, "35" => #{"Name"=>"MsgType" ,"Type"=>"STRING" ,"ValidValues"=>[{"0", "HEARTBEAT"},{"1", "TEST_REQUEST"},{"2", "RESEND_REQUEST"},{"3", "REJECT"},{"4", "SEQUENCE_RESET"},{"5", "LOGOUT"},{"6", "INDICATION_OF_INTEREST"},{"7", "ADVERTISEMENT"},{"8", "EXECUTION_REPORT"},{"9", "ORDER_CANCEL_REJECT"},{"A", "LOGON"},{"B", "NEWS"},{"C", "EMAIL"},{"D", "ORDER_SINGLE"},{"E", "ORDER_LIST"},{"F", "ORDER_CANCEL_REQUEST"},{"G", "ORDER_CANCEL_REPLACE_REQUEST"},{"H", "ORDER_STATUS_REQUEST"},{"J", "ALLOCATION_INSTRUCTION"},{"K", "LIST_CANCEL_REQUEST"},{"L", "LIST_EXECUTE"},{"M", "LIST_STATUS_REQUEST"},{"N", "LIST_STATUS"},{"P", "ALLOCATION_INSTRUCTION_ACK"},{"Q", "DONT_KNOW_TRADE"},{"R", "QUOTE_REQUEST"},{"S", "QUOTE"},{"T", "SETTLEMENT_INSTRUCTIONS"},{"V", "MARKET_DATA_REQUEST"},{"W", "MARKET_DATA_SNAPSHOT_FULL_REFRESH"},{"X", "MARKET_DATA_INCREMENTAL_REFRESH"},{"Y", "MARKET_DATA_REQUEST_REJECT"},{"Z", "QUOTE_CANCEL"},{"a", "QUOTE_STATUS_REQUEST"},{"b", "MASS_QUOTE_ACKNOWLEDGEMENT"},{"c", "SECURITY_DEFINITION_REQUEST"},{"d", "SECURITY_DEFINITION"},{"e", "SECURITY_STATUS_REQUEST"},{"f", "SECURITY_STATUS"},{"g", "TRADING_SESSION_STATUS_REQUEST"},{"h", "TRADING_SESSION_STATUS"},{"i", "MASS_QUOTE"},{"j", "BUSINESS_MESSAGE_REJECT"},{"k", "BID_REQUEST"},{"l", "BID_RESPONSE"},{"m", "LIST_STRIKE_PRICE"},{"n", "XML_MESSAGE"},{"o", "REGISTRATION_INSTRUCTIONS"},{"p", "REGISTRATION_INSTRUCTIONS_RESPONSE"},{"q", "ORDER_MASS_CANCEL_REQUEST"},{"r", "ORDER_MASS_CANCEL_REPORT"},{"s", "NEW_ORDER_s"},{"t", "CROSS_ORDER_CANCEL_REPLACE_REQUEST"},{"u", "CROSS_ORDER_CANCEL_REQUEST"},{"v", "SECURITY_TYPE_REQUEST"},{"w", "SECURITY_TYPES"},{"x", "SECURITY_LIST_REQUEST"},{"y", "SECURITY_LIST"},{"z", "DERIVATIVE_SECURITY_LIST_REQUEST"},{"AA", "DERIVATIVE_SECURITY_LIST"},{"AB", "NEW_ORDER_AB"},{"AC", "MULTILEG_ORDER_CANCEL_REPLACE"},{"AD", "TRADE_CAPTURE_REPORT_REQUEST"},{"AE", "TRADE_CAPTURE_REPORT"},{"AF", "ORDER_MASS_STATUS_REQUEST"},{"AG", "QUOTE_REQUEST_REJECT"},{"AH", "RFQ_REQUEST"},{"AI", "QUOTE_STATUS_REPORT"},{"AJ", "QUOTE_RESPONSE"},{"AK", "CONFIRMATION"},{"AL", "POSITION_MAINTENANCE_REQUEST"},{"AM", "POSITION_MAINTENANCE_REPORT"},{"AN", "REQUEST_FOR_POSITIONS"},{"AO", "REQUEST_FOR_POSITIONS_ACK"},{"AP", "POSITION_REPORT"},{"AQ", "TRADE_CAPTURE_REPORT_REQUEST_ACK"},{"AR", "TRADE_CAPTURE_REPORT_ACK"},{"AS", "ALLOCATION_REPORT"},{"AT", "ALLOCATION_REPORT_ACK"},{"AU", "CONFIRMATION_ACK"},{"AV", "SETTLEMENT_INSTRUCTION_REQUEST"},{"AW", "ASSIGNMENT_REPORT"},{"AX", "COLLATERAL_REQUEST"},{"AY", "COLLATERAL_ASSIGNMENT"},{"AZ", "COLLATERAL_RESPONSE"},{"BA", "COLLATERAL_REPORT"},{"BB", "COLLATERAL_INQUIRY"},{"BC", "NETWORK_BC"},{"BD", "NETWORK_BD"},{"BE", "USER_REQUEST"},{"BF", "USER_RESPONSE"},{"BG", "COLLATERAL_INQUIRY_ACK"},{"BH", "CONFIRMATION_REQUEST"}], "TagNum" => "35"}


,
"NewSeqNo" => #{"TagNum" => "36" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "36" => #{"Name"=>"NewSeqNo" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "36"}


,
"OrderID" => #{"TagNum" => "37" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "37" => #{"Name"=>"OrderID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "37"}


,
"OrderQty" => #{"TagNum" => "38" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "38" => #{"Name"=>"OrderQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "38"}


,
"OrdStatus" => #{"TagNum" => "39" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "PARTIALLY_FILLED"},{"2", "FILLED"},{"3", "DONE_FOR_DAY"},{"4", "CANCELED"},{"6", "PENDING_CANCEL"},{"7", "STOPPED"},{"8", "REJECTED"},{"9", "SUSPENDED"},{"A", "PENDING_NEW"},{"B", "CALCULATED"},{"C", "EXPIRED"},{"D", "ACCEPTED_FOR_BIDDING"},{"E", "PENDING_REPLACE"}]}
, "39" => #{"Name"=>"OrdStatus" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "PARTIALLY_FILLED"},{"2", "FILLED"},{"3", "DONE_FOR_DAY"},{"4", "CANCELED"},{"6", "PENDING_CANCEL"},{"7", "STOPPED"},{"8", "REJECTED"},{"9", "SUSPENDED"},{"A", "PENDING_NEW"},{"B", "CALCULATED"},{"C", "EXPIRED"},{"D", "ACCEPTED_FOR_BIDDING"},{"E", "PENDING_REPLACE"}], "TagNum" => "39"}


,
"OrdType" => #{"TagNum" => "40" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "MARKET"},{"2", "LIMIT"},{"3", "STOP"},{"4", "STOP_LIMIT"},{"6", "WITH_OR_WITHOUT"},{"7", "LIMIT_OR_BETTER"},{"8", "LIMIT_WITH_OR_WITHOUT"},{"9", "ON_BASIS"},{"D", "PREVIOUSLY_QUOTED"},{"E", "PREVIOUSLY_INDICATED"},{"G", "FOREX"},{"I", "FUNARI"},{"J", "MARKET_IF_TOUCHED"},{"K", "MARKET_WITH_LEFTOVER_AS_LIMIT"},{"L", "PREVIOUS_FUND_VALUATION_POINT"},{"M", "NEXT_FUND_VALUATION_POINT"},{"P", "PEGGED"}]}
, "40" => #{"Name"=>"OrdType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "MARKET"},{"2", "LIMIT"},{"3", "STOP"},{"4", "STOP_LIMIT"},{"6", "WITH_OR_WITHOUT"},{"7", "LIMIT_OR_BETTER"},{"8", "LIMIT_WITH_OR_WITHOUT"},{"9", "ON_BASIS"},{"D", "PREVIOUSLY_QUOTED"},{"E", "PREVIOUSLY_INDICATED"},{"G", "FOREX"},{"I", "FUNARI"},{"J", "MARKET_IF_TOUCHED"},{"K", "MARKET_WITH_LEFTOVER_AS_LIMIT"},{"L", "PREVIOUS_FUND_VALUATION_POINT"},{"M", "NEXT_FUND_VALUATION_POINT"},{"P", "PEGGED"}], "TagNum" => "40"}


,
"OrigClOrdID" => #{"TagNum" => "41" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "41" => #{"Name"=>"OrigClOrdID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "41"}


,
"OrigTime" => #{"TagNum" => "42" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "42" => #{"Name"=>"OrigTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "42"}


,
"PossDupFlag" => #{"TagNum" => "43" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "43" => #{"Name"=>"PossDupFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "43"}


,
"Price" => #{"TagNum" => "44" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "44" => #{"Name"=>"Price" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "44"}


,
"RefSeqNum" => #{"TagNum" => "45" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "45" => #{"Name"=>"RefSeqNum" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "45"}


,
"SecurityID" => #{"TagNum" => "48" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "48" => #{"Name"=>"SecurityID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "48"}


,
"SenderCompID" => #{"TagNum" => "49" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "49" => #{"Name"=>"SenderCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "49"}


,
"SenderSubID" => #{"TagNum" => "50" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "50" => #{"Name"=>"SenderSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "50"}


,
"SendingTime" => #{"TagNum" => "52" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "52" => #{"Name"=>"SendingTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "52"}


,
"Quantity" => #{"TagNum" => "53" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "53" => #{"Name"=>"Quantity" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "53"}


,
"Side" => #{"TagNum" => "54" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "BUY"},{"2", "SELL"},{"3", "BUY_MINUS"},{"4", "SELL_PLUS"},{"5", "SELL_SHORT"},{"6", "SELL_SHORT_EXEMPT"},{"7", "UNDISCLOSED"},{"8", "CROSS"},{"9", "CROSS_SHORT"},{"A", "CROSS_SHORT_EXEMPT"},{"B", "AS_DEFINED"},{"C", "OPPOSITE"},{"D", "SUBSCRIBE"},{"E", "REDEEM"},{"F", "LEND"},{"G", "BORROW"}]}
, "54" => #{"Name"=>"Side" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "BUY"},{"2", "SELL"},{"3", "BUY_MINUS"},{"4", "SELL_PLUS"},{"5", "SELL_SHORT"},{"6", "SELL_SHORT_EXEMPT"},{"7", "UNDISCLOSED"},{"8", "CROSS"},{"9", "CROSS_SHORT"},{"A", "CROSS_SHORT_EXEMPT"},{"B", "AS_DEFINED"},{"C", "OPPOSITE"},{"D", "SUBSCRIBE"},{"E", "REDEEM"},{"F", "LEND"},{"G", "BORROW"}], "TagNum" => "54"}


,
"Symbol" => #{"TagNum" => "55" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "55" => #{"Name"=>"Symbol" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "55"}


,
"TargetCompID" => #{"TagNum" => "56" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "56" => #{"Name"=>"TargetCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "56"}


,
"TargetSubID" => #{"TagNum" => "57" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "57" => #{"Name"=>"TargetSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "57"}


,
"Text" => #{"TagNum" => "58" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "58" => #{"Name"=>"Text" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "58"}


,
"TimeInForce" => #{"TagNum" => "59" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "DAY"},{"1", "GOOD_TILL_CANCEL"},{"2", "AT_THE_OPENING"},{"3", "IMMEDIATE_OR_CANCEL"},{"4", "FILL_OR_KILL"},{"5", "GOOD_TILL_CROSSING"},{"6", "GOOD_TILL_DATE"},{"7", "AT_THE_CLOSE"}]}
, "59" => #{"Name"=>"TimeInForce" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "DAY"},{"1", "GOOD_TILL_CANCEL"},{"2", "AT_THE_OPENING"},{"3", "IMMEDIATE_OR_CANCEL"},{"4", "FILL_OR_KILL"},{"5", "GOOD_TILL_CROSSING"},{"6", "GOOD_TILL_DATE"},{"7", "AT_THE_CLOSE"}], "TagNum" => "59"}


,
"TransactTime" => #{"TagNum" => "60" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "60" => #{"Name"=>"TransactTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "60"}


,
"Urgency" => #{"TagNum" => "61" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NORMAL"},{"1", "FLASH"},{"2", "BACKGROUND"}]}
, "61" => #{"Name"=>"Urgency" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NORMAL"},{"1", "FLASH"},{"2", "BACKGROUND"}], "TagNum" => "61"}


,
"ValidUntilTime" => #{"TagNum" => "62" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "62" => #{"Name"=>"ValidUntilTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "62"}


,
"SettlType" => #{"TagNum" => "63" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "REGULAR"},{"1", "CASH"},{"2", "NEXT_DAY"},{"3", "T_PLUS_2"},{"4", "T_PLUS_3"},{"5", "T_PLUS_4"},{"6", "FUTURE"},{"7", "WHEN_AND_IF_ISSUED"},{"8", "SELLERS_OPTION"},{"9", "T_PLUS_5"}]}
, "63" => #{"Name"=>"SettlType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "REGULAR"},{"1", "CASH"},{"2", "NEXT_DAY"},{"3", "T_PLUS_2"},{"4", "T_PLUS_3"},{"5", "T_PLUS_4"},{"6", "FUTURE"},{"7", "WHEN_AND_IF_ISSUED"},{"8", "SELLERS_OPTION"},{"9", "T_PLUS_5"}], "TagNum" => "63"}


,
"SettlDate" => #{"TagNum" => "64" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "64" => #{"Name"=>"SettlDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "64"}


,
"SymbolSfx" => #{"TagNum" => "65" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "65" => #{"Name"=>"SymbolSfx" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "65"}


,
"ListID" => #{"TagNum" => "66" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "66" => #{"Name"=>"ListID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "66"}


,
"ListSeqNo" => #{"TagNum" => "67" ,"Type" => "INT" ,"ValidValues" =>[]}
, "67" => #{"Name"=>"ListSeqNo" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "67"}


,
"TotNoOrders" => #{"TagNum" => "68" ,"Type" => "INT" ,"ValidValues" =>[]}
, "68" => #{"Name"=>"TotNoOrders" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "68"}


,
"ListExecInst" => #{"TagNum" => "69" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "69" => #{"Name"=>"ListExecInst" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "69"}


,
"AllocID" => #{"TagNum" => "70" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "70" => #{"Name"=>"AllocID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "70"}


,
"AllocTransType" => #{"TagNum" => "71" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "REPLACE"},{"2", "CANCEL"}]}
, "71" => #{"Name"=>"AllocTransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "REPLACE"},{"2", "CANCEL"}], "TagNum" => "71"}


,
"RefAllocID" => #{"TagNum" => "72" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "72" => #{"Name"=>"RefAllocID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "72"}


,
"NoOrders" => #{"TagNum" => "73" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "73" => #{"Name"=>"NoOrders" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "73"}


,
"AvgPxPrecision" => #{"TagNum" => "74" ,"Type" => "INT" ,"ValidValues" =>[]}
, "74" => #{"Name"=>"AvgPxPrecision" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "74"}


,
"TradeDate" => #{"TagNum" => "75" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "75" => #{"Name"=>"TradeDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "75"}


,
"PositionEffect" => #{"TagNum" => "77" ,"Type" => "CHAR" ,"ValidValues" =>[{"O", "OPEN"},{"C", "CLOSE"},{"R", "ROLLED"},{"F", "FIFO"}]}
, "77" => #{"Name"=>"PositionEffect" ,"Type"=>"CHAR" ,"ValidValues"=>[{"O", "OPEN"},{"C", "CLOSE"},{"R", "ROLLED"},{"F", "FIFO"}], "TagNum" => "77"}


,
"NoAllocs" => #{"TagNum" => "78" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "78" => #{"Name"=>"NoAllocs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "78"}


,
"AllocAccount" => #{"TagNum" => "79" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "79" => #{"Name"=>"AllocAccount" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "79"}


,
"AllocQty" => #{"TagNum" => "80" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "80" => #{"Name"=>"AllocQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "80"}


,
"ProcessCode" => #{"TagNum" => "81" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "REGULAR"},{"1", "SOFT_DOLLAR"},{"2", "STEP_IN"},{"3", "STEP_OUT"},{"4", "SOFT_DOLLAR_STEP_IN"},{"5", "SOFT_DOLLAR_STEP_OUT"},{"6", "PLAN_SPONSOR"}]}
, "81" => #{"Name"=>"ProcessCode" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "REGULAR"},{"1", "SOFT_DOLLAR"},{"2", "STEP_IN"},{"3", "STEP_OUT"},{"4", "SOFT_DOLLAR_STEP_IN"},{"5", "SOFT_DOLLAR_STEP_OUT"},{"6", "PLAN_SPONSOR"}], "TagNum" => "81"}


,
"NoRpts" => #{"TagNum" => "82" ,"Type" => "INT" ,"ValidValues" =>[]}
, "82" => #{"Name"=>"NoRpts" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "82"}


,
"RptSeq" => #{"TagNum" => "83" ,"Type" => "INT" ,"ValidValues" =>[]}
, "83" => #{"Name"=>"RptSeq" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "83"}


,
"CxlQty" => #{"TagNum" => "84" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "84" => #{"Name"=>"CxlQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "84"}


,
"NoDlvyInst" => #{"TagNum" => "85" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "85" => #{"Name"=>"NoDlvyInst" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "85"}


,
"AllocStatus" => #{"TagNum" => "87" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ACCEPTED"},{"1", "BLOCK_LEVEL_REJECT"},{"2", "ACCOUNT_LEVEL_REJECT"},{"3", "RECEIVED"},{"4", "INCOMPLETE"},{"5", "REJECTED_BY_INTERMEDIARY"}]}
, "87" => #{"Name"=>"AllocStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ACCEPTED"},{"1", "BLOCK_LEVEL_REJECT"},{"2", "ACCOUNT_LEVEL_REJECT"},{"3", "RECEIVED"},{"4", "INCOMPLETE"},{"5", "REJECTED_BY_INTERMEDIARY"}], "TagNum" => "87"}


,
"AllocRejCode" => #{"TagNum" => "88" ,"Type" => "INT" ,"ValidValues" =>[{"0", "UNKNOWN_ACCOUNT"},{"1", "INCORRECT_QUANTITY"},{"2", "INCORRECT_AVERAGE_PRICE"},{"3", "UNKNOWN_EXECUTING_BROKER_MNEMONIC"},{"4", "COMMISSION_DIFFERENCE"},{"5", "UNKNOWN_ORDERID"},{"6", "UNKNOWN_LISTID"},{"7", "OTHER"},{"8", "INCORRECT_ALLOCATED_QUANTITY"},{"9", "CALCULATION_DIFFERENCE"},{"10", "UNKNOWN_OR_STALE_EXECID"},{"11", "MISMATCHED_DATA_VALUE"},{"12", "UNKNOWN_CLORDID"},{"13", "WAREHOUSE_REQUEST_REJECTED"}]}
, "88" => #{"Name"=>"AllocRejCode" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "UNKNOWN_ACCOUNT"},{"1", "INCORRECT_QUANTITY"},{"2", "INCORRECT_AVERAGE_PRICE"},{"3", "UNKNOWN_EXECUTING_BROKER_MNEMONIC"},{"4", "COMMISSION_DIFFERENCE"},{"5", "UNKNOWN_ORDERID"},{"6", "UNKNOWN_LISTID"},{"7", "OTHER"},{"8", "INCORRECT_ALLOCATED_QUANTITY"},{"9", "CALCULATION_DIFFERENCE"},{"10", "UNKNOWN_OR_STALE_EXECID"},{"11", "MISMATCHED_DATA_VALUE"},{"12", "UNKNOWN_CLORDID"},{"13", "WAREHOUSE_REQUEST_REJECTED"}], "TagNum" => "88"}


,
"Signature" => #{"TagNum" => "89" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "89" => #{"Name"=>"Signature" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "89"}


,
"SecureDataLen" => #{"TagNum" => "90" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "90" => #{"Name"=>"SecureDataLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "90"}


,
"SecureData" => #{"TagNum" => "91" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "91" => #{"Name"=>"SecureData" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "91"}


,
"SignatureLength" => #{"TagNum" => "93" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "93" => #{"Name"=>"SignatureLength" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "93"}


,
"EmailType" => #{"TagNum" => "94" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "REPLY"},{"2", "ADMIN_REPLY"}]}
, "94" => #{"Name"=>"EmailType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "REPLY"},{"2", "ADMIN_REPLY"}], "TagNum" => "94"}


,
"RawDataLength" => #{"TagNum" => "95" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "95" => #{"Name"=>"RawDataLength" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "95"}


,
"RawData" => #{"TagNum" => "96" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "96" => #{"Name"=>"RawData" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "96"}


,
"PossResend" => #{"TagNum" => "97" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "97" => #{"Name"=>"PossResend" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "97"}


,
"EncryptMethod" => #{"TagNum" => "98" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NONE"},{"1", "PKCS"},{"2", "DES"},{"3", "PKCS_DES"},{"4", "PGP_DES"},{"5", "PGP_DES_MD5"},{"6", "PEM_DES_MD5"}]}
, "98" => #{"Name"=>"EncryptMethod" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NONE"},{"1", "PKCS"},{"2", "DES"},{"3", "PKCS_DES"},{"4", "PGP_DES"},{"5", "PGP_DES_MD5"},{"6", "PEM_DES_MD5"}], "TagNum" => "98"}


,
"StopPx" => #{"TagNum" => "99" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "99" => #{"Name"=>"StopPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "99"}


,
"ExDestination" => #{"TagNum" => "100" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "100" => #{"Name"=>"ExDestination" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "100"}


,
"CxlRejReason" => #{"TagNum" => "102" ,"Type" => "INT" ,"ValidValues" =>[{"0", "TOO_LATE_TO_CANCEL"},{"1", "UNKNOWN_ORDER"},{"2", "BROKER"},{"3", "ORDER_ALREADY_IN_PENDING_CANCEL_OR_PENDING_REPLACE_STATUS"},{"4", "UNABLE_TO_PROCESS_ORDER_MASS_CANCEL_REQUEST"},{"5", "ORIGORDMODTIME"},{"6", "DUPLICATE_CLORDID"},{"99", "OTHER"}]}
, "102" => #{"Name"=>"CxlRejReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "TOO_LATE_TO_CANCEL"},{"1", "UNKNOWN_ORDER"},{"2", "BROKER"},{"3", "ORDER_ALREADY_IN_PENDING_CANCEL_OR_PENDING_REPLACE_STATUS"},{"4", "UNABLE_TO_PROCESS_ORDER_MASS_CANCEL_REQUEST"},{"5", "ORIGORDMODTIME"},{"6", "DUPLICATE_CLORDID"},{"99", "OTHER"}], "TagNum" => "102"}


,
"OrdRejReason" => #{"TagNum" => "103" ,"Type" => "INT" ,"ValidValues" =>[{"0", "BROKER"},{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE_CLOSED"},{"3", "ORDER_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "UNKNOWN_ORDER"},{"6", "DUPLICATE_ORDER"},{"7", "DUPLICATE_OF_A_VERBALLY_COMMUNICATED_ORDER"},{"8", "STALE_ORDER"},{"9", "TRADE_ALONG_REQUIRED"},{"10", "INVALID_INVESTOR_ID"},{"11", "UNSUPPORTED_ORDER_CHARACTERISTIC12_SURVEILLENCE_OPTION"},{"13", "INCORRECT_QUANTITY"},{"14", "INCORRECT_ALLOCATED_QUANTITY"},{"15", "UNKNOWN_ACCOUNT"},{"99", "OTHER"}]}
, "103" => #{"Name"=>"OrdRejReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "BROKER"},{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE_CLOSED"},{"3", "ORDER_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "UNKNOWN_ORDER"},{"6", "DUPLICATE_ORDER"},{"7", "DUPLICATE_OF_A_VERBALLY_COMMUNICATED_ORDER"},{"8", "STALE_ORDER"},{"9", "TRADE_ALONG_REQUIRED"},{"10", "INVALID_INVESTOR_ID"},{"11", "UNSUPPORTED_ORDER_CHARACTERISTIC12_SURVEILLENCE_OPTION"},{"13", "INCORRECT_QUANTITY"},{"14", "INCORRECT_ALLOCATED_QUANTITY"},{"15", "UNKNOWN_ACCOUNT"},{"99", "OTHER"}], "TagNum" => "103"}


,
"IOIQualifier" => #{"TagNum" => "104" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "ALL_OR_NONE"},{"B", "MARKET_ON_CLOSE"},{"C", "AT_THE_CLOSE"},{"D", "VWAP"},{"I", "IN_TOUCH_WITH"},{"L", "LIMIT"},{"M", "MORE_BEHIND"},{"O", "AT_THE_OPEN"},{"P", "TAKING_A_POSITION"},{"Q", "AT_THE_MARKET"},{"R", "READY_TO_TRADE"},{"S", "PORTFOLIO_SHOWN"},{"T", "THROUGH_THE_DAY"},{"V", "VERSUS"},{"W", "INDICATION"},{"X", "CROSSING_OPPORTUNITY"},{"Y", "AT_THE_MIDPOINT"},{"Z", "PRE_OPEN"}]}
, "104" => #{"Name"=>"IOIQualifier" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "ALL_OR_NONE"},{"B", "MARKET_ON_CLOSE"},{"C", "AT_THE_CLOSE"},{"D", "VWAP"},{"I", "IN_TOUCH_WITH"},{"L", "LIMIT"},{"M", "MORE_BEHIND"},{"O", "AT_THE_OPEN"},{"P", "TAKING_A_POSITION"},{"Q", "AT_THE_MARKET"},{"R", "READY_TO_TRADE"},{"S", "PORTFOLIO_SHOWN"},{"T", "THROUGH_THE_DAY"},{"V", "VERSUS"},{"W", "INDICATION"},{"X", "CROSSING_OPPORTUNITY"},{"Y", "AT_THE_MIDPOINT"},{"Z", "PRE_OPEN"}], "TagNum" => "104"}


,
"Issuer" => #{"TagNum" => "106" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "106" => #{"Name"=>"Issuer" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "106"}


,
"SecurityDesc" => #{"TagNum" => "107" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "107" => #{"Name"=>"SecurityDesc" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "107"}


,
"HeartBtInt" => #{"TagNum" => "108" ,"Type" => "INT" ,"ValidValues" =>[]}
, "108" => #{"Name"=>"HeartBtInt" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "108"}


,
"MinQty" => #{"TagNum" => "110" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "110" => #{"Name"=>"MinQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "110"}


,
"MaxFloor" => #{"TagNum" => "111" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "111" => #{"Name"=>"MaxFloor" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "111"}


,
"TestReqID" => #{"TagNum" => "112" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "112" => #{"Name"=>"TestReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "112"}


,
"ReportToExch" => #{"TagNum" => "113" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "113" => #{"Name"=>"ReportToExch" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "113"}


,
"LocateReqd" => #{"TagNum" => "114" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "114" => #{"Name"=>"LocateReqd" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "114"}


,
"OnBehalfOfCompID" => #{"TagNum" => "115" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "115" => #{"Name"=>"OnBehalfOfCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "115"}


,
"OnBehalfOfSubID" => #{"TagNum" => "116" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "116" => #{"Name"=>"OnBehalfOfSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "116"}


,
"QuoteID" => #{"TagNum" => "117" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "117" => #{"Name"=>"QuoteID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "117"}


,
"NetMoney" => #{"TagNum" => "118" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "118" => #{"Name"=>"NetMoney" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "118"}


,
"SettlCurrAmt" => #{"TagNum" => "119" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "119" => #{"Name"=>"SettlCurrAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "119"}


,
"SettlCurrency" => #{"TagNum" => "120" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "120" => #{"Name"=>"SettlCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "120"}


,
"ForexReq" => #{"TagNum" => "121" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "121" => #{"Name"=>"ForexReq" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "121"}


,
"OrigSendingTime" => #{"TagNum" => "122" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "122" => #{"Name"=>"OrigSendingTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "122"}


,
"GapFillFlag" => #{"TagNum" => "123" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "123" => #{"Name"=>"GapFillFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "123"}


,
"NoExecs" => #{"TagNum" => "124" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "124" => #{"Name"=>"NoExecs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "124"}


,
"ExpireTime" => #{"TagNum" => "126" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "126" => #{"Name"=>"ExpireTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "126"}


,
"DKReason" => #{"TagNum" => "127" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "UNKNOWN_SYMBOL"},{"B", "WRONG_SIDE"},{"C", "QUANTITY_EXCEEDS_ORDER"},{"D", "NO_MATCHING_ORDER"},{"E", "PRICE_EXCEEDS_LIMIT"},{"F", "CALCULATION_DIFFERENCE"},{"Z", "OTHER"}]}
, "127" => #{"Name"=>"DKReason" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "UNKNOWN_SYMBOL"},{"B", "WRONG_SIDE"},{"C", "QUANTITY_EXCEEDS_ORDER"},{"D", "NO_MATCHING_ORDER"},{"E", "PRICE_EXCEEDS_LIMIT"},{"F", "CALCULATION_DIFFERENCE"},{"Z", "OTHER"}], "TagNum" => "127"}


,
"DeliverToCompID" => #{"TagNum" => "128" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "128" => #{"Name"=>"DeliverToCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "128"}


,
"DeliverToSubID" => #{"TagNum" => "129" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "129" => #{"Name"=>"DeliverToSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "129"}


,
"IOINaturalFlag" => #{"TagNum" => "130" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "130" => #{"Name"=>"IOINaturalFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "130"}


,
"QuoteReqID" => #{"TagNum" => "131" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "131" => #{"Name"=>"QuoteReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "131"}


,
"BidPx" => #{"TagNum" => "132" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "132" => #{"Name"=>"BidPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "132"}


,
"OfferPx" => #{"TagNum" => "133" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "133" => #{"Name"=>"OfferPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "133"}


,
"BidSize" => #{"TagNum" => "134" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "134" => #{"Name"=>"BidSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "134"}


,
"OfferSize" => #{"TagNum" => "135" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "135" => #{"Name"=>"OfferSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "135"}


,
"NoMiscFees" => #{"TagNum" => "136" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "136" => #{"Name"=>"NoMiscFees" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "136"}


,
"MiscFeeAmt" => #{"TagNum" => "137" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "137" => #{"Name"=>"MiscFeeAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "137"}


,
"MiscFeeCurr" => #{"TagNum" => "138" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "138" => #{"Name"=>"MiscFeeCurr" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "138"}


,
"MiscFeeType" => #{"TagNum" => "139" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "REGULATORY"},{"2", "TAX"},{"3", "LOCAL_COMMISSION"},{"4", "EXCHANGE_FEES"},{"5", "STAMP"},{"6", "LEVY"},{"7", "OTHER"},{"8", "MARKUP"},{"9", "CONSUMPTION_TAX"},{"10", "PER_TRANSACTION"},{"11", "CONVERSION"},{"12", "AGENT"}]}
, "139" => #{"Name"=>"MiscFeeType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "REGULATORY"},{"2", "TAX"},{"3", "LOCAL_COMMISSION"},{"4", "EXCHANGE_FEES"},{"5", "STAMP"},{"6", "LEVY"},{"7", "OTHER"},{"8", "MARKUP"},{"9", "CONSUMPTION_TAX"},{"10", "PER_TRANSACTION"},{"11", "CONVERSION"},{"12", "AGENT"}], "TagNum" => "139"}


,
"PrevClosePx" => #{"TagNum" => "140" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "140" => #{"Name"=>"PrevClosePx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "140"}


,
"ResetSeqNumFlag" => #{"TagNum" => "141" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "141" => #{"Name"=>"ResetSeqNumFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "141"}


,
"SenderLocationID" => #{"TagNum" => "142" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "142" => #{"Name"=>"SenderLocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "142"}


,
"TargetLocationID" => #{"TagNum" => "143" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "143" => #{"Name"=>"TargetLocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "143"}


,
"OnBehalfOfLocationID" => #{"TagNum" => "144" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "144" => #{"Name"=>"OnBehalfOfLocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "144"}


,
"DeliverToLocationID" => #{"TagNum" => "145" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "145" => #{"Name"=>"DeliverToLocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "145"}


,
"NoRelatedSym" => #{"TagNum" => "146" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "146" => #{"Name"=>"NoRelatedSym" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "146"}


,
"Subject" => #{"TagNum" => "147" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "147" => #{"Name"=>"Subject" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "147"}


,
"Headline" => #{"TagNum" => "148" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "148" => #{"Name"=>"Headline" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "148"}


,
"URLLink" => #{"TagNum" => "149" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "149" => #{"Name"=>"URLLink" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "149"}


,
"ExecType" => #{"TagNum" => "150" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"3", "DONE_FOR_DAY"},{"4", "CANCELED"},{"5", "REPLACE"},{"6", "PENDING_CANCEL"},{"7", "STOPPED"},{"8", "REJECTED"},{"9", "SUSPENDED"},{"A", "PENDING_NEW"},{"B", "CALCULATED"},{"C", "EXPIRED"},{"D", "RESTATED"},{"E", "PENDING_REPLACE"},{"F", "TRADE"},{"G", "TRADE_CORRECT"},{"H", "TRADE_CANCEL"},{"I", "ORDER_STATUS"}]}
, "150" => #{"Name"=>"ExecType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"3", "DONE_FOR_DAY"},{"4", "CANCELED"},{"5", "REPLACE"},{"6", "PENDING_CANCEL"},{"7", "STOPPED"},{"8", "REJECTED"},{"9", "SUSPENDED"},{"A", "PENDING_NEW"},{"B", "CALCULATED"},{"C", "EXPIRED"},{"D", "RESTATED"},{"E", "PENDING_REPLACE"},{"F", "TRADE"},{"G", "TRADE_CORRECT"},{"H", "TRADE_CANCEL"},{"I", "ORDER_STATUS"}], "TagNum" => "150"}


,
"LeavesQty" => #{"TagNum" => "151" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "151" => #{"Name"=>"LeavesQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "151"}


,
"CashOrderQty" => #{"TagNum" => "152" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "152" => #{"Name"=>"CashOrderQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "152"}


,
"AllocAvgPx" => #{"TagNum" => "153" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "153" => #{"Name"=>"AllocAvgPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "153"}


,
"AllocNetMoney" => #{"TagNum" => "154" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "154" => #{"Name"=>"AllocNetMoney" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "154"}


,
"SettlCurrFxRate" => #{"TagNum" => "155" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "155" => #{"Name"=>"SettlCurrFxRate" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "155"}


,
"SettlCurrFxRateCalc" => #{"TagNum" => "156" ,"Type" => "CHAR" ,"ValidValues" =>[{"M", "MULTIPLY"},{"D", "DIVIDE"}]}
, "156" => #{"Name"=>"SettlCurrFxRateCalc" ,"Type"=>"CHAR" ,"ValidValues"=>[{"M", "MULTIPLY"},{"D", "DIVIDE"}], "TagNum" => "156"}


,
"NumDaysInterest" => #{"TagNum" => "157" ,"Type" => "INT" ,"ValidValues" =>[]}
, "157" => #{"Name"=>"NumDaysInterest" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "157"}


,
"AccruedInterestRate" => #{"TagNum" => "158" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "158" => #{"Name"=>"AccruedInterestRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "158"}


,
"AccruedInterestAmt" => #{"TagNum" => "159" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "159" => #{"Name"=>"AccruedInterestAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "159"}


,
"SettlInstMode" => #{"TagNum" => "160" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "STANDING_INSTRUCTIONS_PROVIDED"},{"4", "SPECIFIC_ORDER_FOR_A_SINGLE_ACCOUNT"},{"5", "REQUEST_REJECT"}]}
, "160" => #{"Name"=>"SettlInstMode" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "STANDING_INSTRUCTIONS_PROVIDED"},{"4", "SPECIFIC_ORDER_FOR_A_SINGLE_ACCOUNT"},{"5", "REQUEST_REJECT"}], "TagNum" => "160"}


,
"AllocText" => #{"TagNum" => "161" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "161" => #{"Name"=>"AllocText" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "161"}


,
"SettlInstID" => #{"TagNum" => "162" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "162" => #{"Name"=>"SettlInstID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "162"}


,
"SettlInstTransType" => #{"TagNum" => "163" ,"Type" => "CHAR" ,"ValidValues" =>[{"N", "NEW"},{"C", "CANCEL"},{"R", "REPLACE"},{"T", "RESTATE"}]}
, "163" => #{"Name"=>"SettlInstTransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"N", "NEW"},{"C", "CANCEL"},{"R", "REPLACE"},{"T", "RESTATE"}], "TagNum" => "163"}


,
"EmailThreadID" => #{"TagNum" => "164" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "164" => #{"Name"=>"EmailThreadID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "164"}


,
"SettlInstSource" => #{"TagNum" => "165" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "BROKERS_INSTRUCTIONS"},{"2", "INSTITUTIONS_INSTRUCTIONS"},{"3", "INVESTOR"}]}
, "165" => #{"Name"=>"SettlInstSource" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "BROKERS_INSTRUCTIONS"},{"2", "INSTITUTIONS_INSTRUCTIONS"},{"3", "INVESTOR"}], "TagNum" => "165"}


,
"SecurityType" => #{"TagNum" => "167" ,"Type" => "STRING" ,"ValidValues" =>[{"FUT", "FUTURE"},{"OPT", "OPTION"},{"EUSUPRA", "EURO_SUPRANATIONAL_COUPONS"},{"FAC", "FEDERAL_AGENCY_COUPON"},{"FADN", "FEDERAL_AGENCY_DISCOUNT_NOTE"},{"PEF", "PRIVATE_EXPORT_FUNDING"},{"SUPRA", "USD_SUPRANATIONAL_COUPONS"},{"CORP", "CORPORATE_BOND"},{"CPP", "CORPORATE_PRIVATE_PLACEMENT"},{"CB", "CONVERTIBLE_BOND"},{"DUAL", "DUAL_CURRENCY"},{"EUCORP", "EURO_CORPORATE_BOND"},{"XLINKD", "INDEXED_LINKED"},{"STRUCT", "STRUCTURED_NOTES"},{"YANK", "YANKEE_CORPORATE_BOND"},{"FOR", "FOREIGN_EXCHANGE_CONTRACT"},{"CS", "COMMON_STOCK"},{"PS", "PREFERRED_STOCK"},{"BRADY", "BRADY_BOND"},{"EUSOV", "EURO_SOVEREIGNS"},{"TBOND", "US_TREASURY_BOND"},{"TINT", "INTEREST_STRIP_FROM_ANY_BOND_OR_NOTE"},{"TIPS", "TREASURY_INFLATION_PROTECTED_SECURITIES"},{"TCAL", "PRINCIPAL_STRIP_OF_A_CALLABLE_BOND_OR_NOTE"},{"TPRN", "PRINCIPAL_STRIP_FROM_A_NON_CALLABLE_BOND_OR_NOTE"},{"UST", "US_TREASURY_NOTE_UST"},{"USTB", "US_TREASURY_BILL_USTB"},{"TNOTE", "US_TREASURY_NOTE_TNOTE"},{"TBILL", "US_TREASURY_BILL_TBILL"},{"REPO", "REPURCHASE"},{"FORWARD", "FORWARD"},{"BUYSELL", "BUY_SELLBACK"},{"SECLOAN", "SECURITIES_LOAN"},{"SECPLEDGE", "SECURITIES_PLEDGE"},{"TERM", "TERM_LOAN"},{"RVLV", "REVOLVER_LOAN"},{"RVLVTRM", "REVOLVER_TERM_LOAN"},{"BRIDGE", "BRIDGE_LOAN"},{"LOFC", "LETTER_OF_CREDIT"},{"SWING", "SWING_LINE_FACILITY"},{"DINP", "DEBTOR_IN_POSSESSION"},{"DEFLTED", "DEFAULTED"},{"WITHDRN", "WITHDRAWN"},{"REPLACD", "REPLACED"},{"MATURED", "MATURED"},{"AMENDED", "AMENDED_RESTATED"},{"RETIRED", "RETIRED"},{"BA", "BANKERS_ACCEPTANCE"},{"BN", "BANK_NOTES"},{"BOX", "BILL_OF_EXCHANGES"},{"CD", "CERTIFICATE_OF_DEPOSIT"},{"CL", "CALL_LOANS"},{"CP", "COMMERCIAL_PAPER"},{"DN", "DEPOSIT_NOTES"},{"EUCD", "EURO_CERTIFICATE_OF_DEPOSIT"},{"EUCP", "EURO_COMMERCIAL_PAPER"},{"LQN", "LIQUIDITY_NOTE"},{"MTN", "MEDIUM_TERM_NOTES"},{"ONITE", "OVERNIGHT"},{"PN", "PROMISSORY_NOTE"},{"PZFJ", "PLAZOS_FIJOS"},{"STN", "SHORT_TERM_LOAN_NOTE"},{"TD", "TIME_DEPOSIT"},{"XCN", "EXTENDED_COMM_NOTE"},{"YCD", "YANKEE_CERTIFICATE_OF_DEPOSIT"},{"ABS", "ASSET_BACKED_SECURITIES"},{"CMBS", "CORP_MORTGAGE_BACKED_SECURITIES"},{"CMO", "COLLATERALIZED_MORTGAGE_OBLIGATION"},{"IET", "IOETTE_MORTGAGE"},{"MBS", "MORTGAGE_BACKED_SECURITIES"},{"MIO", "MORTGAGE_INTEREST_ONLY"},{"MPO", "MORTGAGE_PRINCIPAL_ONLY"},{"MPP", "MORTGAGE_PRIVATE_PLACEMENT"},{"MPT", "MISCELLANEOUS_PASS_THROUGH"},{"PFAND", "PFANDBRIEFE"},{"TBA", "TO_BE_ANNOUNCED"},{"AN", "OTHER_ANTICIPATION_NOTES_BAN_GAN_ETC"},{"COFO", "CERTIFICATE_OF_OBLIGATION"},{"COFP", "CERTIFICATE_OF_PARTICIPATION"},{"GO", "GENERAL_OBLIGATION_BONDS"},{"MT", "MANDATORY_TENDER"},{"RAN", "REVENUE_ANTICIPATION_NOTE"},{"REV", "REVENUE_BONDS"},{"SPCLA", "SPECIAL_ASSESSMENT"},{"SPCLO", "SPECIAL_OBLIGATION"},{"SPCLT", "SPECIAL_TAX"},{"TAN", "TAX_ANTICIPATION_NOTE"},{"TAXA", "TAX_ALLOCATION"},{"TECP", "TAX_EXEMPT_COMMERCIAL_PAPER"},{"TRAN", "TAX_REVENUE_ANTICIPATION_NOTE"},{"VRDN", "VARIABLE_RATE_DEMAND_NOTE"},{"WAR", "WARRANT"},{"MF", "MUTUAL_FUND"},{"MLEG", "MULTI_LEG_INSTRUMENT"},{"NONE", "NO_SECURITY_TYPE"}]}
, "167" => #{"Name"=>"SecurityType" ,"Type"=>"STRING" ,"ValidValues"=>[{"FUT", "FUTURE"},{"OPT", "OPTION"},{"EUSUPRA", "EURO_SUPRANATIONAL_COUPONS"},{"FAC", "FEDERAL_AGENCY_COUPON"},{"FADN", "FEDERAL_AGENCY_DISCOUNT_NOTE"},{"PEF", "PRIVATE_EXPORT_FUNDING"},{"SUPRA", "USD_SUPRANATIONAL_COUPONS"},{"CORP", "CORPORATE_BOND"},{"CPP", "CORPORATE_PRIVATE_PLACEMENT"},{"CB", "CONVERTIBLE_BOND"},{"DUAL", "DUAL_CURRENCY"},{"EUCORP", "EURO_CORPORATE_BOND"},{"XLINKD", "INDEXED_LINKED"},{"STRUCT", "STRUCTURED_NOTES"},{"YANK", "YANKEE_CORPORATE_BOND"},{"FOR", "FOREIGN_EXCHANGE_CONTRACT"},{"CS", "COMMON_STOCK"},{"PS", "PREFERRED_STOCK"},{"BRADY", "BRADY_BOND"},{"EUSOV", "EURO_SOVEREIGNS"},{"TBOND", "US_TREASURY_BOND"},{"TINT", "INTEREST_STRIP_FROM_ANY_BOND_OR_NOTE"},{"TIPS", "TREASURY_INFLATION_PROTECTED_SECURITIES"},{"TCAL", "PRINCIPAL_STRIP_OF_A_CALLABLE_BOND_OR_NOTE"},{"TPRN", "PRINCIPAL_STRIP_FROM_A_NON_CALLABLE_BOND_OR_NOTE"},{"UST", "US_TREASURY_NOTE_UST"},{"USTB", "US_TREASURY_BILL_USTB"},{"TNOTE", "US_TREASURY_NOTE_TNOTE"},{"TBILL", "US_TREASURY_BILL_TBILL"},{"REPO", "REPURCHASE"},{"FORWARD", "FORWARD"},{"BUYSELL", "BUY_SELLBACK"},{"SECLOAN", "SECURITIES_LOAN"},{"SECPLEDGE", "SECURITIES_PLEDGE"},{"TERM", "TERM_LOAN"},{"RVLV", "REVOLVER_LOAN"},{"RVLVTRM", "REVOLVER_TERM_LOAN"},{"BRIDGE", "BRIDGE_LOAN"},{"LOFC", "LETTER_OF_CREDIT"},{"SWING", "SWING_LINE_FACILITY"},{"DINP", "DEBTOR_IN_POSSESSION"},{"DEFLTED", "DEFAULTED"},{"WITHDRN", "WITHDRAWN"},{"REPLACD", "REPLACED"},{"MATURED", "MATURED"},{"AMENDED", "AMENDED_RESTATED"},{"RETIRED", "RETIRED"},{"BA", "BANKERS_ACCEPTANCE"},{"BN", "BANK_NOTES"},{"BOX", "BILL_OF_EXCHANGES"},{"CD", "CERTIFICATE_OF_DEPOSIT"},{"CL", "CALL_LOANS"},{"CP", "COMMERCIAL_PAPER"},{"DN", "DEPOSIT_NOTES"},{"EUCD", "EURO_CERTIFICATE_OF_DEPOSIT"},{"EUCP", "EURO_COMMERCIAL_PAPER"},{"LQN", "LIQUIDITY_NOTE"},{"MTN", "MEDIUM_TERM_NOTES"},{"ONITE", "OVERNIGHT"},{"PN", "PROMISSORY_NOTE"},{"PZFJ", "PLAZOS_FIJOS"},{"STN", "SHORT_TERM_LOAN_NOTE"},{"TD", "TIME_DEPOSIT"},{"XCN", "EXTENDED_COMM_NOTE"},{"YCD", "YANKEE_CERTIFICATE_OF_DEPOSIT"},{"ABS", "ASSET_BACKED_SECURITIES"},{"CMBS", "CORP_MORTGAGE_BACKED_SECURITIES"},{"CMO", "COLLATERALIZED_MORTGAGE_OBLIGATION"},{"IET", "IOETTE_MORTGAGE"},{"MBS", "MORTGAGE_BACKED_SECURITIES"},{"MIO", "MORTGAGE_INTEREST_ONLY"},{"MPO", "MORTGAGE_PRINCIPAL_ONLY"},{"MPP", "MORTGAGE_PRIVATE_PLACEMENT"},{"MPT", "MISCELLANEOUS_PASS_THROUGH"},{"PFAND", "PFANDBRIEFE"},{"TBA", "TO_BE_ANNOUNCED"},{"AN", "OTHER_ANTICIPATION_NOTES_BAN_GAN_ETC"},{"COFO", "CERTIFICATE_OF_OBLIGATION"},{"COFP", "CERTIFICATE_OF_PARTICIPATION"},{"GO", "GENERAL_OBLIGATION_BONDS"},{"MT", "MANDATORY_TENDER"},{"RAN", "REVENUE_ANTICIPATION_NOTE"},{"REV", "REVENUE_BONDS"},{"SPCLA", "SPECIAL_ASSESSMENT"},{"SPCLO", "SPECIAL_OBLIGATION"},{"SPCLT", "SPECIAL_TAX"},{"TAN", "TAX_ANTICIPATION_NOTE"},{"TAXA", "TAX_ALLOCATION"},{"TECP", "TAX_EXEMPT_COMMERCIAL_PAPER"},{"TRAN", "TAX_REVENUE_ANTICIPATION_NOTE"},{"VRDN", "VARIABLE_RATE_DEMAND_NOTE"},{"WAR", "WARRANT"},{"MF", "MUTUAL_FUND"},{"MLEG", "MULTI_LEG_INSTRUMENT"},{"NONE", "NO_SECURITY_TYPE"}], "TagNum" => "167"}


,
"EffectiveTime" => #{"TagNum" => "168" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "168" => #{"Name"=>"EffectiveTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "168"}


,
"StandInstDbType" => #{"TagNum" => "169" ,"Type" => "INT" ,"ValidValues" =>[{"0", "OTHER"},{"1", "DTC_SID"},{"2", "THOMSON_ALERT"},{"3", "A_GLOBAL_CUSTODIAN"},{"4", "ACCOUNTNET"}]}
, "169" => #{"Name"=>"StandInstDbType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "OTHER"},{"1", "DTC_SID"},{"2", "THOMSON_ALERT"},{"3", "A_GLOBAL_CUSTODIAN"},{"4", "ACCOUNTNET"}], "TagNum" => "169"}


,
"StandInstDbName" => #{"TagNum" => "170" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "170" => #{"Name"=>"StandInstDbName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "170"}


,
"StandInstDbID" => #{"TagNum" => "171" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "171" => #{"Name"=>"StandInstDbID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "171"}


,
"SettlDeliveryType" => #{"TagNum" => "172" ,"Type" => "INT" ,"ValidValues" =>[{"0", "VERSUS_PAYMENT_DELIVER"},{"1", "FREE_DELIVER"},{"2", "TRI_PARTY"},{"3", "HOLD_IN_CUSTODY"}]}
, "172" => #{"Name"=>"SettlDeliveryType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "VERSUS_PAYMENT_DELIVER"},{"1", "FREE_DELIVER"},{"2", "TRI_PARTY"},{"3", "HOLD_IN_CUSTODY"}], "TagNum" => "172"}


,
"BidSpotRate" => #{"TagNum" => "188" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "188" => #{"Name"=>"BidSpotRate" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "188"}


,
"BidForwardPoints" => #{"TagNum" => "189" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "189" => #{"Name"=>"BidForwardPoints" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "189"}


,
"OfferSpotRate" => #{"TagNum" => "190" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "190" => #{"Name"=>"OfferSpotRate" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "190"}


,
"OfferForwardPoints" => #{"TagNum" => "191" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "191" => #{"Name"=>"OfferForwardPoints" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "191"}


,
"OrderQty2" => #{"TagNum" => "192" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "192" => #{"Name"=>"OrderQty2" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "192"}


,
"SettlDate2" => #{"TagNum" => "193" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "193" => #{"Name"=>"SettlDate2" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "193"}


,
"LastSpotRate" => #{"TagNum" => "194" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "194" => #{"Name"=>"LastSpotRate" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "194"}


,
"LastForwardPoints" => #{"TagNum" => "195" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "195" => #{"Name"=>"LastForwardPoints" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "195"}


,
"AllocLinkID" => #{"TagNum" => "196" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "196" => #{"Name"=>"AllocLinkID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "196"}


,
"AllocLinkType" => #{"TagNum" => "197" ,"Type" => "INT" ,"ValidValues" =>[{"0", "F_X_NETTING"},{"1", "F_X_SWAP"}]}
, "197" => #{"Name"=>"AllocLinkType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "F_X_NETTING"},{"1", "F_X_SWAP"}], "TagNum" => "197"}


,
"SecondaryOrderID" => #{"TagNum" => "198" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "198" => #{"Name"=>"SecondaryOrderID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "198"}


,
"NoIOIQualifiers" => #{"TagNum" => "199" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "199" => #{"Name"=>"NoIOIQualifiers" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "199"}


,
"MaturityMonthYear" => #{"TagNum" => "200" ,"Type" => "MONTHYEAR" ,"ValidValues" =>[]}
, "200" => #{"Name"=>"MaturityMonthYear" ,"Type"=>"MONTHYEAR" ,"ValidValues"=>[], "TagNum" => "200"}


,
"PutOrCall" => #{"TagNum" => "201" ,"Type" => "INT" ,"ValidValues" =>[{"0", "PUT"},{"1", "CALL"}]}
, "201" => #{"Name"=>"PutOrCall" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "PUT"},{"1", "CALL"}], "TagNum" => "201"}


,
"StrikePrice" => #{"TagNum" => "202" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "202" => #{"Name"=>"StrikePrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "202"}


,
"CoveredOrUncovered" => #{"TagNum" => "203" ,"Type" => "INT" ,"ValidValues" =>[{"0", "COVERED"},{"1", "UNCOVERED"}]}
, "203" => #{"Name"=>"CoveredOrUncovered" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "COVERED"},{"1", "UNCOVERED"}], "TagNum" => "203"}


,
"OptAttribute" => #{"TagNum" => "206" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "206" => #{"Name"=>"OptAttribute" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "206"}


,
"SecurityExchange" => #{"TagNum" => "207" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "207" => #{"Name"=>"SecurityExchange" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "207"}


,
"NotifyBrokerOfCredit" => #{"TagNum" => "208" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "208" => #{"Name"=>"NotifyBrokerOfCredit" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "208"}


,
"AllocHandlInst" => #{"TagNum" => "209" ,"Type" => "INT" ,"ValidValues" =>[{"1", "MATCH"},{"2", "FORWARD"},{"3", "FORWARD_AND_MATCH"}]}
, "209" => #{"Name"=>"AllocHandlInst" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "MATCH"},{"2", "FORWARD"},{"3", "FORWARD_AND_MATCH"}], "TagNum" => "209"}


,
"MaxShow" => #{"TagNum" => "210" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "210" => #{"Name"=>"MaxShow" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "210"}


,
"PegOffsetValue" => #{"TagNum" => "211" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "211" => #{"Name"=>"PegOffsetValue" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "211"}


,
"XmlDataLen" => #{"TagNum" => "212" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "212" => #{"Name"=>"XmlDataLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "212"}


,
"XmlData" => #{"TagNum" => "213" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "213" => #{"Name"=>"XmlData" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "213"}


,
"SettlInstRefID" => #{"TagNum" => "214" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "214" => #{"Name"=>"SettlInstRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "214"}


,
"NoRoutingIDs" => #{"TagNum" => "215" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "215" => #{"Name"=>"NoRoutingIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "215"}


,
"RoutingType" => #{"TagNum" => "216" ,"Type" => "INT" ,"ValidValues" =>[{"1", "TARGET_FIRM"},{"2", "TARGET_LIST"},{"3", "BLOCK_FIRM"},{"4", "BLOCK_LIST"}]}
, "216" => #{"Name"=>"RoutingType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "TARGET_FIRM"},{"2", "TARGET_LIST"},{"3", "BLOCK_FIRM"},{"4", "BLOCK_LIST"}], "TagNum" => "216"}


,
"RoutingID" => #{"TagNum" => "217" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "217" => #{"Name"=>"RoutingID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "217"}


,
"Spread" => #{"TagNum" => "218" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "218" => #{"Name"=>"Spread" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "218"}


,
"BenchmarkCurveCurrency" => #{"TagNum" => "220" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "220" => #{"Name"=>"BenchmarkCurveCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "220"}


,
"BenchmarkCurveName" => #{"TagNum" => "221" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "221" => #{"Name"=>"BenchmarkCurveName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "221"}


,
"BenchmarkCurvePoint" => #{"TagNum" => "222" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "222" => #{"Name"=>"BenchmarkCurvePoint" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "222"}


,
"CouponRate" => #{"TagNum" => "223" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "223" => #{"Name"=>"CouponRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "223"}


,
"CouponPaymentDate" => #{"TagNum" => "224" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "224" => #{"Name"=>"CouponPaymentDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "224"}


,
"IssueDate" => #{"TagNum" => "225" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "225" => #{"Name"=>"IssueDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "225"}


,
"RepurchaseTerm" => #{"TagNum" => "226" ,"Type" => "INT" ,"ValidValues" =>[]}
, "226" => #{"Name"=>"RepurchaseTerm" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "226"}


,
"RepurchaseRate" => #{"TagNum" => "227" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "227" => #{"Name"=>"RepurchaseRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "227"}


,
"Factor" => #{"TagNum" => "228" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "228" => #{"Name"=>"Factor" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "228"}


,
"TradeOriginationDate" => #{"TagNum" => "229" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "229" => #{"Name"=>"TradeOriginationDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "229"}


,
"ExDate" => #{"TagNum" => "230" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "230" => #{"Name"=>"ExDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "230"}


,
"ContractMultiplier" => #{"TagNum" => "231" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "231" => #{"Name"=>"ContractMultiplier" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "231"}


,
"NoStipulations" => #{"TagNum" => "232" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "232" => #{"Name"=>"NoStipulations" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "232"}


,
"StipulationType" => #{"TagNum" => "233" ,"Type" => "STRING" ,"ValidValues" =>[{"AMT", "AMT"},{"AUTOREINV", "AUTO_REINVESTMENT_AT_RATE_OR_BETTER"},{"BANKQUAL", "BANK_QUALIFIED"},{"BGNCON", "BARGAIN_CONDITIONS_SEE"},{"COUPON", "COUPON_RANGE"},{"CURRENCY", "ISO_CURRENCY_CODE"},{"CUSTOMDATE", "CUSTOM_START_END_DATE"},{"GEOG", "GEOGRAPHICS_AND_RANGE"},{"HAIRCUT", "VALUATION_DISCOUNT"},{"INSURED", "INSURED"},{"ISSUE", "YEAR_OR_YEAR_MONTH_OF_ISSUE"},{"ISSUER", "ISSUERS_TICKER"},{"ISSUESIZE", "ISSUE_SIZE_RANGE"},{"LOOKBACK", "LOOKBACK_DAYS"},{"LOT", "EXPLICIT_LOT_IDENTIFIER"},{"LOTVAR", "LOT_VARIANCE"},{"MAT", "MATURITY_YEAR_AND_MONTH"},{"MATURITY", "MATURITY_RANGE"},{"MAXSUBS", "MAXIMUM_SUBSTITUTIONS"},{"MINQTY", "MINIMUM_QUANTITY"},{"MININCR", "MINIMUM_INCREMENT"},{"MINDNOM", "MINIMUM_DENOMINATION"},{"PAYFREQ", "PAYMENT_FREQUENCY_CALENDAR"},{"PIECES", "NUMBER_OF_PIECES"},{"PMAX", "POOLS_MAXIMUM"},{"PPM", "POOLS_PER_MILLION"},{"PPL", "POOLS_PER_LOT"},{"PPT", "POOLS_PER_TRADE"},{"PRICE", "PRICE_RANGE"},{"PRICEFREQ", "PRICING_FREQUENCY"},{"PROD", "PRODUCTION_YEAR"},{"PROTECT", "CALL_PROTECTION"},{"PURPOSE", "PURPOSE"},{"PXSOURCE", "BENCHMARK_PRICE_SOURCE"},{"RATING", "RATING_SOURCE_AND_RANGE"},{"REDEMPTION", "TYPE_OF_REDEMPTION_VALUES_ARE_NONCALLABLE_CALLABLE_PREFUNDED_ESCROWEDTOMATURITY_PUTABLE_CONVERTIBLE"},{"RESTRICTED", "RESTRICTED"},{"SECTOR", "MARKET_SECTOR"},{"SECTYPE", "SECURITYTYPE_INCLUDED_OR_EXCLUDED"},{"STRUCT", "STRUCTURE"},{"SUBSFREQ", "SUBSTITUTIONS_FREQUENCY"},{"SUBSLEFT", "SUBSTITUTIONS_LEFT"},{"TEXT", "FREEFORM_TEXT"},{"TRDVAR", "TRADE_VARIANCE"},{"WAC", "WEIGHTED_AVERAGE_COUPONVALUE_IN_PERCENT"},{"WAL", "WEIGHTED_AVERAGE_LIFE_COUPON_VALUE_IN_PERCENT"},{"WALA", "WEIGHTED_AVERAGE_LOAN_AGE_VALUE_IN_MONTHS"},{"WAM", "WEIGHTED_AVERAGE_MATURITY_VALUE_IN_MONTHS"},{"WHOLE", "WHOLE_POOL"},{"YIELD", "YIELD_RANGE"}]}
, "233" => #{"Name"=>"StipulationType" ,"Type"=>"STRING" ,"ValidValues"=>[{"AMT", "AMT"},{"AUTOREINV", "AUTO_REINVESTMENT_AT_RATE_OR_BETTER"},{"BANKQUAL", "BANK_QUALIFIED"},{"BGNCON", "BARGAIN_CONDITIONS_SEE"},{"COUPON", "COUPON_RANGE"},{"CURRENCY", "ISO_CURRENCY_CODE"},{"CUSTOMDATE", "CUSTOM_START_END_DATE"},{"GEOG", "GEOGRAPHICS_AND_RANGE"},{"HAIRCUT", "VALUATION_DISCOUNT"},{"INSURED", "INSURED"},{"ISSUE", "YEAR_OR_YEAR_MONTH_OF_ISSUE"},{"ISSUER", "ISSUERS_TICKER"},{"ISSUESIZE", "ISSUE_SIZE_RANGE"},{"LOOKBACK", "LOOKBACK_DAYS"},{"LOT", "EXPLICIT_LOT_IDENTIFIER"},{"LOTVAR", "LOT_VARIANCE"},{"MAT", "MATURITY_YEAR_AND_MONTH"},{"MATURITY", "MATURITY_RANGE"},{"MAXSUBS", "MAXIMUM_SUBSTITUTIONS"},{"MINQTY", "MINIMUM_QUANTITY"},{"MININCR", "MINIMUM_INCREMENT"},{"MINDNOM", "MINIMUM_DENOMINATION"},{"PAYFREQ", "PAYMENT_FREQUENCY_CALENDAR"},{"PIECES", "NUMBER_OF_PIECES"},{"PMAX", "POOLS_MAXIMUM"},{"PPM", "POOLS_PER_MILLION"},{"PPL", "POOLS_PER_LOT"},{"PPT", "POOLS_PER_TRADE"},{"PRICE", "PRICE_RANGE"},{"PRICEFREQ", "PRICING_FREQUENCY"},{"PROD", "PRODUCTION_YEAR"},{"PROTECT", "CALL_PROTECTION"},{"PURPOSE", "PURPOSE"},{"PXSOURCE", "BENCHMARK_PRICE_SOURCE"},{"RATING", "RATING_SOURCE_AND_RANGE"},{"REDEMPTION", "TYPE_OF_REDEMPTION_VALUES_ARE_NONCALLABLE_CALLABLE_PREFUNDED_ESCROWEDTOMATURITY_PUTABLE_CONVERTIBLE"},{"RESTRICTED", "RESTRICTED"},{"SECTOR", "MARKET_SECTOR"},{"SECTYPE", "SECURITYTYPE_INCLUDED_OR_EXCLUDED"},{"STRUCT", "STRUCTURE"},{"SUBSFREQ", "SUBSTITUTIONS_FREQUENCY"},{"SUBSLEFT", "SUBSTITUTIONS_LEFT"},{"TEXT", "FREEFORM_TEXT"},{"TRDVAR", "TRADE_VARIANCE"},{"WAC", "WEIGHTED_AVERAGE_COUPONVALUE_IN_PERCENT"},{"WAL", "WEIGHTED_AVERAGE_LIFE_COUPON_VALUE_IN_PERCENT"},{"WALA", "WEIGHTED_AVERAGE_LOAN_AGE_VALUE_IN_MONTHS"},{"WAM", "WEIGHTED_AVERAGE_MATURITY_VALUE_IN_MONTHS"},{"WHOLE", "WHOLE_POOL"},{"YIELD", "YIELD_RANGE"}], "TagNum" => "233"}


,
"StipulationValue" => #{"TagNum" => "234" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "234" => #{"Name"=>"StipulationValue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "234"}


,
"YieldType" => #{"TagNum" => "235" ,"Type" => "STRING" ,"ValidValues" =>[{"AFTERTAX", "AFTER_TAX_YIELD"},{"ANNUAL", "ANNUAL_YIELD"},{"ATISSUE", "YIELD_AT_ISSUE"},{"AVGMATURITY", "YIELD_TO_AVERAGE_MATURITY"},{"BOOK", "BOOK_YIELD"},{"CALL", "YIELD_TO_NEXT_CALL"},{"CHANGE", "YIELD_CHANGE_SINCE_CLOSE"},{"CLOSE", "CLOSING_YIELD"},{"COMPOUND", "COMPOUND_YIELD"},{"CURRENT", "CURRENT_YIELD"},{"GROSS", "TRUE_GROSS_YIELD"},{"GOVTEQUIV", "GOVERNMENT_EQUIVALENT_YIELD"},{"INFLATION", "YIELD_WITH_INFLATION_ASSUMPTION"},{"INVERSEFLOATER", "INVERSE_FLOATER_BOND_YIELD"},{"LASTCLOSE", "MOST_RECENT_CLOSING_YIELD"},{"LASTMONTH", "CLOSING_YIELD_MOST_RECENT_MONTH"},{"LASTQUARTER", "CLOSING_YIELD_MOST_RECENT_QUARTER"},{"LASTYEAR", "CLOSING_YIELD_MOST_RECENT_YEAR"},{"LONGAVGLIFE", "YIELD_TO_LONGEST_AVERAGE_LIFE"},{"MARK", "MARK_TO_MARKET_YIELD"},{"MATURITY", "YIELD_TO_MATURITY"},{"NEXTREFUND", "YIELD_TO_NEXT_REFUND"},{"OPENAVG", "OPEN_AVERAGE_YIELD"},{"PUT", "YIELD_TO_NEXT_PUT"},{"PREVCLOSE", "PREVIOUS_CLOSE_YIELD"},{"PROCEEDS", "PROCEEDS_YIELD"},{"SEMIANNUAL", "SEMI_ANNUAL_YIELD"},{"SHORTAVGLIFE", "YIELD_TO_SHORTEST_AVERAGE_LIFE"},{"SIMPLE", "SIMPLE_YIELD"},{"TAXEQUIV", "TAX_EQUIVALENT_YIELD"},{"TENDER", "YIELD_TO_TENDER_DATE"},{"TRUE", "TRUE_YIELD"},{"VALUE1/32", "YIELD_VALUE_OF_1_32"},{"WORST", "YIELD_TO_WORST"}]}
, "235" => #{"Name"=>"YieldType" ,"Type"=>"STRING" ,"ValidValues"=>[{"AFTERTAX", "AFTER_TAX_YIELD"},{"ANNUAL", "ANNUAL_YIELD"},{"ATISSUE", "YIELD_AT_ISSUE"},{"AVGMATURITY", "YIELD_TO_AVERAGE_MATURITY"},{"BOOK", "BOOK_YIELD"},{"CALL", "YIELD_TO_NEXT_CALL"},{"CHANGE", "YIELD_CHANGE_SINCE_CLOSE"},{"CLOSE", "CLOSING_YIELD"},{"COMPOUND", "COMPOUND_YIELD"},{"CURRENT", "CURRENT_YIELD"},{"GROSS", "TRUE_GROSS_YIELD"},{"GOVTEQUIV", "GOVERNMENT_EQUIVALENT_YIELD"},{"INFLATION", "YIELD_WITH_INFLATION_ASSUMPTION"},{"INVERSEFLOATER", "INVERSE_FLOATER_BOND_YIELD"},{"LASTCLOSE", "MOST_RECENT_CLOSING_YIELD"},{"LASTMONTH", "CLOSING_YIELD_MOST_RECENT_MONTH"},{"LASTQUARTER", "CLOSING_YIELD_MOST_RECENT_QUARTER"},{"LASTYEAR", "CLOSING_YIELD_MOST_RECENT_YEAR"},{"LONGAVGLIFE", "YIELD_TO_LONGEST_AVERAGE_LIFE"},{"MARK", "MARK_TO_MARKET_YIELD"},{"MATURITY", "YIELD_TO_MATURITY"},{"NEXTREFUND", "YIELD_TO_NEXT_REFUND"},{"OPENAVG", "OPEN_AVERAGE_YIELD"},{"PUT", "YIELD_TO_NEXT_PUT"},{"PREVCLOSE", "PREVIOUS_CLOSE_YIELD"},{"PROCEEDS", "PROCEEDS_YIELD"},{"SEMIANNUAL", "SEMI_ANNUAL_YIELD"},{"SHORTAVGLIFE", "YIELD_TO_SHORTEST_AVERAGE_LIFE"},{"SIMPLE", "SIMPLE_YIELD"},{"TAXEQUIV", "TAX_EQUIVALENT_YIELD"},{"TENDER", "YIELD_TO_TENDER_DATE"},{"TRUE", "TRUE_YIELD"},{"VALUE1/32", "YIELD_VALUE_OF_1_32"},{"WORST", "YIELD_TO_WORST"}], "TagNum" => "235"}


,
"Yield" => #{"TagNum" => "236" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "236" => #{"Name"=>"Yield" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "236"}


,
"TotalTakedown" => #{"TagNum" => "237" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "237" => #{"Name"=>"TotalTakedown" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "237"}


,
"Concession" => #{"TagNum" => "238" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "238" => #{"Name"=>"Concession" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "238"}


,
"RepoCollateralSecurityType" => #{"TagNum" => "239" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "239" => #{"Name"=>"RepoCollateralSecurityType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "239"}


,
"RedemptionDate" => #{"TagNum" => "240" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "240" => #{"Name"=>"RedemptionDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "240"}


,
"UnderlyingCouponPaymentDate" => #{"TagNum" => "241" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "241" => #{"Name"=>"UnderlyingCouponPaymentDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "241"}


,
"UnderlyingIssueDate" => #{"TagNum" => "242" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "242" => #{"Name"=>"UnderlyingIssueDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "242"}


,
"UnderlyingRepoCollateralSecurityType" => #{"TagNum" => "243" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "243" => #{"Name"=>"UnderlyingRepoCollateralSecurityType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "243"}


,
"UnderlyingRepurchaseTerm" => #{"TagNum" => "244" ,"Type" => "INT" ,"ValidValues" =>[]}
, "244" => #{"Name"=>"UnderlyingRepurchaseTerm" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "244"}


,
"UnderlyingRepurchaseRate" => #{"TagNum" => "245" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "245" => #{"Name"=>"UnderlyingRepurchaseRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "245"}


,
"UnderlyingFactor" => #{"TagNum" => "246" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "246" => #{"Name"=>"UnderlyingFactor" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "246"}


,
"UnderlyingRedemptionDate" => #{"TagNum" => "247" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "247" => #{"Name"=>"UnderlyingRedemptionDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "247"}


,
"LegCouponPaymentDate" => #{"TagNum" => "248" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "248" => #{"Name"=>"LegCouponPaymentDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "248"}


,
"LegIssueDate" => #{"TagNum" => "249" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "249" => #{"Name"=>"LegIssueDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "249"}


,
"LegRepoCollateralSecurityType" => #{"TagNum" => "250" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "250" => #{"Name"=>"LegRepoCollateralSecurityType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "250"}


,
"LegRepurchaseTerm" => #{"TagNum" => "251" ,"Type" => "INT" ,"ValidValues" =>[]}
, "251" => #{"Name"=>"LegRepurchaseTerm" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "251"}


,
"LegRepurchaseRate" => #{"TagNum" => "252" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "252" => #{"Name"=>"LegRepurchaseRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "252"}


,
"LegFactor" => #{"TagNum" => "253" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "253" => #{"Name"=>"LegFactor" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "253"}


,
"LegRedemptionDate" => #{"TagNum" => "254" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "254" => #{"Name"=>"LegRedemptionDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "254"}


,
"CreditRating" => #{"TagNum" => "255" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "255" => #{"Name"=>"CreditRating" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "255"}


,
"UnderlyingCreditRating" => #{"TagNum" => "256" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "256" => #{"Name"=>"UnderlyingCreditRating" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "256"}


,
"LegCreditRating" => #{"TagNum" => "257" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "257" => #{"Name"=>"LegCreditRating" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "257"}


,
"TradedFlatSwitch" => #{"TagNum" => "258" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "258" => #{"Name"=>"TradedFlatSwitch" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "258"}


,
"BasisFeatureDate" => #{"TagNum" => "259" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "259" => #{"Name"=>"BasisFeatureDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "259"}


,
"BasisFeaturePrice" => #{"TagNum" => "260" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "260" => #{"Name"=>"BasisFeaturePrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "260"}


,
"MDReqID" => #{"TagNum" => "262" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "262" => #{"Name"=>"MDReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "262"}


,
"SubscriptionRequestType" => #{"TagNum" => "263" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "SNAPSHOT"},{"1", "SNAPSHOT_PLUS_UPDATES"},{"2", "DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST"}]}
, "263" => #{"Name"=>"SubscriptionRequestType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "SNAPSHOT"},{"1", "SNAPSHOT_PLUS_UPDATES"},{"2", "DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST"}], "TagNum" => "263"}


,
"MarketDepth" => #{"TagNum" => "264" ,"Type" => "INT" ,"ValidValues" =>[]}
, "264" => #{"Name"=>"MarketDepth" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "264"}


,
"MDUpdateType" => #{"TagNum" => "265" ,"Type" => "INT" ,"ValidValues" =>[{"0", "FULL_REFRESH"},{"1", "INCREMENTAL_REFRESH"}]}
, "265" => #{"Name"=>"MDUpdateType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "FULL_REFRESH"},{"1", "INCREMENTAL_REFRESH"}], "TagNum" => "265"}


,
"AggregatedBook" => #{"TagNum" => "266" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "266" => #{"Name"=>"AggregatedBook" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "266"}


,
"NoMDEntryTypes" => #{"TagNum" => "267" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "267" => #{"Name"=>"NoMDEntryTypes" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "267"}


,
"NoMDEntries" => #{"TagNum" => "268" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "268" => #{"Name"=>"NoMDEntries" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "268"}


,
"MDEntryType" => #{"TagNum" => "269" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "BID"},{"1", "OFFER"},{"2", "TRADE"},{"3", "INDEX_VALUE"},{"4", "OPENING_PRICE"},{"5", "CLOSING_PRICE"},{"6", "SETTLEMENT_PRICE"},{"7", "TRADING_SESSION_HIGH_PRICE"},{"8", "TRADING_SESSION_LOW_PRICE"},{"9", "TRADING_SESSION_VWAP_PRICE"},{"A", "IMBALANCE"},{"B", "TRADE_VOLUME"},{"C", "OPEN_INTEREST"}]}
, "269" => #{"Name"=>"MDEntryType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "BID"},{"1", "OFFER"},{"2", "TRADE"},{"3", "INDEX_VALUE"},{"4", "OPENING_PRICE"},{"5", "CLOSING_PRICE"},{"6", "SETTLEMENT_PRICE"},{"7", "TRADING_SESSION_HIGH_PRICE"},{"8", "TRADING_SESSION_LOW_PRICE"},{"9", "TRADING_SESSION_VWAP_PRICE"},{"A", "IMBALANCE"},{"B", "TRADE_VOLUME"},{"C", "OPEN_INTEREST"}], "TagNum" => "269"}


,
"MDEntryPx" => #{"TagNum" => "270" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "270" => #{"Name"=>"MDEntryPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "270"}


,
"MDEntrySize" => #{"TagNum" => "271" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "271" => #{"Name"=>"MDEntrySize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "271"}


,
"MDEntryDate" => #{"TagNum" => "272" ,"Type" => "UTCDATEONLY" ,"ValidValues" =>[]}
, "272" => #{"Name"=>"MDEntryDate" ,"Type"=>"UTCDATEONLY" ,"ValidValues"=>[], "TagNum" => "272"}


,
"MDEntryTime" => #{"TagNum" => "273" ,"Type" => "UTCTIMEONLY" ,"ValidValues" =>[]}
, "273" => #{"Name"=>"MDEntryTime" ,"Type"=>"UTCTIMEONLY" ,"ValidValues"=>[], "TagNum" => "273"}


,
"TickDirection" => #{"TagNum" => "274" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "PLUS_TICK"},{"1", "ZERO_PLUS_TICK"},{"2", "MINUS_TICK"},{"3", "ZERO_MINUS_TICK"}]}
, "274" => #{"Name"=>"TickDirection" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "PLUS_TICK"},{"1", "ZERO_PLUS_TICK"},{"2", "MINUS_TICK"},{"3", "ZERO_MINUS_TICK"}], "TagNum" => "274"}


,
"MDMkt" => #{"TagNum" => "275" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "275" => #{"Name"=>"MDMkt" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "275"}


,
"QuoteCondition" => #{"TagNum" => "276" ,"Type" => "MULTIPLEVALUESTRING" ,"ValidValues" =>[{"A", "OPEN"},{"B", "CLOSED"},{"C", "EXCHANGE_BEST"},{"D", "CONSOLIDATED_BEST"},{"E", "LOCKED"},{"F", "CROSSED"},{"G", "DEPTH"},{"H", "FAST_TRADING"},{"I", "NON_FIRM"}]}
, "276" => #{"Name"=>"QuoteCondition" ,"Type"=>"MULTIPLEVALUESTRING" ,"ValidValues"=>[{"A", "OPEN"},{"B", "CLOSED"},{"C", "EXCHANGE_BEST"},{"D", "CONSOLIDATED_BEST"},{"E", "LOCKED"},{"F", "CROSSED"},{"G", "DEPTH"},{"H", "FAST_TRADING"},{"I", "NON_FIRM"}], "TagNum" => "276"}


,
"TradeCondition" => #{"TagNum" => "277" ,"Type" => "MULTIPLEVALUESTRING" ,"ValidValues" =>[{"A", "CASH"},{"B", "AVERAGE_PRICE_TRADE"},{"C", "CASH_TRADE"},{"D", "NEXT_DAY"},{"E", "OPENING"},{"F", "INTRADAY_TRADE_DETAIL"},{"G", "RULE_127_TRADE"},{"H", "RULE_155_TRADE"},{"I", "SOLD_LAST"},{"J", "NEXT_DAY_TRADE"},{"K", "OPENED"},{"L", "SELLER"},{"M", "SOLD"},{"N", "STOPPED_STOCK"},{"P", "IMBALANCE_MORE_BUYERS"},{"Q", "IMBALANCE_MORE_SELLERS"},{"R", "OPENING_PRICE"}]}
, "277" => #{"Name"=>"TradeCondition" ,"Type"=>"MULTIPLEVALUESTRING" ,"ValidValues"=>[{"A", "CASH"},{"B", "AVERAGE_PRICE_TRADE"},{"C", "CASH_TRADE"},{"D", "NEXT_DAY"},{"E", "OPENING"},{"F", "INTRADAY_TRADE_DETAIL"},{"G", "RULE_127_TRADE"},{"H", "RULE_155_TRADE"},{"I", "SOLD_LAST"},{"J", "NEXT_DAY_TRADE"},{"K", "OPENED"},{"L", "SELLER"},{"M", "SOLD"},{"N", "STOPPED_STOCK"},{"P", "IMBALANCE_MORE_BUYERS"},{"Q", "IMBALANCE_MORE_SELLERS"},{"R", "OPENING_PRICE"}], "TagNum" => "277"}


,
"MDEntryID" => #{"TagNum" => "278" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "278" => #{"Name"=>"MDEntryID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "278"}


,
"MDUpdateAction" => #{"TagNum" => "279" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "CHANGE"},{"2", "DELETE"}]}
, "279" => #{"Name"=>"MDUpdateAction" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "CHANGE"},{"2", "DELETE"}], "TagNum" => "279"}


,
"MDEntryRefID" => #{"TagNum" => "280" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "280" => #{"Name"=>"MDEntryRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "280"}


,
"MDReqRejReason" => #{"TagNum" => "281" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "UNKNOWN_SYMBOL"},{"1", "DUPLICATE_MDREQID"},{"2", "INSUFFICIENT_BANDWIDTH"},{"3", "INSUFFICIENT_PERMISSIONS"},{"4", "UNSUPPORTED_SUBSCRIPTIONREQUESTTYPE"},{"5", "UNSUPPORTED_MARKETDEPTH"},{"6", "UNSUPPORTED_MDUPDATETYPE"},{"7", "UNSUPPORTED_AGGREGATEDBOOK"},{"8", "UNSUPPORTED_MDENTRYTYPE"},{"9", "UNSUPPORTED_TRADINGSESSIONID"},{"A", "UNSUPPORTED_SCOPE"},{"B", "UNSUPPORTED_OPENCLOSESETTLEFLAG"},{"C", "UNSUPPORTED_MDIMPLICITDELETE"}]}
, "281" => #{"Name"=>"MDReqRejReason" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "UNKNOWN_SYMBOL"},{"1", "DUPLICATE_MDREQID"},{"2", "INSUFFICIENT_BANDWIDTH"},{"3", "INSUFFICIENT_PERMISSIONS"},{"4", "UNSUPPORTED_SUBSCRIPTIONREQUESTTYPE"},{"5", "UNSUPPORTED_MARKETDEPTH"},{"6", "UNSUPPORTED_MDUPDATETYPE"},{"7", "UNSUPPORTED_AGGREGATEDBOOK"},{"8", "UNSUPPORTED_MDENTRYTYPE"},{"9", "UNSUPPORTED_TRADINGSESSIONID"},{"A", "UNSUPPORTED_SCOPE"},{"B", "UNSUPPORTED_OPENCLOSESETTLEFLAG"},{"C", "UNSUPPORTED_MDIMPLICITDELETE"}], "TagNum" => "281"}


,
"MDEntryOriginator" => #{"TagNum" => "282" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "282" => #{"Name"=>"MDEntryOriginator" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "282"}


,
"LocationID" => #{"TagNum" => "283" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "283" => #{"Name"=>"LocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "283"}


,
"DeskID" => #{"TagNum" => "284" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "284" => #{"Name"=>"DeskID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "284"}


,
"DeleteReason" => #{"TagNum" => "285" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "CANCELATION"},{"1", "ERROR"}]}
, "285" => #{"Name"=>"DeleteReason" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "CANCELATION"},{"1", "ERROR"}], "TagNum" => "285"}


,
"OpenCloseSettlFlag" => #{"TagNum" => "286" ,"Type" => "MULTIPLEVALUESTRING" ,"ValidValues" =>[{"0", "DAILY_OPEN"},{"1", "SESSION_OPEN"},{"2", "DELIVERY_SETTLEMENT_ENTRY"},{"3", "EXPECTED_ENTRY"},{"4", "ENTRY_FROM_PREVIOUS_BUSINESS_DAY"},{"5", "THEORETICAL_PRICE_VALUE"}]}
, "286" => #{"Name"=>"OpenCloseSettlFlag" ,"Type"=>"MULTIPLEVALUESTRING" ,"ValidValues"=>[{"0", "DAILY_OPEN"},{"1", "SESSION_OPEN"},{"2", "DELIVERY_SETTLEMENT_ENTRY"},{"3", "EXPECTED_ENTRY"},{"4", "ENTRY_FROM_PREVIOUS_BUSINESS_DAY"},{"5", "THEORETICAL_PRICE_VALUE"}], "TagNum" => "286"}


,
"SellerDays" => #{"TagNum" => "287" ,"Type" => "INT" ,"ValidValues" =>[]}
, "287" => #{"Name"=>"SellerDays" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "287"}


,
"MDEntryBuyer" => #{"TagNum" => "288" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "288" => #{"Name"=>"MDEntryBuyer" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "288"}


,
"MDEntrySeller" => #{"TagNum" => "289" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "289" => #{"Name"=>"MDEntrySeller" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "289"}


,
"MDEntryPositionNo" => #{"TagNum" => "290" ,"Type" => "INT" ,"ValidValues" =>[]}
, "290" => #{"Name"=>"MDEntryPositionNo" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "290"}


,
"FinancialStatus" => #{"TagNum" => "291" ,"Type" => "MULTIPLEVALUESTRING" ,"ValidValues" =>[{"1", "BANKRUPT"},{"2", "PENDING_DELISTING"}]}
, "291" => #{"Name"=>"FinancialStatus" ,"Type"=>"MULTIPLEVALUESTRING" ,"ValidValues"=>[{"1", "BANKRUPT"},{"2", "PENDING_DELISTING"}], "TagNum" => "291"}


,
"CorporateAction" => #{"TagNum" => "292" ,"Type" => "MULTIPLEVALUESTRING" ,"ValidValues" =>[{"A", "EX_DIVIDEND"},{"B", "EX_DISTRIBUTION"},{"C", "EX_RIGHTS"},{"D", "NEW"},{"E", "EX_INTEREST"}]}
, "292" => #{"Name"=>"CorporateAction" ,"Type"=>"MULTIPLEVALUESTRING" ,"ValidValues"=>[{"A", "EX_DIVIDEND"},{"B", "EX_DISTRIBUTION"},{"C", "EX_RIGHTS"},{"D", "NEW"},{"E", "EX_INTEREST"}], "TagNum" => "292"}


,
"DefBidSize" => #{"TagNum" => "293" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "293" => #{"Name"=>"DefBidSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "293"}


,
"DefOfferSize" => #{"TagNum" => "294" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "294" => #{"Name"=>"DefOfferSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "294"}


,
"NoQuoteEntries" => #{"TagNum" => "295" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "295" => #{"Name"=>"NoQuoteEntries" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "295"}


,
"NoQuoteSets" => #{"TagNum" => "296" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "296" => #{"Name"=>"NoQuoteSets" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "296"}


,
"QuoteStatus" => #{"TagNum" => "297" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ACCEPTED"},{"1", "CANCELED_FOR_SYMBOL"},{"2", "CANCELED_FOR_SECURITY_TYPE"},{"3", "CANCELED_FOR_UNDERLYING"},{"4", "CANCELED_ALL"},{"5", "REJECTED"},{"6", "REMOVED_FROM_MARKET"},{"7", "EXPIRED"},{"8", "QUERY"},{"9", "QUOTE_NOT_FOUND"},{"10", "PENDING"},{"11", "PASS"},{"12", "LOCKED_MARKET_WARNING"},{"13", "CROSS_MARKET_WARNING"},{"14", "CANCELED_DUE_TO_LOCK_MARKET"},{"15", "CANCELED_DUE_TO_CROSS_MARKET"}]}
, "297" => #{"Name"=>"QuoteStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ACCEPTED"},{"1", "CANCELED_FOR_SYMBOL"},{"2", "CANCELED_FOR_SECURITY_TYPE"},{"3", "CANCELED_FOR_UNDERLYING"},{"4", "CANCELED_ALL"},{"5", "REJECTED"},{"6", "REMOVED_FROM_MARKET"},{"7", "EXPIRED"},{"8", "QUERY"},{"9", "QUOTE_NOT_FOUND"},{"10", "PENDING"},{"11", "PASS"},{"12", "LOCKED_MARKET_WARNING"},{"13", "CROSS_MARKET_WARNING"},{"14", "CANCELED_DUE_TO_LOCK_MARKET"},{"15", "CANCELED_DUE_TO_CROSS_MARKET"}], "TagNum" => "297"}


,
"QuoteCancelType" => #{"TagNum" => "298" ,"Type" => "INT" ,"ValidValues" =>[{"1", "CANCEL_FOR_SYMBOL"},{"2", "CANCEL_FOR_SECURITY_TYPE"},{"3", "CANCEL_FOR_UNDERLYING_SYMBOL"},{"4", "CANCEL_ALL_QUOTES"}]}
, "298" => #{"Name"=>"QuoteCancelType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "CANCEL_FOR_SYMBOL"},{"2", "CANCEL_FOR_SECURITY_TYPE"},{"3", "CANCEL_FOR_UNDERLYING_SYMBOL"},{"4", "CANCEL_ALL_QUOTES"}], "TagNum" => "298"}


,
"QuoteEntryID" => #{"TagNum" => "299" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "299" => #{"Name"=>"QuoteEntryID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "299"}


,
"QuoteRejectReason" => #{"TagNum" => "300" ,"Type" => "INT" ,"ValidValues" =>[{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE"},{"3", "QUOTE_REQUEST_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "UNKNOWN_QUOTE"},{"6", "DUPLICATE_QUOTE"},{"7", "INVALID_BID_ASK_SPREAD"},{"8", "INVALID_PRICE"},{"9", "NOT_AUTHORIZED_TO_QUOTE_SECURITY"},{"99", "OTHER"}]}
, "300" => #{"Name"=>"QuoteRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE"},{"3", "QUOTE_REQUEST_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "UNKNOWN_QUOTE"},{"6", "DUPLICATE_QUOTE"},{"7", "INVALID_BID_ASK_SPREAD"},{"8", "INVALID_PRICE"},{"9", "NOT_AUTHORIZED_TO_QUOTE_SECURITY"},{"99", "OTHER"}], "TagNum" => "300"}


,
"QuoteResponseLevel" => #{"TagNum" => "301" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NO_ACKNOWLEDGEMENT"},{"1", "ACKNOWLEDGE_ONLY_NEGATIVE_OR_ERRONEOUS_QUOTES"},{"2", "ACKNOWLEDGE_EACH_QUOTE_MESSAGES"}]}
, "301" => #{"Name"=>"QuoteResponseLevel" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NO_ACKNOWLEDGEMENT"},{"1", "ACKNOWLEDGE_ONLY_NEGATIVE_OR_ERRONEOUS_QUOTES"},{"2", "ACKNOWLEDGE_EACH_QUOTE_MESSAGES"}], "TagNum" => "301"}


,
"QuoteSetID" => #{"TagNum" => "302" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "302" => #{"Name"=>"QuoteSetID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "302"}


,
"QuoteRequestType" => #{"TagNum" => "303" ,"Type" => "INT" ,"ValidValues" =>[{"1", "MANUAL"},{"2", "AUTOMATIC"}]}
, "303" => #{"Name"=>"QuoteRequestType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "MANUAL"},{"2", "AUTOMATIC"}], "TagNum" => "303"}


,
"TotNoQuoteEntries" => #{"TagNum" => "304" ,"Type" => "INT" ,"ValidValues" =>[]}
, "304" => #{"Name"=>"TotNoQuoteEntries" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "304"}


,
"UnderlyingSecurityIDSource" => #{"TagNum" => "305" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "305" => #{"Name"=>"UnderlyingSecurityIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "305"}


,
"UnderlyingIssuer" => #{"TagNum" => "306" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "306" => #{"Name"=>"UnderlyingIssuer" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "306"}


,
"UnderlyingSecurityDesc" => #{"TagNum" => "307" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "307" => #{"Name"=>"UnderlyingSecurityDesc" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "307"}


,
"UnderlyingSecurityExchange" => #{"TagNum" => "308" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "308" => #{"Name"=>"UnderlyingSecurityExchange" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "308"}


,
"UnderlyingSecurityID" => #{"TagNum" => "309" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "309" => #{"Name"=>"UnderlyingSecurityID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "309"}


,
"UnderlyingSecurityType" => #{"TagNum" => "310" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "310" => #{"Name"=>"UnderlyingSecurityType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "310"}


,
"UnderlyingSymbol" => #{"TagNum" => "311" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "311" => #{"Name"=>"UnderlyingSymbol" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "311"}


,
"UnderlyingSymbolSfx" => #{"TagNum" => "312" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "312" => #{"Name"=>"UnderlyingSymbolSfx" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "312"}


,
"UnderlyingMaturityMonthYear" => #{"TagNum" => "313" ,"Type" => "MONTHYEAR" ,"ValidValues" =>[]}
, "313" => #{"Name"=>"UnderlyingMaturityMonthYear" ,"Type"=>"MONTHYEAR" ,"ValidValues"=>[], "TagNum" => "313"}


,
"UnderlyingPutOrCall" => #{"TagNum" => "315" ,"Type" => "INT" ,"ValidValues" =>[]}
, "315" => #{"Name"=>"UnderlyingPutOrCall" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "315"}


,
"UnderlyingStrikePrice" => #{"TagNum" => "316" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "316" => #{"Name"=>"UnderlyingStrikePrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "316"}


,
"UnderlyingOptAttribute" => #{"TagNum" => "317" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "317" => #{"Name"=>"UnderlyingOptAttribute" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "317"}


,
"UnderlyingCurrency" => #{"TagNum" => "318" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "318" => #{"Name"=>"UnderlyingCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "318"}


,
"SecurityReqID" => #{"TagNum" => "320" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "320" => #{"Name"=>"SecurityReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "320"}


,
"SecurityRequestType" => #{"TagNum" => "321" ,"Type" => "INT" ,"ValidValues" =>[{"0", "REQUEST_SECURITY_IDENTITY_AND_SPECIFICATIONS"},{"1", "REQUEST_SECURITY_IDENTITY_FOR_THE_SPECIFICATIONS_PROVIDED"},{"2", "REQUEST_LIST_SECURITY_TYPES"},{"3", "REQUEST_LIST_SECURITIES"}]}
, "321" => #{"Name"=>"SecurityRequestType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "REQUEST_SECURITY_IDENTITY_AND_SPECIFICATIONS"},{"1", "REQUEST_SECURITY_IDENTITY_FOR_THE_SPECIFICATIONS_PROVIDED"},{"2", "REQUEST_LIST_SECURITY_TYPES"},{"3", "REQUEST_LIST_SECURITIES"}], "TagNum" => "321"}


,
"SecurityResponseID" => #{"TagNum" => "322" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "322" => #{"Name"=>"SecurityResponseID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "322"}


,
"SecurityResponseType" => #{"TagNum" => "323" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ACCEPT_SECURITY_PROPOSAL_AS_IS"},{"2", "ACCEPT_SECURITY_PROPOSAL_WITH_REVISIONS_AS_INDICATED_IN_THE_MESSAGE"},{"5", "REJECT_SECURITY_PROPOSAL"},{"6", "CAN_NOT_MATCH_SELECTION_CRITERIA"}]}
, "323" => #{"Name"=>"SecurityResponseType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ACCEPT_SECURITY_PROPOSAL_AS_IS"},{"2", "ACCEPT_SECURITY_PROPOSAL_WITH_REVISIONS_AS_INDICATED_IN_THE_MESSAGE"},{"5", "REJECT_SECURITY_PROPOSAL"},{"6", "CAN_NOT_MATCH_SELECTION_CRITERIA"}], "TagNum" => "323"}


,
"SecurityStatusReqID" => #{"TagNum" => "324" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "324" => #{"Name"=>"SecurityStatusReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "324"}


,
"UnsolicitedIndicator" => #{"TagNum" => "325" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "325" => #{"Name"=>"UnsolicitedIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "325"}


,
"SecurityTradingStatus" => #{"TagNum" => "326" ,"Type" => "INT" ,"ValidValues" =>[{"1", "OPENING_DELAY"},{"2", "TRADING_HALT"},{"3", "RESUME"},{"4", "NO_OPEN_NO_RESUME"},{"5", "PRICE_INDICATION"},{"6", "TRADING_RANGE_INDICATION"},{"7", "MARKET_IMBALANCE_BUY"},{"8", "MARKET_IMBALANCE_SELL"},{"9", "MARKET_ON_CLOSE_IMBALANCE_BUY"},{"10", "MARKET_ON_CLOSE_IMBALANCE_SELL"},{"12", "NO_MARKET_IMBALANCE"},{"13", "NO_MARKET_ON_CLOSE_IMBALANCE"},{"14", "ITS_PRE_OPENING"},{"15", "NEW_PRICE_INDICATION"},{"16", "TRADE_DISSEMINATION_TIME"},{"17", "READY_TO_TRADE"},{"18", "NOT_AVAILABLE_FOR_TRADING"},{"19", "NOT_TRADED_ON_THIS_MARKET"},{"20", "UNKNOWN_OR_INVALID"},{"21", "PRE_OPEN"},{"22", "OPENING_ROTATION"},{"23", "FAST_MARKET"}]}
, "326" => #{"Name"=>"SecurityTradingStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "OPENING_DELAY"},{"2", "TRADING_HALT"},{"3", "RESUME"},{"4", "NO_OPEN_NO_RESUME"},{"5", "PRICE_INDICATION"},{"6", "TRADING_RANGE_INDICATION"},{"7", "MARKET_IMBALANCE_BUY"},{"8", "MARKET_IMBALANCE_SELL"},{"9", "MARKET_ON_CLOSE_IMBALANCE_BUY"},{"10", "MARKET_ON_CLOSE_IMBALANCE_SELL"},{"12", "NO_MARKET_IMBALANCE"},{"13", "NO_MARKET_ON_CLOSE_IMBALANCE"},{"14", "ITS_PRE_OPENING"},{"15", "NEW_PRICE_INDICATION"},{"16", "TRADE_DISSEMINATION_TIME"},{"17", "READY_TO_TRADE"},{"18", "NOT_AVAILABLE_FOR_TRADING"},{"19", "NOT_TRADED_ON_THIS_MARKET"},{"20", "UNKNOWN_OR_INVALID"},{"21", "PRE_OPEN"},{"22", "OPENING_ROTATION"},{"23", "FAST_MARKET"}], "TagNum" => "326"}


,
"HaltReasonChar" => #{"TagNum" => "327" ,"Type" => "CHAR" ,"ValidValues" =>[{"I", "ORDER_IMBALANCE"},{"X", "EQUIPMENT_CHANGEOVER"},{"P", "NEWS_PENDING"},{"D", "NEWS_DISSEMINATION"},{"E", "ORDER_INFLUX"},{"M", "ADDITIONAL_INFORMATION"}]}
, "327" => #{"Name"=>"HaltReasonChar" ,"Type"=>"CHAR" ,"ValidValues"=>[{"I", "ORDER_IMBALANCE"},{"X", "EQUIPMENT_CHANGEOVER"},{"P", "NEWS_PENDING"},{"D", "NEWS_DISSEMINATION"},{"E", "ORDER_INFLUX"},{"M", "ADDITIONAL_INFORMATION"}], "TagNum" => "327"}


,
"InViewOfCommon" => #{"TagNum" => "328" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "328" => #{"Name"=>"InViewOfCommon" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "328"}


,
"DueToRelated" => #{"TagNum" => "329" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "329" => #{"Name"=>"DueToRelated" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "329"}


,
"BuyVolume" => #{"TagNum" => "330" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "330" => #{"Name"=>"BuyVolume" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "330"}


,
"SellVolume" => #{"TagNum" => "331" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "331" => #{"Name"=>"SellVolume" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "331"}


,
"HighPx" => #{"TagNum" => "332" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "332" => #{"Name"=>"HighPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "332"}


,
"LowPx" => #{"TagNum" => "333" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "333" => #{"Name"=>"LowPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "333"}


,
"Adjustment" => #{"TagNum" => "334" ,"Type" => "INT" ,"ValidValues" =>[{"1", "CANCEL"},{"2", "ERROR"},{"3", "CORRECTION"}]}
, "334" => #{"Name"=>"Adjustment" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "CANCEL"},{"2", "ERROR"},{"3", "CORRECTION"}], "TagNum" => "334"}


,
"TradSesReqID" => #{"TagNum" => "335" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "335" => #{"Name"=>"TradSesReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "335"}


,
"TradingSessionID" => #{"TagNum" => "336" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "336" => #{"Name"=>"TradingSessionID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "336"}


,
"ContraTrader" => #{"TagNum" => "337" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "337" => #{"Name"=>"ContraTrader" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "337"}


,
"TradSesMethod" => #{"TagNum" => "338" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ELECTRONIC"},{"2", "OPEN_OUTCRY"},{"3", "TWO_PARTY"}]}
, "338" => #{"Name"=>"TradSesMethod" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ELECTRONIC"},{"2", "OPEN_OUTCRY"},{"3", "TWO_PARTY"}], "TagNum" => "338"}


,
"TradSesMode" => #{"TagNum" => "339" ,"Type" => "INT" ,"ValidValues" =>[{"1", "TESTING"},{"2", "SIMULATED"},{"3", "PRODUCTION"}]}
, "339" => #{"Name"=>"TradSesMode" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "TESTING"},{"2", "SIMULATED"},{"3", "PRODUCTION"}], "TagNum" => "339"}


,
"TradSesStatus" => #{"TagNum" => "340" ,"Type" => "INT" ,"ValidValues" =>[{"0", "UNKNOWN"},{"1", "HALTED"},{"2", "OPEN"},{"3", "CLOSED"},{"4", "PRE_OPEN"},{"5", "PRE_CLOSE"},{"6", "REQUEST_REJECTED"}]}
, "340" => #{"Name"=>"TradSesStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "UNKNOWN"},{"1", "HALTED"},{"2", "OPEN"},{"3", "CLOSED"},{"4", "PRE_OPEN"},{"5", "PRE_CLOSE"},{"6", "REQUEST_REJECTED"}], "TagNum" => "340"}


,
"TradSesStartTime" => #{"TagNum" => "341" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "341" => #{"Name"=>"TradSesStartTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "341"}


,
"TradSesOpenTime" => #{"TagNum" => "342" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "342" => #{"Name"=>"TradSesOpenTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "342"}


,
"TradSesPreCloseTime" => #{"TagNum" => "343" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "343" => #{"Name"=>"TradSesPreCloseTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "343"}


,
"TradSesCloseTime" => #{"TagNum" => "344" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "344" => #{"Name"=>"TradSesCloseTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "344"}


,
"TradSesEndTime" => #{"TagNum" => "345" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "345" => #{"Name"=>"TradSesEndTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "345"}


,
"NumberOfOrders" => #{"TagNum" => "346" ,"Type" => "INT" ,"ValidValues" =>[]}
, "346" => #{"Name"=>"NumberOfOrders" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "346"}


,
"MessageEncoding" => #{"TagNum" => "347" ,"Type" => "STRING" ,"ValidValues" =>[{"ISO-2022-JP", "JIS"},{"EUC-JP", "EUC"},{"Shift_JIS", "FOR_USING_SJIS"},{"UTF-8", "UNICODE"}]}
, "347" => #{"Name"=>"MessageEncoding" ,"Type"=>"STRING" ,"ValidValues"=>[{"ISO-2022-JP", "JIS"},{"EUC-JP", "EUC"},{"Shift_JIS", "FOR_USING_SJIS"},{"UTF-8", "UNICODE"}], "TagNum" => "347"}


,
"EncodedIssuerLen" => #{"TagNum" => "348" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "348" => #{"Name"=>"EncodedIssuerLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "348"}


,
"EncodedIssuer" => #{"TagNum" => "349" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "349" => #{"Name"=>"EncodedIssuer" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "349"}


,
"EncodedSecurityDescLen" => #{"TagNum" => "350" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "350" => #{"Name"=>"EncodedSecurityDescLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "350"}


,
"EncodedSecurityDesc" => #{"TagNum" => "351" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "351" => #{"Name"=>"EncodedSecurityDesc" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "351"}


,
"EncodedListExecInstLen" => #{"TagNum" => "352" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "352" => #{"Name"=>"EncodedListExecInstLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "352"}


,
"EncodedListExecInst" => #{"TagNum" => "353" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "353" => #{"Name"=>"EncodedListExecInst" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "353"}


,
"EncodedTextLen" => #{"TagNum" => "354" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "354" => #{"Name"=>"EncodedTextLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "354"}


,
"EncodedText" => #{"TagNum" => "355" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "355" => #{"Name"=>"EncodedText" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "355"}


,
"EncodedSubjectLen" => #{"TagNum" => "356" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "356" => #{"Name"=>"EncodedSubjectLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "356"}


,
"EncodedSubject" => #{"TagNum" => "357" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "357" => #{"Name"=>"EncodedSubject" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "357"}


,
"EncodedHeadlineLen" => #{"TagNum" => "358" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "358" => #{"Name"=>"EncodedHeadlineLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "358"}


,
"EncodedHeadline" => #{"TagNum" => "359" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "359" => #{"Name"=>"EncodedHeadline" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "359"}


,
"EncodedAllocTextLen" => #{"TagNum" => "360" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "360" => #{"Name"=>"EncodedAllocTextLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "360"}


,
"EncodedAllocText" => #{"TagNum" => "361" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "361" => #{"Name"=>"EncodedAllocText" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "361"}


,
"EncodedUnderlyingIssuerLen" => #{"TagNum" => "362" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "362" => #{"Name"=>"EncodedUnderlyingIssuerLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "362"}


,
"EncodedUnderlyingIssuer" => #{"TagNum" => "363" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "363" => #{"Name"=>"EncodedUnderlyingIssuer" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "363"}


,
"EncodedUnderlyingSecurityDescLen" => #{"TagNum" => "364" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "364" => #{"Name"=>"EncodedUnderlyingSecurityDescLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "364"}


,
"EncodedUnderlyingSecurityDesc" => #{"TagNum" => "365" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "365" => #{"Name"=>"EncodedUnderlyingSecurityDesc" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "365"}


,
"AllocPrice" => #{"TagNum" => "366" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "366" => #{"Name"=>"AllocPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "366"}


,
"QuoteSetValidUntilTime" => #{"TagNum" => "367" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "367" => #{"Name"=>"QuoteSetValidUntilTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "367"}


,
"QuoteEntryRejectReason" => #{"TagNum" => "368" ,"Type" => "INT" ,"ValidValues" =>[]}
, "368" => #{"Name"=>"QuoteEntryRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "368"}


,
"LastMsgSeqNumProcessed" => #{"TagNum" => "369" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "369" => #{"Name"=>"LastMsgSeqNumProcessed" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "369"}


,
"RefTagID" => #{"TagNum" => "371" ,"Type" => "INT" ,"ValidValues" =>[]}
, "371" => #{"Name"=>"RefTagID" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "371"}


,
"RefMsgType" => #{"TagNum" => "372" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "372" => #{"Name"=>"RefMsgType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "372"}


,
"SessionRejectReason" => #{"TagNum" => "373" ,"Type" => "INT" ,"ValidValues" =>[{"0", "INVALID_TAG_NUMBER"},{"1", "REQUIRED_TAG_MISSING"},{"2", "TAG_NOT_DEFINED_FOR_THIS_MESSAGE_TYPE"},{"3", "UNDEFINED_TAG"},{"4", "TAG_SPECIFIED_WITHOUT_A_VALUE"},{"5", "VALUE_IS_INCORRECT"},{"6", "INCORRECT_DATA_FORMAT_FOR_VALUE"},{"7", "DECRYPTION_PROBLEM"},{"8", "SIGNATURE_PROBLEM"},{"9", "COMPID_PROBLEM"},{"10", "SENDINGTIME_ACCURACY_PROBLEM"},{"11", "INVALID_MSGTYPE"},{"12", "XML_VALIDATION_ERROR"},{"13", "TAG_APPEARS_MORE_THAN_ONCE"},{"14", "TAG_SPECIFIED_OUT_OF_REQUIRED_ORDER"},{"15", "REPEATING_GROUP_FIELDS_OUT_OF_ORDER"},{"16", "INCORRECT_NUMINGROUP_COUNT_FOR_REPEATING_GROUP"},{"17", "NON_DATA_VALUE_INCLUDES_FIELD_DELIMITER"},{"99", "OTHER"}]}
, "373" => #{"Name"=>"SessionRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "INVALID_TAG_NUMBER"},{"1", "REQUIRED_TAG_MISSING"},{"2", "TAG_NOT_DEFINED_FOR_THIS_MESSAGE_TYPE"},{"3", "UNDEFINED_TAG"},{"4", "TAG_SPECIFIED_WITHOUT_A_VALUE"},{"5", "VALUE_IS_INCORRECT"},{"6", "INCORRECT_DATA_FORMAT_FOR_VALUE"},{"7", "DECRYPTION_PROBLEM"},{"8", "SIGNATURE_PROBLEM"},{"9", "COMPID_PROBLEM"},{"10", "SENDINGTIME_ACCURACY_PROBLEM"},{"11", "INVALID_MSGTYPE"},{"12", "XML_VALIDATION_ERROR"},{"13", "TAG_APPEARS_MORE_THAN_ONCE"},{"14", "TAG_SPECIFIED_OUT_OF_REQUIRED_ORDER"},{"15", "REPEATING_GROUP_FIELDS_OUT_OF_ORDER"},{"16", "INCORRECT_NUMINGROUP_COUNT_FOR_REPEATING_GROUP"},{"17", "NON_DATA_VALUE_INCLUDES_FIELD_DELIMITER"},{"99", "OTHER"}], "TagNum" => "373"}


,
"BidRequestTransType" => #{"TagNum" => "374" ,"Type" => "CHAR" ,"ValidValues" =>[{"N", "NEW"},{"C", "CANCEL"}]}
, "374" => #{"Name"=>"BidRequestTransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"N", "NEW"},{"C", "CANCEL"}], "TagNum" => "374"}


,
"ContraBroker" => #{"TagNum" => "375" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "375" => #{"Name"=>"ContraBroker" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "375"}


,
"ComplianceID" => #{"TagNum" => "376" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "376" => #{"Name"=>"ComplianceID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "376"}


,
"SolicitedFlag" => #{"TagNum" => "377" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "377" => #{"Name"=>"SolicitedFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "377"}


,
"ExecRestatementReason" => #{"TagNum" => "378" ,"Type" => "INT" ,"ValidValues" =>[{"0", "GT_CORPORATE_ACTION"},{"1", "GT_RENEWAL"},{"2", "VERBAL_CHANGE"},{"3", "REPRICING_OF_ORDER"},{"4", "BROKER_OPTION"},{"5", "PARTIAL_DECLINE_OF_ORDERQTY"},{"6", "CANCEL_ON_TRADING_HALT"},{"7", "CANCEL_ON_SYSTEM_FAILURE"},{"8", "MARKET"},{"9", "CANCELED_NOT_BEST"},{"10", "WAREHOUSE_RECAP"},{"99", "OTHER"}]}
, "378" => #{"Name"=>"ExecRestatementReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "GT_CORPORATE_ACTION"},{"1", "GT_RENEWAL"},{"2", "VERBAL_CHANGE"},{"3", "REPRICING_OF_ORDER"},{"4", "BROKER_OPTION"},{"5", "PARTIAL_DECLINE_OF_ORDERQTY"},{"6", "CANCEL_ON_TRADING_HALT"},{"7", "CANCEL_ON_SYSTEM_FAILURE"},{"8", "MARKET"},{"9", "CANCELED_NOT_BEST"},{"10", "WAREHOUSE_RECAP"},{"99", "OTHER"}], "TagNum" => "378"}


,
"BusinessRejectRefID" => #{"TagNum" => "379" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "379" => #{"Name"=>"BusinessRejectRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "379"}


,
"BusinessRejectReason" => #{"TagNum" => "380" ,"Type" => "INT" ,"ValidValues" =>[{"0", "OTHER"},{"1", "UNKOWN_ID"},{"2", "UNKNOWN_SECURITY"},{"3", "UNSUPPORTED_MESSAGE_TYPE"},{"4", "APPLICATION_NOT_AVAILABLE"},{"5", "CONDITIONALLY_REQUIRED_FIELD_MISSING"},{"6", "NOT_AUTHORIZED"},{"7", "DELIVERTO_FIRM_NOT_AVAILABLE_AT_THIS_TIME"}]}
, "380" => #{"Name"=>"BusinessRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "OTHER"},{"1", "UNKOWN_ID"},{"2", "UNKNOWN_SECURITY"},{"3", "UNSUPPORTED_MESSAGE_TYPE"},{"4", "APPLICATION_NOT_AVAILABLE"},{"5", "CONDITIONALLY_REQUIRED_FIELD_MISSING"},{"6", "NOT_AUTHORIZED"},{"7", "DELIVERTO_FIRM_NOT_AVAILABLE_AT_THIS_TIME"}], "TagNum" => "380"}


,
"GrossTradeAmt" => #{"TagNum" => "381" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "381" => #{"Name"=>"GrossTradeAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "381"}


,
"NoContraBrokers" => #{"TagNum" => "382" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "382" => #{"Name"=>"NoContraBrokers" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "382"}


,
"MaxMessageSize" => #{"TagNum" => "383" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "383" => #{"Name"=>"MaxMessageSize" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "383"}


,
"NoMsgTypes" => #{"TagNum" => "384" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "384" => #{"Name"=>"NoMsgTypes" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "384"}


,
"MsgDirection" => #{"TagNum" => "385" ,"Type" => "CHAR" ,"ValidValues" =>[{"S", "SEND"},{"R", "RECEIVE"}]}
, "385" => #{"Name"=>"MsgDirection" ,"Type"=>"CHAR" ,"ValidValues"=>[{"S", "SEND"},{"R", "RECEIVE"}], "TagNum" => "385"}


,
"NoTradingSessions" => #{"TagNum" => "386" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "386" => #{"Name"=>"NoTradingSessions" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "386"}


,
"TotalVolumeTraded" => #{"TagNum" => "387" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "387" => #{"Name"=>"TotalVolumeTraded" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "387"}


,
"DiscretionInst" => #{"TagNum" => "388" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "RELATED_TO_DISPLAYED_PRICE"},{"1", "RELATED_TO_MARKET_PRICE"},{"2", "RELATED_TO_PRIMARY_PRICE"},{"3", "RELATED_TO_LOCAL_PRIMARY_PRICE"},{"4", "RELATED_TO_MIDPOINT_PRICE"},{"5", "RELATED_TO_LAST_TRADE_PRICE"},{"6", "RELATED_TO_VWAP"}]}
, "388" => #{"Name"=>"DiscretionInst" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "RELATED_TO_DISPLAYED_PRICE"},{"1", "RELATED_TO_MARKET_PRICE"},{"2", "RELATED_TO_PRIMARY_PRICE"},{"3", "RELATED_TO_LOCAL_PRIMARY_PRICE"},{"4", "RELATED_TO_MIDPOINT_PRICE"},{"5", "RELATED_TO_LAST_TRADE_PRICE"},{"6", "RELATED_TO_VWAP"}], "TagNum" => "388"}


,
"DiscretionOffsetValue" => #{"TagNum" => "389" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "389" => #{"Name"=>"DiscretionOffsetValue" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "389"}


,
"BidID" => #{"TagNum" => "390" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "390" => #{"Name"=>"BidID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "390"}


,
"ClientBidID" => #{"TagNum" => "391" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "391" => #{"Name"=>"ClientBidID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "391"}


,
"ListName" => #{"TagNum" => "392" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "392" => #{"Name"=>"ListName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "392"}


,
"TotNoRelatedSym" => #{"TagNum" => "393" ,"Type" => "INT" ,"ValidValues" =>[]}
, "393" => #{"Name"=>"TotNoRelatedSym" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "393"}


,
"BidType" => #{"TagNum" => "394" ,"Type" => "INT" ,"ValidValues" =>[{"1", "NON_DISCLOSED_STYLE"},{"2", "DISCLOSED_STYLE"},{"3", "NO_BIDDING_PROCESS"}]}
, "394" => #{"Name"=>"BidType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "NON_DISCLOSED_STYLE"},{"2", "DISCLOSED_STYLE"},{"3", "NO_BIDDING_PROCESS"}], "TagNum" => "394"}


,
"NumTickets" => #{"TagNum" => "395" ,"Type" => "INT" ,"ValidValues" =>[]}
, "395" => #{"Name"=>"NumTickets" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "395"}


,
"SideValue1" => #{"TagNum" => "396" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "396" => #{"Name"=>"SideValue1" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "396"}


,
"SideValue2" => #{"TagNum" => "397" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "397" => #{"Name"=>"SideValue2" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "397"}


,
"NoBidDescriptors" => #{"TagNum" => "398" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "398" => #{"Name"=>"NoBidDescriptors" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "398"}


,
"BidDescriptorType" => #{"TagNum" => "399" ,"Type" => "INT" ,"ValidValues" =>[{"1", "SECTOR"},{"2", "COUNTRY"},{"3", "INDEX"}]}
, "399" => #{"Name"=>"BidDescriptorType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "SECTOR"},{"2", "COUNTRY"},{"3", "INDEX"}], "TagNum" => "399"}


,
"BidDescriptor" => #{"TagNum" => "400" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "400" => #{"Name"=>"BidDescriptor" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "400"}


,
"SideValueInd" => #{"TagNum" => "401" ,"Type" => "INT" ,"ValidValues" =>[{"1", "SIDEVALUE1"},{"2", "SIDEVALUE_2"}]}
, "401" => #{"Name"=>"SideValueInd" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "SIDEVALUE1"},{"2", "SIDEVALUE_2"}], "TagNum" => "401"}


,
"LiquidityPctLow" => #{"TagNum" => "402" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "402" => #{"Name"=>"LiquidityPctLow" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "402"}


,
"LiquidityPctHigh" => #{"TagNum" => "403" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "403" => #{"Name"=>"LiquidityPctHigh" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "403"}


,
"LiquidityValue" => #{"TagNum" => "404" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "404" => #{"Name"=>"LiquidityValue" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "404"}


,
"EFPTrackingError" => #{"TagNum" => "405" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "405" => #{"Name"=>"EFPTrackingError" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "405"}


,
"FairValue" => #{"TagNum" => "406" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "406" => #{"Name"=>"FairValue" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "406"}


,
"OutsideIndexPct" => #{"TagNum" => "407" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "407" => #{"Name"=>"OutsideIndexPct" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "407"}


,
"ValueOfFutures" => #{"TagNum" => "408" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "408" => #{"Name"=>"ValueOfFutures" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "408"}


,
"LiquidityIndType" => #{"TagNum" => "409" ,"Type" => "INT" ,"ValidValues" =>[{"1", "5DAY_MOVING_AVERAGE"},{"2", "20_DAY_MOVING_AVERAGE"},{"3", "NORMAL_MARKET_SIZE"},{"4", "OTHER"}]}
, "409" => #{"Name"=>"LiquidityIndType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "5DAY_MOVING_AVERAGE"},{"2", "20_DAY_MOVING_AVERAGE"},{"3", "NORMAL_MARKET_SIZE"},{"4", "OTHER"}], "TagNum" => "409"}


,
"WtAverageLiquidity" => #{"TagNum" => "410" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "410" => #{"Name"=>"WtAverageLiquidity" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "410"}


,
"ExchangeForPhysical" => #{"TagNum" => "411" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "411" => #{"Name"=>"ExchangeForPhysical" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "411"}


,
"OutMainCntryUIndex" => #{"TagNum" => "412" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "412" => #{"Name"=>"OutMainCntryUIndex" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "412"}


,
"CrossPercent" => #{"TagNum" => "413" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "413" => #{"Name"=>"CrossPercent" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "413"}


,
"ProgRptReqs" => #{"TagNum" => "414" ,"Type" => "INT" ,"ValidValues" =>[{"1", "BUYSIDE_EXPLICITLY_REQUESTS_STATUS_USING_STATUSREQUEST"},{"2", "SELLSIDE_PERIODICALLY_SENDS_STATUS_USING_LISTSTATUS_PERIOD_OPTIONALLY_SPECIFIED_IN_PROGRESSPERIOD"},{"3", "REAL_TIME_EXECUTION_REPORTS"}]}
, "414" => #{"Name"=>"ProgRptReqs" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "BUYSIDE_EXPLICITLY_REQUESTS_STATUS_USING_STATUSREQUEST"},{"2", "SELLSIDE_PERIODICALLY_SENDS_STATUS_USING_LISTSTATUS_PERIOD_OPTIONALLY_SPECIFIED_IN_PROGRESSPERIOD"},{"3", "REAL_TIME_EXECUTION_REPORTS"}], "TagNum" => "414"}


,
"ProgPeriodInterval" => #{"TagNum" => "415" ,"Type" => "INT" ,"ValidValues" =>[]}
, "415" => #{"Name"=>"ProgPeriodInterval" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "415"}


,
"IncTaxInd" => #{"TagNum" => "416" ,"Type" => "INT" ,"ValidValues" =>[{"1", "NET"},{"2", "GROSS"}]}
, "416" => #{"Name"=>"IncTaxInd" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "NET"},{"2", "GROSS"}], "TagNum" => "416"}


,
"NumBidders" => #{"TagNum" => "417" ,"Type" => "INT" ,"ValidValues" =>[]}
, "417" => #{"Name"=>"NumBidders" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "417"}


,
"BidTradeType" => #{"TagNum" => "418" ,"Type" => "CHAR" ,"ValidValues" =>[{"R", "RISK_TRADE"},{"G", "VWAP_GUARANTEE"},{"A", "AGENCY"},{"J", "GUARANTEED_CLOSE"}]}
, "418" => #{"Name"=>"BidTradeType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"R", "RISK_TRADE"},{"G", "VWAP_GUARANTEE"},{"A", "AGENCY"},{"J", "GUARANTEED_CLOSE"}], "TagNum" => "418"}


,
"BasisPxType" => #{"TagNum" => "419" ,"Type" => "CHAR" ,"ValidValues" =>[{"2", "CLOSING_PRICE_AT_MORNING_SESSION"},{"3", "CLOSING_PRICE"},{"4", "CURRENT_PRICE"},{"5", "SQ"},{"6", "VWAP_THROUGH_A_DAY"},{"7", "VWAP_THROUGH_A_MORNING_SESSION"},{"8", "VWAP_THROUGH_AN_AFTERNOON_SESSION"},{"9", "VWAP_THROUGH_A_DAY_EXCEPT_YORI"},{"A", "VWAP_THROUGH_A_MORNING_SESSION_EXCEPT_YORI"},{"B", "VWAP_THROUGH_AN_AFTERNOON_SESSION_EXCEPT_YORI"},{"C", "STRIKE"},{"D", "OPEN"},{"Z", "OTHERS"}]}
, "419" => #{"Name"=>"BasisPxType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"2", "CLOSING_PRICE_AT_MORNING_SESSION"},{"3", "CLOSING_PRICE"},{"4", "CURRENT_PRICE"},{"5", "SQ"},{"6", "VWAP_THROUGH_A_DAY"},{"7", "VWAP_THROUGH_A_MORNING_SESSION"},{"8", "VWAP_THROUGH_AN_AFTERNOON_SESSION"},{"9", "VWAP_THROUGH_A_DAY_EXCEPT_YORI"},{"A", "VWAP_THROUGH_A_MORNING_SESSION_EXCEPT_YORI"},{"B", "VWAP_THROUGH_AN_AFTERNOON_SESSION_EXCEPT_YORI"},{"C", "STRIKE"},{"D", "OPEN"},{"Z", "OTHERS"}], "TagNum" => "419"}


,
"NoBidComponents" => #{"TagNum" => "420" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "420" => #{"Name"=>"NoBidComponents" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "420"}


,
"Country" => #{"TagNum" => "421" ,"Type" => "COUNTRY" ,"ValidValues" =>[]}
, "421" => #{"Name"=>"Country" ,"Type"=>"COUNTRY" ,"ValidValues"=>[], "TagNum" => "421"}


,
"TotNoStrikes" => #{"TagNum" => "422" ,"Type" => "INT" ,"ValidValues" =>[]}
, "422" => #{"Name"=>"TotNoStrikes" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "422"}


,
"PriceType" => #{"TagNum" => "423" ,"Type" => "INT" ,"ValidValues" =>[{"1", "PERCENTAGE"},{"2", "PER_UNIT"},{"3", "FIXED_AMOUNT"},{"4", "DISCOUNT_PERCENTAGE_POINTS_BELOW_PAR"},{"5", "PREMIUM_PERCENTAGE_POINTS_OVER_PAR"},{"6", "SPREAD"},{"7", "TED_PRICE"},{"8", "TED_YIELD"},{"9", "YIELD"},{"10", "FIXED_CABINET_TRADE_PRICE"},{"11", "VARIABLE_CABINET_TRADE_PRICE"}]}
, "423" => #{"Name"=>"PriceType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "PERCENTAGE"},{"2", "PER_UNIT"},{"3", "FIXED_AMOUNT"},{"4", "DISCOUNT_PERCENTAGE_POINTS_BELOW_PAR"},{"5", "PREMIUM_PERCENTAGE_POINTS_OVER_PAR"},{"6", "SPREAD"},{"7", "TED_PRICE"},{"8", "TED_YIELD"},{"9", "YIELD"},{"10", "FIXED_CABINET_TRADE_PRICE"},{"11", "VARIABLE_CABINET_TRADE_PRICE"}], "TagNum" => "423"}


,
"DayOrderQty" => #{"TagNum" => "424" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "424" => #{"Name"=>"DayOrderQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "424"}


,
"DayCumQty" => #{"TagNum" => "425" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "425" => #{"Name"=>"DayCumQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "425"}


,
"DayAvgPx" => #{"TagNum" => "426" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "426" => #{"Name"=>"DayAvgPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "426"}


,
"GTBookingInst" => #{"TagNum" => "427" ,"Type" => "INT" ,"ValidValues" =>[{"0", "BOOK_OUT_ALL_TRADES_ON_DAY_OF_EXECUTION"},{"1", "ACCUMULATE_EXECUTIONS_UNTIL_ORDER_IS_FILLED_OR_EXPIRES"},{"2", "ACCUMULATE_UNTIL_VERBALLY_NOTIFIED_OTHERWISE"}]}
, "427" => #{"Name"=>"GTBookingInst" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "BOOK_OUT_ALL_TRADES_ON_DAY_OF_EXECUTION"},{"1", "ACCUMULATE_EXECUTIONS_UNTIL_ORDER_IS_FILLED_OR_EXPIRES"},{"2", "ACCUMULATE_UNTIL_VERBALLY_NOTIFIED_OTHERWISE"}], "TagNum" => "427"}


,
"NoStrikes" => #{"TagNum" => "428" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "428" => #{"Name"=>"NoStrikes" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "428"}


,
"ListStatusType" => #{"TagNum" => "429" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ACK"},{"2", "RESPONSE"},{"3", "TIMED"},{"4", "EXECSTARTED"},{"5", "ALLDONE"},{"6", "ALERT"}]}
, "429" => #{"Name"=>"ListStatusType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ACK"},{"2", "RESPONSE"},{"3", "TIMED"},{"4", "EXECSTARTED"},{"5", "ALLDONE"},{"6", "ALERT"}], "TagNum" => "429"}


,
"NetGrossInd" => #{"TagNum" => "430" ,"Type" => "INT" ,"ValidValues" =>[{"1", "NET"},{"2", "GROSS"}]}
, "430" => #{"Name"=>"NetGrossInd" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "NET"},{"2", "GROSS"}], "TagNum" => "430"}


,
"ListOrderStatus" => #{"TagNum" => "431" ,"Type" => "INT" ,"ValidValues" =>[{"1", "INBIDDINGPROCESS"},{"2", "RECEIVEDFOREXECUTION"},{"3", "EXECUTING"},{"4", "CANCELING"},{"5", "ALERT"},{"6", "ALL_DONE"},{"7", "REJECT"}]}
, "431" => #{"Name"=>"ListOrderStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "INBIDDINGPROCESS"},{"2", "RECEIVEDFOREXECUTION"},{"3", "EXECUTING"},{"4", "CANCELING"},{"5", "ALERT"},{"6", "ALL_DONE"},{"7", "REJECT"}], "TagNum" => "431"}


,
"ExpireDate" => #{"TagNum" => "432" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "432" => #{"Name"=>"ExpireDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "432"}


,
"ListExecInstType" => #{"TagNum" => "433" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "IMMEDIATE"},{"2", "WAIT_FOR_EXECUTE_INSTRUCTION"},{"3", "EXCHANGE_SWITCH_CIV_ORDER_SELL_DRIVEN"},{"4", "EXCHANGE_SWITCH_CIV_ORDER_BUY_DRIVEN_CASH_TOP_UP"},{"5", "EXCHANGE_SWITCH_CIV_ORDER_BUY_DRIVEN_CASH_WITHDRAW"}]}
, "433" => #{"Name"=>"ListExecInstType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "IMMEDIATE"},{"2", "WAIT_FOR_EXECUTE_INSTRUCTION"},{"3", "EXCHANGE_SWITCH_CIV_ORDER_SELL_DRIVEN"},{"4", "EXCHANGE_SWITCH_CIV_ORDER_BUY_DRIVEN_CASH_TOP_UP"},{"5", "EXCHANGE_SWITCH_CIV_ORDER_BUY_DRIVEN_CASH_WITHDRAW"}], "TagNum" => "433"}


,
"CxlRejResponseTo" => #{"TagNum" => "434" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "ORDER_CANCEL_REQUEST"},{"2", "ORDER_CANCEL_REPLACE_REQUEST"}]}
, "434" => #{"Name"=>"CxlRejResponseTo" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "ORDER_CANCEL_REQUEST"},{"2", "ORDER_CANCEL_REPLACE_REQUEST"}], "TagNum" => "434"}


,
"UnderlyingCouponRate" => #{"TagNum" => "435" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "435" => #{"Name"=>"UnderlyingCouponRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "435"}


,
"UnderlyingContractMultiplier" => #{"TagNum" => "436" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "436" => #{"Name"=>"UnderlyingContractMultiplier" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "436"}


,
"ContraTradeQty" => #{"TagNum" => "437" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "437" => #{"Name"=>"ContraTradeQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "437"}


,
"ContraTradeTime" => #{"TagNum" => "438" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "438" => #{"Name"=>"ContraTradeTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "438"}


,
"LiquidityNumSecurities" => #{"TagNum" => "441" ,"Type" => "INT" ,"ValidValues" =>[]}
, "441" => #{"Name"=>"LiquidityNumSecurities" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "441"}


,
"MultiLegReportingType" => #{"TagNum" => "442" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "SINGLE_SECURITY"},{"2", "INDIVIDUAL_LEG_OF_A_MULTI_LEG_SECURITY"},{"3", "MULTI_LEG_SECURITY"}]}
, "442" => #{"Name"=>"MultiLegReportingType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "SINGLE_SECURITY"},{"2", "INDIVIDUAL_LEG_OF_A_MULTI_LEG_SECURITY"},{"3", "MULTI_LEG_SECURITY"}], "TagNum" => "442"}


,
"StrikeTime" => #{"TagNum" => "443" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "443" => #{"Name"=>"StrikeTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "443"}


,
"ListStatusText" => #{"TagNum" => "444" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "444" => #{"Name"=>"ListStatusText" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "444"}


,
"EncodedListStatusTextLen" => #{"TagNum" => "445" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "445" => #{"Name"=>"EncodedListStatusTextLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "445"}


,
"EncodedListStatusText" => #{"TagNum" => "446" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "446" => #{"Name"=>"EncodedListStatusText" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "446"}


,
"PartyIDSource" => #{"TagNum" => "447" ,"Type" => "CHAR" ,"ValidValues" =>[{"B", "BIC"},{"C", "GENERALLY_ACCEPTED_MARKET_PARTICIPANT_IDENTIFIER"},{"D", "PROPRIETARY_CUSTOM_CODE"},{"E", "ISO_COUNTRY_CODE"},{"F", "SETTLEMENT_ENTITY_LOCATION"},{"G", "MIC"},{"H", "CSD_PARTICIPANT_MEMBER_CODE"},{"1", "KOREAN_INVESTOR_ID"},{"2", "TAIWANESE_QUALIFIED_FOREIGN_INVESTOR_ID_QFII"},{"3", "TAIWANESE_TRADING_ACCOUNT"},{"4", "MALAYSIAN_CENTRAL_DEPOSITORY"},{"5", "CHINESE_B_SHARE"},{"6", "UK_NATIONAL_INSURANCE_OR_PENSION_NUMBER"},{"7", "US_SOCIAL_SECURITY_NUMBER"},{"8", "US_EMPLOYER_IDENTIFICATION_NUMBER"},{"9", "AUSTRALIAN_BUSINESS_NUMBER"},{"A", "AUSTRALIAN_TAX_FILE_NUMBER"},{"I", "DIRECTED_BROKER_THREE_CHARACTER_ACRONYM_AS_DEFINED_IN_ISITC_ETC_BEST_PRACTICE_GUIDELINES_DOCUMENT"}]}
, "447" => #{"Name"=>"PartyIDSource" ,"Type"=>"CHAR" ,"ValidValues"=>[{"B", "BIC"},{"C", "GENERALLY_ACCEPTED_MARKET_PARTICIPANT_IDENTIFIER"},{"D", "PROPRIETARY_CUSTOM_CODE"},{"E", "ISO_COUNTRY_CODE"},{"F", "SETTLEMENT_ENTITY_LOCATION"},{"G", "MIC"},{"H", "CSD_PARTICIPANT_MEMBER_CODE"},{"1", "KOREAN_INVESTOR_ID"},{"2", "TAIWANESE_QUALIFIED_FOREIGN_INVESTOR_ID_QFII"},{"3", "TAIWANESE_TRADING_ACCOUNT"},{"4", "MALAYSIAN_CENTRAL_DEPOSITORY"},{"5", "CHINESE_B_SHARE"},{"6", "UK_NATIONAL_INSURANCE_OR_PENSION_NUMBER"},{"7", "US_SOCIAL_SECURITY_NUMBER"},{"8", "US_EMPLOYER_IDENTIFICATION_NUMBER"},{"9", "AUSTRALIAN_BUSINESS_NUMBER"},{"A", "AUSTRALIAN_TAX_FILE_NUMBER"},{"I", "DIRECTED_BROKER_THREE_CHARACTER_ACRONYM_AS_DEFINED_IN_ISITC_ETC_BEST_PRACTICE_GUIDELINES_DOCUMENT"}], "TagNum" => "447"}


,
"PartyID" => #{"TagNum" => "448" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "448" => #{"Name"=>"PartyID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "448"}


,
"NetChgPrevDay" => #{"TagNum" => "451" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "451" => #{"Name"=>"NetChgPrevDay" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "451"}


,
"PartyRole" => #{"TagNum" => "452" ,"Type" => "INT" ,"ValidValues" =>[{"1", "EXECUTING_FIRM"},{"2", "BROKER_OF_CREDIT"},{"3", "CLIENT_ID"},{"4", "CLEARING_FIRM"},{"5", "INVESTOR_ID"},{"6", "INTRODUCING_FIRM"},{"7", "ENTERING_FIRM"},{"8", "LOCATE_LENDING_FIRM"},{"9", "FUND_MANAGER_CLIENT_ID"},{"10", "SETTLEMENT_LOCATION"},{"11", "ORDER_ORIGINATION_TRADER"},{"12", "EXECUTING_TRADER"},{"13", "ORDER_ORIGINATION_FIRM"},{"14", "GIVEUP_CLEARING_FIRM"},{"15", "CORRESPONDANT_CLEARING_FIRM"},{"16", "EXECUTING_SYSTEM"},{"17", "CONTRA_FIRM"},{"18", "CONTRA_CLEARING_FIRM"},{"19", "SPONSORING_FIRM"},{"20", "UNDERLYING_CONTRA_FIRM"},{"21", "CLEARING_ORGANIZATION"},{"22", "EXCHANGE"},{"24", "CUSTOMER_ACCOUNT"},{"25", "CORRESPONDENT_CLEARING_ORGANIZATION"},{"26", "CORRESPONDENT_BROKER"},{"27", "BUYER_SELLER"},{"28", "CUSTODIAN"},{"29", "INTERMEDIARY"},{"30", "AGENT"},{"31", "SUB_CUSTODIAN"},{"32", "BENEFICIARY"},{"33", "INTERESTED_PARTY"},{"34", "REGULATORY_BODY"},{"35", "LIQUIDITY_PROVIDER"},{"36", "ENTERING_TRADER"},{"37", "CONTRA_TRADER"},{"38", "POSITION_ACCOUNT"}]}
, "452" => #{"Name"=>"PartyRole" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "EXECUTING_FIRM"},{"2", "BROKER_OF_CREDIT"},{"3", "CLIENT_ID"},{"4", "CLEARING_FIRM"},{"5", "INVESTOR_ID"},{"6", "INTRODUCING_FIRM"},{"7", "ENTERING_FIRM"},{"8", "LOCATE_LENDING_FIRM"},{"9", "FUND_MANAGER_CLIENT_ID"},{"10", "SETTLEMENT_LOCATION"},{"11", "ORDER_ORIGINATION_TRADER"},{"12", "EXECUTING_TRADER"},{"13", "ORDER_ORIGINATION_FIRM"},{"14", "GIVEUP_CLEARING_FIRM"},{"15", "CORRESPONDANT_CLEARING_FIRM"},{"16", "EXECUTING_SYSTEM"},{"17", "CONTRA_FIRM"},{"18", "CONTRA_CLEARING_FIRM"},{"19", "SPONSORING_FIRM"},{"20", "UNDERLYING_CONTRA_FIRM"},{"21", "CLEARING_ORGANIZATION"},{"22", "EXCHANGE"},{"24", "CUSTOMER_ACCOUNT"},{"25", "CORRESPONDENT_CLEARING_ORGANIZATION"},{"26", "CORRESPONDENT_BROKER"},{"27", "BUYER_SELLER"},{"28", "CUSTODIAN"},{"29", "INTERMEDIARY"},{"30", "AGENT"},{"31", "SUB_CUSTODIAN"},{"32", "BENEFICIARY"},{"33", "INTERESTED_PARTY"},{"34", "REGULATORY_BODY"},{"35", "LIQUIDITY_PROVIDER"},{"36", "ENTERING_TRADER"},{"37", "CONTRA_TRADER"},{"38", "POSITION_ACCOUNT"}], "TagNum" => "452"}


,
"NoPartyIDs" => #{"TagNum" => "453" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "453" => #{"Name"=>"NoPartyIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "453"}


,
"NoSecurityAltID" => #{"TagNum" => "454" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "454" => #{"Name"=>"NoSecurityAltID" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "454"}


,
"SecurityAltID" => #{"TagNum" => "455" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "455" => #{"Name"=>"SecurityAltID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "455"}


,
"SecurityAltIDSource" => #{"TagNum" => "456" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "456" => #{"Name"=>"SecurityAltIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "456"}


,
"NoUnderlyingSecurityAltID" => #{"TagNum" => "457" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "457" => #{"Name"=>"NoUnderlyingSecurityAltID" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "457"}


,
"UnderlyingSecurityAltID" => #{"TagNum" => "458" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "458" => #{"Name"=>"UnderlyingSecurityAltID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "458"}


,
"UnderlyingSecurityAltIDSource" => #{"TagNum" => "459" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "459" => #{"Name"=>"UnderlyingSecurityAltIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "459"}


,
"Product" => #{"TagNum" => "460" ,"Type" => "INT" ,"ValidValues" =>[{"1", "AGENCY"},{"2", "COMMODITY"},{"3", "CORPORATE"},{"4", "CURRENCY"},{"5", "EQUITY"},{"6", "GOVERNMENT"},{"7", "INDEX"},{"8", "LOAN"},{"9", "MONEYMARKET"},{"10", "MORTGAGE"},{"11", "MUNICIPAL"},{"12", "OTHER"},{"13", "FINANCING"}]}
, "460" => #{"Name"=>"Product" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "AGENCY"},{"2", "COMMODITY"},{"3", "CORPORATE"},{"4", "CURRENCY"},{"5", "EQUITY"},{"6", "GOVERNMENT"},{"7", "INDEX"},{"8", "LOAN"},{"9", "MONEYMARKET"},{"10", "MORTGAGE"},{"11", "MUNICIPAL"},{"12", "OTHER"},{"13", "FINANCING"}], "TagNum" => "460"}


,
"CFICode" => #{"TagNum" => "461" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "461" => #{"Name"=>"CFICode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "461"}


,
"UnderlyingProduct" => #{"TagNum" => "462" ,"Type" => "INT" ,"ValidValues" =>[]}
, "462" => #{"Name"=>"UnderlyingProduct" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "462"}


,
"UnderlyingCFICode" => #{"TagNum" => "463" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "463" => #{"Name"=>"UnderlyingCFICode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "463"}


,
"TestMessageIndicator" => #{"TagNum" => "464" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "464" => #{"Name"=>"TestMessageIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "464"}


,
"BookingRefID" => #{"TagNum" => "466" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "466" => #{"Name"=>"BookingRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "466"}


,
"IndividualAllocID" => #{"TagNum" => "467" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "467" => #{"Name"=>"IndividualAllocID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "467"}


,
"RoundingDirection" => #{"TagNum" => "468" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "ROUND_TO_NEAREST"},{"1", "ROUND_DOWN"},{"2", "ROUND_UP"}]}
, "468" => #{"Name"=>"RoundingDirection" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "ROUND_TO_NEAREST"},{"1", "ROUND_DOWN"},{"2", "ROUND_UP"}], "TagNum" => "468"}


,
"RoundingModulus" => #{"TagNum" => "469" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "469" => #{"Name"=>"RoundingModulus" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "469"}


,
"CountryOfIssue" => #{"TagNum" => "470" ,"Type" => "COUNTRY" ,"ValidValues" =>[]}
, "470" => #{"Name"=>"CountryOfIssue" ,"Type"=>"COUNTRY" ,"ValidValues"=>[], "TagNum" => "470"}


,
"StateOrProvinceOfIssue" => #{"TagNum" => "471" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "471" => #{"Name"=>"StateOrProvinceOfIssue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "471"}


,
"LocaleOfIssue" => #{"TagNum" => "472" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "472" => #{"Name"=>"LocaleOfIssue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "472"}


,
"NoRegistDtls" => #{"TagNum" => "473" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "473" => #{"Name"=>"NoRegistDtls" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "473"}


,
"MailingDtls" => #{"TagNum" => "474" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "474" => #{"Name"=>"MailingDtls" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "474"}


,
"InvestorCountryOfResidence" => #{"TagNum" => "475" ,"Type" => "COUNTRY" ,"ValidValues" =>[]}
, "475" => #{"Name"=>"InvestorCountryOfResidence" ,"Type"=>"COUNTRY" ,"ValidValues"=>[], "TagNum" => "475"}


,
"PaymentRef" => #{"TagNum" => "476" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "476" => #{"Name"=>"PaymentRef" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "476"}


,
"DistribPaymentMethod" => #{"TagNum" => "477" ,"Type" => "INT" ,"ValidValues" =>[{"1", "CREST"},{"2", "NSCC"},{"3", "EUROCLEAR"},{"4", "CLEARSTREAM"},{"5", "CHEQUE"},{"6", "TELEGRAPHIC_TRANSFER"},{"7", "FEDWIRE"},{"8", "DIRECT_CREDIT"},{"9", "ACH_CREDIT"},{"10", "BPAY"},{"11", "HIGH_VALUE_CLEARING_SYSTEM"},{"12", "REINVEST_IN_FUND"}]}
, "477" => #{"Name"=>"DistribPaymentMethod" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "CREST"},{"2", "NSCC"},{"3", "EUROCLEAR"},{"4", "CLEARSTREAM"},{"5", "CHEQUE"},{"6", "TELEGRAPHIC_TRANSFER"},{"7", "FEDWIRE"},{"8", "DIRECT_CREDIT"},{"9", "ACH_CREDIT"},{"10", "BPAY"},{"11", "HIGH_VALUE_CLEARING_SYSTEM"},{"12", "REINVEST_IN_FUND"}], "TagNum" => "477"}


,
"CashDistribCurr" => #{"TagNum" => "478" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "478" => #{"Name"=>"CashDistribCurr" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "478"}


,
"CommCurrency" => #{"TagNum" => "479" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "479" => #{"Name"=>"CommCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "479"}


,
"CancellationRights" => #{"TagNum" => "480" ,"Type" => "CHAR" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO_EXECUTION_ONLY"},{"M", "NO_WAIVER_AGREEMENT"},{"O", "NO_INSTITUTIONAL"}]}
, "480" => #{"Name"=>"CancellationRights" ,"Type"=>"CHAR" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO_EXECUTION_ONLY"},{"M", "NO_WAIVER_AGREEMENT"},{"O", "NO_INSTITUTIONAL"}], "TagNum" => "480"}


,
"MoneyLaunderingStatus" => #{"TagNum" => "481" ,"Type" => "CHAR" ,"ValidValues" =>[{"Y", "PASSED"},{"N", "NOT_CHECKED"},{"1", "EXEMPT_BELOW_THE_LIMIT"},{"2", "EXEMPT_CLIENT_MONEY_TYPE_EXEMPTION"},{"3", "EXEMPT_AUTHORISED_CREDIT_OR_FINANCIAL_INSTITUTION"}]}
, "481" => #{"Name"=>"MoneyLaunderingStatus" ,"Type"=>"CHAR" ,"ValidValues"=>[{"Y", "PASSED"},{"N", "NOT_CHECKED"},{"1", "EXEMPT_BELOW_THE_LIMIT"},{"2", "EXEMPT_CLIENT_MONEY_TYPE_EXEMPTION"},{"3", "EXEMPT_AUTHORISED_CREDIT_OR_FINANCIAL_INSTITUTION"}], "TagNum" => "481"}


,
"MailingInst" => #{"TagNum" => "482" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "482" => #{"Name"=>"MailingInst" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "482"}


,
"TransBkdTime" => #{"TagNum" => "483" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "483" => #{"Name"=>"TransBkdTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "483"}


,
"ExecPriceType" => #{"TagNum" => "484" ,"Type" => "CHAR" ,"ValidValues" =>[{"B", "BID_PRICE"},{"C", "CREATION_PRICE"},{"D", "CREATION_PRICE_PLUS_ADJUSTMENT"},{"E", "CREATION_PRICE_PLUS_ADJUSTMENT_AMOUNT"},{"O", "OFFER_PRICE"},{"P", "OFFER_PRICE_MINUS_ADJUSTMENT"},{"Q", "OFFER_PRICE_MINUS_ADJUSTMENT_AMOUNT"},{"S", "SINGLE_PRICE"}]}
, "484" => #{"Name"=>"ExecPriceType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"B", "BID_PRICE"},{"C", "CREATION_PRICE"},{"D", "CREATION_PRICE_PLUS_ADJUSTMENT"},{"E", "CREATION_PRICE_PLUS_ADJUSTMENT_AMOUNT"},{"O", "OFFER_PRICE"},{"P", "OFFER_PRICE_MINUS_ADJUSTMENT"},{"Q", "OFFER_PRICE_MINUS_ADJUSTMENT_AMOUNT"},{"S", "SINGLE_PRICE"}], "TagNum" => "484"}


,
"ExecPriceAdjustment" => #{"TagNum" => "485" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "485" => #{"Name"=>"ExecPriceAdjustment" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "485"}


,
"DateOfBirth" => #{"TagNum" => "486" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "486" => #{"Name"=>"DateOfBirth" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "486"}


,
"TradeReportTransType" => #{"TagNum" => "487" ,"Type" => "INT" ,"ValidValues" =>[]}
, "487" => #{"Name"=>"TradeReportTransType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "487"}


,
"CardHolderName" => #{"TagNum" => "488" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "488" => #{"Name"=>"CardHolderName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "488"}


,
"CardNumber" => #{"TagNum" => "489" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "489" => #{"Name"=>"CardNumber" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "489"}


,
"CardExpDate" => #{"TagNum" => "490" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "490" => #{"Name"=>"CardExpDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "490"}


,
"CardIssNum" => #{"TagNum" => "491" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "491" => #{"Name"=>"CardIssNum" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "491"}


,
"PaymentMethod" => #{"TagNum" => "492" ,"Type" => "INT" ,"ValidValues" =>[{"1", "CREST"},{"2", "NSCC"},{"3", "EUROCLEAR"},{"4", "CLEARSTREAM"},{"5", "CHEQUE"},{"6", "TELEGRAPHIC_TRANSFER"},{"7", "FEDWIRE"},{"8", "DEBIT_CARD"},{"9", "DIRECT_DEBIT"},{"10", "DIRECT_CREDIT"},{"11", "CREDIT_CARD"},{"12", "ACH_DEBIT"},{"13", "ACH_CREDIT"},{"14", "BPAY"},{"15", "HIGH_VALUE_CLEARING_SYSTEM"}]}
, "492" => #{"Name"=>"PaymentMethod" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "CREST"},{"2", "NSCC"},{"3", "EUROCLEAR"},{"4", "CLEARSTREAM"},{"5", "CHEQUE"},{"6", "TELEGRAPHIC_TRANSFER"},{"7", "FEDWIRE"},{"8", "DEBIT_CARD"},{"9", "DIRECT_DEBIT"},{"10", "DIRECT_CREDIT"},{"11", "CREDIT_CARD"},{"12", "ACH_DEBIT"},{"13", "ACH_CREDIT"},{"14", "BPAY"},{"15", "HIGH_VALUE_CLEARING_SYSTEM"}], "TagNum" => "492"}


,
"RegistAcctType" => #{"TagNum" => "493" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "493" => #{"Name"=>"RegistAcctType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "493"}


,
"Designation" => #{"TagNum" => "494" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "494" => #{"Name"=>"Designation" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "494"}


,
"TaxAdvantageType" => #{"TagNum" => "495" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NONE_NOT_APPLICABLE"},{"1", "MAXI_ISA"},{"2", "TESSA"},{"3", "MINI_CASH_ISA"},{"4", "MINI_STOCKS_AND_SHARES_ISA"},{"5", "MINI_INSURANCE_ISA"},{"6", "CURRENT_YEAR_PAYMENT"},{"7", "PRIOR_YEAR_PAYMENT"},{"8", "ASSET_TRANSFER"},{"9", "EMPLOYEE"},{"10", "EMPLOYEE_CURRENT_YEAR"},{"11", "EMPLOYER"},{"12", "EMPLOYER_CURRENT_YEAR"},{"13", "NON_FUND_PROTOTYPE_IRA"},{"14", "NON_FUND_QUALIFIED_PLAN"},{"15", "DEFINED_CONTRIBUTION_PLAN"},{"16", "INDIVIDUAL_RETIREMENT_ACCOUNT"},{"17", "INDIVIDUAL_RETIREMENT_ACCOUNT_ROLLOVER"},{"18", "KEOGH"},{"19", "PROFIT_SHARING_PLAN"},{"20", "401K"},{"21", "SELF_DIRECTED_IRA"},{"22", "403"},{"23", "457"},{"24", "ROTH_IRA_24"},{"25", "ROTH_IRA_25"},{"26", "ROTH_CONVERSION_IRA_26"},{"27", "ROTH_CONVERSION_IRA_27"},{"28", "EDUCATION_IRA_28"},{"29", "EDUCATION_IRA_29"}]}
, "495" => #{"Name"=>"TaxAdvantageType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NONE_NOT_APPLICABLE"},{"1", "MAXI_ISA"},{"2", "TESSA"},{"3", "MINI_CASH_ISA"},{"4", "MINI_STOCKS_AND_SHARES_ISA"},{"5", "MINI_INSURANCE_ISA"},{"6", "CURRENT_YEAR_PAYMENT"},{"7", "PRIOR_YEAR_PAYMENT"},{"8", "ASSET_TRANSFER"},{"9", "EMPLOYEE"},{"10", "EMPLOYEE_CURRENT_YEAR"},{"11", "EMPLOYER"},{"12", "EMPLOYER_CURRENT_YEAR"},{"13", "NON_FUND_PROTOTYPE_IRA"},{"14", "NON_FUND_QUALIFIED_PLAN"},{"15", "DEFINED_CONTRIBUTION_PLAN"},{"16", "INDIVIDUAL_RETIREMENT_ACCOUNT"},{"17", "INDIVIDUAL_RETIREMENT_ACCOUNT_ROLLOVER"},{"18", "KEOGH"},{"19", "PROFIT_SHARING_PLAN"},{"20", "401K"},{"21", "SELF_DIRECTED_IRA"},{"22", "403"},{"23", "457"},{"24", "ROTH_IRA_24"},{"25", "ROTH_IRA_25"},{"26", "ROTH_CONVERSION_IRA_26"},{"27", "ROTH_CONVERSION_IRA_27"},{"28", "EDUCATION_IRA_28"},{"29", "EDUCATION_IRA_29"}], "TagNum" => "495"}


,
"RegistRejReasonText" => #{"TagNum" => "496" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "496" => #{"Name"=>"RegistRejReasonText" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "496"}


,
"FundRenewWaiv" => #{"TagNum" => "497" ,"Type" => "CHAR" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "497" => #{"Name"=>"FundRenewWaiv" ,"Type"=>"CHAR" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "497"}


,
"CashDistribAgentName" => #{"TagNum" => "498" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "498" => #{"Name"=>"CashDistribAgentName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "498"}


,
"CashDistribAgentCode" => #{"TagNum" => "499" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "499" => #{"Name"=>"CashDistribAgentCode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "499"}


,
"CashDistribAgentAcctNumber" => #{"TagNum" => "500" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "500" => #{"Name"=>"CashDistribAgentAcctNumber" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "500"}


,
"CashDistribPayRef" => #{"TagNum" => "501" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "501" => #{"Name"=>"CashDistribPayRef" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "501"}


,
"CashDistribAgentAcctName" => #{"TagNum" => "502" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "502" => #{"Name"=>"CashDistribAgentAcctName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "502"}


,
"CardStartDate" => #{"TagNum" => "503" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "503" => #{"Name"=>"CardStartDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "503"}


,
"PaymentDate" => #{"TagNum" => "504" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "504" => #{"Name"=>"PaymentDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "504"}


,
"PaymentRemitterID" => #{"TagNum" => "505" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "505" => #{"Name"=>"PaymentRemitterID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "505"}


,
"RegistStatus" => #{"TagNum" => "506" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "ACCEPTED"},{"R", "REJECTED"},{"H", "HELD"},{"N", "REMINDER_IE_REGISTRATION_INSTRUCTIONS_ARE_STILL_OUTSTANDING"}]}
, "506" => #{"Name"=>"RegistStatus" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "ACCEPTED"},{"R", "REJECTED"},{"H", "HELD"},{"N", "REMINDER_IE_REGISTRATION_INSTRUCTIONS_ARE_STILL_OUTSTANDING"}], "TagNum" => "506"}


,
"RegistRejReasonCode" => #{"TagNum" => "507" ,"Type" => "INT" ,"ValidValues" =>[{"1", "INVALID_UNACCEPTABLE_ACCOUNT_TYPE"},{"2", "INVALID_UNACCEPTABLE_TAX_EXEMPT_TYPE"},{"3", "INVALID_UNACCEPTABLE_OWNERSHIP_TYPE"},{"4", "INVALID_UNACCEPTABLE_NO_REG_DETLS"},{"5", "INVALID_UNACCEPTABLE_REG_SEQ_NO"},{"6", "INVALID_UNACCEPTABLE_REG_DTLS"},{"7", "INVALID_UNACCEPTABLE_MAILING_DTLS"},{"8", "INVALID_UNACCEPTABLE_MAILING_INST"},{"9", "INVALID_UNACCEPTABLE_INVESTOR_ID"},{"10", "INVALID_UNACCEPTABLE_INVESTOR_ID_SOURCE"},{"11", "INVALID_UNACCEPTABLE_DATE_OF_BIRTH"},{"12", "INVALID_UNACCEPTABLE_INVESTOR_COUNTRY_OF_RESIDENCE"},{"13", "INVALID_UNACCEPTABLE_NODISTRIBINSTNS"},{"14", "INVALID_UNACCEPTABLE_DISTRIB_PERCENTAGE"},{"15", "INVALID_UNACCEPTABLE_DISTRIB_PAYMENT_METHOD"},{"16", "INVALID_UNACCEPTABLE_CASH_DISTRIB_AGENT_ACCT_NAME"},{"17", "INVALID_UNACCEPTABLE_CASH_DISTRIB_AGENT_CODE"},{"18", "INVALID_UNACCEPTABLE_CASH_DISTRIB_AGENT_ACCT_NUM"},{"99", "OTHER"}]}
, "507" => #{"Name"=>"RegistRejReasonCode" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "INVALID_UNACCEPTABLE_ACCOUNT_TYPE"},{"2", "INVALID_UNACCEPTABLE_TAX_EXEMPT_TYPE"},{"3", "INVALID_UNACCEPTABLE_OWNERSHIP_TYPE"},{"4", "INVALID_UNACCEPTABLE_NO_REG_DETLS"},{"5", "INVALID_UNACCEPTABLE_REG_SEQ_NO"},{"6", "INVALID_UNACCEPTABLE_REG_DTLS"},{"7", "INVALID_UNACCEPTABLE_MAILING_DTLS"},{"8", "INVALID_UNACCEPTABLE_MAILING_INST"},{"9", "INVALID_UNACCEPTABLE_INVESTOR_ID"},{"10", "INVALID_UNACCEPTABLE_INVESTOR_ID_SOURCE"},{"11", "INVALID_UNACCEPTABLE_DATE_OF_BIRTH"},{"12", "INVALID_UNACCEPTABLE_INVESTOR_COUNTRY_OF_RESIDENCE"},{"13", "INVALID_UNACCEPTABLE_NODISTRIBINSTNS"},{"14", "INVALID_UNACCEPTABLE_DISTRIB_PERCENTAGE"},{"15", "INVALID_UNACCEPTABLE_DISTRIB_PAYMENT_METHOD"},{"16", "INVALID_UNACCEPTABLE_CASH_DISTRIB_AGENT_ACCT_NAME"},{"17", "INVALID_UNACCEPTABLE_CASH_DISTRIB_AGENT_CODE"},{"18", "INVALID_UNACCEPTABLE_CASH_DISTRIB_AGENT_ACCT_NUM"},{"99", "OTHER"}], "TagNum" => "507"}


,
"RegistRefID" => #{"TagNum" => "508" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "508" => #{"Name"=>"RegistRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "508"}


,
"RegistDtls" => #{"TagNum" => "509" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "509" => #{"Name"=>"RegistDtls" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "509"}


,
"NoDistribInsts" => #{"TagNum" => "510" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "510" => #{"Name"=>"NoDistribInsts" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "510"}


,
"RegistEmail" => #{"TagNum" => "511" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "511" => #{"Name"=>"RegistEmail" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "511"}


,
"DistribPercentage" => #{"TagNum" => "512" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "512" => #{"Name"=>"DistribPercentage" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "512"}


,
"RegistID" => #{"TagNum" => "513" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "513" => #{"Name"=>"RegistID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "513"}


,
"RegistTransType" => #{"TagNum" => "514" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "REPLACE"},{"2", "CANCEL"}]}
, "514" => #{"Name"=>"RegistTransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "REPLACE"},{"2", "CANCEL"}], "TagNum" => "514"}


,
"ExecValuationPoint" => #{"TagNum" => "515" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "515" => #{"Name"=>"ExecValuationPoint" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "515"}


,
"OrderPercent" => #{"TagNum" => "516" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "516" => #{"Name"=>"OrderPercent" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "516"}


,
"OwnershipType" => #{"TagNum" => "517" ,"Type" => "CHAR" ,"ValidValues" =>[{"J", "JOINT_INVESTORS"},{"T", "TENANTS_IN_COMMON"},{"2", "JOINT_TRUSTEES"}]}
, "517" => #{"Name"=>"OwnershipType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"J", "JOINT_INVESTORS"},{"T", "TENANTS_IN_COMMON"},{"2", "JOINT_TRUSTEES"}], "TagNum" => "517"}


,
"NoContAmts" => #{"TagNum" => "518" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "518" => #{"Name"=>"NoContAmts" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "518"}


,
"ContAmtType" => #{"TagNum" => "519" ,"Type" => "INT" ,"ValidValues" =>[{"1", "COMMISSION_AMOUNT"},{"2", "COMMISSION"},{"3", "INITIAL_CHARGE_AMOUNT"},{"4", "INITIAL_CHARGE"},{"5", "DISCOUNT_AMOUNT"},{"6", "DISCOUNT"},{"7", "DILUTION_LEVY_AMOUNT"},{"8", "DILUTION_LEVY"},{"9", "EXIT_CHARGE_AMOUNT"},{"10", "EXIT_CHARGE"},{"11", "FUND_BASED_RENEWAL_COMMISSION"},{"12", "PROJECTED_FUND_VALUE"},{"13", "FUND_BASED_RENEWAL_COMMISSION_AMOUNT_13"},{"14", "FUND_BASED_RENEWAL_COMMISSION_AMOUNT_14"},{"15", "NET_SETTLEMENT_AMOUNT"}]}
, "519" => #{"Name"=>"ContAmtType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "COMMISSION_AMOUNT"},{"2", "COMMISSION"},{"3", "INITIAL_CHARGE_AMOUNT"},{"4", "INITIAL_CHARGE"},{"5", "DISCOUNT_AMOUNT"},{"6", "DISCOUNT"},{"7", "DILUTION_LEVY_AMOUNT"},{"8", "DILUTION_LEVY"},{"9", "EXIT_CHARGE_AMOUNT"},{"10", "EXIT_CHARGE"},{"11", "FUND_BASED_RENEWAL_COMMISSION"},{"12", "PROJECTED_FUND_VALUE"},{"13", "FUND_BASED_RENEWAL_COMMISSION_AMOUNT_13"},{"14", "FUND_BASED_RENEWAL_COMMISSION_AMOUNT_14"},{"15", "NET_SETTLEMENT_AMOUNT"}], "TagNum" => "519"}


,
"ContAmtValue" => #{"TagNum" => "520" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "520" => #{"Name"=>"ContAmtValue" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "520"}


,
"ContAmtCurr" => #{"TagNum" => "521" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "521" => #{"Name"=>"ContAmtCurr" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "521"}


,
"OwnerType" => #{"TagNum" => "522" ,"Type" => "INT" ,"ValidValues" =>[{"1", "INDIVIDUAL_INVESTOR"},{"2", "PUBLIC_COMPANY"},{"3", "PRIVATE_COMPANY"},{"4", "INDIVIDUAL_TRUSTEE"},{"5", "COMPANY_TRUSTEE"},{"6", "PENSION_PLAN"},{"7", "CUSTODIAN_UNDER_GIFTS_TO_MINORS_ACT"},{"8", "TRUSTS"},{"9", "FIDUCIARIES"},{"10", "NETWORKING_SUB_ACCOUNT"},{"11", "NON_PROFIT_ORGANIZATION"},{"12", "CORPORATE_BODY"},{"13", "NOMINEE"}]}
, "522" => #{"Name"=>"OwnerType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "INDIVIDUAL_INVESTOR"},{"2", "PUBLIC_COMPANY"},{"3", "PRIVATE_COMPANY"},{"4", "INDIVIDUAL_TRUSTEE"},{"5", "COMPANY_TRUSTEE"},{"6", "PENSION_PLAN"},{"7", "CUSTODIAN_UNDER_GIFTS_TO_MINORS_ACT"},{"8", "TRUSTS"},{"9", "FIDUCIARIES"},{"10", "NETWORKING_SUB_ACCOUNT"},{"11", "NON_PROFIT_ORGANIZATION"},{"12", "CORPORATE_BODY"},{"13", "NOMINEE"}], "TagNum" => "522"}


,
"PartySubID" => #{"TagNum" => "523" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "523" => #{"Name"=>"PartySubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "523"}


,
"NestedPartyID" => #{"TagNum" => "524" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "524" => #{"Name"=>"NestedPartyID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "524"}


,
"NestedPartyIDSource" => #{"TagNum" => "525" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "525" => #{"Name"=>"NestedPartyIDSource" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "525"}


,
"SecondaryClOrdID" => #{"TagNum" => "526" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "526" => #{"Name"=>"SecondaryClOrdID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "526"}


,
"SecondaryExecID" => #{"TagNum" => "527" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "527" => #{"Name"=>"SecondaryExecID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "527"}


,
"OrderCapacity" => #{"TagNum" => "528" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "AGENCY"},{"G", "PROPRIETARY"},{"I", "INDIVIDUAL"},{"P", "PRINCIPAL"},{"R", "RISKLESS_PRINCIPAL"},{"W", "AGENT_FOR_OTHER_MEMBER"}]}
, "528" => #{"Name"=>"OrderCapacity" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "AGENCY"},{"G", "PROPRIETARY"},{"I", "INDIVIDUAL"},{"P", "PRINCIPAL"},{"R", "RISKLESS_PRINCIPAL"},{"W", "AGENT_FOR_OTHER_MEMBER"}], "TagNum" => "528"}


,
"OrderRestrictions" => #{"TagNum" => "529" ,"Type" => "MULTIPLEVALUESTRING" ,"ValidValues" =>[{"1", "PROGRAM_TRADE"},{"2", "INDEX_ARBITRAGE"},{"3", "NON_INDEX_ARBITRAGE"},{"4", "COMPETING_MARKET_MAKER"},{"5", "ACTING_AS_MARKET_MAKER_OR_SPECIALIST_IN_THE_SECURITY"},{"6", "ACTING_AS_MARKET_MAKER_OR_SPECIALIST_IN_THE_UNDERLYING_SECURITY_OF_A_DERIVATIVE_SECURITY"},{"7", "FOREIGN_ENTITY"},{"8", "EXTERNAL_MARKET_PARTICIPANT"},{"9", "EXTERNAL_INTER_CONNECTED_MARKET_LINKAGE"},{"A", "RISKLESS_ARBITRAGE"}]}
, "529" => #{"Name"=>"OrderRestrictions" ,"Type"=>"MULTIPLEVALUESTRING" ,"ValidValues"=>[{"1", "PROGRAM_TRADE"},{"2", "INDEX_ARBITRAGE"},{"3", "NON_INDEX_ARBITRAGE"},{"4", "COMPETING_MARKET_MAKER"},{"5", "ACTING_AS_MARKET_MAKER_OR_SPECIALIST_IN_THE_SECURITY"},{"6", "ACTING_AS_MARKET_MAKER_OR_SPECIALIST_IN_THE_UNDERLYING_SECURITY_OF_A_DERIVATIVE_SECURITY"},{"7", "FOREIGN_ENTITY"},{"8", "EXTERNAL_MARKET_PARTICIPANT"},{"9", "EXTERNAL_INTER_CONNECTED_MARKET_LINKAGE"},{"A", "RISKLESS_ARBITRAGE"}], "TagNum" => "529"}


,
"MassCancelRequestType" => #{"TagNum" => "530" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "CANCEL_ORDERS_FOR_A_SECURITY"},{"2", "CANCEL_ORDERS_FOR_AN_UNDERLYING_SECURITY"},{"3", "CANCEL_ORDERS_FOR_A_PRODUCT"},{"4", "CANCEL_ORDERS_FOR_A_CFICODE"},{"5", "CANCEL_ORDERS_FOR_A_SECURITYTYPE"},{"6", "CANCEL_ORDERS_FOR_A_TRADING_SESSION"},{"7", "CANCEL_ALL_ORDERS"}]}
, "530" => #{"Name"=>"MassCancelRequestType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "CANCEL_ORDERS_FOR_A_SECURITY"},{"2", "CANCEL_ORDERS_FOR_AN_UNDERLYING_SECURITY"},{"3", "CANCEL_ORDERS_FOR_A_PRODUCT"},{"4", "CANCEL_ORDERS_FOR_A_CFICODE"},{"5", "CANCEL_ORDERS_FOR_A_SECURITYTYPE"},{"6", "CANCEL_ORDERS_FOR_A_TRADING_SESSION"},{"7", "CANCEL_ALL_ORDERS"}], "TagNum" => "530"}


,
"MassCancelResponse" => #{"TagNum" => "531" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "CANCEL_REQUEST_REJECTED"},{"1", "CANCEL_ORDERS_FOR_A_SECURITY"},{"2", "CANCEL_ORDERS_FOR_AN_UNDERLYING_SECURITY"},{"3", "CANCEL_ORDERS_FOR_A_PRODUCT"},{"4", "CANCEL_ORDERS_FOR_A_CFICODE"},{"5", "CANCEL_ORDERS_FOR_A_SECURITYTYPE"},{"6", "CANCEL_ORDERS_FOR_A_TRADING_SESSION"},{"7", "CANCEL_ALL_ORDERS"}]}
, "531" => #{"Name"=>"MassCancelResponse" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "CANCEL_REQUEST_REJECTED"},{"1", "CANCEL_ORDERS_FOR_A_SECURITY"},{"2", "CANCEL_ORDERS_FOR_AN_UNDERLYING_SECURITY"},{"3", "CANCEL_ORDERS_FOR_A_PRODUCT"},{"4", "CANCEL_ORDERS_FOR_A_CFICODE"},{"5", "CANCEL_ORDERS_FOR_A_SECURITYTYPE"},{"6", "CANCEL_ORDERS_FOR_A_TRADING_SESSION"},{"7", "CANCEL_ALL_ORDERS"}], "TagNum" => "531"}


,
"MassCancelRejectReason" => #{"TagNum" => "532" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "MASS_CANCEL_NOT_SUPPORTED"},{"1", "INVALID_OR_UNKNOWN_SECURITY"},{"2", "INVALID_OR_UNKNOWN_UNDERLYING"},{"3", "INVALID_OR_UNKNOWN_PRODUCT"},{"4", "INVALID_OR_UNKNOWN_CFICODE"},{"5", "INVALID_OR_UNKNOWN_SECURITY_TYPE"},{"6", "INVALID_OR_UNKNOWN_TRADING_SESSION"},{"99", "OTHER"}]}
, "532" => #{"Name"=>"MassCancelRejectReason" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "MASS_CANCEL_NOT_SUPPORTED"},{"1", "INVALID_OR_UNKNOWN_SECURITY"},{"2", "INVALID_OR_UNKNOWN_UNDERLYING"},{"3", "INVALID_OR_UNKNOWN_PRODUCT"},{"4", "INVALID_OR_UNKNOWN_CFICODE"},{"5", "INVALID_OR_UNKNOWN_SECURITY_TYPE"},{"6", "INVALID_OR_UNKNOWN_TRADING_SESSION"},{"99", "OTHER"}], "TagNum" => "532"}


,
"TotalAffectedOrders" => #{"TagNum" => "533" ,"Type" => "INT" ,"ValidValues" =>[]}
, "533" => #{"Name"=>"TotalAffectedOrders" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "533"}


,
"NoAffectedOrders" => #{"TagNum" => "534" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "534" => #{"Name"=>"NoAffectedOrders" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "534"}


,
"AffectedOrderID" => #{"TagNum" => "535" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "535" => #{"Name"=>"AffectedOrderID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "535"}


,
"AffectedSecondaryOrderID" => #{"TagNum" => "536" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "536" => #{"Name"=>"AffectedSecondaryOrderID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "536"}


,
"QuoteType" => #{"TagNum" => "537" ,"Type" => "INT" ,"ValidValues" =>[{"0", "INDICATIVE"},{"1", "TRADEABLE"},{"2", "RESTRICTED_TRADEABLE"},{"3", "COUNTER"}]}
, "537" => #{"Name"=>"QuoteType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "INDICATIVE"},{"1", "TRADEABLE"},{"2", "RESTRICTED_TRADEABLE"},{"3", "COUNTER"}], "TagNum" => "537"}


,
"NestedPartyRole" => #{"TagNum" => "538" ,"Type" => "INT" ,"ValidValues" =>[]}
, "538" => #{"Name"=>"NestedPartyRole" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "538"}


,
"NoNestedPartyIDs" => #{"TagNum" => "539" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "539" => #{"Name"=>"NoNestedPartyIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "539"}


,
"TotalAccruedInterestAmt" => #{"TagNum" => "540" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "540" => #{"Name"=>"TotalAccruedInterestAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "540"}


,
"MaturityDate" => #{"TagNum" => "541" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "541" => #{"Name"=>"MaturityDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "541"}


,
"UnderlyingMaturityDate" => #{"TagNum" => "542" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "542" => #{"Name"=>"UnderlyingMaturityDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "542"}


,
"InstrRegistry" => #{"TagNum" => "543" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "543" => #{"Name"=>"InstrRegistry" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "543"}


,
"CashMargin" => #{"TagNum" => "544" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "CASH"},{"2", "MARGIN_OPEN"},{"3", "MARGIN_CLOSE"}]}
, "544" => #{"Name"=>"CashMargin" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "CASH"},{"2", "MARGIN_OPEN"},{"3", "MARGIN_CLOSE"}], "TagNum" => "544"}


,
"NestedPartySubID" => #{"TagNum" => "545" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "545" => #{"Name"=>"NestedPartySubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "545"}


,
"Scope" => #{"TagNum" => "546" ,"Type" => "MULTIPLEVALUESTRING" ,"ValidValues" =>[{"1", "LOCAL"},{"2", "NATIONAL"},{"3", "GLOBAL"}]}
, "546" => #{"Name"=>"Scope" ,"Type"=>"MULTIPLEVALUESTRING" ,"ValidValues"=>[{"1", "LOCAL"},{"2", "NATIONAL"},{"3", "GLOBAL"}], "TagNum" => "546"}


,
"MDImplicitDelete" => #{"TagNum" => "547" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "547" => #{"Name"=>"MDImplicitDelete" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "547"}


,
"CrossID" => #{"TagNum" => "548" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "548" => #{"Name"=>"CrossID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "548"}


,
"CrossType" => #{"TagNum" => "549" ,"Type" => "INT" ,"ValidValues" =>[{"1", "CROSS_TRADE_WHICH_IS_EXECUTED_COMPLETELY_OR_NOT_BOTH_SIDES_ARE_TREATED_IN_THE_SAME_MANNER_THIS_IS_EQUIVALENT_TO_AN_ALL_OR_NONE"},{"2", "CROSS_TRADE_WHICH_IS_EXECUTED_PARTIALLY_AND_THE_REST_IS_CANCELLED_ONE_SIDE_IS_FULLY_EXECUTED_THE_OTHER_SIDE_IS_PARTIALLY_EXECUTED_WITH_THE_REMAINDER_BEING_CANCELLED_THIS_IS_EQUIVALENT_TO_AN_IMMEDIATE_OR_CANCEL_ON_THE_OTHER_SIDE_NOTE_THE_CROSSPRIORITZATION"},{"3", "CROSS_TRADE_WHICH_IS_PARTIALLY_EXECUTED_WITH_THE_UNFILLED_PORTIONS_REMAINING_ACTIVE_ONE_SIDE_OF_THE_CROSS_IS_FULLY_EXECUTED"},{"4", "CROSS_TRADE_IS_EXECUTED_WITH_EXISTING_ORDERS_WITH_THE_SAME_PRICE_IN_THE_CASE_OTHER_ORDERS_EXIST_WITH_THE_SAME_PRICE_THE_QUANTITY_OF_THE_CROSS_IS_EXECUTED_AGAINST_THE_EXISTING_ORDERS_AND_QUOTES_THE_REMAINDER_OF_THE_CROSS_IS_EXECUTED_AGAINST_THE_OTHER_SIDE_OF_THE_CROSS_THE_TWO_SIDES_POTENTIALLY_HAVE_DIFFERENT_QUANTITIES"}]}
, "549" => #{"Name"=>"CrossType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "CROSS_TRADE_WHICH_IS_EXECUTED_COMPLETELY_OR_NOT_BOTH_SIDES_ARE_TREATED_IN_THE_SAME_MANNER_THIS_IS_EQUIVALENT_TO_AN_ALL_OR_NONE"},{"2", "CROSS_TRADE_WHICH_IS_EXECUTED_PARTIALLY_AND_THE_REST_IS_CANCELLED_ONE_SIDE_IS_FULLY_EXECUTED_THE_OTHER_SIDE_IS_PARTIALLY_EXECUTED_WITH_THE_REMAINDER_BEING_CANCELLED_THIS_IS_EQUIVALENT_TO_AN_IMMEDIATE_OR_CANCEL_ON_THE_OTHER_SIDE_NOTE_THE_CROSSPRIORITZATION"},{"3", "CROSS_TRADE_WHICH_IS_PARTIALLY_EXECUTED_WITH_THE_UNFILLED_PORTIONS_REMAINING_ACTIVE_ONE_SIDE_OF_THE_CROSS_IS_FULLY_EXECUTED"},{"4", "CROSS_TRADE_IS_EXECUTED_WITH_EXISTING_ORDERS_WITH_THE_SAME_PRICE_IN_THE_CASE_OTHER_ORDERS_EXIST_WITH_THE_SAME_PRICE_THE_QUANTITY_OF_THE_CROSS_IS_EXECUTED_AGAINST_THE_EXISTING_ORDERS_AND_QUOTES_THE_REMAINDER_OF_THE_CROSS_IS_EXECUTED_AGAINST_THE_OTHER_SIDE_OF_THE_CROSS_THE_TWO_SIDES_POTENTIALLY_HAVE_DIFFERENT_QUANTITIES"}], "TagNum" => "549"}


,
"CrossPrioritization" => #{"TagNum" => "550" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NONE"},{"1", "BUY_SIDE_IS_PRIORITIZED"},{"2", "SELL_SIDE_IS_PRIORITIZED"}]}
, "550" => #{"Name"=>"CrossPrioritization" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NONE"},{"1", "BUY_SIDE_IS_PRIORITIZED"},{"2", "SELL_SIDE_IS_PRIORITIZED"}], "TagNum" => "550"}


,
"OrigCrossID" => #{"TagNum" => "551" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "551" => #{"Name"=>"OrigCrossID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "551"}


,
"NoSides" => #{"TagNum" => "552" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[{"1", "ONE_SIDE"},{"2", "BOTH_SIDES"}]}
, "552" => #{"Name"=>"NoSides" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[{"1", "ONE_SIDE"},{"2", "BOTH_SIDES"}], "TagNum" => "552"}


,
"Username" => #{"TagNum" => "553" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "553" => #{"Name"=>"Username" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "553"}


,
"Password" => #{"TagNum" => "554" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "554" => #{"Name"=>"Password" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "554"}


,
"NoLegs" => #{"TagNum" => "555" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "555" => #{"Name"=>"NoLegs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "555"}


,
"LegCurrency" => #{"TagNum" => "556" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "556" => #{"Name"=>"LegCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "556"}


,
"TotNoSecurityTypes" => #{"TagNum" => "557" ,"Type" => "INT" ,"ValidValues" =>[]}
, "557" => #{"Name"=>"TotNoSecurityTypes" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "557"}


,
"NoSecurityTypes" => #{"TagNum" => "558" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "558" => #{"Name"=>"NoSecurityTypes" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "558"}


,
"SecurityListRequestType" => #{"TagNum" => "559" ,"Type" => "INT" ,"ValidValues" =>[{"0", "SYMBOL"},{"1", "SECURITYTYPE_AND_OR_CFICODE"},{"2", "PRODUCT"},{"3", "TRADINGSESSIONID"},{"4", "ALL_SECURITIES"}]}
, "559" => #{"Name"=>"SecurityListRequestType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "SYMBOL"},{"1", "SECURITYTYPE_AND_OR_CFICODE"},{"2", "PRODUCT"},{"3", "TRADINGSESSIONID"},{"4", "ALL_SECURITIES"}], "TagNum" => "559"}


,
"SecurityRequestResult" => #{"TagNum" => "560" ,"Type" => "INT" ,"ValidValues" =>[{"0", "VALID_REQUEST"},{"1", "INVALID_OR_UNSUPPORTED_REQUEST"},{"2", "NO_INSTRUMENTS_FOUND_THAT_MATCH_SELECTION_CRITERIA"},{"3", "NOT_AUTHORIZED_TO_RETRIEVE_INSTRUMENT_DATA"},{"4", "INSTRUMENT_DATA_TEMPORARILY_UNAVAILABLE"},{"5", "REQUEST_FOR_INSTRUMENT_DATA_NOT_SUPPORTED"}]}
, "560" => #{"Name"=>"SecurityRequestResult" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "VALID_REQUEST"},{"1", "INVALID_OR_UNSUPPORTED_REQUEST"},{"2", "NO_INSTRUMENTS_FOUND_THAT_MATCH_SELECTION_CRITERIA"},{"3", "NOT_AUTHORIZED_TO_RETRIEVE_INSTRUMENT_DATA"},{"4", "INSTRUMENT_DATA_TEMPORARILY_UNAVAILABLE"},{"5", "REQUEST_FOR_INSTRUMENT_DATA_NOT_SUPPORTED"}], "TagNum" => "560"}


,
"RoundLot" => #{"TagNum" => "561" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "561" => #{"Name"=>"RoundLot" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "561"}


,
"MinTradeVol" => #{"TagNum" => "562" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "562" => #{"Name"=>"MinTradeVol" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "562"}


,
"MultiLegRptTypeReq" => #{"TagNum" => "563" ,"Type" => "INT" ,"ValidValues" =>[{"0", "REPORT_BY_MULITLEG_SECURITY_ONLY"},{"1", "REPORT_BY_MULTILEG_SECURITY_AND_BY_INSTRUMENT_LEGS_BELONGING_TO_THE_MULTILEG_SECURITY"},{"2", "REPORT_BY_INSTRUMENT_LEGS_BELONGING_TO_THE_MULTILEG_SECURITY_ONLY"}]}
, "563" => #{"Name"=>"MultiLegRptTypeReq" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "REPORT_BY_MULITLEG_SECURITY_ONLY"},{"1", "REPORT_BY_MULTILEG_SECURITY_AND_BY_INSTRUMENT_LEGS_BELONGING_TO_THE_MULTILEG_SECURITY"},{"2", "REPORT_BY_INSTRUMENT_LEGS_BELONGING_TO_THE_MULTILEG_SECURITY_ONLY"}], "TagNum" => "563"}


,
"LegPositionEffect" => #{"TagNum" => "564" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "564" => #{"Name"=>"LegPositionEffect" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "564"}


,
"LegCoveredOrUncovered" => #{"TagNum" => "565" ,"Type" => "INT" ,"ValidValues" =>[]}
, "565" => #{"Name"=>"LegCoveredOrUncovered" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "565"}


,
"LegPrice" => #{"TagNum" => "566" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "566" => #{"Name"=>"LegPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "566"}


,
"TradSesStatusRejReason" => #{"TagNum" => "567" ,"Type" => "INT" ,"ValidValues" =>[{"1", "UNKNOWN_OR_INVALID_TRADINGSESSIONID"},{"99", "OTHER"}]}
, "567" => #{"Name"=>"TradSesStatusRejReason" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "UNKNOWN_OR_INVALID_TRADINGSESSIONID"},{"99", "OTHER"}], "TagNum" => "567"}


,
"TradeRequestID" => #{"TagNum" => "568" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "568" => #{"Name"=>"TradeRequestID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "568"}


,
"TradeRequestType" => #{"TagNum" => "569" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ALL_TRADES"},{"1", "MATCHED_TRADES_MATCHING_CRITERIA_PROVIDED_ON_REQUEST"},{"2", "UNMATCHED_TRADES_THAT_MATCH_CRITERIA"},{"3", "UNREPORTED_TRADES_THAT_MATCH_CRITERIA"},{"4", "ADVISORIES_THAT_MATCH_CRITERIA"}]}
, "569" => #{"Name"=>"TradeRequestType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ALL_TRADES"},{"1", "MATCHED_TRADES_MATCHING_CRITERIA_PROVIDED_ON_REQUEST"},{"2", "UNMATCHED_TRADES_THAT_MATCH_CRITERIA"},{"3", "UNREPORTED_TRADES_THAT_MATCH_CRITERIA"},{"4", "ADVISORIES_THAT_MATCH_CRITERIA"}], "TagNum" => "569"}


,
"PreviouslyReported" => #{"TagNum" => "570" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "570" => #{"Name"=>"PreviouslyReported" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "570"}


,
"TradeReportID" => #{"TagNum" => "571" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "571" => #{"Name"=>"TradeReportID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "571"}


,
"TradeReportRefID" => #{"TagNum" => "572" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "572" => #{"Name"=>"TradeReportRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "572"}


,
"MatchStatus" => #{"TagNum" => "573" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "COMPARED_MATCHED_OR_AFFIRMED"},{"1", "UNCOMPARED_UNMATCHED_OR_UNAFFIRMED"},{"2", "ADVISORY_OR_ALERT"}]}
, "573" => #{"Name"=>"MatchStatus" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "COMPARED_MATCHED_OR_AFFIRMED"},{"1", "UNCOMPARED_UNMATCHED_OR_UNAFFIRMED"},{"2", "ADVISORY_OR_ALERT"}], "TagNum" => "573"}


,
"MatchType" => #{"TagNum" => "574" ,"Type" => "STRING" ,"ValidValues" =>[{"A1", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_FOUR_BADGES_AND_EXECUTION_TIME"},{"A2", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_FOUR_BADGES"},{"A3", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_TWO_BADGES_AND_EXECUTION_TIME"},{"A4", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_TWO_BADGES"},{"A5", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_EXECUTION_TIME"},{"AQ", "COMPARED_RECORDS_RESULTING_FROM_STAMPED_ADVISORIES_OR_SPECIALIST_ACCEPTS_PAIR_OFFS"},{"S1", "SUMMARIZED_MATCH_USING_A1_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED"},{"S2", "SUMMARIZED_MATCH_USING_A2_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED"},{"S3", "SUMMARIZED_MATCH_USING_A3_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED"},{"S4", "SUMMARIZED_MATCH_USING_A4_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED"},{"S5", "SUMMARIZED_MATCH_USING_A5_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED"},{"M1", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_MINUS_BADGES_AND_TIMES_ACT_M1_MATCH"},{"M2", "SUMMARIZED_MATCH_MINUS_BADGES_AND_TIMES_ACT_M2_MATCH"},{"MT", "OCS_LOCKED_IN_NON_ACT"},{"M3", "ACT_ACCEPTED_TRADE"},{"M4", "ACT_DEFAULT_TRADE"},{"M5", "ACT_DEFAULT_AFTER_M2"},{"M6", "ACT_M6_MATCH"}]}
, "574" => #{"Name"=>"MatchType" ,"Type"=>"STRING" ,"ValidValues"=>[{"A1", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_FOUR_BADGES_AND_EXECUTION_TIME"},{"A2", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_FOUR_BADGES"},{"A3", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_TWO_BADGES_AND_EXECUTION_TIME"},{"A4", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_TWO_BADGES"},{"A5", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_EXECUTION_TIME"},{"AQ", "COMPARED_RECORDS_RESULTING_FROM_STAMPED_ADVISORIES_OR_SPECIALIST_ACCEPTS_PAIR_OFFS"},{"S1", "SUMMARIZED_MATCH_USING_A1_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED"},{"S2", "SUMMARIZED_MATCH_USING_A2_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED"},{"S3", "SUMMARIZED_MATCH_USING_A3_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED"},{"S4", "SUMMARIZED_MATCH_USING_A4_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED"},{"S5", "SUMMARIZED_MATCH_USING_A5_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED"},{"M1", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_MINUS_BADGES_AND_TIMES_ACT_M1_MATCH"},{"M2", "SUMMARIZED_MATCH_MINUS_BADGES_AND_TIMES_ACT_M2_MATCH"},{"MT", "OCS_LOCKED_IN_NON_ACT"},{"M3", "ACT_ACCEPTED_TRADE"},{"M4", "ACT_DEFAULT_TRADE"},{"M5", "ACT_DEFAULT_AFTER_M2"},{"M6", "ACT_M6_MATCH"}], "TagNum" => "574"}


,
"OddLot" => #{"TagNum" => "575" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "575" => #{"Name"=>"OddLot" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "575"}


,
"NoClearingInstructions" => #{"TagNum" => "576" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "576" => #{"Name"=>"NoClearingInstructions" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "576"}


,
"ClearingInstruction" => #{"TagNum" => "577" ,"Type" => "INT" ,"ValidValues" =>[{"0", "PROCESS_NORMALLY"},{"1", "EXCLUDE_FROM_ALL_NETTING"},{"2", "BILATERAL_NETTING_ONLY"},{"3", "EX_CLEARING"},{"4", "SPECIAL_TRADE"},{"5", "MULTILATERAL_NETTING"},{"6", "CLEAR_AGAINST_CENTRAL_COUNTERPARTY"},{"7", "EXCLUDE_FROM_CENTRAL_COUNTERPARTY"},{"8", "MANUAL_MODE"},{"9", "AUTOMATIC_POSTING_MODE"},{"10", "AUTOMATIC_GIVE_UP_MODE"},{"11", "QUALIFIED_SERVICE_REPRESENTATIVE"},{"12", "CUSTOMER_TRADE"},{"13", "SELF_CLEARING"}]}
, "577" => #{"Name"=>"ClearingInstruction" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "PROCESS_NORMALLY"},{"1", "EXCLUDE_FROM_ALL_NETTING"},{"2", "BILATERAL_NETTING_ONLY"},{"3", "EX_CLEARING"},{"4", "SPECIAL_TRADE"},{"5", "MULTILATERAL_NETTING"},{"6", "CLEAR_AGAINST_CENTRAL_COUNTERPARTY"},{"7", "EXCLUDE_FROM_CENTRAL_COUNTERPARTY"},{"8", "MANUAL_MODE"},{"9", "AUTOMATIC_POSTING_MODE"},{"10", "AUTOMATIC_GIVE_UP_MODE"},{"11", "QUALIFIED_SERVICE_REPRESENTATIVE"},{"12", "CUSTOMER_TRADE"},{"13", "SELF_CLEARING"}], "TagNum" => "577"}


,
"TradeInputSource" => #{"TagNum" => "578" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "578" => #{"Name"=>"TradeInputSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "578"}


,
"TradeInputDevice" => #{"TagNum" => "579" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "579" => #{"Name"=>"TradeInputDevice" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "579"}


,
"NoDates" => #{"TagNum" => "580" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "580" => #{"Name"=>"NoDates" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "580"}


,
"AccountType" => #{"TagNum" => "581" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ACCOUNT_IS_CARRIED_ON_CUSTOMER_SIDE_OF_BOOKS"},{"2", "ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS"},{"3", "HOUSE_TRADER"},{"4", "FLOOR_TRADER"},{"6", "ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS_AND_IS_CROSS_MARGINED"},{"7", "ACCOUNT_IS_HOUSE_TRADER_AND_IS_CROSS_MARGINED"},{"8", "JOINT_BACKOFFICE_ACCOUNT"}]}
, "581" => #{"Name"=>"AccountType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ACCOUNT_IS_CARRIED_ON_CUSTOMER_SIDE_OF_BOOKS"},{"2", "ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS"},{"3", "HOUSE_TRADER"},{"4", "FLOOR_TRADER"},{"6", "ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS_AND_IS_CROSS_MARGINED"},{"7", "ACCOUNT_IS_HOUSE_TRADER_AND_IS_CROSS_MARGINED"},{"8", "JOINT_BACKOFFICE_ACCOUNT"}], "TagNum" => "581"}


,
"CustOrderCapacity" => #{"TagNum" => "582" ,"Type" => "INT" ,"ValidValues" =>[{"1", "MEMBER_TRADING_FOR_THEIR_OWN_ACCOUNT"},{"2", "CLEARING_FIRM_TRADING_FOR_ITS_PROPRIETARY_ACCOUNT"},{"3", "MEMBER_TRADING_FOR_ANOTHER_MEMBER"},{"4", "ALL_OTHER"}]}
, "582" => #{"Name"=>"CustOrderCapacity" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "MEMBER_TRADING_FOR_THEIR_OWN_ACCOUNT"},{"2", "CLEARING_FIRM_TRADING_FOR_ITS_PROPRIETARY_ACCOUNT"},{"3", "MEMBER_TRADING_FOR_ANOTHER_MEMBER"},{"4", "ALL_OTHER"}], "TagNum" => "582"}


,
"ClOrdLinkID" => #{"TagNum" => "583" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "583" => #{"Name"=>"ClOrdLinkID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "583"}


,
"MassStatusReqID" => #{"TagNum" => "584" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "584" => #{"Name"=>"MassStatusReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "584"}


,
"MassStatusReqType" => #{"TagNum" => "585" ,"Type" => "INT" ,"ValidValues" =>[{"1", "STATUS_FOR_ORDERS_FOR_A_SECURITY"},{"2", "STATUS_FOR_ORDERS_FOR_AN_UNDERLYING_SECURITY"},{"3", "STATUS_FOR_ORDERS_FOR_A_PRODUCT"},{"4", "STATUS_FOR_ORDERS_FOR_A_CFICODE"},{"5", "STATUS_FOR_ORDERS_FOR_A_SECURITYTYPE"},{"6", "STATUS_FOR_ORDERS_FOR_A_TRADING_SESSION"},{"7", "STATUS_FOR_ALL_ORDERS"},{"8", "STATUS_FOR_ORDERS_FOR_A_PARTYID"}]}
, "585" => #{"Name"=>"MassStatusReqType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "STATUS_FOR_ORDERS_FOR_A_SECURITY"},{"2", "STATUS_FOR_ORDERS_FOR_AN_UNDERLYING_SECURITY"},{"3", "STATUS_FOR_ORDERS_FOR_A_PRODUCT"},{"4", "STATUS_FOR_ORDERS_FOR_A_CFICODE"},{"5", "STATUS_FOR_ORDERS_FOR_A_SECURITYTYPE"},{"6", "STATUS_FOR_ORDERS_FOR_A_TRADING_SESSION"},{"7", "STATUS_FOR_ALL_ORDERS"},{"8", "STATUS_FOR_ORDERS_FOR_A_PARTYID"}], "TagNum" => "585"}


,
"OrigOrdModTime" => #{"TagNum" => "586" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "586" => #{"Name"=>"OrigOrdModTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "586"}


,
"LegSettlType" => #{"TagNum" => "587" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "587" => #{"Name"=>"LegSettlType" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "587"}


,
"LegSettlDate" => #{"TagNum" => "588" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "588" => #{"Name"=>"LegSettlDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "588"}


,
"DayBookingInst" => #{"TagNum" => "589" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "CAN_TRIGGER_BOOKING_WITHOUT_REFERENCE_TO_THE_ORDER_INITIATOR"},{"1", "SPEAK_WITH_ORDER_INITIATOR_BEFORE_BOOKING"},{"2", "ACCUMULATE"}]}
, "589" => #{"Name"=>"DayBookingInst" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "CAN_TRIGGER_BOOKING_WITHOUT_REFERENCE_TO_THE_ORDER_INITIATOR"},{"1", "SPEAK_WITH_ORDER_INITIATOR_BEFORE_BOOKING"},{"2", "ACCUMULATE"}], "TagNum" => "589"}


,
"BookingUnit" => #{"TagNum" => "590" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "EACH_PARTIAL_EXECUTION_IS_A_BOOKABLE_UNIT"},{"1", "AGGREGATE_PARTIAL_EXECUTIONS_ON_THIS_ORDER_AND_BOOK_ONE_TRADE_PER_ORDER"},{"2", "AGGREGATE_EXECUTIONS_FOR_THIS_SYMBOL_SIDE_AND_SETTLEMENT_DATE"}]}
, "590" => #{"Name"=>"BookingUnit" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "EACH_PARTIAL_EXECUTION_IS_A_BOOKABLE_UNIT"},{"1", "AGGREGATE_PARTIAL_EXECUTIONS_ON_THIS_ORDER_AND_BOOK_ONE_TRADE_PER_ORDER"},{"2", "AGGREGATE_EXECUTIONS_FOR_THIS_SYMBOL_SIDE_AND_SETTLEMENT_DATE"}], "TagNum" => "590"}


,
"PreallocMethod" => #{"TagNum" => "591" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "PRO_RATA"},{"1", "DO_NOT_PRO_RATA_DISCUSS_FIRST"}]}
, "591" => #{"Name"=>"PreallocMethod" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "PRO_RATA"},{"1", "DO_NOT_PRO_RATA_DISCUSS_FIRST"}], "TagNum" => "591"}


,
"UnderlyingCountryOfIssue" => #{"TagNum" => "592" ,"Type" => "COUNTRY" ,"ValidValues" =>[]}
, "592" => #{"Name"=>"UnderlyingCountryOfIssue" ,"Type"=>"COUNTRY" ,"ValidValues"=>[], "TagNum" => "592"}


,
"UnderlyingStateOrProvinceOfIssue" => #{"TagNum" => "593" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "593" => #{"Name"=>"UnderlyingStateOrProvinceOfIssue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "593"}


,
"UnderlyingLocaleOfIssue" => #{"TagNum" => "594" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "594" => #{"Name"=>"UnderlyingLocaleOfIssue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "594"}


,
"UnderlyingInstrRegistry" => #{"TagNum" => "595" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "595" => #{"Name"=>"UnderlyingInstrRegistry" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "595"}


,
"LegCountryOfIssue" => #{"TagNum" => "596" ,"Type" => "COUNTRY" ,"ValidValues" =>[]}
, "596" => #{"Name"=>"LegCountryOfIssue" ,"Type"=>"COUNTRY" ,"ValidValues"=>[], "TagNum" => "596"}


,
"LegStateOrProvinceOfIssue" => #{"TagNum" => "597" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "597" => #{"Name"=>"LegStateOrProvinceOfIssue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "597"}


,
"LegLocaleOfIssue" => #{"TagNum" => "598" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "598" => #{"Name"=>"LegLocaleOfIssue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "598"}


,
"LegInstrRegistry" => #{"TagNum" => "599" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "599" => #{"Name"=>"LegInstrRegistry" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "599"}


,
"LegSymbol" => #{"TagNum" => "600" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "600" => #{"Name"=>"LegSymbol" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "600"}


,
"LegSymbolSfx" => #{"TagNum" => "601" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "601" => #{"Name"=>"LegSymbolSfx" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "601"}


,
"LegSecurityID" => #{"TagNum" => "602" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "602" => #{"Name"=>"LegSecurityID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "602"}


,
"LegSecurityIDSource" => #{"TagNum" => "603" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "603" => #{"Name"=>"LegSecurityIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "603"}


,
"NoLegSecurityAltID" => #{"TagNum" => "604" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "604" => #{"Name"=>"NoLegSecurityAltID" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "604"}


,
"LegSecurityAltID" => #{"TagNum" => "605" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "605" => #{"Name"=>"LegSecurityAltID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "605"}


,
"LegSecurityAltIDSource" => #{"TagNum" => "606" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "606" => #{"Name"=>"LegSecurityAltIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "606"}


,
"LegProduct" => #{"TagNum" => "607" ,"Type" => "INT" ,"ValidValues" =>[]}
, "607" => #{"Name"=>"LegProduct" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "607"}


,
"LegCFICode" => #{"TagNum" => "608" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "608" => #{"Name"=>"LegCFICode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "608"}


,
"LegSecurityType" => #{"TagNum" => "609" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "609" => #{"Name"=>"LegSecurityType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "609"}


,
"LegMaturityMonthYear" => #{"TagNum" => "610" ,"Type" => "MONTHYEAR" ,"ValidValues" =>[]}
, "610" => #{"Name"=>"LegMaturityMonthYear" ,"Type"=>"MONTHYEAR" ,"ValidValues"=>[], "TagNum" => "610"}


,
"LegMaturityDate" => #{"TagNum" => "611" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "611" => #{"Name"=>"LegMaturityDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "611"}


,
"LegStrikePrice" => #{"TagNum" => "612" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "612" => #{"Name"=>"LegStrikePrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "612"}


,
"LegOptAttribute" => #{"TagNum" => "613" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "613" => #{"Name"=>"LegOptAttribute" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "613"}


,
"LegContractMultiplier" => #{"TagNum" => "614" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "614" => #{"Name"=>"LegContractMultiplier" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "614"}


,
"LegCouponRate" => #{"TagNum" => "615" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "615" => #{"Name"=>"LegCouponRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "615"}


,
"LegSecurityExchange" => #{"TagNum" => "616" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "616" => #{"Name"=>"LegSecurityExchange" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "616"}


,
"LegIssuer" => #{"TagNum" => "617" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "617" => #{"Name"=>"LegIssuer" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "617"}


,
"EncodedLegIssuerLen" => #{"TagNum" => "618" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "618" => #{"Name"=>"EncodedLegIssuerLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "618"}


,
"EncodedLegIssuer" => #{"TagNum" => "619" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "619" => #{"Name"=>"EncodedLegIssuer" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "619"}


,
"LegSecurityDesc" => #{"TagNum" => "620" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "620" => #{"Name"=>"LegSecurityDesc" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "620"}


,
"EncodedLegSecurityDescLen" => #{"TagNum" => "621" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "621" => #{"Name"=>"EncodedLegSecurityDescLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "621"}


,
"EncodedLegSecurityDesc" => #{"TagNum" => "622" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "622" => #{"Name"=>"EncodedLegSecurityDesc" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "622"}


,
"LegRatioQty" => #{"TagNum" => "623" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "623" => #{"Name"=>"LegRatioQty" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "623"}


,
"LegSide" => #{"TagNum" => "624" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "624" => #{"Name"=>"LegSide" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "624"}


,
"TradingSessionSubID" => #{"TagNum" => "625" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "625" => #{"Name"=>"TradingSessionSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "625"}


,
"AllocType" => #{"TagNum" => "626" ,"Type" => "INT" ,"ValidValues" =>[{"1", "CALCULATED"},{"2", "PRELIMINARY"},{"5", "READY_TO_BOOK"},{"7", "WAREHOUSE_INSTRUCTION"},{"8", "REQUEST_TO_INTERMEDIARY"}]}
, "626" => #{"Name"=>"AllocType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "CALCULATED"},{"2", "PRELIMINARY"},{"5", "READY_TO_BOOK"},{"7", "WAREHOUSE_INSTRUCTION"},{"8", "REQUEST_TO_INTERMEDIARY"}], "TagNum" => "626"}


,
"NoHops" => #{"TagNum" => "627" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "627" => #{"Name"=>"NoHops" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "627"}


,
"HopCompID" => #{"TagNum" => "628" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "628" => #{"Name"=>"HopCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "628"}


,
"HopSendingTime" => #{"TagNum" => "629" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "629" => #{"Name"=>"HopSendingTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "629"}


,
"HopRefID" => #{"TagNum" => "630" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "630" => #{"Name"=>"HopRefID" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "630"}


,
"MidPx" => #{"TagNum" => "631" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "631" => #{"Name"=>"MidPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "631"}


,
"BidYield" => #{"TagNum" => "632" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "632" => #{"Name"=>"BidYield" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "632"}


,
"MidYield" => #{"TagNum" => "633" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "633" => #{"Name"=>"MidYield" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "633"}


,
"OfferYield" => #{"TagNum" => "634" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "634" => #{"Name"=>"OfferYield" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "634"}


,
"ClearingFeeIndicator" => #{"TagNum" => "635" ,"Type" => "STRING" ,"ValidValues" =>[{"B", "CBOE_MEMBER"},{"C", "NON_MEMBER_AND_CUSTOMER"},{"E", "EQUITY_MEMBER_AND_CLEARING_MEMBER"},{"F", "FULL_AND_ASSOCIATE_MEMBER_TRADING_FOR_OWN_ACCOUNT_AND_AS_FLOOR_BROKERS"},{"H", "106H_AND_106J_FIRMS"},{"I", "GIM_IDEM_AND_COM_MEMBERSHIP_INTEREST_HOLDERS"},{"L", "LESSEE_AND_106F_EMPLOYEES"},{"M", "ALL_OTHER_OWNERSHIP_TYPES"},{"1", "1ST_YEAR_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"},{"2", "2ND_YEAR_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"},{"3", "3RD_YEAR_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"},{"4", "4TH_YEAR_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"},{"5", "5TH_YEAR_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"},{"9", "6TH_YEAR_AND_BEYOND_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"}]}
, "635" => #{"Name"=>"ClearingFeeIndicator" ,"Type"=>"STRING" ,"ValidValues"=>[{"B", "CBOE_MEMBER"},{"C", "NON_MEMBER_AND_CUSTOMER"},{"E", "EQUITY_MEMBER_AND_CLEARING_MEMBER"},{"F", "FULL_AND_ASSOCIATE_MEMBER_TRADING_FOR_OWN_ACCOUNT_AND_AS_FLOOR_BROKERS"},{"H", "106H_AND_106J_FIRMS"},{"I", "GIM_IDEM_AND_COM_MEMBERSHIP_INTEREST_HOLDERS"},{"L", "LESSEE_AND_106F_EMPLOYEES"},{"M", "ALL_OTHER_OWNERSHIP_TYPES"},{"1", "1ST_YEAR_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"},{"2", "2ND_YEAR_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"},{"3", "3RD_YEAR_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"},{"4", "4TH_YEAR_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"},{"5", "5TH_YEAR_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"},{"9", "6TH_YEAR_AND_BEYOND_DELEGATE_TRADING_FOR_HIS_OWN_ACCOUNT"}], "TagNum" => "635"}


,
"WorkingIndicator" => #{"TagNum" => "636" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "636" => #{"Name"=>"WorkingIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "636"}


,
"LegLastPx" => #{"TagNum" => "637" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "637" => #{"Name"=>"LegLastPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "637"}


,
"PriorityIndicator" => #{"TagNum" => "638" ,"Type" => "INT" ,"ValidValues" =>[{"0", "PRIORITY_UNCHANGED"},{"1", "LOST_PRIORITY_AS_RESULT_OF_ORDER_CHANGE"}]}
, "638" => #{"Name"=>"PriorityIndicator" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "PRIORITY_UNCHANGED"},{"1", "LOST_PRIORITY_AS_RESULT_OF_ORDER_CHANGE"}], "TagNum" => "638"}


,
"PriceImprovement" => #{"TagNum" => "639" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "639" => #{"Name"=>"PriceImprovement" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "639"}


,
"Price2" => #{"TagNum" => "640" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "640" => #{"Name"=>"Price2" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "640"}


,
"LastForwardPoints2" => #{"TagNum" => "641" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "641" => #{"Name"=>"LastForwardPoints2" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "641"}


,
"BidForwardPoints2" => #{"TagNum" => "642" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "642" => #{"Name"=>"BidForwardPoints2" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "642"}


,
"OfferForwardPoints2" => #{"TagNum" => "643" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "643" => #{"Name"=>"OfferForwardPoints2" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "643"}


,
"RFQReqID" => #{"TagNum" => "644" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "644" => #{"Name"=>"RFQReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "644"}


,
"MktBidPx" => #{"TagNum" => "645" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "645" => #{"Name"=>"MktBidPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "645"}


,
"MktOfferPx" => #{"TagNum" => "646" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "646" => #{"Name"=>"MktOfferPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "646"}


,
"MinBidSize" => #{"TagNum" => "647" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "647" => #{"Name"=>"MinBidSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "647"}


,
"MinOfferSize" => #{"TagNum" => "648" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "648" => #{"Name"=>"MinOfferSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "648"}


,
"QuoteStatusReqID" => #{"TagNum" => "649" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "649" => #{"Name"=>"QuoteStatusReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "649"}


,
"LegalConfirm" => #{"TagNum" => "650" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "650" => #{"Name"=>"LegalConfirm" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "650"}


,
"UnderlyingLastPx" => #{"TagNum" => "651" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "651" => #{"Name"=>"UnderlyingLastPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "651"}


,
"UnderlyingLastQty" => #{"TagNum" => "652" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "652" => #{"Name"=>"UnderlyingLastQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "652"}


,
"LegRefID" => #{"TagNum" => "654" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "654" => #{"Name"=>"LegRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "654"}


,
"ContraLegRefID" => #{"TagNum" => "655" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "655" => #{"Name"=>"ContraLegRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "655"}


,
"SettlCurrBidFxRate" => #{"TagNum" => "656" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "656" => #{"Name"=>"SettlCurrBidFxRate" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "656"}


,
"SettlCurrOfferFxRate" => #{"TagNum" => "657" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "657" => #{"Name"=>"SettlCurrOfferFxRate" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "657"}


,
"QuoteRequestRejectReason" => #{"TagNum" => "658" ,"Type" => "INT" ,"ValidValues" =>[{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE"},{"3", "QUOTE_REQUEST_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "INVALID_PRICE"},{"6", "NOT_AUTHORIZED_TO_REQUEST_QUOTE"},{"7", "NO_MATCH_FOR_INQUIRY"},{"8", "NO_MARKET_FOR_INSTRUMENT"},{"9", "NO_INVENTORY"},{"10", "PASS"},{"99", "OTHER"}]}
, "658" => #{"Name"=>"QuoteRequestRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE"},{"3", "QUOTE_REQUEST_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "INVALID_PRICE"},{"6", "NOT_AUTHORIZED_TO_REQUEST_QUOTE"},{"7", "NO_MATCH_FOR_INQUIRY"},{"8", "NO_MARKET_FOR_INSTRUMENT"},{"9", "NO_INVENTORY"},{"10", "PASS"},{"99", "OTHER"}], "TagNum" => "658"}


,
"SideComplianceID" => #{"TagNum" => "659" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "659" => #{"Name"=>"SideComplianceID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "659"}


,
"AcctIDSource" => #{"TagNum" => "660" ,"Type" => "INT" ,"ValidValues" =>[{"1", "BIC"},{"2", "SID_CODE"},{"3", "TFM"},{"4", "OMGEO"},{"5", "DTCC_CODE"},{"99", "OTHER"}]}
, "660" => #{"Name"=>"AcctIDSource" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "BIC"},{"2", "SID_CODE"},{"3", "TFM"},{"4", "OMGEO"},{"5", "DTCC_CODE"},{"99", "OTHER"}], "TagNum" => "660"}


,
"AllocAcctIDSource" => #{"TagNum" => "661" ,"Type" => "INT" ,"ValidValues" =>[]}
, "661" => #{"Name"=>"AllocAcctIDSource" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "661"}


,
"BenchmarkPrice" => #{"TagNum" => "662" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "662" => #{"Name"=>"BenchmarkPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "662"}


,
"BenchmarkPriceType" => #{"TagNum" => "663" ,"Type" => "INT" ,"ValidValues" =>[]}
, "663" => #{"Name"=>"BenchmarkPriceType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "663"}


,
"ConfirmID" => #{"TagNum" => "664" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "664" => #{"Name"=>"ConfirmID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "664"}


,
"ConfirmStatus" => #{"TagNum" => "665" ,"Type" => "INT" ,"ValidValues" =>[{"1", "RECEIVED"},{"2", "MISMATCHED_ACCOUNT"},{"3", "MISSING_SETTLEMENT_INSTRUCTIONS"},{"4", "CONFIRMED"},{"5", "REQUEST_REJECTED"}]}
, "665" => #{"Name"=>"ConfirmStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "RECEIVED"},{"2", "MISMATCHED_ACCOUNT"},{"3", "MISSING_SETTLEMENT_INSTRUCTIONS"},{"4", "CONFIRMED"},{"5", "REQUEST_REJECTED"}], "TagNum" => "665"}


,
"ConfirmTransType" => #{"TagNum" => "666" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NEW"},{"1", "REPLACE"},{"2", "CANCEL"}]}
, "666" => #{"Name"=>"ConfirmTransType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NEW"},{"1", "REPLACE"},{"2", "CANCEL"}], "TagNum" => "666"}


,
"ContractSettlMonth" => #{"TagNum" => "667" ,"Type" => "MONTHYEAR" ,"ValidValues" =>[]}
, "667" => #{"Name"=>"ContractSettlMonth" ,"Type"=>"MONTHYEAR" ,"ValidValues"=>[], "TagNum" => "667"}


,
"DeliveryForm" => #{"TagNum" => "668" ,"Type" => "INT" ,"ValidValues" =>[{"1", "BOOKENTRY"},{"2", "BEARER"}]}
, "668" => #{"Name"=>"DeliveryForm" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "BOOKENTRY"},{"2", "BEARER"}], "TagNum" => "668"}


,
"LastParPx" => #{"TagNum" => "669" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "669" => #{"Name"=>"LastParPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "669"}


,
"NoLegAllocs" => #{"TagNum" => "670" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "670" => #{"Name"=>"NoLegAllocs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "670"}


,
"LegAllocAccount" => #{"TagNum" => "671" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "671" => #{"Name"=>"LegAllocAccount" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "671"}


,
"LegIndividualAllocID" => #{"TagNum" => "672" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "672" => #{"Name"=>"LegIndividualAllocID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "672"}


,
"LegAllocQty" => #{"TagNum" => "673" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "673" => #{"Name"=>"LegAllocQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "673"}


,
"LegAllocAcctIDSource" => #{"TagNum" => "674" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "674" => #{"Name"=>"LegAllocAcctIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "674"}


,
"LegSettlCurrency" => #{"TagNum" => "675" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "675" => #{"Name"=>"LegSettlCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "675"}


,
"LegBenchmarkCurveCurrency" => #{"TagNum" => "676" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "676" => #{"Name"=>"LegBenchmarkCurveCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "676"}


,
"LegBenchmarkCurveName" => #{"TagNum" => "677" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "677" => #{"Name"=>"LegBenchmarkCurveName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "677"}


,
"LegBenchmarkCurvePoint" => #{"TagNum" => "678" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "678" => #{"Name"=>"LegBenchmarkCurvePoint" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "678"}


,
"LegBenchmarkPrice" => #{"TagNum" => "679" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "679" => #{"Name"=>"LegBenchmarkPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "679"}


,
"LegBenchmarkPriceType" => #{"TagNum" => "680" ,"Type" => "INT" ,"ValidValues" =>[]}
, "680" => #{"Name"=>"LegBenchmarkPriceType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "680"}


,
"LegBidPx" => #{"TagNum" => "681" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "681" => #{"Name"=>"LegBidPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "681"}


,
"LegIOIQty" => #{"TagNum" => "682" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "682" => #{"Name"=>"LegIOIQty" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "682"}


,
"NoLegStipulations" => #{"TagNum" => "683" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "683" => #{"Name"=>"NoLegStipulations" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "683"}


,
"LegOfferPx" => #{"TagNum" => "684" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "684" => #{"Name"=>"LegOfferPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "684"}


,
"LegPriceType" => #{"TagNum" => "686" ,"Type" => "INT" ,"ValidValues" =>[]}
, "686" => #{"Name"=>"LegPriceType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "686"}


,
"LegQty" => #{"TagNum" => "687" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "687" => #{"Name"=>"LegQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "687"}


,
"LegStipulationType" => #{"TagNum" => "688" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "688" => #{"Name"=>"LegStipulationType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "688"}


,
"LegStipulationValue" => #{"TagNum" => "689" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "689" => #{"Name"=>"LegStipulationValue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "689"}


,
"LegSwapType" => #{"TagNum" => "690" ,"Type" => "INT" ,"ValidValues" =>[{"1", "PAR_FOR_PAR"},{"2", "MODIFIED_DURATION"},{"4", "RISK"},{"5", "PROCEEDS"}]}
, "690" => #{"Name"=>"LegSwapType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "PAR_FOR_PAR"},{"2", "MODIFIED_DURATION"},{"4", "RISK"},{"5", "PROCEEDS"}], "TagNum" => "690"}


,
"Pool" => #{"TagNum" => "691" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "691" => #{"Name"=>"Pool" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "691"}


,
"QuotePriceType" => #{"TagNum" => "692" ,"Type" => "INT" ,"ValidValues" =>[{"1", "PERCENT"},{"2", "PER_SHARE"},{"3", "FIXED_AMOUNT"},{"4", "DISCOUNT_PERCENTAGE_POINTS_BELOW_PAR"},{"5", "PREMIUM_PERCENTAGE_POINTS_OVER_PAR"},{"6", "BASIS_POINTS_RELATIVE_TO_BENCHMARK"},{"7", "TED_PRICE"},{"8", "TED_YIELD"},{"9", "YIELD_SPREAD"},{"10", "YIELD"}]}
, "692" => #{"Name"=>"QuotePriceType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "PERCENT"},{"2", "PER_SHARE"},{"3", "FIXED_AMOUNT"},{"4", "DISCOUNT_PERCENTAGE_POINTS_BELOW_PAR"},{"5", "PREMIUM_PERCENTAGE_POINTS_OVER_PAR"},{"6", "BASIS_POINTS_RELATIVE_TO_BENCHMARK"},{"7", "TED_PRICE"},{"8", "TED_YIELD"},{"9", "YIELD_SPREAD"},{"10", "YIELD"}], "TagNum" => "692"}


,
"QuoteRespID" => #{"TagNum" => "693" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "693" => #{"Name"=>"QuoteRespID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "693"}


,
"QuoteRespType" => #{"TagNum" => "694" ,"Type" => "INT" ,"ValidValues" =>[{"1", "HIT_LIFT"},{"2", "COUNTER"},{"3", "EXPIRED"},{"4", "COVER"},{"5", "DONE_AWAY"},{"6", "PASS"}]}
, "694" => #{"Name"=>"QuoteRespType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "HIT_LIFT"},{"2", "COUNTER"},{"3", "EXPIRED"},{"4", "COVER"},{"5", "DONE_AWAY"},{"6", "PASS"}], "TagNum" => "694"}


,
"QuoteQualifier" => #{"TagNum" => "695" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "695" => #{"Name"=>"QuoteQualifier" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "695"}


,
"YieldRedemptionDate" => #{"TagNum" => "696" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "696" => #{"Name"=>"YieldRedemptionDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "696"}


,
"YieldRedemptionPrice" => #{"TagNum" => "697" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "697" => #{"Name"=>"YieldRedemptionPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "697"}


,
"YieldRedemptionPriceType" => #{"TagNum" => "698" ,"Type" => "INT" ,"ValidValues" =>[]}
, "698" => #{"Name"=>"YieldRedemptionPriceType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "698"}


,
"BenchmarkSecurityID" => #{"TagNum" => "699" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "699" => #{"Name"=>"BenchmarkSecurityID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "699"}


,
"ReversalIndicator" => #{"TagNum" => "700" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "700" => #{"Name"=>"ReversalIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "700"}


,
"YieldCalcDate" => #{"TagNum" => "701" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "701" => #{"Name"=>"YieldCalcDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "701"}


,
"NoPositions" => #{"TagNum" => "702" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "702" => #{"Name"=>"NoPositions" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "702"}


,
"PosType" => #{"TagNum" => "703" ,"Type" => "STRING" ,"ValidValues" =>[{"TQ", "TRANSACTION_QUANTITY"},{"IAS", "INTRA_SPREAD_QTY"},{"IES", "INTER_SPREAD_QTY"},{"FIN", "END_OF_DAY_QTY"},{"SOD", "START_OF_DAY_QTY"},{"EX", "OPTION_EXERCISE_QTY"},{"AS", "OPTION_ASSIGNMENT"},{"TX", "TRANSACTION_FROM_EXERCISE"},{"TA", "TRANSACTION_FROM_ASSIGNMENT"},{"PIT", "PIT_TRADE_QTY"},{"TRF", "TRANSFER_TRADE_QTY"},{"ETR", "ELECTRONIC_TRADE_QTY"},{"ALC", "ALLOCATION_TRADE_QTY"},{"PA", "ADJUSTMENT_QTY"},{"ASF", "AS_OF_TRADE_QTY"},{"DLV", "DELIVERY_QTY"},{"TOT", "TOTAL_TRANSACTION_QTY"},{"XM", "CROSS_MARGIN_QTY"},{"SPL", "INTEGRAL_SPLIT"}]}
, "703" => #{"Name"=>"PosType" ,"Type"=>"STRING" ,"ValidValues"=>[{"TQ", "TRANSACTION_QUANTITY"},{"IAS", "INTRA_SPREAD_QTY"},{"IES", "INTER_SPREAD_QTY"},{"FIN", "END_OF_DAY_QTY"},{"SOD", "START_OF_DAY_QTY"},{"EX", "OPTION_EXERCISE_QTY"},{"AS", "OPTION_ASSIGNMENT"},{"TX", "TRANSACTION_FROM_EXERCISE"},{"TA", "TRANSACTION_FROM_ASSIGNMENT"},{"PIT", "PIT_TRADE_QTY"},{"TRF", "TRANSFER_TRADE_QTY"},{"ETR", "ELECTRONIC_TRADE_QTY"},{"ALC", "ALLOCATION_TRADE_QTY"},{"PA", "ADJUSTMENT_QTY"},{"ASF", "AS_OF_TRADE_QTY"},{"DLV", "DELIVERY_QTY"},{"TOT", "TOTAL_TRANSACTION_QTY"},{"XM", "CROSS_MARGIN_QTY"},{"SPL", "INTEGRAL_SPLIT"}], "TagNum" => "703"}


,
"LongQty" => #{"TagNum" => "704" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "704" => #{"Name"=>"LongQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "704"}


,
"ShortQty" => #{"TagNum" => "705" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "705" => #{"Name"=>"ShortQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "705"}


,
"PosQtyStatus" => #{"TagNum" => "706" ,"Type" => "INT" ,"ValidValues" =>[{"0", "SUBMITTED"},{"1", "ACCEPTED"},{"2", "REJECTED"}]}
, "706" => #{"Name"=>"PosQtyStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "SUBMITTED"},{"1", "ACCEPTED"},{"2", "REJECTED"}], "TagNum" => "706"}


,
"PosAmtType" => #{"TagNum" => "707" ,"Type" => "STRING" ,"ValidValues" =>[{"FMTM", "FINAL_MARK_TO_MARKET_AMOUNT"},{"IMTM", "INCREMENTAL_MARK_TO_MARKET_AMOUNT"},{"TVAR", "TRADE_VARIATION_AMOUNT"},{"SMTM", "START_OF_DAY_MARK_TO_MARKET_AMOUNT"},{"PREM", "PREMIUM_AMOUNT"},{"CRES", "CASH_RESIDUAL_AMOUNT"},{"CASH", "CASH_AMOUNT"},{"VADJ", "VALUE_ADJUSTED_AMOUNT"}]}
, "707" => #{"Name"=>"PosAmtType" ,"Type"=>"STRING" ,"ValidValues"=>[{"FMTM", "FINAL_MARK_TO_MARKET_AMOUNT"},{"IMTM", "INCREMENTAL_MARK_TO_MARKET_AMOUNT"},{"TVAR", "TRADE_VARIATION_AMOUNT"},{"SMTM", "START_OF_DAY_MARK_TO_MARKET_AMOUNT"},{"PREM", "PREMIUM_AMOUNT"},{"CRES", "CASH_RESIDUAL_AMOUNT"},{"CASH", "CASH_AMOUNT"},{"VADJ", "VALUE_ADJUSTED_AMOUNT"}], "TagNum" => "707"}


,
"PosAmt" => #{"TagNum" => "708" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "708" => #{"Name"=>"PosAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "708"}


,
"PosTransType" => #{"TagNum" => "709" ,"Type" => "INT" ,"ValidValues" =>[{"1", "EXERCISE"},{"2", "DO_NOT_EXERCISE"},{"3", "POSITION_ADJUSTMENT"},{"4", "POSITION_CHANGE_SUBMISSION_MARGIN_DISPOSITION"},{"5", "PLEDGE"}]}
, "709" => #{"Name"=>"PosTransType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "EXERCISE"},{"2", "DO_NOT_EXERCISE"},{"3", "POSITION_ADJUSTMENT"},{"4", "POSITION_CHANGE_SUBMISSION_MARGIN_DISPOSITION"},{"5", "PLEDGE"}], "TagNum" => "709"}


,
"PosReqID" => #{"TagNum" => "710" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "710" => #{"Name"=>"PosReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "710"}


,
"NoUnderlyings" => #{"TagNum" => "711" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "711" => #{"Name"=>"NoUnderlyings" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "711"}


,
"PosMaintAction" => #{"TagNum" => "712" ,"Type" => "INT" ,"ValidValues" =>[{"1", "NEW_USED_TO_INCREMENT_THE_OVERALL_TRANSACTION_QUANTITY"},{"2", "REPLACE_USED_TO_OVERRIDE_THE_OVERALL_TRANSACTION_QUANTITY_OR_SPECIFIC_ADD_MESSAGES_BASED_ON_THE_REFERENCE_ID"},{"3", "CANCEL_USED_TO_REMOVE_THE_OVERALL_TRANSACTION_OR_SPECIFIC_ADD_MESSAGES_BASED_ON_REFERENCE_ID"}]}
, "712" => #{"Name"=>"PosMaintAction" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "NEW_USED_TO_INCREMENT_THE_OVERALL_TRANSACTION_QUANTITY"},{"2", "REPLACE_USED_TO_OVERRIDE_THE_OVERALL_TRANSACTION_QUANTITY_OR_SPECIFIC_ADD_MESSAGES_BASED_ON_THE_REFERENCE_ID"},{"3", "CANCEL_USED_TO_REMOVE_THE_OVERALL_TRANSACTION_OR_SPECIFIC_ADD_MESSAGES_BASED_ON_REFERENCE_ID"}], "TagNum" => "712"}


,
"OrigPosReqRefID" => #{"TagNum" => "713" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "713" => #{"Name"=>"OrigPosReqRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "713"}


,
"PosMaintRptRefID" => #{"TagNum" => "714" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "714" => #{"Name"=>"PosMaintRptRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "714"}


,
"ClearingBusinessDate" => #{"TagNum" => "715" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "715" => #{"Name"=>"ClearingBusinessDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "715"}


,
"SettlSessID" => #{"TagNum" => "716" ,"Type" => "STRING" ,"ValidValues" =>[{"ITD", "INTRADAY"},{"RTH", "REGULAR_TRADING_HOURS"},{"ETH", "ELECTRONIC_TRADING_HOURS"}]}
, "716" => #{"Name"=>"SettlSessID" ,"Type"=>"STRING" ,"ValidValues"=>[{"ITD", "INTRADAY"},{"RTH", "REGULAR_TRADING_HOURS"},{"ETH", "ELECTRONIC_TRADING_HOURS"}], "TagNum" => "716"}


,
"SettlSessSubID" => #{"TagNum" => "717" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "717" => #{"Name"=>"SettlSessSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "717"}


,
"AdjustmentType" => #{"TagNum" => "718" ,"Type" => "INT" ,"ValidValues" =>[{"0", "PROCESS_REQUEST_AS_MARGIN_DISPOSITION"},{"1", "DELTA_PLUS"},{"2", "DELTA_MINUS"},{"3", "FINAL"}]}
, "718" => #{"Name"=>"AdjustmentType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "PROCESS_REQUEST_AS_MARGIN_DISPOSITION"},{"1", "DELTA_PLUS"},{"2", "DELTA_MINUS"},{"3", "FINAL"}], "TagNum" => "718"}


,
"ContraryInstructionIndicator" => #{"TagNum" => "719" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "719" => #{"Name"=>"ContraryInstructionIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "719"}


,
"PriorSpreadIndicator" => #{"TagNum" => "720" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "720" => #{"Name"=>"PriorSpreadIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "720"}


,
"PosMaintRptID" => #{"TagNum" => "721" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "721" => #{"Name"=>"PosMaintRptID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "721"}


,
"PosMaintStatus" => #{"TagNum" => "722" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ACCEPTED"},{"1", "ACCEPTED_WITH_WARNINGS"},{"2", "REJECTED"},{"3", "COMPLETED"},{"4", "COMPLETED_WITH_WARNINGS"}]}
, "722" => #{"Name"=>"PosMaintStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ACCEPTED"},{"1", "ACCEPTED_WITH_WARNINGS"},{"2", "REJECTED"},{"3", "COMPLETED"},{"4", "COMPLETED_WITH_WARNINGS"}], "TagNum" => "722"}


,
"PosMaintResult" => #{"TagNum" => "723" ,"Type" => "INT" ,"ValidValues" =>[{"0", "SUCCESSFUL_COMPLETION"},{"1", "REJECTED"},{"99", "OTHER"}]}
, "723" => #{"Name"=>"PosMaintResult" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "SUCCESSFUL_COMPLETION"},{"1", "REJECTED"},{"99", "OTHER"}], "TagNum" => "723"}


,
"PosReqType" => #{"TagNum" => "724" ,"Type" => "INT" ,"ValidValues" =>[{"0", "POSITIONS"},{"1", "TRADES"},{"2", "EXERCISES"},{"3", "ASSIGNMENTS"}]}
, "724" => #{"Name"=>"PosReqType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "POSITIONS"},{"1", "TRADES"},{"2", "EXERCISES"},{"3", "ASSIGNMENTS"}], "TagNum" => "724"}


,
"ResponseTransportType" => #{"TagNum" => "725" ,"Type" => "INT" ,"ValidValues" =>[{"0", "INBAND_TRANSPORT_THE_REQUEST_WAS_SENT_OVER"},{"1", "OUT_OF_BAND_PRE_ARRANGED_OUT_OF_BAND_DELIVERY_MECHANISM"}]}
, "725" => #{"Name"=>"ResponseTransportType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "INBAND_TRANSPORT_THE_REQUEST_WAS_SENT_OVER"},{"1", "OUT_OF_BAND_PRE_ARRANGED_OUT_OF_BAND_DELIVERY_MECHANISM"}], "TagNum" => "725"}


,
"ResponseDestination" => #{"TagNum" => "726" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "726" => #{"Name"=>"ResponseDestination" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "726"}


,
"TotalNumPosReports" => #{"TagNum" => "727" ,"Type" => "INT" ,"ValidValues" =>[]}
, "727" => #{"Name"=>"TotalNumPosReports" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "727"}


,
"PosReqResult" => #{"TagNum" => "728" ,"Type" => "INT" ,"ValidValues" =>[{"0", "VALID_REQUEST"},{"1", "INVALID_OR_UNSUPPORTED_REQUEST"},{"2", "NO_POSITIONS_FOUND_THAT_MATCH_CRITERIA"},{"3", "NOT_AUTHORIZED_TO_REQUEST_POSITIONS"},{"4", "REQUEST_FOR_POSITION_NOT_SUPPORTED"},{"99", "OTHER"}]}
, "728" => #{"Name"=>"PosReqResult" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "VALID_REQUEST"},{"1", "INVALID_OR_UNSUPPORTED_REQUEST"},{"2", "NO_POSITIONS_FOUND_THAT_MATCH_CRITERIA"},{"3", "NOT_AUTHORIZED_TO_REQUEST_POSITIONS"},{"4", "REQUEST_FOR_POSITION_NOT_SUPPORTED"},{"99", "OTHER"}], "TagNum" => "728"}


,
"PosReqStatus" => #{"TagNum" => "729" ,"Type" => "INT" ,"ValidValues" =>[{"0", "COMPLETED"},{"1", "COMPLETED_WITH_WARNINGS"},{"2", "REJECTED"}]}
, "729" => #{"Name"=>"PosReqStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "COMPLETED"},{"1", "COMPLETED_WITH_WARNINGS"},{"2", "REJECTED"}], "TagNum" => "729"}


,
"SettlPrice" => #{"TagNum" => "730" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "730" => #{"Name"=>"SettlPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "730"}


,
"SettlPriceType" => #{"TagNum" => "731" ,"Type" => "INT" ,"ValidValues" =>[{"1", "FINAL"},{"2", "THEORETICAL"}]}
, "731" => #{"Name"=>"SettlPriceType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "FINAL"},{"2", "THEORETICAL"}], "TagNum" => "731"}


,
"UnderlyingSettlPrice" => #{"TagNum" => "732" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "732" => #{"Name"=>"UnderlyingSettlPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "732"}


,
"UnderlyingSettlPriceType" => #{"TagNum" => "733" ,"Type" => "INT" ,"ValidValues" =>[]}
, "733" => #{"Name"=>"UnderlyingSettlPriceType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "733"}


,
"PriorSettlPrice" => #{"TagNum" => "734" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "734" => #{"Name"=>"PriorSettlPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "734"}


,
"NoQuoteQualifiers" => #{"TagNum" => "735" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "735" => #{"Name"=>"NoQuoteQualifiers" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "735"}


,
"AllocSettlCurrency" => #{"TagNum" => "736" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "736" => #{"Name"=>"AllocSettlCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "736"}


,
"AllocSettlCurrAmt" => #{"TagNum" => "737" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "737" => #{"Name"=>"AllocSettlCurrAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "737"}


,
"InterestAtMaturity" => #{"TagNum" => "738" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "738" => #{"Name"=>"InterestAtMaturity" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "738"}


,
"LegDatedDate" => #{"TagNum" => "739" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "739" => #{"Name"=>"LegDatedDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "739"}


,
"LegPool" => #{"TagNum" => "740" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "740" => #{"Name"=>"LegPool" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "740"}


,
"AllocInterestAtMaturity" => #{"TagNum" => "741" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "741" => #{"Name"=>"AllocInterestAtMaturity" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "741"}


,
"AllocAccruedInterestAmt" => #{"TagNum" => "742" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "742" => #{"Name"=>"AllocAccruedInterestAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "742"}


,
"DeliveryDate" => #{"TagNum" => "743" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "743" => #{"Name"=>"DeliveryDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "743"}


,
"AssignmentMethod" => #{"TagNum" => "744" ,"Type" => "CHAR" ,"ValidValues" =>[{"R", "RANDOM"},{"P", "PRORATA"}]}
, "744" => #{"Name"=>"AssignmentMethod" ,"Type"=>"CHAR" ,"ValidValues"=>[{"R", "RANDOM"},{"P", "PRORATA"}], "TagNum" => "744"}


,
"AssignmentUnit" => #{"TagNum" => "745" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "745" => #{"Name"=>"AssignmentUnit" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "745"}


,
"OpenInterest" => #{"TagNum" => "746" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "746" => #{"Name"=>"OpenInterest" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "746"}


,
"ExerciseMethod" => #{"TagNum" => "747" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "AUTOMATIC"},{"M", "MANUAL"}]}
, "747" => #{"Name"=>"ExerciseMethod" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "AUTOMATIC"},{"M", "MANUAL"}], "TagNum" => "747"}


,
"TotNumTradeReports" => #{"TagNum" => "748" ,"Type" => "INT" ,"ValidValues" =>[]}
, "748" => #{"Name"=>"TotNumTradeReports" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "748"}


,
"TradeRequestResult" => #{"TagNum" => "749" ,"Type" => "INT" ,"ValidValues" =>[{"0", "SUCCESSFUL"},{"1", "INVALID_OR_UNKNOWN_INSTRUMENT"},{"2", "INVALID_TYPE_OF_TRADE_REQUESTED"},{"3", "INVALID_PARTIES"},{"4", "INVALID_TRANSPORT_TYPE_REQUESTED"},{"5", "INVALID_DESTINATION_REQUESTED"},{"8", "TRADEREQUESTTYPE_NOT_SUPPORTED"},{"9", "UNAUTHORIZED_FOR_TRADE_CAPTURE_REPORT_REQUEST"},{"99", "OTHER"}]}
, "749" => #{"Name"=>"TradeRequestResult" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "SUCCESSFUL"},{"1", "INVALID_OR_UNKNOWN_INSTRUMENT"},{"2", "INVALID_TYPE_OF_TRADE_REQUESTED"},{"3", "INVALID_PARTIES"},{"4", "INVALID_TRANSPORT_TYPE_REQUESTED"},{"5", "INVALID_DESTINATION_REQUESTED"},{"8", "TRADEREQUESTTYPE_NOT_SUPPORTED"},{"9", "UNAUTHORIZED_FOR_TRADE_CAPTURE_REPORT_REQUEST"},{"99", "OTHER"}], "TagNum" => "749"}


,
"TradeRequestStatus" => #{"TagNum" => "750" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ACCEPTED"},{"1", "COMPLETED"},{"2", "REJECTED"}]}
, "750" => #{"Name"=>"TradeRequestStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ACCEPTED"},{"1", "COMPLETED"},{"2", "REJECTED"}], "TagNum" => "750"}


,
"TradeReportRejectReason" => #{"TagNum" => "751" ,"Type" => "INT" ,"ValidValues" =>[{"0", "SUCCESSFUL"},{"1", "INVALID_PARTY_INFORMATION"},{"2", "UNKNOWN_INSTRUMENT"},{"3", "UNAUTHORIZED_TO_REPORT_TRADES"},{"4", "INVALID_TRADE_TYPE"},{"99", "OTHER"}]}
, "751" => #{"Name"=>"TradeReportRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "SUCCESSFUL"},{"1", "INVALID_PARTY_INFORMATION"},{"2", "UNKNOWN_INSTRUMENT"},{"3", "UNAUTHORIZED_TO_REPORT_TRADES"},{"4", "INVALID_TRADE_TYPE"},{"99", "OTHER"}], "TagNum" => "751"}


,
"SideMultiLegReportingType" => #{"TagNum" => "752" ,"Type" => "INT" ,"ValidValues" =>[{"1", "SINGLE_SECURITY"},{"2", "INDIVIDUAL_LEG_OF_A_MULTI_LEG_SECURITY"},{"3", "MULTI_LEG_SECURITY"}]}
, "752" => #{"Name"=>"SideMultiLegReportingType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "SINGLE_SECURITY"},{"2", "INDIVIDUAL_LEG_OF_A_MULTI_LEG_SECURITY"},{"3", "MULTI_LEG_SECURITY"}], "TagNum" => "752"}


,
"NoPosAmt" => #{"TagNum" => "753" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "753" => #{"Name"=>"NoPosAmt" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "753"}


,
"AutoAcceptIndicator" => #{"TagNum" => "754" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "754" => #{"Name"=>"AutoAcceptIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "754"}


,
"AllocReportID" => #{"TagNum" => "755" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "755" => #{"Name"=>"AllocReportID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "755"}


,
"NoNested2PartyIDs" => #{"TagNum" => "756" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "756" => #{"Name"=>"NoNested2PartyIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "756"}


,
"Nested2PartyID" => #{"TagNum" => "757" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "757" => #{"Name"=>"Nested2PartyID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "757"}


,
"Nested2PartyIDSource" => #{"TagNum" => "758" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "758" => #{"Name"=>"Nested2PartyIDSource" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "758"}


,
"Nested2PartyRole" => #{"TagNum" => "759" ,"Type" => "INT" ,"ValidValues" =>[]}
, "759" => #{"Name"=>"Nested2PartyRole" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "759"}


,
"Nested2PartySubID" => #{"TagNum" => "760" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "760" => #{"Name"=>"Nested2PartySubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "760"}


,
"BenchmarkSecurityIDSource" => #{"TagNum" => "761" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "761" => #{"Name"=>"BenchmarkSecurityIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "761"}


,
"SecuritySubType" => #{"TagNum" => "762" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "762" => #{"Name"=>"SecuritySubType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "762"}


,
"UnderlyingSecuritySubType" => #{"TagNum" => "763" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "763" => #{"Name"=>"UnderlyingSecuritySubType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "763"}


,
"LegSecuritySubType" => #{"TagNum" => "764" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "764" => #{"Name"=>"LegSecuritySubType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "764"}


,
"AllowableOneSidednessPct" => #{"TagNum" => "765" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "765" => #{"Name"=>"AllowableOneSidednessPct" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "765"}


,
"AllowableOneSidednessValue" => #{"TagNum" => "766" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "766" => #{"Name"=>"AllowableOneSidednessValue" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "766"}


,
"AllowableOneSidednessCurr" => #{"TagNum" => "767" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "767" => #{"Name"=>"AllowableOneSidednessCurr" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "767"}


,
"NoTrdRegTimestamps" => #{"TagNum" => "768" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "768" => #{"Name"=>"NoTrdRegTimestamps" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "768"}


,
"TrdRegTimestamp" => #{"TagNum" => "769" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "769" => #{"Name"=>"TrdRegTimestamp" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "769"}


,
"TrdRegTimestampType" => #{"TagNum" => "770" ,"Type" => "INT" ,"ValidValues" =>[{"1", "EXECUTION_TIME"},{"2", "TIME_IN"},{"3", "TIME_OUT"},{"4", "BROKER_RECEIPT"},{"5", "BROKER_EXECUTION"}]}
, "770" => #{"Name"=>"TrdRegTimestampType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "EXECUTION_TIME"},{"2", "TIME_IN"},{"3", "TIME_OUT"},{"4", "BROKER_RECEIPT"},{"5", "BROKER_EXECUTION"}], "TagNum" => "770"}


,
"TrdRegTimestampOrigin" => #{"TagNum" => "771" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "771" => #{"Name"=>"TrdRegTimestampOrigin" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "771"}


,
"ConfirmRefID" => #{"TagNum" => "772" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "772" => #{"Name"=>"ConfirmRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "772"}


,
"ConfirmType" => #{"TagNum" => "773" ,"Type" => "INT" ,"ValidValues" =>[{"1", "STATUS"},{"2", "CONFIRMATION"},{"3", "CONFIRMATION_REQUEST_REJECTED"}]}
, "773" => #{"Name"=>"ConfirmType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "STATUS"},{"2", "CONFIRMATION"},{"3", "CONFIRMATION_REQUEST_REJECTED"}], "TagNum" => "773"}


,
"ConfirmRejReason" => #{"TagNum" => "774" ,"Type" => "INT" ,"ValidValues" =>[{"1", "MISMATCHED_ACCOUNT"},{"2", "MISSING_SETTLEMENT_INSTRUCTIONS"},{"99", "OTHER"}]}
, "774" => #{"Name"=>"ConfirmRejReason" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "MISMATCHED_ACCOUNT"},{"2", "MISSING_SETTLEMENT_INSTRUCTIONS"},{"99", "OTHER"}], "TagNum" => "774"}


,
"BookingType" => #{"TagNum" => "775" ,"Type" => "INT" ,"ValidValues" =>[{"0", "REGULAR_BOOKING"},{"1", "CFD"},{"2", "TOTAL_RETURN_SWAP"}]}
, "775" => #{"Name"=>"BookingType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "REGULAR_BOOKING"},{"1", "CFD"},{"2", "TOTAL_RETURN_SWAP"}], "TagNum" => "775"}


,
"IndividualAllocRejCode" => #{"TagNum" => "776" ,"Type" => "INT" ,"ValidValues" =>[]}
, "776" => #{"Name"=>"IndividualAllocRejCode" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "776"}


,
"SettlInstMsgID" => #{"TagNum" => "777" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "777" => #{"Name"=>"SettlInstMsgID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "777"}


,
"NoSettlInst" => #{"TagNum" => "778" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "778" => #{"Name"=>"NoSettlInst" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "778"}


,
"LastUpdateTime" => #{"TagNum" => "779" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "779" => #{"Name"=>"LastUpdateTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "779"}


,
"AllocSettlInstType" => #{"TagNum" => "780" ,"Type" => "INT" ,"ValidValues" =>[{"0", "USE_DEFAULT_INSTRUCTIONS"},{"1", "DERIVE_FROM_PARAMETERS_PROVIDED"},{"2", "FULL_DETAILS_PROVIDED"},{"3", "SSI_DB_IDS_PROVIDED"},{"4", "PHONE_FOR_INSTRUCTIONS"}]}
, "780" => #{"Name"=>"AllocSettlInstType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "USE_DEFAULT_INSTRUCTIONS"},{"1", "DERIVE_FROM_PARAMETERS_PROVIDED"},{"2", "FULL_DETAILS_PROVIDED"},{"3", "SSI_DB_IDS_PROVIDED"},{"4", "PHONE_FOR_INSTRUCTIONS"}], "TagNum" => "780"}


,
"NoSettlPartyIDs" => #{"TagNum" => "781" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "781" => #{"Name"=>"NoSettlPartyIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "781"}


,
"SettlPartyID" => #{"TagNum" => "782" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "782" => #{"Name"=>"SettlPartyID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "782"}


,
"SettlPartyIDSource" => #{"TagNum" => "783" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "783" => #{"Name"=>"SettlPartyIDSource" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "783"}


,
"SettlPartyRole" => #{"TagNum" => "784" ,"Type" => "INT" ,"ValidValues" =>[]}
, "784" => #{"Name"=>"SettlPartyRole" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "784"}


,
"SettlPartySubID" => #{"TagNum" => "785" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "785" => #{"Name"=>"SettlPartySubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "785"}


,
"SettlPartySubIDType" => #{"TagNum" => "786" ,"Type" => "INT" ,"ValidValues" =>[]}
, "786" => #{"Name"=>"SettlPartySubIDType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "786"}


,
"DlvyInstType" => #{"TagNum" => "787" ,"Type" => "CHAR" ,"ValidValues" =>[{"S", "SECURITIES"},{"C", "CASH"}]}
, "787" => #{"Name"=>"DlvyInstType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"S", "SECURITIES"},{"C", "CASH"}], "TagNum" => "787"}


,
"TerminationType" => #{"TagNum" => "788" ,"Type" => "INT" ,"ValidValues" =>[{"1", "OVERNIGHT"},{"2", "TERM"},{"3", "FLEXIBLE"},{"4", "OPEN"}]}
, "788" => #{"Name"=>"TerminationType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "OVERNIGHT"},{"2", "TERM"},{"3", "FLEXIBLE"},{"4", "OPEN"}], "TagNum" => "788"}


,
"NextExpectedMsgSeqNum" => #{"TagNum" => "789" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "789" => #{"Name"=>"NextExpectedMsgSeqNum" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "789"}


,
"OrdStatusReqID" => #{"TagNum" => "790" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "790" => #{"Name"=>"OrdStatusReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "790"}


,
"SettlInstReqID" => #{"TagNum" => "791" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "791" => #{"Name"=>"SettlInstReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "791"}


,
"SettlInstReqRejCode" => #{"TagNum" => "792" ,"Type" => "INT" ,"ValidValues" =>[{"0", "UNABLE_TO_PROCESS_REQUEST"},{"1", "UNKNOWN_ACCOUNT"},{"2", "NO_MATCHING_SETTLEMENT_INSTRUCTIONS_FOUND"},{"99", "OTHER"}]}
, "792" => #{"Name"=>"SettlInstReqRejCode" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "UNABLE_TO_PROCESS_REQUEST"},{"1", "UNKNOWN_ACCOUNT"},{"2", "NO_MATCHING_SETTLEMENT_INSTRUCTIONS_FOUND"},{"99", "OTHER"}], "TagNum" => "792"}


,
"SecondaryAllocID" => #{"TagNum" => "793" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "793" => #{"Name"=>"SecondaryAllocID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "793"}


,
"AllocReportType" => #{"TagNum" => "794" ,"Type" => "INT" ,"ValidValues" =>[{"3", "SELLSIDE_CALCULATED_USING_PRELIMINARY"},{"4", "SELLSIDE_CALCULATED_WITHOUT_PRELIMINARY"},{"5", "WAREHOUSE_RECAP"},{"8", "REQUEST_TO_INTERMEDIARY"}]}
, "794" => #{"Name"=>"AllocReportType" ,"Type"=>"INT" ,"ValidValues"=>[{"3", "SELLSIDE_CALCULATED_USING_PRELIMINARY"},{"4", "SELLSIDE_CALCULATED_WITHOUT_PRELIMINARY"},{"5", "WAREHOUSE_RECAP"},{"8", "REQUEST_TO_INTERMEDIARY"}], "TagNum" => "794"}


,
"AllocReportRefID" => #{"TagNum" => "795" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "795" => #{"Name"=>"AllocReportRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "795"}


,
"AllocCancReplaceReason" => #{"TagNum" => "796" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ORIGINAL_DETAILS_INCOMPLETE_INCORRECT"},{"2", "CHANGE_IN_UNDERLYING_ORDER_DETAILS"},{"99", "OTHER"}]}
, "796" => #{"Name"=>"AllocCancReplaceReason" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ORIGINAL_DETAILS_INCOMPLETE_INCORRECT"},{"2", "CHANGE_IN_UNDERLYING_ORDER_DETAILS"},{"99", "OTHER"}], "TagNum" => "796"}


,
"CopyMsgIndicator" => #{"TagNum" => "797" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "797" => #{"Name"=>"CopyMsgIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "797"}


,
"AllocAccountType" => #{"TagNum" => "798" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ACCOUNT_IS_CARRIED_ON_CUSTOMER_SIDE_OF_BOOKS"},{"2", "ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS"},{"3", "HOUSE_TRADER"},{"4", "FLOOR_TRADER"},{"6", "ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS_AND_IS_CROSS_MARGINED"},{"7", "ACCOUNT_IS_HOUSE_TRADER_AND_IS_CROSS_MARGINED"},{"8", "JOINT_BACKOFFICE_ACCOUNT"}]}
, "798" => #{"Name"=>"AllocAccountType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ACCOUNT_IS_CARRIED_ON_CUSTOMER_SIDE_OF_BOOKS"},{"2", "ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS"},{"3", "HOUSE_TRADER"},{"4", "FLOOR_TRADER"},{"6", "ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS_AND_IS_CROSS_MARGINED"},{"7", "ACCOUNT_IS_HOUSE_TRADER_AND_IS_CROSS_MARGINED"},{"8", "JOINT_BACKOFFICE_ACCOUNT"}], "TagNum" => "798"}


,
"OrderAvgPx" => #{"TagNum" => "799" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "799" => #{"Name"=>"OrderAvgPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "799"}


,
"OrderBookingQty" => #{"TagNum" => "800" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "800" => #{"Name"=>"OrderBookingQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "800"}


,
"NoSettlPartySubIDs" => #{"TagNum" => "801" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "801" => #{"Name"=>"NoSettlPartySubIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "801"}


,
"NoPartySubIDs" => #{"TagNum" => "802" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "802" => #{"Name"=>"NoPartySubIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "802"}


,
"PartySubIDType" => #{"TagNum" => "803" ,"Type" => "INT" ,"ValidValues" =>[{"1", "FIRM"},{"2", "PERSON"},{"3", "SYSTEM"},{"4", "APPLICATION"},{"5", "FULL_LEGAL_NAME_OF_FIRM"},{"6", "POSTAL_ADDRESS"},{"7", "PHONE_NUMBER"},{"8", "EMAIL_ADDRESS"},{"9", "CONTACT_NAME"},{"10", "SECURITIES_ACCOUNT_NUMBER"},{"11", "REGISTRATION_NUMBER"},{"12", "REGISTERED_ADDRESS_12"},{"13", "REGULATORY_STATUS"},{"14", "REGISTRATION_NAME"},{"15", "CASH_ACCOUNT_NUMBER"},{"16", "BIC"},{"17", "CSD_PARTICIPANT_MEMBER_CODE"},{"18", "REGISTERED_ADDRESS_18"},{"19", "FUND_ACCOUNT_NAME"},{"20", "TELEX_NUMBER"},{"21", "FAX_NUMBER"},{"22", "SECURITIES_ACCOUNT_NAME"},{"23", "CASH_ACCOUNT_NAME"},{"24", "DEPARTMENT"},{"25", "LOCATION"},{"26", "POSITION_ACCOUNT_TYPE"}]}
, "803" => #{"Name"=>"PartySubIDType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "FIRM"},{"2", "PERSON"},{"3", "SYSTEM"},{"4", "APPLICATION"},{"5", "FULL_LEGAL_NAME_OF_FIRM"},{"6", "POSTAL_ADDRESS"},{"7", "PHONE_NUMBER"},{"8", "EMAIL_ADDRESS"},{"9", "CONTACT_NAME"},{"10", "SECURITIES_ACCOUNT_NUMBER"},{"11", "REGISTRATION_NUMBER"},{"12", "REGISTERED_ADDRESS_12"},{"13", "REGULATORY_STATUS"},{"14", "REGISTRATION_NAME"},{"15", "CASH_ACCOUNT_NUMBER"},{"16", "BIC"},{"17", "CSD_PARTICIPANT_MEMBER_CODE"},{"18", "REGISTERED_ADDRESS_18"},{"19", "FUND_ACCOUNT_NAME"},{"20", "TELEX_NUMBER"},{"21", "FAX_NUMBER"},{"22", "SECURITIES_ACCOUNT_NAME"},{"23", "CASH_ACCOUNT_NAME"},{"24", "DEPARTMENT"},{"25", "LOCATION"},{"26", "POSITION_ACCOUNT_TYPE"}], "TagNum" => "803"}


,
"NoNestedPartySubIDs" => #{"TagNum" => "804" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "804" => #{"Name"=>"NoNestedPartySubIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "804"}


,
"NestedPartySubIDType" => #{"TagNum" => "805" ,"Type" => "INT" ,"ValidValues" =>[]}
, "805" => #{"Name"=>"NestedPartySubIDType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "805"}


,
"NoNested2PartySubIDs" => #{"TagNum" => "806" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "806" => #{"Name"=>"NoNested2PartySubIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "806"}


,
"Nested2PartySubIDType" => #{"TagNum" => "807" ,"Type" => "INT" ,"ValidValues" =>[]}
, "807" => #{"Name"=>"Nested2PartySubIDType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "807"}


,
"AllocIntermedReqType" => #{"TagNum" => "808" ,"Type" => "INT" ,"ValidValues" =>[{"1", "PENDING_ACCEPT"},{"2", "PENDING_RELEASE"},{"3", "PENDING_REVERSAL"},{"4", "ACCEPT"},{"5", "BLOCK_LEVEL_REJECT"},{"6", "ACCOUNT_LEVEL_REJECT"}]}
, "808" => #{"Name"=>"AllocIntermedReqType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "PENDING_ACCEPT"},{"2", "PENDING_RELEASE"},{"3", "PENDING_REVERSAL"},{"4", "ACCEPT"},{"5", "BLOCK_LEVEL_REJECT"},{"6", "ACCOUNT_LEVEL_REJECT"}], "TagNum" => "808"}


,
"UnderlyingPx" => #{"TagNum" => "810" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "810" => #{"Name"=>"UnderlyingPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "810"}


,
"PriceDelta" => #{"TagNum" => "811" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "811" => #{"Name"=>"PriceDelta" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "811"}


,
"ApplQueueMax" => #{"TagNum" => "812" ,"Type" => "INT" ,"ValidValues" =>[]}
, "812" => #{"Name"=>"ApplQueueMax" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "812"}


,
"ApplQueueDepth" => #{"TagNum" => "813" ,"Type" => "INT" ,"ValidValues" =>[]}
, "813" => #{"Name"=>"ApplQueueDepth" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "813"}


,
"ApplQueueResolution" => #{"TagNum" => "814" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NO_ACTION_TAKEN"},{"1", "QUEUE_FLUSHED"},{"2", "OVERLAY_LAST"},{"3", "END_SESSION"}]}
, "814" => #{"Name"=>"ApplQueueResolution" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NO_ACTION_TAKEN"},{"1", "QUEUE_FLUSHED"},{"2", "OVERLAY_LAST"},{"3", "END_SESSION"}], "TagNum" => "814"}


,
"ApplQueueAction" => #{"TagNum" => "815" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NO_ACTION_TAKEN"},{"1", "QUEUE_FLUSHED"},{"2", "OVERLAY_LAST"},{"3", "END_SESSION"}]}
, "815" => #{"Name"=>"ApplQueueAction" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NO_ACTION_TAKEN"},{"1", "QUEUE_FLUSHED"},{"2", "OVERLAY_LAST"},{"3", "END_SESSION"}], "TagNum" => "815"}


,
"NoAltMDSource" => #{"TagNum" => "816" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "816" => #{"Name"=>"NoAltMDSource" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "816"}


,
"AltMDSourceID" => #{"TagNum" => "817" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "817" => #{"Name"=>"AltMDSourceID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "817"}


,
"SecondaryTradeReportID" => #{"TagNum" => "818" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "818" => #{"Name"=>"SecondaryTradeReportID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "818"}


,
"AvgPxIndicator" => #{"TagNum" => "819" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NO_AVERAGE_PRICING"},{"1", "TRADE_IS_PART_OF_AN_AVERAGE_PRICE_GROUP_IDENTIFIED_BY_THE_TRADELINKID"},{"2", "LAST_TRADE_IN_THE_AVERAGE_PRICE_GROUP_IDENTIFIED_BY_THE_TRADELINKID"}]}
, "819" => #{"Name"=>"AvgPxIndicator" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NO_AVERAGE_PRICING"},{"1", "TRADE_IS_PART_OF_AN_AVERAGE_PRICE_GROUP_IDENTIFIED_BY_THE_TRADELINKID"},{"2", "LAST_TRADE_IN_THE_AVERAGE_PRICE_GROUP_IDENTIFIED_BY_THE_TRADELINKID"}], "TagNum" => "819"}


,
"TradeLinkID" => #{"TagNum" => "820" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "820" => #{"Name"=>"TradeLinkID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "820"}


,
"OrderInputDevice" => #{"TagNum" => "821" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "821" => #{"Name"=>"OrderInputDevice" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "821"}


,
"UnderlyingTradingSessionID" => #{"TagNum" => "822" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "822" => #{"Name"=>"UnderlyingTradingSessionID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "822"}


,
"UnderlyingTradingSessionSubID" => #{"TagNum" => "823" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "823" => #{"Name"=>"UnderlyingTradingSessionSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "823"}


,
"TradeLegRefID" => #{"TagNum" => "824" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "824" => #{"Name"=>"TradeLegRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "824"}


,
"ExchangeRule" => #{"TagNum" => "825" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "825" => #{"Name"=>"ExchangeRule" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "825"}


,
"TradeAllocIndicator" => #{"TagNum" => "826" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ALLOCATION_NOT_REQUIRED"},{"1", "ALLOCATION_REQUIRED"},{"2", "USE_ALLOCATION_PROVIDED_WITH_THE_TRADE"}]}
, "826" => #{"Name"=>"TradeAllocIndicator" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ALLOCATION_NOT_REQUIRED"},{"1", "ALLOCATION_REQUIRED"},{"2", "USE_ALLOCATION_PROVIDED_WITH_THE_TRADE"}], "TagNum" => "826"}


,
"ExpirationCycle" => #{"TagNum" => "827" ,"Type" => "INT" ,"ValidValues" =>[{"0", "EXPIRE_ON_TRADING_SESSION_CLOSE"},{"1", "EXPIRE_ON_TRADING_SESSION_OPEN"}]}
, "827" => #{"Name"=>"ExpirationCycle" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "EXPIRE_ON_TRADING_SESSION_CLOSE"},{"1", "EXPIRE_ON_TRADING_SESSION_OPEN"}], "TagNum" => "827"}


,
"TrdType" => #{"TagNum" => "828" ,"Type" => "INT" ,"ValidValues" =>[{"0", "REGULAR_TRADE"},{"1", "BLOCK_TRADE"},{"2", "EFP"},{"3", "TRANSFER"},{"4", "LATE_TRADE"},{"5", "T_TRADE"},{"6", "WEIGHTED_AVERAGE_PRICE_TRADE"},{"7", "BUNCHED_TRADE"},{"8", "LATE_BUNCHED_TRADE"},{"9", "PRIOR_REFERENCE_PRICE_TRADE"},{"10", "AFTER_HOURS_TRADE"}]}
, "828" => #{"Name"=>"TrdType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "REGULAR_TRADE"},{"1", "BLOCK_TRADE"},{"2", "EFP"},{"3", "TRANSFER"},{"4", "LATE_TRADE"},{"5", "T_TRADE"},{"6", "WEIGHTED_AVERAGE_PRICE_TRADE"},{"7", "BUNCHED_TRADE"},{"8", "LATE_BUNCHED_TRADE"},{"9", "PRIOR_REFERENCE_PRICE_TRADE"},{"10", "AFTER_HOURS_TRADE"}], "TagNum" => "828"}


,
"TrdSubType" => #{"TagNum" => "829" ,"Type" => "INT" ,"ValidValues" =>[]}
, "829" => #{"Name"=>"TrdSubType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "829"}


,
"TransferReason" => #{"TagNum" => "830" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "830" => #{"Name"=>"TransferReason" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "830"}


,
"TotNumAssignmentReports" => #{"TagNum" => "832" ,"Type" => "INT" ,"ValidValues" =>[]}
, "832" => #{"Name"=>"TotNumAssignmentReports" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "832"}


,
"AsgnRptID" => #{"TagNum" => "833" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "833" => #{"Name"=>"AsgnRptID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "833"}


,
"ThresholdAmount" => #{"TagNum" => "834" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "834" => #{"Name"=>"ThresholdAmount" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "834"}


,
"PegMoveType" => #{"TagNum" => "835" ,"Type" => "INT" ,"ValidValues" =>[{"0", "FLOATING"},{"1", "FIXED"}]}
, "835" => #{"Name"=>"PegMoveType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "FLOATING"},{"1", "FIXED"}], "TagNum" => "835"}


,
"PegOffsetType" => #{"TagNum" => "836" ,"Type" => "INT" ,"ValidValues" =>[{"0", "PRICE"},{"1", "BASIS_POINTS"},{"2", "TICKS"},{"3", "PRICE_TIER"}]}
, "836" => #{"Name"=>"PegOffsetType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "PRICE"},{"1", "BASIS_POINTS"},{"2", "TICKS"},{"3", "PRICE_TIER"}], "TagNum" => "836"}


,
"PegLimitType" => #{"TagNum" => "837" ,"Type" => "INT" ,"ValidValues" =>[{"0", "OR_BETTER"},{"1", "STRICT_LIMIT_IS_A_STRICT_LIMIT"},{"2", "OR_WORSE_FOR_A_BUY_THE_PEG_LIMIT_IS_A_MINIMUM_AND_FOR_A_SELL_THE_PEG_LIMIT_IS_A_MAXIMUM"}]}
, "837" => #{"Name"=>"PegLimitType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "OR_BETTER"},{"1", "STRICT_LIMIT_IS_A_STRICT_LIMIT"},{"2", "OR_WORSE_FOR_A_BUY_THE_PEG_LIMIT_IS_A_MINIMUM_AND_FOR_A_SELL_THE_PEG_LIMIT_IS_A_MAXIMUM"}], "TagNum" => "837"}


,
"PegRoundDirection" => #{"TagNum" => "838" ,"Type" => "INT" ,"ValidValues" =>[{"1", "MORE_AGGRESSIVE_ON_A_BUY_ORDER_ROUND_THE_PRICE_UP_ROUND_UP_TO_THE_NEAREST_TICK_ON_A_SELL_ROUND_DOWN_TO_THE_NEAREST_TICK"},{"2", "MORE_PASSIVE_ON_A_BUY_ORDER_ROUND_DOWN_TO_NEAREST_TICK_ON_A_SELL_ORDER_ROUND_UP_TO_NEAREST_TICK"}]}
, "838" => #{"Name"=>"PegRoundDirection" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "MORE_AGGRESSIVE_ON_A_BUY_ORDER_ROUND_THE_PRICE_UP_ROUND_UP_TO_THE_NEAREST_TICK_ON_A_SELL_ROUND_DOWN_TO_THE_NEAREST_TICK"},{"2", "MORE_PASSIVE_ON_A_BUY_ORDER_ROUND_DOWN_TO_NEAREST_TICK_ON_A_SELL_ORDER_ROUND_UP_TO_NEAREST_TICK"}], "TagNum" => "838"}


,
"PeggedPrice" => #{"TagNum" => "839" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "839" => #{"Name"=>"PeggedPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "839"}


,
"PegScope" => #{"TagNum" => "840" ,"Type" => "INT" ,"ValidValues" =>[{"1", "LOCAL"},{"2", "NATIONAL"},{"3", "GLOBAL"},{"4", "NATIONAL_EXCLUDING_LOCAL"}]}
, "840" => #{"Name"=>"PegScope" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "LOCAL"},{"2", "NATIONAL"},{"3", "GLOBAL"},{"4", "NATIONAL_EXCLUDING_LOCAL"}], "TagNum" => "840"}


,
"DiscretionMoveType" => #{"TagNum" => "841" ,"Type" => "INT" ,"ValidValues" =>[{"0", "FLOATING"},{"1", "FIXED"}]}
, "841" => #{"Name"=>"DiscretionMoveType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "FLOATING"},{"1", "FIXED"}], "TagNum" => "841"}


,
"DiscretionOffsetType" => #{"TagNum" => "842" ,"Type" => "INT" ,"ValidValues" =>[{"0", "PRICE"},{"1", "BASIS_POINTS"},{"2", "TICKS"},{"3", "PRICE_TIER"}]}
, "842" => #{"Name"=>"DiscretionOffsetType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "PRICE"},{"1", "BASIS_POINTS"},{"2", "TICKS"},{"3", "PRICE_TIER"}], "TagNum" => "842"}


,
"DiscretionLimitType" => #{"TagNum" => "843" ,"Type" => "INT" ,"ValidValues" =>[{"0", "OR_BETTER"},{"1", "STRICT_LIMIT_IS_A_STRICT_LIMIT"},{"2", "OR_WORSE_FOR_A_BUY_THE_DISCRETION_PRICE_IS_A_MINIMUM_AND_FOR_A_SELL_THE_DISCRETION_PRICE_IS_A_MAXIMUM"}]}
, "843" => #{"Name"=>"DiscretionLimitType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "OR_BETTER"},{"1", "STRICT_LIMIT_IS_A_STRICT_LIMIT"},{"2", "OR_WORSE_FOR_A_BUY_THE_DISCRETION_PRICE_IS_A_MINIMUM_AND_FOR_A_SELL_THE_DISCRETION_PRICE_IS_A_MAXIMUM"}], "TagNum" => "843"}


,
"DiscretionRoundDirection" => #{"TagNum" => "844" ,"Type" => "INT" ,"ValidValues" =>[{"1", "MORE_AGGRESSIVE_ON_A_BUY_ORDER_ROUND_THE_PRICE_UP_ROUND_UP_TO_THE_NEAREST_TICK_ON_A_SELL_ROUND_DOWN_TO_THE_NEAREST_TICK"},{"2", "MORE_PASSIVE_ON_A_BUY_ORDER_ROUND_DOWN_TO_NEAREST_TICK_ON_A_SELL_ORDER_ROUND_UP_TO_NEAREST_TICK"}]}
, "844" => #{"Name"=>"DiscretionRoundDirection" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "MORE_AGGRESSIVE_ON_A_BUY_ORDER_ROUND_THE_PRICE_UP_ROUND_UP_TO_THE_NEAREST_TICK_ON_A_SELL_ROUND_DOWN_TO_THE_NEAREST_TICK"},{"2", "MORE_PASSIVE_ON_A_BUY_ORDER_ROUND_DOWN_TO_NEAREST_TICK_ON_A_SELL_ORDER_ROUND_UP_TO_NEAREST_TICK"}], "TagNum" => "844"}


,
"DiscretionPrice" => #{"TagNum" => "845" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "845" => #{"Name"=>"DiscretionPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "845"}


,
"DiscretionScope" => #{"TagNum" => "846" ,"Type" => "INT" ,"ValidValues" =>[{"1", "LOCAL"},{"2", "NATIONAL"},{"3", "GLOBAL"},{"4", "NATIONAL_EXCLUDING_LOCAL"}]}
, "846" => #{"Name"=>"DiscretionScope" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "LOCAL"},{"2", "NATIONAL"},{"3", "GLOBAL"},{"4", "NATIONAL_EXCLUDING_LOCAL"}], "TagNum" => "846"}


,
"TargetStrategy" => #{"TagNum" => "847" ,"Type" => "INT" ,"ValidValues" =>[{"1", "VWAP"},{"2", "PARTICIPATE"},{"3", "MININIZE_MARKET_IMPACT"}]}
, "847" => #{"Name"=>"TargetStrategy" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "VWAP"},{"2", "PARTICIPATE"},{"3", "MININIZE_MARKET_IMPACT"}], "TagNum" => "847"}


,
"TargetStrategyParameters" => #{"TagNum" => "848" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "848" => #{"Name"=>"TargetStrategyParameters" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "848"}


,
"ParticipationRate" => #{"TagNum" => "849" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "849" => #{"Name"=>"ParticipationRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "849"}


,
"TargetStrategyPerformance" => #{"TagNum" => "850" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "850" => #{"Name"=>"TargetStrategyPerformance" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "850"}


,
"LastLiquidityInd" => #{"TagNum" => "851" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ADDED_LIQUIDITY"},{"2", "REMOVED_LIQUIDITY"},{"3", "LIQUIDITY_ROUTED_OUT"}]}
, "851" => #{"Name"=>"LastLiquidityInd" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ADDED_LIQUIDITY"},{"2", "REMOVED_LIQUIDITY"},{"3", "LIQUIDITY_ROUTED_OUT"}], "TagNum" => "851"}


,
"PublishTrdIndicator" => #{"TagNum" => "852" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "852" => #{"Name"=>"PublishTrdIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "852"}


,
"ShortSaleReason" => #{"TagNum" => "853" ,"Type" => "INT" ,"ValidValues" =>[{"0", "DEALER_SOLD_SHORT"},{"1", "DEALER_SOLD_SHORT_EXEMPT"},{"2", "SELLING_CUSTOMER_SOLD_SHORT"},{"3", "SELLING_CUSTOMER_SOLD_SHORT_EXEMPT"},{"4", "QUALIFED_SERVICE_REPRESENTATIVE"},{"5", "QSR_OR_AGU_CONTRA_SIDE_SOLD_SHORT_EXEMPT"}]}
, "853" => #{"Name"=>"ShortSaleReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "DEALER_SOLD_SHORT"},{"1", "DEALER_SOLD_SHORT_EXEMPT"},{"2", "SELLING_CUSTOMER_SOLD_SHORT"},{"3", "SELLING_CUSTOMER_SOLD_SHORT_EXEMPT"},{"4", "QUALIFED_SERVICE_REPRESENTATIVE"},{"5", "QSR_OR_AGU_CONTRA_SIDE_SOLD_SHORT_EXEMPT"}], "TagNum" => "853"}


,
"QtyType" => #{"TagNum" => "854" ,"Type" => "INT" ,"ValidValues" =>[{"0", "UNITS"},{"1", "CONTRACTS"}]}
, "854" => #{"Name"=>"QtyType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "UNITS"},{"1", "CONTRACTS"}], "TagNum" => "854"}


,
"SecondaryTrdType" => #{"TagNum" => "855" ,"Type" => "INT" ,"ValidValues" =>[]}
, "855" => #{"Name"=>"SecondaryTrdType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "855"}


,
"TradeReportType" => #{"TagNum" => "856" ,"Type" => "INT" ,"ValidValues" =>[{"0", "SUBMIT"},{"1", "ALLEGED"},{"2", "ACCEPT"},{"3", "DECLINE"},{"4", "ADDENDUM"},{"5", "NO_WAS"},{"6", "TRADE_REPORT_CANCEL"},{"7", "LOCKED_IN_TRADE_BREAK"}]}
, "856" => #{"Name"=>"TradeReportType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "SUBMIT"},{"1", "ALLEGED"},{"2", "ACCEPT"},{"3", "DECLINE"},{"4", "ADDENDUM"},{"5", "NO_WAS"},{"6", "TRADE_REPORT_CANCEL"},{"7", "LOCKED_IN_TRADE_BREAK"}], "TagNum" => "856"}


,
"AllocNoOrdersType" => #{"TagNum" => "857" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NOT_SPECIFIED"},{"1", "EXPLICIT_LIST_PROVIDED"}]}
, "857" => #{"Name"=>"AllocNoOrdersType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NOT_SPECIFIED"},{"1", "EXPLICIT_LIST_PROVIDED"}], "TagNum" => "857"}


,
"SharedCommission" => #{"TagNum" => "858" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "858" => #{"Name"=>"SharedCommission" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "858"}


,
"ConfirmReqID" => #{"TagNum" => "859" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "859" => #{"Name"=>"ConfirmReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "859"}


,
"AvgParPx" => #{"TagNum" => "860" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "860" => #{"Name"=>"AvgParPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "860"}


,
"ReportedPx" => #{"TagNum" => "861" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "861" => #{"Name"=>"ReportedPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "861"}


,
"NoCapacities" => #{"TagNum" => "862" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "862" => #{"Name"=>"NoCapacities" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "862"}


,
"OrderCapacityQty" => #{"TagNum" => "863" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "863" => #{"Name"=>"OrderCapacityQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "863"}


,
"NoEvents" => #{"TagNum" => "864" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "864" => #{"Name"=>"NoEvents" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "864"}


,
"EventType" => #{"TagNum" => "865" ,"Type" => "INT" ,"ValidValues" =>[{"1", "PUT"},{"2", "CALL"},{"3", "TENDER"},{"4", "SINKING_FUND_CALL"},{"99", "OTHER"}]}
, "865" => #{"Name"=>"EventType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "PUT"},{"2", "CALL"},{"3", "TENDER"},{"4", "SINKING_FUND_CALL"},{"99", "OTHER"}], "TagNum" => "865"}


,
"EventDate" => #{"TagNum" => "866" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "866" => #{"Name"=>"EventDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "866"}


,
"EventPx" => #{"TagNum" => "867" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "867" => #{"Name"=>"EventPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "867"}


,
"EventText" => #{"TagNum" => "868" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "868" => #{"Name"=>"EventText" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "868"}


,
"PctAtRisk" => #{"TagNum" => "869" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "869" => #{"Name"=>"PctAtRisk" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "869"}


,
"NoInstrAttrib" => #{"TagNum" => "870" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "870" => #{"Name"=>"NoInstrAttrib" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "870"}


,
"InstrAttribType" => #{"TagNum" => "871" ,"Type" => "INT" ,"ValidValues" =>[{"1", "FLAT"},{"2", "ZERO_COUPON"},{"3", "INTEREST_BEARING"},{"4", "NO_PERIODIC_PAYMENTS"},{"5", "VARIABLE_RATE"},{"6", "LESS_FEE_FOR_PUT"},{"7", "STEPPED_COUPON"},{"8", "COUPON_PERIOD"},{"9", "WHEN_AND_IF_ISSUED"},{"10", "ORIGINAL_ISSUE_DISCOUNT"},{"11", "CALLABLE_PUTTABLE"},{"12", "ESCROWED_TO_MATURITY"},{"13", "ESCROWED_TO_REDEMPTION_DATE_CALLABLE_SUPPLY_REDEMPTION_DATE_IN_THE_INSTRATTRIBVALUE"},{"14", "PREREFUNDED"},{"15", "IN_DEFAULT"},{"16", "UNRATED"},{"17", "TAXABLE"},{"18", "INDEXED"},{"19", "SUBJECT_TO_ALTERNATIVE_MINIMUM_TAX"},{"20", "ORIGINAL_ISSUE_DISCOUNT_PRICE_SUPPLY_PRICE_IN_THE_INSTRATTRIBVALUE"},{"21", "CALLABLE_BELOW_MATURITY_VALUE"},{"22", "CALLABLE_WITHOUT_NOTICE_BY_MAIL_TO_HOLDER_UNLESS_REGISTERED"},{"99", "TEXT_SUPPLY_THE_TEXT_OF_THE_ATTRIBUTE_OR_DISCLAIMER_IN_THE_INSTRATTRIBVALUE"}]}
, "871" => #{"Name"=>"InstrAttribType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "FLAT"},{"2", "ZERO_COUPON"},{"3", "INTEREST_BEARING"},{"4", "NO_PERIODIC_PAYMENTS"},{"5", "VARIABLE_RATE"},{"6", "LESS_FEE_FOR_PUT"},{"7", "STEPPED_COUPON"},{"8", "COUPON_PERIOD"},{"9", "WHEN_AND_IF_ISSUED"},{"10", "ORIGINAL_ISSUE_DISCOUNT"},{"11", "CALLABLE_PUTTABLE"},{"12", "ESCROWED_TO_MATURITY"},{"13", "ESCROWED_TO_REDEMPTION_DATE_CALLABLE_SUPPLY_REDEMPTION_DATE_IN_THE_INSTRATTRIBVALUE"},{"14", "PREREFUNDED"},{"15", "IN_DEFAULT"},{"16", "UNRATED"},{"17", "TAXABLE"},{"18", "INDEXED"},{"19", "SUBJECT_TO_ALTERNATIVE_MINIMUM_TAX"},{"20", "ORIGINAL_ISSUE_DISCOUNT_PRICE_SUPPLY_PRICE_IN_THE_INSTRATTRIBVALUE"},{"21", "CALLABLE_BELOW_MATURITY_VALUE"},{"22", "CALLABLE_WITHOUT_NOTICE_BY_MAIL_TO_HOLDER_UNLESS_REGISTERED"},{"99", "TEXT_SUPPLY_THE_TEXT_OF_THE_ATTRIBUTE_OR_DISCLAIMER_IN_THE_INSTRATTRIBVALUE"}], "TagNum" => "871"}


,
"InstrAttribValue" => #{"TagNum" => "872" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "872" => #{"Name"=>"InstrAttribValue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "872"}


,
"DatedDate" => #{"TagNum" => "873" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "873" => #{"Name"=>"DatedDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "873"}


,
"InterestAccrualDate" => #{"TagNum" => "874" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "874" => #{"Name"=>"InterestAccrualDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "874"}


,
"CPProgram" => #{"TagNum" => "875" ,"Type" => "INT" ,"ValidValues" =>[{"1", "3"},{"2", "4"},{"99", "OTHER"}]}
, "875" => #{"Name"=>"CPProgram" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "3"},{"2", "4"},{"99", "OTHER"}], "TagNum" => "875"}


,
"CPRegType" => #{"TagNum" => "876" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "876" => #{"Name"=>"CPRegType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "876"}


,
"UnderlyingCPProgram" => #{"TagNum" => "877" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "877" => #{"Name"=>"UnderlyingCPProgram" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "877"}


,
"UnderlyingCPRegType" => #{"TagNum" => "878" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "878" => #{"Name"=>"UnderlyingCPRegType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "878"}


,
"UnderlyingQty" => #{"TagNum" => "879" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "879" => #{"Name"=>"UnderlyingQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "879"}


,
"TrdMatchID" => #{"TagNum" => "880" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "880" => #{"Name"=>"TrdMatchID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "880"}


,
"SecondaryTradeReportRefID" => #{"TagNum" => "881" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "881" => #{"Name"=>"SecondaryTradeReportRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "881"}


,
"UnderlyingDirtyPrice" => #{"TagNum" => "882" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "882" => #{"Name"=>"UnderlyingDirtyPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "882"}


,
"UnderlyingEndPrice" => #{"TagNum" => "883" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "883" => #{"Name"=>"UnderlyingEndPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "883"}


,
"UnderlyingStartValue" => #{"TagNum" => "884" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "884" => #{"Name"=>"UnderlyingStartValue" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "884"}


,
"UnderlyingCurrentValue" => #{"TagNum" => "885" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "885" => #{"Name"=>"UnderlyingCurrentValue" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "885"}


,
"UnderlyingEndValue" => #{"TagNum" => "886" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "886" => #{"Name"=>"UnderlyingEndValue" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "886"}


,
"NoUnderlyingStips" => #{"TagNum" => "887" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "887" => #{"Name"=>"NoUnderlyingStips" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "887"}


,
"UnderlyingStipType" => #{"TagNum" => "888" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "888" => #{"Name"=>"UnderlyingStipType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "888"}


,
"UnderlyingStipValue" => #{"TagNum" => "889" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "889" => #{"Name"=>"UnderlyingStipValue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "889"}


,
"MaturityNetMoney" => #{"TagNum" => "890" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "890" => #{"Name"=>"MaturityNetMoney" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "890"}


,
"MiscFeeBasis" => #{"TagNum" => "891" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ABSOLUTE"},{"1", "PER_UNIT"},{"2", "PERCENTAGE"}]}
, "891" => #{"Name"=>"MiscFeeBasis" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ABSOLUTE"},{"1", "PER_UNIT"},{"2", "PERCENTAGE"}], "TagNum" => "891"}


,
"TotNoAllocs" => #{"TagNum" => "892" ,"Type" => "INT" ,"ValidValues" =>[]}
, "892" => #{"Name"=>"TotNoAllocs" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "892"}


,
"LastFragment" => #{"TagNum" => "893" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "893" => #{"Name"=>"LastFragment" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "893"}


,
"CollReqID" => #{"TagNum" => "894" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "894" => #{"Name"=>"CollReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "894"}


,
"CollAsgnReason" => #{"TagNum" => "895" ,"Type" => "INT" ,"ValidValues" =>[{"0", "INITIAL"},{"1", "SCHEDULED"},{"2", "TIME_WARNING"},{"3", "MARGIN_DEFICIENCY"},{"4", "MARGIN_EXCESS"},{"5", "FORWARD_COLLATERAL_DEMAND"},{"6", "EVENT_OF_DEFAULT"},{"7", "ADVERSE_TAX_EVENT"}]}
, "895" => #{"Name"=>"CollAsgnReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "INITIAL"},{"1", "SCHEDULED"},{"2", "TIME_WARNING"},{"3", "MARGIN_DEFICIENCY"},{"4", "MARGIN_EXCESS"},{"5", "FORWARD_COLLATERAL_DEMAND"},{"6", "EVENT_OF_DEFAULT"},{"7", "ADVERSE_TAX_EVENT"}], "TagNum" => "895"}


,
"CollInquiryQualifier" => #{"TagNum" => "896" ,"Type" => "INT" ,"ValidValues" =>[{"0", "TRADEDATE"},{"1", "GC_INSTRUMENT"},{"2", "COLLATERALINSTRUMENT"},{"3", "SUBSTITUTION_ELIGIBLE"},{"4", "NOT_ASSIGNED"},{"5", "PARTIALLY_ASSIGNED"},{"6", "FULLY_ASSIGNED"},{"7", "OUTSTANDING_TRADES"}]}
, "896" => #{"Name"=>"CollInquiryQualifier" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "TRADEDATE"},{"1", "GC_INSTRUMENT"},{"2", "COLLATERALINSTRUMENT"},{"3", "SUBSTITUTION_ELIGIBLE"},{"4", "NOT_ASSIGNED"},{"5", "PARTIALLY_ASSIGNED"},{"6", "FULLY_ASSIGNED"},{"7", "OUTSTANDING_TRADES"}], "TagNum" => "896"}


,
"NoTrades" => #{"TagNum" => "897" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "897" => #{"Name"=>"NoTrades" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "897"}


,
"MarginRatio" => #{"TagNum" => "898" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "898" => #{"Name"=>"MarginRatio" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "898"}


,
"MarginExcess" => #{"TagNum" => "899" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "899" => #{"Name"=>"MarginExcess" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "899"}


,
"TotalNetValue" => #{"TagNum" => "900" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "900" => #{"Name"=>"TotalNetValue" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "900"}


,
"CashOutstanding" => #{"TagNum" => "901" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "901" => #{"Name"=>"CashOutstanding" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "901"}


,
"CollAsgnID" => #{"TagNum" => "902" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "902" => #{"Name"=>"CollAsgnID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "902"}


,
"CollAsgnTransType" => #{"TagNum" => "903" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NEW"},{"1", "REPLACE"},{"2", "CANCEL"},{"3", "RELEASE"},{"4", "REVERSE"}]}
, "903" => #{"Name"=>"CollAsgnTransType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NEW"},{"1", "REPLACE"},{"2", "CANCEL"},{"3", "RELEASE"},{"4", "REVERSE"}], "TagNum" => "903"}


,
"CollRespID" => #{"TagNum" => "904" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "904" => #{"Name"=>"CollRespID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "904"}


,
"CollAsgnRespType" => #{"TagNum" => "905" ,"Type" => "INT" ,"ValidValues" =>[{"0", "RECEIVED"},{"1", "ACCEPTED"},{"2", "DECLINED"},{"3", "REJECTED"}]}
, "905" => #{"Name"=>"CollAsgnRespType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "RECEIVED"},{"1", "ACCEPTED"},{"2", "DECLINED"},{"3", "REJECTED"}], "TagNum" => "905"}


,
"CollAsgnRejectReason" => #{"TagNum" => "906" ,"Type" => "INT" ,"ValidValues" =>[{"0", "UNKNOWN_DEAL"},{"1", "UNKNOWN_OR_INVALID_INSTRUMENT"},{"2", "UNAUTHORIZED_TRANSACTION"},{"3", "INSUFFICIENT_COLLATERAL"},{"4", "INVALID_TYPE_OF_COLLATERAL"},{"5", "EXCESSIVE_SUBSTITUTION"},{"99", "OTHER"}]}
, "906" => #{"Name"=>"CollAsgnRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "UNKNOWN_DEAL"},{"1", "UNKNOWN_OR_INVALID_INSTRUMENT"},{"2", "UNAUTHORIZED_TRANSACTION"},{"3", "INSUFFICIENT_COLLATERAL"},{"4", "INVALID_TYPE_OF_COLLATERAL"},{"5", "EXCESSIVE_SUBSTITUTION"},{"99", "OTHER"}], "TagNum" => "906"}


,
"CollAsgnRefID" => #{"TagNum" => "907" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "907" => #{"Name"=>"CollAsgnRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "907"}


,
"CollRptID" => #{"TagNum" => "908" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "908" => #{"Name"=>"CollRptID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "908"}


,
"CollInquiryID" => #{"TagNum" => "909" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "909" => #{"Name"=>"CollInquiryID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "909"}


,
"CollStatus" => #{"TagNum" => "910" ,"Type" => "INT" ,"ValidValues" =>[{"0", "UNASSIGNED"},{"1", "PARTIALLY_ASSIGNED"},{"2", "ASSIGNMENT_PROPOSED"},{"3", "ASSIGNED"},{"4", "CHALLENGED"}]}
, "910" => #{"Name"=>"CollStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "UNASSIGNED"},{"1", "PARTIALLY_ASSIGNED"},{"2", "ASSIGNMENT_PROPOSED"},{"3", "ASSIGNED"},{"4", "CHALLENGED"}], "TagNum" => "910"}


,
"TotNumReports" => #{"TagNum" => "911" ,"Type" => "INT" ,"ValidValues" =>[]}
, "911" => #{"Name"=>"TotNumReports" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "911"}


,
"LastRptRequested" => #{"TagNum" => "912" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "912" => #{"Name"=>"LastRptRequested" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "912"}


,
"AgreementDesc" => #{"TagNum" => "913" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "913" => #{"Name"=>"AgreementDesc" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "913"}


,
"AgreementID" => #{"TagNum" => "914" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "914" => #{"Name"=>"AgreementID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "914"}


,
"AgreementDate" => #{"TagNum" => "915" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "915" => #{"Name"=>"AgreementDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "915"}


,
"StartDate" => #{"TagNum" => "916" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "916" => #{"Name"=>"StartDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "916"}


,
"EndDate" => #{"TagNum" => "917" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "917" => #{"Name"=>"EndDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "917"}


,
"AgreementCurrency" => #{"TagNum" => "918" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "918" => #{"Name"=>"AgreementCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "918"}


,
"DeliveryType" => #{"TagNum" => "919" ,"Type" => "INT" ,"ValidValues" =>[{"0", "VERSUS_PAYMENT_DELIVER"},{"1", "FREE_DELIVER"},{"2", "TRI_PARTY"},{"3", "HOLD_IN_CUSTODY"}]}
, "919" => #{"Name"=>"DeliveryType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "VERSUS_PAYMENT_DELIVER"},{"1", "FREE_DELIVER"},{"2", "TRI_PARTY"},{"3", "HOLD_IN_CUSTODY"}], "TagNum" => "919"}


,
"EndAccruedInterestAmt" => #{"TagNum" => "920" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "920" => #{"Name"=>"EndAccruedInterestAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "920"}


,
"StartCash" => #{"TagNum" => "921" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "921" => #{"Name"=>"StartCash" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "921"}


,
"EndCash" => #{"TagNum" => "922" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "922" => #{"Name"=>"EndCash" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "922"}


,
"UserRequestID" => #{"TagNum" => "923" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "923" => #{"Name"=>"UserRequestID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "923"}


,
"UserRequestType" => #{"TagNum" => "924" ,"Type" => "INT" ,"ValidValues" =>[{"1", "LOGONUSER"},{"2", "LOGOFFUSER"},{"3", "CHANGEPASSWORDFORUSER"},{"4", "REQUEST_INDIVIDUAL_USER_STATUS"}]}
, "924" => #{"Name"=>"UserRequestType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "LOGONUSER"},{"2", "LOGOFFUSER"},{"3", "CHANGEPASSWORDFORUSER"},{"4", "REQUEST_INDIVIDUAL_USER_STATUS"}], "TagNum" => "924"}


,
"NewPassword" => #{"TagNum" => "925" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "925" => #{"Name"=>"NewPassword" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "925"}


,
"UserStatus" => #{"TagNum" => "926" ,"Type" => "INT" ,"ValidValues" =>[{"1", "LOGGED_IN"},{"2", "NOT_LOGGED_IN"},{"3", "USER_NOT_RECOGNISED"},{"4", "PASSWORD_INCORRECT"},{"5", "PASSWORD_CHANGED"},{"6", "OTHER"}]}
, "926" => #{"Name"=>"UserStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "LOGGED_IN"},{"2", "NOT_LOGGED_IN"},{"3", "USER_NOT_RECOGNISED"},{"4", "PASSWORD_INCORRECT"},{"5", "PASSWORD_CHANGED"},{"6", "OTHER"}], "TagNum" => "926"}


,
"UserStatusText" => #{"TagNum" => "927" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "927" => #{"Name"=>"UserStatusText" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "927"}


,
"StatusValue" => #{"TagNum" => "928" ,"Type" => "INT" ,"ValidValues" =>[{"1", "CONNECTED"},{"2", "NOT_CONNECTED_DOWN_EXPECTED_UP"},{"3", "NOT_CONNECTED_DOWN_EXPECTED_DOWN"},{"4", "IN_PROCESS"}]}
, "928" => #{"Name"=>"StatusValue" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "CONNECTED"},{"2", "NOT_CONNECTED_DOWN_EXPECTED_UP"},{"3", "NOT_CONNECTED_DOWN_EXPECTED_DOWN"},{"4", "IN_PROCESS"}], "TagNum" => "928"}


,
"StatusText" => #{"TagNum" => "929" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "929" => #{"Name"=>"StatusText" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "929"}


,
"RefCompID" => #{"TagNum" => "930" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "930" => #{"Name"=>"RefCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "930"}


,
"RefSubID" => #{"TagNum" => "931" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "931" => #{"Name"=>"RefSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "931"}


,
"NetworkResponseID" => #{"TagNum" => "932" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "932" => #{"Name"=>"NetworkResponseID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "932"}


,
"NetworkRequestID" => #{"TagNum" => "933" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "933" => #{"Name"=>"NetworkRequestID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "933"}


,
"LastNetworkResponseID" => #{"TagNum" => "934" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "934" => #{"Name"=>"LastNetworkResponseID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "934"}


,
"NetworkRequestType" => #{"TagNum" => "935" ,"Type" => "INT" ,"ValidValues" =>[{"1", "SNAPSHOT"},{"2", "SUBSCRIBE"},{"4", "STOP_SUBSCRIBING"},{"8", "LEVEL_OF_DETAIL_THEN_NOCOMPIDS_BECOMES_REQUIRED"}]}
, "935" => #{"Name"=>"NetworkRequestType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "SNAPSHOT"},{"2", "SUBSCRIBE"},{"4", "STOP_SUBSCRIBING"},{"8", "LEVEL_OF_DETAIL_THEN_NOCOMPIDS_BECOMES_REQUIRED"}], "TagNum" => "935"}


,
"NoCompIDs" => #{"TagNum" => "936" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "936" => #{"Name"=>"NoCompIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "936"}


,
"NetworkStatusResponseType" => #{"TagNum" => "937" ,"Type" => "INT" ,"ValidValues" =>[{"1", "FULL"},{"2", "INCREMENTAL_UPDATE"}]}
, "937" => #{"Name"=>"NetworkStatusResponseType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "FULL"},{"2", "INCREMENTAL_UPDATE"}], "TagNum" => "937"}


,
"NoCollInquiryQualifier" => #{"TagNum" => "938" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "938" => #{"Name"=>"NoCollInquiryQualifier" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "938"}


,
"TrdRptStatus" => #{"TagNum" => "939" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ACCEPTED"},{"1", "REJECTED"}]}
, "939" => #{"Name"=>"TrdRptStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ACCEPTED"},{"1", "REJECTED"}], "TagNum" => "939"}


,
"AffirmStatus" => #{"TagNum" => "940" ,"Type" => "INT" ,"ValidValues" =>[{"1", "RECEIVED"},{"2", "CONFIRM_REJECTED_IE_NOT_AFFIRMED"},{"3", "AFFIRMED"}]}
, "940" => #{"Name"=>"AffirmStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "RECEIVED"},{"2", "CONFIRM_REJECTED_IE_NOT_AFFIRMED"},{"3", "AFFIRMED"}], "TagNum" => "940"}


,
"UnderlyingStrikeCurrency" => #{"TagNum" => "941" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "941" => #{"Name"=>"UnderlyingStrikeCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "941"}


,
"LegStrikeCurrency" => #{"TagNum" => "942" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "942" => #{"Name"=>"LegStrikeCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "942"}


,
"TimeBracket" => #{"TagNum" => "943" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "943" => #{"Name"=>"TimeBracket" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "943"}


,
"CollAction" => #{"TagNum" => "944" ,"Type" => "INT" ,"ValidValues" =>[{"0", "RETAIN"},{"1", "ADD"},{"2", "REMOVE"}]}
, "944" => #{"Name"=>"CollAction" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "RETAIN"},{"1", "ADD"},{"2", "REMOVE"}], "TagNum" => "944"}


,
"CollInquiryStatus" => #{"TagNum" => "945" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ACCEPTED"},{"1", "ACCEPTED_WITH_WARNINGS"},{"2", "COMPLETED"},{"3", "COMPLETED_WITH_WARNINGS"},{"4", "REJECTED"}]}
, "945" => #{"Name"=>"CollInquiryStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ACCEPTED"},{"1", "ACCEPTED_WITH_WARNINGS"},{"2", "COMPLETED"},{"3", "COMPLETED_WITH_WARNINGS"},{"4", "REJECTED"}], "TagNum" => "945"}


,
"CollInquiryResult" => #{"TagNum" => "946" ,"Type" => "INT" ,"ValidValues" =>[{"0", "SUCCESSFUL"},{"1", "INVALID_OR_UNKNOWN_INSTRUMENT"},{"2", "INVALID_OR_UNKNOWN_COLLATERAL_TYPE"},{"3", "INVALID_PARTIES"},{"4", "INVALID_TRANSPORT_TYPE_REQUESTED"},{"5", "INVALID_DESTINATION_REQUESTED"},{"6", "NO_COLLATERAL_FOUND_FOR_THE_TRADE_SPECIFIED"},{"7", "NO_COLLATERAL_FOUND_FOR_THE_ORDER_SPECIFIED"},{"8", "COLLATERAL_INQUIRY_TYPE_NOT_SUPPORTED"},{"9", "UNAUTHORIZED_FOR_COLLATERAL_INQUIRY"},{"99", "OTHER"}]}
, "946" => #{"Name"=>"CollInquiryResult" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "SUCCESSFUL"},{"1", "INVALID_OR_UNKNOWN_INSTRUMENT"},{"2", "INVALID_OR_UNKNOWN_COLLATERAL_TYPE"},{"3", "INVALID_PARTIES"},{"4", "INVALID_TRANSPORT_TYPE_REQUESTED"},{"5", "INVALID_DESTINATION_REQUESTED"},{"6", "NO_COLLATERAL_FOUND_FOR_THE_TRADE_SPECIFIED"},{"7", "NO_COLLATERAL_FOUND_FOR_THE_ORDER_SPECIFIED"},{"8", "COLLATERAL_INQUIRY_TYPE_NOT_SUPPORTED"},{"9", "UNAUTHORIZED_FOR_COLLATERAL_INQUIRY"},{"99", "OTHER"}], "TagNum" => "946"}


,
"StrikeCurrency" => #{"TagNum" => "947" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "947" => #{"Name"=>"StrikeCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "947"}


,
"NoNested3PartyIDs" => #{"TagNum" => "948" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "948" => #{"Name"=>"NoNested3PartyIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "948"}


,
"Nested3PartyID" => #{"TagNum" => "949" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "949" => #{"Name"=>"Nested3PartyID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "949"}


,
"Nested3PartyIDSource" => #{"TagNum" => "950" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "950" => #{"Name"=>"Nested3PartyIDSource" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "950"}


,
"Nested3PartyRole" => #{"TagNum" => "951" ,"Type" => "INT" ,"ValidValues" =>[]}
, "951" => #{"Name"=>"Nested3PartyRole" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "951"}


,
"NoNested3PartySubIDs" => #{"TagNum" => "952" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "952" => #{"Name"=>"NoNested3PartySubIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "952"}


,
"Nested3PartySubID" => #{"TagNum" => "953" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "953" => #{"Name"=>"Nested3PartySubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "953"}


,
"Nested3PartySubIDType" => #{"TagNum" => "954" ,"Type" => "INT" ,"ValidValues" =>[]}
, "954" => #{"Name"=>"Nested3PartySubIDType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "954"}


,
"LegContractSettlMonth" => #{"TagNum" => "955" ,"Type" => "MONTHYEAR" ,"ValidValues" =>[]}
, "955" => #{"Name"=>"LegContractSettlMonth" ,"Type"=>"MONTHYEAR" ,"ValidValues"=>[], "TagNum" => "955"}


,
"LegInterestAccrualDate" => #{"TagNum" => "956" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "956" => #{"Name"=>"LegInterestAccrualDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "956"}


}.



components() ->
 #{"LegSecAltIDGrp" => #{
                              "Fields" => #{"LegSecurityAltID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecurityAltIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegSecurityAltID" => #{
                              "Fields" => #{"LegSecurityAltID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecurityAltIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"TrdAllocGrp" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties2" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"BidCompRspGrp" => #{
                              "Fields" => #{"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Country" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"FairValue" =>#{"Required" => "N", "Sequence" => undefined}
,"NetGrossInd" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoBidComponents" => #{
                              "Fields" => #{"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Country" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"FairValue" =>#{"Required" => "N", "Sequence" => undefined}
,"NetGrossInd" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"CommissionData" =>#{"Required" => "Y", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"CommissionData" => #{
                              "Fields" => #{"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"CommCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"FundRenewWaiv" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"PositionAmountData" => #{
                              "Fields" => #{"PosAmtType" =>#{"Required" => "N", "Sequence" => undefined}
,"PosAmt" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoPosAmt" => #{
                              "Fields" => #{"PosAmtType" =>#{"Required" => "N", "Sequence" => undefined}
,"PosAmt" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"NstdPtys3SubGrp" => #{
                              "Fields" => #{"Nested3PartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested3PartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoNested3PartySubIDs" => #{
                              "Fields" => #{"Nested3PartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested3PartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"TrdCapDtGrp" => #{
                              "Fields" => #{"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoDates" => #{
                              "Fields" => #{"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"Parties" => #{
                              "Fields" => #{"PartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"PartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"PartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoPartyIDs" => #{
                              "Fields" => #{"PartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"PartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"PartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"PtysSubGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"AffectedOrdGrp" => #{
                              "Fields" => #{"OrigClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"AffectedOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"AffectedSecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAffectedOrders" => #{
                              "Fields" => #{"OrigClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"AffectedOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"AffectedSecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"Instrument" => #{
                              "Fields" => #{"Symbol" =>#{"Required" => "N", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"Product" =>#{"Required" => "N", "Sequence" => undefined}
,"CFICode" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySubType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDate" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponPaymentDate" =>#{"Required" => "N", "Sequence" => undefined}
,"IssueDate" =>#{"Required" => "N", "Sequence" => undefined}
,"RepoCollateralSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"RepurchaseTerm" =>#{"Required" => "N", "Sequence" => undefined}
,"RepurchaseRate" =>#{"Required" => "N", "Sequence" => undefined}
,"Factor" =>#{"Required" => "N", "Sequence" => undefined}
,"CreditRating" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrRegistry" =>#{"Required" => "N", "Sequence" => undefined}
,"CountryOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"StateOrProvinceOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"LocaleOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"RedemptionDate" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikeCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Pool" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractSettlMonth" =>#{"Required" => "N", "Sequence" => undefined}
,"CPProgram" =>#{"Required" => "N", "Sequence" => undefined}
,"CPRegType" =>#{"Required" => "N", "Sequence" => undefined}
,"DatedDate" =>#{"Required" => "N", "Sequence" => undefined}
,"InterestAccrualDate" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"SecAltIDGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"EvntGrp" =>#{"Required" => "N", "Sequence" => undefined}
}}
,"ContraGrp" => #{
                              "Fields" => #{"ContraBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTrader" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTradeQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTradeTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraLegRefID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoContraBrokers" => #{
                              "Fields" => #{"ContraBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTrader" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTradeQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTradeTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraLegRefID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"ClrInstGrp" => #{
                              "Fields" => #{"ClearingInstruction" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoClearingInstructions" => #{
                              "Fields" => #{"ClearingInstruction" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"MDFullGrp" => #{
                              "Fields" => #{"MDEntryType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MDEntryPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySize" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TickDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"MDMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryOriginator" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenCloseSettlFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"SellerDays" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryBuyer" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySeller" =>#{"Required" => "N", "Sequence" => undefined}
,"NumberOfOrders" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPositionNo" =>#{"Required" => "N", "Sequence" => undefined}
,"Scope" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceDelta" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMDEntries" => #{
                              "Fields" => #{"MDEntryType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MDEntryPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySize" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TickDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"MDMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryOriginator" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenCloseSettlFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"SellerDays" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryBuyer" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySeller" =>#{"Required" => "N", "Sequence" => undefined}
,"NumberOfOrders" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPositionNo" =>#{"Required" => "N", "Sequence" => undefined}
,"Scope" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceDelta" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"RFQReqGrp" => #{
                              "Fields" => #{"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"QuotEntryAckGrp" => #{
                              "Fields" => #{"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"MidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"MidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferYield" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryRejectReason" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoQuoteEntries" => #{
                              "Fields" => #{"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"MidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"MidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferYield" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryRejectReason" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"UndInstrmtStrkPxGrp" => #{
                              "Fields" => #{"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoUnderlyings" => #{
                              "Fields" => #{"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"SideCrossOrdModGrp" => #{
                              "Fields" => #{"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"SideComplianceID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSides" => #{
                              "Fields" => #{"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"SideComplianceID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"PreAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"RgstDtlsGrp" => #{
                              "Fields" => #{"RegistDtls" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistEmail" =>#{"Required" => "N", "Sequence" => undefined}
,"MailingDtls" =>#{"Required" => "N", "Sequence" => undefined}
,"MailingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OwnerType" =>#{"Required" => "N", "Sequence" => undefined}
,"DateOfBirth" =>#{"Required" => "N", "Sequence" => undefined}
,"InvestorCountryOfResidence" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRegistDtls" => #{
                              "Fields" => #{"RegistDtls" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistEmail" =>#{"Required" => "N", "Sequence" => undefined}
,"MailingDtls" =>#{"Required" => "N", "Sequence" => undefined}
,"MailingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OwnerType" =>#{"Required" => "N", "Sequence" => undefined}
,"DateOfBirth" =>#{"Required" => "N", "Sequence" => undefined}
,"InvestorCountryOfResidence" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"QuotSetAckGrp" => #{
                              "Fields" => #{"QuoteSetID" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoQuoteEntries" =>#{"Required" => "N", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoQuoteSets" => #{
                              "Fields" => #{"QuoteSetID" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoQuoteEntries" =>#{"Required" => "N", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotEntryAckGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"QuotReqGrp" => #{
                              "Fields" => #{"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotePriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotePriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotReqLegsGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotQualGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"InstrmtMDReqGrp" => #{
                              "Fields" => #{}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"TrdRegTimestamps" => #{
                              "Fields" => #{"TrdRegTimestamp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestampType" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestampOrigin" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoTrdRegTimestamps" => #{
                              "Fields" => #{"TrdRegTimestamp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestampType" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestampOrigin" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"CollInqQualGrp" => #{
                              "Fields" => #{"CollInquiryQualifier" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoCollInquiryQualifier" => #{
                              "Fields" => #{"CollInquiryQualifier" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"TrdgSesGrp" => #{
                              "Fields" => #{"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoTradingSessions" => #{
                              "Fields" => #{"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"TrdInstrmtLegGrp" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegLastPx" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegLastPx" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"BidDescReqGrp" => #{
                              "Fields" => #{"BidDescriptorType" =>#{"Required" => "N", "Sequence" => undefined}
,"BidDescriptor" =>#{"Required" => "N", "Sequence" => undefined}
,"SideValueInd" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityValue" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityNumSecurities" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityPctLow" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityPctHigh" =>#{"Required" => "N", "Sequence" => undefined}
,"EFPTrackingError" =>#{"Required" => "N", "Sequence" => undefined}
,"FairValue" =>#{"Required" => "N", "Sequence" => undefined}
,"OutsideIndexPct" =>#{"Required" => "N", "Sequence" => undefined}
,"ValueOfFutures" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoBidDescriptors" => #{
                              "Fields" => #{"BidDescriptorType" =>#{"Required" => "N", "Sequence" => undefined}
,"BidDescriptor" =>#{"Required" => "N", "Sequence" => undefined}
,"SideValueInd" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityValue" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityNumSecurities" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityPctLow" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityPctHigh" =>#{"Required" => "N", "Sequence" => undefined}
,"EFPTrackingError" =>#{"Required" => "N", "Sequence" => undefined}
,"FairValue" =>#{"Required" => "N", "Sequence" => undefined}
,"OutsideIndexPct" =>#{"Required" => "N", "Sequence" => undefined}
,"ValueOfFutures" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"ExecAllocGrp" => #{
                              "Fields" => #{"LastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastParPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastCapacity" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoExecs" => #{
                              "Fields" => #{"LastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastParPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastCapacity" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"ExecsGrp" => #{
                              "Fields" => #{"ExecID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoExecs" => #{
                              "Fields" => #{"ExecID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"MDRjctGrp" => #{
                              "Fields" => #{"AltMDSourceID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAltMDSource" => #{
                              "Fields" => #{"AltMDSourceID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"BidCompReqGrp" => #{
                              "Fields" => #{"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"NetGrossInd" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoBidComponents" => #{
                              "Fields" => #{"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"NetGrossInd" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"PtysSubGrp" => #{
                              "Fields" => #{"PartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"PartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoPartySubIDs" => #{
                              "Fields" => #{"PartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"PartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"LegPreAllocGrp" => #{
                              "Fields" => #{"LegAllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"LegIndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegAllocQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegAllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegAllocs" => #{
                              "Fields" => #{"LegAllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"LegIndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegAllocQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegAllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties2" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"PreAllocGrp" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"NestedParties2" => #{
                              "Fields" => #{"Nested2PartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested2PartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested2PartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoNested2PartyIDs" => #{
                              "Fields" => #{"Nested2PartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested2PartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested2PartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NstdPtys2SubGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"DiscretionInstructions" => #{
                              "Fields" => #{"DiscretionInst" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionOffsetValue" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionMoveType" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionOffsetType" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionLimitType" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionRoundDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionScope" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"InstrmtStrkPxGrp" => #{
                              "Fields" => #{}
                              ,"Groups" => #{
                              "NoStrikes" => #{
                              "Fields" => #{}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"InstrmtLegIOIGrp" => #{
                              "Fields" => #{"LegIOIQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{"LegIOIQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStipulations" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"Hop" => #{
                              "Fields" => #{"HopCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"HopSendingTime" =>#{"Required" => "N", "Sequence" => undefined}
,"HopRefID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoHops" => #{
                              "Fields" => #{"HopCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"HopSendingTime" =>#{"Required" => "N", "Sequence" => undefined}
,"HopRefID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"SecAltIDGrp" => #{
                              "Fields" => #{"SecurityAltID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityAltIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSecurityAltID" => #{
                              "Fields" => #{"SecurityAltID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityAltIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"RoutingGrp" => #{
                              "Fields" => #{"RoutingType" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRoutingIDs" => #{
                              "Fields" => #{"RoutingType" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"NestedParties3" => #{
                              "Fields" => #{"Nested3PartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested3PartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested3PartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoNested3PartyIDs" => #{
                              "Fields" => #{"Nested3PartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested3PartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested3PartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NstdPtys3SubGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"CompIDReqGrp" => #{
                              "Fields" => #{"RefCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoCompIDs" => #{
                              "Fields" => #{"RefCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"DlvyInstGrp" => #{
                              "Fields" => #{"SettlInstSource" =>#{"Required" => "N", "Sequence" => undefined}
,"DlvyInstType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoDlvyInst" => #{
                              "Fields" => #{"SettlInstSource" =>#{"Required" => "N", "Sequence" => undefined}
,"DlvyInstType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"SettlParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"TrdCollGrp" => #{
                              "Fields" => #{"TradeReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTradeReportID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoTrades" => #{
                              "Fields" => #{"TradeReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTradeReportID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"SideCrossOrdCxlGrp" => #{
                              "Fields" => #{"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigOrdModTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSides" => #{
                              "Fields" => #{"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigOrdModTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"PreAllocMlegGrp" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties3" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"ListOrdGrp" => #{
                              "Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstMode" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"SideValueInd" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategy" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategyParameters" =>#{"Required" => "N", "Sequence" => undefined}
,"ParticipationRate" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoOrders" => #{
                              "Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstMode" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"SideValueInd" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategy" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategyParameters" =>#{"Required" => "N", "Sequence" => undefined}
,"ParticipationRate" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"PreAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdgSesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
,"PegInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInstructions" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"LegStipulations" => #{
                              "Fields" => #{"LegStipulationType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStipulationValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegStipulations" => #{
                              "Fields" => #{"LegStipulationType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStipulationValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"QuotQualGrp" => #{
                              "Fields" => #{"QuoteQualifier" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoQuoteQualifiers" => #{
                              "Fields" => #{"QuoteQualifier" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"PegInstructions" => #{
                              "Fields" => #{"PegOffsetValue" =>#{"Required" => "N", "Sequence" => undefined}
,"PegMoveType" =>#{"Required" => "N", "Sequence" => undefined}
,"PegOffsetType" =>#{"Required" => "N", "Sequence" => undefined}
,"PegLimitType" =>#{"Required" => "N", "Sequence" => undefined}
,"PegRoundDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"PegScope" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"SettlPtysSubGrp" => #{
                              "Fields" => #{"SettlPartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlPartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSettlPartySubIDs" => #{
                              "Fields" => #{"SettlPartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlPartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"UndInstrmtCollGrp" => #{
                              "Fields" => #{"CollAction" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoUnderlyings" => #{
                              "Fields" => #{"CollAction" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"TrdCapRptSideGrp" => #{
                              "Fields" => #{"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"OddLot" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputSource" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputDevice" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderInputDevice" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"TransBkdTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeBracket" =>#{"Required" => "N", "Sequence" => undefined}
,"GrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"NumDaysInterest" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"InterestAtMaturity" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"Concession" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalTakedown" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"SideMultiLegReportingType" =>#{"Required" => "N", "Sequence" => undefined}
,"ExchangeRule" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeAllocIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSides" => #{
                              "Fields" => #{"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"OddLot" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputSource" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputDevice" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderInputDevice" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"TransBkdTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeBracket" =>#{"Required" => "N", "Sequence" => undefined}
,"GrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"NumDaysInterest" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"InterestAtMaturity" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"Concession" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalTakedown" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"SideMultiLegReportingType" =>#{"Required" => "N", "Sequence" => undefined}
,"ExchangeRule" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeAllocIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"ClrInstGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"SecListGrp" => #{
                              "Fields" => #{"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"RoundLot" =>#{"Required" => "N", "Sequence" => undefined}
,"MinTradeVol" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpirationCycle" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"RoundLot" =>#{"Required" => "N", "Sequence" => undefined}
,"MinTradeVol" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpirationCycle" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegSecListGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"CompIDStatGrp" => #{
                              "Fields" => #{"RefCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}
,"StatusValue" =>#{"Required" => "N", "Sequence" => undefined}
,"StatusText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoCompIDs" => #{
                              "Fields" => #{"RefCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}
,"StatusValue" =>#{"Required" => "N", "Sequence" => undefined}
,"StatusText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"UnderlyingStipulations" => #{
                              "Fields" => #{"UnderlyingStipType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStipValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoUnderlyingStips" => #{
                              "Fields" => #{"UnderlyingStipType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStipValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"ExecCollGrp" => #{
                              "Fields" => #{"ExecID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoExecs" => #{
                              "Fields" => #{"ExecID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"InstrmtLegExecGrp" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegLastPx" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegLastPx" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"SettlParties" => #{
                              "Fields" => #{"SettlPartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlPartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlPartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSettlPartyIDs" => #{
                              "Fields" => #{"SettlPartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlPartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlPartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"SettlPtysSubGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"AttrbGrp" => #{
                              "Fields" => #{"InstrAttribType" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrAttribValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoInstrAttrib" => #{
                              "Fields" => #{"InstrAttribType" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrAttribValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"RelSymDerivSecGrp" => #{
                              "Fields" => #{"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpirationCycle" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpirationCycle" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"ContAmtGrp" => #{
                              "Fields" => #{"ContAmtType" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtValue" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtCurr" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoContAmts" => #{
                              "Fields" => #{"ContAmtType" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtValue" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtCurr" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"AllocGrp" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"NotifyBrokerOfCredit" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocHandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocNetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocInterestAtMaturity" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlInstType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"NotifyBrokerOfCredit" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocHandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocNetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocInterestAtMaturity" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlInstType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"ClrInstGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstructionsData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"LegBenchmarkCurveData" => #{
                              "Fields" => #{"LegBenchmarkCurveCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"LegBenchmarkCurveName" =>#{"Required" => "N", "Sequence" => undefined}
,"LegBenchmarkCurvePoint" =>#{"Required" => "N", "Sequence" => undefined}
,"LegBenchmarkPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"LegBenchmarkPriceType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"NstdPtysSubGrp" => #{
                              "Fields" => #{"NestedPartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedPartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoNestedPartySubIDs" => #{
                              "Fields" => #{"NestedPartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedPartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"EvntGrp" => #{
                              "Fields" => #{"EventType" =>#{"Required" => "N", "Sequence" => undefined}
,"EventDate" =>#{"Required" => "N", "Sequence" => undefined}
,"EventPx" =>#{"Required" => "N", "Sequence" => undefined}
,"EventText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoEvents" => #{
                              "Fields" => #{"EventType" =>#{"Required" => "N", "Sequence" => undefined}
,"EventDate" =>#{"Required" => "N", "Sequence" => undefined}
,"EventPx" =>#{"Required" => "N", "Sequence" => undefined}
,"EventText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"UndInstrmtGrp" => #{
                              "Fields" => #{}
                              ,"Groups" => #{
                              "NoUnderlyings" => #{
                              "Fields" => #{}
                              ,"Groups" => #{}
                              ,"Components" => #{"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"QuotReqLegsGrp" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
,"LegBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"QuotEntryGrp" => #{
                              "Fields" => #{"QuoteEntryID" =>#{"Required" => "Y", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"MidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"MidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferYield" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoQuoteEntries" => #{
                              "Fields" => #{"QuoteEntryID" =>#{"Required" => "Y", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"MidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"MidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferYield" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"AllocAckGrp" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocRejCode" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocRejCode" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"CpctyConfGrp" => #{
                              "Fields" => #{"OrderCapacity" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacityQty" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoCapacities" => #{
                              "Fields" => #{"OrderCapacity" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacityQty" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"SettlInstGrp" => #{
                              "Fields" => #{"SettlInstID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstTransType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Product" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"CFICode" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"LastUpdateTime" =>#{"Required" => "N", "Sequence" => undefined}
,"PaymentMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"PaymentRef" =>#{"Required" => "N", "Sequence" => undefined}
,"CardHolderName" =>#{"Required" => "N", "Sequence" => undefined}
,"CardNumber" =>#{"Required" => "N", "Sequence" => undefined}
,"CardStartDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CardExpDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CardIssNum" =>#{"Required" => "N", "Sequence" => undefined}
,"PaymentDate" =>#{"Required" => "N", "Sequence" => undefined}
,"PaymentRemitterID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSettlInst" => #{
                              "Fields" => #{"SettlInstID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstTransType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Product" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"CFICode" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"LastUpdateTime" =>#{"Required" => "N", "Sequence" => undefined}
,"PaymentMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"PaymentRef" =>#{"Required" => "N", "Sequence" => undefined}
,"CardHolderName" =>#{"Required" => "N", "Sequence" => undefined}
,"CardNumber" =>#{"Required" => "N", "Sequence" => undefined}
,"CardStartDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CardExpDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CardIssNum" =>#{"Required" => "N", "Sequence" => undefined}
,"PaymentDate" =>#{"Required" => "N", "Sequence" => undefined}
,"PaymentRemitterID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstructionsData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"QuotSetGrp" => #{
                              "Fields" => #{"QuoteSetID" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteSetValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoQuoteEntries" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoQuoteSets" => #{
                              "Fields" => #{"QuoteSetID" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteSetValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoQuoteEntries" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotEntryGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"InstrumentLeg" => #{
                              "Fields" => #{"LegSymbol" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecurityIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"LegProduct" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCFICode" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecuritySubType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegMaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"LegMaturityDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCouponPaymentDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegIssueDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRepoCollateralSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRepurchaseTerm" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRepurchaseRate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegFactor" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCreditRating" =>#{"Required" => "N", "Sequence" => undefined}
,"LegInstrRegistry" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCountryOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStateOrProvinceOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"LegLocaleOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRedemptionDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStrikeCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"LegContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"LegIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedLegIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedLegIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedLegSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedLegSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRatioQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSide" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPool" =>#{"Required" => "N", "Sequence" => undefined}
,"LegDatedDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegContractSettlMonth" =>#{"Required" => "N", "Sequence" => undefined}
,"LegInterestAccrualDate" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"LegSecAltIDGrp" =>#{"Required" => "N", "Sequence" => undefined}
}}
,"LinesOfTextGrp" => #{
                              "Fields" => #{"Text" =>#{"Required" => "Y", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLinesOfText" => #{
                              "Fields" => #{"Text" =>#{"Required" => "Y", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"IOIQualGrp" => #{
                              "Fields" => #{"IOIQualifier" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoIOIQualifiers" => #{
                              "Fields" => #{"IOIQualifier" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"SettlInstructionsData" => #{
                              "Fields" => #{"SettlDeliveryType" =>#{"Required" => "N", "Sequence" => undefined}
,"StandInstDbType" =>#{"Required" => "N", "Sequence" => undefined}
,"StandInstDbName" =>#{"Required" => "N", "Sequence" => undefined}
,"StandInstDbID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"DlvyInstGrp" =>#{"Required" => "N", "Sequence" => undefined}
}}
,"UndSecAltIDGrp" => #{
                              "Fields" => #{"UnderlyingSecurityAltID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityAltIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoUnderlyingSecurityAltID" => #{
                              "Fields" => #{"UnderlyingSecurityAltID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityAltIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"QuotReqRjctGrp" => #{
                              "Fields" => #{"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotePriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotePriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotReqLegsGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotQualGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"InstrmtLegGrp" => #{
                              "Fields" => #{}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"OrderQtyData" => #{
                              "Fields" => #{"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderPercent" =>#{"Required" => "N", "Sequence" => undefined}
,"RoundingDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"RoundingModulus" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"LegOrdGrp" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPreAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"UnderlyingInstrument" => #{
                              "Fields" => #{"UnderlyingSymbol" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingProduct" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCFICode" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecuritySubType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityDate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingPutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCouponPaymentDate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingIssueDate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingRepoCollateralSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingRepurchaseTerm" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingRepurchaseRate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingFactor" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCreditRating" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingInstrRegistry" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCountryOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStateOrProvinceOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLocaleOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingRedemptionDate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStrikeCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingOptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCPProgram" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCPRegType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingQty" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingPx" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingDirtyPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingEndPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStartValue" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCurrentValue" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingEndValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"UndSecAltIDGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStipulations" =>#{"Required" => "N", "Sequence" => undefined}
}}
,"LegQuotGrp" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegBidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOfferPx" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegBidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOfferPx" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
,"LegBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"MiscFeesGrp" => #{
                              "Fields" => #{"MiscFeeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeType" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeBasis" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMiscFees" => #{
                              "Fields" => #{"MiscFeeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeType" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeBasis" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"PosUndInstrmtGrp" => #{
                              "Fields" => #{"UnderlyingSettlPrice" =>#{"Required" => "Y", "Sequence" => undefined}
,"UnderlyingSettlPriceType" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoUnderlyings" => #{
                              "Fields" => #{"UnderlyingSettlPrice" =>#{"Required" => "Y", "Sequence" => undefined}
,"UnderlyingSettlPriceType" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"FinancingDetails" => #{
                              "Fields" => #{"AgreementDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"AgreementID" =>#{"Required" => "N", "Sequence" => undefined}
,"AgreementDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AgreementCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"TerminationType" =>#{"Required" => "N", "Sequence" => undefined}
,"StartDate" =>#{"Required" => "N", "Sequence" => undefined}
,"EndDate" =>#{"Required" => "N", "Sequence" => undefined}
,"DeliveryType" =>#{"Required" => "N", "Sequence" => undefined}
,"MarginRatio" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"RgstDistInstGrp" => #{
                              "Fields" => #{"DistribPaymentMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"DistribPercentage" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribAgentName" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribAgentCode" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribAgentAcctNumber" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribPayRef" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribAgentAcctName" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoDistribInsts" => #{
                              "Fields" => #{"DistribPaymentMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"DistribPercentage" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribAgentName" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribAgentCode" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribAgentAcctNumber" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribPayRef" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribAgentAcctName" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"MDIncGrp" => #{
                              "Fields" => #{"MDUpdateAction" =>#{"Required" => "Y", "Sequence" => undefined}
,"DeleteReason" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryType" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancialStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySize" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TickDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"MDMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryOriginator" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenCloseSettlFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"SellerDays" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryBuyer" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySeller" =>#{"Required" => "N", "Sequence" => undefined}
,"NumberOfOrders" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPositionNo" =>#{"Required" => "N", "Sequence" => undefined}
,"Scope" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceDelta" =>#{"Required" => "N", "Sequence" => undefined}
,"NetChgPrevDay" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMDEntries" => #{
                              "Fields" => #{"MDUpdateAction" =>#{"Required" => "Y", "Sequence" => undefined}
,"DeleteReason" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryType" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancialStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySize" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TickDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"MDMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryOriginator" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenCloseSettlFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"SellerDays" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryBuyer" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySeller" =>#{"Required" => "N", "Sequence" => undefined}
,"NumberOfOrders" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPositionNo" =>#{"Required" => "N", "Sequence" => undefined}
,"Scope" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceDelta" =>#{"Required" => "N", "Sequence" => undefined}
,"NetChgPrevDay" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"OrdAllocGrp" => #{
                              "Fields" => #{"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderAvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderBookingQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoOrders" => #{
                              "Fields" => #{"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderAvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderBookingQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties2" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"InstrmtLegSecListGrp" => #{
                              "Fields" => #{"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"LegBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"InstrumentExtension" => #{
                              "Fields" => #{"DeliveryForm" =>#{"Required" => "N", "Sequence" => undefined}
,"PctAtRisk" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"AttrbGrp" =>#{"Required" => "N", "Sequence" => undefined}
}}
,"MDReqGrp" => #{
                              "Fields" => #{"MDEntryType" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMDEntryTypes" => #{
                              "Fields" => #{"MDEntryType" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"NstdPtys2SubGrp" => #{
                              "Fields" => #{"Nested2PartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested2PartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoNested2PartySubIDs" => #{
                              "Fields" => #{"Nested2PartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested2PartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"SecTypesGrp" => #{
                              "Fields" => #{"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySubType" =>#{"Required" => "N", "Sequence" => undefined}
,"Product" =>#{"Required" => "N", "Sequence" => undefined}
,"CFICode" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSecurityTypes" => #{
                              "Fields" => #{"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySubType" =>#{"Required" => "N", "Sequence" => undefined}
,"Product" =>#{"Required" => "N", "Sequence" => undefined}
,"CFICode" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"QuotCxlEntriesGrp" => #{
                              "Fields" => #{}
                              ,"Groups" => #{
                              "NoQuoteEntries" => #{
                              "Fields" => #{}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"InstrmtGrp" => #{
                              "Fields" => #{}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"Stipulations" => #{
                              "Fields" => #{"StipulationType" =>#{"Required" => "N", "Sequence" => undefined}
,"StipulationValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoStipulations" => #{
                              "Fields" => #{"StipulationType" =>#{"Required" => "N", "Sequence" => undefined}
,"StipulationValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"NestedParties" => #{
                              "Fields" => #{"NestedPartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedPartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedPartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoNestedPartyIDs" => #{
                              "Fields" => #{"NestedPartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedPartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedPartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NstdPtysSubGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"LegQuotStatGrp" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"PositionQty" => #{
                              "Fields" => #{"PosType" =>#{"Required" => "N", "Sequence" => undefined}
,"LongQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ShortQty" =>#{"Required" => "N", "Sequence" => undefined}
,"PosQtyStatus" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoPositions" => #{
                              "Fields" => #{"PosType" =>#{"Required" => "N", "Sequence" => undefined}
,"LongQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ShortQty" =>#{"Required" => "N", "Sequence" => undefined}
,"PosQtyStatus" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"YieldData" => #{
                              "Fields" => #{"YieldType" =>#{"Required" => "N", "Sequence" => undefined}
,"Yield" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldCalcDate" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldRedemptionDate" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldRedemptionPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldRedemptionPriceType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"SpreadOrBenchmarkCurveData" => #{
                              "Fields" => #{"Spread" =>#{"Required" => "N", "Sequence" => undefined}
,"BenchmarkCurveCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"BenchmarkCurveName" =>#{"Required" => "N", "Sequence" => undefined}
,"BenchmarkCurvePoint" =>#{"Required" => "N", "Sequence" => undefined}
,"BenchmarkPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"BenchmarkPriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"BenchmarkSecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"BenchmarkSecurityIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"OrdListStatGrp" => #{
                              "Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"CumQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"WorkingIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"LeavesQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"CxlQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoOrders" => #{
                              "Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"CumQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"WorkingIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"LeavesQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"CxlQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
}.



groups() ->
 #{}.



header()->
#{
"Fields" => #{"BeginString" =>#{"Required" => "Y", "Sequence" => undefined}
,"BodyLength" =>#{"Required" => "Y", "Sequence" => undefined}
,"MsgType" =>#{"Required" => "Y", "Sequence" => undefined}
,"SenderCompID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TargetCompID" =>#{"Required" => "Y", "Sequence" => undefined}
,"OnBehalfOfCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeliverToCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecureDataLen" =>#{"Required" => "N", "Sequence" => undefined}
,"SecureData" =>#{"Required" => "N", "Sequence" => undefined}
,"MsgSeqNum" =>#{"Required" => "Y", "Sequence" => undefined}
,"SenderSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SenderLocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetLocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"OnBehalfOfSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"OnBehalfOfLocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeliverToSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeliverToLocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"PossDupFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"PossResend" =>#{"Required" => "N", "Sequence" => undefined}
,"SendingTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigSendingTime" =>#{"Required" => "N", "Sequence" => undefined}
,"XmlDataLen" =>#{"Required" => "N", "Sequence" => undefined}
,"XmlData" =>#{"Required" => "N", "Sequence" => undefined}
,"MessageEncoding" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMsgSeqNumProcessed" =>#{"Required" => "N", "Sequence" => undefined}}, 
"Groups" => #{}, 
 "Components" => #{"Hop" =>#{"Required" => "N", "Sequence" => undefined}
}

}.



trailer()->
#{
"Fields" => #{"SignatureLength" =>#{"Required" => "N", "Sequence" => undefined}
,"Signature" =>#{"Required" => "N", "Sequence" => undefined}
,"CheckSum" =>#{"Required" => "Y", "Sequence" => undefined}}, 
"Groups" => #{}, 
 "Components" => #{}

}.


-module(fix50sp2).
-export([messages/0,
         fields/0,
         components/0,
         groups/0,
         header/0,
         trailer/0]).

messages() ->
 #{"IOI" => #{
                              "Category" => "app"
                              ,"Type" => "6"
                              ,"Fields" => #{"IOIID" =>#{"Required" => "Y", "Sequence" => undefined}
,"IOITransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"IOIRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIQltyInd" =>#{"Required" => "N", "Sequence" => undefined}
,"IOINaturalFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"URLLink" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegIOIGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIQualGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"6" => #{"Category"=>"app"
           ,"Name" => "IOI"}

,"Advertisement" => #{
                              "Category" => "app"
                              ,"Type" => "7"
                              ,"Fields" => #{"AdvId" =>#{"Required" => "Y", "Sequence" => undefined}
,"AdvTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"AdvRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"AdvSide" =>#{"Required" => "Y", "Sequence" => undefined}
,"Quantity" =>#{"Required" => "Y", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"URLLink" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"7" => #{"Category"=>"app"
           ,"Name" => "Advertisement"}

,"ExecutionReport" => #{
                              "Category" => "app"
                              ,"Type" => "8"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRespID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdStatusReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"MassStatusReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"HostCrossID" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNumReports" =>#{"Required" => "N", "Sequence" => undefined}
,"LastRptRequested" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"CrossID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigCrossID" =>#{"Required" => "N", "Sequence" => undefined}
,"CrossType" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdMatchID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExecRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecType" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"WorkingIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecRestatementReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCategory" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"LotType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceProtectionScope" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"PeggedPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"PeggedRefPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategy" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategyParameters" =>#{"Required" => "N", "Sequence" => undefined}
,"ParticipationRate" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategyPerformance" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"AggressorIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"PreTradeAnonymity" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"LastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"CalculatedCcyLastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LastSwapPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastParPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"LastForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeBracket" =>#{"Required" => "N", "Sequence" => undefined}
,"LastCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"LeavesQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"CumQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"DayOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"DayCumQty" =>#{"Required" => "N", "Sequence" => undefined}
,"DayAvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoFills" =>#{"Required" => "N", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ReportToExch" =>#{"Required" => "N", "Sequence" => undefined}
,"GrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"NumDaysInterest" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"InterestAtMaturity" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"TradedFlatSwitch" =>#{"Required" => "N", "Sequence" => undefined}
,"BasisFeatureDate" =>#{"Required" => "N", "Sequence" => undefined}
,"BasisFeaturePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"Concession" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalTakedown" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchIncrement" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxPriceLevels" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"LastForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"MultiLegReportingType" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}
,"TransBkdTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecValuationPoint" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecPriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecPriceAdjustment" =>#{"Required" => "N", "Sequence" => undefined}
,"PriorityIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceImprovement" =>#{"Required" => "N", "Sequence" => undefined}
,"LastLiquidityInd" =>#{"Required" => "N", "Sequence" => undefined}
,"CopyMsgIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"DividendYield" =>#{"Required" => "N", "Sequence" => undefined}
,"ManualOrderIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"CustDirectedOrder" =>#{"Required" => "N", "Sequence" => undefined}
,"ReceivedDeptID" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderHandlingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderHandlingInstSource" =>#{"Required" => "N", "Sequence" => undefined}
,"Volatility" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeToExpiration" =>#{"Required" => "N", "Sequence" => undefined}
,"RiskFreeRate" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceDelta" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"PreAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "N", "Sequence" => undefined}
,"TriggeringInstruction" =>#{"Required" => "N", "Sequence" => undefined}
,"PegInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"StrategyParametersGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"FillsGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"DisplayInstruction" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegExecGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestamps" =>#{"Required" => "N", "Sequence" => undefined}
,"RateSource" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"8" => #{"Category"=>"app"
           ,"Name" => "ExecutionReport"}

,"OrderCancelReject" => #{
                              "Category" => "app"
                              ,"Type" => "9"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"WorkingIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigOrdModTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"CxlRejResponseTo" =>#{"Required" => "Y", "Sequence" => undefined}
,"CxlRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"9" => #{"Category"=>"app"
           ,"Name" => "OrderCancelReject"}

,"News" => #{
                              "Category" => "app"
                              ,"Type" => "B"
                              ,"Fields" => #{"OrigTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Urgency" =>#{"Required" => "N", "Sequence" => undefined}
,"Headline" =>#{"Required" => "Y", "Sequence" => undefined}
,"EncodedHeadlineLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedHeadline" =>#{"Required" => "N", "Sequence" => undefined}
,"URLLink" =>#{"Required" => "N", "Sequence" => undefined}
,"RawDataLength" =>#{"Required" => "N", "Sequence" => undefined}
,"RawData" =>#{"Required" => "N", "Sequence" => undefined}
,"NewsID" =>#{"Required" => "N", "Sequence" => undefined}
,"NewsCategory" =>#{"Required" => "N", "Sequence" => undefined}
,"LanguageCode" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"LinesOfTextGrp" =>#{"Required" => "Y", "Sequence" => undefined}
,"NewsRefGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"B" => #{"Category"=>"app"
           ,"Name" => "News"}

,"Email" => #{
                              "Category" => "app"
                              ,"Type" => "C"
                              ,"Fields" => #{"EmailThreadID" =>#{"Required" => "Y", "Sequence" => undefined}
,"EmailType" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Subject" =>#{"Required" => "Y", "Sequence" => undefined}
,"EncodedSubjectLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSubject" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"RawDataLength" =>#{"Required" => "N", "Sequence" => undefined}
,"RawData" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"RoutingGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"LinesOfTextGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"C" => #{"Category"=>"app"
           ,"Name" => "Email"}

,"NewOrderSingle" => #{
                              "Category" => "app"
                              ,"Type" => "D"
                              ,"Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchIncrement" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxPriceLevels" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestinationIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceProtectionScope" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"PreTradeAnonymity" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategy" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategyParameters" =>#{"Required" => "N", "Sequence" => undefined}
,"ParticipationRate" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}
,"ManualOrderIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"CustDirectedOrder" =>#{"Required" => "N", "Sequence" => undefined}
,"ReceivedDeptID" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderHandlingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderHandlingInstSource" =>#{"Required" => "N", "Sequence" => undefined}
,"RefOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefOrderIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"PreAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"DisplayInstruction" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdgSesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
,"TriggeringInstruction" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
,"PegInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"StrategyParametersGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestamps" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"D" => #{"Category"=>"app"
           ,"Name" => "NewOrderSingle"}

,"NewOrderList" => #{
                              "Category" => "app"
                              ,"Type" => "E"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"BidID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientBidID" =>#{"Required" => "N", "Sequence" => undefined}
,"ProgRptReqs" =>#{"Required" => "N", "Sequence" => undefined}
,"BidType" =>#{"Required" => "Y", "Sequence" => undefined}
,"ProgPeriodInterval" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListExecInstType" =>#{"Required" => "N", "Sequence" => undefined}
,"ListExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"ContingencyType" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedListExecInstLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedListExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"AllowableOneSidednessPct" =>#{"Required" => "N", "Sequence" => undefined}
,"AllowableOneSidednessValue" =>#{"Required" => "N", "Sequence" => undefined}
,"AllowableOneSidednessCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoOrders" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"RootParties" =>#{"Required" => "N", "Sequence" => undefined}
,"ListOrdGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"E" => #{"Category"=>"app"
           ,"Name" => "NewOrderList"}

,"OrderCancelRequest" => #{
                              "Category" => "app"
                              ,"Type" => "F"
                              ,"Fields" => #{"OrigClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigOrdModTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"F" => #{"Category"=>"app"
           ,"Name" => "OrderCancelRequest"}

,"OrderCancelReplaceRequest" => #{
                              "Category" => "app"
                              ,"Type" => "G"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigOrdModTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchIncrement" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxPriceLevels" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestinationIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceProtectionScope" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategy" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategyParameters" =>#{"Required" => "N", "Sequence" => undefined}
,"ParticipationRate" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"PreTradeAnonymity" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}
,"ManualOrderIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"CustDirectedOrder" =>#{"Required" => "N", "Sequence" => undefined}
,"ReceivedDeptID" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderHandlingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderHandlingInstSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"PreAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"DisplayInstruction" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdgSesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
,"TriggeringInstruction" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"PegInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"StrategyParametersGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestamps" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"G" => #{"Category"=>"app"
           ,"Name" => "OrderCancelReplaceRequest"}

,"OrderStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "H"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdStatusReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"H" => #{"Category"=>"app"
           ,"Name" => "OrderStatusRequest"}

,"AllocationInstruction" => #{
                              "Category" => "app"
                              ,"Type" => "J"
                              ,"Fields" => #{"AllocID" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocType" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocCancReplaceReason" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocIntermedReqType" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocLinkType" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocNoOrdersType" =>#{"Required" => "N", "Sequence" => undefined}
,"PreviouslyReported" =>#{"Required" => "N", "Sequence" => undefined}
,"ReversalIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchType" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"Quantity" =>#{"Required" => "Y", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgParPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPxPrecision" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"GrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"Concession" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalTakedown" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"AutoAcceptIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"NumDaysInterest" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"InterestAtMaturity" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"LegalConfirm" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoAllocs" =>#{"Required" => "N", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPxIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdSubType" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputSource" =>#{"Required" => "N", "Sequence" => undefined}
,"MultiLegReportingType" =>#{"Required" => "N", "Sequence" => undefined}
,"MessageEventSource" =>#{"Required" => "N", "Sequence" => undefined}
,"RndPx" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"OrdAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionAmountData" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"RateSource" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"J" => #{"Category"=>"app"
           ,"Name" => "AllocationInstruction"}

,"ListCancelRequest" => #{
                              "Category" => "app"
                              ,"Type" => "K"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"K" => #{"Category"=>"app"
           ,"Name" => "ListCancelRequest"}

,"ListExecute" => #{
                              "Category" => "app"
                              ,"Type" => "L"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClientBidID" =>#{"Required" => "N", "Sequence" => undefined}
,"BidID" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"L" => #{"Category"=>"app"
           ,"Name" => "ListExecute"}

,"ListStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "M"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"M" => #{"Category"=>"app"
           ,"Name" => "ListStatusRequest"}

,"ListStatus" => #{
                              "Category" => "app"
                              ,"Type" => "N"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListStatusType" =>#{"Required" => "Y", "Sequence" => undefined}
,"NoRpts" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListOrderStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"ContingencyType" =>#{"Required" => "N", "Sequence" => undefined}
,"ListRejectReason" =>#{"Required" => "N", "Sequence" => undefined}
,"RptSeq" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListStatusText" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedListStatusTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedListStatusText" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoOrders" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"OrdListStatGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"N" => #{"Category"=>"app"
           ,"Name" => "ListStatus"}

,"AllocationInstructionAck" => #{
                              "Category" => "app"
                              ,"Type" => "P"
                              ,"Fields" => #{"AllocID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocRejCode" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocType" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocIntermedReqType" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"Product" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAckGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"P" => #{"Category"=>"app"
           ,"Name" => "AllocationInstructionAck"}

,"DontKnowTrade" => #{
                              "Category" => "app"
                              ,"Type" => "Q"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "Y", "Sequence" => undefined}
,"DKReason" =>#{"Required" => "Y", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"Q" => #{"Category"=>"app"
           ,"Name" => "DontKnowTrade"}

,"QuoteRequest" => #{
                              "Category" => "app"
                              ,"Type" => "R"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"RFQReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"PrivateQuote" =>#{"Required" => "N", "Sequence" => undefined}
,"RespondentType" =>#{"Required" => "N", "Sequence" => undefined}
,"PreTradeAnonymity" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"RootParties" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotReqGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"R" => #{"Category"=>"app"
           ,"Name" => "QuoteRequest"}

,"Quote" => #{
                              "Category" => "app"
                              ,"Type" => "S"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteMsgID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRespID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"PrivateQuote" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteResponseLevel" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MktBidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MktOfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MinBidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"MinOfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSwapPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSwapPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"MidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"MidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferYield" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrBidFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrOfferFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestinationIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"QuotQualGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"LegQuotGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"RateSource" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"S" => #{"Category"=>"app"
           ,"Name" => "Quote"}

,"SettlementInstructions" => #{
                              "Category" => "app"
                              ,"Type" => "T"
                              ,"Fields" => #{"SettlInstMsgID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlInstReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstMode" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlInstReqRejCode" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"SettlInstGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"T" => #{"Category"=>"app"
           ,"Name" => "SettlementInstructions"}

,"MarketDataRequest" => #{
                              "Category" => "app"
                              ,"Type" => "V"
                              ,"Fields" => #{"MDReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MarketDepth" =>#{"Required" => "Y", "Sequence" => undefined}
,"MDUpdateType" =>#{"Required" => "N", "Sequence" => undefined}
,"AggregatedBook" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenCloseSettlFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"Scope" =>#{"Required" => "N", "Sequence" => undefined}
,"MDImplicitDelete" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplQueueAction" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplQueueMax" =>#{"Required" => "N", "Sequence" => undefined}
,"MDQuoteType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"MDReqGrp" =>#{"Required" => "Y", "Sequence" => undefined}
,"InstrmtMDReqGrp" =>#{"Required" => "Y", "Sequence" => undefined}
,"TrdgSesGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"V" => #{"Category"=>"app"
           ,"Name" => "MarketDataRequest"}

,"MarketDataSnapshotFullRefresh" => #{
                              "Category" => "app"
                              ,"Type" => "W"
                              ,"Fields" => #{"TotNumReports" =>#{"Required" => "N", "Sequence" => undefined}
,"MDReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDBookType" =>#{"Required" => "N", "Sequence" => undefined}
,"MDSubBookType" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketDepth" =>#{"Required" => "N", "Sequence" => undefined}
,"MDFeedType" =>#{"Required" => "N", "Sequence" => undefined}
,"RefreshIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancialStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"NetChgPrevDay" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplQueueDepth" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplQueueResolution" =>#{"Required" => "N", "Sequence" => undefined}
,"MDStreamID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"MDFullGrp" =>#{"Required" => "Y", "Sequence" => undefined}
,"RoutingGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"W" => #{"Category"=>"app"
           ,"Name" => "MarketDataSnapshotFullRefresh"}

,"MarketDataIncrementalRefresh" => #{
                              "Category" => "app"
                              ,"Type" => "X"
                              ,"Fields" => #{"MDBookType" =>#{"Required" => "N", "Sequence" => undefined}
,"MDFeedType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplQueueDepth" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplQueueResolution" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"MDIncGrp" =>#{"Required" => "Y", "Sequence" => undefined}
,"RoutingGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"X" => #{"Category"=>"app"
           ,"Name" => "MarketDataIncrementalRefresh"}

,"MarketDataRequestReject" => #{
                              "Category" => "app"
                              ,"Type" => "Y"
                              ,"Fields" => #{"MDReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"MDReqRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"MDRjctGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"Y" => #{"Category"=>"app"
           ,"Name" => "MarketDataRequestReject"}

,"QuoteCancel" => #{
                              "Category" => "app"
                              ,"Type" => "Z"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteMsgID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteCancelType" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteResponseLevel" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotCxlEntriesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"Z" => #{"Category"=>"app"
           ,"Name" => "QuoteCancel"}

,"QuoteStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "a"
                              ,"Fields" => #{"QuoteStatusReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"a" => #{"Category"=>"app"
           ,"Name" => "QuoteStatusRequest"}

,"MassQuoteAcknowledgement" => #{
                              "Category" => "app"
                              ,"Type" => "b"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteRejectReason" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteResponseLevel" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteCancelType" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotSetAckGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"b" => #{"Category"=>"app"
           ,"Name" => "MassQuoteAcknowledgement"}

,"SecurityDefinitionRequest" => #{
                              "Category" => "app"
                              ,"Type" => "c"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpirationCycle" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"c" => #{"Category"=>"app"
           ,"Name" => "SecurityDefinitionRequest"}

,"SecurityDefinition" => #{
                              "Category" => "app"
                              ,"Type" => "d"
                              ,"Fields" => #{"SecurityReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityResponseID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityResponseType" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"d" => #{"Category"=>"app"
           ,"Name" => "SecurityDefinition"}

,"SecurityStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "e"
                              ,"Fields" => #{"SecurityStatusReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"e" => #{"Category"=>"app"
           ,"Name" => "SecurityStatusRequest"}

,"SecurityStatus" => #{
                              "Category" => "app"
                              ,"Type" => "f"
                              ,"Fields" => #{"SecurityStatusReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnsolicitedIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityTradingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityTradingEvent" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancialStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"HaltReasonInt" =>#{"Required" => "N", "Sequence" => undefined}
,"InViewOfCommon" =>#{"Required" => "N", "Sequence" => undefined}
,"DueToRelated" =>#{"Required" => "N", "Sequence" => undefined}
,"MDBookType" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketDepth" =>#{"Required" => "N", "Sequence" => undefined}
,"BuyVolume" =>#{"Required" => "N", "Sequence" => undefined}
,"SellVolume" =>#{"Required" => "N", "Sequence" => undefined}
,"HighPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LowPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Adjustment" =>#{"Required" => "N", "Sequence" => undefined}
,"FirstPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"f" => #{"Category"=>"app"
           ,"Name" => "SecurityStatus"}

,"TradingSessionStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "g"
                              ,"Fields" => #{"TradSesReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesMode" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"g" => #{"Category"=>"app"
           ,"Name" => "TradingSessionStatusRequest"}

,"TradingSessionStatus" => #{
                              "Category" => "app"
                              ,"Type" => "h"
                              ,"Fields" => #{"TradSesReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesMode" =>#{"Required" => "N", "Sequence" => undefined}
,"UnsolicitedIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradSesEvent" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesStatusRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesStartTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesOpenTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesPreCloseTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesCloseTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesEndTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalVolumeTraded" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"h" => #{"Category"=>"app"
           ,"Name" => "TradingSessionStatus"}

,"MassQuote" => #{
                              "Category" => "app"
                              ,"Type" => "i"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteResponseLevel" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DefBidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"DefOfferSize" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotSetGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"i" => #{"Category"=>"app"
           ,"Name" => "MassQuote"}

,"BusinessMessageReject" => #{
                              "Category" => "app"
                              ,"Type" => "j"
                              ,"Fields" => #{"RefSeqNum" =>#{"Required" => "N", "Sequence" => undefined}
,"RefMsgType" =>#{"Required" => "Y", "Sequence" => undefined}
,"RefApplVerID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefApplExtID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefCstmApplVerID" =>#{"Required" => "N", "Sequence" => undefined}
,"BusinessRejectRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"BusinessRejectReason" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"j" => #{"Category"=>"app"
           ,"Name" => "BusinessMessageReject"}

,"BidRequest" => #{
                              "Category" => "app"
                              ,"Type" => "k"
                              ,"Fields" => #{"BidID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientBidID" =>#{"Required" => "Y", "Sequence" => undefined}
,"BidRequestTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"ListName" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoRelatedSym" =>#{"Required" => "Y", "Sequence" => undefined}
,"BidType" =>#{"Required" => "Y", "Sequence" => undefined}
,"NumTickets" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"SideValue1" =>#{"Required" => "N", "Sequence" => undefined}
,"SideValue2" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityIndType" =>#{"Required" => "N", "Sequence" => undefined}
,"WtAverageLiquidity" =>#{"Required" => "N", "Sequence" => undefined}
,"ExchangeForPhysical" =>#{"Required" => "N", "Sequence" => undefined}
,"OutMainCntryUIndex" =>#{"Required" => "N", "Sequence" => undefined}
,"CrossPercent" =>#{"Required" => "N", "Sequence" => undefined}
,"ProgRptReqs" =>#{"Required" => "N", "Sequence" => undefined}
,"ProgPeriodInterval" =>#{"Required" => "N", "Sequence" => undefined}
,"IncTaxInd" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"NumBidders" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidTradeType" =>#{"Required" => "Y", "Sequence" => undefined}
,"BasisPxType" =>#{"Required" => "Y", "Sequence" => undefined}
,"StrikeTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"BidDescReqGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"BidCompReqGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"k" => #{"Category"=>"app"
           ,"Name" => "BidRequest"}

,"BidResponse" => #{
                              "Category" => "app"
                              ,"Type" => "l"
                              ,"Fields" => #{"BidID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClientBidID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"BidCompRspGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"l" => #{"Category"=>"app"
           ,"Name" => "BidResponse"}

,"ListStrikePrice" => #{
                              "Category" => "app"
                              ,"Type" => "m"
                              ,"Fields" => #{"ListID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TotNoStrikes" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrmtStrkPxGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"m" => #{"Category"=>"app"
           ,"Name" => "ListStrikePrice"}

,"RegistrationInstructions" => #{
                              "Category" => "app"
                              ,"Type" => "o"
                              ,"Fields" => #{"RegistID" =>#{"Required" => "Y", "Sequence" => undefined}
,"RegistTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"RegistRefID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistAcctType" =>#{"Required" => "N", "Sequence" => undefined}
,"TaxAdvantageType" =>#{"Required" => "N", "Sequence" => undefined}
,"OwnershipType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"RgstDtlsGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"RgstDistInstGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"o" => #{"Category"=>"app"
           ,"Name" => "RegistrationInstructions"}

,"RegistrationInstructionsResponse" => #{
                              "Category" => "app"
                              ,"Type" => "p"
                              ,"Fields" => #{"RegistID" =>#{"Required" => "Y", "Sequence" => undefined}
,"RegistTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"RegistRefID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"RegistRejReasonCode" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistRejReasonText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"p" => #{"Category"=>"app"
           ,"Name" => "RegistrationInstructionsResponse"}

,"OrderMassCancelRequest" => #{
                              "Category" => "app"
                              ,"Type" => "q"
                              ,"Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"MassCancelRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"q" => #{"Category"=>"app"
           ,"Name" => "OrderMassCancelRequest"}

,"OrderMassCancelReport" => #{
                              "Category" => "app"
                              ,"Type" => "r"
                              ,"Fields" => #{"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "Y", "Sequence" => undefined}
,"MassActionReportID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"MassCancelRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MassCancelResponse" =>#{"Required" => "Y", "Sequence" => undefined}
,"MassCancelRejectReason" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalAffectedOrders" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"AffectedOrdGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"NotAffectedOrdersGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"r" => #{"Category"=>"app"
           ,"Name" => "OrderMassCancelReport"}

,"NewOrderCross" => #{
                              "Category" => "app"
                              ,"Type" => "s"
                              ,"Fields" => #{"CrossID" =>#{"Required" => "Y", "Sequence" => undefined}
,"CrossType" =>#{"Required" => "Y", "Sequence" => undefined}
,"CrossPrioritization" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchIncrement" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxPriceLevels" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestinationIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransBkdTime" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceProtectionScope" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategy" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategyParameters" =>#{"Required" => "N", "Sequence" => undefined}
,"ParticipationRate" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"RootParties" =>#{"Required" => "N", "Sequence" => undefined}
,"SideCrossOrdModGrp" =>#{"Required" => "Y", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"DisplayInstruction" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdgSesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"TriggeringInstruction" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"PegInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"StrategyParametersGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"s" => #{"Category"=>"app"
           ,"Name" => "NewOrderCross"}

,"CrossOrderCancelReplaceRequest" => #{
                              "Category" => "app"
                              ,"Type" => "t"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"CrossID" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigCrossID" =>#{"Required" => "Y", "Sequence" => undefined}
,"HostCrossID" =>#{"Required" => "N", "Sequence" => undefined}
,"CrossType" =>#{"Required" => "Y", "Sequence" => undefined}
,"CrossPrioritization" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchIncrement" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxPriceLevels" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestinationIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransBkdTime" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceProtectionScope" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategy" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategyParameters" =>#{"Required" => "N", "Sequence" => undefined}
,"ParticipationRate" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"RootParties" =>#{"Required" => "N", "Sequence" => undefined}
,"SideCrossOrdModGrp" =>#{"Required" => "Y", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"DisplayInstruction" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdgSesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"TriggeringInstruction" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"PegInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"StrategyParametersGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"t" => #{"Category"=>"app"
           ,"Name" => "CrossOrderCancelReplaceRequest"}

,"CrossOrderCancelRequest" => #{
                              "Category" => "app"
                              ,"Type" => "u"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"CrossID" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigCrossID" =>#{"Required" => "Y", "Sequence" => undefined}
,"HostCrossID" =>#{"Required" => "N", "Sequence" => undefined}
,"CrossType" =>#{"Required" => "Y", "Sequence" => undefined}
,"CrossPrioritization" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"RootParties" =>#{"Required" => "N", "Sequence" => undefined}
,"SideCrossOrdCxlGrp" =>#{"Required" => "Y", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"u" => #{"Category"=>"app"
           ,"Name" => "CrossOrderCancelRequest"}

,"SecurityTypeRequest" => #{
                              "Category" => "app"
                              ,"Type" => "v"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Product" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySubType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"v" => #{"Category"=>"app"
           ,"Name" => "SecurityTypeRequest"}

,"SecurityTypes" => #{
                              "Category" => "app"
                              ,"Type" => "w"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityResponseID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityResponseType" =>#{"Required" => "Y", "Sequence" => undefined}
,"TotNoSecurityTypes" =>#{"Required" => "N", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"SecTypesGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"w" => #{"Category"=>"app"
           ,"Name" => "SecurityTypes"}

,"SecurityListRequest" => #{
                              "Category" => "app"
                              ,"Type" => "x"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityListRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityListID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityListType" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityListTypeSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"x" => #{"Category"=>"app"
           ,"Name" => "SecurityListRequest"}

,"SecurityList" => #{
                              "Category" => "app"
                              ,"Type" => "y"
                              ,"Fields" => #{"SecurityReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityResponseID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityRequestResult" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoRelatedSym" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityListID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityListRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityListDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityListDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityListDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityListType" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityListTypeSource" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"SecListGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"y" => #{"Category"=>"app"
           ,"Name" => "SecurityList"}

,"DerivativeSecurityListRequest" => #{
                              "Category" => "app"
                              ,"Type" => "z"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecurityListRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySubType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeInstrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"z" => #{"Category"=>"app"
           ,"Name" => "DerivativeSecurityListRequest"}

,"DerivativeSecurityList" => #{
                              "Category" => "app"
                              ,"Type" => "AA"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityResponseID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityRequestResult" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoRelatedSym" =>#{"Required" => "N", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeSecurityDefinition" =>#{"Required" => "N", "Sequence" => undefined}
,"RelSymDerivSecGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AA" => #{"Category"=>"app"
           ,"Name" => "DerivativeSecurityList"}

,"NewOrderMultileg" => #{
                              "Category" => "app"
                              ,"Type" => "AB"
                              ,"Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchIncrement" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxPriceLevels" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestinationIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"SwapPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MultilegModel" =>#{"Required" => "N", "Sequence" => undefined}
,"MultilegPriceMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceProtectionScope" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefOrderIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"PreTradeAnonymity" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategy" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategyParameters" =>#{"Required" => "N", "Sequence" => undefined}
,"RiskFreeRate" =>#{"Required" => "N", "Sequence" => undefined}
,"ParticipationRate" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}
,"MultiLegRptTypeReq" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"PreAllocMlegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"DisplayInstruction" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdgSesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOrdGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "N", "Sequence" => undefined}
,"TriggeringInstruction" =>#{"Required" => "N", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
,"PegInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"StrategyParametersGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AB" => #{"Category"=>"app"
           ,"Name" => "NewOrderMultileg"}

,"MultilegOrderCancelReplace" => #{
                              "Category" => "app"
                              ,"Type" => "AC"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigOrdModTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchIncrement" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxPriceLevels" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestinationIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"SwapPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MultilegModel" =>#{"Required" => "N", "Sequence" => undefined}
,"MultilegPriceMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceProtectionScope" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"PreTradeAnonymity" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategy" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategyParameters" =>#{"Required" => "N", "Sequence" => undefined}
,"RiskFreeRate" =>#{"Required" => "N", "Sequence" => undefined}
,"ParticipationRate" =>#{"Required" => "N", "Sequence" => undefined}
,"CancellationRights" =>#{"Required" => "N", "Sequence" => undefined}
,"MoneyLaunderingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistID" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}
,"MultiLegRptTypeReq" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"PreAllocMlegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"DisplayInstruction" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdgSesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOrdGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
,"TriggeringInstruction" =>#{"Required" => "N", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
,"PegInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"StrategyParametersGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AC" => #{"Category"=>"app"
           ,"Name" => "MultilegOrderCancelReplace"}

,"TradeCaptureReportRequest" => #{
                              "Category" => "app"
                              ,"Type" => "AD"
                              ,"Fields" => #{"TradeRequestID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"FirmTradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryFirmTradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTradeReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdSubType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeHandlingInstr" =>#{"Required" => "N", "Sequence" => undefined}
,"TransferReason" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdMatchID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeBracket" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"MultiLegReportingType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputSource" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputDevice" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseTransportType" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"MessageEventSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdCapDtGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AD" => #{"Category"=>"app"
           ,"Name" => "TradeCaptureReportRequest"}

,"TradeCaptureReport" => #{
                              "Category" => "app"
                              ,"Type" => "AE"
                              ,"Fields" => #{"TradeReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"FirmTradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryFirmTradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeReportTransType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeReportType" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRptStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeRequestID" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdSubType" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeHandlingInstr" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigTradeHandlingInstr" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigTradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigTradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigSecondaryTradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"TransferReason" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecType" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNumTradeReports" =>#{"Required" => "N", "Sequence" => undefined}
,"LastRptRequested" =>#{"Required" => "N", "Sequence" => undefined}
,"UnsolicitedIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeReportRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTradeReportRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTradeReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdMatchID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecRestatementReason" =>#{"Required" => "N", "Sequence" => undefined}
,"PreviouslyReported" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"AsOfIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingTradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingTradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"LastQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"CalculatedCcyLastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"LastParPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"LastForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"LastSwapPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPxIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"MultiLegReportingType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeLegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSettlementDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchType" =>#{"Required" => "N", "Sequence" => undefined}
,"Volatility" =>#{"Required" => "N", "Sequence" => undefined}
,"DividendYield" =>#{"Required" => "N", "Sequence" => undefined}
,"RiskFreeRate" =>#{"Required" => "N", "Sequence" => undefined}
,"CurrencyRatio" =>#{"Required" => "N", "Sequence" => undefined}
,"CopyMsgIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"PublishTrdIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"TradePublishIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"ShortSaleReason" =>#{"Required" => "N", "Sequence" => undefined}
,"TierCode" =>#{"Required" => "N", "Sequence" => undefined}
,"MessageEventSource" =>#{"Required" => "N", "Sequence" => undefined}
,"LastUpdateTime" =>#{"Required" => "N", "Sequence" => undefined}
,"RndPx" =>#{"Required" => "N", "Sequence" => undefined}
,"TZTransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ReportedPxDiff" =>#{"Required" => "N", "Sequence" => undefined}
,"GrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"RejectText" =>#{"Required" => "N", "Sequence" => undefined}
,"FeeMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"VenueType" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"RootParties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionAmountData" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdInstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestamps" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdCapRptSideGrp" =>#{"Required" => "Y", "Sequence" => undefined}
,"TrdRepIndicatorsGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AE" => #{"Category"=>"app"
           ,"Name" => "TradeCaptureReport"}

,"OrderMassStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "AF"
                              ,"Fields" => #{"MassStatusReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"MassStatusReqType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AF" => #{"Category"=>"app"
           ,"Name" => "OrderMassStatusRequest"}

,"QuoteRequestReject" => #{
                              "Category" => "app"
                              ,"Type" => "AG"
                              ,"Fields" => #{"QuoteReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"RFQReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestRejectReason" =>#{"Required" => "Y", "Sequence" => undefined}
,"PrivateQuote" =>#{"Required" => "N", "Sequence" => undefined}
,"RespondentType" =>#{"Required" => "N", "Sequence" => undefined}
,"PreTradeAnonymity" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"RootParties" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotReqRjctGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"AG" => #{"Category"=>"app"
           ,"Name" => "QuoteRequestReject"}

,"RFQRequest" => #{
                              "Category" => "app"
                              ,"Type" => "AH"
                              ,"Fields" => #{"RFQReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"PrivateQuote" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"RFQReqGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"AH" => #{"Category"=>"app"
           ,"Name" => "RFQRequest"}

,"QuoteStatusReport" => #{
                              "Category" => "app"
                              ,"Type" => "AI"
                              ,"Fields" => #{"QuoteStatusReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteMsgID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRespID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteCancelType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MktBidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MktOfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MinBidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"MinOfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"MidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"MidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferYield" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrBidFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrOfferFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestinationIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRejectReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"LegQuotStatGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotQualGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AI" => #{"Category"=>"app"
           ,"Name" => "QuoteStatusReport"}

,"QuoteResponse" => #{
                              "Category" => "app"
                              ,"Type" => "AJ"
                              ,"Fields" => #{"QuoteRespID" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteMsgID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRespType" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"PreTradeAnonymity" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MktBidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MktOfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MinBidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"MinOfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"MidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"MidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferYield" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrBidFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrOfferFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestinationIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"QuotQualGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"LegQuotGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AJ" => #{"Category"=>"app"
           ,"Name" => "QuoteResponse"}

,"Confirmation" => #{
                              "Category" => "app"
                              ,"Type" => "AK"
                              ,"Fields" => #{"ConfirmID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ConfirmRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"ConfirmReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"ConfirmTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"ConfirmType" =>#{"Required" => "Y", "Sequence" => undefined}
,"CopyMsgIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"LegalConfirm" =>#{"Required" => "N", "Sequence" => undefined}
,"ConfirmStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"AvgPxPrecision" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgParPx" =>#{"Required" => "N", "Sequence" => undefined}
,"ReportedPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"GrossTradeAmt" =>#{"Required" => "Y", "Sequence" => undefined}
,"NumDaysInterest" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"InterestAtMaturity" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"Concession" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalTakedown" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "Y", "Sequence" => undefined}
,"MaturityNetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"SharedCommission" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestamps" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"CpctyConfGrp" =>#{"Required" => "Y", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstructionsData" =>#{"Required" => "N", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeesGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AK" => #{"Category"=>"app"
           ,"Name" => "Confirmation"}

,"PositionMaintenanceRequest" => #{
                              "Category" => "app"
                              ,"Type" => "AL"
                              ,"Fields" => #{"PosReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"PosTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PosMaintAction" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigPosReqRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"PosMaintRptRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"AdjustmentType" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraryInstructionIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"PriorSpreadIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"ThresholdAmount" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "Y", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdgSesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"PositionAmountData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AL" => #{"Category"=>"app"
           ,"Name" => "PositionMaintenanceRequest"}

,"PositionMaintenanceReport" => #{
                              "Category" => "app"
                              ,"Type" => "AM"
                              ,"Fields" => #{"PosMaintRptID" =>#{"Required" => "Y", "Sequence" => undefined}
,"PosTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"PosReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"PosMaintAction" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigPosReqRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"PosMaintStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"PosMaintResult" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"PosMaintRptRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraryInstructionIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"PriorSpreadIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"AdjustmentType" =>#{"Required" => "N", "Sequence" => undefined}
,"ThresholdAmount" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdgSesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"PositionAmountData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AM" => #{"Category"=>"app"
           ,"Name" => "PositionMaintenanceReport"}

,"RequestForPositions" => #{
                              "Category" => "app"
                              ,"Type" => "AN"
                              ,"Fields" => #{"PosReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"PosReqType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MatchStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"ResponseTransportType" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "Y", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdgSesGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AN" => #{"Category"=>"app"
           ,"Name" => "RequestForPositions"}

,"RequestForPositionsAck" => #{
                              "Category" => "app"
                              ,"Type" => "AO"
                              ,"Fields" => #{"PosMaintRptID" =>#{"Required" => "Y", "Sequence" => undefined}
,"PosReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalNumPosReports" =>#{"Required" => "N", "Sequence" => undefined}
,"UnsolicitedIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"PosReqResult" =>#{"Required" => "Y", "Sequence" => undefined}
,"PosReqStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"PosReqType" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseTransportType" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "Y", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AO" => #{"Category"=>"app"
           ,"Name" => "RequestForPositionsAck"}

,"PositionReport" => #{
                              "Category" => "app"
                              ,"Type" => "AP"
                              ,"Fields" => #{"PosMaintRptID" =>#{"Required" => "Y", "Sequence" => undefined}
,"PosReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"PosReqType" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalNumPosReports" =>#{"Required" => "N", "Sequence" => undefined}
,"PosReqResult" =>#{"Required" => "N", "Sequence" => undefined}
,"UnsolicitedIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"MessageEventSource" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlPriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"PriorSettlPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"DeliveryDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"ModelType" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceDelta" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "Y", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"PosUndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionQty" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionAmountData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AP" => #{"Category"=>"app"
           ,"Name" => "PositionReport"}

,"TradeCaptureReportRequestAck" => #{
                              "Category" => "app"
                              ,"Type" => "AQ"
                              ,"Fields" => #{"TradeRequestID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"FirmTradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryFirmTradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNumTradeReports" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeRequestResult" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeRequestStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"MultiLegReportingType" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseTransportType" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"MessageEventSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AQ" => #{"Category"=>"app"
           ,"Name" => "TradeCaptureReportRequestAck"}

,"TradeCaptureReportAck" => #{
                              "Category" => "app"
                              ,"Type" => "AR"
                              ,"Fields" => #{"TradeReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"FirmTradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryFirmTradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeReportTransType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeReportType" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdSubType" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeHandlingInstr" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigTradeHandlingInstr" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigTradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigTradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigSecondaryTradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"TransferReason" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeReportRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTradeReportRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRptStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeReportRejectReason" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTradeReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdMatchID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecRestatementReason" =>#{"Required" => "N", "Sequence" => undefined}
,"PreviouslyReported" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingTradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingTradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"LastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastParPx" =>#{"Required" => "N", "Sequence" => undefined}
,"CalculatedCcyLastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LastSwapPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"LastSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"LastForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPxIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"MultiLegReportingType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeLegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchType" =>#{"Required" => "N", "Sequence" => undefined}
,"CopyMsgIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"PublishTrdIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"TradePublishIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"ShortSaleReason" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseTransportType" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"AsOfIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"TierCode" =>#{"Required" => "N", "Sequence" => undefined}
,"MessageEventSource" =>#{"Required" => "N", "Sequence" => undefined}
,"LastUpdateTime" =>#{"Required" => "N", "Sequence" => undefined}
,"RndPx" =>#{"Required" => "N", "Sequence" => undefined}
,"RptSys" =>#{"Required" => "N", "Sequence" => undefined}
,"GrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"FeeMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"VenueType" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"RootParties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRepIndicatorsGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdInstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestamps" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionAmountData" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdCapRptAckSideGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AR" => #{"Category"=>"app"
           ,"Name" => "TradeCaptureReportAck"}

,"AllocationReport" => #{
                              "Category" => "app"
                              ,"Type" => "AS"
                              ,"Fields" => #{"AllocReportID" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocReportRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocCancReplaceReason" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocReportType" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocRejCode" =>#{"Required" => "N", "Sequence" => undefined}
,"RefAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocIntermedReqType" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocLinkType" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdSubType" =>#{"Required" => "N", "Sequence" => undefined}
,"MultiLegReportingType" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputSource" =>#{"Required" => "N", "Sequence" => undefined}
,"RndPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MessageEventSource" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputDevice" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPxIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocNoOrdersType" =>#{"Required" => "N", "Sequence" => undefined}
,"PreviouslyReported" =>#{"Required" => "N", "Sequence" => undefined}
,"ReversalIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchType" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"Quantity" =>#{"Required" => "Y", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"AvgParPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPxPrecision" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"GrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"Concession" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalTakedown" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"AutoAcceptIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"NumDaysInterest" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"InterestAtMaturity" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"LegalConfirm" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoAllocs" =>#{"Required" => "N", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"OrdAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionAmountData" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"RateSource" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AS" => #{"Category"=>"app"
           ,"Name" => "AllocationReport"}

,"AllocationReportAck" => #{
                              "Category" => "app"
                              ,"Type" => "AT"
                              ,"Fields" => #{"AllocReportID" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPxIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"Quantity" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocTransType" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocRejCode" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocReportType" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocIntermedReqType" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"Product" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAckGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AT" => #{"Category"=>"app"
           ,"Name" => "AllocationReportAck"}

,"ConfirmationAck" => #{
                              "Category" => "app"
                              ,"Type" => "AU"
                              ,"Fields" => #{"ConfirmID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"AffirmStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"ConfirmRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"AU" => #{"Category"=>"app"
           ,"Name" => "ConfirmationAck"}

,"SettlementInstructionRequest" => #{
                              "Category" => "app"
                              ,"Type" => "AV"
                              ,"Fields" => #{"SettlInstReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Product" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"CFICode" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"LastUpdateTime" =>#{"Required" => "N", "Sequence" => undefined}
,"StandInstDbType" =>#{"Required" => "N", "Sequence" => undefined}
,"StandInstDbName" =>#{"Required" => "N", "Sequence" => undefined}
,"StandInstDbID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AV" => #{"Category"=>"app"
           ,"Name" => "SettlementInstructionRequest"}

,"AssignmentReport" => #{
                              "Category" => "app"
                              ,"Type" => "AW"
                              ,"Fields" => #{"AsgnRptID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TotNumAssignmentReports" =>#{"Required" => "N", "Sequence" => undefined}
,"LastRptRequested" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ThresholdAmount" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlPriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSettlPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"PriorSettlPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AssignmentMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AssignmentUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenInterest" =>#{"Required" => "N", "Sequence" => undefined}
,"ExerciseMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"PosReqID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "Y", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionQty" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionAmountData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AW" => #{"Category"=>"app"
           ,"Name" => "AssignmentReport"}

,"CollateralRequest" => #{
                              "Category" => "app"
                              ,"Type" => "AX"
                              ,"Fields" => #{"CollReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"CollAsgnReason" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Quantity" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MarginExcess" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalNetValue" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOutstanding" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestamps" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AX" => #{"Category"=>"app"
           ,"Name" => "CollateralRequest"}

,"CollateralAssignment" => #{
                              "Category" => "app"
                              ,"Type" => "AY"
                              ,"Fields" => #{"CollAsgnID" =>#{"Required" => "Y", "Sequence" => undefined}
,"CollReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"CollAsgnReason" =>#{"Required" => "Y", "Sequence" => undefined}
,"CollAsgnTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"CollAsgnRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Quantity" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MarginExcess" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalNetValue" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOutstanding" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestamps" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstructionsData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AY" => #{"Category"=>"app"
           ,"Name" => "CollateralAssignment"}

,"CollateralResponse" => #{
                              "Category" => "app"
                              ,"Type" => "AZ"
                              ,"Fields" => #{"CollRespID" =>#{"Required" => "Y", "Sequence" => undefined}
,"CollAsgnID" =>#{"Required" => "N", "Sequence" => undefined}
,"CollReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"CollAsgnReason" =>#{"Required" => "N", "Sequence" => undefined}
,"CollAsgnTransType" =>#{"Required" => "N", "Sequence" => undefined}
,"CollAsgnRespType" =>#{"Required" => "Y", "Sequence" => undefined}
,"CollAsgnRejectReason" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"CollApplType" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancialStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Quantity" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MarginExcess" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalNetValue" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOutstanding" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestamps" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"AZ" => #{"Category"=>"app"
           ,"Name" => "CollateralResponse"}

,"CollateralReport" => #{
                              "Category" => "app"
                              ,"Type" => "BA"
                              ,"Fields" => #{"CollRptID" =>#{"Required" => "Y", "Sequence" => undefined}
,"CollInquiryID" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"CollApplType" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancialStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"CollStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"TotNumReports" =>#{"Required" => "N", "Sequence" => undefined}
,"LastRptRequested" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Quantity" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MarginExcess" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalNetValue" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOutstanding" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestamps" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstructionsData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"BA" => #{"Category"=>"app"
           ,"Name" => "CollateralReport"}

,"CollateralInquiry" => #{
                              "Category" => "app"
                              ,"Type" => "BB"
                              ,"Fields" => #{"CollInquiryID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseTransportType" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Quantity" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MarginExcess" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalNetValue" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOutstanding" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"CollInqQualGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestamps" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstructionsData" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"BB" => #{"Category"=>"app"
           ,"Name" => "CollateralInquiry"}

,"NetworkCounterpartySystemStatusRequest" => #{
                              "Category" => "app"
                              ,"Type" => "BC"
                              ,"Fields" => #{"NetworkRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"NetworkRequestID" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"CompIDReqGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"BC" => #{"Category"=>"app"
           ,"Name" => "NetworkCounterpartySystemStatusRequest"}

,"NetworkCounterpartySystemStatusResponse" => #{
                              "Category" => "app"
                              ,"Type" => "BD"
                              ,"Fields" => #{"NetworkStatusResponseType" =>#{"Required" => "Y", "Sequence" => undefined}
,"NetworkRequestID" =>#{"Required" => "N", "Sequence" => undefined}
,"NetworkResponseID" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastNetworkResponseID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"CompIDStatGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"BD" => #{"Category"=>"app"
           ,"Name" => "NetworkCounterpartySystemStatusResponse"}

,"UserRequest" => #{
                              "Category" => "app"
                              ,"Type" => "BE"
                              ,"Fields" => #{"UserRequestID" =>#{"Required" => "Y", "Sequence" => undefined}
,"UserRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Username" =>#{"Required" => "Y", "Sequence" => undefined}
,"Password" =>#{"Required" => "N", "Sequence" => undefined}
,"NewPassword" =>#{"Required" => "N", "Sequence" => undefined}
,"EncryptedPasswordMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"EncryptedPasswordLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncryptedPassword" =>#{"Required" => "N", "Sequence" => undefined}
,"EncryptedNewPasswordLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncryptedNewPassword" =>#{"Required" => "N", "Sequence" => undefined}
,"RawDataLength" =>#{"Required" => "N", "Sequence" => undefined}
,"RawData" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"BE" => #{"Category"=>"app"
           ,"Name" => "UserRequest"}

,"UserResponse" => #{
                              "Category" => "app"
                              ,"Type" => "BF"
                              ,"Fields" => #{"UserRequestID" =>#{"Required" => "Y", "Sequence" => undefined}
,"Username" =>#{"Required" => "Y", "Sequence" => undefined}
,"UserStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"UserStatusText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"BF" => #{"Category"=>"app"
           ,"Name" => "UserResponse"}

,"CollateralInquiryAck" => #{
                              "Category" => "app"
                              ,"Type" => "BG"
                              ,"Fields" => #{"CollInquiryID" =>#{"Required" => "Y", "Sequence" => undefined}
,"CollInquiryStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"CollInquiryResult" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNumReports" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Quantity" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlSessSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseTransportType" =>#{"Required" => "N", "Sequence" => undefined}
,"ResponseDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"CollInqQualGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdCollGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"BG" => #{"Category"=>"app"
           ,"Name" => "CollateralInquiryAck"}

,"ConfirmationRequest" => #{
                              "Category" => "app"
                              ,"Type" => "BH"
                              ,"Fields" => #{"ConfirmReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ConfirmType" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"OrdAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"BH" => #{"Category"=>"app"
           ,"Name" => "ConfirmationRequest"}

,"ContraryIntentionReport" => #{
                              "Category" => "app"
                              ,"Type" => "BO"
                              ,"Fields" => #{"ContIntRptID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"LateIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"InputSource" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExpirationQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"BO" => #{"Category"=>"app"
           ,"Name" => "ContraryIntentionReport"}

,"SecurityDefinitionUpdateReport" => #{
                              "Category" => "app"
                              ,"Type" => "BP"
                              ,"Fields" => #{"SecurityReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityResponseID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityResponseType" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityUpdateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"BP" => #{"Category"=>"app"
           ,"Name" => "SecurityDefinitionUpdateReport"}

,"SecurityListUpdateReport" => #{
                              "Category" => "app"
                              ,"Type" => "BK"
                              ,"Fields" => #{"SecurityReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityResponseID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityRequestResult" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoRelatedSym" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityUpdateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityListID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityListRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityListDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityListDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityListDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityListType" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityListTypeSource" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"SecLstUpdRelSymGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"BK" => #{"Category"=>"app"
           ,"Name" => "SecurityListUpdateReport"}

,"AdjustedPositionReport" => #{
                              "Category" => "app"
                              ,"Type" => "BL"
                              ,"Fields" => #{"PosMaintRptID" =>#{"Required" => "Y", "Sequence" => undefined}
,"PosReqType" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlSessID" =>#{"Required" => "N", "Sequence" => undefined}
,"PosMaintRptRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"PriorSettlPrice" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "Y", "Sequence" => undefined}
,"PositionQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"InstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"BL" => #{"Category"=>"app"
           ,"Name" => "AdjustedPositionReport"}

,"AllocationInstructionAlert" => #{
                              "Category" => "app"
                              ,"Type" => "BM"
                              ,"Fields" => #{"AllocID" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocTransType" =>#{"Required" => "Y", "Sequence" => undefined}
,"AllocType" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocCancReplaceReason" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocIntermedReqType" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocLinkType" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocNoOrdersType" =>#{"Required" => "N", "Sequence" => undefined}
,"PreviouslyReported" =>#{"Required" => "N", "Sequence" => undefined}
,"ReversalIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchType" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"Quantity" =>#{"Required" => "Y", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgParPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPxPrecision" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "Y", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"GrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"Concession" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalTakedown" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"AutoAcceptIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"NumDaysInterest" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"InterestAtMaturity" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"LegalConfirm" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoAllocs" =>#{"Required" => "N", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPxIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdSubType" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputSource" =>#{"Required" => "N", "Sequence" => undefined}
,"MultiLegReportingType" =>#{"Required" => "N", "Sequence" => undefined}
,"MessageEventSource" =>#{"Required" => "N", "Sequence" => undefined}
,"RndPx" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"OrdAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionAmountData" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"BM" => #{"Category"=>"app"
           ,"Name" => "AllocationInstructionAlert"}

,"ExecutionAcknowledgement" => #{
                              "Category" => "app"
                              ,"Type" => "BN"
                              ,"Fields" => #{"OrderID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecAckStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "Y", "Sequence" => undefined}
,"DKReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"LastParPx" =>#{"Required" => "N", "Sequence" => undefined}
,"CumQty" =>#{"Required" => "N", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"BN" => #{"Category"=>"app"
           ,"Name" => "ExecutionAcknowledgement"}

,"TradingSessionList" => #{
                              "Category" => "app"
                              ,"Type" => "BJ"
                              ,"Fields" => #{"TradSesReqID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdSessLstGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"BJ" => #{"Category"=>"app"
           ,"Name" => "TradingSessionList"}

,"TradingSessionListRequest" => #{
                              "Category" => "app"
                              ,"Type" => "BI"
                              ,"Fields" => #{"TradSesReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesMode" =>#{"Required" => "N", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"BI" => #{"Category"=>"app"
           ,"Name" => "TradingSessionListRequest"}

,"SettlementObligationReport" => #{
                              "Category" => "app"
                              ,"Type" => "BQ"
                              ,"Fields" => #{"ClearingBusinessDate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlementCycleNo" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlObligMsgID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SettlObligMode" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlObligationInstructions" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"BQ" => #{"Category"=>"app"
           ,"Name" => "SettlementObligationReport"}

,"DerivativeSecurityListUpdateReport" => #{
                              "Category" => "app"
                              ,"Type" => "BR"
                              ,"Fields" => #{"SecurityReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityResponseID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityRequestResult" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityUpdateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoRelatedSym" =>#{"Required" => "N", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeSecurityDefinition" =>#{"Required" => "N", "Sequence" => undefined}
,"RelSymDerivSecUpdGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"BR" => #{"Category"=>"app"
           ,"Name" => "DerivativeSecurityListUpdateReport"}

,"TradingSessionListUpdateReport" => #{
                              "Category" => "app"
                              ,"Type" => "BS"
                              ,"Fields" => #{"TradSesReqID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdSessLstGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"BS" => #{"Category"=>"app"
           ,"Name" => "TradingSessionListUpdateReport"}

,"MarketDefinitionRequest" => #{
                              "Category" => "app"
                              ,"Type" => "BT"
                              ,"Fields" => #{"MarketReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SubscriptionRequestType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"ParentMktSegmID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"BT" => #{"Category"=>"app"
           ,"Name" => "MarketDefinitionRequest"}

,"MarketDefinition" => #{
                              "Category" => "app"
                              ,"Type" => "BU"
                              ,"Fields" => #{"MarketReportID" =>#{"Required" => "Y", "Sequence" => undefined}
,"MarketReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "Y", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedMktSegmDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedMktSegmDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"ParentMktSegmID" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"BaseTradingRules" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdTypeRules" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForceRules" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInstRules" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"BU" => #{"Category"=>"app"
           ,"Name" => "MarketDefinition"}

,"MarketDefinitionUpdateReport" => #{
                              "Category" => "app"
                              ,"Type" => "BV"
                              ,"Fields" => #{"MarketReportID" =>#{"Required" => "Y", "Sequence" => undefined}
,"MarketReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketUpdateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "Y", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedMktSegmDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedMktSegmDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"ParentMktSegmID" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplicationSequenceControl" =>#{"Required" => "N", "Sequence" => undefined}
,"BaseTradingRules" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdTypeRules" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForceRules" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInstRules" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"BV" => #{"Category"=>"app"
           ,"Name" => "MarketDefinitionUpdateReport"}

,"ApplicationMessageRequest" => #{
                              "Category" => "app"
                              ,"Type" => "BW"
                              ,"Fields" => #{"ApplReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ApplReqType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplIDRequestGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"BW" => #{"Category"=>"app"
           ,"Name" => "ApplicationMessageRequest"}

,"ApplicationMessageRequestAck" => #{
                              "Category" => "app"
                              ,"Type" => "BX"
                              ,"Fields" => #{"ApplResponseID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ApplReqID" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplReqType" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplResponseType" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplTotalMessageCount" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplIDRequestAckGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"BX" => #{"Category"=>"app"
           ,"Name" => "ApplicationMessageRequestAck"}

,"ApplicationMessageReport" => #{
                              "Category" => "app"
                              ,"Type" => "BY"
                              ,"Fields" => #{"ApplReportID" =>#{"Required" => "Y", "Sequence" => undefined}
,"ApplReportType" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplReqID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ApplIDReportGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"BY" => #{"Category"=>"app"
           ,"Name" => "ApplicationMessageReport"}

,"OrderMassActionReport" => #{
                              "Category" => "app"
                              ,"Type" => "BZ"
                              ,"Fields" => #{"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"MassActionReportID" =>#{"Required" => "Y", "Sequence" => undefined}
,"MassActionType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MassActionScope" =>#{"Required" => "Y", "Sequence" => undefined}
,"MassActionResponse" =>#{"Required" => "Y", "Sequence" => undefined}
,"MassActionRejectReason" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalAffectedOrders" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"AffectedOrdGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"NotAffectedOrdersGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"BZ" => #{"Category"=>"app"
           ,"Name" => "OrderMassActionReport"}

,"OrderMassActionRequest" => #{
                              "Category" => "app"
                              ,"Type" => "CA"
                              ,"Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"MassActionType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MassActionScope" =>#{"Required" => "Y", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"CA" => #{"Category"=>"app"
           ,"Name" => "OrderMassActionRequest"}

,"UserNotification" => #{
                              "Category" => "app"
                              ,"Type" => "CB"
                              ,"Fields" => #{"UserStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"UsernameGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"CB" => #{"Category"=>"app"
           ,"Name" => "UserNotification"}

,"StreamAssignmentRequest" => #{
                              "Category" => "app"
                              ,"Type" => "CC"
                              ,"Fields" => #{"StreamAsgnReqID" =>#{"Required" => "Y", "Sequence" => undefined}
,"StreamAsgnReqType" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"StrmAsgnReqGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}
,"CC" => #{"Category"=>"app"
           ,"Name" => "StreamAssignmentRequest"}

,"StreamAssignmentReport" => #{
                              "Category" => "app"
                              ,"Type" => "CD"
                              ,"Fields" => #{"StreamAsgnRptID" =>#{"Required" => "Y", "Sequence" => undefined}
,"StreamAsgnReqType" =>#{"Required" => "N", "Sequence" => undefined}
,"StreamAsgnReqID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"StrmAsgnRptGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"CD" => #{"Category"=>"app"
           ,"Name" => "StreamAssignmentReport"}

,"StreamAssignmentReportACK" => #{
                              "Category" => "app"
                              ,"Type" => "CE"
                              ,"Fields" => #{"StreamAsgnAckType" =>#{"Required" => "Y", "Sequence" => undefined}
,"StreamAsgnRptID" =>#{"Required" => "Y", "Sequence" => undefined}
,"StreamAsgnRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"CE" => #{"Category"=>"app"
           ,"Name" => "StreamAssignmentReportACK"}
}.


fields() ->
#{
"Account" => #{"TagNum" => "1" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1" => #{"Name"=>"Account" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1"}


,
"AdvId" => #{"TagNum" => "2" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "2" => #{"Name"=>"AdvId" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "2"}


,
"AdvRefID" => #{"TagNum" => "3" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "3" => #{"Name"=>"AdvRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "3"}


,
"AdvSide" => #{"TagNum" => "4" ,"Type" => "CHAR" ,"ValidValues" =>[{"B", "BUY"},{"S", "SELL"},{"T", "TRADE"},{"X", "CROSS"}]}
, "4" => #{"Name"=>"AdvSide" ,"Type"=>"CHAR" ,"ValidValues"=>[{"B", "BUY"},{"S", "SELL"},{"T", "TRADE"},{"X", "CROSS"}], "TagNum" => "4"}


,
"AdvTransType" => #{"TagNum" => "5" ,"Type" => "STRING" ,"ValidValues" =>[{"N", "NEW"},{"C", "CANCEL"},{"R", "REPLACE"}]}
, "5" => #{"Name"=>"AdvTransType" ,"Type"=>"STRING" ,"ValidValues"=>[{"N", "NEW"},{"C", "CANCEL"},{"R", "REPLACE"}], "TagNum" => "5"}


,
"AvgPx" => #{"TagNum" => "6" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "6" => #{"Name"=>"AvgPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "6"}


,
"BeginSeqNo" => #{"TagNum" => "7" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "7" => #{"Name"=>"BeginSeqNo" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "7"}


,
"BeginString" => #{"TagNum" => "8" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "8" => #{"Name"=>"BeginString" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "8"}


,
"BodyLength" => #{"TagNum" => "9" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "9" => #{"Name"=>"BodyLength" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "9"}


,
"CheckSum" => #{"TagNum" => "10" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "10" => #{"Name"=>"CheckSum" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "10"}


,
"ClOrdID" => #{"TagNum" => "11" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "11" => #{"Name"=>"ClOrdID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "11"}


,
"Commission" => #{"TagNum" => "12" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "12" => #{"Name"=>"Commission" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "12"}


,
"CommType" => #{"TagNum" => "13" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "PER_UNIT"},{"2", "PERCENT"},{"3", "ABSOLUTE"},{"4", "PERCENTAGE_WAIVED_4"},{"5", "PERCENTAGE_WAIVED_5"},{"6", "POINTS_PER_BOND_OR_CONTRACT"}]}
, "13" => #{"Name"=>"CommType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "PER_UNIT"},{"2", "PERCENT"},{"3", "ABSOLUTE"},{"4", "PERCENTAGE_WAIVED_4"},{"5", "PERCENTAGE_WAIVED_5"},{"6", "POINTS_PER_BOND_OR_CONTRACT"}], "TagNum" => "13"}


,
"CumQty" => #{"TagNum" => "14" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "14" => #{"Name"=>"CumQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "14"}


,
"Currency" => #{"TagNum" => "15" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "15" => #{"Name"=>"Currency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "15"}


,
"EndSeqNo" => #{"TagNum" => "16" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "16" => #{"Name"=>"EndSeqNo" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "16"}


,
"ExecID" => #{"TagNum" => "17" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "17" => #{"Name"=>"ExecID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "17"}


,
"ExecInst" => #{"TagNum" => "18" ,"Type" => "MULTIPLECHARVALUE" ,"ValidValues" =>[{"0", "STAY_ON_OFFER_SIDE"},{"1", "NOT_HELD"},{"2", "WORK"},{"3", "GO_ALONG"},{"4", "OVER_THE_DAY"},{"5", "HELD"},{"6", "PARTICIPATE_DONT_INITIATE"},{"7", "STRICT_SCALE"},{"8", "TRY_TO_SCALE"},{"9", "STAY_ON_BID_SIDE"},{"A", "NO_CROSS"},{"B", "OK_TO_CROSS"},{"C", "CALL_FIRST"},{"D", "PERCENT_OF_VOLUME"},{"E", "DO_NOT_INCREASE"},{"F", "DO_NOT_REDUCE"},{"G", "ALL_OR_NONE"},{"H", "REINSTATE_ON_SYSTEM_FAILURE"},{"I", "INSTITUTIONS_ONLY"},{"J", "REINSTATE_ON_TRADING_HALT"},{"K", "CANCEL_ON_TRADING_HALT"},{"L", "LAST_PEG"},{"M", "MID_PRICE_PEG"},{"N", "NON_NEGOTIABLE"},{"O", "OPENING_PEG"},{"P", "MARKET_PEG"},{"Q", "CANCEL_ON_SYSTEM_FAILURE"},{"R", "PRIMARY_PEG"},{"S", "SUSPEND"},{"T", "FIXED_PEG_TO_LOCAL_BEST_BID_OR_OFFER_AT_TIME_OF_ORDER"},{"U", "CUSTOMER_DISPLAY_INSTRUCTION"},{"V", "NETTING"},{"W", "PEG_TO_VWAP"},{"X", "TRADE_ALONG"},{"Y", "TRY_TO_STOP"},{"Z", "CANCEL_IF_NOT_BEST"},{"a", "TRAILING_STOP_PEG"},{"b", "STRICT_LIMIT"},{"c", "IGNORE_PRICE_VALIDITY_CHECKS"},{"d", "PEG_TO_LIMIT_PRICE"},{"e", "WORK_TO_TARGET_STRATEGY"},{"f", "INTERMARKET_SWEEP"},{"g", "EXTERNAL_ROUTING_ALLOWED"},{"h", "EXTERNAL_ROUTING_NOT_ALLOWED"},{"i", "IMBALANCE_ONLY"},{"j", "SINGLE_EXECUTION_REQUESTED_FOR_BLOCK_TRADE"},{"k", "BEST_EXECUTION"},{"l", "SUSPEND_ON_SYSTEM_FAILURE"},{"m", "SUSPEND_ON_TRADING_HALT"},{"n", "REINSTATE_ON_CONNECTION_LOSS"},{"o", "CANCEL_ON_CONNECTION_LOSS"},{"p", "SUSPEND_ON_CONNECTION_LOSS"},{"q", "RELEASE_FROM_SUSPENSION"},{"r", "EXECUTE_AS_DELTA_NEUTRAL_USING_VOLATILITY_PROVIDED"},{"s", "EXECUTE_AS_DURATION_NEUTRAL"},{"t", "EXECUTE_AS_FX_NEUTRAL"}]}
, "18" => #{"Name"=>"ExecInst" ,"Type"=>"MULTIPLECHARVALUE" ,"ValidValues"=>[{"0", "STAY_ON_OFFER_SIDE"},{"1", "NOT_HELD"},{"2", "WORK"},{"3", "GO_ALONG"},{"4", "OVER_THE_DAY"},{"5", "HELD"},{"6", "PARTICIPATE_DONT_INITIATE"},{"7", "STRICT_SCALE"},{"8", "TRY_TO_SCALE"},{"9", "STAY_ON_BID_SIDE"},{"A", "NO_CROSS"},{"B", "OK_TO_CROSS"},{"C", "CALL_FIRST"},{"D", "PERCENT_OF_VOLUME"},{"E", "DO_NOT_INCREASE"},{"F", "DO_NOT_REDUCE"},{"G", "ALL_OR_NONE"},{"H", "REINSTATE_ON_SYSTEM_FAILURE"},{"I", "INSTITUTIONS_ONLY"},{"J", "REINSTATE_ON_TRADING_HALT"},{"K", "CANCEL_ON_TRADING_HALT"},{"L", "LAST_PEG"},{"M", "MID_PRICE_PEG"},{"N", "NON_NEGOTIABLE"},{"O", "OPENING_PEG"},{"P", "MARKET_PEG"},{"Q", "CANCEL_ON_SYSTEM_FAILURE"},{"R", "PRIMARY_PEG"},{"S", "SUSPEND"},{"T", "FIXED_PEG_TO_LOCAL_BEST_BID_OR_OFFER_AT_TIME_OF_ORDER"},{"U", "CUSTOMER_DISPLAY_INSTRUCTION"},{"V", "NETTING"},{"W", "PEG_TO_VWAP"},{"X", "TRADE_ALONG"},{"Y", "TRY_TO_STOP"},{"Z", "CANCEL_IF_NOT_BEST"},{"a", "TRAILING_STOP_PEG"},{"b", "STRICT_LIMIT"},{"c", "IGNORE_PRICE_VALIDITY_CHECKS"},{"d", "PEG_TO_LIMIT_PRICE"},{"e", "WORK_TO_TARGET_STRATEGY"},{"f", "INTERMARKET_SWEEP"},{"g", "EXTERNAL_ROUTING_ALLOWED"},{"h", "EXTERNAL_ROUTING_NOT_ALLOWED"},{"i", "IMBALANCE_ONLY"},{"j", "SINGLE_EXECUTION_REQUESTED_FOR_BLOCK_TRADE"},{"k", "BEST_EXECUTION"},{"l", "SUSPEND_ON_SYSTEM_FAILURE"},{"m", "SUSPEND_ON_TRADING_HALT"},{"n", "REINSTATE_ON_CONNECTION_LOSS"},{"o", "CANCEL_ON_CONNECTION_LOSS"},{"p", "SUSPEND_ON_CONNECTION_LOSS"},{"q", "RELEASE_FROM_SUSPENSION"},{"r", "EXECUTE_AS_DELTA_NEUTRAL_USING_VOLATILITY_PROVIDED"},{"s", "EXECUTE_AS_DURATION_NEUTRAL"},{"t", "EXECUTE_AS_FX_NEUTRAL"}], "TagNum" => "18"}


,
"ExecRefID" => #{"TagNum" => "19" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "19" => #{"Name"=>"ExecRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "19"}


,
"HandlInst" => #{"TagNum" => "21" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "AUTOMATED_EXECUTION_ORDER_PRIVATE_NO_BROKER_INTERVENTION"},{"2", "AUTOMATED_EXECUTION_ORDER_PUBLIC_BROKER_INTERVENTION_OK"},{"3", "MANUAL_ORDER_BEST_EXECUTION"}]}
, "21" => #{"Name"=>"HandlInst" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "AUTOMATED_EXECUTION_ORDER_PRIVATE_NO_BROKER_INTERVENTION"},{"2", "AUTOMATED_EXECUTION_ORDER_PUBLIC_BROKER_INTERVENTION_OK"},{"3", "MANUAL_ORDER_BEST_EXECUTION"}], "TagNum" => "21"}


,
"SecurityIDSource" => #{"TagNum" => "22" ,"Type" => "STRING" ,"ValidValues" =>[{"1", "CUSIP"},{"2", "SEDOL"},{"3", "QUIK"},{"4", "ISIN_NUMBER"},{"5", "RIC_CODE"},{"6", "ISO_CURRENCY_CODE"},{"7", "ISO_COUNTRY_CODE"},{"8", "EXCHANGE_SYMBOL"},{"9", "CONSOLIDATED_TAPE_ASSOCIATION"},{"A", "BLOOMBERG_SYMBOL"},{"B", "WERTPAPIER"},{"C", "DUTCH"},{"D", "VALOREN"},{"E", "SICOVAM"},{"F", "BELGIAN"},{"G", "COMMON"},{"H", "CLEARING_HOUSE"},{"I", "ISDA_FPML_PRODUCT_SPECIFICATION"},{"J", "OPTION_PRICE_REPORTING_AUTHORITY"},{"K", "ISDA_FPML_PRODUCT_URL"},{"L", "LETTER_OF_CREDIT"},{"M", "MARKETPLACE_ASSIGNED_IDENTIFIER"}]}
, "22" => #{"Name"=>"SecurityIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[{"1", "CUSIP"},{"2", "SEDOL"},{"3", "QUIK"},{"4", "ISIN_NUMBER"},{"5", "RIC_CODE"},{"6", "ISO_CURRENCY_CODE"},{"7", "ISO_COUNTRY_CODE"},{"8", "EXCHANGE_SYMBOL"},{"9", "CONSOLIDATED_TAPE_ASSOCIATION"},{"A", "BLOOMBERG_SYMBOL"},{"B", "WERTPAPIER"},{"C", "DUTCH"},{"D", "VALOREN"},{"E", "SICOVAM"},{"F", "BELGIAN"},{"G", "COMMON"},{"H", "CLEARING_HOUSE"},{"I", "ISDA_FPML_PRODUCT_SPECIFICATION"},{"J", "OPTION_PRICE_REPORTING_AUTHORITY"},{"K", "ISDA_FPML_PRODUCT_URL"},{"L", "LETTER_OF_CREDIT"},{"M", "MARKETPLACE_ASSIGNED_IDENTIFIER"}], "TagNum" => "22"}


,
"IOIID" => #{"TagNum" => "23" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "23" => #{"Name"=>"IOIID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "23"}


,
"IOIQltyInd" => #{"TagNum" => "25" ,"Type" => "CHAR" ,"ValidValues" =>[{"H", "HIGH"},{"L", "LOW"},{"M", "MEDIUM"}]}
, "25" => #{"Name"=>"IOIQltyInd" ,"Type"=>"CHAR" ,"ValidValues"=>[{"H", "HIGH"},{"L", "LOW"},{"M", "MEDIUM"}], "TagNum" => "25"}


,
"IOIRefID" => #{"TagNum" => "26" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "26" => #{"Name"=>"IOIRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "26"}


,
"IOIQty" => #{"TagNum" => "27" ,"Type" => "STRING" ,"ValidValues" =>[{"S", "SMALL"},{"M", "MEDIUM"},{"L", "LARGE"},{"U", "UNDISCLOSED_QUANTITY"}]}
, "27" => #{"Name"=>"IOIQty" ,"Type"=>"STRING" ,"ValidValues"=>[{"S", "SMALL"},{"M", "MEDIUM"},{"L", "LARGE"},{"U", "UNDISCLOSED_QUANTITY"}], "TagNum" => "27"}


,
"IOITransType" => #{"TagNum" => "28" ,"Type" => "CHAR" ,"ValidValues" =>[{"N", "NEW"},{"C", "CANCEL"},{"R", "REPLACE"}]}
, "28" => #{"Name"=>"IOITransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"N", "NEW"},{"C", "CANCEL"},{"R", "REPLACE"}], "TagNum" => "28"}


,
"LastCapacity" => #{"TagNum" => "29" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "AGENT"},{"2", "CROSS_AS_AGENT"},{"3", "CROSS_AS_PRINCIPAL"},{"4", "PRINCIPAL"}]}
, "29" => #{"Name"=>"LastCapacity" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "AGENT"},{"2", "CROSS_AS_AGENT"},{"3", "CROSS_AS_PRINCIPAL"},{"4", "PRINCIPAL"}], "TagNum" => "29"}


,
"LastMkt" => #{"TagNum" => "30" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "30" => #{"Name"=>"LastMkt" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "30"}


,
"LastPx" => #{"TagNum" => "31" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "31" => #{"Name"=>"LastPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "31"}


,
"LastQty" => #{"TagNum" => "32" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "32" => #{"Name"=>"LastQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "32"}


,
"NoLinesOfText" => #{"TagNum" => "33" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "33" => #{"Name"=>"NoLinesOfText" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "33"}


,
"MsgSeqNum" => #{"TagNum" => "34" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "34" => #{"Name"=>"MsgSeqNum" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "34"}


,
"MsgType" => #{"TagNum" => "35" ,"Type" => "STRING" ,"ValidValues" =>[{"0", "HEARTBEAT"},{"1", "TESTREQUEST"},{"2", "RESENDREQUEST"},{"3", "REJECT"},{"4", "SEQUENCERESET"},{"5", "LOGOUT"},{"6", "IOI"},{"7", "ADVERTISEMENT"},{"8", "EXECUTIONREPORT"},{"9", "ORDERCANCELREJECT"},{"A", "LOGON"},{"AA", "DERIVATIVESECURITYLIST"},{"AB", "NEWORDERMULTILEG"},{"AC", "MULTILEGORDERCANCELREPLACE"},{"AD", "TRADECAPTUREREPORTREQUEST"},{"AE", "TRADECAPTUREREPORT"},{"AF", "ORDERMASSSTATUSREQUEST"},{"AG", "QUOTEREQUESTREJECT"},{"AH", "RFQREQUEST"},{"AI", "QUOTESTATUSREPORT"},{"AJ", "QUOTERESPONSE"},{"AK", "CONFIRMATION"},{"AL", "POSITIONMAINTENANCEREQUEST"},{"AM", "POSITIONMAINTENANCEREPORT"},{"AN", "REQUESTFORPOSITIONS"},{"AO", "REQUESTFORPOSITIONSACK"},{"AP", "POSITIONREPORT"},{"AQ", "TRADECAPTUREREPORTREQUESTACK"},{"AR", "TRADECAPTUREREPORTACK"},{"AS", "ALLOCATIONREPORT"},{"AT", "ALLOCATIONREPORTACK"},{"AU", "CONFIRMATIONACK"},{"AV", "SETTLEMENTINSTRUCTIONREQUEST"},{"AW", "ASSIGNMENTREPORT"},{"AX", "COLLATERALREQUEST"},{"AY", "COLLATERALASSIGNMENT"},{"AZ", "COLLATERALRESPONSE"},{"B", "NEWS"},{"BA", "COLLATERALREPORT"},{"BB", "COLLATERALINQUIRY"},{"BC", "NETWORKCOUNTERPARTYSYSTEMSTATUSREQUEST"},{"BD", "NETWORKCOUNTERPARTYSYSTEMSTATUSRESPONSE"},{"BE", "USERREQUEST"},{"BF", "USERRESPONSE"},{"BG", "COLLATERALINQUIRYACK"},{"BH", "CONFIRMATIONREQUEST"},{"BI", "TRADINGSESSIONLISTREQUEST"},{"BJ", "TRADINGSESSIONLIST"},{"BK", "SECURITYLISTUPDATEREPORT"},{"BL", "ADJUSTEDPOSITIONREPORT"},{"BM", "ALLOCATIONINSTRUCTIONALERT"},{"BN", "EXECUTIONACKNOWLEDGEMENT"},{"BO", "CONTRARYINTENTIONREPORT"},{"BP", "SECURITYDEFINITIONUPDATEREPORT"},{"BQ", "SETTLEMENTOBLIGATIONREPORT"},{"BR", "DERIVATIVESECURITYLISTUPDATEREPORT"},{"BS", "TRADINGSESSIONLISTUPDATEREPORT"},{"BT", "MARKETDEFINITIONREQUEST"},{"BU", "MARKETDEFINITION"},{"BV", "MARKETDEFINITIONUPDATEREPORT"},{"BW", "APPLICATIONMESSAGEREQUEST"},{"BX", "APPLICATIONMESSAGEREQUESTACK"},{"BY", "APPLICATIONMESSAGEREPORT"},{"BZ", "ORDERMASSACTIONREPORT"},{"C", "EMAIL"},{"CA", "ORDERMASSACTIONREQUEST"},{"CB", "USERNOTIFICATION"},{"CC", "STREAMASSIGNMENTREQUEST"},{"CD", "STREAMASSIGNMENTREPORT"},{"CE", "STREAMASSIGNMENTREPORTACK"},{"D", "NEWORDERSINGLE"},{"E", "NEWORDERLIST"},{"F", "ORDERCANCELREQUEST"},{"G", "ORDERCANCELREPLACEREQUEST"},{"H", "ORDERSTATUSREQUEST"},{"J", "ALLOCATIONINSTRUCTION"},{"K", "LISTCANCELREQUEST"},{"L", "LISTEXECUTE"},{"M", "LISTSTATUSREQUEST"},{"N", "LISTSTATUS"},{"P", "ALLOCATIONINSTRUCTIONACK"},{"Q", "DONTKNOWTRADE"},{"R", "QUOTEREQUEST"},{"S", "QUOTE"},{"T", "SETTLEMENTINSTRUCTIONS"},{"V", "MARKETDATAREQUEST"},{"W", "MARKETDATASNAPSHOTFULLREFRESH"},{"X", "MARKETDATAINCREMENTALREFRESH"},{"Y", "MARKETDATAREQUESTREJECT"},{"Z", "QUOTECANCEL"},{"a", "QUOTESTATUSREQUEST"},{"b", "MASSQUOTEACKNOWLEDGEMENT"},{"c", "SECURITYDEFINITIONREQUEST"},{"d", "SECURITYDEFINITION"},{"e", "SECURITYSTATUSREQUEST"},{"f", "SECURITYSTATUS"},{"g", "TRADINGSESSIONSTATUSREQUEST"},{"h", "TRADINGSESSIONSTATUS"},{"i", "MASSQUOTE"},{"j", "BUSINESSMESSAGEREJECT"},{"k", "BIDREQUEST"},{"l", "BIDRESPONSE"},{"m", "LISTSTRIKEPRICE"},{"n", "XMLNONFIX"},{"o", "REGISTRATIONINSTRUCTIONS"},{"p", "REGISTRATIONINSTRUCTIONSRESPONSE"},{"q", "ORDERMASSCANCELREQUEST"},{"r", "ORDERMASSCANCELREPORT"},{"s", "NEWORDERCROSS"},{"t", "CROSSORDERCANCELREPLACEREQUEST"},{"u", "CROSSORDERCANCELREQUEST"},{"v", "SECURITYTYPEREQUEST"},{"w", "SECURITYTYPES"},{"x", "SECURITYLISTREQUEST"},{"y", "SECURITYLIST"},{"z", "DERIVATIVESECURITYLISTREQUEST"}]}
, "35" => #{"Name"=>"MsgType" ,"Type"=>"STRING" ,"ValidValues"=>[{"0", "HEARTBEAT"},{"1", "TESTREQUEST"},{"2", "RESENDREQUEST"},{"3", "REJECT"},{"4", "SEQUENCERESET"},{"5", "LOGOUT"},{"6", "IOI"},{"7", "ADVERTISEMENT"},{"8", "EXECUTIONREPORT"},{"9", "ORDERCANCELREJECT"},{"A", "LOGON"},{"AA", "DERIVATIVESECURITYLIST"},{"AB", "NEWORDERMULTILEG"},{"AC", "MULTILEGORDERCANCELREPLACE"},{"AD", "TRADECAPTUREREPORTREQUEST"},{"AE", "TRADECAPTUREREPORT"},{"AF", "ORDERMASSSTATUSREQUEST"},{"AG", "QUOTEREQUESTREJECT"},{"AH", "RFQREQUEST"},{"AI", "QUOTESTATUSREPORT"},{"AJ", "QUOTERESPONSE"},{"AK", "CONFIRMATION"},{"AL", "POSITIONMAINTENANCEREQUEST"},{"AM", "POSITIONMAINTENANCEREPORT"},{"AN", "REQUESTFORPOSITIONS"},{"AO", "REQUESTFORPOSITIONSACK"},{"AP", "POSITIONREPORT"},{"AQ", "TRADECAPTUREREPORTREQUESTACK"},{"AR", "TRADECAPTUREREPORTACK"},{"AS", "ALLOCATIONREPORT"},{"AT", "ALLOCATIONREPORTACK"},{"AU", "CONFIRMATIONACK"},{"AV", "SETTLEMENTINSTRUCTIONREQUEST"},{"AW", "ASSIGNMENTREPORT"},{"AX", "COLLATERALREQUEST"},{"AY", "COLLATERALASSIGNMENT"},{"AZ", "COLLATERALRESPONSE"},{"B", "NEWS"},{"BA", "COLLATERALREPORT"},{"BB", "COLLATERALINQUIRY"},{"BC", "NETWORKCOUNTERPARTYSYSTEMSTATUSREQUEST"},{"BD", "NETWORKCOUNTERPARTYSYSTEMSTATUSRESPONSE"},{"BE", "USERREQUEST"},{"BF", "USERRESPONSE"},{"BG", "COLLATERALINQUIRYACK"},{"BH", "CONFIRMATIONREQUEST"},{"BI", "TRADINGSESSIONLISTREQUEST"},{"BJ", "TRADINGSESSIONLIST"},{"BK", "SECURITYLISTUPDATEREPORT"},{"BL", "ADJUSTEDPOSITIONREPORT"},{"BM", "ALLOCATIONINSTRUCTIONALERT"},{"BN", "EXECUTIONACKNOWLEDGEMENT"},{"BO", "CONTRARYINTENTIONREPORT"},{"BP", "SECURITYDEFINITIONUPDATEREPORT"},{"BQ", "SETTLEMENTOBLIGATIONREPORT"},{"BR", "DERIVATIVESECURITYLISTUPDATEREPORT"},{"BS", "TRADINGSESSIONLISTUPDATEREPORT"},{"BT", "MARKETDEFINITIONREQUEST"},{"BU", "MARKETDEFINITION"},{"BV", "MARKETDEFINITIONUPDATEREPORT"},{"BW", "APPLICATIONMESSAGEREQUEST"},{"BX", "APPLICATIONMESSAGEREQUESTACK"},{"BY", "APPLICATIONMESSAGEREPORT"},{"BZ", "ORDERMASSACTIONREPORT"},{"C", "EMAIL"},{"CA", "ORDERMASSACTIONREQUEST"},{"CB", "USERNOTIFICATION"},{"CC", "STREAMASSIGNMENTREQUEST"},{"CD", "STREAMASSIGNMENTREPORT"},{"CE", "STREAMASSIGNMENTREPORTACK"},{"D", "NEWORDERSINGLE"},{"E", "NEWORDERLIST"},{"F", "ORDERCANCELREQUEST"},{"G", "ORDERCANCELREPLACEREQUEST"},{"H", "ORDERSTATUSREQUEST"},{"J", "ALLOCATIONINSTRUCTION"},{"K", "LISTCANCELREQUEST"},{"L", "LISTEXECUTE"},{"M", "LISTSTATUSREQUEST"},{"N", "LISTSTATUS"},{"P", "ALLOCATIONINSTRUCTIONACK"},{"Q", "DONTKNOWTRADE"},{"R", "QUOTEREQUEST"},{"S", "QUOTE"},{"T", "SETTLEMENTINSTRUCTIONS"},{"V", "MARKETDATAREQUEST"},{"W", "MARKETDATASNAPSHOTFULLREFRESH"},{"X", "MARKETDATAINCREMENTALREFRESH"},{"Y", "MARKETDATAREQUESTREJECT"},{"Z", "QUOTECANCEL"},{"a", "QUOTESTATUSREQUEST"},{"b", "MASSQUOTEACKNOWLEDGEMENT"},{"c", "SECURITYDEFINITIONREQUEST"},{"d", "SECURITYDEFINITION"},{"e", "SECURITYSTATUSREQUEST"},{"f", "SECURITYSTATUS"},{"g", "TRADINGSESSIONSTATUSREQUEST"},{"h", "TRADINGSESSIONSTATUS"},{"i", "MASSQUOTE"},{"j", "BUSINESSMESSAGEREJECT"},{"k", "BIDREQUEST"},{"l", "BIDRESPONSE"},{"m", "LISTSTRIKEPRICE"},{"n", "XMLNONFIX"},{"o", "REGISTRATIONINSTRUCTIONS"},{"p", "REGISTRATIONINSTRUCTIONSRESPONSE"},{"q", "ORDERMASSCANCELREQUEST"},{"r", "ORDERMASSCANCELREPORT"},{"s", "NEWORDERCROSS"},{"t", "CROSSORDERCANCELREPLACEREQUEST"},{"u", "CROSSORDERCANCELREQUEST"},{"v", "SECURITYTYPEREQUEST"},{"w", "SECURITYTYPES"},{"x", "SECURITYLISTREQUEST"},{"y", "SECURITYLIST"},{"z", "DERIVATIVESECURITYLISTREQUEST"}], "TagNum" => "35"}


,
"NewSeqNo" => #{"TagNum" => "36" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "36" => #{"Name"=>"NewSeqNo" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "36"}


,
"OrderID" => #{"TagNum" => "37" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "37" => #{"Name"=>"OrderID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "37"}


,
"OrderQty" => #{"TagNum" => "38" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "38" => #{"Name"=>"OrderQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "38"}


,
"OrdStatus" => #{"TagNum" => "39" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "PARTIALLY_FILLED"},{"2", "FILLED"},{"3", "DONE_FOR_DAY"},{"4", "CANCELED"},{"5", "REPLACED"},{"6", "PENDING_CANCEL"},{"7", "STOPPED"},{"8", "REJECTED"},{"9", "SUSPENDED"},{"A", "PENDING_NEW"},{"B", "CALCULATED"},{"C", "EXPIRED"},{"D", "ACCEPTED_FOR_BIDDING"},{"E", "PENDING_REPLACE"}]}
, "39" => #{"Name"=>"OrdStatus" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "PARTIALLY_FILLED"},{"2", "FILLED"},{"3", "DONE_FOR_DAY"},{"4", "CANCELED"},{"5", "REPLACED"},{"6", "PENDING_CANCEL"},{"7", "STOPPED"},{"8", "REJECTED"},{"9", "SUSPENDED"},{"A", "PENDING_NEW"},{"B", "CALCULATED"},{"C", "EXPIRED"},{"D", "ACCEPTED_FOR_BIDDING"},{"E", "PENDING_REPLACE"}], "TagNum" => "39"}


,
"OrdType" => #{"TagNum" => "40" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "MARKET"},{"2", "LIMIT"},{"3", "STOP"},{"4", "STOP_LIMIT"},{"5", "MARKET_ON_CLOSE"},{"6", "WITH_OR_WITHOUT"},{"7", "LIMIT_OR_BETTER"},{"8", "LIMIT_WITH_OR_WITHOUT"},{"9", "ON_BASIS"},{"A", "ON_CLOSE"},{"B", "LIMIT_ON_CLOSE"},{"C", "FOREX_MARKET"},{"D", "PREVIOUSLY_QUOTED"},{"E", "PREVIOUSLY_INDICATED"},{"F", "FOREX_LIMIT"},{"G", "FOREX_SWAP"},{"H", "FOREX_PREVIOUSLY_QUOTED"},{"I", "FUNARI"},{"J", "MARKET_IF_TOUCHED"},{"K", "MARKET_WITH_LEFT_OVER_AS_LIMIT"},{"L", "PREVIOUS_FUND_VALUATION_POINT"},{"M", "NEXT_FUND_VALUATION_POINT"},{"P", "PEGGED"},{"Q", "COUNTER_ORDER_SELECTION"}]}
, "40" => #{"Name"=>"OrdType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "MARKET"},{"2", "LIMIT"},{"3", "STOP"},{"4", "STOP_LIMIT"},{"5", "MARKET_ON_CLOSE"},{"6", "WITH_OR_WITHOUT"},{"7", "LIMIT_OR_BETTER"},{"8", "LIMIT_WITH_OR_WITHOUT"},{"9", "ON_BASIS"},{"A", "ON_CLOSE"},{"B", "LIMIT_ON_CLOSE"},{"C", "FOREX_MARKET"},{"D", "PREVIOUSLY_QUOTED"},{"E", "PREVIOUSLY_INDICATED"},{"F", "FOREX_LIMIT"},{"G", "FOREX_SWAP"},{"H", "FOREX_PREVIOUSLY_QUOTED"},{"I", "FUNARI"},{"J", "MARKET_IF_TOUCHED"},{"K", "MARKET_WITH_LEFT_OVER_AS_LIMIT"},{"L", "PREVIOUS_FUND_VALUATION_POINT"},{"M", "NEXT_FUND_VALUATION_POINT"},{"P", "PEGGED"},{"Q", "COUNTER_ORDER_SELECTION"}], "TagNum" => "40"}


,
"OrigClOrdID" => #{"TagNum" => "41" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "41" => #{"Name"=>"OrigClOrdID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "41"}


,
"OrigTime" => #{"TagNum" => "42" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "42" => #{"Name"=>"OrigTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "42"}


,
"PossDupFlag" => #{"TagNum" => "43" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "43" => #{"Name"=>"PossDupFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "43"}


,
"Price" => #{"TagNum" => "44" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "44" => #{"Name"=>"Price" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "44"}


,
"RefSeqNum" => #{"TagNum" => "45" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "45" => #{"Name"=>"RefSeqNum" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "45"}


,
"SecurityID" => #{"TagNum" => "48" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "48" => #{"Name"=>"SecurityID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "48"}


,
"SenderCompID" => #{"TagNum" => "49" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "49" => #{"Name"=>"SenderCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "49"}


,
"SenderSubID" => #{"TagNum" => "50" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "50" => #{"Name"=>"SenderSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "50"}


,
"SendingTime" => #{"TagNum" => "52" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "52" => #{"Name"=>"SendingTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "52"}


,
"Quantity" => #{"TagNum" => "53" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "53" => #{"Name"=>"Quantity" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "53"}


,
"Side" => #{"TagNum" => "54" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "BUY"},{"2", "SELL"},{"3", "BUY_MINUS"},{"4", "SELL_PLUS"},{"5", "SELL_SHORT"},{"6", "SELL_SHORT_EXEMPT"},{"7", "UNDISCLOSED"},{"8", "CROSS"},{"9", "CROSS_SHORT"},{"A", "CROSS_SHORT_EXEMPT"},{"B", "AS_DEFINED"},{"C", "OPPOSITE"},{"D", "SUBSCRIBE"},{"E", "REDEEM"},{"F", "LEND"},{"G", "BORROW"}]}
, "54" => #{"Name"=>"Side" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "BUY"},{"2", "SELL"},{"3", "BUY_MINUS"},{"4", "SELL_PLUS"},{"5", "SELL_SHORT"},{"6", "SELL_SHORT_EXEMPT"},{"7", "UNDISCLOSED"},{"8", "CROSS"},{"9", "CROSS_SHORT"},{"A", "CROSS_SHORT_EXEMPT"},{"B", "AS_DEFINED"},{"C", "OPPOSITE"},{"D", "SUBSCRIBE"},{"E", "REDEEM"},{"F", "LEND"},{"G", "BORROW"}], "TagNum" => "54"}


,
"Symbol" => #{"TagNum" => "55" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "55" => #{"Name"=>"Symbol" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "55"}


,
"TargetCompID" => #{"TagNum" => "56" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "56" => #{"Name"=>"TargetCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "56"}


,
"TargetSubID" => #{"TagNum" => "57" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "57" => #{"Name"=>"TargetSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "57"}


,
"Text" => #{"TagNum" => "58" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "58" => #{"Name"=>"Text" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "58"}


,
"TimeInForce" => #{"TagNum" => "59" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "DAY"},{"1", "GOOD_TILL_CANCEL"},{"2", "AT_THE_OPENING"},{"3", "IMMEDIATE_OR_CANCEL"},{"4", "FILL_OR_KILL"},{"5", "GOOD_TILL_CROSSING"},{"6", "GOOD_TILL_DATE"},{"7", "AT_THE_CLOSE"},{"8", "GOOD_THROUGH_CROSSING"},{"9", "AT_CROSSING"}]}
, "59" => #{"Name"=>"TimeInForce" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "DAY"},{"1", "GOOD_TILL_CANCEL"},{"2", "AT_THE_OPENING"},{"3", "IMMEDIATE_OR_CANCEL"},{"4", "FILL_OR_KILL"},{"5", "GOOD_TILL_CROSSING"},{"6", "GOOD_TILL_DATE"},{"7", "AT_THE_CLOSE"},{"8", "GOOD_THROUGH_CROSSING"},{"9", "AT_CROSSING"}], "TagNum" => "59"}


,
"TransactTime" => #{"TagNum" => "60" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "60" => #{"Name"=>"TransactTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "60"}


,
"Urgency" => #{"TagNum" => "61" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NORMAL"},{"1", "FLASH"},{"2", "BACKGROUND"}]}
, "61" => #{"Name"=>"Urgency" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NORMAL"},{"1", "FLASH"},{"2", "BACKGROUND"}], "TagNum" => "61"}


,
"ValidUntilTime" => #{"TagNum" => "62" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "62" => #{"Name"=>"ValidUntilTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "62"}


,
"SettlType" => #{"TagNum" => "63" ,"Type" => "STRING" ,"ValidValues" =>[{"0", "REGULAR"},{"1", "CASH"},{"2", "NEXT_DAY"},{"3", "T_PLUS_2"},{"4", "T_PLUS_3"},{"5", "T_PLUS_4"},{"6", "FUTURE"},{"7", "WHEN_AND_IF_ISSUED"},{"8", "SELLERS_OPTION"},{"9", "T_PLUS_5"},{"B", "BROKEN_DATE"},{"C", "FX_SPOT_NEXT_SETTLEMENT"}]}
, "63" => #{"Name"=>"SettlType" ,"Type"=>"STRING" ,"ValidValues"=>[{"0", "REGULAR"},{"1", "CASH"},{"2", "NEXT_DAY"},{"3", "T_PLUS_2"},{"4", "T_PLUS_3"},{"5", "T_PLUS_4"},{"6", "FUTURE"},{"7", "WHEN_AND_IF_ISSUED"},{"8", "SELLERS_OPTION"},{"9", "T_PLUS_5"},{"B", "BROKEN_DATE"},{"C", "FX_SPOT_NEXT_SETTLEMENT"}], "TagNum" => "63"}


,
"SettlDate" => #{"TagNum" => "64" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "64" => #{"Name"=>"SettlDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "64"}


,
"SymbolSfx" => #{"TagNum" => "65" ,"Type" => "STRING" ,"ValidValues" =>[{"CD", "EUCP_WITH_LUMP_SUM_INTEREST_RATHER_THAN_DISCOUNT_PRICE"},{"WI", "WHEN_ISSUED_FOR_A_SECURITY_TO_BE_REISSUED_UNDER_AN_OLD_CUSIP_OR_ISIN"}]}
, "65" => #{"Name"=>"SymbolSfx" ,"Type"=>"STRING" ,"ValidValues"=>[{"CD", "EUCP_WITH_LUMP_SUM_INTEREST_RATHER_THAN_DISCOUNT_PRICE"},{"WI", "WHEN_ISSUED_FOR_A_SECURITY_TO_BE_REISSUED_UNDER_AN_OLD_CUSIP_OR_ISIN"}], "TagNum" => "65"}


,
"ListID" => #{"TagNum" => "66" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "66" => #{"Name"=>"ListID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "66"}


,
"ListSeqNo" => #{"TagNum" => "67" ,"Type" => "INT" ,"ValidValues" =>[]}
, "67" => #{"Name"=>"ListSeqNo" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "67"}


,
"TotNoOrders" => #{"TagNum" => "68" ,"Type" => "INT" ,"ValidValues" =>[]}
, "68" => #{"Name"=>"TotNoOrders" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "68"}


,
"ListExecInst" => #{"TagNum" => "69" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "69" => #{"Name"=>"ListExecInst" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "69"}


,
"AllocID" => #{"TagNum" => "70" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "70" => #{"Name"=>"AllocID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "70"}


,
"AllocTransType" => #{"TagNum" => "71" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "REPLACE"},{"2", "CANCEL"},{"3", "PRELIMINARY"},{"4", "CALCULATED"},{"5", "CALCULATED_WITHOUT_PRELIMINARY"},{"6", "REVERSAL"}]}
, "71" => #{"Name"=>"AllocTransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "REPLACE"},{"2", "CANCEL"},{"3", "PRELIMINARY"},{"4", "CALCULATED"},{"5", "CALCULATED_WITHOUT_PRELIMINARY"},{"6", "REVERSAL"}], "TagNum" => "71"}


,
"RefAllocID" => #{"TagNum" => "72" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "72" => #{"Name"=>"RefAllocID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "72"}


,
"NoOrders" => #{"TagNum" => "73" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "73" => #{"Name"=>"NoOrders" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "73"}


,
"AvgPxPrecision" => #{"TagNum" => "74" ,"Type" => "INT" ,"ValidValues" =>[]}
, "74" => #{"Name"=>"AvgPxPrecision" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "74"}


,
"TradeDate" => #{"TagNum" => "75" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "75" => #{"Name"=>"TradeDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "75"}


,
"PositionEffect" => #{"TagNum" => "77" ,"Type" => "CHAR" ,"ValidValues" =>[{"C", "CLOSE"},{"F", "FIFO"},{"O", "OPEN"},{"R", "ROLLED"},{"N", "CLOSE_BUT_NOTIFY_ON_OPEN"},{"D", "DEFAULT"}]}
, "77" => #{"Name"=>"PositionEffect" ,"Type"=>"CHAR" ,"ValidValues"=>[{"C", "CLOSE"},{"F", "FIFO"},{"O", "OPEN"},{"R", "ROLLED"},{"N", "CLOSE_BUT_NOTIFY_ON_OPEN"},{"D", "DEFAULT"}], "TagNum" => "77"}


,
"NoAllocs" => #{"TagNum" => "78" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "78" => #{"Name"=>"NoAllocs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "78"}


,
"AllocAccount" => #{"TagNum" => "79" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "79" => #{"Name"=>"AllocAccount" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "79"}


,
"AllocQty" => #{"TagNum" => "80" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "80" => #{"Name"=>"AllocQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "80"}


,
"ProcessCode" => #{"TagNum" => "81" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "REGULAR"},{"1", "SOFT_DOLLAR"},{"2", "STEP_IN"},{"3", "STEP_OUT"},{"4", "SOFT_DOLLAR_STEP_IN"},{"5", "SOFT_DOLLAR_STEP_OUT"},{"6", "PLAN_SPONSOR"}]}
, "81" => #{"Name"=>"ProcessCode" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "REGULAR"},{"1", "SOFT_DOLLAR"},{"2", "STEP_IN"},{"3", "STEP_OUT"},{"4", "SOFT_DOLLAR_STEP_IN"},{"5", "SOFT_DOLLAR_STEP_OUT"},{"6", "PLAN_SPONSOR"}], "TagNum" => "81"}


,
"NoRpts" => #{"TagNum" => "82" ,"Type" => "INT" ,"ValidValues" =>[]}
, "82" => #{"Name"=>"NoRpts" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "82"}


,
"RptSeq" => #{"TagNum" => "83" ,"Type" => "INT" ,"ValidValues" =>[]}
, "83" => #{"Name"=>"RptSeq" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "83"}


,
"CxlQty" => #{"TagNum" => "84" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "84" => #{"Name"=>"CxlQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "84"}


,
"NoDlvyInst" => #{"TagNum" => "85" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "85" => #{"Name"=>"NoDlvyInst" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "85"}


,
"AllocStatus" => #{"TagNum" => "87" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ACCEPTED"},{"1", "BLOCK_LEVEL_REJECT"},{"2", "ACCOUNT_LEVEL_REJECT"},{"3", "RECEIVED"},{"4", "INCOMPLETE"},{"5", "REJECTED_BY_INTERMEDIARY"},{"6", "ALLOCATION_PENDING"},{"7", "REVERSED"}]}
, "87" => #{"Name"=>"AllocStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ACCEPTED"},{"1", "BLOCK_LEVEL_REJECT"},{"2", "ACCOUNT_LEVEL_REJECT"},{"3", "RECEIVED"},{"4", "INCOMPLETE"},{"5", "REJECTED_BY_INTERMEDIARY"},{"6", "ALLOCATION_PENDING"},{"7", "REVERSED"}], "TagNum" => "87"}


,
"AllocRejCode" => #{"TagNum" => "88" ,"Type" => "INT" ,"ValidValues" =>[{"0", "UNKNOWN_ACCOUNT"},{"1", "INCORRECT_QUANTITY"},{"2", "INCORRECT_AVERAGEG_PRICE"},{"3", "UNKNOWN_EXECUTING_BROKER_MNEMONIC"},{"4", "COMMISSION_DIFFERENCE"},{"5", "UNKNOWN_ORDERID"},{"6", "UNKNOWN_LISTID"},{"7", "OTHER_7"},{"8", "INCORRECT_ALLOCATED_QUANTITY"},{"9", "CALCULATION_DIFFERENCE"},{"10", "UNKNOWN_OR_STALE_EXECID"},{"11", "MISMATCHED_DATA"},{"12", "UNKNOWN_CLORDID"},{"13", "WAREHOUSE_REQUEST_REJECTED"},{"99", "OTHER_99"}]}
, "88" => #{"Name"=>"AllocRejCode" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "UNKNOWN_ACCOUNT"},{"1", "INCORRECT_QUANTITY"},{"2", "INCORRECT_AVERAGEG_PRICE"},{"3", "UNKNOWN_EXECUTING_BROKER_MNEMONIC"},{"4", "COMMISSION_DIFFERENCE"},{"5", "UNKNOWN_ORDERID"},{"6", "UNKNOWN_LISTID"},{"7", "OTHER_7"},{"8", "INCORRECT_ALLOCATED_QUANTITY"},{"9", "CALCULATION_DIFFERENCE"},{"10", "UNKNOWN_OR_STALE_EXECID"},{"11", "MISMATCHED_DATA"},{"12", "UNKNOWN_CLORDID"},{"13", "WAREHOUSE_REQUEST_REJECTED"},{"99", "OTHER_99"}], "TagNum" => "88"}


,
"Signature" => #{"TagNum" => "89" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "89" => #{"Name"=>"Signature" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "89"}


,
"SecureDataLen" => #{"TagNum" => "90" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "90" => #{"Name"=>"SecureDataLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "90"}


,
"SecureData" => #{"TagNum" => "91" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "91" => #{"Name"=>"SecureData" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "91"}


,
"SignatureLength" => #{"TagNum" => "93" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "93" => #{"Name"=>"SignatureLength" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "93"}


,
"EmailType" => #{"TagNum" => "94" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "REPLY"},{"2", "ADMIN_REPLY"}]}
, "94" => #{"Name"=>"EmailType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "REPLY"},{"2", "ADMIN_REPLY"}], "TagNum" => "94"}


,
"RawDataLength" => #{"TagNum" => "95" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "95" => #{"Name"=>"RawDataLength" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "95"}


,
"RawData" => #{"TagNum" => "96" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "96" => #{"Name"=>"RawData" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "96"}


,
"PossResend" => #{"TagNum" => "97" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "97" => #{"Name"=>"PossResend" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "97"}


,
"EncryptMethod" => #{"TagNum" => "98" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NONE"},{"1", "PKCS_1"},{"2", "DES"},{"3", "PKCS_3"},{"4", "PGP_4"},{"5", "PGP_5"},{"6", "PEM"}]}
, "98" => #{"Name"=>"EncryptMethod" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NONE"},{"1", "PKCS_1"},{"2", "DES"},{"3", "PKCS_3"},{"4", "PGP_4"},{"5", "PGP_5"},{"6", "PEM"}], "TagNum" => "98"}


,
"StopPx" => #{"TagNum" => "99" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "99" => #{"Name"=>"StopPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "99"}


,
"ExDestination" => #{"TagNum" => "100" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "100" => #{"Name"=>"ExDestination" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "100"}


,
"CxlRejReason" => #{"TagNum" => "102" ,"Type" => "INT" ,"ValidValues" =>[{"0", "TOO_LATE_TO_CANCEL"},{"1", "UNKNOWN_ORDER"},{"2", "BROKER"},{"3", "ORDER_ALREADY_IN_PENDING_CANCEL_OR_PENDING_REPLACE_STATUS"},{"4", "UNABLE_TO_PROCESS_ORDER_MASS_CANCEL_REQUEST"},{"5", "ORIGORDMODTIME"},{"6", "DUPLICATE_CLORDID"},{"7", "PRICE_EXCEEDS_CURRENT_PRICE"},{"8", "PRICE_EXCEEDS_CURRENT_PRICE_BAND"},{"18", "INVALID_PRICE_INCREMENT"},{"99", "OTHER"}]}
, "102" => #{"Name"=>"CxlRejReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "TOO_LATE_TO_CANCEL"},{"1", "UNKNOWN_ORDER"},{"2", "BROKER"},{"3", "ORDER_ALREADY_IN_PENDING_CANCEL_OR_PENDING_REPLACE_STATUS"},{"4", "UNABLE_TO_PROCESS_ORDER_MASS_CANCEL_REQUEST"},{"5", "ORIGORDMODTIME"},{"6", "DUPLICATE_CLORDID"},{"7", "PRICE_EXCEEDS_CURRENT_PRICE"},{"8", "PRICE_EXCEEDS_CURRENT_PRICE_BAND"},{"18", "INVALID_PRICE_INCREMENT"},{"99", "OTHER"}], "TagNum" => "102"}


,
"OrdRejReason" => #{"TagNum" => "103" ,"Type" => "INT" ,"ValidValues" =>[{"0", "BROKER"},{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE_CLOSED"},{"3", "ORDER_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "UNKNOWN_ORDER"},{"6", "DUPLICATE_ORDER"},{"7", "DUPLICATE_OF_A_VERBALLY_COMMUNICATED_ORDER"},{"8", "STALE_ORDER"},{"9", "TRADE_ALONG_REQUIRED"},{"10", "INVALID_INVESTOR_ID"},{"11", "UNSUPPORTED_ORDER_CHARACTERISTIC"},{"12", "SURVEILLENCE_OPTION"},{"13", "INCORRECT_QUANTITY"},{"14", "INCORRECT_ALLOCATED_QUANTITY"},{"15", "UNKNOWN_ACCOUNT"},{"16", "PRICE_EXCEEDS_CURRENT_PRICE_BAND"},{"18", "INVALID_PRICE_INCREMENT"},{"99", "OTHER"}]}
, "103" => #{"Name"=>"OrdRejReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "BROKER"},{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE_CLOSED"},{"3", "ORDER_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "UNKNOWN_ORDER"},{"6", "DUPLICATE_ORDER"},{"7", "DUPLICATE_OF_A_VERBALLY_COMMUNICATED_ORDER"},{"8", "STALE_ORDER"},{"9", "TRADE_ALONG_REQUIRED"},{"10", "INVALID_INVESTOR_ID"},{"11", "UNSUPPORTED_ORDER_CHARACTERISTIC"},{"12", "SURVEILLENCE_OPTION"},{"13", "INCORRECT_QUANTITY"},{"14", "INCORRECT_ALLOCATED_QUANTITY"},{"15", "UNKNOWN_ACCOUNT"},{"16", "PRICE_EXCEEDS_CURRENT_PRICE_BAND"},{"18", "INVALID_PRICE_INCREMENT"},{"99", "OTHER"}], "TagNum" => "103"}


,
"IOIQualifier" => #{"TagNum" => "104" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "ALL_OR_NONE"},{"B", "MARKET_ON_CLOSE"},{"C", "AT_THE_CLOSE"},{"D", "VWAP"},{"I", "IN_TOUCH_WITH"},{"L", "LIMIT"},{"M", "MORE_BEHIND"},{"O", "AT_THE_OPEN"},{"P", "TAKING_A_POSITION"},{"Q", "AT_THE_MARKET"},{"R", "READY_TO_TRADE"},{"S", "PORTFOLIO_SHOWN"},{"T", "THROUGH_THE_DAY"},{"V", "VERSUS"},{"W", "INDICATION"},{"X", "CROSSING_OPPORTUNITY"},{"Y", "AT_THE_MIDPOINT"},{"Z", "PRE_OPEN"}]}
, "104" => #{"Name"=>"IOIQualifier" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "ALL_OR_NONE"},{"B", "MARKET_ON_CLOSE"},{"C", "AT_THE_CLOSE"},{"D", "VWAP"},{"I", "IN_TOUCH_WITH"},{"L", "LIMIT"},{"M", "MORE_BEHIND"},{"O", "AT_THE_OPEN"},{"P", "TAKING_A_POSITION"},{"Q", "AT_THE_MARKET"},{"R", "READY_TO_TRADE"},{"S", "PORTFOLIO_SHOWN"},{"T", "THROUGH_THE_DAY"},{"V", "VERSUS"},{"W", "INDICATION"},{"X", "CROSSING_OPPORTUNITY"},{"Y", "AT_THE_MIDPOINT"},{"Z", "PRE_OPEN"}], "TagNum" => "104"}


,
"Issuer" => #{"TagNum" => "106" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "106" => #{"Name"=>"Issuer" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "106"}


,
"SecurityDesc" => #{"TagNum" => "107" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "107" => #{"Name"=>"SecurityDesc" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "107"}


,
"HeartBtInt" => #{"TagNum" => "108" ,"Type" => "INT" ,"ValidValues" =>[]}
, "108" => #{"Name"=>"HeartBtInt" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "108"}


,
"MinQty" => #{"TagNum" => "110" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "110" => #{"Name"=>"MinQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "110"}


,
"MaxFloor" => #{"TagNum" => "111" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "111" => #{"Name"=>"MaxFloor" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "111"}


,
"TestReqID" => #{"TagNum" => "112" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "112" => #{"Name"=>"TestReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "112"}


,
"ReportToExch" => #{"TagNum" => "113" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "113" => #{"Name"=>"ReportToExch" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "113"}


,
"LocateReqd" => #{"TagNum" => "114" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "114" => #{"Name"=>"LocateReqd" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "114"}


,
"OnBehalfOfCompID" => #{"TagNum" => "115" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "115" => #{"Name"=>"OnBehalfOfCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "115"}


,
"OnBehalfOfSubID" => #{"TagNum" => "116" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "116" => #{"Name"=>"OnBehalfOfSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "116"}


,
"QuoteID" => #{"TagNum" => "117" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "117" => #{"Name"=>"QuoteID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "117"}


,
"NetMoney" => #{"TagNum" => "118" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "118" => #{"Name"=>"NetMoney" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "118"}


,
"SettlCurrAmt" => #{"TagNum" => "119" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "119" => #{"Name"=>"SettlCurrAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "119"}


,
"SettlCurrency" => #{"TagNum" => "120" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "120" => #{"Name"=>"SettlCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "120"}


,
"ForexReq" => #{"TagNum" => "121" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "121" => #{"Name"=>"ForexReq" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "121"}


,
"OrigSendingTime" => #{"TagNum" => "122" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "122" => #{"Name"=>"OrigSendingTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "122"}


,
"GapFillFlag" => #{"TagNum" => "123" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "123" => #{"Name"=>"GapFillFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "123"}


,
"NoExecs" => #{"TagNum" => "124" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "124" => #{"Name"=>"NoExecs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "124"}


,
"ExpireTime" => #{"TagNum" => "126" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "126" => #{"Name"=>"ExpireTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "126"}


,
"DKReason" => #{"TagNum" => "127" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "UNKNOWN_SYMBOL"},{"B", "WRONG_SIDE"},{"C", "QUANTITY_EXCEEDS_ORDER"},{"D", "NO_MATCHING_ORDER"},{"E", "PRICE_EXCEEDS_LIMIT"},{"F", "CALCULATION_DIFFERENCE"},{"Z", "OTHER"}]}
, "127" => #{"Name"=>"DKReason" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "UNKNOWN_SYMBOL"},{"B", "WRONG_SIDE"},{"C", "QUANTITY_EXCEEDS_ORDER"},{"D", "NO_MATCHING_ORDER"},{"E", "PRICE_EXCEEDS_LIMIT"},{"F", "CALCULATION_DIFFERENCE"},{"Z", "OTHER"}], "TagNum" => "127"}


,
"DeliverToCompID" => #{"TagNum" => "128" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "128" => #{"Name"=>"DeliverToCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "128"}


,
"DeliverToSubID" => #{"TagNum" => "129" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "129" => #{"Name"=>"DeliverToSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "129"}


,
"IOINaturalFlag" => #{"TagNum" => "130" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "130" => #{"Name"=>"IOINaturalFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "130"}


,
"QuoteReqID" => #{"TagNum" => "131" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "131" => #{"Name"=>"QuoteReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "131"}


,
"BidPx" => #{"TagNum" => "132" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "132" => #{"Name"=>"BidPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "132"}


,
"OfferPx" => #{"TagNum" => "133" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "133" => #{"Name"=>"OfferPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "133"}


,
"BidSize" => #{"TagNum" => "134" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "134" => #{"Name"=>"BidSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "134"}


,
"OfferSize" => #{"TagNum" => "135" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "135" => #{"Name"=>"OfferSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "135"}


,
"NoMiscFees" => #{"TagNum" => "136" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "136" => #{"Name"=>"NoMiscFees" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "136"}


,
"MiscFeeAmt" => #{"TagNum" => "137" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "137" => #{"Name"=>"MiscFeeAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "137"}


,
"MiscFeeCurr" => #{"TagNum" => "138" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "138" => #{"Name"=>"MiscFeeCurr" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "138"}


,
"MiscFeeType" => #{"TagNum" => "139" ,"Type" => "STRING" ,"ValidValues" =>[{"1", "REGULATORY"},{"2", "TAX"},{"3", "LOCAL_COMMISSION"},{"4", "EXCHANGE_FEES"},{"5", "STAMP"},{"6", "LEVY"},{"7", "OTHER"},{"8", "MARKUP"},{"9", "CONSUMPTION_TAX"},{"10", "PER_TRANSACTION"},{"11", "CONVERSION"},{"12", "AGENT"},{"13", "TRANSFER_FEE"},{"14", "SECURITY_LENDING"}]}
, "139" => #{"Name"=>"MiscFeeType" ,"Type"=>"STRING" ,"ValidValues"=>[{"1", "REGULATORY"},{"2", "TAX"},{"3", "LOCAL_COMMISSION"},{"4", "EXCHANGE_FEES"},{"5", "STAMP"},{"6", "LEVY"},{"7", "OTHER"},{"8", "MARKUP"},{"9", "CONSUMPTION_TAX"},{"10", "PER_TRANSACTION"},{"11", "CONVERSION"},{"12", "AGENT"},{"13", "TRANSFER_FEE"},{"14", "SECURITY_LENDING"}], "TagNum" => "139"}


,
"PrevClosePx" => #{"TagNum" => "140" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "140" => #{"Name"=>"PrevClosePx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "140"}


,
"ResetSeqNumFlag" => #{"TagNum" => "141" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "141" => #{"Name"=>"ResetSeqNumFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "141"}


,
"SenderLocationID" => #{"TagNum" => "142" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "142" => #{"Name"=>"SenderLocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "142"}


,
"TargetLocationID" => #{"TagNum" => "143" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "143" => #{"Name"=>"TargetLocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "143"}


,
"OnBehalfOfLocationID" => #{"TagNum" => "144" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "144" => #{"Name"=>"OnBehalfOfLocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "144"}


,
"DeliverToLocationID" => #{"TagNum" => "145" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "145" => #{"Name"=>"DeliverToLocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "145"}


,
"NoRelatedSym" => #{"TagNum" => "146" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "146" => #{"Name"=>"NoRelatedSym" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "146"}


,
"Subject" => #{"TagNum" => "147" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "147" => #{"Name"=>"Subject" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "147"}


,
"Headline" => #{"TagNum" => "148" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "148" => #{"Name"=>"Headline" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "148"}


,
"URLLink" => #{"TagNum" => "149" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "149" => #{"Name"=>"URLLink" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "149"}


,
"ExecType" => #{"TagNum" => "150" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"3", "DONE_FOR_DAY"},{"4", "CANCELED"},{"5", "REPLACED"},{"6", "PENDING_CANCEL"},{"7", "STOPPED"},{"8", "REJECTED"},{"9", "SUSPENDED"},{"A", "PENDING_NEW"},{"B", "CALCULATED"},{"C", "EXPIRED"},{"D", "RESTATED"},{"E", "PENDING_REPLACE"},{"F", "TRADE"},{"G", "TRADE_CORRECT"},{"H", "TRADE_CANCEL"},{"I", "ORDER_STATUS"},{"J", "TRADE_IN_A_CLEARING_HOLD"},{"K", "TRADE_HAS_BEEN_RELEASED_TO_CLEARING"},{"L", "TRIGGERED_OR_ACTIVATED_BY_SYSTEM"}]}
, "150" => #{"Name"=>"ExecType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"3", "DONE_FOR_DAY"},{"4", "CANCELED"},{"5", "REPLACED"},{"6", "PENDING_CANCEL"},{"7", "STOPPED"},{"8", "REJECTED"},{"9", "SUSPENDED"},{"A", "PENDING_NEW"},{"B", "CALCULATED"},{"C", "EXPIRED"},{"D", "RESTATED"},{"E", "PENDING_REPLACE"},{"F", "TRADE"},{"G", "TRADE_CORRECT"},{"H", "TRADE_CANCEL"},{"I", "ORDER_STATUS"},{"J", "TRADE_IN_A_CLEARING_HOLD"},{"K", "TRADE_HAS_BEEN_RELEASED_TO_CLEARING"},{"L", "TRIGGERED_OR_ACTIVATED_BY_SYSTEM"}], "TagNum" => "150"}


,
"LeavesQty" => #{"TagNum" => "151" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "151" => #{"Name"=>"LeavesQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "151"}


,
"CashOrderQty" => #{"TagNum" => "152" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "152" => #{"Name"=>"CashOrderQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "152"}


,
"AllocAvgPx" => #{"TagNum" => "153" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "153" => #{"Name"=>"AllocAvgPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "153"}


,
"AllocNetMoney" => #{"TagNum" => "154" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "154" => #{"Name"=>"AllocNetMoney" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "154"}


,
"SettlCurrFxRate" => #{"TagNum" => "155" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "155" => #{"Name"=>"SettlCurrFxRate" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "155"}


,
"SettlCurrFxRateCalc" => #{"TagNum" => "156" ,"Type" => "CHAR" ,"ValidValues" =>[{"M", "MULTIPLY"},{"D", "DIVIDE"}]}
, "156" => #{"Name"=>"SettlCurrFxRateCalc" ,"Type"=>"CHAR" ,"ValidValues"=>[{"M", "MULTIPLY"},{"D", "DIVIDE"}], "TagNum" => "156"}


,
"NumDaysInterest" => #{"TagNum" => "157" ,"Type" => "INT" ,"ValidValues" =>[]}
, "157" => #{"Name"=>"NumDaysInterest" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "157"}


,
"AccruedInterestRate" => #{"TagNum" => "158" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "158" => #{"Name"=>"AccruedInterestRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "158"}


,
"AccruedInterestAmt" => #{"TagNum" => "159" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "159" => #{"Name"=>"AccruedInterestAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "159"}


,
"SettlInstMode" => #{"TagNum" => "160" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "DEFAULT"},{"1", "STANDING_INSTRUCTIONS_PROVIDED"},{"2", "SPECIFIC_ALLOCATION_ACCOUNT_OVERRIDING"},{"3", "SPECIFIC_ALLOCATION_ACCOUNT_STANDING"},{"4", "SPECIFIC_ORDER_FOR_A_SINGLE_ACCOUNT"},{"5", "REQUEST_REJECT"}]}
, "160" => #{"Name"=>"SettlInstMode" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "DEFAULT"},{"1", "STANDING_INSTRUCTIONS_PROVIDED"},{"2", "SPECIFIC_ALLOCATION_ACCOUNT_OVERRIDING"},{"3", "SPECIFIC_ALLOCATION_ACCOUNT_STANDING"},{"4", "SPECIFIC_ORDER_FOR_A_SINGLE_ACCOUNT"},{"5", "REQUEST_REJECT"}], "TagNum" => "160"}


,
"AllocText" => #{"TagNum" => "161" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "161" => #{"Name"=>"AllocText" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "161"}


,
"SettlInstID" => #{"TagNum" => "162" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "162" => #{"Name"=>"SettlInstID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "162"}


,
"SettlInstTransType" => #{"TagNum" => "163" ,"Type" => "CHAR" ,"ValidValues" =>[{"N", "NEW"},{"C", "CANCEL"},{"R", "REPLACE"},{"T", "RESTATE"}]}
, "163" => #{"Name"=>"SettlInstTransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"N", "NEW"},{"C", "CANCEL"},{"R", "REPLACE"},{"T", "RESTATE"}], "TagNum" => "163"}


,
"EmailThreadID" => #{"TagNum" => "164" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "164" => #{"Name"=>"EmailThreadID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "164"}


,
"SettlInstSource" => #{"TagNum" => "165" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "BROKERS_INSTRUCTIONS"},{"2", "INSTITUTIONS_INSTRUCTIONS"},{"3", "INVESTOR"}]}
, "165" => #{"Name"=>"SettlInstSource" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "BROKERS_INSTRUCTIONS"},{"2", "INSTITUTIONS_INSTRUCTIONS"},{"3", "INVESTOR"}], "TagNum" => "165"}


,
"SecurityType" => #{"TagNum" => "167" ,"Type" => "STRING" ,"ValidValues" =>[{"UST", "US_TREASURY_NOTE_UST"},{"USTB", "US_TREASURY_BILL_USTB"},{"EUSUPRA", "EURO_SUPRANATIONAL_COUPONS"},{"FAC", "FEDERAL_AGENCY_COUPON"},{"FADN", "FEDERAL_AGENCY_DISCOUNT_NOTE"},{"PEF", "PRIVATE_EXPORT_FUNDING"},{"SUPRA", "USD_SUPRANATIONAL_COUPONS"},{"CORP", "CORPORATE_BOND"},{"CPP", "CORPORATE_PRIVATE_PLACEMENT"},{"CB", "CONVERTIBLE_BOND"},{"DUAL", "DUAL_CURRENCY"},{"EUCORP", "EURO_CORPORATE_BOND"},{"EUFRN", "EURO_CORPORATE_FLOATING_RATE_NOTES"},{"FRN", "US_CORPORATE_FLOATING_RATE_NOTES"},{"XLINKD", "INDEXED_LINKED"},{"STRUCT", "STRUCTURED_NOTES"},{"YANK", "YANKEE_CORPORATE_BOND"},{"FOR", "FOREIGN_EXCHANGE_CONTRACT"},{"CDS", "CREDIT_DEFAULT_SWAP"},{"FUT", "FUTURE"},{"OPT", "OPTION"},{"OOF", "OPTIONS_ON_FUTURES"},{"OOP", "OPTIONS_ON_PHYSICAL"},{"IRS", "INTEREST_RATE_SWAP"},{"OOC", "OPTIONS_ON_COMBO"},{"CS", "COMMON_STOCK"},{"PS", "PREFERRED_STOCK"},{"REPO", "REPURCHASE"},{"FORWARD", "FORWARD"},{"BUYSELL", "BUY_SELLBACK"},{"SECLOAN", "SECURITIES_LOAN"},{"SECPLEDGE", "SECURITIES_PLEDGE"},{"BRADY", "BRADY_BOND"},{"CAN", "CANADIAN_TREASURY_NOTES"},{"CTB", "CANADIAN_TREASURY_BILLS"},{"EUSOV", "EURO_SOVEREIGNS"},{"PROV", "CANADIAN_PROVINCIAL_BONDS"},{"TB", "TREASURY_BILL"},{"TBOND", "US_TREASURY_BOND"},{"TINT", "INTEREST_STRIP_FROM_ANY_BOND_OR_NOTE"},{"TBILL", "US_TREASURY_BILL_TBILL"},{"TIPS", "TREASURY_INFLATION_PROTECTED_SECURITIES"},{"TCAL", "PRINCIPAL_STRIP_OF_A_CALLABLE_BOND_OR_NOTE"},{"TPRN", "PRINCIPAL_STRIP_FROM_A_NON_CALLABLE_BOND_OR_NOTE"},{"TNOTE", "US_TREASURY_NOTE_TNOTE"},{"TERM", "TERM_LOAN"},{"RVLV", "REVOLVER_LOAN"},{"RVLVTRM", "REVOLVER_TERM_LOAN"},{"BRIDGE", "BRIDGE_LOAN"},{"LOFC", "LETTER_OF_CREDIT"},{"SWING", "SWING_LINE_FACILITY"},{"DINP", "DEBTOR_IN_POSSESSION"},{"DEFLTED", "DEFAULTED"},{"WITHDRN", "WITHDRAWN"},{"REPLACD", "REPLACED"},{"MATURED", "MATURED"},{"AMENDED", "AMENDED_RESTATED"},{"RETIRED", "RETIRED"},{"BA", "BANKERS_ACCEPTANCE"},{"BDN", "BANK_DEPOSITORY_NOTE"},{"BN", "BANK_NOTES"},{"BOX", "BILL_OF_EXCHANGES"},{"CAMM", "CANADIAN_MONEY_MARKETS"},{"CD", "CERTIFICATE_OF_DEPOSIT"},{"CL", "CALL_LOANS"},{"CP", "COMMERCIAL_PAPER"},{"DN", "DEPOSIT_NOTES"},{"EUCD", "EURO_CERTIFICATE_OF_DEPOSIT"},{"EUCP", "EURO_COMMERCIAL_PAPER"},{"LQN", "LIQUIDITY_NOTE"},{"MTN", "MEDIUM_TERM_NOTES"},{"ONITE", "OVERNIGHT"},{"PN", "PROMISSORY_NOTE"},{"STN", "SHORT_TERM_LOAN_NOTE"},{"PZFJ", "PLAZOS_FIJOS"},{"SLQN", "SECURED_LIQUIDITY_NOTE"},{"TD", "TIME_DEPOSIT"},{"TLQN", "TERM_LIQUIDITY_NOTE"},{"XCN", "EXTENDED_COMM_NOTE"},{"YCD", "YANKEE_CERTIFICATE_OF_DEPOSIT"},{"ABS", "ASSET_BACKED_SECURITIES"},{"CMB", "CANADIAN_MORTGAGE_BONDS"},{"CMBS", "CORP_MORTGAGE_BACKED_SECURITIES"},{"CMO", "COLLATERALIZED_MORTGAGE_OBLIGATION"},{"IET", "IOETTE_MORTGAGE"},{"MBS", "MORTGAGE_BACKED_SECURITIES"},{"MIO", "MORTGAGE_INTEREST_ONLY"},{"MPO", "MORTGAGE_PRINCIPAL_ONLY"},{"MPP", "MORTGAGE_PRIVATE_PLACEMENT"},{"MPT", "MISCELLANEOUS_PASS_THROUGH"},{"PFAND", "PFANDBRIEFE"},{"TBA", "TO_BE_ANNOUNCED"},{"AN", "OTHER_ANTICIPATION_NOTES"},{"COFO", "CERTIFICATE_OF_OBLIGATION"},{"COFP", "CERTIFICATE_OF_PARTICIPATION"},{"GO", "GENERAL_OBLIGATION_BONDS"},{"MT", "MANDATORY_TENDER"},{"RAN", "REVENUE_ANTICIPATION_NOTE"},{"REV", "REVENUE_BONDS"},{"SPCLA", "SPECIAL_ASSESSMENT"},{"SPCLO", "SPECIAL_OBLIGATION"},{"SPCLT", "SPECIAL_TAX"},{"TAN", "TAX_ANTICIPATION_NOTE"},{"TAXA", "TAX_ALLOCATION"},{"TECP", "TAX_EXEMPT_COMMERCIAL_PAPER"},{"TMCP", "TAXABLE_MUNICIPAL_CP"},{"TRAN", "TAX_REVENUE_ANTICIPATION_NOTE"},{"VRDN", "VARIABLE_RATE_DEMAND_NOTE"},{"WAR", "WARRANT"},{"MF", "MUTUAL_FUND"},{"MLEG", "MULTILEG_INSTRUMENT"},{"NONE", "NO_SECURITY_TYPE"},{"?", "WILDCARD_ENTRY_FOR_USE_ON_SECURITY_DEFINITION_REQUEST"},{"CASH", "CASH"},{"FXNDF", "NON_DELIVERABLE_FORWARD"},{"FXSPOT", "FX_SPOT"},{"FXFWD", "FX_FORWARD"},{"FXSWAP", "FX_SWAP"}]}
, "167" => #{"Name"=>"SecurityType" ,"Type"=>"STRING" ,"ValidValues"=>[{"UST", "US_TREASURY_NOTE_UST"},{"USTB", "US_TREASURY_BILL_USTB"},{"EUSUPRA", "EURO_SUPRANATIONAL_COUPONS"},{"FAC", "FEDERAL_AGENCY_COUPON"},{"FADN", "FEDERAL_AGENCY_DISCOUNT_NOTE"},{"PEF", "PRIVATE_EXPORT_FUNDING"},{"SUPRA", "USD_SUPRANATIONAL_COUPONS"},{"CORP", "CORPORATE_BOND"},{"CPP", "CORPORATE_PRIVATE_PLACEMENT"},{"CB", "CONVERTIBLE_BOND"},{"DUAL", "DUAL_CURRENCY"},{"EUCORP", "EURO_CORPORATE_BOND"},{"EUFRN", "EURO_CORPORATE_FLOATING_RATE_NOTES"},{"FRN", "US_CORPORATE_FLOATING_RATE_NOTES"},{"XLINKD", "INDEXED_LINKED"},{"STRUCT", "STRUCTURED_NOTES"},{"YANK", "YANKEE_CORPORATE_BOND"},{"FOR", "FOREIGN_EXCHANGE_CONTRACT"},{"CDS", "CREDIT_DEFAULT_SWAP"},{"FUT", "FUTURE"},{"OPT", "OPTION"},{"OOF", "OPTIONS_ON_FUTURES"},{"OOP", "OPTIONS_ON_PHYSICAL"},{"IRS", "INTEREST_RATE_SWAP"},{"OOC", "OPTIONS_ON_COMBO"},{"CS", "COMMON_STOCK"},{"PS", "PREFERRED_STOCK"},{"REPO", "REPURCHASE"},{"FORWARD", "FORWARD"},{"BUYSELL", "BUY_SELLBACK"},{"SECLOAN", "SECURITIES_LOAN"},{"SECPLEDGE", "SECURITIES_PLEDGE"},{"BRADY", "BRADY_BOND"},{"CAN", "CANADIAN_TREASURY_NOTES"},{"CTB", "CANADIAN_TREASURY_BILLS"},{"EUSOV", "EURO_SOVEREIGNS"},{"PROV", "CANADIAN_PROVINCIAL_BONDS"},{"TB", "TREASURY_BILL"},{"TBOND", "US_TREASURY_BOND"},{"TINT", "INTEREST_STRIP_FROM_ANY_BOND_OR_NOTE"},{"TBILL", "US_TREASURY_BILL_TBILL"},{"TIPS", "TREASURY_INFLATION_PROTECTED_SECURITIES"},{"TCAL", "PRINCIPAL_STRIP_OF_A_CALLABLE_BOND_OR_NOTE"},{"TPRN", "PRINCIPAL_STRIP_FROM_A_NON_CALLABLE_BOND_OR_NOTE"},{"TNOTE", "US_TREASURY_NOTE_TNOTE"},{"TERM", "TERM_LOAN"},{"RVLV", "REVOLVER_LOAN"},{"RVLVTRM", "REVOLVER_TERM_LOAN"},{"BRIDGE", "BRIDGE_LOAN"},{"LOFC", "LETTER_OF_CREDIT"},{"SWING", "SWING_LINE_FACILITY"},{"DINP", "DEBTOR_IN_POSSESSION"},{"DEFLTED", "DEFAULTED"},{"WITHDRN", "WITHDRAWN"},{"REPLACD", "REPLACED"},{"MATURED", "MATURED"},{"AMENDED", "AMENDED_RESTATED"},{"RETIRED", "RETIRED"},{"BA", "BANKERS_ACCEPTANCE"},{"BDN", "BANK_DEPOSITORY_NOTE"},{"BN", "BANK_NOTES"},{"BOX", "BILL_OF_EXCHANGES"},{"CAMM", "CANADIAN_MONEY_MARKETS"},{"CD", "CERTIFICATE_OF_DEPOSIT"},{"CL", "CALL_LOANS"},{"CP", "COMMERCIAL_PAPER"},{"DN", "DEPOSIT_NOTES"},{"EUCD", "EURO_CERTIFICATE_OF_DEPOSIT"},{"EUCP", "EURO_COMMERCIAL_PAPER"},{"LQN", "LIQUIDITY_NOTE"},{"MTN", "MEDIUM_TERM_NOTES"},{"ONITE", "OVERNIGHT"},{"PN", "PROMISSORY_NOTE"},{"STN", "SHORT_TERM_LOAN_NOTE"},{"PZFJ", "PLAZOS_FIJOS"},{"SLQN", "SECURED_LIQUIDITY_NOTE"},{"TD", "TIME_DEPOSIT"},{"TLQN", "TERM_LIQUIDITY_NOTE"},{"XCN", "EXTENDED_COMM_NOTE"},{"YCD", "YANKEE_CERTIFICATE_OF_DEPOSIT"},{"ABS", "ASSET_BACKED_SECURITIES"},{"CMB", "CANADIAN_MORTGAGE_BONDS"},{"CMBS", "CORP_MORTGAGE_BACKED_SECURITIES"},{"CMO", "COLLATERALIZED_MORTGAGE_OBLIGATION"},{"IET", "IOETTE_MORTGAGE"},{"MBS", "MORTGAGE_BACKED_SECURITIES"},{"MIO", "MORTGAGE_INTEREST_ONLY"},{"MPO", "MORTGAGE_PRINCIPAL_ONLY"},{"MPP", "MORTGAGE_PRIVATE_PLACEMENT"},{"MPT", "MISCELLANEOUS_PASS_THROUGH"},{"PFAND", "PFANDBRIEFE"},{"TBA", "TO_BE_ANNOUNCED"},{"AN", "OTHER_ANTICIPATION_NOTES"},{"COFO", "CERTIFICATE_OF_OBLIGATION"},{"COFP", "CERTIFICATE_OF_PARTICIPATION"},{"GO", "GENERAL_OBLIGATION_BONDS"},{"MT", "MANDATORY_TENDER"},{"RAN", "REVENUE_ANTICIPATION_NOTE"},{"REV", "REVENUE_BONDS"},{"SPCLA", "SPECIAL_ASSESSMENT"},{"SPCLO", "SPECIAL_OBLIGATION"},{"SPCLT", "SPECIAL_TAX"},{"TAN", "TAX_ANTICIPATION_NOTE"},{"TAXA", "TAX_ALLOCATION"},{"TECP", "TAX_EXEMPT_COMMERCIAL_PAPER"},{"TMCP", "TAXABLE_MUNICIPAL_CP"},{"TRAN", "TAX_REVENUE_ANTICIPATION_NOTE"},{"VRDN", "VARIABLE_RATE_DEMAND_NOTE"},{"WAR", "WARRANT"},{"MF", "MUTUAL_FUND"},{"MLEG", "MULTILEG_INSTRUMENT"},{"NONE", "NO_SECURITY_TYPE"},{"?", "WILDCARD_ENTRY_FOR_USE_ON_SECURITY_DEFINITION_REQUEST"},{"CASH", "CASH"},{"FXNDF", "NON_DELIVERABLE_FORWARD"},{"FXSPOT", "FX_SPOT"},{"FXFWD", "FX_FORWARD"},{"FXSWAP", "FX_SWAP"}], "TagNum" => "167"}


,
"EffectiveTime" => #{"TagNum" => "168" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "168" => #{"Name"=>"EffectiveTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "168"}


,
"StandInstDbType" => #{"TagNum" => "169" ,"Type" => "INT" ,"ValidValues" =>[{"0", "OTHER"},{"1", "DTC_SID"},{"2", "THOMSON_ALERT"},{"3", "A_GLOBAL_CUSTODIAN"},{"4", "ACCOUNTNET"}]}
, "169" => #{"Name"=>"StandInstDbType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "OTHER"},{"1", "DTC_SID"},{"2", "THOMSON_ALERT"},{"3", "A_GLOBAL_CUSTODIAN"},{"4", "ACCOUNTNET"}], "TagNum" => "169"}


,
"StandInstDbName" => #{"TagNum" => "170" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "170" => #{"Name"=>"StandInstDbName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "170"}


,
"StandInstDbID" => #{"TagNum" => "171" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "171" => #{"Name"=>"StandInstDbID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "171"}


,
"SettlDeliveryType" => #{"TagNum" => "172" ,"Type" => "INT" ,"ValidValues" =>[{"0", "VERSUS_PAYMENT_DELIVER"},{"1", "FREE_DELIVER"},{"2", "TRI_PARTY"},{"3", "HOLD_IN_CUSTODY"}]}
, "172" => #{"Name"=>"SettlDeliveryType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "VERSUS_PAYMENT_DELIVER"},{"1", "FREE_DELIVER"},{"2", "TRI_PARTY"},{"3", "HOLD_IN_CUSTODY"}], "TagNum" => "172"}


,
"BidSpotRate" => #{"TagNum" => "188" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "188" => #{"Name"=>"BidSpotRate" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "188"}


,
"BidForwardPoints" => #{"TagNum" => "189" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "189" => #{"Name"=>"BidForwardPoints" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "189"}


,
"OfferSpotRate" => #{"TagNum" => "190" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "190" => #{"Name"=>"OfferSpotRate" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "190"}


,
"OfferForwardPoints" => #{"TagNum" => "191" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "191" => #{"Name"=>"OfferForwardPoints" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "191"}


,
"OrderQty2" => #{"TagNum" => "192" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "192" => #{"Name"=>"OrderQty2" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "192"}


,
"SettlDate2" => #{"TagNum" => "193" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "193" => #{"Name"=>"SettlDate2" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "193"}


,
"LastSpotRate" => #{"TagNum" => "194" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "194" => #{"Name"=>"LastSpotRate" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "194"}


,
"LastForwardPoints" => #{"TagNum" => "195" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "195" => #{"Name"=>"LastForwardPoints" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "195"}


,
"AllocLinkID" => #{"TagNum" => "196" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "196" => #{"Name"=>"AllocLinkID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "196"}


,
"AllocLinkType" => #{"TagNum" => "197" ,"Type" => "INT" ,"ValidValues" =>[{"0", "FX_NETTING"},{"1", "FX_SWAP"}]}
, "197" => #{"Name"=>"AllocLinkType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "FX_NETTING"},{"1", "FX_SWAP"}], "TagNum" => "197"}


,
"SecondaryOrderID" => #{"TagNum" => "198" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "198" => #{"Name"=>"SecondaryOrderID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "198"}


,
"NoIOIQualifiers" => #{"TagNum" => "199" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "199" => #{"Name"=>"NoIOIQualifiers" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "199"}


,
"MaturityMonthYear" => #{"TagNum" => "200" ,"Type" => "MONTHYEAR" ,"ValidValues" =>[]}
, "200" => #{"Name"=>"MaturityMonthYear" ,"Type"=>"MONTHYEAR" ,"ValidValues"=>[], "TagNum" => "200"}


,
"PutOrCall" => #{"TagNum" => "201" ,"Type" => "INT" ,"ValidValues" =>[{"0", "PUT"},{"1", "CALL"}]}
, "201" => #{"Name"=>"PutOrCall" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "PUT"},{"1", "CALL"}], "TagNum" => "201"}


,
"StrikePrice" => #{"TagNum" => "202" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "202" => #{"Name"=>"StrikePrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "202"}


,
"CoveredOrUncovered" => #{"TagNum" => "203" ,"Type" => "INT" ,"ValidValues" =>[{"0", "COVERED"},{"1", "UNCOVERED"}]}
, "203" => #{"Name"=>"CoveredOrUncovered" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "COVERED"},{"1", "UNCOVERED"}], "TagNum" => "203"}


,
"OptAttribute" => #{"TagNum" => "206" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "206" => #{"Name"=>"OptAttribute" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "206"}


,
"SecurityExchange" => #{"TagNum" => "207" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "207" => #{"Name"=>"SecurityExchange" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "207"}


,
"NotifyBrokerOfCredit" => #{"TagNum" => "208" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "208" => #{"Name"=>"NotifyBrokerOfCredit" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "208"}


,
"AllocHandlInst" => #{"TagNum" => "209" ,"Type" => "INT" ,"ValidValues" =>[{"1", "MATCH"},{"2", "FORWARD"},{"3", "FORWARD_AND_MATCH"}]}
, "209" => #{"Name"=>"AllocHandlInst" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "MATCH"},{"2", "FORWARD"},{"3", "FORWARD_AND_MATCH"}], "TagNum" => "209"}


,
"MaxShow" => #{"TagNum" => "210" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "210" => #{"Name"=>"MaxShow" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "210"}


,
"PegOffsetValue" => #{"TagNum" => "211" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "211" => #{"Name"=>"PegOffsetValue" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "211"}


,
"XmlDataLen" => #{"TagNum" => "212" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "212" => #{"Name"=>"XmlDataLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "212"}


,
"XmlData" => #{"TagNum" => "213" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "213" => #{"Name"=>"XmlData" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "213"}


,
"SettlInstRefID" => #{"TagNum" => "214" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "214" => #{"Name"=>"SettlInstRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "214"}


,
"NoRoutingIDs" => #{"TagNum" => "215" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "215" => #{"Name"=>"NoRoutingIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "215"}


,
"RoutingType" => #{"TagNum" => "216" ,"Type" => "INT" ,"ValidValues" =>[{"1", "TARGET_FIRM"},{"2", "TARGET_LIST"},{"3", "BLOCK_FIRM"},{"4", "BLOCK_LIST"}]}
, "216" => #{"Name"=>"RoutingType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "TARGET_FIRM"},{"2", "TARGET_LIST"},{"3", "BLOCK_FIRM"},{"4", "BLOCK_LIST"}], "TagNum" => "216"}


,
"RoutingID" => #{"TagNum" => "217" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "217" => #{"Name"=>"RoutingID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "217"}


,
"Spread" => #{"TagNum" => "218" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "218" => #{"Name"=>"Spread" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "218"}


,
"BenchmarkCurveCurrency" => #{"TagNum" => "220" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "220" => #{"Name"=>"BenchmarkCurveCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "220"}


,
"BenchmarkCurveName" => #{"TagNum" => "221" ,"Type" => "STRING" ,"ValidValues" =>[{"EONIA", "EONIA"},{"EUREPO", "EUREPO"},{"Euribor", "EURIBOR"},{"FutureSWAP", "FUTURESWAP"},{"LIBID", "LIBID"},{"LIBOR", "LIBOR"},{"MuniAAA", "MUNIAAA"},{"OTHER", "OTHER"},{"Pfandbriefe", "PFANDBRIEFE"},{"SONIA", "SONIA"},{"SWAP", "SWAP"},{"Treasury", "TREASURY"}]}
, "221" => #{"Name"=>"BenchmarkCurveName" ,"Type"=>"STRING" ,"ValidValues"=>[{"EONIA", "EONIA"},{"EUREPO", "EUREPO"},{"Euribor", "EURIBOR"},{"FutureSWAP", "FUTURESWAP"},{"LIBID", "LIBID"},{"LIBOR", "LIBOR"},{"MuniAAA", "MUNIAAA"},{"OTHER", "OTHER"},{"Pfandbriefe", "PFANDBRIEFE"},{"SONIA", "SONIA"},{"SWAP", "SWAP"},{"Treasury", "TREASURY"}], "TagNum" => "221"}


,
"BenchmarkCurvePoint" => #{"TagNum" => "222" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "222" => #{"Name"=>"BenchmarkCurvePoint" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "222"}


,
"CouponRate" => #{"TagNum" => "223" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "223" => #{"Name"=>"CouponRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "223"}


,
"CouponPaymentDate" => #{"TagNum" => "224" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "224" => #{"Name"=>"CouponPaymentDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "224"}


,
"IssueDate" => #{"TagNum" => "225" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "225" => #{"Name"=>"IssueDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "225"}


,
"RepurchaseTerm" => #{"TagNum" => "226" ,"Type" => "INT" ,"ValidValues" =>[]}
, "226" => #{"Name"=>"RepurchaseTerm" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "226"}


,
"RepurchaseRate" => #{"TagNum" => "227" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "227" => #{"Name"=>"RepurchaseRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "227"}


,
"Factor" => #{"TagNum" => "228" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "228" => #{"Name"=>"Factor" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "228"}


,
"TradeOriginationDate" => #{"TagNum" => "229" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "229" => #{"Name"=>"TradeOriginationDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "229"}


,
"ExDate" => #{"TagNum" => "230" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "230" => #{"Name"=>"ExDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "230"}


,
"ContractMultiplier" => #{"TagNum" => "231" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "231" => #{"Name"=>"ContractMultiplier" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "231"}


,
"NoStipulations" => #{"TagNum" => "232" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "232" => #{"Name"=>"NoStipulations" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "232"}


,
"StipulationType" => #{"TagNum" => "233" ,"Type" => "STRING" ,"ValidValues" =>[{"AMT", "ALTERNATIVE_MINIMUM_TAX"},{"AUTOREINV", "AUTO_REINVESTMENT_AT_RATE_OR_BETTER"},{"BANKQUAL", "BANK_QUALIFIED"},{"BGNCON", "BARGAIN_CONDITIONS"},{"COUPON", "COUPON_RANGE"},{"CURRENCY", "ISO_CURRENCY_CODE"},{"CUSTOMDATE", "CUSTOM_START_END_DATE"},{"GEOG", "GEOGRAPHICS_AND_RANGE"},{"HAIRCUT", "VALUATION_DISCOUNT"},{"INSURED", "INSURED"},{"ISSUE", "YEAR_OR_YEAR_MONTH_OF_ISSUE"},{"ISSUER", "ISSUERS_TICKER"},{"ISSUESIZE", "ISSUE_SIZE_RANGE"},{"LOOKBACK", "LOOKBACK_DAYS"},{"LOT", "EXPLICIT_LOT_IDENTIFIER"},{"LOTVAR", "LOT_VARIANCE"},{"MAT", "MATURITY_YEAR_AND_MONTH"},{"MATURITY", "MATURITY_RANGE"},{"MAXSUBS", "MAXIMUM_SUBSTITUTIONS"},{"MINDNOM", "MINIMUM_DENOMINATION"},{"MININCR", "MINIMUM_INCREMENT"},{"MINQTY", "MINIMUM_QUANTITY"},{"PAYFREQ", "PAYMENT_FREQUENCY_CALENDAR"},{"PIECES", "NUMBER_OF_PIECES"},{"PMAX", "POOLS_MAXIMUM"},{"PPL", "POOLS_PER_LOT"},{"PPM", "POOLS_PER_MILLION"},{"PPT", "POOLS_PER_TRADE"},{"PRICE", "PRICE_RANGE"},{"PRICEFREQ", "PRICING_FREQUENCY"},{"PROD", "PRODUCTION_YEAR"},{"PROTECT", "CALL_PROTECTION"},{"PURPOSE", "PURPOSE"},{"PXSOURCE", "BENCHMARK_PRICE_SOURCE"},{"RATING", "RATING_SOURCE_AND_RANGE"},{"REDEMPTION", "TYPE_OF_REDEMPTION"},{"RESTRICTED", "RESTRICTED"},{"SECTOR", "MARKET_SECTOR"},{"SECTYPE", "SECURITY_TYPE_INCLUDED_OR_EXCLUDED"},{"STRUCT", "STRUCTURE"},{"SUBSFREQ", "SUBSTITUTIONS_FREQUENCY"},{"SUBSLEFT", "SUBSTITUTIONS_LEFT"},{"TEXT", "FREEFORM_TEXT"},{"TRDVAR", "TRADE_VARIANCE"},{"WAC", "WEIGHTED_AVERAGE_COUPON"},{"WAL", "WEIGHTED_AVERAGE_LIFE_COUPON"},{"WALA", "WEIGHTED_AVERAGE_LOAN_AGE"},{"WAM", "WEIGHTED_AVERAGE_MATURITY"},{"WHOLE", "WHOLE_POOL"},{"YIELD", "YIELD_RANGE"},{"AVFICO", "AVERAGE_FICO_SCORE"},{"AVSIZE", "AVERAGE_LOAN_SIZE"},{"MAXBAL", "MAXIMUM_LOAN_BALANCE"},{"POOL", "POOL_IDENTIFIER"},{"ROLLTYPE", "TYPE_OF_ROLL_TRADE"},{"REFTRADE", "REFERENCE_TO_ROLLING_OR_CLOSING_TRADE"},{"REFPRIN", "PRINCIPAL_OF_ROLLING_OR_CLOSING_TRADE"},{"REFINT", "INTEREST_OF_ROLLING_OR_CLOSING_TRADE"},{"AVAILQTY", "AVAILABLE_OFFER_QUANTITY_TO_BE_SHOWN_TO_THE_STREET"},{"BROKERCREDIT", "BROKERS_SALES_CREDIT"},{"INTERNALPX", "OFFER_PRICE_TO_BE_SHOWN_TO_INTERNAL_BROKERS"},{"INTERNALQTY", "OFFER_QUANTITY_TO_BE_SHOWN_TO_INTERNAL_BROKERS"},{"LEAVEQTY", "THE_MINIMUM_RESIDUAL_OFFER_QUANTITY"},{"MAXORDQTY", "MAXIMUM_ORDER_SIZE"},{"ORDRINCR", "ORDER_QUANTITY_INCREMENT"},{"PRIMARY", "PRIMARY_OR_SECONDARY_MARKET_INDICATOR"},{"SALESCREDITOVR", "BROKER_SALES_CREDIT_OVERRIDE"},{"TRADERCREDIT", "TRADERS_CREDIT"},{"DISCOUNT", "DISCOUNT_RATE"},{"YTM", "YIELD_TO_MATURITY"},{"ABS", "ABSOLUTE_PREPAYMENT_SPEED"},{"CPP", "CONSTANT_PREPAYMENT_PENALTY"},{"CPR", "CONSTANT_PREPAYMENT_RATE"},{"CPY", "CONSTANT_PREPAYMENT_YIELD"},{"HEP", "FINAL_CPR_OF_HOME_EQUITY_PREPAYMENT_CURVE"},{"MHP", "PERCENT_OF_MANUFACTURED_HOUSING_PREPAYMENT_CURVE"},{"MPR", "MONTHLY_PREPAYMENT_RATE"},{"PPC", "PERCENT_OF_PROSPECTUS_PREPAYMENT_CURVE"},{"PSA", "PERCENT_OF_BMA_PREPAYMENT_CURVE"},{"SMM", "SINGLE_MONTHLY_MORTALITY"}]}
, "233" => #{"Name"=>"StipulationType" ,"Type"=>"STRING" ,"ValidValues"=>[{"AMT", "ALTERNATIVE_MINIMUM_TAX"},{"AUTOREINV", "AUTO_REINVESTMENT_AT_RATE_OR_BETTER"},{"BANKQUAL", "BANK_QUALIFIED"},{"BGNCON", "BARGAIN_CONDITIONS"},{"COUPON", "COUPON_RANGE"},{"CURRENCY", "ISO_CURRENCY_CODE"},{"CUSTOMDATE", "CUSTOM_START_END_DATE"},{"GEOG", "GEOGRAPHICS_AND_RANGE"},{"HAIRCUT", "VALUATION_DISCOUNT"},{"INSURED", "INSURED"},{"ISSUE", "YEAR_OR_YEAR_MONTH_OF_ISSUE"},{"ISSUER", "ISSUERS_TICKER"},{"ISSUESIZE", "ISSUE_SIZE_RANGE"},{"LOOKBACK", "LOOKBACK_DAYS"},{"LOT", "EXPLICIT_LOT_IDENTIFIER"},{"LOTVAR", "LOT_VARIANCE"},{"MAT", "MATURITY_YEAR_AND_MONTH"},{"MATURITY", "MATURITY_RANGE"},{"MAXSUBS", "MAXIMUM_SUBSTITUTIONS"},{"MINDNOM", "MINIMUM_DENOMINATION"},{"MININCR", "MINIMUM_INCREMENT"},{"MINQTY", "MINIMUM_QUANTITY"},{"PAYFREQ", "PAYMENT_FREQUENCY_CALENDAR"},{"PIECES", "NUMBER_OF_PIECES"},{"PMAX", "POOLS_MAXIMUM"},{"PPL", "POOLS_PER_LOT"},{"PPM", "POOLS_PER_MILLION"},{"PPT", "POOLS_PER_TRADE"},{"PRICE", "PRICE_RANGE"},{"PRICEFREQ", "PRICING_FREQUENCY"},{"PROD", "PRODUCTION_YEAR"},{"PROTECT", "CALL_PROTECTION"},{"PURPOSE", "PURPOSE"},{"PXSOURCE", "BENCHMARK_PRICE_SOURCE"},{"RATING", "RATING_SOURCE_AND_RANGE"},{"REDEMPTION", "TYPE_OF_REDEMPTION"},{"RESTRICTED", "RESTRICTED"},{"SECTOR", "MARKET_SECTOR"},{"SECTYPE", "SECURITY_TYPE_INCLUDED_OR_EXCLUDED"},{"STRUCT", "STRUCTURE"},{"SUBSFREQ", "SUBSTITUTIONS_FREQUENCY"},{"SUBSLEFT", "SUBSTITUTIONS_LEFT"},{"TEXT", "FREEFORM_TEXT"},{"TRDVAR", "TRADE_VARIANCE"},{"WAC", "WEIGHTED_AVERAGE_COUPON"},{"WAL", "WEIGHTED_AVERAGE_LIFE_COUPON"},{"WALA", "WEIGHTED_AVERAGE_LOAN_AGE"},{"WAM", "WEIGHTED_AVERAGE_MATURITY"},{"WHOLE", "WHOLE_POOL"},{"YIELD", "YIELD_RANGE"},{"AVFICO", "AVERAGE_FICO_SCORE"},{"AVSIZE", "AVERAGE_LOAN_SIZE"},{"MAXBAL", "MAXIMUM_LOAN_BALANCE"},{"POOL", "POOL_IDENTIFIER"},{"ROLLTYPE", "TYPE_OF_ROLL_TRADE"},{"REFTRADE", "REFERENCE_TO_ROLLING_OR_CLOSING_TRADE"},{"REFPRIN", "PRINCIPAL_OF_ROLLING_OR_CLOSING_TRADE"},{"REFINT", "INTEREST_OF_ROLLING_OR_CLOSING_TRADE"},{"AVAILQTY", "AVAILABLE_OFFER_QUANTITY_TO_BE_SHOWN_TO_THE_STREET"},{"BROKERCREDIT", "BROKERS_SALES_CREDIT"},{"INTERNALPX", "OFFER_PRICE_TO_BE_SHOWN_TO_INTERNAL_BROKERS"},{"INTERNALQTY", "OFFER_QUANTITY_TO_BE_SHOWN_TO_INTERNAL_BROKERS"},{"LEAVEQTY", "THE_MINIMUM_RESIDUAL_OFFER_QUANTITY"},{"MAXORDQTY", "MAXIMUM_ORDER_SIZE"},{"ORDRINCR", "ORDER_QUANTITY_INCREMENT"},{"PRIMARY", "PRIMARY_OR_SECONDARY_MARKET_INDICATOR"},{"SALESCREDITOVR", "BROKER_SALES_CREDIT_OVERRIDE"},{"TRADERCREDIT", "TRADERS_CREDIT"},{"DISCOUNT", "DISCOUNT_RATE"},{"YTM", "YIELD_TO_MATURITY"},{"ABS", "ABSOLUTE_PREPAYMENT_SPEED"},{"CPP", "CONSTANT_PREPAYMENT_PENALTY"},{"CPR", "CONSTANT_PREPAYMENT_RATE"},{"CPY", "CONSTANT_PREPAYMENT_YIELD"},{"HEP", "FINAL_CPR_OF_HOME_EQUITY_PREPAYMENT_CURVE"},{"MHP", "PERCENT_OF_MANUFACTURED_HOUSING_PREPAYMENT_CURVE"},{"MPR", "MONTHLY_PREPAYMENT_RATE"},{"PPC", "PERCENT_OF_PROSPECTUS_PREPAYMENT_CURVE"},{"PSA", "PERCENT_OF_BMA_PREPAYMENT_CURVE"},{"SMM", "SINGLE_MONTHLY_MORTALITY"}], "TagNum" => "233"}


,
"StipulationValue" => #{"TagNum" => "234" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "234" => #{"Name"=>"StipulationValue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "234"}


,
"YieldType" => #{"TagNum" => "235" ,"Type" => "STRING" ,"ValidValues" =>[{"AFTERTAX", "AFTER_TAX_YIELD"},{"ANNUAL", "ANNUAL_YIELD"},{"ATISSUE", "YIELD_AT_ISSUE"},{"AVGMATURITY", "YIELD_TO_AVG_MATURITY"},{"BOOK", "BOOK_YIELD"},{"CALL", "YIELD_TO_NEXT_CALL"},{"CHANGE", "YIELD_CHANGE_SINCE_CLOSE"},{"CLOSE", "CLOSING_YIELD"},{"COMPOUND", "COMPOUND_YIELD"},{"CURRENT", "CURRENT_YIELD"},{"GOVTEQUIV", "GVNT_EQUIVALENT_YIELD"},{"GROSS", "TRUE_GROSS_YIELD"},{"INFLATION", "YIELD_WITH_INFLATION_ASSUMPTION"},{"INVERSEFLOATER", "INVERSE_FLOATER_BOND_YIELD"},{"LASTCLOSE", "MOST_RECENT_CLOSING_YIELD"},{"LASTMONTH", "CLOSING_YIELD_MOST_RECENT_MONTH"},{"LASTQUARTER", "CLOSING_YIELD_MOST_RECENT_QUARTER"},{"LASTYEAR", "CLOSING_YIELD_MOST_RECENT_YEAR"},{"LONGAVGLIFE", "YIELD_TO_LONGEST_AVERAGE_LIFE"},{"MARK", "MARK_TO_MARKET_YIELD"},{"MATURITY", "YIELD_TO_MATURITY"},{"NEXTREFUND", "YIELD_TO_NEXT_REFUND"},{"OPENAVG", "OPEN_AVERAGE_YIELD"},{"PREVCLOSE", "PREVIOUS_CLOSE_YIELD"},{"PROCEEDS", "PROCEEDS_YIELD"},{"PUT", "YIELD_TO_NEXT_PUT"},{"SEMIANNUAL", "SEMI_ANNUAL_YIELD"},{"SHORTAVGLIFE", "YIELD_TO_SHORTEST_AVERAGE_LIFE"},{"SIMPLE", "SIMPLE_YIELD"},{"TAXEQUIV", "TAX_EQUIVALENT_YIELD"},{"TENDER", "YIELD_TO_TENDER_DATE"},{"TRUE", "TRUE_YIELD"},{"VALUE1_32", "YIELD_VALUE_OF_1_32"},{"WORST", "YIELD_TO_WORST"}]}
, "235" => #{"Name"=>"YieldType" ,"Type"=>"STRING" ,"ValidValues"=>[{"AFTERTAX", "AFTER_TAX_YIELD"},{"ANNUAL", "ANNUAL_YIELD"},{"ATISSUE", "YIELD_AT_ISSUE"},{"AVGMATURITY", "YIELD_TO_AVG_MATURITY"},{"BOOK", "BOOK_YIELD"},{"CALL", "YIELD_TO_NEXT_CALL"},{"CHANGE", "YIELD_CHANGE_SINCE_CLOSE"},{"CLOSE", "CLOSING_YIELD"},{"COMPOUND", "COMPOUND_YIELD"},{"CURRENT", "CURRENT_YIELD"},{"GOVTEQUIV", "GVNT_EQUIVALENT_YIELD"},{"GROSS", "TRUE_GROSS_YIELD"},{"INFLATION", "YIELD_WITH_INFLATION_ASSUMPTION"},{"INVERSEFLOATER", "INVERSE_FLOATER_BOND_YIELD"},{"LASTCLOSE", "MOST_RECENT_CLOSING_YIELD"},{"LASTMONTH", "CLOSING_YIELD_MOST_RECENT_MONTH"},{"LASTQUARTER", "CLOSING_YIELD_MOST_RECENT_QUARTER"},{"LASTYEAR", "CLOSING_YIELD_MOST_RECENT_YEAR"},{"LONGAVGLIFE", "YIELD_TO_LONGEST_AVERAGE_LIFE"},{"MARK", "MARK_TO_MARKET_YIELD"},{"MATURITY", "YIELD_TO_MATURITY"},{"NEXTREFUND", "YIELD_TO_NEXT_REFUND"},{"OPENAVG", "OPEN_AVERAGE_YIELD"},{"PREVCLOSE", "PREVIOUS_CLOSE_YIELD"},{"PROCEEDS", "PROCEEDS_YIELD"},{"PUT", "YIELD_TO_NEXT_PUT"},{"SEMIANNUAL", "SEMI_ANNUAL_YIELD"},{"SHORTAVGLIFE", "YIELD_TO_SHORTEST_AVERAGE_LIFE"},{"SIMPLE", "SIMPLE_YIELD"},{"TAXEQUIV", "TAX_EQUIVALENT_YIELD"},{"TENDER", "YIELD_TO_TENDER_DATE"},{"TRUE", "TRUE_YIELD"},{"VALUE1_32", "YIELD_VALUE_OF_1_32"},{"WORST", "YIELD_TO_WORST"}], "TagNum" => "235"}


,
"Yield" => #{"TagNum" => "236" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "236" => #{"Name"=>"Yield" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "236"}


,
"TotalTakedown" => #{"TagNum" => "237" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "237" => #{"Name"=>"TotalTakedown" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "237"}


,
"Concession" => #{"TagNum" => "238" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "238" => #{"Name"=>"Concession" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "238"}


,
"RepoCollateralSecurityType" => #{"TagNum" => "239" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "239" => #{"Name"=>"RepoCollateralSecurityType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "239"}


,
"RedemptionDate" => #{"TagNum" => "240" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "240" => #{"Name"=>"RedemptionDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "240"}


,
"UnderlyingCouponPaymentDate" => #{"TagNum" => "241" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "241" => #{"Name"=>"UnderlyingCouponPaymentDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "241"}


,
"UnderlyingIssueDate" => #{"TagNum" => "242" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "242" => #{"Name"=>"UnderlyingIssueDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "242"}


,
"UnderlyingRepoCollateralSecurityType" => #{"TagNum" => "243" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "243" => #{"Name"=>"UnderlyingRepoCollateralSecurityType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "243"}


,
"UnderlyingRepurchaseTerm" => #{"TagNum" => "244" ,"Type" => "INT" ,"ValidValues" =>[]}
, "244" => #{"Name"=>"UnderlyingRepurchaseTerm" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "244"}


,
"UnderlyingRepurchaseRate" => #{"TagNum" => "245" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "245" => #{"Name"=>"UnderlyingRepurchaseRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "245"}


,
"UnderlyingFactor" => #{"TagNum" => "246" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "246" => #{"Name"=>"UnderlyingFactor" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "246"}


,
"UnderlyingRedemptionDate" => #{"TagNum" => "247" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "247" => #{"Name"=>"UnderlyingRedemptionDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "247"}


,
"LegCouponPaymentDate" => #{"TagNum" => "248" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "248" => #{"Name"=>"LegCouponPaymentDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "248"}


,
"LegIssueDate" => #{"TagNum" => "249" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "249" => #{"Name"=>"LegIssueDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "249"}


,
"LegRepoCollateralSecurityType" => #{"TagNum" => "250" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "250" => #{"Name"=>"LegRepoCollateralSecurityType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "250"}


,
"LegRepurchaseTerm" => #{"TagNum" => "251" ,"Type" => "INT" ,"ValidValues" =>[]}
, "251" => #{"Name"=>"LegRepurchaseTerm" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "251"}


,
"LegRepurchaseRate" => #{"TagNum" => "252" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "252" => #{"Name"=>"LegRepurchaseRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "252"}


,
"LegFactor" => #{"TagNum" => "253" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "253" => #{"Name"=>"LegFactor" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "253"}


,
"LegRedemptionDate" => #{"TagNum" => "254" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "254" => #{"Name"=>"LegRedemptionDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "254"}


,
"CreditRating" => #{"TagNum" => "255" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "255" => #{"Name"=>"CreditRating" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "255"}


,
"UnderlyingCreditRating" => #{"TagNum" => "256" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "256" => #{"Name"=>"UnderlyingCreditRating" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "256"}


,
"LegCreditRating" => #{"TagNum" => "257" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "257" => #{"Name"=>"LegCreditRating" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "257"}


,
"TradedFlatSwitch" => #{"TagNum" => "258" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "258" => #{"Name"=>"TradedFlatSwitch" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "258"}


,
"BasisFeatureDate" => #{"TagNum" => "259" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "259" => #{"Name"=>"BasisFeatureDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "259"}


,
"BasisFeaturePrice" => #{"TagNum" => "260" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "260" => #{"Name"=>"BasisFeaturePrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "260"}


,
"MDReqID" => #{"TagNum" => "262" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "262" => #{"Name"=>"MDReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "262"}


,
"SubscriptionRequestType" => #{"TagNum" => "263" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "SNAPSHOT"},{"1", "SNAPSHOT_PLUS_UPDATES"},{"2", "DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST"}]}
, "263" => #{"Name"=>"SubscriptionRequestType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "SNAPSHOT"},{"1", "SNAPSHOT_PLUS_UPDATES"},{"2", "DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST"}], "TagNum" => "263"}


,
"MarketDepth" => #{"TagNum" => "264" ,"Type" => "INT" ,"ValidValues" =>[]}
, "264" => #{"Name"=>"MarketDepth" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "264"}


,
"MDUpdateType" => #{"TagNum" => "265" ,"Type" => "INT" ,"ValidValues" =>[{"0", "FULL_REFRESH"},{"1", "INCREMENTAL_REFRESH"}]}
, "265" => #{"Name"=>"MDUpdateType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "FULL_REFRESH"},{"1", "INCREMENTAL_REFRESH"}], "TagNum" => "265"}


,
"AggregatedBook" => #{"TagNum" => "266" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "266" => #{"Name"=>"AggregatedBook" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "266"}


,
"NoMDEntryTypes" => #{"TagNum" => "267" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "267" => #{"Name"=>"NoMDEntryTypes" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "267"}


,
"NoMDEntries" => #{"TagNum" => "268" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "268" => #{"Name"=>"NoMDEntries" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "268"}


,
"MDEntryType" => #{"TagNum" => "269" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "BID"},{"1", "OFFER"},{"2", "TRADE"},{"3", "INDEX_VALUE"},{"4", "OPENING_PRICE"},{"5", "CLOSING_PRICE"},{"6", "SETTLEMENT_PRICE"},{"7", "TRADING_SESSION_HIGH_PRICE"},{"8", "TRADING_SESSION_LOW_PRICE"},{"9", "TRADING_SESSION_VWAP_PRICE"},{"A", "IMBALANCE"},{"B", "TRADE_VOLUME"},{"C", "OPEN_INTEREST"},{"D", "COMPOSITE_UNDERLYING_PRICE"},{"E", "SIMULATED_SELL_PRICE"},{"F", "SIMULATED_BUY_PRICE"},{"G", "MARGIN_RATE"},{"H", "MID_PRICE"},{"J", "EMPTY_BOOK"},{"K", "SETTLE_HIGH_PRICE"},{"L", "SETTLE_LOW_PRICE"},{"M", "PRIOR_SETTLE_PRICE"},{"N", "SESSION_HIGH_BID"},{"O", "SESSION_LOW_OFFER"},{"P", "EARLY_PRICES"},{"Q", "AUCTION_CLEARING_PRICE"},{"S", "SWAP_VALUE_FACTOR"},{"R", "DAILY_VALUE_ADJUSTMENT_FOR_LONG_POSITIONS"},{"T", "CUMULATIVE_VALUE_ADJUSTMENT_FOR_LONG_POSITIONS"},{"U", "DAILY_VALUE_ADJUSTMENT_FOR_SHORT_POSITIONS"},{"V", "CUMULATIVE_VALUE_ADJUSTMENT_FOR_SHORT_POSITIONS"},{"Y", "RECOVERY_RATE"},{"Z", "RECOVERY_RATE_FOR_LONG"},{"a", "RECOVERY_RATE_FOR_SHORT"},{"W", "FIXING_PRICE"},{"X", "CASH_RATE"}]}
, "269" => #{"Name"=>"MDEntryType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "BID"},{"1", "OFFER"},{"2", "TRADE"},{"3", "INDEX_VALUE"},{"4", "OPENING_PRICE"},{"5", "CLOSING_PRICE"},{"6", "SETTLEMENT_PRICE"},{"7", "TRADING_SESSION_HIGH_PRICE"},{"8", "TRADING_SESSION_LOW_PRICE"},{"9", "TRADING_SESSION_VWAP_PRICE"},{"A", "IMBALANCE"},{"B", "TRADE_VOLUME"},{"C", "OPEN_INTEREST"},{"D", "COMPOSITE_UNDERLYING_PRICE"},{"E", "SIMULATED_SELL_PRICE"},{"F", "SIMULATED_BUY_PRICE"},{"G", "MARGIN_RATE"},{"H", "MID_PRICE"},{"J", "EMPTY_BOOK"},{"K", "SETTLE_HIGH_PRICE"},{"L", "SETTLE_LOW_PRICE"},{"M", "PRIOR_SETTLE_PRICE"},{"N", "SESSION_HIGH_BID"},{"O", "SESSION_LOW_OFFER"},{"P", "EARLY_PRICES"},{"Q", "AUCTION_CLEARING_PRICE"},{"S", "SWAP_VALUE_FACTOR"},{"R", "DAILY_VALUE_ADJUSTMENT_FOR_LONG_POSITIONS"},{"T", "CUMULATIVE_VALUE_ADJUSTMENT_FOR_LONG_POSITIONS"},{"U", "DAILY_VALUE_ADJUSTMENT_FOR_SHORT_POSITIONS"},{"V", "CUMULATIVE_VALUE_ADJUSTMENT_FOR_SHORT_POSITIONS"},{"Y", "RECOVERY_RATE"},{"Z", "RECOVERY_RATE_FOR_LONG"},{"a", "RECOVERY_RATE_FOR_SHORT"},{"W", "FIXING_PRICE"},{"X", "CASH_RATE"}], "TagNum" => "269"}


,
"MDEntryPx" => #{"TagNum" => "270" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "270" => #{"Name"=>"MDEntryPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "270"}


,
"MDEntrySize" => #{"TagNum" => "271" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "271" => #{"Name"=>"MDEntrySize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "271"}


,
"MDEntryDate" => #{"TagNum" => "272" ,"Type" => "UTCDATEONLY" ,"ValidValues" =>[]}
, "272" => #{"Name"=>"MDEntryDate" ,"Type"=>"UTCDATEONLY" ,"ValidValues"=>[], "TagNum" => "272"}


,
"MDEntryTime" => #{"TagNum" => "273" ,"Type" => "UTCTIMEONLY" ,"ValidValues" =>[]}
, "273" => #{"Name"=>"MDEntryTime" ,"Type"=>"UTCTIMEONLY" ,"ValidValues"=>[], "TagNum" => "273"}


,
"TickDirection" => #{"TagNum" => "274" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "PLUS_TICK"},{"1", "ZERO_PLUS_TICK"},{"2", "MINUS_TICK"},{"3", "ZERO_MINUS_TICK"}]}
, "274" => #{"Name"=>"TickDirection" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "PLUS_TICK"},{"1", "ZERO_PLUS_TICK"},{"2", "MINUS_TICK"},{"3", "ZERO_MINUS_TICK"}], "TagNum" => "274"}


,
"MDMkt" => #{"TagNum" => "275" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "275" => #{"Name"=>"MDMkt" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "275"}


,
"QuoteCondition" => #{"TagNum" => "276" ,"Type" => "MULTIPLESTRINGVALUE" ,"ValidValues" =>[{"A", "OPEN_ACTIVE"},{"B", "CLOSED_INACTIVE"},{"C", "EXCHANGE_BEST"},{"D", "CONSOLIDATED_BEST"},{"E", "LOCKED"},{"F", "CROSSED"},{"G", "DEPTH"},{"H", "FAST_TRADING"},{"I", "NON_FIRM"},{"L", "MANUAL_SLOW_QUOTE"},{"J", "OUTRIGHT_PRICE"},{"K", "IMPLIED_PRICE"},{"M", "DEPTH_ON_OFFER"},{"N", "DEPTH_ON_BID"},{"O", "CLOSING"},{"P", "NEWS_DISSEMINATION"},{"Q", "TRADING_RANGE"},{"R", "ORDER_INFLUX"},{"S", "DUE_TO_RELATED"},{"T", "NEWS_PENDING"},{"U", "ADDITIONAL_INFO"},{"V", "ADDITIONAL_INFO_DUE_TO_RELATED"},{"W", "RESUME"},{"X", "VIEW_OF_COMMON"},{"Y", "VOLUME_ALERT"},{"Z", "ORDER_IMBALANCE"},{"a", "EQUIPMENT_CHANGEOVER"},{"b", "NO_OPEN"},{"c", "REGULAR_ETH"},{"d", "AUTOMATIC_EXECUTION"},{"e", "AUTOMATIC_EXECUTION_ETH"},{"f ", "FAST_MARKET_ETH"},{"g", "INACTIVE_ETH"},{"h", "ROTATION"},{"i", "ROTATION_ETH"},{"j", "HALT"},{"k", "HALT_ETH"},{"l", "DUE_TO_NEWS_DISSEMINATION"},{"m", "DUE_TO_NEWS_PENDING"},{"n", "TRADING_RESUME"},{"o", "OUT_OF_SEQUENCE"},{"p", "BID_SPECIALIST"},{"q", "OFFER_SPECIALIST"},{"r", "BID_OFFER_SPECIALIST"},{"s", "END_OF_DAY_SAM"},{"t", "FORBIDDEN_SAM"},{"u", "FROZEN_SAM"},{"v", "PREOPENING_SAM"},{"w", "OPENING_SAM"},{"x", "OPEN_SAM"},{"y", "SURVEILLANCE_SAM"},{"z", "SUSPENDED_SAM"},{"0", "RESERVED_SAM"},{"1", "NO_ACTIVE_SAM"},{"2", "RESTRICTED"},{"3", "REST_OF_BOOK_VWAP"},{"4", "BETTER_PRICES_IN_CONDITIONAL_ORDERS"},{"5", "MEDIAN_PRICE"},{"6", "FULL_CURVE"},{"7", "FLAT_CURVE"}]}
, "276" => #{"Name"=>"QuoteCondition" ,"Type"=>"MULTIPLESTRINGVALUE" ,"ValidValues"=>[{"A", "OPEN_ACTIVE"},{"B", "CLOSED_INACTIVE"},{"C", "EXCHANGE_BEST"},{"D", "CONSOLIDATED_BEST"},{"E", "LOCKED"},{"F", "CROSSED"},{"G", "DEPTH"},{"H", "FAST_TRADING"},{"I", "NON_FIRM"},{"L", "MANUAL_SLOW_QUOTE"},{"J", "OUTRIGHT_PRICE"},{"K", "IMPLIED_PRICE"},{"M", "DEPTH_ON_OFFER"},{"N", "DEPTH_ON_BID"},{"O", "CLOSING"},{"P", "NEWS_DISSEMINATION"},{"Q", "TRADING_RANGE"},{"R", "ORDER_INFLUX"},{"S", "DUE_TO_RELATED"},{"T", "NEWS_PENDING"},{"U", "ADDITIONAL_INFO"},{"V", "ADDITIONAL_INFO_DUE_TO_RELATED"},{"W", "RESUME"},{"X", "VIEW_OF_COMMON"},{"Y", "VOLUME_ALERT"},{"Z", "ORDER_IMBALANCE"},{"a", "EQUIPMENT_CHANGEOVER"},{"b", "NO_OPEN"},{"c", "REGULAR_ETH"},{"d", "AUTOMATIC_EXECUTION"},{"e", "AUTOMATIC_EXECUTION_ETH"},{"f ", "FAST_MARKET_ETH"},{"g", "INACTIVE_ETH"},{"h", "ROTATION"},{"i", "ROTATION_ETH"},{"j", "HALT"},{"k", "HALT_ETH"},{"l", "DUE_TO_NEWS_DISSEMINATION"},{"m", "DUE_TO_NEWS_PENDING"},{"n", "TRADING_RESUME"},{"o", "OUT_OF_SEQUENCE"},{"p", "BID_SPECIALIST"},{"q", "OFFER_SPECIALIST"},{"r", "BID_OFFER_SPECIALIST"},{"s", "END_OF_DAY_SAM"},{"t", "FORBIDDEN_SAM"},{"u", "FROZEN_SAM"},{"v", "PREOPENING_SAM"},{"w", "OPENING_SAM"},{"x", "OPEN_SAM"},{"y", "SURVEILLANCE_SAM"},{"z", "SUSPENDED_SAM"},{"0", "RESERVED_SAM"},{"1", "NO_ACTIVE_SAM"},{"2", "RESTRICTED"},{"3", "REST_OF_BOOK_VWAP"},{"4", "BETTER_PRICES_IN_CONDITIONAL_ORDERS"},{"5", "MEDIAN_PRICE"},{"6", "FULL_CURVE"},{"7", "FLAT_CURVE"}], "TagNum" => "276"}


,
"TradeCondition" => #{"TagNum" => "277" ,"Type" => "MULTIPLESTRINGVALUE" ,"ValidValues" =>[{"A", "CASH"},{"B", "AVERAGE_PRICE_TRADE"},{"C", "CASH_TRADE"},{"D", "NEXT_DAY"},{"E", "OPENING_REOPENING_TRADE_DETAIL"},{"F", "INTRADAY_TRADE_DETAIL"},{"G", "RULE_127_TRADE"},{"H", "RULE_155_TRADE"},{"I", "SOLD_LAST"},{"J", "NEXT_DAY_TRADE"},{"K", "OPENED"},{"L", "SELLER"},{"M", "SOLD"},{"N", "STOPPED_STOCK"},{"P", "IMBALANCE_MORE_BUYERS"},{"Q", "IMBALANCE_MORE_SELLERS"},{"R", "OPENING_PRICE"},{"S", "BARGAIN_CONDITION"},{"T", "CONVERTED_PRICE_INDICATOR"},{"U", "EXCHANGE_LAST"},{"V", "FINAL_PRICE_OF_SESSION"},{"W", "EX_PIT"},{"X", "CROSSED_X"},{"Y", "TRADES_RESULTING_FROM_MANUAL_SLOW_QUOTE"},{"Z", "TRADES_RESULTING_FROM_INTERMARKET_SWEEP"},{"a", "VOLUME_ONLY"},{"b", "DIRECT_PLUS"},{"c", "ACQUISITION"},{"d", "BUNCHED"},{"e", "DISTRIBUTION"},{"f", "BUNCHED_SALE"},{"g", "SPLIT_TRADE"},{"h", "CANCEL_STOPPED"},{"i", "CANCEL_ETH"},{"j", "CANCEL_STOPPED_ETH"},{"k", "OUT_OF_SEQUENCE_ETH"},{"l", "CANCEL_LAST_ETH"},{"m", "SOLD_LAST_SALE_ETH"},{"n", "CANCEL_LAST"},{"o", "SOLD_LAST_SALE"},{"p", "CANCEL_OPEN"},{"q", "CANCEL_OPEN_ETH"},{"r", "OPENED_SALE_ETH"},{"s", "CANCEL_ONLY"},{"t", "CANCEL_ONLY_ETH"},{"u", "LATE_OPEN_ETH"},{"v", "AUTO_EXECUTION_ETH"},{"w", "REOPEN"},{"x", "REOPEN_ETH"},{"y", "ADJUSTED"},{"z", "ADJUSTED_ETH"},{"AA", "SPREAD"},{"AB", "SPREAD_ETH"},{"AC", "STRADDLE"},{"AD", "STRADDLE_ETH"},{"AE", "STOPPED"},{"AF", "STOPPED_ETH"},{"AG", "REGULAR_ETH"},{"AH", "COMBO"},{"AI", "COMBO_ETH"},{"AJ", "OFFICIAL_CLOSING_PRICE"},{"AK", "PRIOR_REFERENCE_PRICE"},{"0", "CANCEL"},{"AL", "STOPPED_SOLD_LAST"},{"AM", "STOPPED_OUT_OF_SEQUENCE"},{"AN", "OFFICAL_CLOSING_PRICE"},{"AO", "CROSSED_AO"},{"AP", "FAST_MARKET"},{"AQ", "AUTOMATIC_EXECUTION"},{"AR", "FORM_T"},{"AS", "BASKET_INDEX"},{"AT", "BURST_BASKET"},{"AV", "OUTSIDE_SPREAD"},{"1", "IMPLIED_TRADE"},{"2", "MARKETPLACE_ENTERED_TRADE"},{"3", "MULT_ASSET_CLASS_MULTILEG_TRADE"},{"4", "MULTILEG_TO_MULTILEG_TRADE"}]}
, "277" => #{"Name"=>"TradeCondition" ,"Type"=>"MULTIPLESTRINGVALUE" ,"ValidValues"=>[{"A", "CASH"},{"B", "AVERAGE_PRICE_TRADE"},{"C", "CASH_TRADE"},{"D", "NEXT_DAY"},{"E", "OPENING_REOPENING_TRADE_DETAIL"},{"F", "INTRADAY_TRADE_DETAIL"},{"G", "RULE_127_TRADE"},{"H", "RULE_155_TRADE"},{"I", "SOLD_LAST"},{"J", "NEXT_DAY_TRADE"},{"K", "OPENED"},{"L", "SELLER"},{"M", "SOLD"},{"N", "STOPPED_STOCK"},{"P", "IMBALANCE_MORE_BUYERS"},{"Q", "IMBALANCE_MORE_SELLERS"},{"R", "OPENING_PRICE"},{"S", "BARGAIN_CONDITION"},{"T", "CONVERTED_PRICE_INDICATOR"},{"U", "EXCHANGE_LAST"},{"V", "FINAL_PRICE_OF_SESSION"},{"W", "EX_PIT"},{"X", "CROSSED_X"},{"Y", "TRADES_RESULTING_FROM_MANUAL_SLOW_QUOTE"},{"Z", "TRADES_RESULTING_FROM_INTERMARKET_SWEEP"},{"a", "VOLUME_ONLY"},{"b", "DIRECT_PLUS"},{"c", "ACQUISITION"},{"d", "BUNCHED"},{"e", "DISTRIBUTION"},{"f", "BUNCHED_SALE"},{"g", "SPLIT_TRADE"},{"h", "CANCEL_STOPPED"},{"i", "CANCEL_ETH"},{"j", "CANCEL_STOPPED_ETH"},{"k", "OUT_OF_SEQUENCE_ETH"},{"l", "CANCEL_LAST_ETH"},{"m", "SOLD_LAST_SALE_ETH"},{"n", "CANCEL_LAST"},{"o", "SOLD_LAST_SALE"},{"p", "CANCEL_OPEN"},{"q", "CANCEL_OPEN_ETH"},{"r", "OPENED_SALE_ETH"},{"s", "CANCEL_ONLY"},{"t", "CANCEL_ONLY_ETH"},{"u", "LATE_OPEN_ETH"},{"v", "AUTO_EXECUTION_ETH"},{"w", "REOPEN"},{"x", "REOPEN_ETH"},{"y", "ADJUSTED"},{"z", "ADJUSTED_ETH"},{"AA", "SPREAD"},{"AB", "SPREAD_ETH"},{"AC", "STRADDLE"},{"AD", "STRADDLE_ETH"},{"AE", "STOPPED"},{"AF", "STOPPED_ETH"},{"AG", "REGULAR_ETH"},{"AH", "COMBO"},{"AI", "COMBO_ETH"},{"AJ", "OFFICIAL_CLOSING_PRICE"},{"AK", "PRIOR_REFERENCE_PRICE"},{"0", "CANCEL"},{"AL", "STOPPED_SOLD_LAST"},{"AM", "STOPPED_OUT_OF_SEQUENCE"},{"AN", "OFFICAL_CLOSING_PRICE"},{"AO", "CROSSED_AO"},{"AP", "FAST_MARKET"},{"AQ", "AUTOMATIC_EXECUTION"},{"AR", "FORM_T"},{"AS", "BASKET_INDEX"},{"AT", "BURST_BASKET"},{"AV", "OUTSIDE_SPREAD"},{"1", "IMPLIED_TRADE"},{"2", "MARKETPLACE_ENTERED_TRADE"},{"3", "MULT_ASSET_CLASS_MULTILEG_TRADE"},{"4", "MULTILEG_TO_MULTILEG_TRADE"}], "TagNum" => "277"}


,
"MDEntryID" => #{"TagNum" => "278" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "278" => #{"Name"=>"MDEntryID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "278"}


,
"MDUpdateAction" => #{"TagNum" => "279" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"1", "CHANGE"},{"2", "DELETE"},{"3", "DELETE_THRU"},{"4", "DELETE_FROM"},{"5", "OVERLAY"}]}
, "279" => #{"Name"=>"MDUpdateAction" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"1", "CHANGE"},{"2", "DELETE"},{"3", "DELETE_THRU"},{"4", "DELETE_FROM"},{"5", "OVERLAY"}], "TagNum" => "279"}


,
"MDEntryRefID" => #{"TagNum" => "280" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "280" => #{"Name"=>"MDEntryRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "280"}


,
"MDReqRejReason" => #{"TagNum" => "281" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "UNKNOWN_SYMBOL"},{"1", "DUPLICATE_MDREQID"},{"2", "INSUFFICIENT_BANDWIDTH"},{"3", "INSUFFICIENT_PERMISSIONS"},{"4", "UNSUPPORTED_SUBSCRIPTIONREQUESTTYPE"},{"5", "UNSUPPORTED_MARKETDEPTH"},{"6", "UNSUPPORTED_MDUPDATETYPE"},{"7", "UNSUPPORTED_AGGREGATEDBOOK"},{"8", "UNSUPPORTED_MDENTRYTYPE"},{"9", "UNSUPPORTED_TRADINGSESSIONID"},{"A", "UNSUPPORTED_SCOPE"},{"B", "UNSUPPORTED_OPENCLOSESETTLEFLAG"},{"C", "UNSUPPORTED_MDIMPLICITDELETE"},{"D", "INSUFFICIENT_CREDIT"}]}
, "281" => #{"Name"=>"MDReqRejReason" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "UNKNOWN_SYMBOL"},{"1", "DUPLICATE_MDREQID"},{"2", "INSUFFICIENT_BANDWIDTH"},{"3", "INSUFFICIENT_PERMISSIONS"},{"4", "UNSUPPORTED_SUBSCRIPTIONREQUESTTYPE"},{"5", "UNSUPPORTED_MARKETDEPTH"},{"6", "UNSUPPORTED_MDUPDATETYPE"},{"7", "UNSUPPORTED_AGGREGATEDBOOK"},{"8", "UNSUPPORTED_MDENTRYTYPE"},{"9", "UNSUPPORTED_TRADINGSESSIONID"},{"A", "UNSUPPORTED_SCOPE"},{"B", "UNSUPPORTED_OPENCLOSESETTLEFLAG"},{"C", "UNSUPPORTED_MDIMPLICITDELETE"},{"D", "INSUFFICIENT_CREDIT"}], "TagNum" => "281"}


,
"MDEntryOriginator" => #{"TagNum" => "282" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "282" => #{"Name"=>"MDEntryOriginator" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "282"}


,
"LocationID" => #{"TagNum" => "283" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "283" => #{"Name"=>"LocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "283"}


,
"DeskID" => #{"TagNum" => "284" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "284" => #{"Name"=>"DeskID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "284"}


,
"DeleteReason" => #{"TagNum" => "285" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "CANCELLATION"},{"1", "ERROR"}]}
, "285" => #{"Name"=>"DeleteReason" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "CANCELLATION"},{"1", "ERROR"}], "TagNum" => "285"}


,
"OpenCloseSettlFlag" => #{"TagNum" => "286" ,"Type" => "MULTIPLECHARVALUE" ,"ValidValues" =>[{"0", "DAILY_OPEN"},{"1", "SESSION_OPEN"},{"2", "DELIVERY_SETTLEMENT_ENTRY"},{"3", "EXPECTED_ENTRY"},{"4", "ENTRY_FROM_PREVIOUS_BUSINESS_DAY"},{"5", "THEORETICAL_PRICE_VALUE"}]}
, "286" => #{"Name"=>"OpenCloseSettlFlag" ,"Type"=>"MULTIPLECHARVALUE" ,"ValidValues"=>[{"0", "DAILY_OPEN"},{"1", "SESSION_OPEN"},{"2", "DELIVERY_SETTLEMENT_ENTRY"},{"3", "EXPECTED_ENTRY"},{"4", "ENTRY_FROM_PREVIOUS_BUSINESS_DAY"},{"5", "THEORETICAL_PRICE_VALUE"}], "TagNum" => "286"}


,
"SellerDays" => #{"TagNum" => "287" ,"Type" => "INT" ,"ValidValues" =>[]}
, "287" => #{"Name"=>"SellerDays" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "287"}


,
"MDEntryBuyer" => #{"TagNum" => "288" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "288" => #{"Name"=>"MDEntryBuyer" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "288"}


,
"MDEntrySeller" => #{"TagNum" => "289" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "289" => #{"Name"=>"MDEntrySeller" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "289"}


,
"MDEntryPositionNo" => #{"TagNum" => "290" ,"Type" => "INT" ,"ValidValues" =>[]}
, "290" => #{"Name"=>"MDEntryPositionNo" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "290"}


,
"FinancialStatus" => #{"TagNum" => "291" ,"Type" => "MULTIPLECHARVALUE" ,"ValidValues" =>[{"1", "BANKRUPT"},{"2", "PENDING_DELISTING"},{"3", "RESTRICTED"}]}
, "291" => #{"Name"=>"FinancialStatus" ,"Type"=>"MULTIPLECHARVALUE" ,"ValidValues"=>[{"1", "BANKRUPT"},{"2", "PENDING_DELISTING"},{"3", "RESTRICTED"}], "TagNum" => "291"}


,
"CorporateAction" => #{"TagNum" => "292" ,"Type" => "MULTIPLECHARVALUE" ,"ValidValues" =>[{"A", "EX_DIVIDEND"},{"B", "EX_DISTRIBUTION"},{"C", "EX_RIGHTS"},{"D", "NEW"},{"E", "EX_INTEREST"},{"F", "CASH_DIVIDEND"},{"G", "STOCK_DIVIDEND"},{"H", "NON_INTEGER_STOCK_SPLIT"},{"I", "REVERSE_STOCK_SPLIT"},{"J", "STANDARD_INTEGER_STOCK_SPLIT"},{"K", "POSITION_CONSOLIDATION"},{"L", "LIQUIDATION_REORGANIZATION"},{"M", "MERGER_REORGANIZATION"},{"N", "RIGHTS_OFFERING"},{"O", "SHAREHOLDER_MEETING"},{"P", "SPINOFF"},{"Q", "TENDER_OFFER"},{"R", "WARRANT"},{"S", "SPECIAL_ACTION"},{"T", "SYMBOL_CONVERSION"},{"U", "CUSIP"},{"V", "LEAP_ROLLOVER"},{"W", "SUCCESSION_EVENT"}]}
, "292" => #{"Name"=>"CorporateAction" ,"Type"=>"MULTIPLECHARVALUE" ,"ValidValues"=>[{"A", "EX_DIVIDEND"},{"B", "EX_DISTRIBUTION"},{"C", "EX_RIGHTS"},{"D", "NEW"},{"E", "EX_INTEREST"},{"F", "CASH_DIVIDEND"},{"G", "STOCK_DIVIDEND"},{"H", "NON_INTEGER_STOCK_SPLIT"},{"I", "REVERSE_STOCK_SPLIT"},{"J", "STANDARD_INTEGER_STOCK_SPLIT"},{"K", "POSITION_CONSOLIDATION"},{"L", "LIQUIDATION_REORGANIZATION"},{"M", "MERGER_REORGANIZATION"},{"N", "RIGHTS_OFFERING"},{"O", "SHAREHOLDER_MEETING"},{"P", "SPINOFF"},{"Q", "TENDER_OFFER"},{"R", "WARRANT"},{"S", "SPECIAL_ACTION"},{"T", "SYMBOL_CONVERSION"},{"U", "CUSIP"},{"V", "LEAP_ROLLOVER"},{"W", "SUCCESSION_EVENT"}], "TagNum" => "292"}


,
"DefBidSize" => #{"TagNum" => "293" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "293" => #{"Name"=>"DefBidSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "293"}


,
"DefOfferSize" => #{"TagNum" => "294" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "294" => #{"Name"=>"DefOfferSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "294"}


,
"NoQuoteEntries" => #{"TagNum" => "295" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "295" => #{"Name"=>"NoQuoteEntries" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "295"}


,
"NoQuoteSets" => #{"TagNum" => "296" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "296" => #{"Name"=>"NoQuoteSets" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "296"}


,
"QuoteStatus" => #{"TagNum" => "297" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ACCEPTED"},{"1", "CANCEL_FOR_SYMBOL"},{"2", "CANCELED_FOR_SECURITY_TYPE"},{"3", "CANCELED_FOR_UNDERLYING"},{"4", "CANCELED_ALL"},{"5", "REJECTED"},{"6", "REMOVED_FROM_MARKET"},{"7", "EXPIRED"},{"8", "QUERY"},{"9", "QUOTE_NOT_FOUND"},{"10", "PENDING"},{"11", "PASS"},{"12", "LOCKED_MARKET_WARNING"},{"13", "CROSS_MARKET_WARNING"},{"14", "CANCELED_DUE_TO_LOCK_MARKET"},{"15", "CANCELED_DUE_TO_CROSS_MARKET"},{"16", "ACTIVE"},{"17", "CANCELED"},{"18", "UNSOLICITED_QUOTE_REPLENISHMENT"},{"19", "PENDING_END_TRADE"},{"20", "TOO_LATE_TO_END"}]}
, "297" => #{"Name"=>"QuoteStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ACCEPTED"},{"1", "CANCEL_FOR_SYMBOL"},{"2", "CANCELED_FOR_SECURITY_TYPE"},{"3", "CANCELED_FOR_UNDERLYING"},{"4", "CANCELED_ALL"},{"5", "REJECTED"},{"6", "REMOVED_FROM_MARKET"},{"7", "EXPIRED"},{"8", "QUERY"},{"9", "QUOTE_NOT_FOUND"},{"10", "PENDING"},{"11", "PASS"},{"12", "LOCKED_MARKET_WARNING"},{"13", "CROSS_MARKET_WARNING"},{"14", "CANCELED_DUE_TO_LOCK_MARKET"},{"15", "CANCELED_DUE_TO_CROSS_MARKET"},{"16", "ACTIVE"},{"17", "CANCELED"},{"18", "UNSOLICITED_QUOTE_REPLENISHMENT"},{"19", "PENDING_END_TRADE"},{"20", "TOO_LATE_TO_END"}], "TagNum" => "297"}


,
"QuoteCancelType" => #{"TagNum" => "298" ,"Type" => "INT" ,"ValidValues" =>[{"1", "CANCEL_FOR_ONE_OR_MORE_SECURITIES"},{"2", "CANCEL_FOR_SECURITY_TYPE"},{"3", "CANCEL_FOR_UNDERLYING_SECURITY"},{"4", "CANCEL_ALL_QUOTES"},{"5", "CANCEL_QUOTE_SPECIFIED_IN_QUOTEID"},{"6", "CANCEL_BY_QUOTETYPE"},{"7", "CANCEL_FOR_SECURITY_ISSUER"},{"8", "CANCEL_FOR_ISSUER_OF_UNDERLYING_SECURITY"}]}
, "298" => #{"Name"=>"QuoteCancelType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "CANCEL_FOR_ONE_OR_MORE_SECURITIES"},{"2", "CANCEL_FOR_SECURITY_TYPE"},{"3", "CANCEL_FOR_UNDERLYING_SECURITY"},{"4", "CANCEL_ALL_QUOTES"},{"5", "CANCEL_QUOTE_SPECIFIED_IN_QUOTEID"},{"6", "CANCEL_BY_QUOTETYPE"},{"7", "CANCEL_FOR_SECURITY_ISSUER"},{"8", "CANCEL_FOR_ISSUER_OF_UNDERLYING_SECURITY"}], "TagNum" => "298"}


,
"QuoteEntryID" => #{"TagNum" => "299" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "299" => #{"Name"=>"QuoteEntryID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "299"}


,
"QuoteRejectReason" => #{"TagNum" => "300" ,"Type" => "INT" ,"ValidValues" =>[{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE"},{"3", "QUOTE_REQUEST_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "UNKNOWN_QUOTE"},{"6", "DUPLICATE_QUOTE"},{"7", "INVALID_BID_ASK_SPREAD"},{"8", "INVALID_PRICE"},{"9", "NOT_AUTHORIZED_TO_QUOTE_SECURITY"},{"10", "PRICE_EXCEEDS_CURRENT_PRICE_BAND"},{"11", "QUOTE_LOCKED"},{"99", "OTHER"},{"12", "INVALID_OR_UNKNOWN_SECURITY_ISSUER"},{"13", "INVALID_OR_UNKNOWN_ISSUER_OF_UNDERLYING_SECURITY"}]}
, "300" => #{"Name"=>"QuoteRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE"},{"3", "QUOTE_REQUEST_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "UNKNOWN_QUOTE"},{"6", "DUPLICATE_QUOTE"},{"7", "INVALID_BID_ASK_SPREAD"},{"8", "INVALID_PRICE"},{"9", "NOT_AUTHORIZED_TO_QUOTE_SECURITY"},{"10", "PRICE_EXCEEDS_CURRENT_PRICE_BAND"},{"11", "QUOTE_LOCKED"},{"99", "OTHER"},{"12", "INVALID_OR_UNKNOWN_SECURITY_ISSUER"},{"13", "INVALID_OR_UNKNOWN_ISSUER_OF_UNDERLYING_SECURITY"}], "TagNum" => "300"}


,
"QuoteResponseLevel" => #{"TagNum" => "301" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NO_ACKNOWLEDGEMENT"},{"1", "ACKNOWLEDGE_ONLY_NEGATIVE_OR_ERRONEOUS_QUOTES"},{"2", "ACKNOWLEDGE_EACH_QUOTE_MESSAGE"},{"3", "SUMMARY_ACKNOWLEDGEMENT"}]}
, "301" => #{"Name"=>"QuoteResponseLevel" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NO_ACKNOWLEDGEMENT"},{"1", "ACKNOWLEDGE_ONLY_NEGATIVE_OR_ERRONEOUS_QUOTES"},{"2", "ACKNOWLEDGE_EACH_QUOTE_MESSAGE"},{"3", "SUMMARY_ACKNOWLEDGEMENT"}], "TagNum" => "301"}


,
"QuoteSetID" => #{"TagNum" => "302" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "302" => #{"Name"=>"QuoteSetID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "302"}


,
"QuoteRequestType" => #{"TagNum" => "303" ,"Type" => "INT" ,"ValidValues" =>[{"1", "MANUAL"},{"2", "AUTOMATIC"}]}
, "303" => #{"Name"=>"QuoteRequestType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "MANUAL"},{"2", "AUTOMATIC"}], "TagNum" => "303"}


,
"TotNoQuoteEntries" => #{"TagNum" => "304" ,"Type" => "INT" ,"ValidValues" =>[]}
, "304" => #{"Name"=>"TotNoQuoteEntries" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "304"}


,
"UnderlyingSecurityIDSource" => #{"TagNum" => "305" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "305" => #{"Name"=>"UnderlyingSecurityIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "305"}


,
"UnderlyingIssuer" => #{"TagNum" => "306" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "306" => #{"Name"=>"UnderlyingIssuer" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "306"}


,
"UnderlyingSecurityDesc" => #{"TagNum" => "307" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "307" => #{"Name"=>"UnderlyingSecurityDesc" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "307"}


,
"UnderlyingSecurityExchange" => #{"TagNum" => "308" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "308" => #{"Name"=>"UnderlyingSecurityExchange" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "308"}


,
"UnderlyingSecurityID" => #{"TagNum" => "309" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "309" => #{"Name"=>"UnderlyingSecurityID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "309"}


,
"UnderlyingSecurityType" => #{"TagNum" => "310" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "310" => #{"Name"=>"UnderlyingSecurityType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "310"}


,
"UnderlyingSymbol" => #{"TagNum" => "311" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "311" => #{"Name"=>"UnderlyingSymbol" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "311"}


,
"UnderlyingSymbolSfx" => #{"TagNum" => "312" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "312" => #{"Name"=>"UnderlyingSymbolSfx" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "312"}


,
"UnderlyingMaturityMonthYear" => #{"TagNum" => "313" ,"Type" => "MONTHYEAR" ,"ValidValues" =>[]}
, "313" => #{"Name"=>"UnderlyingMaturityMonthYear" ,"Type"=>"MONTHYEAR" ,"ValidValues"=>[], "TagNum" => "313"}


,
"UnderlyingPutOrCall" => #{"TagNum" => "315" ,"Type" => "INT" ,"ValidValues" =>[]}
, "315" => #{"Name"=>"UnderlyingPutOrCall" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "315"}


,
"UnderlyingStrikePrice" => #{"TagNum" => "316" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "316" => #{"Name"=>"UnderlyingStrikePrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "316"}


,
"UnderlyingOptAttribute" => #{"TagNum" => "317" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "317" => #{"Name"=>"UnderlyingOptAttribute" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "317"}


,
"UnderlyingCurrency" => #{"TagNum" => "318" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "318" => #{"Name"=>"UnderlyingCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "318"}


,
"SecurityReqID" => #{"TagNum" => "320" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "320" => #{"Name"=>"SecurityReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "320"}


,
"SecurityRequestType" => #{"TagNum" => "321" ,"Type" => "INT" ,"ValidValues" =>[{"0", "REQUEST_SECURITY_IDENTITY_AND_SPECIFICATIONS"},{"1", "REQUEST_SECURITY_IDENTITY_FOR_THE_SPECIFICATIONS_PROVIDED"},{"2", "REQUEST_LIST_SECURITY_TYPES"},{"3", "REQUEST_LIST_SECURITIES"},{"4", "SYMBOL"},{"5", "SECURITYTYPE_AND_OR_CFICODE"},{"6", "PRODUCT"},{"7", "TRADINGSESSIONID"},{"8", "ALL_SECURITIES"},{"9", "MARKETID_OR_MARKETID_PLUS_MARKETSEGMENTID"}]}
, "321" => #{"Name"=>"SecurityRequestType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "REQUEST_SECURITY_IDENTITY_AND_SPECIFICATIONS"},{"1", "REQUEST_SECURITY_IDENTITY_FOR_THE_SPECIFICATIONS_PROVIDED"},{"2", "REQUEST_LIST_SECURITY_TYPES"},{"3", "REQUEST_LIST_SECURITIES"},{"4", "SYMBOL"},{"5", "SECURITYTYPE_AND_OR_CFICODE"},{"6", "PRODUCT"},{"7", "TRADINGSESSIONID"},{"8", "ALL_SECURITIES"},{"9", "MARKETID_OR_MARKETID_PLUS_MARKETSEGMENTID"}], "TagNum" => "321"}


,
"SecurityResponseID" => #{"TagNum" => "322" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "322" => #{"Name"=>"SecurityResponseID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "322"}


,
"SecurityResponseType" => #{"TagNum" => "323" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ACCEPT_SECURITY_PROPOSAL_AS_IS"},{"2", "ACCEPT_SECURITY_PROPOSAL_WITH_REVISIONS_AS_INDICATED_IN_THE_MESSAGE"},{"3", "LIST_OF_SECURITY_TYPES_RETURNED_PER_REQUEST"},{"4", "LIST_OF_SECURITIES_RETURNED_PER_REQUEST"},{"5", "REJECT_SECURITY_PROPOSAL"},{"6", "CANNOT_MATCH_SELECTION_CRITERIA"}]}
, "323" => #{"Name"=>"SecurityResponseType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ACCEPT_SECURITY_PROPOSAL_AS_IS"},{"2", "ACCEPT_SECURITY_PROPOSAL_WITH_REVISIONS_AS_INDICATED_IN_THE_MESSAGE"},{"3", "LIST_OF_SECURITY_TYPES_RETURNED_PER_REQUEST"},{"4", "LIST_OF_SECURITIES_RETURNED_PER_REQUEST"},{"5", "REJECT_SECURITY_PROPOSAL"},{"6", "CANNOT_MATCH_SELECTION_CRITERIA"}], "TagNum" => "323"}


,
"SecurityStatusReqID" => #{"TagNum" => "324" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "324" => #{"Name"=>"SecurityStatusReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "324"}


,
"UnsolicitedIndicator" => #{"TagNum" => "325" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "325" => #{"Name"=>"UnsolicitedIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "325"}


,
"SecurityTradingStatus" => #{"TagNum" => "326" ,"Type" => "INT" ,"ValidValues" =>[{"1", "OPENING_DELAY"},{"2", "TRADING_HALT"},{"3", "RESUME"},{"4", "NO_OPEN"},{"5", "PRICE_INDICATION"},{"6", "TRADING_RANGE_INDICATION"},{"7", "MARKET_IMBALANCE_BUY"},{"8", "MARKET_IMBALANCE_SELL"},{"9", "MARKET_ON_CLOSE_IMBALANCE_BUY"},{"10", "MARKET_ON_CLOSE_IMBALANCE_SELL"},{"12", "NO_MARKET_IMBALANCE"},{"13", "NO_MARKET_ON_CLOSE_IMBALANCE"},{"14", "ITS_PRE_OPENING"},{"15", "NEW_PRICE_INDICATION"},{"16", "TRADE_DISSEMINATION_TIME"},{"17", "READY_TO_TRADE"},{"18", "NOT_AVAILABLE_FOR_TRADING"},{"19", "NOT_TRADED_ON_THIS_MARKET"},{"20", "UNKNOWN_OR_INVALID"},{"21", "PRE_OPEN"},{"22", "OPENING_ROTATION"},{"23", "FAST_MARKET"},{"24", "PRE_CROSS"},{"25", "CROSS"},{"26", "POST_CLOSE"}]}
, "326" => #{"Name"=>"SecurityTradingStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "OPENING_DELAY"},{"2", "TRADING_HALT"},{"3", "RESUME"},{"4", "NO_OPEN"},{"5", "PRICE_INDICATION"},{"6", "TRADING_RANGE_INDICATION"},{"7", "MARKET_IMBALANCE_BUY"},{"8", "MARKET_IMBALANCE_SELL"},{"9", "MARKET_ON_CLOSE_IMBALANCE_BUY"},{"10", "MARKET_ON_CLOSE_IMBALANCE_SELL"},{"12", "NO_MARKET_IMBALANCE"},{"13", "NO_MARKET_ON_CLOSE_IMBALANCE"},{"14", "ITS_PRE_OPENING"},{"15", "NEW_PRICE_INDICATION"},{"16", "TRADE_DISSEMINATION_TIME"},{"17", "READY_TO_TRADE"},{"18", "NOT_AVAILABLE_FOR_TRADING"},{"19", "NOT_TRADED_ON_THIS_MARKET"},{"20", "UNKNOWN_OR_INVALID"},{"21", "PRE_OPEN"},{"22", "OPENING_ROTATION"},{"23", "FAST_MARKET"},{"24", "PRE_CROSS"},{"25", "CROSS"},{"26", "POST_CLOSE"}], "TagNum" => "326"}


,
"HaltReasonInt" => #{"TagNum" => "327" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NEWS_DISSEMINATION"},{"1", "ORDER_INFLUX"},{"2", "ORDER_IMBALANCE"},{"3", "ADDITIONAL_INFORMATION"},{"4", "NEWS_PENDING"},{"5", "EQUIPMENT_CHANGEOVER"}]}
, "327" => #{"Name"=>"HaltReasonInt" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NEWS_DISSEMINATION"},{"1", "ORDER_INFLUX"},{"2", "ORDER_IMBALANCE"},{"3", "ADDITIONAL_INFORMATION"},{"4", "NEWS_PENDING"},{"5", "EQUIPMENT_CHANGEOVER"}], "TagNum" => "327"}


,
"InViewOfCommon" => #{"TagNum" => "328" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "328" => #{"Name"=>"InViewOfCommon" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "328"}


,
"DueToRelated" => #{"TagNum" => "329" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "329" => #{"Name"=>"DueToRelated" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "329"}


,
"BuyVolume" => #{"TagNum" => "330" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "330" => #{"Name"=>"BuyVolume" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "330"}


,
"SellVolume" => #{"TagNum" => "331" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "331" => #{"Name"=>"SellVolume" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "331"}


,
"HighPx" => #{"TagNum" => "332" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "332" => #{"Name"=>"HighPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "332"}


,
"LowPx" => #{"TagNum" => "333" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "333" => #{"Name"=>"LowPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "333"}


,
"Adjustment" => #{"TagNum" => "334" ,"Type" => "INT" ,"ValidValues" =>[{"1", "CANCEL"},{"2", "ERROR"},{"3", "CORRECTION"}]}
, "334" => #{"Name"=>"Adjustment" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "CANCEL"},{"2", "ERROR"},{"3", "CORRECTION"}], "TagNum" => "334"}


,
"TradSesReqID" => #{"TagNum" => "335" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "335" => #{"Name"=>"TradSesReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "335"}


,
"TradingSessionID" => #{"TagNum" => "336" ,"Type" => "STRING" ,"ValidValues" =>[{"1", "DAY"},{"2", "HALFDAY"},{"3", "MORNING"},{"4", "AFTERNOON"},{"5", "EVENING"},{"6", "AFTER_HOURS"}]}
, "336" => #{"Name"=>"TradingSessionID" ,"Type"=>"STRING" ,"ValidValues"=>[{"1", "DAY"},{"2", "HALFDAY"},{"3", "MORNING"},{"4", "AFTERNOON"},{"5", "EVENING"},{"6", "AFTER_HOURS"}], "TagNum" => "336"}


,
"ContraTrader" => #{"TagNum" => "337" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "337" => #{"Name"=>"ContraTrader" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "337"}


,
"TradSesMethod" => #{"TagNum" => "338" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ELECTRONIC"},{"2", "OPEN_OUTCRY"},{"3", "TWO_PARTY"}]}
, "338" => #{"Name"=>"TradSesMethod" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ELECTRONIC"},{"2", "OPEN_OUTCRY"},{"3", "TWO_PARTY"}], "TagNum" => "338"}


,
"TradSesMode" => #{"TagNum" => "339" ,"Type" => "INT" ,"ValidValues" =>[{"1", "TESTING"},{"2", "SIMULATED"},{"3", "PRODUCTION"}]}
, "339" => #{"Name"=>"TradSesMode" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "TESTING"},{"2", "SIMULATED"},{"3", "PRODUCTION"}], "TagNum" => "339"}


,
"TradSesStatus" => #{"TagNum" => "340" ,"Type" => "INT" ,"ValidValues" =>[{"0", "UNKNOWN"},{"1", "HALTED"},{"2", "OPEN"},{"3", "CLOSED"},{"4", "PRE_OPEN"},{"5", "PRE_CLOSE"},{"6", "REQUEST_REJECTED"}]}
, "340" => #{"Name"=>"TradSesStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "UNKNOWN"},{"1", "HALTED"},{"2", "OPEN"},{"3", "CLOSED"},{"4", "PRE_OPEN"},{"5", "PRE_CLOSE"},{"6", "REQUEST_REJECTED"}], "TagNum" => "340"}


,
"TradSesStartTime" => #{"TagNum" => "341" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "341" => #{"Name"=>"TradSesStartTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "341"}


,
"TradSesOpenTime" => #{"TagNum" => "342" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "342" => #{"Name"=>"TradSesOpenTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "342"}


,
"TradSesPreCloseTime" => #{"TagNum" => "343" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "343" => #{"Name"=>"TradSesPreCloseTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "343"}


,
"TradSesCloseTime" => #{"TagNum" => "344" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "344" => #{"Name"=>"TradSesCloseTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "344"}


,
"TradSesEndTime" => #{"TagNum" => "345" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "345" => #{"Name"=>"TradSesEndTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "345"}


,
"NumberOfOrders" => #{"TagNum" => "346" ,"Type" => "INT" ,"ValidValues" =>[]}
, "346" => #{"Name"=>"NumberOfOrders" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "346"}


,
"MessageEncoding" => #{"TagNum" => "347" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "347" => #{"Name"=>"MessageEncoding" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "347"}


,
"EncodedIssuerLen" => #{"TagNum" => "348" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "348" => #{"Name"=>"EncodedIssuerLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "348"}


,
"EncodedIssuer" => #{"TagNum" => "349" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "349" => #{"Name"=>"EncodedIssuer" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "349"}


,
"EncodedSecurityDescLen" => #{"TagNum" => "350" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "350" => #{"Name"=>"EncodedSecurityDescLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "350"}


,
"EncodedSecurityDesc" => #{"TagNum" => "351" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "351" => #{"Name"=>"EncodedSecurityDesc" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "351"}


,
"EncodedListExecInstLen" => #{"TagNum" => "352" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "352" => #{"Name"=>"EncodedListExecInstLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "352"}


,
"EncodedListExecInst" => #{"TagNum" => "353" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "353" => #{"Name"=>"EncodedListExecInst" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "353"}


,
"EncodedTextLen" => #{"TagNum" => "354" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "354" => #{"Name"=>"EncodedTextLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "354"}


,
"EncodedText" => #{"TagNum" => "355" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "355" => #{"Name"=>"EncodedText" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "355"}


,
"EncodedSubjectLen" => #{"TagNum" => "356" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "356" => #{"Name"=>"EncodedSubjectLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "356"}


,
"EncodedSubject" => #{"TagNum" => "357" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "357" => #{"Name"=>"EncodedSubject" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "357"}


,
"EncodedHeadlineLen" => #{"TagNum" => "358" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "358" => #{"Name"=>"EncodedHeadlineLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "358"}


,
"EncodedHeadline" => #{"TagNum" => "359" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "359" => #{"Name"=>"EncodedHeadline" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "359"}


,
"EncodedAllocTextLen" => #{"TagNum" => "360" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "360" => #{"Name"=>"EncodedAllocTextLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "360"}


,
"EncodedAllocText" => #{"TagNum" => "361" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "361" => #{"Name"=>"EncodedAllocText" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "361"}


,
"EncodedUnderlyingIssuerLen" => #{"TagNum" => "362" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "362" => #{"Name"=>"EncodedUnderlyingIssuerLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "362"}


,
"EncodedUnderlyingIssuer" => #{"TagNum" => "363" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "363" => #{"Name"=>"EncodedUnderlyingIssuer" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "363"}


,
"EncodedUnderlyingSecurityDescLen" => #{"TagNum" => "364" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "364" => #{"Name"=>"EncodedUnderlyingSecurityDescLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "364"}


,
"EncodedUnderlyingSecurityDesc" => #{"TagNum" => "365" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "365" => #{"Name"=>"EncodedUnderlyingSecurityDesc" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "365"}


,
"AllocPrice" => #{"TagNum" => "366" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "366" => #{"Name"=>"AllocPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "366"}


,
"QuoteSetValidUntilTime" => #{"TagNum" => "367" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "367" => #{"Name"=>"QuoteSetValidUntilTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "367"}


,
"QuoteEntryRejectReason" => #{"TagNum" => "368" ,"Type" => "INT" ,"ValidValues" =>[]}
, "368" => #{"Name"=>"QuoteEntryRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "368"}


,
"LastMsgSeqNumProcessed" => #{"TagNum" => "369" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "369" => #{"Name"=>"LastMsgSeqNumProcessed" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "369"}


,
"RefTagID" => #{"TagNum" => "371" ,"Type" => "INT" ,"ValidValues" =>[]}
, "371" => #{"Name"=>"RefTagID" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "371"}


,
"RefMsgType" => #{"TagNum" => "372" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "372" => #{"Name"=>"RefMsgType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "372"}


,
"SessionRejectReason" => #{"TagNum" => "373" ,"Type" => "INT" ,"ValidValues" =>[{"0", "INVALID_TAG_NUMBER"},{"1", "REQUIRED_TAG_MISSING"},{"2", "TAG_NOT_DEFINED_FOR_THIS_MESSAGE_TYPE"},{"3", "UNDEFINED_TAG"},{"4", "TAG_SPECIFIED_WITHOUT_A_VALUE"},{"5", "VALUE_IS_INCORRECT"},{"6", "INCORRECT_DATA_FORMAT_FOR_VALUE"},{"7", "DECRYPTION_PROBLEM"},{"8", "SIGNATURE_PROBLEM"},{"9", "COMPID_PROBLEM"},{"10", "SENDINGTIME_ACCURACY_PROBLEM"},{"11", "INVALID_MSGTYPE"},{"12", "XML_VALIDATION_ERROR"},{"13", "TAG_APPEARS_MORE_THAN_ONCE"},{"14", "TAG_SPECIFIED_OUT_OF_REQUIRED_ORDER"},{"15", "REPEATING_GROUP_FIELDS_OUT_OF_ORDER"},{"16", "INCORRECT_NUMINGROUP_COUNT_FOR_REPEATING_GROUP"},{"17", "NON_DATA_VALUE_INCLUDES_FIELD_DELIMITER"},{"18", "INVALID_UNSUPPORTED_APPLICATION_VERSION"},{"99", "OTHER"}]}
, "373" => #{"Name"=>"SessionRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "INVALID_TAG_NUMBER"},{"1", "REQUIRED_TAG_MISSING"},{"2", "TAG_NOT_DEFINED_FOR_THIS_MESSAGE_TYPE"},{"3", "UNDEFINED_TAG"},{"4", "TAG_SPECIFIED_WITHOUT_A_VALUE"},{"5", "VALUE_IS_INCORRECT"},{"6", "INCORRECT_DATA_FORMAT_FOR_VALUE"},{"7", "DECRYPTION_PROBLEM"},{"8", "SIGNATURE_PROBLEM"},{"9", "COMPID_PROBLEM"},{"10", "SENDINGTIME_ACCURACY_PROBLEM"},{"11", "INVALID_MSGTYPE"},{"12", "XML_VALIDATION_ERROR"},{"13", "TAG_APPEARS_MORE_THAN_ONCE"},{"14", "TAG_SPECIFIED_OUT_OF_REQUIRED_ORDER"},{"15", "REPEATING_GROUP_FIELDS_OUT_OF_ORDER"},{"16", "INCORRECT_NUMINGROUP_COUNT_FOR_REPEATING_GROUP"},{"17", "NON_DATA_VALUE_INCLUDES_FIELD_DELIMITER"},{"18", "INVALID_UNSUPPORTED_APPLICATION_VERSION"},{"99", "OTHER"}], "TagNum" => "373"}


,
"BidRequestTransType" => #{"TagNum" => "374" ,"Type" => "CHAR" ,"ValidValues" =>[{"C", "CANCEL"},{"N", "NO"}]}
, "374" => #{"Name"=>"BidRequestTransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"C", "CANCEL"},{"N", "NO"}], "TagNum" => "374"}


,
"ContraBroker" => #{"TagNum" => "375" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "375" => #{"Name"=>"ContraBroker" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "375"}


,
"ComplianceID" => #{"TagNum" => "376" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "376" => #{"Name"=>"ComplianceID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "376"}


,
"SolicitedFlag" => #{"TagNum" => "377" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "377" => #{"Name"=>"SolicitedFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "377"}


,
"ExecRestatementReason" => #{"TagNum" => "378" ,"Type" => "INT" ,"ValidValues" =>[{"0", "GT_CORPORATE_ACTION"},{"1", "GT_RENEWAL"},{"2", "VERBAL_CHANGE"},{"3", "REPRICING_OF_ORDER"},{"4", "BROKER_OPTION"},{"5", "PARTIAL_DECLINE_OF_ORDERQTY"},{"6", "CANCEL_ON_TRADING_HALT"},{"7", "CANCEL_ON_SYSTEM_FAILURE"},{"8", "MARKET"},{"9", "CANCELED_NOT_BEST"},{"10", "WAREHOUSE_RECAP"},{"11", "PEG_REFRESH"},{"99", "OTHER"}]}
, "378" => #{"Name"=>"ExecRestatementReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "GT_CORPORATE_ACTION"},{"1", "GT_RENEWAL"},{"2", "VERBAL_CHANGE"},{"3", "REPRICING_OF_ORDER"},{"4", "BROKER_OPTION"},{"5", "PARTIAL_DECLINE_OF_ORDERQTY"},{"6", "CANCEL_ON_TRADING_HALT"},{"7", "CANCEL_ON_SYSTEM_FAILURE"},{"8", "MARKET"},{"9", "CANCELED_NOT_BEST"},{"10", "WAREHOUSE_RECAP"},{"11", "PEG_REFRESH"},{"99", "OTHER"}], "TagNum" => "378"}


,
"BusinessRejectRefID" => #{"TagNum" => "379" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "379" => #{"Name"=>"BusinessRejectRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "379"}


,
"BusinessRejectReason" => #{"TagNum" => "380" ,"Type" => "INT" ,"ValidValues" =>[{"0", "OTHER"},{"1", "UNKNOWN_ID"},{"2", "UNKNOWN_SECURITY"},{"3", "UNSUPPORTED_MESSAGE_TYPE"},{"4", "APPLICATION_NOT_AVAILABLE"},{"5", "CONDITIONALLY_REQUIRED_FIELD_MISSING"},{"6", "NOT_AUTHORIZED"},{"7", "DELIVERTO_FIRM_NOT_AVAILABLE_AT_THIS_TIME"},{"18", "INVALID_PRICE_INCREMENT"}]}
, "380" => #{"Name"=>"BusinessRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "OTHER"},{"1", "UNKNOWN_ID"},{"2", "UNKNOWN_SECURITY"},{"3", "UNSUPPORTED_MESSAGE_TYPE"},{"4", "APPLICATION_NOT_AVAILABLE"},{"5", "CONDITIONALLY_REQUIRED_FIELD_MISSING"},{"6", "NOT_AUTHORIZED"},{"7", "DELIVERTO_FIRM_NOT_AVAILABLE_AT_THIS_TIME"},{"18", "INVALID_PRICE_INCREMENT"}], "TagNum" => "380"}


,
"GrossTradeAmt" => #{"TagNum" => "381" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "381" => #{"Name"=>"GrossTradeAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "381"}


,
"NoContraBrokers" => #{"TagNum" => "382" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "382" => #{"Name"=>"NoContraBrokers" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "382"}


,
"MaxMessageSize" => #{"TagNum" => "383" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "383" => #{"Name"=>"MaxMessageSize" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "383"}


,
"NoMsgTypes" => #{"TagNum" => "384" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "384" => #{"Name"=>"NoMsgTypes" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "384"}


,
"MsgDirection" => #{"TagNum" => "385" ,"Type" => "CHAR" ,"ValidValues" =>[{"R", "RECEIVE"},{"S", "SEND"}]}
, "385" => #{"Name"=>"MsgDirection" ,"Type"=>"CHAR" ,"ValidValues"=>[{"R", "RECEIVE"},{"S", "SEND"}], "TagNum" => "385"}


,
"NoTradingSessions" => #{"TagNum" => "386" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "386" => #{"Name"=>"NoTradingSessions" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "386"}


,
"TotalVolumeTraded" => #{"TagNum" => "387" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "387" => #{"Name"=>"TotalVolumeTraded" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "387"}


,
"DiscretionInst" => #{"TagNum" => "388" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "RELATED_TO_DISPLAYED_PRICE"},{"1", "RELATED_TO_MARKET_PRICE"},{"2", "RELATED_TO_PRIMARY_PRICE"},{"3", "RELATED_TO_LOCAL_PRIMARY_PRICE"},{"4", "RELATED_TO_MIDPOINT_PRICE"},{"5", "RELATED_TO_LAST_TRADE_PRICE"},{"6", "RELATED_TO_VWAP"},{"7", "AVERAGE_PRICE_GUARANTEE"}]}
, "388" => #{"Name"=>"DiscretionInst" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "RELATED_TO_DISPLAYED_PRICE"},{"1", "RELATED_TO_MARKET_PRICE"},{"2", "RELATED_TO_PRIMARY_PRICE"},{"3", "RELATED_TO_LOCAL_PRIMARY_PRICE"},{"4", "RELATED_TO_MIDPOINT_PRICE"},{"5", "RELATED_TO_LAST_TRADE_PRICE"},{"6", "RELATED_TO_VWAP"},{"7", "AVERAGE_PRICE_GUARANTEE"}], "TagNum" => "388"}


,
"DiscretionOffsetValue" => #{"TagNum" => "389" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "389" => #{"Name"=>"DiscretionOffsetValue" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "389"}


,
"BidID" => #{"TagNum" => "390" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "390" => #{"Name"=>"BidID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "390"}


,
"ClientBidID" => #{"TagNum" => "391" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "391" => #{"Name"=>"ClientBidID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "391"}


,
"ListName" => #{"TagNum" => "392" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "392" => #{"Name"=>"ListName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "392"}


,
"TotNoRelatedSym" => #{"TagNum" => "393" ,"Type" => "INT" ,"ValidValues" =>[]}
, "393" => #{"Name"=>"TotNoRelatedSym" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "393"}


,
"BidType" => #{"TagNum" => "394" ,"Type" => "INT" ,"ValidValues" =>[{"1", "NON_DISCLOSED_STYLE"},{"2", "DISCLOSED_SYTLE"},{"3", "NO_BIDDING_PROCESS"}]}
, "394" => #{"Name"=>"BidType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "NON_DISCLOSED_STYLE"},{"2", "DISCLOSED_SYTLE"},{"3", "NO_BIDDING_PROCESS"}], "TagNum" => "394"}


,
"NumTickets" => #{"TagNum" => "395" ,"Type" => "INT" ,"ValidValues" =>[]}
, "395" => #{"Name"=>"NumTickets" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "395"}


,
"SideValue1" => #{"TagNum" => "396" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "396" => #{"Name"=>"SideValue1" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "396"}


,
"SideValue2" => #{"TagNum" => "397" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "397" => #{"Name"=>"SideValue2" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "397"}


,
"NoBidDescriptors" => #{"TagNum" => "398" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "398" => #{"Name"=>"NoBidDescriptors" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "398"}


,
"BidDescriptorType" => #{"TagNum" => "399" ,"Type" => "INT" ,"ValidValues" =>[{"1", "SECTOR"},{"2", "COUNTRY"},{"3", "INDEX"}]}
, "399" => #{"Name"=>"BidDescriptorType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "SECTOR"},{"2", "COUNTRY"},{"3", "INDEX"}], "TagNum" => "399"}


,
"BidDescriptor" => #{"TagNum" => "400" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "400" => #{"Name"=>"BidDescriptor" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "400"}


,
"SideValueInd" => #{"TagNum" => "401" ,"Type" => "INT" ,"ValidValues" =>[{"1", "SIDE_VALUE_1"},{"2", "SIDE_VALUE_2"}]}
, "401" => #{"Name"=>"SideValueInd" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "SIDE_VALUE_1"},{"2", "SIDE_VALUE_2"}], "TagNum" => "401"}


,
"LiquidityPctLow" => #{"TagNum" => "402" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "402" => #{"Name"=>"LiquidityPctLow" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "402"}


,
"LiquidityPctHigh" => #{"TagNum" => "403" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "403" => #{"Name"=>"LiquidityPctHigh" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "403"}


,
"LiquidityValue" => #{"TagNum" => "404" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "404" => #{"Name"=>"LiquidityValue" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "404"}


,
"EFPTrackingError" => #{"TagNum" => "405" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "405" => #{"Name"=>"EFPTrackingError" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "405"}


,
"FairValue" => #{"TagNum" => "406" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "406" => #{"Name"=>"FairValue" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "406"}


,
"OutsideIndexPct" => #{"TagNum" => "407" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "407" => #{"Name"=>"OutsideIndexPct" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "407"}


,
"ValueOfFutures" => #{"TagNum" => "408" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "408" => #{"Name"=>"ValueOfFutures" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "408"}


,
"LiquidityIndType" => #{"TagNum" => "409" ,"Type" => "INT" ,"ValidValues" =>[{"1", "5_DAY_MOVING_AVERAGE"},{"2", "20_DAY_MOVING_AVERAGE"},{"3", "NORMAL_MARKET_SIZE"},{"4", "OTHER"}]}
, "409" => #{"Name"=>"LiquidityIndType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "5_DAY_MOVING_AVERAGE"},{"2", "20_DAY_MOVING_AVERAGE"},{"3", "NORMAL_MARKET_SIZE"},{"4", "OTHER"}], "TagNum" => "409"}


,
"WtAverageLiquidity" => #{"TagNum" => "410" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "410" => #{"Name"=>"WtAverageLiquidity" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "410"}


,
"ExchangeForPhysical" => #{"TagNum" => "411" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "411" => #{"Name"=>"ExchangeForPhysical" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "411"}


,
"OutMainCntryUIndex" => #{"TagNum" => "412" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "412" => #{"Name"=>"OutMainCntryUIndex" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "412"}


,
"CrossPercent" => #{"TagNum" => "413" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "413" => #{"Name"=>"CrossPercent" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "413"}


,
"ProgRptReqs" => #{"TagNum" => "414" ,"Type" => "INT" ,"ValidValues" =>[{"1", "BUY_SIDE_EXPLICITLY_REQUESTS_STATUS_USING_STATUE_REQUEST"},{"2", "SELL_SIDE_PERIODICALLY_SENDS_STATUS_USING_LIST_STATUS_PERIOD_OPTIONALLY_SPECIFIED_IN_PROGRESSPERIOD"},{"3", "REAL_TIME_EXECUTION_REPORTS"}]}
, "414" => #{"Name"=>"ProgRptReqs" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "BUY_SIDE_EXPLICITLY_REQUESTS_STATUS_USING_STATUE_REQUEST"},{"2", "SELL_SIDE_PERIODICALLY_SENDS_STATUS_USING_LIST_STATUS_PERIOD_OPTIONALLY_SPECIFIED_IN_PROGRESSPERIOD"},{"3", "REAL_TIME_EXECUTION_REPORTS"}], "TagNum" => "414"}


,
"ProgPeriodInterval" => #{"TagNum" => "415" ,"Type" => "INT" ,"ValidValues" =>[]}
, "415" => #{"Name"=>"ProgPeriodInterval" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "415"}


,
"IncTaxInd" => #{"TagNum" => "416" ,"Type" => "INT" ,"ValidValues" =>[{"1", "NET"},{"2", "GROSS"}]}
, "416" => #{"Name"=>"IncTaxInd" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "NET"},{"2", "GROSS"}], "TagNum" => "416"}


,
"NumBidders" => #{"TagNum" => "417" ,"Type" => "INT" ,"ValidValues" =>[]}
, "417" => #{"Name"=>"NumBidders" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "417"}


,
"BidTradeType" => #{"TagNum" => "418" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "AGENCY"},{"G", "VWAP_GUARANTEE"},{"J", "GUARANTEED_CLOSE"},{"R", "RISK_TRADE"}]}
, "418" => #{"Name"=>"BidTradeType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "AGENCY"},{"G", "VWAP_GUARANTEE"},{"J", "GUARANTEED_CLOSE"},{"R", "RISK_TRADE"}], "TagNum" => "418"}


,
"BasisPxType" => #{"TagNum" => "419" ,"Type" => "CHAR" ,"ValidValues" =>[{"2", "CLOSING_PRICE_AT_MORNING_SESSION"},{"3", "CLOSING_PRICE"},{"4", "CURRENT_PRICE"},{"5", "SQ"},{"6", "VWAP_THROUGH_A_DAY"},{"7", "VWAP_THROUGH_A_MORNING_SESSION"},{"8", "VWAP_THROUGH_AN_AFTERNOON_SESSION"},{"9", "VWAP_THROUGH_A_DAY_EXCEPT_YORI"},{"A", "VWAP_THROUGH_A_MORNING_SESSION_EXCEPT_YORI"},{"B", "VWAP_THROUGH_AN_AFTERNOON_SESSION_EXCEPT_YORI"},{"C", "STRIKE"},{"D", "OPEN"},{"Z", "OTHERS"}]}
, "419" => #{"Name"=>"BasisPxType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"2", "CLOSING_PRICE_AT_MORNING_SESSION"},{"3", "CLOSING_PRICE"},{"4", "CURRENT_PRICE"},{"5", "SQ"},{"6", "VWAP_THROUGH_A_DAY"},{"7", "VWAP_THROUGH_A_MORNING_SESSION"},{"8", "VWAP_THROUGH_AN_AFTERNOON_SESSION"},{"9", "VWAP_THROUGH_A_DAY_EXCEPT_YORI"},{"A", "VWAP_THROUGH_A_MORNING_SESSION_EXCEPT_YORI"},{"B", "VWAP_THROUGH_AN_AFTERNOON_SESSION_EXCEPT_YORI"},{"C", "STRIKE"},{"D", "OPEN"},{"Z", "OTHERS"}], "TagNum" => "419"}


,
"NoBidComponents" => #{"TagNum" => "420" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "420" => #{"Name"=>"NoBidComponents" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "420"}


,
"Country" => #{"TagNum" => "421" ,"Type" => "COUNTRY" ,"ValidValues" =>[]}
, "421" => #{"Name"=>"Country" ,"Type"=>"COUNTRY" ,"ValidValues"=>[], "TagNum" => "421"}


,
"TotNoStrikes" => #{"TagNum" => "422" ,"Type" => "INT" ,"ValidValues" =>[]}
, "422" => #{"Name"=>"TotNoStrikes" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "422"}


,
"PriceType" => #{"TagNum" => "423" ,"Type" => "INT" ,"ValidValues" =>[{"1", "PERCENTAGE"},{"2", "PER_UNIT"},{"3", "FIXED_AMOUNT"},{"4", "DISCOUNT"},{"5", "PREMIUM"},{"6", "SPREAD"},{"7", "TED_PRICE"},{"8", "TED_YIELD"},{"9", "YIELD"},{"10", "FIXED_CABINET_TRADE_PRICE"},{"11", "VARIABLE_CABINET_TRADE_PRICE"},{"13", "PRODUCT_TICKS_IN_HALFS"},{"14", "PRODUCT_TICKS_IN_FOURTHS"},{"15", "PRODUCT_TICKS_IN_EIGHTS"},{"16", "PRODUCT_TICKS_IN_SIXTEENTHS"},{"17", "PRODUCT_TICKS_IN_THIRTY_SECONDS"},{"18", "PRODUCT_TICKS_IN_SIXTY_FORTHS"},{"19", "PRODUCT_TICKS_IN_ONE_TWENTY_EIGHTS"}]}
, "423" => #{"Name"=>"PriceType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "PERCENTAGE"},{"2", "PER_UNIT"},{"3", "FIXED_AMOUNT"},{"4", "DISCOUNT"},{"5", "PREMIUM"},{"6", "SPREAD"},{"7", "TED_PRICE"},{"8", "TED_YIELD"},{"9", "YIELD"},{"10", "FIXED_CABINET_TRADE_PRICE"},{"11", "VARIABLE_CABINET_TRADE_PRICE"},{"13", "PRODUCT_TICKS_IN_HALFS"},{"14", "PRODUCT_TICKS_IN_FOURTHS"},{"15", "PRODUCT_TICKS_IN_EIGHTS"},{"16", "PRODUCT_TICKS_IN_SIXTEENTHS"},{"17", "PRODUCT_TICKS_IN_THIRTY_SECONDS"},{"18", "PRODUCT_TICKS_IN_SIXTY_FORTHS"},{"19", "PRODUCT_TICKS_IN_ONE_TWENTY_EIGHTS"}], "TagNum" => "423"}


,
"DayOrderQty" => #{"TagNum" => "424" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "424" => #{"Name"=>"DayOrderQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "424"}


,
"DayCumQty" => #{"TagNum" => "425" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "425" => #{"Name"=>"DayCumQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "425"}


,
"DayAvgPx" => #{"TagNum" => "426" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "426" => #{"Name"=>"DayAvgPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "426"}


,
"GTBookingInst" => #{"TagNum" => "427" ,"Type" => "INT" ,"ValidValues" =>[{"0", "BOOK_OUT_ALL_TRADES_ON_DAY_OF_EXECUTION"},{"1", "ACCUMULATE_EXECTUIONS_UNTIL_FORDER_IS_FILLED_OR_EXPIRES"},{"2", "ACCUMULATE_UNTIL_VERBALLLY_NOTIFIED_OTHERWISE"}]}
, "427" => #{"Name"=>"GTBookingInst" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "BOOK_OUT_ALL_TRADES_ON_DAY_OF_EXECUTION"},{"1", "ACCUMULATE_EXECTUIONS_UNTIL_FORDER_IS_FILLED_OR_EXPIRES"},{"2", "ACCUMULATE_UNTIL_VERBALLLY_NOTIFIED_OTHERWISE"}], "TagNum" => "427"}


,
"NoStrikes" => #{"TagNum" => "428" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "428" => #{"Name"=>"NoStrikes" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "428"}


,
"ListStatusType" => #{"TagNum" => "429" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ACK"},{"2", "RESPONSE"},{"3", "TIMED"},{"4", "EXEC_STARTED"},{"5", "ALL_DONE"},{"6", "ALERT"}]}
, "429" => #{"Name"=>"ListStatusType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ACK"},{"2", "RESPONSE"},{"3", "TIMED"},{"4", "EXEC_STARTED"},{"5", "ALL_DONE"},{"6", "ALERT"}], "TagNum" => "429"}


,
"NetGrossInd" => #{"TagNum" => "430" ,"Type" => "INT" ,"ValidValues" =>[{"1", "NET"},{"2", "GROSS"}]}
, "430" => #{"Name"=>"NetGrossInd" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "NET"},{"2", "GROSS"}], "TagNum" => "430"}


,
"ListOrderStatus" => #{"TagNum" => "431" ,"Type" => "INT" ,"ValidValues" =>[{"1", "IN_BIDDING_PROCESS"},{"2", "RECEIVED_FOR_EXECUTION"},{"3", "EXECUTING"},{"4", "CANCELLING"},{"5", "ALERT"},{"6", "ALL_DONE"},{"7", "REJECT"}]}
, "431" => #{"Name"=>"ListOrderStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "IN_BIDDING_PROCESS"},{"2", "RECEIVED_FOR_EXECUTION"},{"3", "EXECUTING"},{"4", "CANCELLING"},{"5", "ALERT"},{"6", "ALL_DONE"},{"7", "REJECT"}], "TagNum" => "431"}


,
"ExpireDate" => #{"TagNum" => "432" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "432" => #{"Name"=>"ExpireDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "432"}


,
"ListExecInstType" => #{"TagNum" => "433" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "IMMEDIATE"},{"2", "WAIT_FOR_EXECUT_INSTRUCTION"},{"3", "EXCHANGE_SWITCH_CIV_ORDER_3"},{"4", "EXCHANGE_SWITCH_CIV_ORDER_4"},{"5", "EXCHANGE_SWITCH_CIV_ORDER_5"}]}
, "433" => #{"Name"=>"ListExecInstType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "IMMEDIATE"},{"2", "WAIT_FOR_EXECUT_INSTRUCTION"},{"3", "EXCHANGE_SWITCH_CIV_ORDER_3"},{"4", "EXCHANGE_SWITCH_CIV_ORDER_4"},{"5", "EXCHANGE_SWITCH_CIV_ORDER_5"}], "TagNum" => "433"}


,
"CxlRejResponseTo" => #{"TagNum" => "434" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "ORDER_CANCEL_REQUEST"},{"2", "ORDER_CANCEL_REPLACE_REQUEST"}]}
, "434" => #{"Name"=>"CxlRejResponseTo" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "ORDER_CANCEL_REQUEST"},{"2", "ORDER_CANCEL_REPLACE_REQUEST"}], "TagNum" => "434"}


,
"UnderlyingCouponRate" => #{"TagNum" => "435" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "435" => #{"Name"=>"UnderlyingCouponRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "435"}


,
"UnderlyingContractMultiplier" => #{"TagNum" => "436" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "436" => #{"Name"=>"UnderlyingContractMultiplier" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "436"}


,
"ContraTradeQty" => #{"TagNum" => "437" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "437" => #{"Name"=>"ContraTradeQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "437"}


,
"ContraTradeTime" => #{"TagNum" => "438" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "438" => #{"Name"=>"ContraTradeTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "438"}


,
"LiquidityNumSecurities" => #{"TagNum" => "441" ,"Type" => "INT" ,"ValidValues" =>[]}
, "441" => #{"Name"=>"LiquidityNumSecurities" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "441"}


,
"MultiLegReportingType" => #{"TagNum" => "442" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "SINGLE_SECURITY"},{"2", "INDIVIDUAL_LEG_OF_A_MULTI_LEG_SECURITY"},{"3", "MULTI_LEG_SECURITY"}]}
, "442" => #{"Name"=>"MultiLegReportingType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "SINGLE_SECURITY"},{"2", "INDIVIDUAL_LEG_OF_A_MULTI_LEG_SECURITY"},{"3", "MULTI_LEG_SECURITY"}], "TagNum" => "442"}


,
"StrikeTime" => #{"TagNum" => "443" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "443" => #{"Name"=>"StrikeTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "443"}


,
"ListStatusText" => #{"TagNum" => "444" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "444" => #{"Name"=>"ListStatusText" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "444"}


,
"EncodedListStatusTextLen" => #{"TagNum" => "445" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "445" => #{"Name"=>"EncodedListStatusTextLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "445"}


,
"EncodedListStatusText" => #{"TagNum" => "446" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "446" => #{"Name"=>"EncodedListStatusText" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "446"}


,
"PartyIDSource" => #{"TagNum" => "447" ,"Type" => "CHAR" ,"ValidValues" =>[{"6", "UK_NATIONAL_INSURANCE_OR_PENSION_NUMBER"},{"7", "US_SOCIAL_SECURITY_NUMBER"},{"8", "US_EMPLOYER_OR_TAX_ID_NUMBER"},{"9", "AUSTRALIAN_BUSINESS_NUMBER"},{"A", "AUSTRALIAN_TAX_FILE_NUMBER"},{"1", "KOREAN_INVESTOR_ID"},{"2", "TAIWANESE_QUALIFIED_FOREIGN_INVESTOR_ID_QFII_FID"},{"3", "TAIWANESE_TRADING_ACCT"},{"4", "MALAYSIAN_CENTRAL_DEPOSITORY"},{"5", "CHINESE_INVESTOR_ID"},{"I", "DIRECTED_BROKER_THREE_CHARACTER_ACRONYM_AS_DEFINED_IN_ISITC_ETC_BEST_PRACTICE_GUIDELINES_DOCUMENT"},{"B", "BIC"},{"C", "GENERALLY_ACCEPTED_MARKET_PARTICIPANT_IDENTIFIER"},{"D", "PROPRIETARY"},{"E", "ISO_COUNTRY_CODE"},{"F", "SETTLEMENT_ENTITY_LOCATION"},{"G", "MIC"},{"H", "CSD_PARTICIPANT_MEMBER_CODE"}]}
, "447" => #{"Name"=>"PartyIDSource" ,"Type"=>"CHAR" ,"ValidValues"=>[{"6", "UK_NATIONAL_INSURANCE_OR_PENSION_NUMBER"},{"7", "US_SOCIAL_SECURITY_NUMBER"},{"8", "US_EMPLOYER_OR_TAX_ID_NUMBER"},{"9", "AUSTRALIAN_BUSINESS_NUMBER"},{"A", "AUSTRALIAN_TAX_FILE_NUMBER"},{"1", "KOREAN_INVESTOR_ID"},{"2", "TAIWANESE_QUALIFIED_FOREIGN_INVESTOR_ID_QFII_FID"},{"3", "TAIWANESE_TRADING_ACCT"},{"4", "MALAYSIAN_CENTRAL_DEPOSITORY"},{"5", "CHINESE_INVESTOR_ID"},{"I", "DIRECTED_BROKER_THREE_CHARACTER_ACRONYM_AS_DEFINED_IN_ISITC_ETC_BEST_PRACTICE_GUIDELINES_DOCUMENT"},{"B", "BIC"},{"C", "GENERALLY_ACCEPTED_MARKET_PARTICIPANT_IDENTIFIER"},{"D", "PROPRIETARY"},{"E", "ISO_COUNTRY_CODE"},{"F", "SETTLEMENT_ENTITY_LOCATION"},{"G", "MIC"},{"H", "CSD_PARTICIPANT_MEMBER_CODE"}], "TagNum" => "447"}


,
"PartyID" => #{"TagNum" => "448" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "448" => #{"Name"=>"PartyID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "448"}


,
"NetChgPrevDay" => #{"TagNum" => "451" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "451" => #{"Name"=>"NetChgPrevDay" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "451"}


,
"PartyRole" => #{"TagNum" => "452" ,"Type" => "INT" ,"ValidValues" =>[{"1", "EXECUTING_FIRM"},{"2", "BROKER_OF_CREDIT"},{"3", "CLIENT_ID"},{"4", "CLEARING_FIRM"},{"5", "INVESTOR_ID"},{"6", "INTRODUCING_FIRM"},{"7", "ENTERING_FIRM"},{"8", "LOCATE"},{"9", "FUND_MANAGER_CLIENT_ID"},{"10", "SETTLEMENT_LOCATION"},{"11", "ORDER_ORIGINATION_TRADER"},{"12", "EXECUTING_TRADER"},{"13", "ORDER_ORIGINATION_FIRM"},{"14", "GIVEUP_CLEARING_FIRM"},{"15", "CORRESPONDANT_CLEARING_FIRM"},{"16", "EXECUTING_SYSTEM"},{"17", "CONTRA_FIRM"},{"18", "CONTRA_CLEARING_FIRM"},{"19", "SPONSORING_FIRM"},{"20", "UNDERLYING_CONTRA_FIRM"},{"21", "CLEARING_ORGANIZATION"},{"22", "EXCHANGE"},{"24", "CUSTOMER_ACCOUNT"},{"25", "CORRESPONDENT_CLEARING_ORGANIZATION"},{"26", "CORRESPONDENT_BROKER"},{"27", "BUYER_SELLER"},{"28", "CUSTODIAN"},{"29", "INTERMEDIARY"},{"30", "AGENT"},{"31", "SUB_CUSTODIAN"},{"32", "BENEFICIARY"},{"33", "INTERESTED_PARTY"},{"34", "REGULATORY_BODY"},{"35", "LIQUIDITY_PROVIDER"},{"36", "ENTERING_TRADER"},{"37", "CONTRA_TRADER"},{"38", "POSITION_ACCOUNT"},{"39", "CONTRA_INVESTOR_ID"},{"40", "TRANSFER_TO_FIRM"},{"41", "CONTRA_POSITION_ACCOUNT"},{"42", "CONTRA_EXCHANGE"},{"43", "INTERNAL_CARRY_ACCOUNT"},{"44", "ORDER_ENTRY_OPERATOR_ID"},{"45", "SECONDARY_ACCOUNT_NUMBER"},{"46", "FOREIGN_FIRM"},{"47", "THIRD_PARTY_ALLOCATION_FIRM"},{"48", "CLAIMING_ACCOUNT"},{"49", "ASSET_MANAGER"},{"50", "PLEDGOR_ACCOUNT"},{"51", "PLEDGEE_ACCOUNT"},{"52", "LARGE_TRADER_REPORTABLE_ACCOUNT"},{"53", "TRADER_MNEMONIC"},{"54", "SENDER_LOCATION"},{"55", "SESSION_ID"},{"56", "ACCEPTABLE_COUNTERPARTY"},{"57", "UNACCEPTABLE_COUNTERPARTY"},{"58", "ENTERING_UNIT"},{"59", "EXECUTING_UNIT"},{"60", "INTRODUCING_BROKER"},{"61", "QUOTE_ORIGINATOR"},{"62", "REPORT_ORIGINATOR"},{"63", "SYSTEMATIC_INTERNALISER"},{"64", "MULTILATERAL_TRADING_FACILITY"},{"65", "REGULATED_MARKET"},{"66", "MARKET_MAKER"},{"67", "INVESTMENT_FIRM"},{"68", "HOST_COMPETENT_AUTHORITY"},{"69", "HOME_COMPETENT_AUTHORITY"},{"70", "COMPETENT_AUTHORITY_OF_THE_MOST_RELEVANT_MARKET_IN_TERMS_OF_LIQUIDITY"},{"71", "COMPETENT_AUTHORITY_OF_THE_TRANSACTION"},{"72", "REPORTING_INTERMEDIARY"},{"73", "EXECUTION_VENUE"},{"74", "MARKET_DATA_ENTRY_ORIGINATOR"},{"75", "LOCATION_ID"},{"76", "DESK_ID"},{"77", "MARKET_DATA_MARKET"},{"78", "ALLOCATION_ENTITY"},{"79", "PRIME_BROKER_PROVIDING_GENERAL_TRADE_SERVICES"},{"80", "STEP_OUT_FIRM"},{"81", "BROKERCLEARINGID"},{"82", "CENTRAL_REGISTRATION_DEPOSITORY"},{"83", "CLEARING_ACCOUNT"},{"84", "ACCEPTABLE_SETTLING_COUNTERPARTY"},{"85", "UNACCEPTABLE_SETTLING_COUNTERPARTY"}]}
, "452" => #{"Name"=>"PartyRole" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "EXECUTING_FIRM"},{"2", "BROKER_OF_CREDIT"},{"3", "CLIENT_ID"},{"4", "CLEARING_FIRM"},{"5", "INVESTOR_ID"},{"6", "INTRODUCING_FIRM"},{"7", "ENTERING_FIRM"},{"8", "LOCATE"},{"9", "FUND_MANAGER_CLIENT_ID"},{"10", "SETTLEMENT_LOCATION"},{"11", "ORDER_ORIGINATION_TRADER"},{"12", "EXECUTING_TRADER"},{"13", "ORDER_ORIGINATION_FIRM"},{"14", "GIVEUP_CLEARING_FIRM"},{"15", "CORRESPONDANT_CLEARING_FIRM"},{"16", "EXECUTING_SYSTEM"},{"17", "CONTRA_FIRM"},{"18", "CONTRA_CLEARING_FIRM"},{"19", "SPONSORING_FIRM"},{"20", "UNDERLYING_CONTRA_FIRM"},{"21", "CLEARING_ORGANIZATION"},{"22", "EXCHANGE"},{"24", "CUSTOMER_ACCOUNT"},{"25", "CORRESPONDENT_CLEARING_ORGANIZATION"},{"26", "CORRESPONDENT_BROKER"},{"27", "BUYER_SELLER"},{"28", "CUSTODIAN"},{"29", "INTERMEDIARY"},{"30", "AGENT"},{"31", "SUB_CUSTODIAN"},{"32", "BENEFICIARY"},{"33", "INTERESTED_PARTY"},{"34", "REGULATORY_BODY"},{"35", "LIQUIDITY_PROVIDER"},{"36", "ENTERING_TRADER"},{"37", "CONTRA_TRADER"},{"38", "POSITION_ACCOUNT"},{"39", "CONTRA_INVESTOR_ID"},{"40", "TRANSFER_TO_FIRM"},{"41", "CONTRA_POSITION_ACCOUNT"},{"42", "CONTRA_EXCHANGE"},{"43", "INTERNAL_CARRY_ACCOUNT"},{"44", "ORDER_ENTRY_OPERATOR_ID"},{"45", "SECONDARY_ACCOUNT_NUMBER"},{"46", "FOREIGN_FIRM"},{"47", "THIRD_PARTY_ALLOCATION_FIRM"},{"48", "CLAIMING_ACCOUNT"},{"49", "ASSET_MANAGER"},{"50", "PLEDGOR_ACCOUNT"},{"51", "PLEDGEE_ACCOUNT"},{"52", "LARGE_TRADER_REPORTABLE_ACCOUNT"},{"53", "TRADER_MNEMONIC"},{"54", "SENDER_LOCATION"},{"55", "SESSION_ID"},{"56", "ACCEPTABLE_COUNTERPARTY"},{"57", "UNACCEPTABLE_COUNTERPARTY"},{"58", "ENTERING_UNIT"},{"59", "EXECUTING_UNIT"},{"60", "INTRODUCING_BROKER"},{"61", "QUOTE_ORIGINATOR"},{"62", "REPORT_ORIGINATOR"},{"63", "SYSTEMATIC_INTERNALISER"},{"64", "MULTILATERAL_TRADING_FACILITY"},{"65", "REGULATED_MARKET"},{"66", "MARKET_MAKER"},{"67", "INVESTMENT_FIRM"},{"68", "HOST_COMPETENT_AUTHORITY"},{"69", "HOME_COMPETENT_AUTHORITY"},{"70", "COMPETENT_AUTHORITY_OF_THE_MOST_RELEVANT_MARKET_IN_TERMS_OF_LIQUIDITY"},{"71", "COMPETENT_AUTHORITY_OF_THE_TRANSACTION"},{"72", "REPORTING_INTERMEDIARY"},{"73", "EXECUTION_VENUE"},{"74", "MARKET_DATA_ENTRY_ORIGINATOR"},{"75", "LOCATION_ID"},{"76", "DESK_ID"},{"77", "MARKET_DATA_MARKET"},{"78", "ALLOCATION_ENTITY"},{"79", "PRIME_BROKER_PROVIDING_GENERAL_TRADE_SERVICES"},{"80", "STEP_OUT_FIRM"},{"81", "BROKERCLEARINGID"},{"82", "CENTRAL_REGISTRATION_DEPOSITORY"},{"83", "CLEARING_ACCOUNT"},{"84", "ACCEPTABLE_SETTLING_COUNTERPARTY"},{"85", "UNACCEPTABLE_SETTLING_COUNTERPARTY"}], "TagNum" => "452"}


,
"NoPartyIDs" => #{"TagNum" => "453" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "453" => #{"Name"=>"NoPartyIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "453"}


,
"NoSecurityAltID" => #{"TagNum" => "454" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "454" => #{"Name"=>"NoSecurityAltID" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "454"}


,
"SecurityAltID" => #{"TagNum" => "455" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "455" => #{"Name"=>"SecurityAltID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "455"}


,
"SecurityAltIDSource" => #{"TagNum" => "456" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "456" => #{"Name"=>"SecurityAltIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "456"}


,
"NoUnderlyingSecurityAltID" => #{"TagNum" => "457" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "457" => #{"Name"=>"NoUnderlyingSecurityAltID" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "457"}


,
"UnderlyingSecurityAltID" => #{"TagNum" => "458" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "458" => #{"Name"=>"UnderlyingSecurityAltID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "458"}


,
"UnderlyingSecurityAltIDSource" => #{"TagNum" => "459" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "459" => #{"Name"=>"UnderlyingSecurityAltIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "459"}


,
"Product" => #{"TagNum" => "460" ,"Type" => "INT" ,"ValidValues" =>[{"1", "AGENCY"},{"2", "COMMODITY"},{"3", "CORPORATE"},{"4", "CURRENCY"},{"5", "EQUITY"},{"6", "GOVERNMENT"},{"7", "INDEX"},{"8", "LOAN"},{"9", "MONEYMARKET"},{"10", "MORTGAGE"},{"11", "MUNICIPAL"},{"12", "OTHER"},{"13", "FINANCING"}]}
, "460" => #{"Name"=>"Product" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "AGENCY"},{"2", "COMMODITY"},{"3", "CORPORATE"},{"4", "CURRENCY"},{"5", "EQUITY"},{"6", "GOVERNMENT"},{"7", "INDEX"},{"8", "LOAN"},{"9", "MONEYMARKET"},{"10", "MORTGAGE"},{"11", "MUNICIPAL"},{"12", "OTHER"},{"13", "FINANCING"}], "TagNum" => "460"}


,
"CFICode" => #{"TagNum" => "461" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "461" => #{"Name"=>"CFICode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "461"}


,
"UnderlyingProduct" => #{"TagNum" => "462" ,"Type" => "INT" ,"ValidValues" =>[]}
, "462" => #{"Name"=>"UnderlyingProduct" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "462"}


,
"UnderlyingCFICode" => #{"TagNum" => "463" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "463" => #{"Name"=>"UnderlyingCFICode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "463"}


,
"TestMessageIndicator" => #{"TagNum" => "464" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "464" => #{"Name"=>"TestMessageIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "464"}


,
"BookingRefID" => #{"TagNum" => "466" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "466" => #{"Name"=>"BookingRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "466"}


,
"IndividualAllocID" => #{"TagNum" => "467" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "467" => #{"Name"=>"IndividualAllocID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "467"}


,
"RoundingDirection" => #{"TagNum" => "468" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "ROUND_TO_NEAREST"},{"1", "ROUND_DOWN"},{"2", "ROUND_UP"}]}
, "468" => #{"Name"=>"RoundingDirection" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "ROUND_TO_NEAREST"},{"1", "ROUND_DOWN"},{"2", "ROUND_UP"}], "TagNum" => "468"}


,
"RoundingModulus" => #{"TagNum" => "469" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "469" => #{"Name"=>"RoundingModulus" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "469"}


,
"CountryOfIssue" => #{"TagNum" => "470" ,"Type" => "COUNTRY" ,"ValidValues" =>[]}
, "470" => #{"Name"=>"CountryOfIssue" ,"Type"=>"COUNTRY" ,"ValidValues"=>[], "TagNum" => "470"}


,
"StateOrProvinceOfIssue" => #{"TagNum" => "471" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "471" => #{"Name"=>"StateOrProvinceOfIssue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "471"}


,
"LocaleOfIssue" => #{"TagNum" => "472" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "472" => #{"Name"=>"LocaleOfIssue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "472"}


,
"NoRegistDtls" => #{"TagNum" => "473" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "473" => #{"Name"=>"NoRegistDtls" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "473"}


,
"MailingDtls" => #{"TagNum" => "474" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "474" => #{"Name"=>"MailingDtls" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "474"}


,
"InvestorCountryOfResidence" => #{"TagNum" => "475" ,"Type" => "COUNTRY" ,"ValidValues" =>[]}
, "475" => #{"Name"=>"InvestorCountryOfResidence" ,"Type"=>"COUNTRY" ,"ValidValues"=>[], "TagNum" => "475"}


,
"PaymentRef" => #{"TagNum" => "476" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "476" => #{"Name"=>"PaymentRef" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "476"}


,
"DistribPaymentMethod" => #{"TagNum" => "477" ,"Type" => "INT" ,"ValidValues" =>[{"1", "CREST"},{"2", "NSCC"},{"3", "EUROCLEAR"},{"4", "CLEARSTREAM"},{"5", "CHEQUE"},{"6", "TELEGRAPHIC_TRANSFER"},{"7", "FED_WIRE"},{"8", "DIRECT_CREDIT"},{"9", "ACH_CREDIT"},{"10", "BPAY"},{"11", "HIGH_VALUE_CLEARING_SYSTEM_HVACS"},{"12", "REINVEST_IN_FUND"}]}
, "477" => #{"Name"=>"DistribPaymentMethod" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "CREST"},{"2", "NSCC"},{"3", "EUROCLEAR"},{"4", "CLEARSTREAM"},{"5", "CHEQUE"},{"6", "TELEGRAPHIC_TRANSFER"},{"7", "FED_WIRE"},{"8", "DIRECT_CREDIT"},{"9", "ACH_CREDIT"},{"10", "BPAY"},{"11", "HIGH_VALUE_CLEARING_SYSTEM_HVACS"},{"12", "REINVEST_IN_FUND"}], "TagNum" => "477"}


,
"CashDistribCurr" => #{"TagNum" => "478" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "478" => #{"Name"=>"CashDistribCurr" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "478"}


,
"CommCurrency" => #{"TagNum" => "479" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "479" => #{"Name"=>"CommCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "479"}


,
"CancellationRights" => #{"TagNum" => "480" ,"Type" => "CHAR" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO_N"},{"M", "NO_M"},{"O", "NO_O"}]}
, "480" => #{"Name"=>"CancellationRights" ,"Type"=>"CHAR" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO_N"},{"M", "NO_M"},{"O", "NO_O"}], "TagNum" => "480"}


,
"MoneyLaunderingStatus" => #{"TagNum" => "481" ,"Type" => "CHAR" ,"ValidValues" =>[{"Y", "PASSED"},{"N", "NOT_CHECKED"},{"1", "EXEMPT_1"},{"2", "EXEMPT_2"},{"3", "EXEMPT_3"}]}
, "481" => #{"Name"=>"MoneyLaunderingStatus" ,"Type"=>"CHAR" ,"ValidValues"=>[{"Y", "PASSED"},{"N", "NOT_CHECKED"},{"1", "EXEMPT_1"},{"2", "EXEMPT_2"},{"3", "EXEMPT_3"}], "TagNum" => "481"}


,
"MailingInst" => #{"TagNum" => "482" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "482" => #{"Name"=>"MailingInst" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "482"}


,
"TransBkdTime" => #{"TagNum" => "483" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "483" => #{"Name"=>"TransBkdTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "483"}


,
"ExecPriceType" => #{"TagNum" => "484" ,"Type" => "CHAR" ,"ValidValues" =>[{"B", "BID_PRICE"},{"C", "CREATION_PRICE"},{"D", "CREATION_PRICE_PLUS_ADJUSTMENT_PERCENT"},{"E", "CREATION_PRICE_PLUS_ADJUSTMENT_AMOUNT"},{"O", "OFFER_PRICE"},{"P", "OFFER_PRICE_MINUS_ADJUSTMENT_PERCENT"},{"Q", "OFFER_PRICE_MINUS_ADJUSTMENT_AMOUNT"},{"S", "SINGLE_PRICE"}]}
, "484" => #{"Name"=>"ExecPriceType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"B", "BID_PRICE"},{"C", "CREATION_PRICE"},{"D", "CREATION_PRICE_PLUS_ADJUSTMENT_PERCENT"},{"E", "CREATION_PRICE_PLUS_ADJUSTMENT_AMOUNT"},{"O", "OFFER_PRICE"},{"P", "OFFER_PRICE_MINUS_ADJUSTMENT_PERCENT"},{"Q", "OFFER_PRICE_MINUS_ADJUSTMENT_AMOUNT"},{"S", "SINGLE_PRICE"}], "TagNum" => "484"}


,
"ExecPriceAdjustment" => #{"TagNum" => "485" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "485" => #{"Name"=>"ExecPriceAdjustment" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "485"}


,
"DateOfBirth" => #{"TagNum" => "486" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "486" => #{"Name"=>"DateOfBirth" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "486"}


,
"TradeReportTransType" => #{"TagNum" => "487" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NEW"},{"1", "CANCEL"},{"2", "REPLACE"},{"3", "RELEASE"},{"4", "REVERSE"},{"5", "CANCEL_DUE_TO_BACK_OUT_OF_TRADE"}]}
, "487" => #{"Name"=>"TradeReportTransType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NEW"},{"1", "CANCEL"},{"2", "REPLACE"},{"3", "RELEASE"},{"4", "REVERSE"},{"5", "CANCEL_DUE_TO_BACK_OUT_OF_TRADE"}], "TagNum" => "487"}


,
"CardHolderName" => #{"TagNum" => "488" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "488" => #{"Name"=>"CardHolderName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "488"}


,
"CardNumber" => #{"TagNum" => "489" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "489" => #{"Name"=>"CardNumber" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "489"}


,
"CardExpDate" => #{"TagNum" => "490" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "490" => #{"Name"=>"CardExpDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "490"}


,
"CardIssNum" => #{"TagNum" => "491" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "491" => #{"Name"=>"CardIssNum" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "491"}


,
"PaymentMethod" => #{"TagNum" => "492" ,"Type" => "INT" ,"ValidValues" =>[{"1", "CREST"},{"2", "NSCC"},{"3", "EUROCLEAR"},{"4", "CLEARSTREAM"},{"5", "CHEQUE"},{"6", "TELEGRAPHIC_TRANSFER"},{"7", "FED_WIRE"},{"8", "DEBIT_CARD"},{"9", "DIRECT_DEBIT"},{"10", "DIRECT_CREDIT"},{"11", "CREDIT_CARD"},{"12", "ACH_DEBIT"},{"13", "ACH_CREDIT"},{"14", "BPAY"},{"15", "HIGH_VALUE_CLEARING_SYSTEM"}]}
, "492" => #{"Name"=>"PaymentMethod" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "CREST"},{"2", "NSCC"},{"3", "EUROCLEAR"},{"4", "CLEARSTREAM"},{"5", "CHEQUE"},{"6", "TELEGRAPHIC_TRANSFER"},{"7", "FED_WIRE"},{"8", "DEBIT_CARD"},{"9", "DIRECT_DEBIT"},{"10", "DIRECT_CREDIT"},{"11", "CREDIT_CARD"},{"12", "ACH_DEBIT"},{"13", "ACH_CREDIT"},{"14", "BPAY"},{"15", "HIGH_VALUE_CLEARING_SYSTEM"}], "TagNum" => "492"}


,
"RegistAcctType" => #{"TagNum" => "493" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "493" => #{"Name"=>"RegistAcctType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "493"}


,
"Designation" => #{"TagNum" => "494" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "494" => #{"Name"=>"Designation" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "494"}


,
"TaxAdvantageType" => #{"TagNum" => "495" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NONE_NOT_APPLICABLE"},{"1", "MAXI_ISA"},{"2", "TESSA"},{"3", "MINI_CASH_ISA"},{"4", "MINI_STOCKS_AND_SHARES_ISA"},{"5", "MINI_INSURANCE_ISA"},{"6", "CURRENT_YEAR_PAYMENT"},{"7", "PRIOR_YEAR_PAYMENT"},{"8", "ASSET_TRANSFER"},{"9", "EMPLOYEE_9"},{"10", "EMPLOYEE_10"},{"11", "EMPLOYER_11"},{"12", "EMPLOYER_12"},{"13", "NON_FUND_PROTOTYPE_IRA"},{"14", "NON_FUND_QUALIFIED_PLAN"},{"15", "DEFINED_CONTRIBUTION_PLAN"},{"16", "INDIVIDUAL_RETIREMENT_ACCOUNT_16"},{"17", "INDIVIDUAL_RETIREMENT_ACCOUNT_17"},{"18", "KEOGH"},{"19", "PROFIT_SHARING_PLAN"},{"20", "401"},{"21", "SELF_DIRECTED_IRA"},{"22", "403"},{"23", "457"},{"24", "ROTH_IRA_24"},{"25", "ROTH_IRA_25"},{"26", "ROTH_CONVERSION_IRA_26"},{"27", "ROTH_CONVERSION_IRA_27"},{"28", "EDUCATION_IRA_28"},{"29", "EDUCATION_IRA_29"},{"999", "OTHER"}]}
, "495" => #{"Name"=>"TaxAdvantageType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NONE_NOT_APPLICABLE"},{"1", "MAXI_ISA"},{"2", "TESSA"},{"3", "MINI_CASH_ISA"},{"4", "MINI_STOCKS_AND_SHARES_ISA"},{"5", "MINI_INSURANCE_ISA"},{"6", "CURRENT_YEAR_PAYMENT"},{"7", "PRIOR_YEAR_PAYMENT"},{"8", "ASSET_TRANSFER"},{"9", "EMPLOYEE_9"},{"10", "EMPLOYEE_10"},{"11", "EMPLOYER_11"},{"12", "EMPLOYER_12"},{"13", "NON_FUND_PROTOTYPE_IRA"},{"14", "NON_FUND_QUALIFIED_PLAN"},{"15", "DEFINED_CONTRIBUTION_PLAN"},{"16", "INDIVIDUAL_RETIREMENT_ACCOUNT_16"},{"17", "INDIVIDUAL_RETIREMENT_ACCOUNT_17"},{"18", "KEOGH"},{"19", "PROFIT_SHARING_PLAN"},{"20", "401"},{"21", "SELF_DIRECTED_IRA"},{"22", "403"},{"23", "457"},{"24", "ROTH_IRA_24"},{"25", "ROTH_IRA_25"},{"26", "ROTH_CONVERSION_IRA_26"},{"27", "ROTH_CONVERSION_IRA_27"},{"28", "EDUCATION_IRA_28"},{"29", "EDUCATION_IRA_29"},{"999", "OTHER"}], "TagNum" => "495"}


,
"RegistRejReasonText" => #{"TagNum" => "496" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "496" => #{"Name"=>"RegistRejReasonText" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "496"}


,
"FundRenewWaiv" => #{"TagNum" => "497" ,"Type" => "CHAR" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "497" => #{"Name"=>"FundRenewWaiv" ,"Type"=>"CHAR" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "497"}


,
"CashDistribAgentName" => #{"TagNum" => "498" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "498" => #{"Name"=>"CashDistribAgentName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "498"}


,
"CashDistribAgentCode" => #{"TagNum" => "499" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "499" => #{"Name"=>"CashDistribAgentCode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "499"}


,
"CashDistribAgentAcctNumber" => #{"TagNum" => "500" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "500" => #{"Name"=>"CashDistribAgentAcctNumber" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "500"}


,
"CashDistribPayRef" => #{"TagNum" => "501" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "501" => #{"Name"=>"CashDistribPayRef" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "501"}


,
"CashDistribAgentAcctName" => #{"TagNum" => "502" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "502" => #{"Name"=>"CashDistribAgentAcctName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "502"}


,
"CardStartDate" => #{"TagNum" => "503" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "503" => #{"Name"=>"CardStartDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "503"}


,
"PaymentDate" => #{"TagNum" => "504" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "504" => #{"Name"=>"PaymentDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "504"}


,
"PaymentRemitterID" => #{"TagNum" => "505" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "505" => #{"Name"=>"PaymentRemitterID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "505"}


,
"RegistStatus" => #{"TagNum" => "506" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "ACCEPTED"},{"R", "REJECTED"},{"H", "HELD"},{"N", "REMINDER"}]}
, "506" => #{"Name"=>"RegistStatus" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "ACCEPTED"},{"R", "REJECTED"},{"H", "HELD"},{"N", "REMINDER"}], "TagNum" => "506"}


,
"RegistRejReasonCode" => #{"TagNum" => "507" ,"Type" => "INT" ,"ValidValues" =>[{"1", "INVALID_UNACCEPTABLE_ACCOUNT_TYPE"},{"2", "INVALID_UNACCEPTABLE_TAX_EXEMPT_TYPE"},{"3", "INVALID_UNACCEPTABLE_OWNERSHIP_TYPE"},{"4", "INVALID_UNACCEPTABLE_NO_REG_DETAILS"},{"5", "INVALID_UNACCEPTABLE_REG_SEQ_NO"},{"6", "INVALID_UNACCEPTABLE_REG_DETAILS"},{"7", "INVALID_UNACCEPTABLE_MAILING_DETAILS"},{"8", "INVALID_UNACCEPTABLE_MAILING_INSTRUCTIONS"},{"9", "INVALID_UNACCEPTABLE_INVESTOR_ID"},{"10", "INVALID_UNACEEPTABLE_INVESTOR_ID_SOURCE"},{"11", "INVALID_UNACCEPTABLE_DATE_OF_BIRTH"},{"12", "INVALID_UNACCEPTABLE_INVESTOR_COUNTRY_OF_RESIDENCE"},{"13", "INVALID_UNACCEPTABLE_NO_DISTRIB_INSTNS"},{"14", "INVALID_UNACCEPTABLE_DISTRIB_PERCENTAGE"},{"15", "INVALID_UNACCEPTABLE_DISTRIB_PAYMENT_METHOD"},{"16", "INVALID_UNACCEPTABLE_CASH_DISTRIB_AGENT_ACCT_NAME"},{"17", "INVALID_UNACCEPTABLE_CASH_DISTRIB_AGENT_CODE"},{"18", "INVALID_UNACCEPTABLE_CASH_DISTRIB_AGENT_ACCT_NUM"},{"99", "OTHER"}]}
, "507" => #{"Name"=>"RegistRejReasonCode" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "INVALID_UNACCEPTABLE_ACCOUNT_TYPE"},{"2", "INVALID_UNACCEPTABLE_TAX_EXEMPT_TYPE"},{"3", "INVALID_UNACCEPTABLE_OWNERSHIP_TYPE"},{"4", "INVALID_UNACCEPTABLE_NO_REG_DETAILS"},{"5", "INVALID_UNACCEPTABLE_REG_SEQ_NO"},{"6", "INVALID_UNACCEPTABLE_REG_DETAILS"},{"7", "INVALID_UNACCEPTABLE_MAILING_DETAILS"},{"8", "INVALID_UNACCEPTABLE_MAILING_INSTRUCTIONS"},{"9", "INVALID_UNACCEPTABLE_INVESTOR_ID"},{"10", "INVALID_UNACEEPTABLE_INVESTOR_ID_SOURCE"},{"11", "INVALID_UNACCEPTABLE_DATE_OF_BIRTH"},{"12", "INVALID_UNACCEPTABLE_INVESTOR_COUNTRY_OF_RESIDENCE"},{"13", "INVALID_UNACCEPTABLE_NO_DISTRIB_INSTNS"},{"14", "INVALID_UNACCEPTABLE_DISTRIB_PERCENTAGE"},{"15", "INVALID_UNACCEPTABLE_DISTRIB_PAYMENT_METHOD"},{"16", "INVALID_UNACCEPTABLE_CASH_DISTRIB_AGENT_ACCT_NAME"},{"17", "INVALID_UNACCEPTABLE_CASH_DISTRIB_AGENT_CODE"},{"18", "INVALID_UNACCEPTABLE_CASH_DISTRIB_AGENT_ACCT_NUM"},{"99", "OTHER"}], "TagNum" => "507"}


,
"RegistRefID" => #{"TagNum" => "508" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "508" => #{"Name"=>"RegistRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "508"}


,
"RegistDtls" => #{"TagNum" => "509" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "509" => #{"Name"=>"RegistDtls" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "509"}


,
"NoDistribInsts" => #{"TagNum" => "510" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "510" => #{"Name"=>"NoDistribInsts" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "510"}


,
"RegistEmail" => #{"TagNum" => "511" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "511" => #{"Name"=>"RegistEmail" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "511"}


,
"DistribPercentage" => #{"TagNum" => "512" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "512" => #{"Name"=>"DistribPercentage" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "512"}


,
"RegistID" => #{"TagNum" => "513" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "513" => #{"Name"=>"RegistID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "513"}


,
"RegistTransType" => #{"TagNum" => "514" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NEW"},{"2", "CANCEL"},{"1", "REPLACE"}]}
, "514" => #{"Name"=>"RegistTransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NEW"},{"2", "CANCEL"},{"1", "REPLACE"}], "TagNum" => "514"}


,
"ExecValuationPoint" => #{"TagNum" => "515" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "515" => #{"Name"=>"ExecValuationPoint" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "515"}


,
"OrderPercent" => #{"TagNum" => "516" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "516" => #{"Name"=>"OrderPercent" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "516"}


,
"OwnershipType" => #{"TagNum" => "517" ,"Type" => "CHAR" ,"ValidValues" =>[{"J", "JOINT_INVESTORS"},{"T", "TENANTS_IN_COMMON"},{"2", "JOINT_TRUSTEES"}]}
, "517" => #{"Name"=>"OwnershipType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"J", "JOINT_INVESTORS"},{"T", "TENANTS_IN_COMMON"},{"2", "JOINT_TRUSTEES"}], "TagNum" => "517"}


,
"NoContAmts" => #{"TagNum" => "518" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "518" => #{"Name"=>"NoContAmts" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "518"}


,
"ContAmtType" => #{"TagNum" => "519" ,"Type" => "INT" ,"ValidValues" =>[{"1", "COMMISSION_AMOUNT"},{"2", "COMMISSION_PERCENT"},{"3", "INITIAL_CHARGE_AMOUNT"},{"4", "INITIAL_CHARGE_PERCENT"},{"5", "DISCOUNT_AMOUNT"},{"6", "DISCOUNT_PERCENT"},{"7", "DILUTION_LEVY_AMOUNT"},{"8", "DILUTION_LEVY_PERCENT"},{"9", "EXIT_CHARGE_AMOUNT"},{"10", "EXIT_CHARGE_PERCENT"},{"11", "FUND_BASED_RENEWAL_COMMISSION_PERCENT"},{"12", "PROJECTED_FUND_VALUE"},{"13", "FUND_BASED_RENEWAL_COMMISSION_AMOUNT_13"},{"14", "FUND_BASED_RENEWAL_COMMISSION_AMOUNT_14"},{"15", "NET_SETTLEMENT_AMOUNT"}]}
, "519" => #{"Name"=>"ContAmtType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "COMMISSION_AMOUNT"},{"2", "COMMISSION_PERCENT"},{"3", "INITIAL_CHARGE_AMOUNT"},{"4", "INITIAL_CHARGE_PERCENT"},{"5", "DISCOUNT_AMOUNT"},{"6", "DISCOUNT_PERCENT"},{"7", "DILUTION_LEVY_AMOUNT"},{"8", "DILUTION_LEVY_PERCENT"},{"9", "EXIT_CHARGE_AMOUNT"},{"10", "EXIT_CHARGE_PERCENT"},{"11", "FUND_BASED_RENEWAL_COMMISSION_PERCENT"},{"12", "PROJECTED_FUND_VALUE"},{"13", "FUND_BASED_RENEWAL_COMMISSION_AMOUNT_13"},{"14", "FUND_BASED_RENEWAL_COMMISSION_AMOUNT_14"},{"15", "NET_SETTLEMENT_AMOUNT"}], "TagNum" => "519"}


,
"ContAmtValue" => #{"TagNum" => "520" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "520" => #{"Name"=>"ContAmtValue" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "520"}


,
"ContAmtCurr" => #{"TagNum" => "521" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "521" => #{"Name"=>"ContAmtCurr" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "521"}


,
"OwnerType" => #{"TagNum" => "522" ,"Type" => "INT" ,"ValidValues" =>[{"1", "INDIVIDUAL_INVESTOR"},{"2", "PUBLIC_COMPANY"},{"3", "PRIVATE_COMPANY"},{"4", "INDIVIDUAL_TRUSTEE"},{"5", "COMPANY_TRUSTEE"},{"6", "PENSION_PLAN"},{"7", "CUSTODIAN_UNDER_GIFTS_TO_MINORS_ACT"},{"8", "TRUSTS"},{"9", "FIDUCIARIES"},{"10", "NETWORKING_SUB_ACCOUNT"},{"11", "NON_PROFIT_ORGANIZATION"},{"12", "CORPORATE_BODY"},{"13", "NOMINEE"}]}
, "522" => #{"Name"=>"OwnerType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "INDIVIDUAL_INVESTOR"},{"2", "PUBLIC_COMPANY"},{"3", "PRIVATE_COMPANY"},{"4", "INDIVIDUAL_TRUSTEE"},{"5", "COMPANY_TRUSTEE"},{"6", "PENSION_PLAN"},{"7", "CUSTODIAN_UNDER_GIFTS_TO_MINORS_ACT"},{"8", "TRUSTS"},{"9", "FIDUCIARIES"},{"10", "NETWORKING_SUB_ACCOUNT"},{"11", "NON_PROFIT_ORGANIZATION"},{"12", "CORPORATE_BODY"},{"13", "NOMINEE"}], "TagNum" => "522"}


,
"PartySubID" => #{"TagNum" => "523" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "523" => #{"Name"=>"PartySubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "523"}


,
"NestedPartyID" => #{"TagNum" => "524" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "524" => #{"Name"=>"NestedPartyID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "524"}


,
"NestedPartyIDSource" => #{"TagNum" => "525" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "525" => #{"Name"=>"NestedPartyIDSource" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "525"}


,
"SecondaryClOrdID" => #{"TagNum" => "526" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "526" => #{"Name"=>"SecondaryClOrdID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "526"}


,
"SecondaryExecID" => #{"TagNum" => "527" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "527" => #{"Name"=>"SecondaryExecID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "527"}


,
"OrderCapacity" => #{"TagNum" => "528" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "AGENCY"},{"G", "PROPRIETARY"},{"I", "INDIVIDUAL"},{"P", "PRINCIPAL"},{"R", "RISKLESS_PRINCIPAL"},{"W", "AGENT_FOR_OTHER_MEMBER"}]}
, "528" => #{"Name"=>"OrderCapacity" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "AGENCY"},{"G", "PROPRIETARY"},{"I", "INDIVIDUAL"},{"P", "PRINCIPAL"},{"R", "RISKLESS_PRINCIPAL"},{"W", "AGENT_FOR_OTHER_MEMBER"}], "TagNum" => "528"}


,
"OrderRestrictions" => #{"TagNum" => "529" ,"Type" => "MULTIPLECHARVALUE" ,"ValidValues" =>[{"1", "PROGRAM_TRADE"},{"2", "INDEX_ARBITRAGE"},{"3", "NON_INDEX_ARBITRAGE"},{"4", "COMPETING_MARKET_MAKER"},{"5", "ACTING_AS_MARKET_MAKER_OR_SPECIALIST_IN_THE_SECURITY"},{"6", "ACTING_AS_MARKET_MAKER_OR_SPECIALIST_IN_THE_UNDERLYING_SECURITY_OF_A_DERIVATIVE_SECURITY"},{"7", "FOREIGN_ENTITY"},{"8", "EXTERNAL_MARKET_PARTICIPANT"},{"9", "EXTERNAL_INTER_CONNECTED_MARKET_LINKAGE"},{"A", "RISKLESS_ARBITRAGE"},{"B", "ISSUER_HOLDING"},{"C", "ISSUE_PRICE_STABILIZATION"},{"D", "NON_ALGORITHMIC"},{"E", "ALGORITHMIC"},{"F", "CROSS"}]}
, "529" => #{"Name"=>"OrderRestrictions" ,"Type"=>"MULTIPLECHARVALUE" ,"ValidValues"=>[{"1", "PROGRAM_TRADE"},{"2", "INDEX_ARBITRAGE"},{"3", "NON_INDEX_ARBITRAGE"},{"4", "COMPETING_MARKET_MAKER"},{"5", "ACTING_AS_MARKET_MAKER_OR_SPECIALIST_IN_THE_SECURITY"},{"6", "ACTING_AS_MARKET_MAKER_OR_SPECIALIST_IN_THE_UNDERLYING_SECURITY_OF_A_DERIVATIVE_SECURITY"},{"7", "FOREIGN_ENTITY"},{"8", "EXTERNAL_MARKET_PARTICIPANT"},{"9", "EXTERNAL_INTER_CONNECTED_MARKET_LINKAGE"},{"A", "RISKLESS_ARBITRAGE"},{"B", "ISSUER_HOLDING"},{"C", "ISSUE_PRICE_STABILIZATION"},{"D", "NON_ALGORITHMIC"},{"E", "ALGORITHMIC"},{"F", "CROSS"}], "TagNum" => "529"}


,
"MassCancelRequestType" => #{"TagNum" => "530" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "CANCEL_ORDERS_FOR_A_SECURITY"},{"2", "CANCEL_ORDERS_FOR_AN_UNDERLYING_SECURITY"},{"3", "CANCEL_ORDERS_FOR_A_PRODUCT"},{"4", "CANCEL_ORDERS_FOR_A_CFICODE"},{"5", "CANCEL_ORDERS_FOR_A_SECURITYTYPE"},{"6", "CANCEL_ORDERS_FOR_A_TRADING_SESSION"},{"7", "CANCEL_ALL_ORDERS"},{"8", "CANCEL_ORDERS_FOR_A_MARKET"},{"9", "CANCEL_ORDERS_FOR_A_MARKET_SEGMENT"},{"A", "CANCEL_ORDERS_FOR_A_SECURITY_GROUP"},{"B", "CANCEL_FOR_SECURITY_ISSUER"},{"C", "CANCEL_FOR_ISSUER_OF_UNDERLYING_SECURITY"}]}
, "530" => #{"Name"=>"MassCancelRequestType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "CANCEL_ORDERS_FOR_A_SECURITY"},{"2", "CANCEL_ORDERS_FOR_AN_UNDERLYING_SECURITY"},{"3", "CANCEL_ORDERS_FOR_A_PRODUCT"},{"4", "CANCEL_ORDERS_FOR_A_CFICODE"},{"5", "CANCEL_ORDERS_FOR_A_SECURITYTYPE"},{"6", "CANCEL_ORDERS_FOR_A_TRADING_SESSION"},{"7", "CANCEL_ALL_ORDERS"},{"8", "CANCEL_ORDERS_FOR_A_MARKET"},{"9", "CANCEL_ORDERS_FOR_A_MARKET_SEGMENT"},{"A", "CANCEL_ORDERS_FOR_A_SECURITY_GROUP"},{"B", "CANCEL_FOR_SECURITY_ISSUER"},{"C", "CANCEL_FOR_ISSUER_OF_UNDERLYING_SECURITY"}], "TagNum" => "530"}


,
"MassCancelResponse" => #{"TagNum" => "531" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "CANCEL_REQUEST_REJECTED"},{"1", "CANCEL_ORDERS_FOR_A_SECURITY"},{"2", "CANCEL_ORDERS_FOR_AN_UNDERLYING_SECURITY"},{"3", "CANCEL_ORDERS_FOR_A_PRODUCT"},{"4", "CANCEL_ORDERS_FOR_A_CFICODE"},{"5", "CANCEL_ORDERS_FOR_A_SECURITYTYPE"},{"6", "CANCEL_ORDERS_FOR_A_TRADING_SESSION"},{"7", "CANCEL_ALL_ORDERS"},{"8", "CANCEL_ORDERS_FOR_A_MARKET"},{"9", "CANCEL_ORDERS_FOR_A_MARKET_SEGMENT"},{"A", "CANCEL_ORDERS_FOR_A_SECURITY_GROUP"},{"B", "CANCEL_ORDERS_FOR_A_SECURITIES_ISSUER"},{"C", "CANCEL_ORDERS_FOR_ISSUER_OF_UNDERLYING_SECURITY"}]}
, "531" => #{"Name"=>"MassCancelResponse" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "CANCEL_REQUEST_REJECTED"},{"1", "CANCEL_ORDERS_FOR_A_SECURITY"},{"2", "CANCEL_ORDERS_FOR_AN_UNDERLYING_SECURITY"},{"3", "CANCEL_ORDERS_FOR_A_PRODUCT"},{"4", "CANCEL_ORDERS_FOR_A_CFICODE"},{"5", "CANCEL_ORDERS_FOR_A_SECURITYTYPE"},{"6", "CANCEL_ORDERS_FOR_A_TRADING_SESSION"},{"7", "CANCEL_ALL_ORDERS"},{"8", "CANCEL_ORDERS_FOR_A_MARKET"},{"9", "CANCEL_ORDERS_FOR_A_MARKET_SEGMENT"},{"A", "CANCEL_ORDERS_FOR_A_SECURITY_GROUP"},{"B", "CANCEL_ORDERS_FOR_A_SECURITIES_ISSUER"},{"C", "CANCEL_ORDERS_FOR_ISSUER_OF_UNDERLYING_SECURITY"}], "TagNum" => "531"}


,
"MassCancelRejectReason" => #{"TagNum" => "532" ,"Type" => "INT" ,"ValidValues" =>[{"0", "MASS_CANCEL_NOT_SUPPORTED"},{"1", "INVALID_OR_UNKNOWN_SECURITY"},{"2", "INVALID_OR_UNKOWN_UNDERLYING_SECURITY"},{"3", "INVALID_OR_UNKNOWN_PRODUCT"},{"4", "INVALID_OR_UNKNOWN_CFICODE"},{"5", "INVALID_OR_UNKNOWN_SECURITYTYPE"},{"6", "INVALID_OR_UNKNOWN_TRADING_SESSION"},{"7", "INVALID_OR_UNKNOWN_MARKET"},{"8", "INVALID_OR_UNKOWN_MARKET_SEGMENT"},{"9", "INVALID_OR_UNKNOWN_SECURITY_GROUP"},{"99", "OTHER"},{"10", "INVALID_OR_UNKNOWN_SECURITY_ISSUER"},{"11", "INVALID_OR_UNKNOWN_ISSUER_OF_UNDERLYING_SECURITY"}]}
, "532" => #{"Name"=>"MassCancelRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "MASS_CANCEL_NOT_SUPPORTED"},{"1", "INVALID_OR_UNKNOWN_SECURITY"},{"2", "INVALID_OR_UNKOWN_UNDERLYING_SECURITY"},{"3", "INVALID_OR_UNKNOWN_PRODUCT"},{"4", "INVALID_OR_UNKNOWN_CFICODE"},{"5", "INVALID_OR_UNKNOWN_SECURITYTYPE"},{"6", "INVALID_OR_UNKNOWN_TRADING_SESSION"},{"7", "INVALID_OR_UNKNOWN_MARKET"},{"8", "INVALID_OR_UNKOWN_MARKET_SEGMENT"},{"9", "INVALID_OR_UNKNOWN_SECURITY_GROUP"},{"99", "OTHER"},{"10", "INVALID_OR_UNKNOWN_SECURITY_ISSUER"},{"11", "INVALID_OR_UNKNOWN_ISSUER_OF_UNDERLYING_SECURITY"}], "TagNum" => "532"}


,
"TotalAffectedOrders" => #{"TagNum" => "533" ,"Type" => "INT" ,"ValidValues" =>[]}
, "533" => #{"Name"=>"TotalAffectedOrders" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "533"}


,
"NoAffectedOrders" => #{"TagNum" => "534" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "534" => #{"Name"=>"NoAffectedOrders" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "534"}


,
"AffectedOrderID" => #{"TagNum" => "535" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "535" => #{"Name"=>"AffectedOrderID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "535"}


,
"AffectedSecondaryOrderID" => #{"TagNum" => "536" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "536" => #{"Name"=>"AffectedSecondaryOrderID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "536"}


,
"QuoteType" => #{"TagNum" => "537" ,"Type" => "INT" ,"ValidValues" =>[{"0", "INDICATIVE"},{"1", "TRADEABLE"},{"2", "RESTRICTED_TRADEABLE"},{"3", "COUNTER"}]}
, "537" => #{"Name"=>"QuoteType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "INDICATIVE"},{"1", "TRADEABLE"},{"2", "RESTRICTED_TRADEABLE"},{"3", "COUNTER"}], "TagNum" => "537"}


,
"NestedPartyRole" => #{"TagNum" => "538" ,"Type" => "INT" ,"ValidValues" =>[]}
, "538" => #{"Name"=>"NestedPartyRole" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "538"}


,
"NoNestedPartyIDs" => #{"TagNum" => "539" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "539" => #{"Name"=>"NoNestedPartyIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "539"}


,
"TotalAccruedInterestAmt" => #{"TagNum" => "540" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "540" => #{"Name"=>"TotalAccruedInterestAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "540"}


,
"MaturityDate" => #{"TagNum" => "541" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "541" => #{"Name"=>"MaturityDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "541"}


,
"UnderlyingMaturityDate" => #{"TagNum" => "542" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "542" => #{"Name"=>"UnderlyingMaturityDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "542"}


,
"InstrRegistry" => #{"TagNum" => "543" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "543" => #{"Name"=>"InstrRegistry" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "543"}


,
"CashMargin" => #{"TagNum" => "544" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "CASH"},{"2", "MARGIN_OPEN"},{"3", "MARGIN_CLOSE"}]}
, "544" => #{"Name"=>"CashMargin" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "CASH"},{"2", "MARGIN_OPEN"},{"3", "MARGIN_CLOSE"}], "TagNum" => "544"}


,
"NestedPartySubID" => #{"TagNum" => "545" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "545" => #{"Name"=>"NestedPartySubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "545"}


,
"Scope" => #{"TagNum" => "546" ,"Type" => "MULTIPLECHARVALUE" ,"ValidValues" =>[{"1", "LOCAL_MARKET"},{"2", "NATIONAL"},{"3", "GLOBAL"}]}
, "546" => #{"Name"=>"Scope" ,"Type"=>"MULTIPLECHARVALUE" ,"ValidValues"=>[{"1", "LOCAL_MARKET"},{"2", "NATIONAL"},{"3", "GLOBAL"}], "TagNum" => "546"}


,
"MDImplicitDelete" => #{"TagNum" => "547" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "547" => #{"Name"=>"MDImplicitDelete" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "547"}


,
"CrossID" => #{"TagNum" => "548" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "548" => #{"Name"=>"CrossID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "548"}


,
"CrossType" => #{"TagNum" => "549" ,"Type" => "INT" ,"ValidValues" =>[{"1", "CROSS_AON"},{"2", "CROSS_IOC"},{"3", "CROSS_ONE_SIDE"},{"4", "CROSS_SAME_PRICE"}]}
, "549" => #{"Name"=>"CrossType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "CROSS_AON"},{"2", "CROSS_IOC"},{"3", "CROSS_ONE_SIDE"},{"4", "CROSS_SAME_PRICE"}], "TagNum" => "549"}


,
"CrossPrioritization" => #{"TagNum" => "550" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NONE"},{"1", "BUY_SIDE_IS_PRIORITIZED"},{"2", "SELL_SIDE_IS_PRIORITIZED"}]}
, "550" => #{"Name"=>"CrossPrioritization" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NONE"},{"1", "BUY_SIDE_IS_PRIORITIZED"},{"2", "SELL_SIDE_IS_PRIORITIZED"}], "TagNum" => "550"}


,
"OrigCrossID" => #{"TagNum" => "551" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "551" => #{"Name"=>"OrigCrossID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "551"}


,
"NoSides" => #{"TagNum" => "552" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[{"1", "ONE_SIDE"},{"2", "BOTH_SIDES"}]}
, "552" => #{"Name"=>"NoSides" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[{"1", "ONE_SIDE"},{"2", "BOTH_SIDES"}], "TagNum" => "552"}


,
"Username" => #{"TagNum" => "553" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "553" => #{"Name"=>"Username" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "553"}


,
"Password" => #{"TagNum" => "554" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "554" => #{"Name"=>"Password" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "554"}


,
"NoLegs" => #{"TagNum" => "555" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "555" => #{"Name"=>"NoLegs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "555"}


,
"LegCurrency" => #{"TagNum" => "556" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "556" => #{"Name"=>"LegCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "556"}


,
"TotNoSecurityTypes" => #{"TagNum" => "557" ,"Type" => "INT" ,"ValidValues" =>[]}
, "557" => #{"Name"=>"TotNoSecurityTypes" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "557"}


,
"NoSecurityTypes" => #{"TagNum" => "558" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "558" => #{"Name"=>"NoSecurityTypes" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "558"}


,
"SecurityListRequestType" => #{"TagNum" => "559" ,"Type" => "INT" ,"ValidValues" =>[{"0", "SYMBOL"},{"1", "SECURITYTYPE_AND_OR_CFICODE"},{"2", "PRODUCT"},{"3", "TRADINGSESSIONID"},{"4", "ALL_SECURITIES"},{"5", "MARKETID_OR_MARKETID_PLUS_MARKETSEGMENTID"}]}
, "559" => #{"Name"=>"SecurityListRequestType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "SYMBOL"},{"1", "SECURITYTYPE_AND_OR_CFICODE"},{"2", "PRODUCT"},{"3", "TRADINGSESSIONID"},{"4", "ALL_SECURITIES"},{"5", "MARKETID_OR_MARKETID_PLUS_MARKETSEGMENTID"}], "TagNum" => "559"}


,
"SecurityRequestResult" => #{"TagNum" => "560" ,"Type" => "INT" ,"ValidValues" =>[{"0", "VALID_REQUEST"},{"1", "INVALID_OR_UNSUPPORTED_REQUEST"},{"2", "NO_INSTRUMENTS_FOUND_THAT_MATCH_SELECTION_CRITERIA"},{"3", "NOT_AUTHORIZED_TO_RETRIEVE_INSTRUMENT_DATA"},{"4", "INSTRUMENT_DATA_TEMPORARILY_UNAVAILABLE"},{"5", "REQUEST_FOR_INSTRUMENT_DATA_NOT_SUPPORTED"}]}
, "560" => #{"Name"=>"SecurityRequestResult" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "VALID_REQUEST"},{"1", "INVALID_OR_UNSUPPORTED_REQUEST"},{"2", "NO_INSTRUMENTS_FOUND_THAT_MATCH_SELECTION_CRITERIA"},{"3", "NOT_AUTHORIZED_TO_RETRIEVE_INSTRUMENT_DATA"},{"4", "INSTRUMENT_DATA_TEMPORARILY_UNAVAILABLE"},{"5", "REQUEST_FOR_INSTRUMENT_DATA_NOT_SUPPORTED"}], "TagNum" => "560"}


,
"RoundLot" => #{"TagNum" => "561" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "561" => #{"Name"=>"RoundLot" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "561"}


,
"MinTradeVol" => #{"TagNum" => "562" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "562" => #{"Name"=>"MinTradeVol" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "562"}


,
"MultiLegRptTypeReq" => #{"TagNum" => "563" ,"Type" => "INT" ,"ValidValues" =>[{"0", "REPORT_BY_MULITLEG_SECURITY_ONLY"},{"1", "REPORT_BY_MULTILEG_SECURITY_AND_BY_INSTRUMENT_LEGS_BELONGING_TO_THE_MULTILEG_SECURITY"},{"2", "REPORT_BY_INSTRUMENT_LEGS_BELONGING_TO_THE_MULTILEG_SECURITY_ONLY"}]}
, "563" => #{"Name"=>"MultiLegRptTypeReq" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "REPORT_BY_MULITLEG_SECURITY_ONLY"},{"1", "REPORT_BY_MULTILEG_SECURITY_AND_BY_INSTRUMENT_LEGS_BELONGING_TO_THE_MULTILEG_SECURITY"},{"2", "REPORT_BY_INSTRUMENT_LEGS_BELONGING_TO_THE_MULTILEG_SECURITY_ONLY"}], "TagNum" => "563"}


,
"LegPositionEffect" => #{"TagNum" => "564" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "564" => #{"Name"=>"LegPositionEffect" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "564"}


,
"LegCoveredOrUncovered" => #{"TagNum" => "565" ,"Type" => "INT" ,"ValidValues" =>[]}
, "565" => #{"Name"=>"LegCoveredOrUncovered" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "565"}


,
"LegPrice" => #{"TagNum" => "566" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "566" => #{"Name"=>"LegPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "566"}


,
"TradSesStatusRejReason" => #{"TagNum" => "567" ,"Type" => "INT" ,"ValidValues" =>[{"1", "UNKNOWN_OR_INVALID_TRADINGSESSIONID"},{"99", "OTHER"}]}
, "567" => #{"Name"=>"TradSesStatusRejReason" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "UNKNOWN_OR_INVALID_TRADINGSESSIONID"},{"99", "OTHER"}], "TagNum" => "567"}


,
"TradeRequestID" => #{"TagNum" => "568" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "568" => #{"Name"=>"TradeRequestID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "568"}


,
"TradeRequestType" => #{"TagNum" => "569" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ALL_TRADES"},{"1", "MATCHED_TRADES_MATCHING_CRITERIA_PROVIDED_ON_REQUEST"},{"2", "UNMATCHED_TRADES_THAT_MATCH_CRITERIA"},{"3", "UNREPORTED_TRADES_THAT_MATCH_CRITERIA"},{"4", "ADVISORIES_THAT_MATCH_CRITERIA"}]}
, "569" => #{"Name"=>"TradeRequestType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ALL_TRADES"},{"1", "MATCHED_TRADES_MATCHING_CRITERIA_PROVIDED_ON_REQUEST"},{"2", "UNMATCHED_TRADES_THAT_MATCH_CRITERIA"},{"3", "UNREPORTED_TRADES_THAT_MATCH_CRITERIA"},{"4", "ADVISORIES_THAT_MATCH_CRITERIA"}], "TagNum" => "569"}


,
"PreviouslyReported" => #{"TagNum" => "570" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "570" => #{"Name"=>"PreviouslyReported" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "570"}


,
"TradeReportID" => #{"TagNum" => "571" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "571" => #{"Name"=>"TradeReportID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "571"}


,
"TradeReportRefID" => #{"TagNum" => "572" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "572" => #{"Name"=>"TradeReportRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "572"}


,
"MatchStatus" => #{"TagNum" => "573" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "COMPARED_MATCHED_OR_AFFIRMED"},{"1", "UNCOMPARED_UNMATCHED_OR_UNAFFIRMED"},{"2", "ADVISORY_OR_ALERT"}]}
, "573" => #{"Name"=>"MatchStatus" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "COMPARED_MATCHED_OR_AFFIRMED"},{"1", "UNCOMPARED_UNMATCHED_OR_UNAFFIRMED"},{"2", "ADVISORY_OR_ALERT"}], "TagNum" => "573"}


,
"MatchType" => #{"TagNum" => "574" ,"Type" => "STRING" ,"ValidValues" =>[{"1", "ONE_PARTY_TRADE_REPORT"},{"2", "TWO_PARTY_TRADE_REPORT"},{"3", "CONFIRMED_TRADE_REPORT"},{"4", "AUTO_MATCH"},{"5", "CROSS_AUCTION"},{"6", "COUNTER_ORDER_SELECTION"},{"7", "CALL_AUCTION"},{"8", "ISSUING_BUY_BACK_AUCTION"},{"M3", "ACT_ACCEPTED_TRADE"},{"M4", "ACT_DEFAULT_TRADE"},{"M5", "ACT_DEFAULT_AFTER_M2"},{"M6", "ACT_M6_MATCH"},{"A1", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_FOUR_BADGES_AND_EXECUTION_TIME"},{"A2", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_FOUR_BADGES"},{"A3", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_TWO_BADGES_AND_EXECUTION_TIME"},{"A4", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_TWO_BADGES"},{"A5", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADETYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_EXECUTION_TIME"},{"AQ", "COMPARED_RECORDS_RESULTING_FROM_STAMPED_ADVISORIES_OR_SPECIALIST_ACCEPTS_PAIR_OFFS"},{"S1", "SUMMARIZED_MATCH_USING_A1_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIED"},{"S2", "SUMMARIZED_MATCH_USING_A2_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED"},{"S3", "SUMMARIZED_MATCH_USING_A3_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED"},{"S4", "SUMMARIZED_MATCH_USING_A4_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED"},{"S5", "SUMMARIZED_MATCH_USING_A5_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED"},{"M1", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_MINUS_BADGES_AND_TIMES_ACT_M1_MATCH"},{"M2", "SUMMARIZED_MATCH_MINUS_BADGES_AND_TIMES_ACT_M2_MATCH"},{"MT", "OCS_LOCKED_IN_NON_ACT"}]}
, "574" => #{"Name"=>"MatchType" ,"Type"=>"STRING" ,"ValidValues"=>[{"1", "ONE_PARTY_TRADE_REPORT"},{"2", "TWO_PARTY_TRADE_REPORT"},{"3", "CONFIRMED_TRADE_REPORT"},{"4", "AUTO_MATCH"},{"5", "CROSS_AUCTION"},{"6", "COUNTER_ORDER_SELECTION"},{"7", "CALL_AUCTION"},{"8", "ISSUING_BUY_BACK_AUCTION"},{"M3", "ACT_ACCEPTED_TRADE"},{"M4", "ACT_DEFAULT_TRADE"},{"M5", "ACT_DEFAULT_AFTER_M2"},{"M6", "ACT_M6_MATCH"},{"A1", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_FOUR_BADGES_AND_EXECUTION_TIME"},{"A2", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_FOUR_BADGES"},{"A3", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_TWO_BADGES_AND_EXECUTION_TIME"},{"A4", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_TWO_BADGES"},{"A5", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADETYPE_AND_SPECIAL_TRADE_INDICATOR_PLUS_EXECUTION_TIME"},{"AQ", "COMPARED_RECORDS_RESULTING_FROM_STAMPED_ADVISORIES_OR_SPECIALIST_ACCEPTS_PAIR_OFFS"},{"S1", "SUMMARIZED_MATCH_USING_A1_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIED"},{"S2", "SUMMARIZED_MATCH_USING_A2_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED"},{"S3", "SUMMARIZED_MATCH_USING_A3_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED"},{"S4", "SUMMARIZED_MATCH_USING_A4_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED"},{"S5", "SUMMARIZED_MATCH_USING_A5_EXACT_MATCH_CRITERIA_EXCEPT_QUANTITY_IS_SUMMARIZED"},{"M1", "EXACT_MATCH_ON_TRADE_DATE_STOCK_SYMBOL_QUANTITY_PRICE_TRADE_TYPE_AND_SPECIAL_TRADE_INDICATOR_MINUS_BADGES_AND_TIMES_ACT_M1_MATCH"},{"M2", "SUMMARIZED_MATCH_MINUS_BADGES_AND_TIMES_ACT_M2_MATCH"},{"MT", "OCS_LOCKED_IN_NON_ACT"}], "TagNum" => "574"}


,
"OddLot" => #{"TagNum" => "575" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "575" => #{"Name"=>"OddLot" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "575"}


,
"NoClearingInstructions" => #{"TagNum" => "576" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "576" => #{"Name"=>"NoClearingInstructions" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "576"}


,
"ClearingInstruction" => #{"TagNum" => "577" ,"Type" => "INT" ,"ValidValues" =>[{"0", "PROCESS_NORMALLY"},{"1", "EXCLUDE_FROM_ALL_NETTING"},{"2", "BILATERAL_NETTING_ONLY"},{"3", "EX_CLEARING"},{"4", "SPECIAL_TRADE"},{"5", "MULTILATERAL_NETTING"},{"6", "CLEAR_AGAINST_CENTRAL_COUNTERPARTY"},{"7", "EXCLUDE_FROM_CENTRAL_COUNTERPARTY"},{"8", "MANUAL_MODE"},{"9", "AUTOMATIC_POSTING_MODE"},{"10", "AUTOMATIC_GIVE_UP_MODE"},{"11", "QUALIFIED_SERVICE_REPRESENTATIVE_QSR"},{"12", "CUSTOMER_TRADE"},{"13", "SELF_CLEARING"}]}
, "577" => #{"Name"=>"ClearingInstruction" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "PROCESS_NORMALLY"},{"1", "EXCLUDE_FROM_ALL_NETTING"},{"2", "BILATERAL_NETTING_ONLY"},{"3", "EX_CLEARING"},{"4", "SPECIAL_TRADE"},{"5", "MULTILATERAL_NETTING"},{"6", "CLEAR_AGAINST_CENTRAL_COUNTERPARTY"},{"7", "EXCLUDE_FROM_CENTRAL_COUNTERPARTY"},{"8", "MANUAL_MODE"},{"9", "AUTOMATIC_POSTING_MODE"},{"10", "AUTOMATIC_GIVE_UP_MODE"},{"11", "QUALIFIED_SERVICE_REPRESENTATIVE_QSR"},{"12", "CUSTOMER_TRADE"},{"13", "SELF_CLEARING"}], "TagNum" => "577"}


,
"TradeInputSource" => #{"TagNum" => "578" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "578" => #{"Name"=>"TradeInputSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "578"}


,
"TradeInputDevice" => #{"TagNum" => "579" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "579" => #{"Name"=>"TradeInputDevice" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "579"}


,
"NoDates" => #{"TagNum" => "580" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "580" => #{"Name"=>"NoDates" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "580"}


,
"AccountType" => #{"TagNum" => "581" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ACCOUNT_IS_CARRIED_ON_CUSTOMER_SIDE_OF_THE_BOOKS"},{"2", "ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS"},{"3", "HOUSE_TRADER"},{"4", "FLOOR_TRADER"},{"6", "ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS_AND_IS_CROSS_MARGINED"},{"7", "ACCOUNT_IS_HOUSE_TRADER_AND_IS_CROSS_MARGINED"},{"8", "JOINT_BACK_OFFICE_ACCOUNT"}]}
, "581" => #{"Name"=>"AccountType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ACCOUNT_IS_CARRIED_ON_CUSTOMER_SIDE_OF_THE_BOOKS"},{"2", "ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS"},{"3", "HOUSE_TRADER"},{"4", "FLOOR_TRADER"},{"6", "ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS_AND_IS_CROSS_MARGINED"},{"7", "ACCOUNT_IS_HOUSE_TRADER_AND_IS_CROSS_MARGINED"},{"8", "JOINT_BACK_OFFICE_ACCOUNT"}], "TagNum" => "581"}


,
"CustOrderCapacity" => #{"TagNum" => "582" ,"Type" => "INT" ,"ValidValues" =>[{"1", "MEMBER_TRADING_FOR_THEIR_OWN_ACCOUNT"},{"2", "CLEARING_FIRM_TRADING_FOR_ITS_PROPRIETARY_ACCOUNT"},{"3", "MEMBER_TRADING_FOR_ANOTHER_MEMBER"},{"4", "ALL_OTHER"}]}
, "582" => #{"Name"=>"CustOrderCapacity" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "MEMBER_TRADING_FOR_THEIR_OWN_ACCOUNT"},{"2", "CLEARING_FIRM_TRADING_FOR_ITS_PROPRIETARY_ACCOUNT"},{"3", "MEMBER_TRADING_FOR_ANOTHER_MEMBER"},{"4", "ALL_OTHER"}], "TagNum" => "582"}


,
"ClOrdLinkID" => #{"TagNum" => "583" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "583" => #{"Name"=>"ClOrdLinkID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "583"}


,
"MassStatusReqID" => #{"TagNum" => "584" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "584" => #{"Name"=>"MassStatusReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "584"}


,
"MassStatusReqType" => #{"TagNum" => "585" ,"Type" => "INT" ,"ValidValues" =>[{"1", "STATUS_FOR_ORDERS_FOR_A_SECURITY"},{"2", "STATUS_FOR_ORDERS_FOR_AN_UNDERLYING_SECURITY"},{"3", "STATUS_FOR_ORDERS_FOR_A_PRODUCT"},{"4", "STATUS_FOR_ORDERS_FOR_A_CFICODE"},{"5", "STATUS_FOR_ORDERS_FOR_A_SECURITYTYPE"},{"6", "STATUS_FOR_ORDERS_FOR_A_TRADING_SESSION"},{"7", "STATUS_FOR_ALL_ORDERS"},{"8", "STATUS_FOR_ORDERS_FOR_A_PARTYID"},{"9", "STATUS_FOR_SECURITY_ISSUER"},{"10", "STATUS_FOR_ISSUER_OF_UNDERLYING_SECURITY"}]}
, "585" => #{"Name"=>"MassStatusReqType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "STATUS_FOR_ORDERS_FOR_A_SECURITY"},{"2", "STATUS_FOR_ORDERS_FOR_AN_UNDERLYING_SECURITY"},{"3", "STATUS_FOR_ORDERS_FOR_A_PRODUCT"},{"4", "STATUS_FOR_ORDERS_FOR_A_CFICODE"},{"5", "STATUS_FOR_ORDERS_FOR_A_SECURITYTYPE"},{"6", "STATUS_FOR_ORDERS_FOR_A_TRADING_SESSION"},{"7", "STATUS_FOR_ALL_ORDERS"},{"8", "STATUS_FOR_ORDERS_FOR_A_PARTYID"},{"9", "STATUS_FOR_SECURITY_ISSUER"},{"10", "STATUS_FOR_ISSUER_OF_UNDERLYING_SECURITY"}], "TagNum" => "585"}


,
"OrigOrdModTime" => #{"TagNum" => "586" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "586" => #{"Name"=>"OrigOrdModTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "586"}


,
"LegSettlType" => #{"TagNum" => "587" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "587" => #{"Name"=>"LegSettlType" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "587"}


,
"LegSettlDate" => #{"TagNum" => "588" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "588" => #{"Name"=>"LegSettlDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "588"}


,
"DayBookingInst" => #{"TagNum" => "589" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "CAN_TRIGGER_BOOKING_WITHOUT_REFERENCE_TO_THE_ORDER_INITIATOR"},{"1", "SPEAK_WITH_ORDER_INITIATOR_BEFORE_BOOKING"},{"2", "ACCUMULATE"}]}
, "589" => #{"Name"=>"DayBookingInst" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "CAN_TRIGGER_BOOKING_WITHOUT_REFERENCE_TO_THE_ORDER_INITIATOR"},{"1", "SPEAK_WITH_ORDER_INITIATOR_BEFORE_BOOKING"},{"2", "ACCUMULATE"}], "TagNum" => "589"}


,
"BookingUnit" => #{"TagNum" => "590" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "EACH_PARTIAL_EXECUTION_IS_A_BOOKABLE_UNIT"},{"1", "AGGREGATE_PARTIAL_EXECUTIONS_ON_THIS_ORDER_AND_BOOK_ONE_TRADE_PER_ORDER"},{"2", "AGGREGATE_EXECUTIONS_FOR_THIS_SYMBOL_SIDE_AND_SETTLEMENT_DATE"}]}
, "590" => #{"Name"=>"BookingUnit" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "EACH_PARTIAL_EXECUTION_IS_A_BOOKABLE_UNIT"},{"1", "AGGREGATE_PARTIAL_EXECUTIONS_ON_THIS_ORDER_AND_BOOK_ONE_TRADE_PER_ORDER"},{"2", "AGGREGATE_EXECUTIONS_FOR_THIS_SYMBOL_SIDE_AND_SETTLEMENT_DATE"}], "TagNum" => "590"}


,
"PreallocMethod" => #{"TagNum" => "591" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "PRO_RATA"},{"1", "DO_NOT_PRO_RATA"}]}
, "591" => #{"Name"=>"PreallocMethod" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "PRO_RATA"},{"1", "DO_NOT_PRO_RATA"}], "TagNum" => "591"}


,
"UnderlyingCountryOfIssue" => #{"TagNum" => "592" ,"Type" => "COUNTRY" ,"ValidValues" =>[]}
, "592" => #{"Name"=>"UnderlyingCountryOfIssue" ,"Type"=>"COUNTRY" ,"ValidValues"=>[], "TagNum" => "592"}


,
"UnderlyingStateOrProvinceOfIssue" => #{"TagNum" => "593" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "593" => #{"Name"=>"UnderlyingStateOrProvinceOfIssue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "593"}


,
"UnderlyingLocaleOfIssue" => #{"TagNum" => "594" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "594" => #{"Name"=>"UnderlyingLocaleOfIssue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "594"}


,
"UnderlyingInstrRegistry" => #{"TagNum" => "595" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "595" => #{"Name"=>"UnderlyingInstrRegistry" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "595"}


,
"LegCountryOfIssue" => #{"TagNum" => "596" ,"Type" => "COUNTRY" ,"ValidValues" =>[]}
, "596" => #{"Name"=>"LegCountryOfIssue" ,"Type"=>"COUNTRY" ,"ValidValues"=>[], "TagNum" => "596"}


,
"LegStateOrProvinceOfIssue" => #{"TagNum" => "597" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "597" => #{"Name"=>"LegStateOrProvinceOfIssue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "597"}


,
"LegLocaleOfIssue" => #{"TagNum" => "598" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "598" => #{"Name"=>"LegLocaleOfIssue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "598"}


,
"LegInstrRegistry" => #{"TagNum" => "599" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "599" => #{"Name"=>"LegInstrRegistry" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "599"}


,
"LegSymbol" => #{"TagNum" => "600" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "600" => #{"Name"=>"LegSymbol" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "600"}


,
"LegSymbolSfx" => #{"TagNum" => "601" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "601" => #{"Name"=>"LegSymbolSfx" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "601"}


,
"LegSecurityID" => #{"TagNum" => "602" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "602" => #{"Name"=>"LegSecurityID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "602"}


,
"LegSecurityIDSource" => #{"TagNum" => "603" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "603" => #{"Name"=>"LegSecurityIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "603"}


,
"NoLegSecurityAltID" => #{"TagNum" => "604" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "604" => #{"Name"=>"NoLegSecurityAltID" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "604"}


,
"LegSecurityAltID" => #{"TagNum" => "605" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "605" => #{"Name"=>"LegSecurityAltID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "605"}


,
"LegSecurityAltIDSource" => #{"TagNum" => "606" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "606" => #{"Name"=>"LegSecurityAltIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "606"}


,
"LegProduct" => #{"TagNum" => "607" ,"Type" => "INT" ,"ValidValues" =>[]}
, "607" => #{"Name"=>"LegProduct" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "607"}


,
"LegCFICode" => #{"TagNum" => "608" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "608" => #{"Name"=>"LegCFICode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "608"}


,
"LegSecurityType" => #{"TagNum" => "609" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "609" => #{"Name"=>"LegSecurityType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "609"}


,
"LegMaturityMonthYear" => #{"TagNum" => "610" ,"Type" => "MONTHYEAR" ,"ValidValues" =>[]}
, "610" => #{"Name"=>"LegMaturityMonthYear" ,"Type"=>"MONTHYEAR" ,"ValidValues"=>[], "TagNum" => "610"}


,
"LegMaturityDate" => #{"TagNum" => "611" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "611" => #{"Name"=>"LegMaturityDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "611"}


,
"LegStrikePrice" => #{"TagNum" => "612" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "612" => #{"Name"=>"LegStrikePrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "612"}


,
"LegOptAttribute" => #{"TagNum" => "613" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "613" => #{"Name"=>"LegOptAttribute" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "613"}


,
"LegContractMultiplier" => #{"TagNum" => "614" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "614" => #{"Name"=>"LegContractMultiplier" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "614"}


,
"LegCouponRate" => #{"TagNum" => "615" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "615" => #{"Name"=>"LegCouponRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "615"}


,
"LegSecurityExchange" => #{"TagNum" => "616" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "616" => #{"Name"=>"LegSecurityExchange" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "616"}


,
"LegIssuer" => #{"TagNum" => "617" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "617" => #{"Name"=>"LegIssuer" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "617"}


,
"EncodedLegIssuerLen" => #{"TagNum" => "618" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "618" => #{"Name"=>"EncodedLegIssuerLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "618"}


,
"EncodedLegIssuer" => #{"TagNum" => "619" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "619" => #{"Name"=>"EncodedLegIssuer" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "619"}


,
"LegSecurityDesc" => #{"TagNum" => "620" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "620" => #{"Name"=>"LegSecurityDesc" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "620"}


,
"EncodedLegSecurityDescLen" => #{"TagNum" => "621" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "621" => #{"Name"=>"EncodedLegSecurityDescLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "621"}


,
"EncodedLegSecurityDesc" => #{"TagNum" => "622" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "622" => #{"Name"=>"EncodedLegSecurityDesc" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "622"}


,
"LegRatioQty" => #{"TagNum" => "623" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "623" => #{"Name"=>"LegRatioQty" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "623"}


,
"LegSide" => #{"TagNum" => "624" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "624" => #{"Name"=>"LegSide" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "624"}


,
"TradingSessionSubID" => #{"TagNum" => "625" ,"Type" => "STRING" ,"ValidValues" =>[{"1", "PRE_TRADING"},{"2", "OPENING_OR_OPENING_AUCTION"},{"3", "3"},{"4", "CLOSING_OR_CLOSING_AUCTION"},{"5", "POST_TRADING"},{"6", "INTRADAY_AUCTION"},{"7", "QUIESCENT"}]}
, "625" => #{"Name"=>"TradingSessionSubID" ,"Type"=>"STRING" ,"ValidValues"=>[{"1", "PRE_TRADING"},{"2", "OPENING_OR_OPENING_AUCTION"},{"3", "3"},{"4", "CLOSING_OR_CLOSING_AUCTION"},{"5", "POST_TRADING"},{"6", "INTRADAY_AUCTION"},{"7", "QUIESCENT"}], "TagNum" => "625"}


,
"AllocType" => #{"TagNum" => "626" ,"Type" => "INT" ,"ValidValues" =>[{"1", "CALCULATED"},{"2", "PRELIMINARY"},{"3", "SELLSIDE_CALCULATED_USING_PRELIMINARY"},{"4", "SELLSIDE_CALCULATED_WITHOUT_PRELIMINARY"},{"5", "READY_TO_BOOK"},{"6", "BUYSIDE_READY_TO_BOOK"},{"7", "WAREHOUSE_INSTRUCTION"},{"8", "REQUEST_TO_INTERMEDIARY"},{"9", "ACCEPT"},{"10", "REJECT"},{"11", "ACCEPT_PENDING"},{"12", "INCOMPLETE_GROUP"},{"13", "COMPLETE_GROUP"},{"14", "REVERSAL_PENDING"}]}
, "626" => #{"Name"=>"AllocType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "CALCULATED"},{"2", "PRELIMINARY"},{"3", "SELLSIDE_CALCULATED_USING_PRELIMINARY"},{"4", "SELLSIDE_CALCULATED_WITHOUT_PRELIMINARY"},{"5", "READY_TO_BOOK"},{"6", "BUYSIDE_READY_TO_BOOK"},{"7", "WAREHOUSE_INSTRUCTION"},{"8", "REQUEST_TO_INTERMEDIARY"},{"9", "ACCEPT"},{"10", "REJECT"},{"11", "ACCEPT_PENDING"},{"12", "INCOMPLETE_GROUP"},{"13", "COMPLETE_GROUP"},{"14", "REVERSAL_PENDING"}], "TagNum" => "626"}


,
"NoHops" => #{"TagNum" => "627" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "627" => #{"Name"=>"NoHops" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "627"}


,
"HopCompID" => #{"TagNum" => "628" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "628" => #{"Name"=>"HopCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "628"}


,
"HopSendingTime" => #{"TagNum" => "629" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "629" => #{"Name"=>"HopSendingTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "629"}


,
"HopRefID" => #{"TagNum" => "630" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "630" => #{"Name"=>"HopRefID" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "630"}


,
"MidPx" => #{"TagNum" => "631" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "631" => #{"Name"=>"MidPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "631"}


,
"BidYield" => #{"TagNum" => "632" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "632" => #{"Name"=>"BidYield" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "632"}


,
"MidYield" => #{"TagNum" => "633" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "633" => #{"Name"=>"MidYield" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "633"}


,
"OfferYield" => #{"TagNum" => "634" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "634" => #{"Name"=>"OfferYield" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "634"}


,
"ClearingFeeIndicator" => #{"TagNum" => "635" ,"Type" => "STRING" ,"ValidValues" =>[{"1", "1ST_YEAR_DELEGATE_TRADING_FOR_OWN_ACCOUNT"},{"2", "2ND_YEAR_DELEGATE_TRADING_FOR_OWN_ACCOUNT"},{"3", "3RD_YEAR_DELEGATE_TRADING_FOR_OWN_ACCOUNT"},{"4", "4TH_YEAR_DELEGATE_TRADING_FOR_OWN_ACCOUNT"},{"5", "5TH_YEAR_DELEGATE_TRADING_FOR_OWN_ACCOUNT"},{"9", "6TH_YEAR_DELEGATE_TRADING_FOR_OWN_ACCOUNT"},{"B", "CBOE_MEMBER"},{"C", "NON_MEMBER_AND_CUSTOMER"},{"E", "EQUITY_MEMBER_AND_CLEARING_MEMBER"},{"F", "FULL_AND_ASSOCIATE_MEMBER_TRADING_FOR_OWN_ACCOUNT_AND_AS_FLOOR_BROKERS"},{"H", "106H_AND_106J_FIRMS"},{"I", "GIM_IDEM_AND_COM_MEMBERSHIP_INTEREST_HOLDERS"},{"L", "LESSEE_106F_EMPLOYEES"},{"M", "ALL_OTHER_OWNERSHIP_TYPES"}]}
, "635" => #{"Name"=>"ClearingFeeIndicator" ,"Type"=>"STRING" ,"ValidValues"=>[{"1", "1ST_YEAR_DELEGATE_TRADING_FOR_OWN_ACCOUNT"},{"2", "2ND_YEAR_DELEGATE_TRADING_FOR_OWN_ACCOUNT"},{"3", "3RD_YEAR_DELEGATE_TRADING_FOR_OWN_ACCOUNT"},{"4", "4TH_YEAR_DELEGATE_TRADING_FOR_OWN_ACCOUNT"},{"5", "5TH_YEAR_DELEGATE_TRADING_FOR_OWN_ACCOUNT"},{"9", "6TH_YEAR_DELEGATE_TRADING_FOR_OWN_ACCOUNT"},{"B", "CBOE_MEMBER"},{"C", "NON_MEMBER_AND_CUSTOMER"},{"E", "EQUITY_MEMBER_AND_CLEARING_MEMBER"},{"F", "FULL_AND_ASSOCIATE_MEMBER_TRADING_FOR_OWN_ACCOUNT_AND_AS_FLOOR_BROKERS"},{"H", "106H_AND_106J_FIRMS"},{"I", "GIM_IDEM_AND_COM_MEMBERSHIP_INTEREST_HOLDERS"},{"L", "LESSEE_106F_EMPLOYEES"},{"M", "ALL_OTHER_OWNERSHIP_TYPES"}], "TagNum" => "635"}


,
"WorkingIndicator" => #{"TagNum" => "636" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "636" => #{"Name"=>"WorkingIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "636"}


,
"LegLastPx" => #{"TagNum" => "637" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "637" => #{"Name"=>"LegLastPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "637"}


,
"PriorityIndicator" => #{"TagNum" => "638" ,"Type" => "INT" ,"ValidValues" =>[{"0", "PRIORITY_UNCHANGED"},{"1", "LOST_PRIORITY_AS_RESULT_OF_ORDER_CHANGE"}]}
, "638" => #{"Name"=>"PriorityIndicator" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "PRIORITY_UNCHANGED"},{"1", "LOST_PRIORITY_AS_RESULT_OF_ORDER_CHANGE"}], "TagNum" => "638"}


,
"PriceImprovement" => #{"TagNum" => "639" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "639" => #{"Name"=>"PriceImprovement" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "639"}


,
"Price2" => #{"TagNum" => "640" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "640" => #{"Name"=>"Price2" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "640"}


,
"LastForwardPoints2" => #{"TagNum" => "641" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "641" => #{"Name"=>"LastForwardPoints2" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "641"}


,
"BidForwardPoints2" => #{"TagNum" => "642" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "642" => #{"Name"=>"BidForwardPoints2" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "642"}


,
"OfferForwardPoints2" => #{"TagNum" => "643" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "643" => #{"Name"=>"OfferForwardPoints2" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "643"}


,
"RFQReqID" => #{"TagNum" => "644" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "644" => #{"Name"=>"RFQReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "644"}


,
"MktBidPx" => #{"TagNum" => "645" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "645" => #{"Name"=>"MktBidPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "645"}


,
"MktOfferPx" => #{"TagNum" => "646" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "646" => #{"Name"=>"MktOfferPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "646"}


,
"MinBidSize" => #{"TagNum" => "647" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "647" => #{"Name"=>"MinBidSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "647"}


,
"MinOfferSize" => #{"TagNum" => "648" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "648" => #{"Name"=>"MinOfferSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "648"}


,
"QuoteStatusReqID" => #{"TagNum" => "649" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "649" => #{"Name"=>"QuoteStatusReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "649"}


,
"LegalConfirm" => #{"TagNum" => "650" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "650" => #{"Name"=>"LegalConfirm" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "650"}


,
"UnderlyingLastPx" => #{"TagNum" => "651" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "651" => #{"Name"=>"UnderlyingLastPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "651"}


,
"UnderlyingLastQty" => #{"TagNum" => "652" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "652" => #{"Name"=>"UnderlyingLastQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "652"}


,
"LegRefID" => #{"TagNum" => "654" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "654" => #{"Name"=>"LegRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "654"}


,
"ContraLegRefID" => #{"TagNum" => "655" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "655" => #{"Name"=>"ContraLegRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "655"}


,
"SettlCurrBidFxRate" => #{"TagNum" => "656" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "656" => #{"Name"=>"SettlCurrBidFxRate" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "656"}


,
"SettlCurrOfferFxRate" => #{"TagNum" => "657" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "657" => #{"Name"=>"SettlCurrOfferFxRate" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "657"}


,
"QuoteRequestRejectReason" => #{"TagNum" => "658" ,"Type" => "INT" ,"ValidValues" =>[{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE"},{"3", "QUOTE_REQUEST_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "INVALID_PRICE"},{"6", "NOT_AUTHORIZED_TO_REQUEST_QUOTE"},{"7", "NO_MATCH_FOR_INQUIRY"},{"8", "NO_MARKET_FOR_INSTRUMENT"},{"9", "NO_INVENTORY"},{"10", "PASS"},{"11", "INSUFFICIENT_CREDIT"},{"99", "OTHER"}]}
, "658" => #{"Name"=>"QuoteRequestRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "UNKNOWN_SYMBOL"},{"2", "EXCHANGE"},{"3", "QUOTE_REQUEST_EXCEEDS_LIMIT"},{"4", "TOO_LATE_TO_ENTER"},{"5", "INVALID_PRICE"},{"6", "NOT_AUTHORIZED_TO_REQUEST_QUOTE"},{"7", "NO_MATCH_FOR_INQUIRY"},{"8", "NO_MARKET_FOR_INSTRUMENT"},{"9", "NO_INVENTORY"},{"10", "PASS"},{"11", "INSUFFICIENT_CREDIT"},{"99", "OTHER"}], "TagNum" => "658"}


,
"SideComplianceID" => #{"TagNum" => "659" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "659" => #{"Name"=>"SideComplianceID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "659"}


,
"AcctIDSource" => #{"TagNum" => "660" ,"Type" => "INT" ,"ValidValues" =>[{"1", "BIC"},{"2", "SID_CODE"},{"3", "TFM"},{"4", "OMGEO"},{"5", "DTCC_CODE"},{"99", "OTHER"}]}
, "660" => #{"Name"=>"AcctIDSource" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "BIC"},{"2", "SID_CODE"},{"3", "TFM"},{"4", "OMGEO"},{"5", "DTCC_CODE"},{"99", "OTHER"}], "TagNum" => "660"}


,
"AllocAcctIDSource" => #{"TagNum" => "661" ,"Type" => "INT" ,"ValidValues" =>[]}
, "661" => #{"Name"=>"AllocAcctIDSource" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "661"}


,
"BenchmarkPrice" => #{"TagNum" => "662" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "662" => #{"Name"=>"BenchmarkPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "662"}


,
"BenchmarkPriceType" => #{"TagNum" => "663" ,"Type" => "INT" ,"ValidValues" =>[]}
, "663" => #{"Name"=>"BenchmarkPriceType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "663"}


,
"ConfirmID" => #{"TagNum" => "664" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "664" => #{"Name"=>"ConfirmID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "664"}


,
"ConfirmStatus" => #{"TagNum" => "665" ,"Type" => "INT" ,"ValidValues" =>[{"1", "RECEIVED"},{"2", "MISMATCHED_ACCOUNT"},{"3", "MISSING_SETTLEMENT_INSTRUCTIONS"},{"4", "CONFIRMED"},{"5", "REQUEST_REJECTED"}]}
, "665" => #{"Name"=>"ConfirmStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "RECEIVED"},{"2", "MISMATCHED_ACCOUNT"},{"3", "MISSING_SETTLEMENT_INSTRUCTIONS"},{"4", "CONFIRMED"},{"5", "REQUEST_REJECTED"}], "TagNum" => "665"}


,
"ConfirmTransType" => #{"TagNum" => "666" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NEW"},{"1", "REPLACE"},{"2", "CANCEL"}]}
, "666" => #{"Name"=>"ConfirmTransType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NEW"},{"1", "REPLACE"},{"2", "CANCEL"}], "TagNum" => "666"}


,
"ContractSettlMonth" => #{"TagNum" => "667" ,"Type" => "MONTHYEAR" ,"ValidValues" =>[]}
, "667" => #{"Name"=>"ContractSettlMonth" ,"Type"=>"MONTHYEAR" ,"ValidValues"=>[], "TagNum" => "667"}


,
"DeliveryForm" => #{"TagNum" => "668" ,"Type" => "INT" ,"ValidValues" =>[{"1", "BOOK_ENTRY"},{"2", "BEARER"}]}
, "668" => #{"Name"=>"DeliveryForm" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "BOOK_ENTRY"},{"2", "BEARER"}], "TagNum" => "668"}


,
"LastParPx" => #{"TagNum" => "669" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "669" => #{"Name"=>"LastParPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "669"}


,
"NoLegAllocs" => #{"TagNum" => "670" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "670" => #{"Name"=>"NoLegAllocs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "670"}


,
"LegAllocAccount" => #{"TagNum" => "671" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "671" => #{"Name"=>"LegAllocAccount" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "671"}


,
"LegIndividualAllocID" => #{"TagNum" => "672" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "672" => #{"Name"=>"LegIndividualAllocID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "672"}


,
"LegAllocQty" => #{"TagNum" => "673" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "673" => #{"Name"=>"LegAllocQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "673"}


,
"LegAllocAcctIDSource" => #{"TagNum" => "674" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "674" => #{"Name"=>"LegAllocAcctIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "674"}


,
"LegSettlCurrency" => #{"TagNum" => "675" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "675" => #{"Name"=>"LegSettlCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "675"}


,
"LegBenchmarkCurveCurrency" => #{"TagNum" => "676" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "676" => #{"Name"=>"LegBenchmarkCurveCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "676"}


,
"LegBenchmarkCurveName" => #{"TagNum" => "677" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "677" => #{"Name"=>"LegBenchmarkCurveName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "677"}


,
"LegBenchmarkCurvePoint" => #{"TagNum" => "678" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "678" => #{"Name"=>"LegBenchmarkCurvePoint" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "678"}


,
"LegBenchmarkPrice" => #{"TagNum" => "679" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "679" => #{"Name"=>"LegBenchmarkPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "679"}


,
"LegBenchmarkPriceType" => #{"TagNum" => "680" ,"Type" => "INT" ,"ValidValues" =>[]}
, "680" => #{"Name"=>"LegBenchmarkPriceType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "680"}


,
"LegBidPx" => #{"TagNum" => "681" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "681" => #{"Name"=>"LegBidPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "681"}


,
"LegIOIQty" => #{"TagNum" => "682" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "682" => #{"Name"=>"LegIOIQty" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "682"}


,
"NoLegStipulations" => #{"TagNum" => "683" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "683" => #{"Name"=>"NoLegStipulations" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "683"}


,
"LegOfferPx" => #{"TagNum" => "684" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "684" => #{"Name"=>"LegOfferPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "684"}


,
"LegOrderQty" => #{"TagNum" => "685" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "685" => #{"Name"=>"LegOrderQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "685"}


,
"LegPriceType" => #{"TagNum" => "686" ,"Type" => "INT" ,"ValidValues" =>[]}
, "686" => #{"Name"=>"LegPriceType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "686"}


,
"LegQty" => #{"TagNum" => "687" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "687" => #{"Name"=>"LegQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "687"}


,
"LegStipulationType" => #{"TagNum" => "688" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "688" => #{"Name"=>"LegStipulationType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "688"}


,
"LegStipulationValue" => #{"TagNum" => "689" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "689" => #{"Name"=>"LegStipulationValue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "689"}


,
"LegSwapType" => #{"TagNum" => "690" ,"Type" => "INT" ,"ValidValues" =>[{"1", "PAR_FOR_PAR"},{"2", "MODIFIED_DURATION"},{"4", "RISK"},{"5", "PROCEEDS"}]}
, "690" => #{"Name"=>"LegSwapType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "PAR_FOR_PAR"},{"2", "MODIFIED_DURATION"},{"4", "RISK"},{"5", "PROCEEDS"}], "TagNum" => "690"}


,
"Pool" => #{"TagNum" => "691" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "691" => #{"Name"=>"Pool" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "691"}


,
"QuotePriceType" => #{"TagNum" => "692" ,"Type" => "INT" ,"ValidValues" =>[{"1", "PERCENT"},{"2", "PER_SHARE"},{"3", "FIXED_AMOUNT"},{"4", "DISCOUNT"},{"5", "PREMIUM"},{"6", "SPREAD"},{"7", "TED_PRICE"},{"8", "TED_YIELD"},{"9", "YIELD_SPREAD"},{"10", "YIELD"}]}
, "692" => #{"Name"=>"QuotePriceType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "PERCENT"},{"2", "PER_SHARE"},{"3", "FIXED_AMOUNT"},{"4", "DISCOUNT"},{"5", "PREMIUM"},{"6", "SPREAD"},{"7", "TED_PRICE"},{"8", "TED_YIELD"},{"9", "YIELD_SPREAD"},{"10", "YIELD"}], "TagNum" => "692"}


,
"QuoteRespID" => #{"TagNum" => "693" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "693" => #{"Name"=>"QuoteRespID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "693"}


,
"QuoteRespType" => #{"TagNum" => "694" ,"Type" => "INT" ,"ValidValues" =>[{"1", "HIT_LIFT"},{"2", "COUNTER"},{"3", "EXPIRED"},{"4", "COVER"},{"5", "DONE_AWAY"},{"6", "PASS"},{"7", "END_TRADE"},{"8", "TIMED_OUT"}]}
, "694" => #{"Name"=>"QuoteRespType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "HIT_LIFT"},{"2", "COUNTER"},{"3", "EXPIRED"},{"4", "COVER"},{"5", "DONE_AWAY"},{"6", "PASS"},{"7", "END_TRADE"},{"8", "TIMED_OUT"}], "TagNum" => "694"}


,
"QuoteQualifier" => #{"TagNum" => "695" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "695" => #{"Name"=>"QuoteQualifier" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "695"}


,
"YieldRedemptionDate" => #{"TagNum" => "696" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "696" => #{"Name"=>"YieldRedemptionDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "696"}


,
"YieldRedemptionPrice" => #{"TagNum" => "697" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "697" => #{"Name"=>"YieldRedemptionPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "697"}


,
"YieldRedemptionPriceType" => #{"TagNum" => "698" ,"Type" => "INT" ,"ValidValues" =>[]}
, "698" => #{"Name"=>"YieldRedemptionPriceType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "698"}


,
"BenchmarkSecurityID" => #{"TagNum" => "699" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "699" => #{"Name"=>"BenchmarkSecurityID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "699"}


,
"ReversalIndicator" => #{"TagNum" => "700" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "700" => #{"Name"=>"ReversalIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "700"}


,
"YieldCalcDate" => #{"TagNum" => "701" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "701" => #{"Name"=>"YieldCalcDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "701"}


,
"NoPositions" => #{"TagNum" => "702" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "702" => #{"Name"=>"NoPositions" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "702"}


,
"PosType" => #{"TagNum" => "703" ,"Type" => "STRING" ,"ValidValues" =>[{"ALC", "ALLOCATION_TRADE_QTY"},{"AS", "OPTION_ASSIGNMENT"},{"ASF", "AS_OF_TRADE_QTY"},{"DLV", "DELIVERY_QTY"},{"ETR", "ELECTRONIC_TRADE_QTY"},{"EX", "OPTION_EXERCISE_QTY"},{"FIN", "END_OF_DAY_QTY"},{"IAS", "INTRA_SPREAD_QTY"},{"IES", "INTER_SPREAD_QTY"},{"PA", "ADJUSTMENT_QTY"},{"PIT", "PIT_TRADE_QTY"},{"SOD", "START_OF_DAY_QTY"},{"SPL", "INTEGRAL_SPLIT"},{"TA", "TRANSACTION_FROM_ASSIGNMENT"},{"TOT", "TOTAL_TRANSACTION_QTY"},{"TQ", "TRANSACTION_QUANTITY"},{"TRF", "TRANSFER_TRADE_QTY"},{"TX", "TRANSACTION_FROM_EXERCISE"},{"XM", "CROSS_MARGIN_QTY"},{"RCV", "RECEIVE_QUANTITY"},{"CAA", "CORPORATE_ACTION_ADJUSTMENT"},{"DN", "DELIVERY_NOTICE_QTY"},{"EP", "EXCHANGE_FOR_PHYSICAL_QTY"},{"PNTN", "PRIVATELY_NEGOTIATED_TRADE_QTY"},{"DLT", "NET_DELTA_QTY"},{"CEA", "CREDIT_EVENT_ADJUSTMENT"},{"SEA", "SUCCESSION_EVENT_ADJUSTMENT"}]}
, "703" => #{"Name"=>"PosType" ,"Type"=>"STRING" ,"ValidValues"=>[{"ALC", "ALLOCATION_TRADE_QTY"},{"AS", "OPTION_ASSIGNMENT"},{"ASF", "AS_OF_TRADE_QTY"},{"DLV", "DELIVERY_QTY"},{"ETR", "ELECTRONIC_TRADE_QTY"},{"EX", "OPTION_EXERCISE_QTY"},{"FIN", "END_OF_DAY_QTY"},{"IAS", "INTRA_SPREAD_QTY"},{"IES", "INTER_SPREAD_QTY"},{"PA", "ADJUSTMENT_QTY"},{"PIT", "PIT_TRADE_QTY"},{"SOD", "START_OF_DAY_QTY"},{"SPL", "INTEGRAL_SPLIT"},{"TA", "TRANSACTION_FROM_ASSIGNMENT"},{"TOT", "TOTAL_TRANSACTION_QTY"},{"TQ", "TRANSACTION_QUANTITY"},{"TRF", "TRANSFER_TRADE_QTY"},{"TX", "TRANSACTION_FROM_EXERCISE"},{"XM", "CROSS_MARGIN_QTY"},{"RCV", "RECEIVE_QUANTITY"},{"CAA", "CORPORATE_ACTION_ADJUSTMENT"},{"DN", "DELIVERY_NOTICE_QTY"},{"EP", "EXCHANGE_FOR_PHYSICAL_QTY"},{"PNTN", "PRIVATELY_NEGOTIATED_TRADE_QTY"},{"DLT", "NET_DELTA_QTY"},{"CEA", "CREDIT_EVENT_ADJUSTMENT"},{"SEA", "SUCCESSION_EVENT_ADJUSTMENT"}], "TagNum" => "703"}


,
"LongQty" => #{"TagNum" => "704" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "704" => #{"Name"=>"LongQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "704"}


,
"ShortQty" => #{"TagNum" => "705" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "705" => #{"Name"=>"ShortQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "705"}


,
"PosQtyStatus" => #{"TagNum" => "706" ,"Type" => "INT" ,"ValidValues" =>[{"0", "SUBMITTED"},{"1", "ACCEPTED"},{"2", "REJECTED"}]}
, "706" => #{"Name"=>"PosQtyStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "SUBMITTED"},{"1", "ACCEPTED"},{"2", "REJECTED"}], "TagNum" => "706"}


,
"PosAmtType" => #{"TagNum" => "707" ,"Type" => "STRING" ,"ValidValues" =>[{"CASH", "CASH_AMOUNT"},{"CRES", "CASH_RESIDUAL_AMOUNT"},{"FMTM", "FINAL_MARK_TO_MARKET_AMOUNT"},{"IMTM", "INCREMENTAL_MARK_TO_MARKET_AMOUNT"},{"PREM", "PREMIUM_AMOUNT"},{"SMTM", "START_OF_DAY_MARK_TO_MARKET_AMOUNT"},{"TVAR", "TRADE_VARIATION_AMOUNT"},{"VADJ", "VALUE_ADJUSTED_AMOUNT"},{"SETL", "SETTLEMENT_VALUE"},{"ICPN", "INITIAL_TRADE_COUPON_AMOUNT"},{"ACPN", "ACCRUED_COUPON_AMOUNT"},{"CPN", "COUPON_AMOUNT"},{"IACPN", "INCREMENTAL_ACCRUED_COUPON"},{"CMTM", "COLLATERALIZED_MARK_TO_MARKET"},{"ICMTM", "INCREMENTAL_COLLATERALIZED_MARK_TO_MARKET"},{"DLV", "COMPENSATION_AMOUNT"},{"BANK", "TOTAL_BANKED_AMOUNT"},{"COLAT", "TOTAL_COLLATERALIZED_AMOUNT"}]}
, "707" => #{"Name"=>"PosAmtType" ,"Type"=>"STRING" ,"ValidValues"=>[{"CASH", "CASH_AMOUNT"},{"CRES", "CASH_RESIDUAL_AMOUNT"},{"FMTM", "FINAL_MARK_TO_MARKET_AMOUNT"},{"IMTM", "INCREMENTAL_MARK_TO_MARKET_AMOUNT"},{"PREM", "PREMIUM_AMOUNT"},{"SMTM", "START_OF_DAY_MARK_TO_MARKET_AMOUNT"},{"TVAR", "TRADE_VARIATION_AMOUNT"},{"VADJ", "VALUE_ADJUSTED_AMOUNT"},{"SETL", "SETTLEMENT_VALUE"},{"ICPN", "INITIAL_TRADE_COUPON_AMOUNT"},{"ACPN", "ACCRUED_COUPON_AMOUNT"},{"CPN", "COUPON_AMOUNT"},{"IACPN", "INCREMENTAL_ACCRUED_COUPON"},{"CMTM", "COLLATERALIZED_MARK_TO_MARKET"},{"ICMTM", "INCREMENTAL_COLLATERALIZED_MARK_TO_MARKET"},{"DLV", "COMPENSATION_AMOUNT"},{"BANK", "TOTAL_BANKED_AMOUNT"},{"COLAT", "TOTAL_COLLATERALIZED_AMOUNT"}], "TagNum" => "707"}


,
"PosAmt" => #{"TagNum" => "708" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "708" => #{"Name"=>"PosAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "708"}


,
"PosTransType" => #{"TagNum" => "709" ,"Type" => "INT" ,"ValidValues" =>[{"1", "EXERCISE"},{"2", "DO_NOT_EXERCISE"},{"3", "POSITION_ADJUSTMENT"},{"4", "POSITION_CHANGE_SUBMISSION_MARGIN_DISPOSITION"},{"5", "PLEDGE"},{"6", "LARGE_TRADER_SUBMISSION"}]}
, "709" => #{"Name"=>"PosTransType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "EXERCISE"},{"2", "DO_NOT_EXERCISE"},{"3", "POSITION_ADJUSTMENT"},{"4", "POSITION_CHANGE_SUBMISSION_MARGIN_DISPOSITION"},{"5", "PLEDGE"},{"6", "LARGE_TRADER_SUBMISSION"}], "TagNum" => "709"}


,
"PosReqID" => #{"TagNum" => "710" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "710" => #{"Name"=>"PosReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "710"}


,
"NoUnderlyings" => #{"TagNum" => "711" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "711" => #{"Name"=>"NoUnderlyings" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "711"}


,
"PosMaintAction" => #{"TagNum" => "712" ,"Type" => "INT" ,"ValidValues" =>[{"1", "NEW"},{"2", "REPLACE"},{"3", "CANCEL"},{"4", "REVERSE"}]}
, "712" => #{"Name"=>"PosMaintAction" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "NEW"},{"2", "REPLACE"},{"3", "CANCEL"},{"4", "REVERSE"}], "TagNum" => "712"}


,
"OrigPosReqRefID" => #{"TagNum" => "713" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "713" => #{"Name"=>"OrigPosReqRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "713"}


,
"PosMaintRptRefID" => #{"TagNum" => "714" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "714" => #{"Name"=>"PosMaintRptRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "714"}


,
"ClearingBusinessDate" => #{"TagNum" => "715" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "715" => #{"Name"=>"ClearingBusinessDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "715"}


,
"SettlSessID" => #{"TagNum" => "716" ,"Type" => "STRING" ,"ValidValues" =>[{"ITD", "INTRADAY"},{"RTH", "REGULAR_TRADING_HOURS"},{"ETH", "ELECTRONIC_TRADING_HOURS"},{"EOD", "END_OF_DAY"}]}
, "716" => #{"Name"=>"SettlSessID" ,"Type"=>"STRING" ,"ValidValues"=>[{"ITD", "INTRADAY"},{"RTH", "REGULAR_TRADING_HOURS"},{"ETH", "ELECTRONIC_TRADING_HOURS"},{"EOD", "END_OF_DAY"}], "TagNum" => "716"}


,
"SettlSessSubID" => #{"TagNum" => "717" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "717" => #{"Name"=>"SettlSessSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "717"}


,
"AdjustmentType" => #{"TagNum" => "718" ,"Type" => "INT" ,"ValidValues" =>[{"0", "PROCESS_REQUEST_AS_MARGIN_DISPOSITION"},{"1", "DELTA_PLUS"},{"2", "DELTA_MINUS"},{"3", "FINAL"}]}
, "718" => #{"Name"=>"AdjustmentType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "PROCESS_REQUEST_AS_MARGIN_DISPOSITION"},{"1", "DELTA_PLUS"},{"2", "DELTA_MINUS"},{"3", "FINAL"}], "TagNum" => "718"}


,
"ContraryInstructionIndicator" => #{"TagNum" => "719" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "719" => #{"Name"=>"ContraryInstructionIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "719"}


,
"PriorSpreadIndicator" => #{"TagNum" => "720" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "720" => #{"Name"=>"PriorSpreadIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "720"}


,
"PosMaintRptID" => #{"TagNum" => "721" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "721" => #{"Name"=>"PosMaintRptID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "721"}


,
"PosMaintStatus" => #{"TagNum" => "722" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ACCEPTED"},{"1", "ACCEPTED_WITH_WARNINGS"},{"2", "REJECTED"},{"3", "COMPLETED"},{"4", "COMPLETED_WITH_WARNINGS"}]}
, "722" => #{"Name"=>"PosMaintStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ACCEPTED"},{"1", "ACCEPTED_WITH_WARNINGS"},{"2", "REJECTED"},{"3", "COMPLETED"},{"4", "COMPLETED_WITH_WARNINGS"}], "TagNum" => "722"}


,
"PosMaintResult" => #{"TagNum" => "723" ,"Type" => "INT" ,"ValidValues" =>[{"0", "SUCCESSFUL_COMPLETION"},{"1", "REJECTED"},{"99", "OTHER"}]}
, "723" => #{"Name"=>"PosMaintResult" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "SUCCESSFUL_COMPLETION"},{"1", "REJECTED"},{"99", "OTHER"}], "TagNum" => "723"}


,
"PosReqType" => #{"TagNum" => "724" ,"Type" => "INT" ,"ValidValues" =>[{"0", "POSITIONS"},{"1", "TRADES"},{"2", "EXERCISES"},{"3", "ASSIGNMENTS"},{"4", "SETTLEMENT_ACTIVITY"},{"5", "BACKOUT_MESSAGE"},{"6", "DELTA_POSITIONS"}]}
, "724" => #{"Name"=>"PosReqType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "POSITIONS"},{"1", "TRADES"},{"2", "EXERCISES"},{"3", "ASSIGNMENTS"},{"4", "SETTLEMENT_ACTIVITY"},{"5", "BACKOUT_MESSAGE"},{"6", "DELTA_POSITIONS"}], "TagNum" => "724"}


,
"ResponseTransportType" => #{"TagNum" => "725" ,"Type" => "INT" ,"ValidValues" =>[{"0", "INBAND"},{"1", "OUT_OF_BAND"}]}
, "725" => #{"Name"=>"ResponseTransportType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "INBAND"},{"1", "OUT_OF_BAND"}], "TagNum" => "725"}


,
"ResponseDestination" => #{"TagNum" => "726" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "726" => #{"Name"=>"ResponseDestination" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "726"}


,
"TotalNumPosReports" => #{"TagNum" => "727" ,"Type" => "INT" ,"ValidValues" =>[]}
, "727" => #{"Name"=>"TotalNumPosReports" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "727"}


,
"PosReqResult" => #{"TagNum" => "728" ,"Type" => "INT" ,"ValidValues" =>[{"0", "VALID_REQUEST"},{"1", "INVALID_OR_UNSUPPORTED_REQUEST"},{"2", "NO_POSITIONS_FOUND_THAT_MATCH_CRITERIA"},{"3", "NOT_AUTHORIZED_TO_REQUEST_POSITIONS"},{"4", "REQUEST_FOR_POSITION_NOT_SUPPORTED"},{"99", "OTHER"}]}
, "728" => #{"Name"=>"PosReqResult" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "VALID_REQUEST"},{"1", "INVALID_OR_UNSUPPORTED_REQUEST"},{"2", "NO_POSITIONS_FOUND_THAT_MATCH_CRITERIA"},{"3", "NOT_AUTHORIZED_TO_REQUEST_POSITIONS"},{"4", "REQUEST_FOR_POSITION_NOT_SUPPORTED"},{"99", "OTHER"}], "TagNum" => "728"}


,
"PosReqStatus" => #{"TagNum" => "729" ,"Type" => "INT" ,"ValidValues" =>[{"0", "COMPLETED"},{"1", "COMPLETED_WITH_WARNINGS"},{"2", "REJECTED"}]}
, "729" => #{"Name"=>"PosReqStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "COMPLETED"},{"1", "COMPLETED_WITH_WARNINGS"},{"2", "REJECTED"}], "TagNum" => "729"}


,
"SettlPrice" => #{"TagNum" => "730" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "730" => #{"Name"=>"SettlPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "730"}


,
"SettlPriceType" => #{"TagNum" => "731" ,"Type" => "INT" ,"ValidValues" =>[{"1", "FINAL"},{"2", "THEORETICAL"}]}
, "731" => #{"Name"=>"SettlPriceType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "FINAL"},{"2", "THEORETICAL"}], "TagNum" => "731"}


,
"UnderlyingSettlPrice" => #{"TagNum" => "732" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "732" => #{"Name"=>"UnderlyingSettlPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "732"}


,
"UnderlyingSettlPriceType" => #{"TagNum" => "733" ,"Type" => "INT" ,"ValidValues" =>[]}
, "733" => #{"Name"=>"UnderlyingSettlPriceType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "733"}


,
"PriorSettlPrice" => #{"TagNum" => "734" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "734" => #{"Name"=>"PriorSettlPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "734"}


,
"NoQuoteQualifiers" => #{"TagNum" => "735" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "735" => #{"Name"=>"NoQuoteQualifiers" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "735"}


,
"AllocSettlCurrency" => #{"TagNum" => "736" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "736" => #{"Name"=>"AllocSettlCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "736"}


,
"AllocSettlCurrAmt" => #{"TagNum" => "737" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "737" => #{"Name"=>"AllocSettlCurrAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "737"}


,
"InterestAtMaturity" => #{"TagNum" => "738" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "738" => #{"Name"=>"InterestAtMaturity" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "738"}


,
"LegDatedDate" => #{"TagNum" => "739" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "739" => #{"Name"=>"LegDatedDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "739"}


,
"LegPool" => #{"TagNum" => "740" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "740" => #{"Name"=>"LegPool" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "740"}


,
"AllocInterestAtMaturity" => #{"TagNum" => "741" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "741" => #{"Name"=>"AllocInterestAtMaturity" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "741"}


,
"AllocAccruedInterestAmt" => #{"TagNum" => "742" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "742" => #{"Name"=>"AllocAccruedInterestAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "742"}


,
"DeliveryDate" => #{"TagNum" => "743" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "743" => #{"Name"=>"DeliveryDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "743"}


,
"AssignmentMethod" => #{"TagNum" => "744" ,"Type" => "CHAR" ,"ValidValues" =>[{"P", "PRO_RATA"},{"R", "RANDOM"}]}
, "744" => #{"Name"=>"AssignmentMethod" ,"Type"=>"CHAR" ,"ValidValues"=>[{"P", "PRO_RATA"},{"R", "RANDOM"}], "TagNum" => "744"}


,
"AssignmentUnit" => #{"TagNum" => "745" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "745" => #{"Name"=>"AssignmentUnit" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "745"}


,
"OpenInterest" => #{"TagNum" => "746" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "746" => #{"Name"=>"OpenInterest" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "746"}


,
"ExerciseMethod" => #{"TagNum" => "747" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "AUTOMATIC"},{"M", "MANUAL"}]}
, "747" => #{"Name"=>"ExerciseMethod" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "AUTOMATIC"},{"M", "MANUAL"}], "TagNum" => "747"}


,
"TotNumTradeReports" => #{"TagNum" => "748" ,"Type" => "INT" ,"ValidValues" =>[]}
, "748" => #{"Name"=>"TotNumTradeReports" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "748"}


,
"TradeRequestResult" => #{"TagNum" => "749" ,"Type" => "INT" ,"ValidValues" =>[{"0", "SUCCESSFUL"},{"1", "INVALID_OR_UNKNOWN_INSTRUMENT"},{"2", "INVALID_TYPE_OF_TRADE_REQUESTED"},{"3", "INVALID_PARTIES"},{"4", "INVALID_TRANSPORT_TYPE_REQUESTED"},{"5", "INVALID_DESTINATION_REQUESTED"},{"8", "TRADEREQUESTTYPE_NOT_SUPPORTED"},{"9", "NOT_AUTHORIZED"},{"99", "OTHER"}]}
, "749" => #{"Name"=>"TradeRequestResult" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "SUCCESSFUL"},{"1", "INVALID_OR_UNKNOWN_INSTRUMENT"},{"2", "INVALID_TYPE_OF_TRADE_REQUESTED"},{"3", "INVALID_PARTIES"},{"4", "INVALID_TRANSPORT_TYPE_REQUESTED"},{"5", "INVALID_DESTINATION_REQUESTED"},{"8", "TRADEREQUESTTYPE_NOT_SUPPORTED"},{"9", "NOT_AUTHORIZED"},{"99", "OTHER"}], "TagNum" => "749"}


,
"TradeRequestStatus" => #{"TagNum" => "750" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ACCEPTED"},{"1", "COMPLETED"},{"2", "REJECTED"}]}
, "750" => #{"Name"=>"TradeRequestStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ACCEPTED"},{"1", "COMPLETED"},{"2", "REJECTED"}], "TagNum" => "750"}


,
"TradeReportRejectReason" => #{"TagNum" => "751" ,"Type" => "INT" ,"ValidValues" =>[{"0", "SUCCESSFUL"},{"1", "INVALID_PARTY_ONFORMATION"},{"2", "UNKNOWN_INSTRUMENT"},{"3", "UNAUTHORIZED_TO_REPORT_TRADES"},{"4", "INVALID_TRADE_TYPE"},{"99", "OTHER"}]}
, "751" => #{"Name"=>"TradeReportRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "SUCCESSFUL"},{"1", "INVALID_PARTY_ONFORMATION"},{"2", "UNKNOWN_INSTRUMENT"},{"3", "UNAUTHORIZED_TO_REPORT_TRADES"},{"4", "INVALID_TRADE_TYPE"},{"99", "OTHER"}], "TagNum" => "751"}


,
"SideMultiLegReportingType" => #{"TagNum" => "752" ,"Type" => "INT" ,"ValidValues" =>[{"1", "SINGLE_SECURITY"},{"2", "INDIVIDUAL_LEG_OF_A_MULTILEG_SECURITY"},{"3", "MULTILEG_SECURITY"}]}
, "752" => #{"Name"=>"SideMultiLegReportingType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "SINGLE_SECURITY"},{"2", "INDIVIDUAL_LEG_OF_A_MULTILEG_SECURITY"},{"3", "MULTILEG_SECURITY"}], "TagNum" => "752"}


,
"NoPosAmt" => #{"TagNum" => "753" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "753" => #{"Name"=>"NoPosAmt" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "753"}


,
"AutoAcceptIndicator" => #{"TagNum" => "754" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "754" => #{"Name"=>"AutoAcceptIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "754"}


,
"AllocReportID" => #{"TagNum" => "755" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "755" => #{"Name"=>"AllocReportID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "755"}


,
"NoNested2PartyIDs" => #{"TagNum" => "756" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "756" => #{"Name"=>"NoNested2PartyIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "756"}


,
"Nested2PartyID" => #{"TagNum" => "757" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "757" => #{"Name"=>"Nested2PartyID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "757"}


,
"Nested2PartyIDSource" => #{"TagNum" => "758" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "758" => #{"Name"=>"Nested2PartyIDSource" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "758"}


,
"Nested2PartyRole" => #{"TagNum" => "759" ,"Type" => "INT" ,"ValidValues" =>[]}
, "759" => #{"Name"=>"Nested2PartyRole" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "759"}


,
"Nested2PartySubID" => #{"TagNum" => "760" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "760" => #{"Name"=>"Nested2PartySubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "760"}


,
"BenchmarkSecurityIDSource" => #{"TagNum" => "761" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "761" => #{"Name"=>"BenchmarkSecurityIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "761"}


,
"SecuritySubType" => #{"TagNum" => "762" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "762" => #{"Name"=>"SecuritySubType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "762"}


,
"UnderlyingSecuritySubType" => #{"TagNum" => "763" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "763" => #{"Name"=>"UnderlyingSecuritySubType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "763"}


,
"LegSecuritySubType" => #{"TagNum" => "764" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "764" => #{"Name"=>"LegSecuritySubType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "764"}


,
"AllowableOneSidednessPct" => #{"TagNum" => "765" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "765" => #{"Name"=>"AllowableOneSidednessPct" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "765"}


,
"AllowableOneSidednessValue" => #{"TagNum" => "766" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "766" => #{"Name"=>"AllowableOneSidednessValue" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "766"}


,
"AllowableOneSidednessCurr" => #{"TagNum" => "767" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "767" => #{"Name"=>"AllowableOneSidednessCurr" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "767"}


,
"NoTrdRegTimestamps" => #{"TagNum" => "768" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "768" => #{"Name"=>"NoTrdRegTimestamps" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "768"}


,
"TrdRegTimestamp" => #{"TagNum" => "769" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "769" => #{"Name"=>"TrdRegTimestamp" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "769"}


,
"TrdRegTimestampType" => #{"TagNum" => "770" ,"Type" => "INT" ,"ValidValues" =>[{"1", "EXECUTION_TIME"},{"2", "TIME_IN"},{"3", "TIME_OUT"},{"4", "BROKER_RECEIPT"},{"5", "BROKER_EXECUTION"},{"6", "DESK_RECEIPT"},{"7", "SUBMISSION_TO_CLEARING"}]}
, "770" => #{"Name"=>"TrdRegTimestampType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "EXECUTION_TIME"},{"2", "TIME_IN"},{"3", "TIME_OUT"},{"4", "BROKER_RECEIPT"},{"5", "BROKER_EXECUTION"},{"6", "DESK_RECEIPT"},{"7", "SUBMISSION_TO_CLEARING"}], "TagNum" => "770"}


,
"TrdRegTimestampOrigin" => #{"TagNum" => "771" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "771" => #{"Name"=>"TrdRegTimestampOrigin" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "771"}


,
"ConfirmRefID" => #{"TagNum" => "772" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "772" => #{"Name"=>"ConfirmRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "772"}


,
"ConfirmType" => #{"TagNum" => "773" ,"Type" => "INT" ,"ValidValues" =>[{"1", "STATUS"},{"2", "CONFIRMATION"},{"3", "CONFIRMATION_REQUEST_REJECTED"}]}
, "773" => #{"Name"=>"ConfirmType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "STATUS"},{"2", "CONFIRMATION"},{"3", "CONFIRMATION_REQUEST_REJECTED"}], "TagNum" => "773"}


,
"ConfirmRejReason" => #{"TagNum" => "774" ,"Type" => "INT" ,"ValidValues" =>[{"1", "MISMATCHED_ACCOUNT"},{"2", "MISSING_SETTLEMENT_INSTRUCTIONS"},{"99", "OTHER"}]}
, "774" => #{"Name"=>"ConfirmRejReason" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "MISMATCHED_ACCOUNT"},{"2", "MISSING_SETTLEMENT_INSTRUCTIONS"},{"99", "OTHER"}], "TagNum" => "774"}


,
"BookingType" => #{"TagNum" => "775" ,"Type" => "INT" ,"ValidValues" =>[{"0", "REGULAR_BOOKING"},{"1", "CFD"},{"2", "TOTAL_RETURN_SWAP"}]}
, "775" => #{"Name"=>"BookingType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "REGULAR_BOOKING"},{"1", "CFD"},{"2", "TOTAL_RETURN_SWAP"}], "TagNum" => "775"}


,
"IndividualAllocRejCode" => #{"TagNum" => "776" ,"Type" => "INT" ,"ValidValues" =>[]}
, "776" => #{"Name"=>"IndividualAllocRejCode" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "776"}


,
"SettlInstMsgID" => #{"TagNum" => "777" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "777" => #{"Name"=>"SettlInstMsgID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "777"}


,
"NoSettlInst" => #{"TagNum" => "778" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "778" => #{"Name"=>"NoSettlInst" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "778"}


,
"LastUpdateTime" => #{"TagNum" => "779" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "779" => #{"Name"=>"LastUpdateTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "779"}


,
"AllocSettlInstType" => #{"TagNum" => "780" ,"Type" => "INT" ,"ValidValues" =>[{"0", "USE_DEFAULT_INSTRUCTIONS"},{"1", "DERIVE_FROM_PARAMETERS_PROVIDED"},{"2", "FULL_DETAILS_PROVIDED"},{"3", "SSI_DB_IDS_PROVIDED"},{"4", "PHONE_FOR_INSTRUCTIONS"}]}
, "780" => #{"Name"=>"AllocSettlInstType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "USE_DEFAULT_INSTRUCTIONS"},{"1", "DERIVE_FROM_PARAMETERS_PROVIDED"},{"2", "FULL_DETAILS_PROVIDED"},{"3", "SSI_DB_IDS_PROVIDED"},{"4", "PHONE_FOR_INSTRUCTIONS"}], "TagNum" => "780"}


,
"NoSettlPartyIDs" => #{"TagNum" => "781" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "781" => #{"Name"=>"NoSettlPartyIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "781"}


,
"SettlPartyID" => #{"TagNum" => "782" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "782" => #{"Name"=>"SettlPartyID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "782"}


,
"SettlPartyIDSource" => #{"TagNum" => "783" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "783" => #{"Name"=>"SettlPartyIDSource" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "783"}


,
"SettlPartyRole" => #{"TagNum" => "784" ,"Type" => "INT" ,"ValidValues" =>[]}
, "784" => #{"Name"=>"SettlPartyRole" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "784"}


,
"SettlPartySubID" => #{"TagNum" => "785" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "785" => #{"Name"=>"SettlPartySubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "785"}


,
"SettlPartySubIDType" => #{"TagNum" => "786" ,"Type" => "INT" ,"ValidValues" =>[]}
, "786" => #{"Name"=>"SettlPartySubIDType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "786"}


,
"DlvyInstType" => #{"TagNum" => "787" ,"Type" => "CHAR" ,"ValidValues" =>[{"C", "CASH"},{"S", "SECURITIES"}]}
, "787" => #{"Name"=>"DlvyInstType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"C", "CASH"},{"S", "SECURITIES"}], "TagNum" => "787"}


,
"TerminationType" => #{"TagNum" => "788" ,"Type" => "INT" ,"ValidValues" =>[{"1", "OVERNIGHT"},{"2", "TERM"},{"3", "FLEXIBLE"},{"4", "OPEN"}]}
, "788" => #{"Name"=>"TerminationType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "OVERNIGHT"},{"2", "TERM"},{"3", "FLEXIBLE"},{"4", "OPEN"}], "TagNum" => "788"}


,
"NextExpectedMsgSeqNum" => #{"TagNum" => "789" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "789" => #{"Name"=>"NextExpectedMsgSeqNum" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "789"}


,
"OrdStatusReqID" => #{"TagNum" => "790" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "790" => #{"Name"=>"OrdStatusReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "790"}


,
"SettlInstReqID" => #{"TagNum" => "791" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "791" => #{"Name"=>"SettlInstReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "791"}


,
"SettlInstReqRejCode" => #{"TagNum" => "792" ,"Type" => "INT" ,"ValidValues" =>[{"0", "UNABLE_TO_PROCESS_REQUEST"},{"1", "UNKNOWN_ACCOUNT"},{"2", "NO_MATCHING_SETTLEMENT_INSTRUCTIONS_FOUND"},{"99", "OTHER"}]}
, "792" => #{"Name"=>"SettlInstReqRejCode" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "UNABLE_TO_PROCESS_REQUEST"},{"1", "UNKNOWN_ACCOUNT"},{"2", "NO_MATCHING_SETTLEMENT_INSTRUCTIONS_FOUND"},{"99", "OTHER"}], "TagNum" => "792"}


,
"SecondaryAllocID" => #{"TagNum" => "793" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "793" => #{"Name"=>"SecondaryAllocID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "793"}


,
"AllocReportType" => #{"TagNum" => "794" ,"Type" => "INT" ,"ValidValues" =>[{"2", "PRELIMINARY_REQUEST_TO_INTERMEDIARY"},{"3", "SELLSIDE_CALCULATED_USING_PRELIMINARY"},{"4", "SELLSIDE_CALCULATED_WITHOUT_PRELIMINARY"},{"5", "WAREHOUSE_RECAP"},{"8", "REQUEST_TO_INTERMEDIARY"},{"9", "ACCEPT"},{"10", "REJECT"},{"11", "ACCEPT_PENDING"},{"12", "COMPLETE"},{"14", "REVERSE_PENDING"}]}
, "794" => #{"Name"=>"AllocReportType" ,"Type"=>"INT" ,"ValidValues"=>[{"2", "PRELIMINARY_REQUEST_TO_INTERMEDIARY"},{"3", "SELLSIDE_CALCULATED_USING_PRELIMINARY"},{"4", "SELLSIDE_CALCULATED_WITHOUT_PRELIMINARY"},{"5", "WAREHOUSE_RECAP"},{"8", "REQUEST_TO_INTERMEDIARY"},{"9", "ACCEPT"},{"10", "REJECT"},{"11", "ACCEPT_PENDING"},{"12", "COMPLETE"},{"14", "REVERSE_PENDING"}], "TagNum" => "794"}


,
"AllocReportRefID" => #{"TagNum" => "795" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "795" => #{"Name"=>"AllocReportRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "795"}


,
"AllocCancReplaceReason" => #{"TagNum" => "796" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ORIGINAL_DETAILS_INCOMPLETE_INCORRECT"},{"2", "CHANGE_IN_UNDERLYING_ORDER_DETAILS"},{"99", "OTHER"}]}
, "796" => #{"Name"=>"AllocCancReplaceReason" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ORIGINAL_DETAILS_INCOMPLETE_INCORRECT"},{"2", "CHANGE_IN_UNDERLYING_ORDER_DETAILS"},{"99", "OTHER"}], "TagNum" => "796"}


,
"CopyMsgIndicator" => #{"TagNum" => "797" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "797" => #{"Name"=>"CopyMsgIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "797"}


,
"AllocAccountType" => #{"TagNum" => "798" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ACCOUNT_IS_CARRIED_PN_CUSTOMER_SIDE_OF_BOOKS"},{"2", "ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS"},{"3", "HOUSE_TRADER"},{"4", "FLOOR_TRADER"},{"6", "ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS_AND_IS_CROSS_MARGINED"},{"7", "ACCOUNT_IS_HOUSE_TRADER_AND_IS_CROSS_MARGINED"},{"8", "JOINT_BACK_OFFICE_ACCOUNT"}]}
, "798" => #{"Name"=>"AllocAccountType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ACCOUNT_IS_CARRIED_PN_CUSTOMER_SIDE_OF_BOOKS"},{"2", "ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS"},{"3", "HOUSE_TRADER"},{"4", "FLOOR_TRADER"},{"6", "ACCOUNT_IS_CARRIED_ON_NON_CUSTOMER_SIDE_OF_BOOKS_AND_IS_CROSS_MARGINED"},{"7", "ACCOUNT_IS_HOUSE_TRADER_AND_IS_CROSS_MARGINED"},{"8", "JOINT_BACK_OFFICE_ACCOUNT"}], "TagNum" => "798"}


,
"OrderAvgPx" => #{"TagNum" => "799" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "799" => #{"Name"=>"OrderAvgPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "799"}


,
"OrderBookingQty" => #{"TagNum" => "800" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "800" => #{"Name"=>"OrderBookingQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "800"}


,
"NoSettlPartySubIDs" => #{"TagNum" => "801" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "801" => #{"Name"=>"NoSettlPartySubIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "801"}


,
"NoPartySubIDs" => #{"TagNum" => "802" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "802" => #{"Name"=>"NoPartySubIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "802"}


,
"PartySubIDType" => #{"TagNum" => "803" ,"Type" => "INT" ,"ValidValues" =>[{"1", "FIRM"},{"2", "PERSON"},{"3", "SYSTEM"},{"4", "APPLICATION"},{"5", "FULL_LEGAL_NAME_OF_FIRM"},{"6", "POSTAL_ADDRESS"},{"7", "PHONE_NUMBER"},{"8", "EMAIL_ADDRESS"},{"9", "CONTACT_NAME"},{"10", "SECURITIES_ACCOUNT_NUMBER"},{"11", "REGISTRATION_NUMBER"},{"12", "REGISTERED_ADDRESS_12"},{"13", "REGULATORY_STATUS"},{"14", "REGISTRATION_NAME"},{"15", "CASH_ACCOUNT_NUMBER"},{"16", "BIC"},{"17", "CSD_PARTICIPANT_MEMBER_CODE"},{"18", "REGISTERED_ADDRESS_18"},{"19", "FUND_ACCOUNT_NAME"},{"20", "TELEX_NUMBER"},{"21", "FAX_NUMBER"},{"22", "SECURITIES_ACCOUNT_NAME"},{"23", "CASH_ACCOUNT_NAME"},{"24", "DEPARTMENT"},{"25", "LOCATION_DESK"},{"26", "POSITION_ACCOUNT_TYPE"},{"27", "SECURITY_LOCATE_ID"},{"28", "MARKET_MAKER"},{"29", "ELIGIBLE_COUNTERPARTY"},{"30", "PROFESSIONAL_CLIENT"},{"31", "LOCATION"},{"32", "EXECUTION_VENUE"},{"33", "CURRENCY_DELIVERY_IDENTIFIER"}]}
, "803" => #{"Name"=>"PartySubIDType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "FIRM"},{"2", "PERSON"},{"3", "SYSTEM"},{"4", "APPLICATION"},{"5", "FULL_LEGAL_NAME_OF_FIRM"},{"6", "POSTAL_ADDRESS"},{"7", "PHONE_NUMBER"},{"8", "EMAIL_ADDRESS"},{"9", "CONTACT_NAME"},{"10", "SECURITIES_ACCOUNT_NUMBER"},{"11", "REGISTRATION_NUMBER"},{"12", "REGISTERED_ADDRESS_12"},{"13", "REGULATORY_STATUS"},{"14", "REGISTRATION_NAME"},{"15", "CASH_ACCOUNT_NUMBER"},{"16", "BIC"},{"17", "CSD_PARTICIPANT_MEMBER_CODE"},{"18", "REGISTERED_ADDRESS_18"},{"19", "FUND_ACCOUNT_NAME"},{"20", "TELEX_NUMBER"},{"21", "FAX_NUMBER"},{"22", "SECURITIES_ACCOUNT_NAME"},{"23", "CASH_ACCOUNT_NAME"},{"24", "DEPARTMENT"},{"25", "LOCATION_DESK"},{"26", "POSITION_ACCOUNT_TYPE"},{"27", "SECURITY_LOCATE_ID"},{"28", "MARKET_MAKER"},{"29", "ELIGIBLE_COUNTERPARTY"},{"30", "PROFESSIONAL_CLIENT"},{"31", "LOCATION"},{"32", "EXECUTION_VENUE"},{"33", "CURRENCY_DELIVERY_IDENTIFIER"}], "TagNum" => "803"}


,
"NoNestedPartySubIDs" => #{"TagNum" => "804" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "804" => #{"Name"=>"NoNestedPartySubIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "804"}


,
"NestedPartySubIDType" => #{"TagNum" => "805" ,"Type" => "INT" ,"ValidValues" =>[]}
, "805" => #{"Name"=>"NestedPartySubIDType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "805"}


,
"NoNested2PartySubIDs" => #{"TagNum" => "806" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "806" => #{"Name"=>"NoNested2PartySubIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "806"}


,
"Nested2PartySubIDType" => #{"TagNum" => "807" ,"Type" => "INT" ,"ValidValues" =>[]}
, "807" => #{"Name"=>"Nested2PartySubIDType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "807"}


,
"AllocIntermedReqType" => #{"TagNum" => "808" ,"Type" => "INT" ,"ValidValues" =>[{"1", "PENDING_ACCEPT"},{"2", "PENDING_RELEASE"},{"3", "PENDING_REVERSAL"},{"4", "ACCEPT"},{"5", "BLOCK_LEVEL_REJECT"},{"6", "ACCOUNT_LEVEL_REJECT"}]}
, "808" => #{"Name"=>"AllocIntermedReqType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "PENDING_ACCEPT"},{"2", "PENDING_RELEASE"},{"3", "PENDING_REVERSAL"},{"4", "ACCEPT"},{"5", "BLOCK_LEVEL_REJECT"},{"6", "ACCOUNT_LEVEL_REJECT"}], "TagNum" => "808"}


,
"UnderlyingPx" => #{"TagNum" => "810" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "810" => #{"Name"=>"UnderlyingPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "810"}


,
"PriceDelta" => #{"TagNum" => "811" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "811" => #{"Name"=>"PriceDelta" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "811"}


,
"ApplQueueMax" => #{"TagNum" => "812" ,"Type" => "INT" ,"ValidValues" =>[]}
, "812" => #{"Name"=>"ApplQueueMax" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "812"}


,
"ApplQueueDepth" => #{"TagNum" => "813" ,"Type" => "INT" ,"ValidValues" =>[]}
, "813" => #{"Name"=>"ApplQueueDepth" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "813"}


,
"ApplQueueResolution" => #{"TagNum" => "814" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NO_ACTION_TAKEN"},{"1", "QUEUE_FLUSHED"},{"2", "OVERLAY_LAST"},{"3", "END_SESSION"}]}
, "814" => #{"Name"=>"ApplQueueResolution" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NO_ACTION_TAKEN"},{"1", "QUEUE_FLUSHED"},{"2", "OVERLAY_LAST"},{"3", "END_SESSION"}], "TagNum" => "814"}


,
"ApplQueueAction" => #{"TagNum" => "815" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NO_ACTION_TAKEN"},{"1", "QUEUE_FLUSHED"},{"2", "OVERLAY_LAST"},{"3", "END_SESSION"}]}
, "815" => #{"Name"=>"ApplQueueAction" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NO_ACTION_TAKEN"},{"1", "QUEUE_FLUSHED"},{"2", "OVERLAY_LAST"},{"3", "END_SESSION"}], "TagNum" => "815"}


,
"NoAltMDSource" => #{"TagNum" => "816" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "816" => #{"Name"=>"NoAltMDSource" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "816"}


,
"AltMDSourceID" => #{"TagNum" => "817" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "817" => #{"Name"=>"AltMDSourceID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "817"}


,
"SecondaryTradeReportID" => #{"TagNum" => "818" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "818" => #{"Name"=>"SecondaryTradeReportID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "818"}


,
"AvgPxIndicator" => #{"TagNum" => "819" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NO_AVERAGE_PRICING"},{"1", "TRADE_IS_PART_OF_AN_AVERAGE_PRICE_GROUP_IDENTIFIED_BY_THE_TRADELINKID"},{"2", "LAST_TRADE_IS_THE_AVERAGE_PRICE_GROUP_IDENTIFIED_BY_THE_TRADELINKID"}]}
, "819" => #{"Name"=>"AvgPxIndicator" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NO_AVERAGE_PRICING"},{"1", "TRADE_IS_PART_OF_AN_AVERAGE_PRICE_GROUP_IDENTIFIED_BY_THE_TRADELINKID"},{"2", "LAST_TRADE_IS_THE_AVERAGE_PRICE_GROUP_IDENTIFIED_BY_THE_TRADELINKID"}], "TagNum" => "819"}


,
"TradeLinkID" => #{"TagNum" => "820" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "820" => #{"Name"=>"TradeLinkID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "820"}


,
"OrderInputDevice" => #{"TagNum" => "821" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "821" => #{"Name"=>"OrderInputDevice" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "821"}


,
"UnderlyingTradingSessionID" => #{"TagNum" => "822" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "822" => #{"Name"=>"UnderlyingTradingSessionID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "822"}


,
"UnderlyingTradingSessionSubID" => #{"TagNum" => "823" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "823" => #{"Name"=>"UnderlyingTradingSessionSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "823"}


,
"TradeLegRefID" => #{"TagNum" => "824" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "824" => #{"Name"=>"TradeLegRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "824"}


,
"ExchangeRule" => #{"TagNum" => "825" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "825" => #{"Name"=>"ExchangeRule" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "825"}


,
"TradeAllocIndicator" => #{"TagNum" => "826" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ALLOCATION_NOT_REQUIRED"},{"1", "ALLOCATION_REQUIRED"},{"2", "USE_ALLOCATION_PROVIDED_WITH_THE_TRADE"},{"3", "ALLOCATION_GIVE_UP_EXECUTOR"},{"4", "ALLOCATION_FROM_EXECUTOR"},{"5", "ALLOCATION_TO_CLAIM_ACCOUNT"}]}
, "826" => #{"Name"=>"TradeAllocIndicator" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ALLOCATION_NOT_REQUIRED"},{"1", "ALLOCATION_REQUIRED"},{"2", "USE_ALLOCATION_PROVIDED_WITH_THE_TRADE"},{"3", "ALLOCATION_GIVE_UP_EXECUTOR"},{"4", "ALLOCATION_FROM_EXECUTOR"},{"5", "ALLOCATION_TO_CLAIM_ACCOUNT"}], "TagNum" => "826"}


,
"ExpirationCycle" => #{"TagNum" => "827" ,"Type" => "INT" ,"ValidValues" =>[{"0", "EXPIRE_ON_TRADING_SESSION_CLOSE"},{"1", "EXPIRE_ON_TRADING_SESSION_OPEN"},{"2", "TRADING_ELIGIBILITY_EXPIRATION_SPECIFIED_IN_THE_DATE_AND_TIME_FIELDS_EVENTDATE"}]}
, "827" => #{"Name"=>"ExpirationCycle" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "EXPIRE_ON_TRADING_SESSION_CLOSE"},{"1", "EXPIRE_ON_TRADING_SESSION_OPEN"},{"2", "TRADING_ELIGIBILITY_EXPIRATION_SPECIFIED_IN_THE_DATE_AND_TIME_FIELDS_EVENTDATE"}], "TagNum" => "827"}


,
"TrdType" => #{"TagNum" => "828" ,"Type" => "INT" ,"ValidValues" =>[{"0", "REGULAR_TRADE"},{"1", "BLOCK_TRADE_1"},{"2", "EFP"},{"3", "TRANSFER"},{"4", "LATE_TRADE"},{"5", "T_TRADE"},{"6", "WEIGHTED_AVERAGE_PRICE_TRADE"},{"7", "BUNCHED_TRADE"},{"8", "LATE_BUNCHED_TRADE"},{"9", "PRIOR_REFERENCE_PRICE_TRADE"},{"10", "AFTER_HOURS_TRADE"},{"11", "EXCHANGE_FOR_RISK"},{"12", "EXCHANGE_FOR_SWAP"},{"13", "EXCHANGE_OF_FUTURES_FOR"},{"14", "EXCHANGE_OF_OPTIONS_FOR_OPTIONS"},{"15", "TRADING_AT_SETTLEMENT"},{"16", "ALL_OR_NONE"},{"17", "FUTURES_LARGE_ORDER_EXECUTION"},{"18", "EXCHANGE_OF_FUTURES_FOR_FUTURES"},{"19", "OPTION_INTERIM_TRADE"},{"20", "OPTION_CABINET_TRADE"},{"22", "PRIVATELY_NEGOTIATED_TRADES"},{"23", "SUBSTITUTION_OF_FUTURES_FOR_FORWARDS"},{"48", "NON_STANDARD_SETTLEMENT"},{"49", "DERIVATIVE_RELATED_TRANSACTION"},{"50", "PORTFOLIO_TRADE"},{"51", "VOLUME_WEIGHTED_AVERAGE_TRADE"},{"52", "EXCHANGE_GRANTED_TRADE"},{"53", "REPURCHASE_AGREEMENT"},{"54", "OTC"},{"55", "EXCHANGE_BASIS_FACILITY"},{"24", "ERROR_TRADE"},{"25", "SPECIAL_CUM_DIVIDEND"},{"26", "SPECIAL_EX_DIVIDEND"},{"27", "SPECIAL_CUM_COUPON"},{"28", "SPECIAL_EX_COUPON"},{"29", "CASH_SETTLEMENT"},{"30", "SPECIAL_PRICE"},{"31", "GUARANTEED_DELIVERY"},{"32", "SPECIAL_CUM_RIGHTS"},{"33", "SPECIAL_EX_RIGHTS"},{"34", "SPECIAL_CUM_CAPITAL_REPAYMENTS"},{"35", "SPECIAL_EX_CAPITAL_REPAYMENTS"},{"36", "SPECIAL_CUM_BONUS"},{"37", "SPECIAL_EX_BONUS"},{"38", "BLOCK_TRADE_38"},{"39", "WORKED_PRINCIPAL_TRADE"},{"40", "BLOCK_TRADES"},{"41", "NAME_CHANGE"},{"42", "PORTFOLIO_TRANSFER"},{"43", "PROROGATION_BUY"},{"44", "PROROGATION_SELL"},{"45", "OPTION_EXERCISE"},{"46", "DELTA_NEUTRAL_TRANSACTION"},{"47", "FINANCING_TRANSACTION"}]}
, "828" => #{"Name"=>"TrdType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "REGULAR_TRADE"},{"1", "BLOCK_TRADE_1"},{"2", "EFP"},{"3", "TRANSFER"},{"4", "LATE_TRADE"},{"5", "T_TRADE"},{"6", "WEIGHTED_AVERAGE_PRICE_TRADE"},{"7", "BUNCHED_TRADE"},{"8", "LATE_BUNCHED_TRADE"},{"9", "PRIOR_REFERENCE_PRICE_TRADE"},{"10", "AFTER_HOURS_TRADE"},{"11", "EXCHANGE_FOR_RISK"},{"12", "EXCHANGE_FOR_SWAP"},{"13", "EXCHANGE_OF_FUTURES_FOR"},{"14", "EXCHANGE_OF_OPTIONS_FOR_OPTIONS"},{"15", "TRADING_AT_SETTLEMENT"},{"16", "ALL_OR_NONE"},{"17", "FUTURES_LARGE_ORDER_EXECUTION"},{"18", "EXCHANGE_OF_FUTURES_FOR_FUTURES"},{"19", "OPTION_INTERIM_TRADE"},{"20", "OPTION_CABINET_TRADE"},{"22", "PRIVATELY_NEGOTIATED_TRADES"},{"23", "SUBSTITUTION_OF_FUTURES_FOR_FORWARDS"},{"48", "NON_STANDARD_SETTLEMENT"},{"49", "DERIVATIVE_RELATED_TRANSACTION"},{"50", "PORTFOLIO_TRADE"},{"51", "VOLUME_WEIGHTED_AVERAGE_TRADE"},{"52", "EXCHANGE_GRANTED_TRADE"},{"53", "REPURCHASE_AGREEMENT"},{"54", "OTC"},{"55", "EXCHANGE_BASIS_FACILITY"},{"24", "ERROR_TRADE"},{"25", "SPECIAL_CUM_DIVIDEND"},{"26", "SPECIAL_EX_DIVIDEND"},{"27", "SPECIAL_CUM_COUPON"},{"28", "SPECIAL_EX_COUPON"},{"29", "CASH_SETTLEMENT"},{"30", "SPECIAL_PRICE"},{"31", "GUARANTEED_DELIVERY"},{"32", "SPECIAL_CUM_RIGHTS"},{"33", "SPECIAL_EX_RIGHTS"},{"34", "SPECIAL_CUM_CAPITAL_REPAYMENTS"},{"35", "SPECIAL_EX_CAPITAL_REPAYMENTS"},{"36", "SPECIAL_CUM_BONUS"},{"37", "SPECIAL_EX_BONUS"},{"38", "BLOCK_TRADE_38"},{"39", "WORKED_PRINCIPAL_TRADE"},{"40", "BLOCK_TRADES"},{"41", "NAME_CHANGE"},{"42", "PORTFOLIO_TRANSFER"},{"43", "PROROGATION_BUY"},{"44", "PROROGATION_SELL"},{"45", "OPTION_EXERCISE"},{"46", "DELTA_NEUTRAL_TRANSACTION"},{"47", "FINANCING_TRANSACTION"}], "TagNum" => "828"}


,
"TrdSubType" => #{"TagNum" => "829" ,"Type" => "INT" ,"ValidValues" =>[{"0", "CMTA"},{"1", "INTERNAL_TRANSFER_OR_ADJUSTMENT"},{"2", "EXTERNAL_TRANSFER_OR_TRANSFER_OF_ACCOUNT"},{"3", "REJECT_FOR_SUBMITTING_SIDE"},{"4", "ADVISORY_FOR_CONTRA_SIDE"},{"5", "OFFSET_DUE_TO_AN_ALLOCATION"},{"6", "ONSET_DUE_TO_AN_ALLOCATION"},{"7", "DIFFERENTIAL_SPREAD"},{"8", "IMPLIED_SPREAD_LEG_EXECUTED_AGAINST_AN_OUTRIGHT"},{"9", "TRANSACTION_FROM_EXERCISE"},{"10", "TRANSACTION_FROM_ASSIGNMENT"},{"11", "ACATS"},{"33", "OFF_HOURS_TRADE"},{"34", "ON_HOURS_TRADE"},{"35", "OTC_QUOTE"},{"36", "CONVERTED_SWAP"},{"14", "AI"},{"15", "B"},{"16", "K"},{"17", "LC"},{"18", "M"},{"19", "N"},{"20", "NM"},{"21", "NR"},{"22", "P"},{"23", "PA"},{"24", "PC"},{"25", "PN"},{"26", "R"},{"27", "RO"},{"28", "RT"},{"29", "SW"},{"30", "T"},{"31", "WN"},{"32", "WT"},{"37", "CROSSED_TRADE"},{"38", "INTERIM_PROTECTED_TRADE"},{"39", "LARGE_IN_SCALE"}]}
, "829" => #{"Name"=>"TrdSubType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "CMTA"},{"1", "INTERNAL_TRANSFER_OR_ADJUSTMENT"},{"2", "EXTERNAL_TRANSFER_OR_TRANSFER_OF_ACCOUNT"},{"3", "REJECT_FOR_SUBMITTING_SIDE"},{"4", "ADVISORY_FOR_CONTRA_SIDE"},{"5", "OFFSET_DUE_TO_AN_ALLOCATION"},{"6", "ONSET_DUE_TO_AN_ALLOCATION"},{"7", "DIFFERENTIAL_SPREAD"},{"8", "IMPLIED_SPREAD_LEG_EXECUTED_AGAINST_AN_OUTRIGHT"},{"9", "TRANSACTION_FROM_EXERCISE"},{"10", "TRANSACTION_FROM_ASSIGNMENT"},{"11", "ACATS"},{"33", "OFF_HOURS_TRADE"},{"34", "ON_HOURS_TRADE"},{"35", "OTC_QUOTE"},{"36", "CONVERTED_SWAP"},{"14", "AI"},{"15", "B"},{"16", "K"},{"17", "LC"},{"18", "M"},{"19", "N"},{"20", "NM"},{"21", "NR"},{"22", "P"},{"23", "PA"},{"24", "PC"},{"25", "PN"},{"26", "R"},{"27", "RO"},{"28", "RT"},{"29", "SW"},{"30", "T"},{"31", "WN"},{"32", "WT"},{"37", "CROSSED_TRADE"},{"38", "INTERIM_PROTECTED_TRADE"},{"39", "LARGE_IN_SCALE"}], "TagNum" => "829"}


,
"TransferReason" => #{"TagNum" => "830" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "830" => #{"Name"=>"TransferReason" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "830"}


,
"TotNumAssignmentReports" => #{"TagNum" => "832" ,"Type" => "INT" ,"ValidValues" =>[]}
, "832" => #{"Name"=>"TotNumAssignmentReports" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "832"}


,
"AsgnRptID" => #{"TagNum" => "833" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "833" => #{"Name"=>"AsgnRptID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "833"}


,
"ThresholdAmount" => #{"TagNum" => "834" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "834" => #{"Name"=>"ThresholdAmount" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "834"}


,
"PegMoveType" => #{"TagNum" => "835" ,"Type" => "INT" ,"ValidValues" =>[{"0", "FLOATING"},{"1", "FIXED"}]}
, "835" => #{"Name"=>"PegMoveType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "FLOATING"},{"1", "FIXED"}], "TagNum" => "835"}


,
"PegOffsetType" => #{"TagNum" => "836" ,"Type" => "INT" ,"ValidValues" =>[{"0", "PRICE"},{"1", "BASIS_POINTS"},{"2", "TICKS"},{"3", "PRICE_TIER"}]}
, "836" => #{"Name"=>"PegOffsetType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "PRICE"},{"1", "BASIS_POINTS"},{"2", "TICKS"},{"3", "PRICE_TIER"}], "TagNum" => "836"}


,
"PegLimitType" => #{"TagNum" => "837" ,"Type" => "INT" ,"ValidValues" =>[{"0", "OR_BETTER"},{"1", "STRICT"},{"2", "OR_WORSE"}]}
, "837" => #{"Name"=>"PegLimitType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "OR_BETTER"},{"1", "STRICT"},{"2", "OR_WORSE"}], "TagNum" => "837"}


,
"PegRoundDirection" => #{"TagNum" => "838" ,"Type" => "INT" ,"ValidValues" =>[{"1", "MORE_AGGRESSIVE"},{"2", "MORE_PASSIVE"}]}
, "838" => #{"Name"=>"PegRoundDirection" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "MORE_AGGRESSIVE"},{"2", "MORE_PASSIVE"}], "TagNum" => "838"}


,
"PeggedPrice" => #{"TagNum" => "839" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "839" => #{"Name"=>"PeggedPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "839"}


,
"PegScope" => #{"TagNum" => "840" ,"Type" => "INT" ,"ValidValues" =>[{"1", "LOCAL"},{"2", "NATIONAL"},{"3", "GLOBAL"},{"4", "NATIONAL_EXCLUDING_LOCAL"}]}
, "840" => #{"Name"=>"PegScope" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "LOCAL"},{"2", "NATIONAL"},{"3", "GLOBAL"},{"4", "NATIONAL_EXCLUDING_LOCAL"}], "TagNum" => "840"}


,
"DiscretionMoveType" => #{"TagNum" => "841" ,"Type" => "INT" ,"ValidValues" =>[{"0", "FLOATING"},{"1", "FIXED"}]}
, "841" => #{"Name"=>"DiscretionMoveType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "FLOATING"},{"1", "FIXED"}], "TagNum" => "841"}


,
"DiscretionOffsetType" => #{"TagNum" => "842" ,"Type" => "INT" ,"ValidValues" =>[{"0", "PRICE"},{"1", "BASIS_POINTS"},{"2", "TICKS"},{"3", "PRICE_TIER"}]}
, "842" => #{"Name"=>"DiscretionOffsetType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "PRICE"},{"1", "BASIS_POINTS"},{"2", "TICKS"},{"3", "PRICE_TIER"}], "TagNum" => "842"}


,
"DiscretionLimitType" => #{"TagNum" => "843" ,"Type" => "INT" ,"ValidValues" =>[{"0", "OR_BETTER"},{"1", "STRICT"},{"2", "OR_WORSE"}]}
, "843" => #{"Name"=>"DiscretionLimitType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "OR_BETTER"},{"1", "STRICT"},{"2", "OR_WORSE"}], "TagNum" => "843"}


,
"DiscretionRoundDirection" => #{"TagNum" => "844" ,"Type" => "INT" ,"ValidValues" =>[{"1", "MORE_AGGRESSIVE"},{"2", "MORE_PASSIVE"}]}
, "844" => #{"Name"=>"DiscretionRoundDirection" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "MORE_AGGRESSIVE"},{"2", "MORE_PASSIVE"}], "TagNum" => "844"}


,
"DiscretionPrice" => #{"TagNum" => "845" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "845" => #{"Name"=>"DiscretionPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "845"}


,
"DiscretionScope" => #{"TagNum" => "846" ,"Type" => "INT" ,"ValidValues" =>[{"1", "LOCAL"},{"2", "NATIONAL"},{"3", "GLOBAL"},{"4", "NATIONAL_EXCLUDING_LOCAL"}]}
, "846" => #{"Name"=>"DiscretionScope" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "LOCAL"},{"2", "NATIONAL"},{"3", "GLOBAL"},{"4", "NATIONAL_EXCLUDING_LOCAL"}], "TagNum" => "846"}


,
"TargetStrategy" => #{"TagNum" => "847" ,"Type" => "INT" ,"ValidValues" =>[{"1", "VWAP"},{"2", "PARTICIPATE"},{"3", "MININIZE_MARKET_IMPACT"}]}
, "847" => #{"Name"=>"TargetStrategy" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "VWAP"},{"2", "PARTICIPATE"},{"3", "MININIZE_MARKET_IMPACT"}], "TagNum" => "847"}


,
"TargetStrategyParameters" => #{"TagNum" => "848" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "848" => #{"Name"=>"TargetStrategyParameters" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "848"}


,
"ParticipationRate" => #{"TagNum" => "849" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "849" => #{"Name"=>"ParticipationRate" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "849"}


,
"TargetStrategyPerformance" => #{"TagNum" => "850" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "850" => #{"Name"=>"TargetStrategyPerformance" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "850"}


,
"LastLiquidityInd" => #{"TagNum" => "851" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ADDED_LIQUIDITY"},{"2", "REMOVED_LIQUIDITY"},{"3", "LIQUIDITY_ROUTED_OUT"},{"4", "AUCTION"}]}
, "851" => #{"Name"=>"LastLiquidityInd" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ADDED_LIQUIDITY"},{"2", "REMOVED_LIQUIDITY"},{"3", "LIQUIDITY_ROUTED_OUT"},{"4", "AUCTION"}], "TagNum" => "851"}


,
"PublishTrdIndicator" => #{"TagNum" => "852" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "852" => #{"Name"=>"PublishTrdIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "852"}


,
"ShortSaleReason" => #{"TagNum" => "853" ,"Type" => "INT" ,"ValidValues" =>[{"0", "DEALER_SOLD_SHORT"},{"1", "DEALER_SOLD_SHORT_EXEMPT"},{"2", "SELLING_CUSTOMER_SOLD_SHORT"},{"3", "SELLING_CUSTOMER_SOLD_SHORT_EXEMPT"},{"4", "QUALIFIED_SERVICE_REPRESENTATIVE"},{"5", "QSR_OR_AGU_CONTRA_SIDE_SOLD_SHORT_EXEMPT"}]}
, "853" => #{"Name"=>"ShortSaleReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "DEALER_SOLD_SHORT"},{"1", "DEALER_SOLD_SHORT_EXEMPT"},{"2", "SELLING_CUSTOMER_SOLD_SHORT"},{"3", "SELLING_CUSTOMER_SOLD_SHORT_EXEMPT"},{"4", "QUALIFIED_SERVICE_REPRESENTATIVE"},{"5", "QSR_OR_AGU_CONTRA_SIDE_SOLD_SHORT_EXEMPT"}], "TagNum" => "853"}


,
"QtyType" => #{"TagNum" => "854" ,"Type" => "INT" ,"ValidValues" =>[{"0", "UNITS"},{"1", "CONTRACTS"},{"2", "UNITS_OF_MEASURE_PER_TIME_UNIT"}]}
, "854" => #{"Name"=>"QtyType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "UNITS"},{"1", "CONTRACTS"},{"2", "UNITS_OF_MEASURE_PER_TIME_UNIT"}], "TagNum" => "854"}


,
"SecondaryTrdType" => #{"TagNum" => "855" ,"Type" => "INT" ,"ValidValues" =>[]}
, "855" => #{"Name"=>"SecondaryTrdType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "855"}


,
"TradeReportType" => #{"TagNum" => "856" ,"Type" => "INT" ,"ValidValues" =>[{"0", "SUBMIT"},{"1", "ALLEGED_1"},{"2", "ACCEPT"},{"3", "DECLINE"},{"4", "ADDENDUM"},{"5", "NO_WAS"},{"6", "TRADE_REPORT_CANCEL"},{"7", "7"},{"8", "DEFAULTED"},{"9", "INVALID_CMTA"},{"10", "PENDED"},{"11", "ALLEGED_NEW"},{"12", "ALLEGED_ADDENDUM"},{"13", "ALLEGED_NO_WAS"},{"14", "ALLEGED_TRADE_REPORT_CANCEL"},{"15", "ALLEGED_15"}]}
, "856" => #{"Name"=>"TradeReportType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "SUBMIT"},{"1", "ALLEGED_1"},{"2", "ACCEPT"},{"3", "DECLINE"},{"4", "ADDENDUM"},{"5", "NO_WAS"},{"6", "TRADE_REPORT_CANCEL"},{"7", "7"},{"8", "DEFAULTED"},{"9", "INVALID_CMTA"},{"10", "PENDED"},{"11", "ALLEGED_NEW"},{"12", "ALLEGED_ADDENDUM"},{"13", "ALLEGED_NO_WAS"},{"14", "ALLEGED_TRADE_REPORT_CANCEL"},{"15", "ALLEGED_15"}], "TagNum" => "856"}


,
"AllocNoOrdersType" => #{"TagNum" => "857" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NOT_SPECIFIED"},{"1", "EXPLICIT_LIST_PROVIDED"}]}
, "857" => #{"Name"=>"AllocNoOrdersType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NOT_SPECIFIED"},{"1", "EXPLICIT_LIST_PROVIDED"}], "TagNum" => "857"}


,
"SharedCommission" => #{"TagNum" => "858" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "858" => #{"Name"=>"SharedCommission" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "858"}


,
"ConfirmReqID" => #{"TagNum" => "859" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "859" => #{"Name"=>"ConfirmReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "859"}


,
"AvgParPx" => #{"TagNum" => "860" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "860" => #{"Name"=>"AvgParPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "860"}


,
"ReportedPx" => #{"TagNum" => "861" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "861" => #{"Name"=>"ReportedPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "861"}


,
"NoCapacities" => #{"TagNum" => "862" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "862" => #{"Name"=>"NoCapacities" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "862"}


,
"OrderCapacityQty" => #{"TagNum" => "863" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "863" => #{"Name"=>"OrderCapacityQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "863"}


,
"NoEvents" => #{"TagNum" => "864" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "864" => #{"Name"=>"NoEvents" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "864"}


,
"EventType" => #{"TagNum" => "865" ,"Type" => "INT" ,"ValidValues" =>[{"1", "PUT"},{"2", "CALL"},{"3", "TENDER"},{"4", "SINKING_FUND_CALL"},{"5", "ACTIVATION"},{"6", "INACTIVIATION"},{"7", "LAST_ELIGIBLE_TRADE_DATE"},{"8", "SWAP_START_DATE"},{"9", "SWAP_END_DATE"},{"10", "SWAP_ROLL_DATE"},{"11", "SWAP_NEXT_START_DATE"},{"12", "SWAP_NEXT_ROLL_DATE"},{"13", "FIRST_DELIVERY_DATE"},{"14", "LAST_DELIVERY_DATE"},{"15", "INITIAL_INVENTORY_DUE_DATE"},{"16", "FINAL_INVENTORY_DUE_DATE"},{"17", "FIRST_INTENT_DATE"},{"18", "LAST_INTENT_DATE"},{"19", "POSITION_REMOVAL_DATE"},{"99", "OTHER"}]}
, "865" => #{"Name"=>"EventType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "PUT"},{"2", "CALL"},{"3", "TENDER"},{"4", "SINKING_FUND_CALL"},{"5", "ACTIVATION"},{"6", "INACTIVIATION"},{"7", "LAST_ELIGIBLE_TRADE_DATE"},{"8", "SWAP_START_DATE"},{"9", "SWAP_END_DATE"},{"10", "SWAP_ROLL_DATE"},{"11", "SWAP_NEXT_START_DATE"},{"12", "SWAP_NEXT_ROLL_DATE"},{"13", "FIRST_DELIVERY_DATE"},{"14", "LAST_DELIVERY_DATE"},{"15", "INITIAL_INVENTORY_DUE_DATE"},{"16", "FINAL_INVENTORY_DUE_DATE"},{"17", "FIRST_INTENT_DATE"},{"18", "LAST_INTENT_DATE"},{"19", "POSITION_REMOVAL_DATE"},{"99", "OTHER"}], "TagNum" => "865"}


,
"EventDate" => #{"TagNum" => "866" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "866" => #{"Name"=>"EventDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "866"}


,
"EventPx" => #{"TagNum" => "867" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "867" => #{"Name"=>"EventPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "867"}


,
"EventText" => #{"TagNum" => "868" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "868" => #{"Name"=>"EventText" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "868"}


,
"PctAtRisk" => #{"TagNum" => "869" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "869" => #{"Name"=>"PctAtRisk" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "869"}


,
"NoInstrAttrib" => #{"TagNum" => "870" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "870" => #{"Name"=>"NoInstrAttrib" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "870"}


,
"InstrAttribType" => #{"TagNum" => "871" ,"Type" => "INT" ,"ValidValues" =>[{"1", "FLAT"},{"2", "ZERO_COUPON"},{"3", "INTEREST_BEARING"},{"4", "NO_PERIODIC_PAYMENTS"},{"5", "VARIABLE_RATE"},{"6", "LESS_FEE_FOR_PUT"},{"7", "STEPPED_COUPON"},{"8", "COUPON_PERIOD"},{"9", "WHEN_AND_IF_ISSUED"},{"10", "ORIGINAL_ISSUE_DISCOUNT"},{"11", "CALLABLE_PUTTABLE"},{"12", "ESCROWED_TO_MATURITY"},{"13", "ESCROWED_TO_REDEMPTION_DATE"},{"14", "PRE_REFUNDED"},{"15", "IN_DEFAULT"},{"16", "UNRATED"},{"17", "TAXABLE"},{"18", "INDEXED"},{"19", "SUBJECT_TO_ALTERNATIVE_MINIMUM_TAX"},{"20", "ORIGINAL_ISSUE_DISCOUNT_PRICE_SUPPLY_PRICE_IN_THE_INSTRATTRIBVALUE"},{"21", "CALLABLE_BELOW_MATURITY_VALUE"},{"22", "CALLABLE_WITHOUT_NOTICE_BY_MAIL_TO_HOLDER_UNLESS_REGISTERED"},{"23", "PRICE_TICK_RULES_FOR_SECURITY"},{"24", "TRADE_TYPE_ELIGIBILITY_DETAILS_FOR_SECURITY"},{"25", "INSTRUMENT_DENOMINATOR"},{"26", "INSTRUMENT_NUMERATOR"},{"27", "INSTRUMENT_PRICE_PRECISION"},{"28", "INSTRUMENT_STRIKE_PRICE"},{"29", "TRADEABLE_INDICATOR"},{"99", "TEXT_SUPPLY_THE_TEXT_OF_THE_ATTRIBUTE_OR_DISCLAIMER_IN_THE_INSTRATTRIBVALUE"}]}
, "871" => #{"Name"=>"InstrAttribType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "FLAT"},{"2", "ZERO_COUPON"},{"3", "INTEREST_BEARING"},{"4", "NO_PERIODIC_PAYMENTS"},{"5", "VARIABLE_RATE"},{"6", "LESS_FEE_FOR_PUT"},{"7", "STEPPED_COUPON"},{"8", "COUPON_PERIOD"},{"9", "WHEN_AND_IF_ISSUED"},{"10", "ORIGINAL_ISSUE_DISCOUNT"},{"11", "CALLABLE_PUTTABLE"},{"12", "ESCROWED_TO_MATURITY"},{"13", "ESCROWED_TO_REDEMPTION_DATE"},{"14", "PRE_REFUNDED"},{"15", "IN_DEFAULT"},{"16", "UNRATED"},{"17", "TAXABLE"},{"18", "INDEXED"},{"19", "SUBJECT_TO_ALTERNATIVE_MINIMUM_TAX"},{"20", "ORIGINAL_ISSUE_DISCOUNT_PRICE_SUPPLY_PRICE_IN_THE_INSTRATTRIBVALUE"},{"21", "CALLABLE_BELOW_MATURITY_VALUE"},{"22", "CALLABLE_WITHOUT_NOTICE_BY_MAIL_TO_HOLDER_UNLESS_REGISTERED"},{"23", "PRICE_TICK_RULES_FOR_SECURITY"},{"24", "TRADE_TYPE_ELIGIBILITY_DETAILS_FOR_SECURITY"},{"25", "INSTRUMENT_DENOMINATOR"},{"26", "INSTRUMENT_NUMERATOR"},{"27", "INSTRUMENT_PRICE_PRECISION"},{"28", "INSTRUMENT_STRIKE_PRICE"},{"29", "TRADEABLE_INDICATOR"},{"99", "TEXT_SUPPLY_THE_TEXT_OF_THE_ATTRIBUTE_OR_DISCLAIMER_IN_THE_INSTRATTRIBVALUE"}], "TagNum" => "871"}


,
"InstrAttribValue" => #{"TagNum" => "872" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "872" => #{"Name"=>"InstrAttribValue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "872"}


,
"DatedDate" => #{"TagNum" => "873" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "873" => #{"Name"=>"DatedDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "873"}


,
"InterestAccrualDate" => #{"TagNum" => "874" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "874" => #{"Name"=>"InterestAccrualDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "874"}


,
"CPProgram" => #{"TagNum" => "875" ,"Type" => "INT" ,"ValidValues" =>[{"1", "3"},{"2", "4"},{"99", "OTHER"}]}
, "875" => #{"Name"=>"CPProgram" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "3"},{"2", "4"},{"99", "OTHER"}], "TagNum" => "875"}


,
"CPRegType" => #{"TagNum" => "876" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "876" => #{"Name"=>"CPRegType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "876"}


,
"UnderlyingCPProgram" => #{"TagNum" => "877" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "877" => #{"Name"=>"UnderlyingCPProgram" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "877"}


,
"UnderlyingCPRegType" => #{"TagNum" => "878" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "878" => #{"Name"=>"UnderlyingCPRegType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "878"}


,
"UnderlyingQty" => #{"TagNum" => "879" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "879" => #{"Name"=>"UnderlyingQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "879"}


,
"TrdMatchID" => #{"TagNum" => "880" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "880" => #{"Name"=>"TrdMatchID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "880"}


,
"SecondaryTradeReportRefID" => #{"TagNum" => "881" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "881" => #{"Name"=>"SecondaryTradeReportRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "881"}


,
"UnderlyingDirtyPrice" => #{"TagNum" => "882" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "882" => #{"Name"=>"UnderlyingDirtyPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "882"}


,
"UnderlyingEndPrice" => #{"TagNum" => "883" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "883" => #{"Name"=>"UnderlyingEndPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "883"}


,
"UnderlyingStartValue" => #{"TagNum" => "884" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "884" => #{"Name"=>"UnderlyingStartValue" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "884"}


,
"UnderlyingCurrentValue" => #{"TagNum" => "885" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "885" => #{"Name"=>"UnderlyingCurrentValue" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "885"}


,
"UnderlyingEndValue" => #{"TagNum" => "886" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "886" => #{"Name"=>"UnderlyingEndValue" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "886"}


,
"NoUnderlyingStips" => #{"TagNum" => "887" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "887" => #{"Name"=>"NoUnderlyingStips" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "887"}


,
"UnderlyingStipType" => #{"TagNum" => "888" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "888" => #{"Name"=>"UnderlyingStipType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "888"}


,
"UnderlyingStipValue" => #{"TagNum" => "889" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "889" => #{"Name"=>"UnderlyingStipValue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "889"}


,
"MaturityNetMoney" => #{"TagNum" => "890" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "890" => #{"Name"=>"MaturityNetMoney" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "890"}


,
"MiscFeeBasis" => #{"TagNum" => "891" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ABSOLUTE"},{"1", "PER_UNIT"},{"2", "PERCENTAGE"}]}
, "891" => #{"Name"=>"MiscFeeBasis" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ABSOLUTE"},{"1", "PER_UNIT"},{"2", "PERCENTAGE"}], "TagNum" => "891"}


,
"TotNoAllocs" => #{"TagNum" => "892" ,"Type" => "INT" ,"ValidValues" =>[]}
, "892" => #{"Name"=>"TotNoAllocs" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "892"}


,
"LastFragment" => #{"TagNum" => "893" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "893" => #{"Name"=>"LastFragment" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "893"}


,
"CollReqID" => #{"TagNum" => "894" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "894" => #{"Name"=>"CollReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "894"}


,
"CollAsgnReason" => #{"TagNum" => "895" ,"Type" => "INT" ,"ValidValues" =>[{"0", "INITIAL"},{"1", "SCHEDULED"},{"2", "TIME_WARNING"},{"3", "MARGIN_DEFICIENCY"},{"4", "MARGIN_EXCESS"},{"5", "FORWARD_COLLATERAL_DEMAND"},{"6", "EVENT_OF_DEFAULT"},{"7", "ADVERSE_TAX_EVENT"}]}
, "895" => #{"Name"=>"CollAsgnReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "INITIAL"},{"1", "SCHEDULED"},{"2", "TIME_WARNING"},{"3", "MARGIN_DEFICIENCY"},{"4", "MARGIN_EXCESS"},{"5", "FORWARD_COLLATERAL_DEMAND"},{"6", "EVENT_OF_DEFAULT"},{"7", "ADVERSE_TAX_EVENT"}], "TagNum" => "895"}


,
"CollInquiryQualifier" => #{"TagNum" => "896" ,"Type" => "INT" ,"ValidValues" =>[{"0", "TRADE_DATE"},{"1", "GC_INSTRUMENT"},{"2", "COLLATERAL_INSTRUMENT"},{"3", "SUBSTITUTION_ELIGIBLE"},{"4", "NOT_ASSIGNED"},{"5", "PARTIALLY_ASSIGNED"},{"6", "FULLY_ASSIGNED"},{"7", "OUTSTANDING_TRADES"}]}
, "896" => #{"Name"=>"CollInquiryQualifier" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "TRADE_DATE"},{"1", "GC_INSTRUMENT"},{"2", "COLLATERAL_INSTRUMENT"},{"3", "SUBSTITUTION_ELIGIBLE"},{"4", "NOT_ASSIGNED"},{"5", "PARTIALLY_ASSIGNED"},{"6", "FULLY_ASSIGNED"},{"7", "OUTSTANDING_TRADES"}], "TagNum" => "896"}


,
"NoTrades" => #{"TagNum" => "897" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "897" => #{"Name"=>"NoTrades" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "897"}


,
"MarginRatio" => #{"TagNum" => "898" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "898" => #{"Name"=>"MarginRatio" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "898"}


,
"MarginExcess" => #{"TagNum" => "899" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "899" => #{"Name"=>"MarginExcess" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "899"}


,
"TotalNetValue" => #{"TagNum" => "900" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "900" => #{"Name"=>"TotalNetValue" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "900"}


,
"CashOutstanding" => #{"TagNum" => "901" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "901" => #{"Name"=>"CashOutstanding" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "901"}


,
"CollAsgnID" => #{"TagNum" => "902" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "902" => #{"Name"=>"CollAsgnID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "902"}


,
"CollAsgnTransType" => #{"TagNum" => "903" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NEW"},{"1", "REPLACE"},{"2", "CANCEL"},{"3", "RELEASE"},{"4", "REVERSE"}]}
, "903" => #{"Name"=>"CollAsgnTransType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NEW"},{"1", "REPLACE"},{"2", "CANCEL"},{"3", "RELEASE"},{"4", "REVERSE"}], "TagNum" => "903"}


,
"CollRespID" => #{"TagNum" => "904" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "904" => #{"Name"=>"CollRespID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "904"}


,
"CollAsgnRespType" => #{"TagNum" => "905" ,"Type" => "INT" ,"ValidValues" =>[{"0", "RECEIVED"},{"1", "ACCEPTED"},{"2", "DECLINED"},{"3", "REJECTED"}]}
, "905" => #{"Name"=>"CollAsgnRespType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "RECEIVED"},{"1", "ACCEPTED"},{"2", "DECLINED"},{"3", "REJECTED"}], "TagNum" => "905"}


,
"CollAsgnRejectReason" => #{"TagNum" => "906" ,"Type" => "INT" ,"ValidValues" =>[{"0", "UNKNOWN_DEAL"},{"1", "UNKNOWN_OR_INVALID_INSTRUMENT"},{"2", "UNAUTHORIZED_TRANSACTION"},{"3", "INSUFFICIENT_COLLATERAL"},{"4", "INVALID_TYPE_OF_COLLATERAL"},{"5", "EXCESSIVE_SUBSTITUTION"},{"99", "OTHER"}]}
, "906" => #{"Name"=>"CollAsgnRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "UNKNOWN_DEAL"},{"1", "UNKNOWN_OR_INVALID_INSTRUMENT"},{"2", "UNAUTHORIZED_TRANSACTION"},{"3", "INSUFFICIENT_COLLATERAL"},{"4", "INVALID_TYPE_OF_COLLATERAL"},{"5", "EXCESSIVE_SUBSTITUTION"},{"99", "OTHER"}], "TagNum" => "906"}


,
"CollAsgnRefID" => #{"TagNum" => "907" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "907" => #{"Name"=>"CollAsgnRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "907"}


,
"CollRptID" => #{"TagNum" => "908" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "908" => #{"Name"=>"CollRptID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "908"}


,
"CollInquiryID" => #{"TagNum" => "909" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "909" => #{"Name"=>"CollInquiryID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "909"}


,
"CollStatus" => #{"TagNum" => "910" ,"Type" => "INT" ,"ValidValues" =>[{"0", "UNASSIGNED"},{"1", "PARTIALLY_ASSIGNED"},{"2", "ASSIGNMENT_PROPOSED"},{"3", "ASSIGNED"},{"4", "CHALLENGED"}]}
, "910" => #{"Name"=>"CollStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "UNASSIGNED"},{"1", "PARTIALLY_ASSIGNED"},{"2", "ASSIGNMENT_PROPOSED"},{"3", "ASSIGNED"},{"4", "CHALLENGED"}], "TagNum" => "910"}


,
"TotNumReports" => #{"TagNum" => "911" ,"Type" => "INT" ,"ValidValues" =>[]}
, "911" => #{"Name"=>"TotNumReports" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "911"}


,
"LastRptRequested" => #{"TagNum" => "912" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"N", "NO"},{"Y", "YES"}]}
, "912" => #{"Name"=>"LastRptRequested" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"N", "NO"},{"Y", "YES"}], "TagNum" => "912"}


,
"AgreementDesc" => #{"TagNum" => "913" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "913" => #{"Name"=>"AgreementDesc" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "913"}


,
"AgreementID" => #{"TagNum" => "914" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "914" => #{"Name"=>"AgreementID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "914"}


,
"AgreementDate" => #{"TagNum" => "915" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "915" => #{"Name"=>"AgreementDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "915"}


,
"StartDate" => #{"TagNum" => "916" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "916" => #{"Name"=>"StartDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "916"}


,
"EndDate" => #{"TagNum" => "917" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "917" => #{"Name"=>"EndDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "917"}


,
"AgreementCurrency" => #{"TagNum" => "918" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "918" => #{"Name"=>"AgreementCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "918"}


,
"DeliveryType" => #{"TagNum" => "919" ,"Type" => "INT" ,"ValidValues" =>[{"0", "VERSUS_PAYMENT_DELIVER"},{"1", "FREE_DELIVER"},{"2", "TRI_PARTY"},{"3", "HOLD_IN_CUSTODY"}]}
, "919" => #{"Name"=>"DeliveryType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "VERSUS_PAYMENT_DELIVER"},{"1", "FREE_DELIVER"},{"2", "TRI_PARTY"},{"3", "HOLD_IN_CUSTODY"}], "TagNum" => "919"}


,
"EndAccruedInterestAmt" => #{"TagNum" => "920" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "920" => #{"Name"=>"EndAccruedInterestAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "920"}


,
"StartCash" => #{"TagNum" => "921" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "921" => #{"Name"=>"StartCash" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "921"}


,
"EndCash" => #{"TagNum" => "922" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "922" => #{"Name"=>"EndCash" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "922"}


,
"UserRequestID" => #{"TagNum" => "923" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "923" => #{"Name"=>"UserRequestID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "923"}


,
"UserRequestType" => #{"TagNum" => "924" ,"Type" => "INT" ,"ValidValues" =>[{"1", "LOG_ON_USER"},{"2", "LOG_OFF_USER"},{"3", "CHANGE_PASSWORD_FOR_USER"},{"4", "REQUEST_INDIVIDUAL_USER_STATUS"}]}
, "924" => #{"Name"=>"UserRequestType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "LOG_ON_USER"},{"2", "LOG_OFF_USER"},{"3", "CHANGE_PASSWORD_FOR_USER"},{"4", "REQUEST_INDIVIDUAL_USER_STATUS"}], "TagNum" => "924"}


,
"NewPassword" => #{"TagNum" => "925" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "925" => #{"Name"=>"NewPassword" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "925"}


,
"UserStatus" => #{"TagNum" => "926" ,"Type" => "INT" ,"ValidValues" =>[{"1", "LOGGED_IN"},{"2", "NOT_LOGGED_IN"},{"3", "USER_NOT_RECOGNISED"},{"4", "PASSWORD_INCORRECT"},{"5", "PASSWORD_CHANGED"},{"6", "OTHER"},{"7", "FORCED_USER_LOGOUT_BY_EXCHANGE"},{"8", "SESSION_SHUTDOWN_WARNING"}]}
, "926" => #{"Name"=>"UserStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "LOGGED_IN"},{"2", "NOT_LOGGED_IN"},{"3", "USER_NOT_RECOGNISED"},{"4", "PASSWORD_INCORRECT"},{"5", "PASSWORD_CHANGED"},{"6", "OTHER"},{"7", "FORCED_USER_LOGOUT_BY_EXCHANGE"},{"8", "SESSION_SHUTDOWN_WARNING"}], "TagNum" => "926"}


,
"UserStatusText" => #{"TagNum" => "927" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "927" => #{"Name"=>"UserStatusText" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "927"}


,
"StatusValue" => #{"TagNum" => "928" ,"Type" => "INT" ,"ValidValues" =>[{"1", "CONNECTED"},{"2", "NOT_CONNECTED_2"},{"3", "NOT_CONNECTED_3"},{"4", "IN_PROCESS"}]}
, "928" => #{"Name"=>"StatusValue" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "CONNECTED"},{"2", "NOT_CONNECTED_2"},{"3", "NOT_CONNECTED_3"},{"4", "IN_PROCESS"}], "TagNum" => "928"}


,
"StatusText" => #{"TagNum" => "929" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "929" => #{"Name"=>"StatusText" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "929"}


,
"RefCompID" => #{"TagNum" => "930" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "930" => #{"Name"=>"RefCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "930"}


,
"RefSubID" => #{"TagNum" => "931" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "931" => #{"Name"=>"RefSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "931"}


,
"NetworkResponseID" => #{"TagNum" => "932" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "932" => #{"Name"=>"NetworkResponseID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "932"}


,
"NetworkRequestID" => #{"TagNum" => "933" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "933" => #{"Name"=>"NetworkRequestID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "933"}


,
"LastNetworkResponseID" => #{"TagNum" => "934" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "934" => #{"Name"=>"LastNetworkResponseID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "934"}


,
"NetworkRequestType" => #{"TagNum" => "935" ,"Type" => "INT" ,"ValidValues" =>[{"1", "SNAPSHOT"},{"2", "SUBSCRIBE"},{"4", "STOP_SUBSCRIBING"},{"8", "LEVEL_OF_DETAIL_THEN_NOCOMPIDS_BECOMES_REQUIRED"}]}
, "935" => #{"Name"=>"NetworkRequestType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "SNAPSHOT"},{"2", "SUBSCRIBE"},{"4", "STOP_SUBSCRIBING"},{"8", "LEVEL_OF_DETAIL_THEN_NOCOMPIDS_BECOMES_REQUIRED"}], "TagNum" => "935"}


,
"NoCompIDs" => #{"TagNum" => "936" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "936" => #{"Name"=>"NoCompIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "936"}


,
"NetworkStatusResponseType" => #{"TagNum" => "937" ,"Type" => "INT" ,"ValidValues" =>[{"1", "FULL"},{"2", "INCREMENTAL_UPDATE"}]}
, "937" => #{"Name"=>"NetworkStatusResponseType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "FULL"},{"2", "INCREMENTAL_UPDATE"}], "TagNum" => "937"}


,
"NoCollInquiryQualifier" => #{"TagNum" => "938" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "938" => #{"Name"=>"NoCollInquiryQualifier" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "938"}


,
"TrdRptStatus" => #{"TagNum" => "939" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ACCEPTED"},{"1", "REJECTED"},{"3", "ACCEPTED_WITH_ERRORS"}]}
, "939" => #{"Name"=>"TrdRptStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ACCEPTED"},{"1", "REJECTED"},{"3", "ACCEPTED_WITH_ERRORS"}], "TagNum" => "939"}


,
"AffirmStatus" => #{"TagNum" => "940" ,"Type" => "INT" ,"ValidValues" =>[{"1", "RECEIVED"},{"2", "CONFIRM_REJECTED_IE_NOT_AFFIRMED"},{"3", "AFFIRMED"}]}
, "940" => #{"Name"=>"AffirmStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "RECEIVED"},{"2", "CONFIRM_REJECTED_IE_NOT_AFFIRMED"},{"3", "AFFIRMED"}], "TagNum" => "940"}


,
"UnderlyingStrikeCurrency" => #{"TagNum" => "941" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "941" => #{"Name"=>"UnderlyingStrikeCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "941"}


,
"LegStrikeCurrency" => #{"TagNum" => "942" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "942" => #{"Name"=>"LegStrikeCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "942"}


,
"TimeBracket" => #{"TagNum" => "943" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "943" => #{"Name"=>"TimeBracket" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "943"}


,
"CollAction" => #{"TagNum" => "944" ,"Type" => "INT" ,"ValidValues" =>[{"0", "RETAIN"},{"1", "ADD"},{"2", "REMOVE"}]}
, "944" => #{"Name"=>"CollAction" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "RETAIN"},{"1", "ADD"},{"2", "REMOVE"}], "TagNum" => "944"}


,
"CollInquiryStatus" => #{"TagNum" => "945" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ACCEPTED"},{"1", "ACCEPTED_WITH_WARNINGS"},{"2", "COMPLETED"},{"3", "COMPLETED_WITH_WARNINGS"},{"4", "REJECTED"}]}
, "945" => #{"Name"=>"CollInquiryStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ACCEPTED"},{"1", "ACCEPTED_WITH_WARNINGS"},{"2", "COMPLETED"},{"3", "COMPLETED_WITH_WARNINGS"},{"4", "REJECTED"}], "TagNum" => "945"}


,
"CollInquiryResult" => #{"TagNum" => "946" ,"Type" => "INT" ,"ValidValues" =>[{"0", "SUCCESSFUL"},{"1", "INVALID_OR_UNKNOWN_INSTRUMENT"},{"2", "INVALID_OR_UNKNOWN_COLLATERAL_TYPE"},{"3", "INVALID_PARTIES"},{"4", "INVALID_TRANSPORT_TYPE_REQUESTED"},{"5", "INVALID_DESTINATION_REQUESTED"},{"6", "NO_COLLATERAL_FOUND_FOR_THE_TRADE_SPECIFIED"},{"7", "NO_COLLATERAL_FOUND_FOR_THE_ORDER_SPECIFIED"},{"8", "COLLATERAL_INQUIRY_TYPE_NOT_SUPPORTED"},{"9", "UNAUTHORIZED_FOR_COLLATERAL_INQUIRY"},{"99", "OTHER"}]}
, "946" => #{"Name"=>"CollInquiryResult" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "SUCCESSFUL"},{"1", "INVALID_OR_UNKNOWN_INSTRUMENT"},{"2", "INVALID_OR_UNKNOWN_COLLATERAL_TYPE"},{"3", "INVALID_PARTIES"},{"4", "INVALID_TRANSPORT_TYPE_REQUESTED"},{"5", "INVALID_DESTINATION_REQUESTED"},{"6", "NO_COLLATERAL_FOUND_FOR_THE_TRADE_SPECIFIED"},{"7", "NO_COLLATERAL_FOUND_FOR_THE_ORDER_SPECIFIED"},{"8", "COLLATERAL_INQUIRY_TYPE_NOT_SUPPORTED"},{"9", "UNAUTHORIZED_FOR_COLLATERAL_INQUIRY"},{"99", "OTHER"}], "TagNum" => "946"}


,
"StrikeCurrency" => #{"TagNum" => "947" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "947" => #{"Name"=>"StrikeCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "947"}


,
"NoNested3PartyIDs" => #{"TagNum" => "948" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "948" => #{"Name"=>"NoNested3PartyIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "948"}


,
"Nested3PartyID" => #{"TagNum" => "949" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "949" => #{"Name"=>"Nested3PartyID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "949"}


,
"Nested3PartyIDSource" => #{"TagNum" => "950" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "950" => #{"Name"=>"Nested3PartyIDSource" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "950"}


,
"Nested3PartyRole" => #{"TagNum" => "951" ,"Type" => "INT" ,"ValidValues" =>[]}
, "951" => #{"Name"=>"Nested3PartyRole" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "951"}


,
"NoNested3PartySubIDs" => #{"TagNum" => "952" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "952" => #{"Name"=>"NoNested3PartySubIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "952"}


,
"Nested3PartySubID" => #{"TagNum" => "953" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "953" => #{"Name"=>"Nested3PartySubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "953"}


,
"Nested3PartySubIDType" => #{"TagNum" => "954" ,"Type" => "INT" ,"ValidValues" =>[]}
, "954" => #{"Name"=>"Nested3PartySubIDType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "954"}


,
"LegContractSettlMonth" => #{"TagNum" => "955" ,"Type" => "MONTHYEAR" ,"ValidValues" =>[]}
, "955" => #{"Name"=>"LegContractSettlMonth" ,"Type"=>"MONTHYEAR" ,"ValidValues"=>[], "TagNum" => "955"}


,
"LegInterestAccrualDate" => #{"TagNum" => "956" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "956" => #{"Name"=>"LegInterestAccrualDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "956"}


,
"NoStrategyParameters" => #{"TagNum" => "957" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "957" => #{"Name"=>"NoStrategyParameters" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "957"}


,
"StrategyParameterName" => #{"TagNum" => "958" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "958" => #{"Name"=>"StrategyParameterName" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "958"}


,
"StrategyParameterType" => #{"TagNum" => "959" ,"Type" => "INT" ,"ValidValues" =>[{"1", "INT"},{"2", "LENGTH"},{"3", "NUMINGROUP"},{"4", "SEQNUM"},{"5", "TAGNUM"},{"6", "FLOAT"},{"7", "QTY"},{"8", "PRICE"},{"9", "PRICEOFFSET"},{"10", "AMT"},{"11", "PERCENTAGE"},{"12", "CHAR"},{"13", "BOOLEAN"},{"14", "STRING"},{"15", "MULTIPLECHARVALUE"},{"16", "CURRENCY"},{"17", "EXCHANGE"},{"18", "MONTHYEAR"},{"19", "UTCTIMESTAMP"},{"20", "UTCTIMEONLY"},{"21", "LOCALMKTDATE"},{"22", "UTCDATEONLY"},{"23", "DATA"},{"24", "MULTIPLESTRINGVALUE"},{"25", "COUNTRY"},{"26", "LANGUAGE"},{"27", "TZTIMEONLY"},{"28", "TZTIMESTAMP"},{"29", "TENOR"}]}
, "959" => #{"Name"=>"StrategyParameterType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "INT"},{"2", "LENGTH"},{"3", "NUMINGROUP"},{"4", "SEQNUM"},{"5", "TAGNUM"},{"6", "FLOAT"},{"7", "QTY"},{"8", "PRICE"},{"9", "PRICEOFFSET"},{"10", "AMT"},{"11", "PERCENTAGE"},{"12", "CHAR"},{"13", "BOOLEAN"},{"14", "STRING"},{"15", "MULTIPLECHARVALUE"},{"16", "CURRENCY"},{"17", "EXCHANGE"},{"18", "MONTHYEAR"},{"19", "UTCTIMESTAMP"},{"20", "UTCTIMEONLY"},{"21", "LOCALMKTDATE"},{"22", "UTCDATEONLY"},{"23", "DATA"},{"24", "MULTIPLESTRINGVALUE"},{"25", "COUNTRY"},{"26", "LANGUAGE"},{"27", "TZTIMEONLY"},{"28", "TZTIMESTAMP"},{"29", "TENOR"}], "TagNum" => "959"}


,
"StrategyParameterValue" => #{"TagNum" => "960" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "960" => #{"Name"=>"StrategyParameterValue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "960"}


,
"HostCrossID" => #{"TagNum" => "961" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "961" => #{"Name"=>"HostCrossID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "961"}


,
"SideTimeInForce" => #{"TagNum" => "962" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "962" => #{"Name"=>"SideTimeInForce" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "962"}


,
"MDReportID" => #{"TagNum" => "963" ,"Type" => "INT" ,"ValidValues" =>[]}
, "963" => #{"Name"=>"MDReportID" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "963"}


,
"SecurityReportID" => #{"TagNum" => "964" ,"Type" => "INT" ,"ValidValues" =>[]}
, "964" => #{"Name"=>"SecurityReportID" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "964"}


,
"SecurityStatus" => #{"TagNum" => "965" ,"Type" => "STRING" ,"ValidValues" =>[{"1", "ACTIVE"},{"2", "INACTIVE"}]}
, "965" => #{"Name"=>"SecurityStatus" ,"Type"=>"STRING" ,"ValidValues"=>[{"1", "ACTIVE"},{"2", "INACTIVE"}], "TagNum" => "965"}


,
"SettleOnOpenFlag" => #{"TagNum" => "966" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "966" => #{"Name"=>"SettleOnOpenFlag" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "966"}


,
"StrikeMultiplier" => #{"TagNum" => "967" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "967" => #{"Name"=>"StrikeMultiplier" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "967"}


,
"StrikeValue" => #{"TagNum" => "968" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "968" => #{"Name"=>"StrikeValue" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "968"}


,
"MinPriceIncrement" => #{"TagNum" => "969" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "969" => #{"Name"=>"MinPriceIncrement" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "969"}


,
"PositionLimit" => #{"TagNum" => "970" ,"Type" => "INT" ,"ValidValues" =>[]}
, "970" => #{"Name"=>"PositionLimit" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "970"}


,
"NTPositionLimit" => #{"TagNum" => "971" ,"Type" => "INT" ,"ValidValues" =>[]}
, "971" => #{"Name"=>"NTPositionLimit" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "971"}


,
"UnderlyingAllocationPercent" => #{"TagNum" => "972" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "972" => #{"Name"=>"UnderlyingAllocationPercent" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "972"}


,
"UnderlyingCashAmount" => #{"TagNum" => "973" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "973" => #{"Name"=>"UnderlyingCashAmount" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "973"}


,
"UnderlyingCashType" => #{"TagNum" => "974" ,"Type" => "STRING" ,"ValidValues" =>[{"FIXED", "FIXED"},{"DIFF", "DIFF"}]}
, "974" => #{"Name"=>"UnderlyingCashType" ,"Type"=>"STRING" ,"ValidValues"=>[{"FIXED", "FIXED"},{"DIFF", "DIFF"}], "TagNum" => "974"}


,
"UnderlyingSettlementType" => #{"TagNum" => "975" ,"Type" => "INT" ,"ValidValues" =>[{"2", "T_PLUS_1"},{"4", "T_PLUS_3"},{"5", "T_PLUS_4"}]}
, "975" => #{"Name"=>"UnderlyingSettlementType" ,"Type"=>"INT" ,"ValidValues"=>[{"2", "T_PLUS_1"},{"4", "T_PLUS_3"},{"5", "T_PLUS_4"}], "TagNum" => "975"}


,
"QuantityDate" => #{"TagNum" => "976" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "976" => #{"Name"=>"QuantityDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "976"}


,
"ContIntRptID" => #{"TagNum" => "977" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "977" => #{"Name"=>"ContIntRptID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "977"}


,
"LateIndicator" => #{"TagNum" => "978" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "978" => #{"Name"=>"LateIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "978"}


,
"InputSource" => #{"TagNum" => "979" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "979" => #{"Name"=>"InputSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "979"}


,
"SecurityUpdateAction" => #{"TagNum" => "980" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "ADD"},{"D", "DELETE"},{"M", "MODIFY"}]}
, "980" => #{"Name"=>"SecurityUpdateAction" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "ADD"},{"D", "DELETE"},{"M", "MODIFY"}], "TagNum" => "980"}


,
"NoExpiration" => #{"TagNum" => "981" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "981" => #{"Name"=>"NoExpiration" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "981"}


,
"ExpirationQtyType" => #{"TagNum" => "982" ,"Type" => "INT" ,"ValidValues" =>[{"1", "AUTO_EXERCISE"},{"2", "NON_AUTO_EXERCISE"},{"3", "FINAL_WILL_BE_EXERCISED"},{"4", "CONTRARY_INTENTION"},{"5", "DIFFERENCE"}]}
, "982" => #{"Name"=>"ExpirationQtyType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "AUTO_EXERCISE"},{"2", "NON_AUTO_EXERCISE"},{"3", "FINAL_WILL_BE_EXERCISED"},{"4", "CONTRARY_INTENTION"},{"5", "DIFFERENCE"}], "TagNum" => "982"}


,
"ExpQty" => #{"TagNum" => "983" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "983" => #{"Name"=>"ExpQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "983"}


,
"NoUnderlyingAmounts" => #{"TagNum" => "984" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "984" => #{"Name"=>"NoUnderlyingAmounts" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "984"}


,
"UnderlyingPayAmount" => #{"TagNum" => "985" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "985" => #{"Name"=>"UnderlyingPayAmount" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "985"}


,
"UnderlyingCollectAmount" => #{"TagNum" => "986" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "986" => #{"Name"=>"UnderlyingCollectAmount" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "986"}


,
"UnderlyingSettlementDate" => #{"TagNum" => "987" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "987" => #{"Name"=>"UnderlyingSettlementDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "987"}


,
"UnderlyingSettlementStatus" => #{"TagNum" => "988" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "988" => #{"Name"=>"UnderlyingSettlementStatus" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "988"}


,
"SecondaryIndividualAllocID" => #{"TagNum" => "989" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "989" => #{"Name"=>"SecondaryIndividualAllocID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "989"}


,
"LegReportID" => #{"TagNum" => "990" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "990" => #{"Name"=>"LegReportID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "990"}


,
"RndPx" => #{"TagNum" => "991" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "991" => #{"Name"=>"RndPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "991"}


,
"IndividualAllocType" => #{"TagNum" => "992" ,"Type" => "INT" ,"ValidValues" =>[{"1", "SUB_ALLOCATE"},{"2", "THIRD_PARTY_ALLOCATION"}]}
, "992" => #{"Name"=>"IndividualAllocType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "SUB_ALLOCATE"},{"2", "THIRD_PARTY_ALLOCATION"}], "TagNum" => "992"}


,
"AllocCustomerCapacity" => #{"TagNum" => "993" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "993" => #{"Name"=>"AllocCustomerCapacity" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "993"}


,
"TierCode" => #{"TagNum" => "994" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "994" => #{"Name"=>"TierCode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "994"}


,
"UnitOfMeasure" => #{"TagNum" => "996" ,"Type" => "STRING" ,"ValidValues" =>[{"Bcf", "BILLION_CUBIC_FEET"},{"MMbbl", "MILLION_BARRELS"},{"MMBtu", "ONE_MILLION_BTU"},{"MWh", "MEGAWATT_HOURS"},{"Bbl", "BARRELS"},{"Bu", "BUSHELS"},{"lbs", "POUNDS"},{"Gal", "GALLONS"},{"oz_tr", "TROY_OUNCES"},{"t", "METRIC_TONS"},{"tn", "TONS"},{"USD", "US_DOLLARS"},{"Alw", "ALLOWANCES"}]}
, "996" => #{"Name"=>"UnitOfMeasure" ,"Type"=>"STRING" ,"ValidValues"=>[{"Bcf", "BILLION_CUBIC_FEET"},{"MMbbl", "MILLION_BARRELS"},{"MMBtu", "ONE_MILLION_BTU"},{"MWh", "MEGAWATT_HOURS"},{"Bbl", "BARRELS"},{"Bu", "BUSHELS"},{"lbs", "POUNDS"},{"Gal", "GALLONS"},{"oz_tr", "TROY_OUNCES"},{"t", "METRIC_TONS"},{"tn", "TONS"},{"USD", "US_DOLLARS"},{"Alw", "ALLOWANCES"}], "TagNum" => "996"}


,
"TimeUnit" => #{"TagNum" => "997" ,"Type" => "STRING" ,"ValidValues" =>[{"H", "HOUR"},{"Min", "MINUTE"},{"S", "SECOND"},{"D", "DAY"},{"Wk", "WEEK"},{"Mo", "MONTH"},{"Yr", "YEAR"}]}
, "997" => #{"Name"=>"TimeUnit" ,"Type"=>"STRING" ,"ValidValues"=>[{"H", "HOUR"},{"Min", "MINUTE"},{"S", "SECOND"},{"D", "DAY"},{"Wk", "WEEK"},{"Mo", "MONTH"},{"Yr", "YEAR"}], "TagNum" => "997"}


,
"UnderlyingUnitOfMeasure" => #{"TagNum" => "998" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "998" => #{"Name"=>"UnderlyingUnitOfMeasure" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "998"}


,
"LegUnitOfMeasure" => #{"TagNum" => "999" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "999" => #{"Name"=>"LegUnitOfMeasure" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "999"}


,
"UnderlyingTimeUnit" => #{"TagNum" => "1000" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1000" => #{"Name"=>"UnderlyingTimeUnit" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1000"}


,
"LegTimeUnit" => #{"TagNum" => "1001" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1001" => #{"Name"=>"LegTimeUnit" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1001"}


,
"AllocMethod" => #{"TagNum" => "1002" ,"Type" => "INT" ,"ValidValues" =>[{"1", "AUTOMATIC"},{"2", "GUARANTOR"},{"3", "MANUAL"}]}
, "1002" => #{"Name"=>"AllocMethod" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "AUTOMATIC"},{"2", "GUARANTOR"},{"3", "MANUAL"}], "TagNum" => "1002"}


,
"TradeID" => #{"TagNum" => "1003" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1003" => #{"Name"=>"TradeID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1003"}


,
"SideTradeReportID" => #{"TagNum" => "1005" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1005" => #{"Name"=>"SideTradeReportID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1005"}


,
"SideFillStationCd" => #{"TagNum" => "1006" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1006" => #{"Name"=>"SideFillStationCd" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1006"}


,
"SideReasonCd" => #{"TagNum" => "1007" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1007" => #{"Name"=>"SideReasonCd" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1007"}


,
"SideTrdSubTyp" => #{"TagNum" => "1008" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1008" => #{"Name"=>"SideTrdSubTyp" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1008"}


,
"SideLastQty" => #{"TagNum" => "1009" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1009" => #{"Name"=>"SideLastQty" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1009"}


,
"MessageEventSource" => #{"TagNum" => "1011" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1011" => #{"Name"=>"MessageEventSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1011"}


,
"SideTrdRegTimestamp" => #{"TagNum" => "1012" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "1012" => #{"Name"=>"SideTrdRegTimestamp" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "1012"}


,
"SideTrdRegTimestampType" => #{"TagNum" => "1013" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1013" => #{"Name"=>"SideTrdRegTimestampType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1013"}


,
"SideTrdRegTimestampSrc" => #{"TagNum" => "1014" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1014" => #{"Name"=>"SideTrdRegTimestampSrc" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1014"}


,
"AsOfIndicator" => #{"TagNum" => "1015" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "FALSE"},{"1", "TRUE"}]}
, "1015" => #{"Name"=>"AsOfIndicator" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "FALSE"},{"1", "TRUE"}], "TagNum" => "1015"}


,
"NoSideTrdRegTS" => #{"TagNum" => "1016" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1016" => #{"Name"=>"NoSideTrdRegTS" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1016"}


,
"LegOptionRatio" => #{"TagNum" => "1017" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "1017" => #{"Name"=>"LegOptionRatio" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "1017"}


,
"NoInstrumentParties" => #{"TagNum" => "1018" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1018" => #{"Name"=>"NoInstrumentParties" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1018"}


,
"InstrumentPartyID" => #{"TagNum" => "1019" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1019" => #{"Name"=>"InstrumentPartyID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1019"}


,
"TradeVolume" => #{"TagNum" => "1020" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1020" => #{"Name"=>"TradeVolume" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1020"}


,
"MDBookType" => #{"TagNum" => "1021" ,"Type" => "INT" ,"ValidValues" =>[{"1", "TOP_OF_BOOK"},{"2", "PRICE_DEPTH"},{"3", "ORDER_DEPTH"}]}
, "1021" => #{"Name"=>"MDBookType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "TOP_OF_BOOK"},{"2", "PRICE_DEPTH"},{"3", "ORDER_DEPTH"}], "TagNum" => "1021"}


,
"MDFeedType" => #{"TagNum" => "1022" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1022" => #{"Name"=>"MDFeedType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1022"}


,
"MDPriceLevel" => #{"TagNum" => "1023" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1023" => #{"Name"=>"MDPriceLevel" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1023"}


,
"MDOriginType" => #{"TagNum" => "1024" ,"Type" => "INT" ,"ValidValues" =>[{"0", "BOOK"},{"1", "OFF_BOOK"},{"2", "CROSS"}]}
, "1024" => #{"Name"=>"MDOriginType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "BOOK"},{"1", "OFF_BOOK"},{"2", "CROSS"}], "TagNum" => "1024"}


,
"FirstPx" => #{"TagNum" => "1025" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1025" => #{"Name"=>"FirstPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1025"}


,
"MDEntrySpotRate" => #{"TagNum" => "1026" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "1026" => #{"Name"=>"MDEntrySpotRate" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "1026"}


,
"MDEntryForwardPoints" => #{"TagNum" => "1027" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "1027" => #{"Name"=>"MDEntryForwardPoints" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "1027"}


,
"ManualOrderIndicator" => #{"TagNum" => "1028" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "1028" => #{"Name"=>"ManualOrderIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "1028"}


,
"CustDirectedOrder" => #{"TagNum" => "1029" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "1029" => #{"Name"=>"CustDirectedOrder" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "1029"}


,
"ReceivedDeptID" => #{"TagNum" => "1030" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1030" => #{"Name"=>"ReceivedDeptID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1030"}


,
"CustOrderHandlingInst" => #{"TagNum" => "1031" ,"Type" => "MULTIPLESTRINGVALUE" ,"ValidValues" =>[{"ADD", "ADD_ON_ORDER"},{"AON", "ALL_OR_NONE"},{"CNH", "CASH_NOT_HELD"},{"DIR", "DIRECTED_ORDER"},{"E.W", "EXCHANGE_FOR_PHYSICAL_TRANSACTION"},{"FOK", "FILL_OR_KILL"},{"IO", "IMBALANCE_ONLY"},{"IOC", "IMMEDIATE_OR_CANCEL"},{"LOO", "LIMIT_ON_OPEN"},{"LOC", "LIMIT_ON_CLOSE"},{"MAO", "MARKET_AT_OPEN"},{"MAC", "MARKET_AT_CLOSE"},{"MOO", "MARKET_ON_OPEN"},{"MOC", "MARKET_ON_CLOSE"},{"MQT", "MINIMUM_QUANTITY"},{"NH", "NOT_HELD"},{"OVD", "OVER_THE_DAY"},{"PEG", "PEGGED"},{"RSV", "RESERVE_SIZE_ORDER"},{"S.W", "STOP_STOCK_TRANSACTION"},{"SCL", "SCALE"},{"TMO", "TIME_ORDER"},{"TS", "TRAILING_STOP"},{"WRK", "WORK"}]}
, "1031" => #{"Name"=>"CustOrderHandlingInst" ,"Type"=>"MULTIPLESTRINGVALUE" ,"ValidValues"=>[{"ADD", "ADD_ON_ORDER"},{"AON", "ALL_OR_NONE"},{"CNH", "CASH_NOT_HELD"},{"DIR", "DIRECTED_ORDER"},{"E.W", "EXCHANGE_FOR_PHYSICAL_TRANSACTION"},{"FOK", "FILL_OR_KILL"},{"IO", "IMBALANCE_ONLY"},{"IOC", "IMMEDIATE_OR_CANCEL"},{"LOO", "LIMIT_ON_OPEN"},{"LOC", "LIMIT_ON_CLOSE"},{"MAO", "MARKET_AT_OPEN"},{"MAC", "MARKET_AT_CLOSE"},{"MOO", "MARKET_ON_OPEN"},{"MOC", "MARKET_ON_CLOSE"},{"MQT", "MINIMUM_QUANTITY"},{"NH", "NOT_HELD"},{"OVD", "OVER_THE_DAY"},{"PEG", "PEGGED"},{"RSV", "RESERVE_SIZE_ORDER"},{"S.W", "STOP_STOCK_TRANSACTION"},{"SCL", "SCALE"},{"TMO", "TIME_ORDER"},{"TS", "TRAILING_STOP"},{"WRK", "WORK"}], "TagNum" => "1031"}


,
"OrderHandlingInstSource" => #{"TagNum" => "1032" ,"Type" => "INT" ,"ValidValues" =>[{"1", "NASD_OATS"}]}
, "1032" => #{"Name"=>"OrderHandlingInstSource" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "NASD_OATS"}], "TagNum" => "1032"}


,
"DeskType" => #{"TagNum" => "1033" ,"Type" => "STRING" ,"ValidValues" =>[{"A", "AGENCY"},{"AR", "ARBITRAGE"},{"D", "DERIVATIVES"},{"IN", "INTERNATIONAL"},{"IS", "INSTITUTIONAL"},{"O", "OTHER"},{"PF", "PREFERRED_TRADING"},{"PR", "PROPRIETARY"},{"PT", "PROGRAM_TRADING"},{"S", "SALES"},{"T", "TRADING"}]}
, "1033" => #{"Name"=>"DeskType" ,"Type"=>"STRING" ,"ValidValues"=>[{"A", "AGENCY"},{"AR", "ARBITRAGE"},{"D", "DERIVATIVES"},{"IN", "INTERNATIONAL"},{"IS", "INSTITUTIONAL"},{"O", "OTHER"},{"PF", "PREFERRED_TRADING"},{"PR", "PROPRIETARY"},{"PT", "PROGRAM_TRADING"},{"S", "SALES"},{"T", "TRADING"}], "TagNum" => "1033"}


,
"DeskTypeSource" => #{"TagNum" => "1034" ,"Type" => "INT" ,"ValidValues" =>[{"1", "NASD_OATS"}]}
, "1034" => #{"Name"=>"DeskTypeSource" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "NASD_OATS"}], "TagNum" => "1034"}


,
"DeskOrderHandlingInst" => #{"TagNum" => "1035" ,"Type" => "MULTIPLESTRINGVALUE" ,"ValidValues" =>[{"ADD", "ADD_ON_ORDER"},{"AON", "ALL_OR_NONE"},{"CNH", "CASH_NOT_HELD"},{"DIR", "DIRECTED_ORDER"},{"E.W", "EXCHANGE_FOR_PHYSICAL_TRANSACTION"},{"FOK", "FILL_OR_KILL"},{"IO", "IMBALANCE_ONLY"},{"IOC", "IMMEDIATE_OR_CANCEL"},{"LOO", "LIMIT_ON_OPEN"},{"LOC", "LIMIT_ON_CLOSE"},{"MAO", "MARKET_AT_OPEN"},{"MAC", "MARKET_AT_CLOSE"},{"MOO", "MARKET_ON_OPEN"},{"MOC", "MARKET_ON_CLOSE"},{"MQT", "MINIMUM_QUANTITY"},{"NH", "NOT_HELD"},{"OVD", "OVER_THE_DAY"},{"PEG", "PEGGED"},{"RSV", "RESERVE_SIZE_ORDER"},{"S.W", "STOP_STOCK_TRANSACTION"},{"SCL", "SCALE"},{"TMO", "TIME_ORDER"},{"TS", "TRAILING_STOP"},{"WRK", "WORK"}]}
, "1035" => #{"Name"=>"DeskOrderHandlingInst" ,"Type"=>"MULTIPLESTRINGVALUE" ,"ValidValues"=>[{"ADD", "ADD_ON_ORDER"},{"AON", "ALL_OR_NONE"},{"CNH", "CASH_NOT_HELD"},{"DIR", "DIRECTED_ORDER"},{"E.W", "EXCHANGE_FOR_PHYSICAL_TRANSACTION"},{"FOK", "FILL_OR_KILL"},{"IO", "IMBALANCE_ONLY"},{"IOC", "IMMEDIATE_OR_CANCEL"},{"LOO", "LIMIT_ON_OPEN"},{"LOC", "LIMIT_ON_CLOSE"},{"MAO", "MARKET_AT_OPEN"},{"MAC", "MARKET_AT_CLOSE"},{"MOO", "MARKET_ON_OPEN"},{"MOC", "MARKET_ON_CLOSE"},{"MQT", "MINIMUM_QUANTITY"},{"NH", "NOT_HELD"},{"OVD", "OVER_THE_DAY"},{"PEG", "PEGGED"},{"RSV", "RESERVE_SIZE_ORDER"},{"S.W", "STOP_STOCK_TRANSACTION"},{"SCL", "SCALE"},{"TMO", "TIME_ORDER"},{"TS", "TRAILING_STOP"},{"WRK", "WORK"}], "TagNum" => "1035"}


,
"ExecAckStatus" => #{"TagNum" => "1036" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "RECEIVED_NOT_YET_PROCESSED"},{"1", "ACCEPTED"},{"2", "DONT_KNOW"}]}
, "1036" => #{"Name"=>"ExecAckStatus" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "RECEIVED_NOT_YET_PROCESSED"},{"1", "ACCEPTED"},{"2", "DONT_KNOW"}], "TagNum" => "1036"}


,
"UnderlyingDeliveryAmount" => #{"TagNum" => "1037" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "1037" => #{"Name"=>"UnderlyingDeliveryAmount" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "1037"}


,
"UnderlyingCapValue" => #{"TagNum" => "1038" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "1038" => #{"Name"=>"UnderlyingCapValue" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "1038"}


,
"UnderlyingSettlMethod" => #{"TagNum" => "1039" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1039" => #{"Name"=>"UnderlyingSettlMethod" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1039"}


,
"SecondaryTradeID" => #{"TagNum" => "1040" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1040" => #{"Name"=>"SecondaryTradeID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1040"}


,
"FirmTradeID" => #{"TagNum" => "1041" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1041" => #{"Name"=>"FirmTradeID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1041"}


,
"SecondaryFirmTradeID" => #{"TagNum" => "1042" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1042" => #{"Name"=>"SecondaryFirmTradeID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1042"}


,
"CollApplType" => #{"TagNum" => "1043" ,"Type" => "INT" ,"ValidValues" =>[{"0", "SPECIFIC_DEPOSIT"},{"1", "GENERAL"}]}
, "1043" => #{"Name"=>"CollApplType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "SPECIFIC_DEPOSIT"},{"1", "GENERAL"}], "TagNum" => "1043"}


,
"UnderlyingAdjustedQuantity" => #{"TagNum" => "1044" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1044" => #{"Name"=>"UnderlyingAdjustedQuantity" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1044"}


,
"UnderlyingFXRate" => #{"TagNum" => "1045" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "1045" => #{"Name"=>"UnderlyingFXRate" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "1045"}


,
"UnderlyingFXRateCalc" => #{"TagNum" => "1046" ,"Type" => "CHAR" ,"ValidValues" =>[{"D", "DIVIDE"},{"M", "MULTIPLY"}]}
, "1046" => #{"Name"=>"UnderlyingFXRateCalc" ,"Type"=>"CHAR" ,"ValidValues"=>[{"D", "DIVIDE"},{"M", "MULTIPLY"}], "TagNum" => "1046"}


,
"AllocPositionEffect" => #{"TagNum" => "1047" ,"Type" => "CHAR" ,"ValidValues" =>[{"O", "OPEN"},{"C", "CLOSE"},{"R", "ROLLED"},{"F", "FIFO"}]}
, "1047" => #{"Name"=>"AllocPositionEffect" ,"Type"=>"CHAR" ,"ValidValues"=>[{"O", "OPEN"},{"C", "CLOSE"},{"R", "ROLLED"},{"F", "FIFO"}], "TagNum" => "1047"}


,
"DealingCapacity" => #{"TagNum" => "1048" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "AGENT"},{"P", "PRINCIPAL"},{"R", "RISKLESS_PRINCIPAL"}]}
, "1048" => #{"Name"=>"DealingCapacity" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "AGENT"},{"P", "PRINCIPAL"},{"R", "RISKLESS_PRINCIPAL"}], "TagNum" => "1048"}


,
"InstrmtAssignmentMethod" => #{"TagNum" => "1049" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "1049" => #{"Name"=>"InstrmtAssignmentMethod" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "1049"}


,
"InstrumentPartyIDSource" => #{"TagNum" => "1050" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "1050" => #{"Name"=>"InstrumentPartyIDSource" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "1050"}


,
"InstrumentPartyRole" => #{"TagNum" => "1051" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1051" => #{"Name"=>"InstrumentPartyRole" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1051"}


,
"NoInstrumentPartySubIDs" => #{"TagNum" => "1052" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1052" => #{"Name"=>"NoInstrumentPartySubIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1052"}


,
"InstrumentPartySubID" => #{"TagNum" => "1053" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1053" => #{"Name"=>"InstrumentPartySubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1053"}


,
"InstrumentPartySubIDType" => #{"TagNum" => "1054" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1054" => #{"Name"=>"InstrumentPartySubIDType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1054"}


,
"PositionCurrency" => #{"TagNum" => "1055" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1055" => #{"Name"=>"PositionCurrency" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1055"}


,
"CalculatedCcyLastQty" => #{"TagNum" => "1056" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1056" => #{"Name"=>"CalculatedCcyLastQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1056"}


,
"AggressorIndicator" => #{"TagNum" => "1057" ,"Type" => "BOOLEAN" ,"ValidValues" =>[{"Y", "YES"},{"N", "NO"}]}
, "1057" => #{"Name"=>"AggressorIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[{"Y", "YES"},{"N", "NO"}], "TagNum" => "1057"}


,
"NoUndlyInstrumentParties" => #{"TagNum" => "1058" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1058" => #{"Name"=>"NoUndlyInstrumentParties" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1058"}


,
"UnderlyingInstrumentPartyID" => #{"TagNum" => "1059" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1059" => #{"Name"=>"UnderlyingInstrumentPartyID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1059"}


,
"UnderlyingInstrumentPartyIDSource" => #{"TagNum" => "1060" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "1060" => #{"Name"=>"UnderlyingInstrumentPartyIDSource" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "1060"}


,
"UnderlyingInstrumentPartyRole" => #{"TagNum" => "1061" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1061" => #{"Name"=>"UnderlyingInstrumentPartyRole" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1061"}


,
"NoUndlyInstrumentPartySubIDs" => #{"TagNum" => "1062" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1062" => #{"Name"=>"NoUndlyInstrumentPartySubIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1062"}


,
"UnderlyingInstrumentPartySubID" => #{"TagNum" => "1063" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1063" => #{"Name"=>"UnderlyingInstrumentPartySubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1063"}


,
"UnderlyingInstrumentPartySubIDType" => #{"TagNum" => "1064" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1064" => #{"Name"=>"UnderlyingInstrumentPartySubIDType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1064"}


,
"BidSwapPoints" => #{"TagNum" => "1065" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "1065" => #{"Name"=>"BidSwapPoints" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "1065"}


,
"OfferSwapPoints" => #{"TagNum" => "1066" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "1066" => #{"Name"=>"OfferSwapPoints" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "1066"}


,
"LegBidForwardPoints" => #{"TagNum" => "1067" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "1067" => #{"Name"=>"LegBidForwardPoints" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "1067"}


,
"LegOfferForwardPoints" => #{"TagNum" => "1068" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "1068" => #{"Name"=>"LegOfferForwardPoints" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "1068"}


,
"SwapPoints" => #{"TagNum" => "1069" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "1069" => #{"Name"=>"SwapPoints" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "1069"}


,
"MDQuoteType" => #{"TagNum" => "1070" ,"Type" => "INT" ,"ValidValues" =>[{"0", "INDICATIVE"},{"1", "TRADEABLE"},{"2", "RESTRICTED_TRADEABLE"},{"3", "COUNTER"},{"4", "INDICATIVE_AND_TRADEABLE"}]}
, "1070" => #{"Name"=>"MDQuoteType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "INDICATIVE"},{"1", "TRADEABLE"},{"2", "RESTRICTED_TRADEABLE"},{"3", "COUNTER"},{"4", "INDICATIVE_AND_TRADEABLE"}], "TagNum" => "1070"}


,
"LastSwapPoints" => #{"TagNum" => "1071" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "1071" => #{"Name"=>"LastSwapPoints" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "1071"}


,
"SideGrossTradeAmt" => #{"TagNum" => "1072" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "1072" => #{"Name"=>"SideGrossTradeAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "1072"}


,
"LegLastForwardPoints" => #{"TagNum" => "1073" ,"Type" => "PRICEOFFSET" ,"ValidValues" =>[]}
, "1073" => #{"Name"=>"LegLastForwardPoints" ,"Type"=>"PRICEOFFSET" ,"ValidValues"=>[], "TagNum" => "1073"}


,
"LegCalculatedCcyLastQty" => #{"TagNum" => "1074" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1074" => #{"Name"=>"LegCalculatedCcyLastQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1074"}


,
"LegGrossTradeAmt" => #{"TagNum" => "1075" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "1075" => #{"Name"=>"LegGrossTradeAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "1075"}


,
"MaturityTime" => #{"TagNum" => "1079" ,"Type" => "TZTIMEONLY" ,"ValidValues" =>[]}
, "1079" => #{"Name"=>"MaturityTime" ,"Type"=>"TZTIMEONLY" ,"ValidValues"=>[], "TagNum" => "1079"}


,
"RefOrderID" => #{"TagNum" => "1080" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1080" => #{"Name"=>"RefOrderID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1080"}


,
"RefOrderIDSource" => #{"TagNum" => "1081" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "SECONDARYORDERID"},{"1", "ORDERID"},{"2", "MDENTRYID"},{"3", "QUOTEENTRYID"},{"4", "ORIGINAL_ORDER_ID"}]}
, "1081" => #{"Name"=>"RefOrderIDSource" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "SECONDARYORDERID"},{"1", "ORDERID"},{"2", "MDENTRYID"},{"3", "QUOTEENTRYID"},{"4", "ORIGINAL_ORDER_ID"}], "TagNum" => "1081"}


,
"SecondaryDisplayQty" => #{"TagNum" => "1082" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1082" => #{"Name"=>"SecondaryDisplayQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1082"}


,
"DisplayWhen" => #{"TagNum" => "1083" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "IMMEDIATE"},{"2", "EXHAUST"}]}
, "1083" => #{"Name"=>"DisplayWhen" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "IMMEDIATE"},{"2", "EXHAUST"}], "TagNum" => "1083"}


,
"DisplayMethod" => #{"TagNum" => "1084" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "INITIAL"},{"2", "NEW"},{"3", "RANDOM"},{"4", "UNDISCLOSED"}]}
, "1084" => #{"Name"=>"DisplayMethod" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "INITIAL"},{"2", "NEW"},{"3", "RANDOM"},{"4", "UNDISCLOSED"}], "TagNum" => "1084"}


,
"DisplayLowQty" => #{"TagNum" => "1085" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1085" => #{"Name"=>"DisplayLowQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1085"}


,
"DisplayHighQty" => #{"TagNum" => "1086" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1086" => #{"Name"=>"DisplayHighQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1086"}


,
"DisplayMinIncr" => #{"TagNum" => "1087" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1087" => #{"Name"=>"DisplayMinIncr" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1087"}


,
"RefreshQty" => #{"TagNum" => "1088" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1088" => #{"Name"=>"RefreshQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1088"}


,
"MatchIncrement" => #{"TagNum" => "1089" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1089" => #{"Name"=>"MatchIncrement" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1089"}


,
"MaxPriceLevels" => #{"TagNum" => "1090" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1090" => #{"Name"=>"MaxPriceLevels" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1090"}


,
"PreTradeAnonymity" => #{"TagNum" => "1091" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "1091" => #{"Name"=>"PreTradeAnonymity" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "1091"}


,
"PriceProtectionScope" => #{"TagNum" => "1092" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NONE"},{"1", "LOCAL"},{"2", "NATIONAL"},{"3", "GLOBAL"}]}
, "1092" => #{"Name"=>"PriceProtectionScope" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NONE"},{"1", "LOCAL"},{"2", "NATIONAL"},{"3", "GLOBAL"}], "TagNum" => "1092"}


,
"LotType" => #{"TagNum" => "1093" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "ODD_LOT"},{"2", "ROUND_LOT"},{"3", "BLOCK_LOT"},{"4", "ROUND_LOT_BASED_UPON_UNITOFMEASURE"}]}
, "1093" => #{"Name"=>"LotType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "ODD_LOT"},{"2", "ROUND_LOT"},{"3", "BLOCK_LOT"},{"4", "ROUND_LOT_BASED_UPON_UNITOFMEASURE"}], "TagNum" => "1093"}


,
"PegPriceType" => #{"TagNum" => "1094" ,"Type" => "INT" ,"ValidValues" =>[{"1", "LAST_PEG"},{"2", "MID_PRICE_PEG"},{"3", "OPENING_PEG"},{"4", "MARKET_PEG"},{"5", "PRIMARY_PEG"},{"7", "PEG_TO_VWAP"},{"8", "TRAILING_STOP_PEG"},{"9", "PEG_TO_LIMIT_PRICE"}]}
, "1094" => #{"Name"=>"PegPriceType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "LAST_PEG"},{"2", "MID_PRICE_PEG"},{"3", "OPENING_PEG"},{"4", "MARKET_PEG"},{"5", "PRIMARY_PEG"},{"7", "PEG_TO_VWAP"},{"8", "TRAILING_STOP_PEG"},{"9", "PEG_TO_LIMIT_PRICE"}], "TagNum" => "1094"}


,
"PeggedRefPrice" => #{"TagNum" => "1095" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1095" => #{"Name"=>"PeggedRefPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1095"}


,
"PegSecurityIDSource" => #{"TagNum" => "1096" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1096" => #{"Name"=>"PegSecurityIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1096"}


,
"PegSecurityID" => #{"TagNum" => "1097" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1097" => #{"Name"=>"PegSecurityID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1097"}


,
"PegSymbol" => #{"TagNum" => "1098" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1098" => #{"Name"=>"PegSymbol" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1098"}


,
"PegSecurityDesc" => #{"TagNum" => "1099" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1099" => #{"Name"=>"PegSecurityDesc" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1099"}


,
"TriggerType" => #{"TagNum" => "1100" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "PARTIAL_EXECUTION"},{"2", "SPECIFIED_TRADING_SESSION"},{"3", "NEXT_AUCTION"},{"4", "PRICE_MOVEMENT"}]}
, "1100" => #{"Name"=>"TriggerType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "PARTIAL_EXECUTION"},{"2", "SPECIFIED_TRADING_SESSION"},{"3", "NEXT_AUCTION"},{"4", "PRICE_MOVEMENT"}], "TagNum" => "1100"}


,
"TriggerAction" => #{"TagNum" => "1101" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "ACTIVATE"},{"2", "MODIFY"},{"3", "CANCEL"}]}
, "1101" => #{"Name"=>"TriggerAction" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "ACTIVATE"},{"2", "MODIFY"},{"3", "CANCEL"}], "TagNum" => "1101"}


,
"TriggerPrice" => #{"TagNum" => "1102" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1102" => #{"Name"=>"TriggerPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1102"}


,
"TriggerSymbol" => #{"TagNum" => "1103" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1103" => #{"Name"=>"TriggerSymbol" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1103"}


,
"TriggerSecurityID" => #{"TagNum" => "1104" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1104" => #{"Name"=>"TriggerSecurityID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1104"}


,
"TriggerSecurityIDSource" => #{"TagNum" => "1105" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1105" => #{"Name"=>"TriggerSecurityIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1105"}


,
"TriggerSecurityDesc" => #{"TagNum" => "1106" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1106" => #{"Name"=>"TriggerSecurityDesc" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1106"}


,
"TriggerPriceType" => #{"TagNum" => "1107" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "BEST_OFFER"},{"2", "LAST_TRADE"},{"3", "BEST_BID"},{"4", "BEST_BID_OR_LAST_TRADE"},{"5", "BEST_OFFER_OR_LAST_TRADE"},{"6", "BEST_MID"}]}
, "1107" => #{"Name"=>"TriggerPriceType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "BEST_OFFER"},{"2", "LAST_TRADE"},{"3", "BEST_BID"},{"4", "BEST_BID_OR_LAST_TRADE"},{"5", "BEST_OFFER_OR_LAST_TRADE"},{"6", "BEST_MID"}], "TagNum" => "1107"}


,
"TriggerPriceTypeScope" => #{"TagNum" => "1108" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "NONE"},{"1", "LOCAL"},{"2", "NATIONAL"},{"3", "GLOBAL"}]}
, "1108" => #{"Name"=>"TriggerPriceTypeScope" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "NONE"},{"1", "LOCAL"},{"2", "NATIONAL"},{"3", "GLOBAL"}], "TagNum" => "1108"}


,
"TriggerPriceDirection" => #{"TagNum" => "1109" ,"Type" => "CHAR" ,"ValidValues" =>[{"U", "TRIGGER_IF_THE_PRICE_OF_THE_SPECIFIED_TYPE_GOES_UP_TO_OR_THROUGH_THE_SPECIFIED_TRIGGER_PRICE"},{"D", "TRIGGER_IF_THE_PRICE_OF_THE_SPECIFIED_TYPE_GOES_DOWN_TO_OR_THROUGH_THE_SPECIFIED_TRIGGER_PRICE"}]}
, "1109" => #{"Name"=>"TriggerPriceDirection" ,"Type"=>"CHAR" ,"ValidValues"=>[{"U", "TRIGGER_IF_THE_PRICE_OF_THE_SPECIFIED_TYPE_GOES_UP_TO_OR_THROUGH_THE_SPECIFIED_TRIGGER_PRICE"},{"D", "TRIGGER_IF_THE_PRICE_OF_THE_SPECIFIED_TYPE_GOES_DOWN_TO_OR_THROUGH_THE_SPECIFIED_TRIGGER_PRICE"}], "TagNum" => "1109"}


,
"TriggerNewPrice" => #{"TagNum" => "1110" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1110" => #{"Name"=>"TriggerNewPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1110"}


,
"TriggerOrderType" => #{"TagNum" => "1111" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "MARKET"},{"2", "LIMIT"}]}
, "1111" => #{"Name"=>"TriggerOrderType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "MARKET"},{"2", "LIMIT"}], "TagNum" => "1111"}


,
"TriggerNewQty" => #{"TagNum" => "1112" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1112" => #{"Name"=>"TriggerNewQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1112"}


,
"TriggerTradingSessionID" => #{"TagNum" => "1113" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1113" => #{"Name"=>"TriggerTradingSessionID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1113"}


,
"TriggerTradingSessionSubID" => #{"TagNum" => "1114" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1114" => #{"Name"=>"TriggerTradingSessionSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1114"}


,
"OrderCategory" => #{"TagNum" => "1115" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "ORDER"},{"2", "QUOTE"},{"3", "PRIVATELY_NEGOTIATED_TRADE"},{"4", "MULTILEG_ORDER"},{"5", "LINKED_ORDER"},{"6", "QUOTE_REQUEST"},{"7", "IMPLIED_ORDER"},{"8", "CROSS_ORDER"},{"9", "STREAMING_PRICE"}]}
, "1115" => #{"Name"=>"OrderCategory" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "ORDER"},{"2", "QUOTE"},{"3", "PRIVATELY_NEGOTIATED_TRADE"},{"4", "MULTILEG_ORDER"},{"5", "LINKED_ORDER"},{"6", "QUOTE_REQUEST"},{"7", "IMPLIED_ORDER"},{"8", "CROSS_ORDER"},{"9", "STREAMING_PRICE"}], "TagNum" => "1115"}


,
"NoRootPartyIDs" => #{"TagNum" => "1116" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1116" => #{"Name"=>"NoRootPartyIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1116"}


,
"RootPartyID" => #{"TagNum" => "1117" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1117" => #{"Name"=>"RootPartyID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1117"}


,
"RootPartyIDSource" => #{"TagNum" => "1118" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "1118" => #{"Name"=>"RootPartyIDSource" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "1118"}


,
"RootPartyRole" => #{"TagNum" => "1119" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1119" => #{"Name"=>"RootPartyRole" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1119"}


,
"NoRootPartySubIDs" => #{"TagNum" => "1120" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1120" => #{"Name"=>"NoRootPartySubIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1120"}


,
"RootPartySubID" => #{"TagNum" => "1121" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1121" => #{"Name"=>"RootPartySubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1121"}


,
"RootPartySubIDType" => #{"TagNum" => "1122" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1122" => #{"Name"=>"RootPartySubIDType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1122"}


,
"TradeHandlingInstr" => #{"TagNum" => "1123" ,"Type" => "CHAR" ,"ValidValues" =>[{"0", "TRADE_CONFIRMATION"},{"1", "TWO_PARTY_REPORT"},{"2", "ONE_PARTY_REPORT_FOR_MATCHING"},{"3", "ONE_PARTY_REPORT_FOR_PASS_THROUGH"},{"4", "AUTOMATED_FLOOR_ORDER_ROUTING"},{"5", "TWO_PARTY_REPORT_FOR_CLAIM"}]}
, "1123" => #{"Name"=>"TradeHandlingInstr" ,"Type"=>"CHAR" ,"ValidValues"=>[{"0", "TRADE_CONFIRMATION"},{"1", "TWO_PARTY_REPORT"},{"2", "ONE_PARTY_REPORT_FOR_MATCHING"},{"3", "ONE_PARTY_REPORT_FOR_PASS_THROUGH"},{"4", "AUTOMATED_FLOOR_ORDER_ROUTING"},{"5", "TWO_PARTY_REPORT_FOR_CLAIM"}], "TagNum" => "1123"}


,
"OrigTradeHandlingInstr" => #{"TagNum" => "1124" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "1124" => #{"Name"=>"OrigTradeHandlingInstr" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "1124"}


,
"OrigTradeDate" => #{"TagNum" => "1125" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "1125" => #{"Name"=>"OrigTradeDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "1125"}


,
"OrigTradeID" => #{"TagNum" => "1126" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1126" => #{"Name"=>"OrigTradeID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1126"}


,
"OrigSecondaryTradeID" => #{"TagNum" => "1127" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1127" => #{"Name"=>"OrigSecondaryTradeID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1127"}


,
"ApplVerID" => #{"TagNum" => "1128" ,"Type" => "STRING" ,"ValidValues" =>[{"0", "FIX27"},{"1", "FIX30"},{"2", "FIX40"},{"3", "FIX41"},{"4", "FIX42"},{"5", "FIX43"},{"6", "FIX44"},{"7", "FIX50"},{"8", "FIX50SP1"},{"9", "FIX50SP2"}]}
, "1128" => #{"Name"=>"ApplVerID" ,"Type"=>"STRING" ,"ValidValues"=>[{"0", "FIX27"},{"1", "FIX30"},{"2", "FIX40"},{"3", "FIX41"},{"4", "FIX42"},{"5", "FIX43"},{"6", "FIX44"},{"7", "FIX50"},{"8", "FIX50SP1"},{"9", "FIX50SP2"}], "TagNum" => "1128"}


,
"CstmApplVerID" => #{"TagNum" => "1129" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1129" => #{"Name"=>"CstmApplVerID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1129"}


,
"RefApplVerID" => #{"TagNum" => "1130" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1130" => #{"Name"=>"RefApplVerID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1130"}


,
"RefCstmApplVerID" => #{"TagNum" => "1131" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1131" => #{"Name"=>"RefCstmApplVerID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1131"}


,
"TZTransactTime" => #{"TagNum" => "1132" ,"Type" => "TZTIMESTAMP" ,"ValidValues" =>[]}
, "1132" => #{"Name"=>"TZTransactTime" ,"Type"=>"TZTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "1132"}


,
"ExDestinationIDSource" => #{"TagNum" => "1133" ,"Type" => "CHAR" ,"ValidValues" =>[{"B", "BIC"},{"C", "GENERALLY_ACCEPTED_MARKET_PARTICIPANT_IDENTIFIER"},{"D", "PROPRIETARY"},{"E", "ISO_COUNTRY_CODE"},{"G", "MIC"}]}
, "1133" => #{"Name"=>"ExDestinationIDSource" ,"Type"=>"CHAR" ,"ValidValues"=>[{"B", "BIC"},{"C", "GENERALLY_ACCEPTED_MARKET_PARTICIPANT_IDENTIFIER"},{"D", "PROPRIETARY"},{"E", "ISO_COUNTRY_CODE"},{"G", "MIC"}], "TagNum" => "1133"}


,
"ReportedPxDiff" => #{"TagNum" => "1134" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "1134" => #{"Name"=>"ReportedPxDiff" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "1134"}


,
"RptSys" => #{"TagNum" => "1135" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1135" => #{"Name"=>"RptSys" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1135"}


,
"AllocClearingFeeIndicator" => #{"TagNum" => "1136" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1136" => #{"Name"=>"AllocClearingFeeIndicator" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1136"}


,
"DefaultApplVerID" => #{"TagNum" => "1137" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1137" => #{"Name"=>"DefaultApplVerID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1137"}


,
"DisplayQty" => #{"TagNum" => "1138" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1138" => #{"Name"=>"DisplayQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1138"}


,
"ExchangeSpecialInstructions" => #{"TagNum" => "1139" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1139" => #{"Name"=>"ExchangeSpecialInstructions" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1139"}


,
"MaxTradeVol" => #{"TagNum" => "1140" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1140" => #{"Name"=>"MaxTradeVol" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1140"}


,
"NoMDFeedTypes" => #{"TagNum" => "1141" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1141" => #{"Name"=>"NoMDFeedTypes" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1141"}


,
"MatchAlgorithm" => #{"TagNum" => "1142" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1142" => #{"Name"=>"MatchAlgorithm" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1142"}


,
"MaxPriceVariation" => #{"TagNum" => "1143" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "1143" => #{"Name"=>"MaxPriceVariation" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "1143"}


,
"ImpliedMarketIndicator" => #{"TagNum" => "1144" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NOT_IMPLIED"},{"1", "IMPLIED_IN"},{"2", "IMPLIED_OUT"},{"3", "BOTH_IMPLIED_IN_AND_IMPLIED_OUT"}]}
, "1144" => #{"Name"=>"ImpliedMarketIndicator" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NOT_IMPLIED"},{"1", "IMPLIED_IN"},{"2", "IMPLIED_OUT"},{"3", "BOTH_IMPLIED_IN_AND_IMPLIED_OUT"}], "TagNum" => "1144"}


,
"EventTime" => #{"TagNum" => "1145" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "1145" => #{"Name"=>"EventTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "1145"}


,
"MinPriceIncrementAmount" => #{"TagNum" => "1146" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "1146" => #{"Name"=>"MinPriceIncrementAmount" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "1146"}


,
"UnitOfMeasureQty" => #{"TagNum" => "1147" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1147" => #{"Name"=>"UnitOfMeasureQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1147"}


,
"LowLimitPrice" => #{"TagNum" => "1148" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1148" => #{"Name"=>"LowLimitPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1148"}


,
"HighLimitPrice" => #{"TagNum" => "1149" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1149" => #{"Name"=>"HighLimitPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1149"}


,
"TradingReferencePrice" => #{"TagNum" => "1150" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1150" => #{"Name"=>"TradingReferencePrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1150"}


,
"SecurityGroup" => #{"TagNum" => "1151" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1151" => #{"Name"=>"SecurityGroup" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1151"}


,
"LegNumber" => #{"TagNum" => "1152" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1152" => #{"Name"=>"LegNumber" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1152"}


,
"SettlementCycleNo" => #{"TagNum" => "1153" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1153" => #{"Name"=>"SettlementCycleNo" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1153"}


,
"SideCurrency" => #{"TagNum" => "1154" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "1154" => #{"Name"=>"SideCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "1154"}


,
"SideSettlCurrency" => #{"TagNum" => "1155" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "1155" => #{"Name"=>"SideSettlCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "1155"}


,
"ApplExtID" => #{"TagNum" => "1156" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1156" => #{"Name"=>"ApplExtID" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1156"}


,
"CcyAmt" => #{"TagNum" => "1157" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "1157" => #{"Name"=>"CcyAmt" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "1157"}


,
"NoSettlDetails" => #{"TagNum" => "1158" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1158" => #{"Name"=>"NoSettlDetails" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1158"}


,
"SettlObligMode" => #{"TagNum" => "1159" ,"Type" => "INT" ,"ValidValues" =>[{"1", "PRELIMINARY"},{"2", "FINAL"}]}
, "1159" => #{"Name"=>"SettlObligMode" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "PRELIMINARY"},{"2", "FINAL"}], "TagNum" => "1159"}


,
"SettlObligMsgID" => #{"TagNum" => "1160" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1160" => #{"Name"=>"SettlObligMsgID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1160"}


,
"SettlObligID" => #{"TagNum" => "1161" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1161" => #{"Name"=>"SettlObligID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1161"}


,
"SettlObligTransType" => #{"TagNum" => "1162" ,"Type" => "CHAR" ,"ValidValues" =>[{"C", "CANCEL"},{"N", "NEW"},{"R", "REPLACE"},{"T", "RESTATE"}]}
, "1162" => #{"Name"=>"SettlObligTransType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"C", "CANCEL"},{"N", "NEW"},{"R", "REPLACE"},{"T", "RESTATE"}], "TagNum" => "1162"}


,
"SettlObligRefID" => #{"TagNum" => "1163" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1163" => #{"Name"=>"SettlObligRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1163"}


,
"SettlObligSource" => #{"TagNum" => "1164" ,"Type" => "CHAR" ,"ValidValues" =>[{"1", "INSTRUCTIONS_OF_BROKER"},{"2", "INSTRUCTIONS_FOR_INSTITUTION"},{"3", "INVESTOR"}]}
, "1164" => #{"Name"=>"SettlObligSource" ,"Type"=>"CHAR" ,"ValidValues"=>[{"1", "INSTRUCTIONS_OF_BROKER"},{"2", "INSTRUCTIONS_FOR_INSTITUTION"},{"3", "INVESTOR"}], "TagNum" => "1164"}


,
"NoSettlOblig" => #{"TagNum" => "1165" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1165" => #{"Name"=>"NoSettlOblig" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1165"}


,
"QuoteMsgID" => #{"TagNum" => "1166" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1166" => #{"Name"=>"QuoteMsgID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1166"}


,
"QuoteEntryStatus" => #{"TagNum" => "1167" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ACCEPTED"},{"5", "REJECTED"},{"6", "REMOVED_FROM_MARKET"},{"7", "EXPIRED"},{"12", "LOCKED_MARKET_WARNING"},{"13", "CROSS_MARKET_WARNING"},{"14", "CANCELED_DUE_TO_LOCK_MARKET"},{"15", "CANCELED_DUE_TO_CROSS_MARKET"},{"16", "ACTIVE"}]}
, "1167" => #{"Name"=>"QuoteEntryStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ACCEPTED"},{"5", "REJECTED"},{"6", "REMOVED_FROM_MARKET"},{"7", "EXPIRED"},{"12", "LOCKED_MARKET_WARNING"},{"13", "CROSS_MARKET_WARNING"},{"14", "CANCELED_DUE_TO_LOCK_MARKET"},{"15", "CANCELED_DUE_TO_CROSS_MARKET"},{"16", "ACTIVE"}], "TagNum" => "1167"}


,
"TotNoCxldQuotes" => #{"TagNum" => "1168" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1168" => #{"Name"=>"TotNoCxldQuotes" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1168"}


,
"TotNoAccQuotes" => #{"TagNum" => "1169" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1169" => #{"Name"=>"TotNoAccQuotes" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1169"}


,
"TotNoRejQuotes" => #{"TagNum" => "1170" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1170" => #{"Name"=>"TotNoRejQuotes" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1170"}


,
"PrivateQuote" => #{"TagNum" => "1171" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "1171" => #{"Name"=>"PrivateQuote" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "1171"}


,
"RespondentType" => #{"TagNum" => "1172" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ALL_MARKET_PARTICIPANTS"},{"2", "SPECIFIED_MARKET_PARTICIPANTS"},{"3", "ALL_MARKET_MAKERS"},{"4", "PRIMARY_MARKET_MAKER"}]}
, "1172" => #{"Name"=>"RespondentType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ALL_MARKET_PARTICIPANTS"},{"2", "SPECIFIED_MARKET_PARTICIPANTS"},{"3", "ALL_MARKET_MAKERS"},{"4", "PRIMARY_MARKET_MAKER"}], "TagNum" => "1172"}


,
"MDSubBookType" => #{"TagNum" => "1173" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1173" => #{"Name"=>"MDSubBookType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1173"}


,
"SecurityTradingEvent" => #{"TagNum" => "1174" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ORDER_IMBALANCE_AUCTION_IS_EXTENDED"},{"2", "TRADING_RESUMES"},{"3", "PRICE_VOLATILITY_INTERRUPTION"},{"4", "CHANGE_OF_TRADING_SESSION"},{"5", "CHANGE_OF_TRADING_SUBSESSION"},{"6", "CHANGE_OF_SECURITY_TRADING_STATUS"},{"7", "CHANGE_OF_BOOK_TYPE"},{"8", "CHANGE_OF_MARKET_DEPTH"}]}
, "1174" => #{"Name"=>"SecurityTradingEvent" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ORDER_IMBALANCE_AUCTION_IS_EXTENDED"},{"2", "TRADING_RESUMES"},{"3", "PRICE_VOLATILITY_INTERRUPTION"},{"4", "CHANGE_OF_TRADING_SESSION"},{"5", "CHANGE_OF_TRADING_SUBSESSION"},{"6", "CHANGE_OF_SECURITY_TRADING_STATUS"},{"7", "CHANGE_OF_BOOK_TYPE"},{"8", "CHANGE_OF_MARKET_DEPTH"}], "TagNum" => "1174"}


,
"NoStatsIndicators" => #{"TagNum" => "1175" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1175" => #{"Name"=>"NoStatsIndicators" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1175"}


,
"StatsType" => #{"TagNum" => "1176" ,"Type" => "INT" ,"ValidValues" =>[{"1", "EXCHANGE_LAST"},{"2", "HIGH"},{"3", "AVERAGE_PRICE"},{"4", "TURNOVER"}]}
, "1176" => #{"Name"=>"StatsType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "EXCHANGE_LAST"},{"2", "HIGH"},{"3", "AVERAGE_PRICE"},{"4", "TURNOVER"}], "TagNum" => "1176"}


,
"NoOfSecSizes" => #{"TagNum" => "1177" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1177" => #{"Name"=>"NoOfSecSizes" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1177"}


,
"MDSecSizeType" => #{"TagNum" => "1178" ,"Type" => "INT" ,"ValidValues" =>[{"1", "CUSTOMER"}]}
, "1178" => #{"Name"=>"MDSecSizeType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "CUSTOMER"}], "TagNum" => "1178"}


,
"MDSecSize" => #{"TagNum" => "1179" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1179" => #{"Name"=>"MDSecSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1179"}


,
"ApplID" => #{"TagNum" => "1180" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1180" => #{"Name"=>"ApplID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1180"}


,
"ApplSeqNum" => #{"TagNum" => "1181" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "1181" => #{"Name"=>"ApplSeqNum" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "1181"}


,
"ApplBegSeqNum" => #{"TagNum" => "1182" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "1182" => #{"Name"=>"ApplBegSeqNum" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "1182"}


,
"ApplEndSeqNum" => #{"TagNum" => "1183" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "1183" => #{"Name"=>"ApplEndSeqNum" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "1183"}


,
"SecurityXMLLen" => #{"TagNum" => "1184" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "1184" => #{"Name"=>"SecurityXMLLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "1184"}


,
"SecurityXML" => #{"TagNum" => "1185" ,"Type" => "XMLDATA" ,"ValidValues" =>[]}
, "1185" => #{"Name"=>"SecurityXML" ,"Type"=>"XMLDATA" ,"ValidValues"=>[], "TagNum" => "1185"}


,
"SecurityXMLSchema" => #{"TagNum" => "1186" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1186" => #{"Name"=>"SecurityXMLSchema" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1186"}


,
"RefreshIndicator" => #{"TagNum" => "1187" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "1187" => #{"Name"=>"RefreshIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "1187"}


,
"Volatility" => #{"TagNum" => "1188" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "1188" => #{"Name"=>"Volatility" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "1188"}


,
"TimeToExpiration" => #{"TagNum" => "1189" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "1189" => #{"Name"=>"TimeToExpiration" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "1189"}


,
"RiskFreeRate" => #{"TagNum" => "1190" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "1190" => #{"Name"=>"RiskFreeRate" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "1190"}


,
"PriceUnitOfMeasure" => #{"TagNum" => "1191" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1191" => #{"Name"=>"PriceUnitOfMeasure" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1191"}


,
"PriceUnitOfMeasureQty" => #{"TagNum" => "1192" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1192" => #{"Name"=>"PriceUnitOfMeasureQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1192"}


,
"SettlMethod" => #{"TagNum" => "1193" ,"Type" => "CHAR" ,"ValidValues" =>[{"C", "CASH_SETTLEMENT_REQUIRED"},{"P", "PHYSICAL_SETTLEMENT_REQUIRED"}]}
, "1193" => #{"Name"=>"SettlMethod" ,"Type"=>"CHAR" ,"ValidValues"=>[{"C", "CASH_SETTLEMENT_REQUIRED"},{"P", "PHYSICAL_SETTLEMENT_REQUIRED"}], "TagNum" => "1193"}


,
"ExerciseStyle" => #{"TagNum" => "1194" ,"Type" => "INT" ,"ValidValues" =>[{"0", "EUROPEAN"},{"1", "AMERICAN"},{"2", "BERMUDA"}]}
, "1194" => #{"Name"=>"ExerciseStyle" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "EUROPEAN"},{"1", "AMERICAN"},{"2", "BERMUDA"}], "TagNum" => "1194"}


,
"OptPayoutAmount" => #{"TagNum" => "1195" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "1195" => #{"Name"=>"OptPayoutAmount" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "1195"}


,
"PriceQuoteMethod" => #{"TagNum" => "1196" ,"Type" => "STRING" ,"ValidValues" =>[{"STD", "STANDARD_MONEY_PER_UNIT_OF_A_PHYSICAL"},{"INX", "INDEX"},{"INT", "INTEREST_RATE_INDEX"},{"PCTPAR", "PERCENT_OF_PAR"}]}
, "1196" => #{"Name"=>"PriceQuoteMethod" ,"Type"=>"STRING" ,"ValidValues"=>[{"STD", "STANDARD_MONEY_PER_UNIT_OF_A_PHYSICAL"},{"INX", "INDEX"},{"INT", "INTEREST_RATE_INDEX"},{"PCTPAR", "PERCENT_OF_PAR"}], "TagNum" => "1196"}


,
"ValuationMethod" => #{"TagNum" => "1197" ,"Type" => "STRING" ,"ValidValues" =>[{"EQTY", "PREMIUM_STYLE"},{"FUT", "FUTURES_STYLE_MARK_TO_MARKET"},{"FUTDA", "FUTURES_STYLE_WITH_AN_ATTACHED_CASH_ADJUSTMENT"},{"CDS", "CDS_STYLE_COLLATERALIZATION_OF_MARKET_TO_MARKET_AND_COUPON"},{"CDSD", "CDS_IN_DELIVERY"}]}
, "1197" => #{"Name"=>"ValuationMethod" ,"Type"=>"STRING" ,"ValidValues"=>[{"EQTY", "PREMIUM_STYLE"},{"FUT", "FUTURES_STYLE_MARK_TO_MARKET"},{"FUTDA", "FUTURES_STYLE_WITH_AN_ATTACHED_CASH_ADJUSTMENT"},{"CDS", "CDS_STYLE_COLLATERALIZATION_OF_MARKET_TO_MARKET_AND_COUPON"},{"CDSD", "CDS_IN_DELIVERY"}], "TagNum" => "1197"}


,
"ListMethod" => #{"TagNum" => "1198" ,"Type" => "INT" ,"ValidValues" =>[{"0", "PRE_LISTED_ONLY"},{"1", "USER_REQUESTED"}]}
, "1198" => #{"Name"=>"ListMethod" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "PRE_LISTED_ONLY"},{"1", "USER_REQUESTED"}], "TagNum" => "1198"}


,
"CapPrice" => #{"TagNum" => "1199" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1199" => #{"Name"=>"CapPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1199"}


,
"FloorPrice" => #{"TagNum" => "1200" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1200" => #{"Name"=>"FloorPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1200"}


,
"NoStrikeRules" => #{"TagNum" => "1201" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1201" => #{"Name"=>"NoStrikeRules" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1201"}


,
"StartStrikePxRange" => #{"TagNum" => "1202" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1202" => #{"Name"=>"StartStrikePxRange" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1202"}


,
"EndStrikePxRange" => #{"TagNum" => "1203" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1203" => #{"Name"=>"EndStrikePxRange" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1203"}


,
"StrikeIncrement" => #{"TagNum" => "1204" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "1204" => #{"Name"=>"StrikeIncrement" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "1204"}


,
"NoTickRules" => #{"TagNum" => "1205" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1205" => #{"Name"=>"NoTickRules" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1205"}


,
"StartTickPriceRange" => #{"TagNum" => "1206" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1206" => #{"Name"=>"StartTickPriceRange" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1206"}


,
"EndTickPriceRange" => #{"TagNum" => "1207" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1207" => #{"Name"=>"EndTickPriceRange" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1207"}


,
"TickIncrement" => #{"TagNum" => "1208" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1208" => #{"Name"=>"TickIncrement" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1208"}


,
"TickRuleType" => #{"TagNum" => "1209" ,"Type" => "INT" ,"ValidValues" =>[{"0", "REGULAR"},{"1", "VARIABLE"},{"2", "FIXED"},{"3", "TRADED_AS_A_SPREAD_LEG"},{"4", "SETTLED_AS_A_SPREAD_LEG"}]}
, "1209" => #{"Name"=>"TickRuleType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "REGULAR"},{"1", "VARIABLE"},{"2", "FIXED"},{"3", "TRADED_AS_A_SPREAD_LEG"},{"4", "SETTLED_AS_A_SPREAD_LEG"}], "TagNum" => "1209"}


,
"NestedInstrAttribType" => #{"TagNum" => "1210" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1210" => #{"Name"=>"NestedInstrAttribType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1210"}


,
"NestedInstrAttribValue" => #{"TagNum" => "1211" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1211" => #{"Name"=>"NestedInstrAttribValue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1211"}


,
"LegMaturityTime" => #{"TagNum" => "1212" ,"Type" => "TZTIMEONLY" ,"ValidValues" =>[]}
, "1212" => #{"Name"=>"LegMaturityTime" ,"Type"=>"TZTIMEONLY" ,"ValidValues"=>[], "TagNum" => "1212"}


,
"UnderlyingMaturityTime" => #{"TagNum" => "1213" ,"Type" => "TZTIMEONLY" ,"ValidValues" =>[]}
, "1213" => #{"Name"=>"UnderlyingMaturityTime" ,"Type"=>"TZTIMEONLY" ,"ValidValues"=>[], "TagNum" => "1213"}


,
"DerivativeSymbol" => #{"TagNum" => "1214" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1214" => #{"Name"=>"DerivativeSymbol" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1214"}


,
"DerivativeSymbolSfx" => #{"TagNum" => "1215" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1215" => #{"Name"=>"DerivativeSymbolSfx" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1215"}


,
"DerivativeSecurityID" => #{"TagNum" => "1216" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1216" => #{"Name"=>"DerivativeSecurityID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1216"}


,
"DerivativeSecurityIDSource" => #{"TagNum" => "1217" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1217" => #{"Name"=>"DerivativeSecurityIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1217"}


,
"NoDerivativeSecurityAltID" => #{"TagNum" => "1218" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1218" => #{"Name"=>"NoDerivativeSecurityAltID" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1218"}


,
"DerivativeSecurityAltID" => #{"TagNum" => "1219" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1219" => #{"Name"=>"DerivativeSecurityAltID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1219"}


,
"DerivativeSecurityAltIDSource" => #{"TagNum" => "1220" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1220" => #{"Name"=>"DerivativeSecurityAltIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1220"}


,
"SecondaryLowLimitPrice" => #{"TagNum" => "1221" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1221" => #{"Name"=>"SecondaryLowLimitPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1221"}


,
"MaturityRuleID" => #{"TagNum" => "1222" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1222" => #{"Name"=>"MaturityRuleID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1222"}


,
"StrikeRuleID" => #{"TagNum" => "1223" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1223" => #{"Name"=>"StrikeRuleID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1223"}


,
"LegUnitOfMeasureQty" => #{"TagNum" => "1224" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1224" => #{"Name"=>"LegUnitOfMeasureQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1224"}


,
"DerivativeOptPayAmount" => #{"TagNum" => "1225" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "1225" => #{"Name"=>"DerivativeOptPayAmount" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "1225"}


,
"EndMaturityMonthYear" => #{"TagNum" => "1226" ,"Type" => "MONTHYEAR" ,"ValidValues" =>[]}
, "1226" => #{"Name"=>"EndMaturityMonthYear" ,"Type"=>"MONTHYEAR" ,"ValidValues"=>[], "TagNum" => "1226"}


,
"ProductComplex" => #{"TagNum" => "1227" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1227" => #{"Name"=>"ProductComplex" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1227"}


,
"DerivativeProductComplex" => #{"TagNum" => "1228" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1228" => #{"Name"=>"DerivativeProductComplex" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1228"}


,
"MaturityMonthYearIncrement" => #{"TagNum" => "1229" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1229" => #{"Name"=>"MaturityMonthYearIncrement" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1229"}


,
"SecondaryHighLimitPrice" => #{"TagNum" => "1230" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1230" => #{"Name"=>"SecondaryHighLimitPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1230"}


,
"MinLotSize" => #{"TagNum" => "1231" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1231" => #{"Name"=>"MinLotSize" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1231"}


,
"NoExecInstRules" => #{"TagNum" => "1232" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1232" => #{"Name"=>"NoExecInstRules" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1232"}


,
"NoLotTypeRules" => #{"TagNum" => "1234" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1234" => #{"Name"=>"NoLotTypeRules" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1234"}


,
"NoMatchRules" => #{"TagNum" => "1235" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1235" => #{"Name"=>"NoMatchRules" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1235"}


,
"NoMaturityRules" => #{"TagNum" => "1236" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1236" => #{"Name"=>"NoMaturityRules" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1236"}


,
"NoOrdTypeRules" => #{"TagNum" => "1237" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1237" => #{"Name"=>"NoOrdTypeRules" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1237"}


,
"NoTimeInForceRules" => #{"TagNum" => "1239" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1239" => #{"Name"=>"NoTimeInForceRules" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1239"}


,
"SecondaryTradingReferencePrice" => #{"TagNum" => "1240" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1240" => #{"Name"=>"SecondaryTradingReferencePrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1240"}


,
"StartMaturityMonthYear" => #{"TagNum" => "1241" ,"Type" => "MONTHYEAR" ,"ValidValues" =>[]}
, "1241" => #{"Name"=>"StartMaturityMonthYear" ,"Type"=>"MONTHYEAR" ,"ValidValues"=>[], "TagNum" => "1241"}


,
"FlexProductEligibilityIndicator" => #{"TagNum" => "1242" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "1242" => #{"Name"=>"FlexProductEligibilityIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "1242"}


,
"DerivFlexProductEligibilityIndicator" => #{"TagNum" => "1243" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "1243" => #{"Name"=>"DerivFlexProductEligibilityIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "1243"}


,
"FlexibleIndicator" => #{"TagNum" => "1244" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "1244" => #{"Name"=>"FlexibleIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "1244"}


,
"TradingCurrency" => #{"TagNum" => "1245" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "1245" => #{"Name"=>"TradingCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "1245"}


,
"DerivativeProduct" => #{"TagNum" => "1246" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1246" => #{"Name"=>"DerivativeProduct" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1246"}


,
"DerivativeSecurityGroup" => #{"TagNum" => "1247" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1247" => #{"Name"=>"DerivativeSecurityGroup" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1247"}


,
"DerivativeCFICode" => #{"TagNum" => "1248" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1248" => #{"Name"=>"DerivativeCFICode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1248"}


,
"DerivativeSecurityType" => #{"TagNum" => "1249" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1249" => #{"Name"=>"DerivativeSecurityType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1249"}


,
"DerivativeSecuritySubType" => #{"TagNum" => "1250" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1250" => #{"Name"=>"DerivativeSecuritySubType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1250"}


,
"DerivativeMaturityMonthYear" => #{"TagNum" => "1251" ,"Type" => "MONTHYEAR" ,"ValidValues" =>[]}
, "1251" => #{"Name"=>"DerivativeMaturityMonthYear" ,"Type"=>"MONTHYEAR" ,"ValidValues"=>[], "TagNum" => "1251"}


,
"DerivativeMaturityDate" => #{"TagNum" => "1252" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "1252" => #{"Name"=>"DerivativeMaturityDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "1252"}


,
"DerivativeMaturityTime" => #{"TagNum" => "1253" ,"Type" => "TZTIMEONLY" ,"ValidValues" =>[]}
, "1253" => #{"Name"=>"DerivativeMaturityTime" ,"Type"=>"TZTIMEONLY" ,"ValidValues"=>[], "TagNum" => "1253"}


,
"DerivativeSettleOnOpenFlag" => #{"TagNum" => "1254" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1254" => #{"Name"=>"DerivativeSettleOnOpenFlag" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1254"}


,
"DerivativeInstrmtAssignmentMethod" => #{"TagNum" => "1255" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "1255" => #{"Name"=>"DerivativeInstrmtAssignmentMethod" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "1255"}


,
"DerivativeSecurityStatus" => #{"TagNum" => "1256" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1256" => #{"Name"=>"DerivativeSecurityStatus" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1256"}


,
"DerivativeInstrRegistry" => #{"TagNum" => "1257" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1257" => #{"Name"=>"DerivativeInstrRegistry" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1257"}


,
"DerivativeCountryOfIssue" => #{"TagNum" => "1258" ,"Type" => "COUNTRY" ,"ValidValues" =>[]}
, "1258" => #{"Name"=>"DerivativeCountryOfIssue" ,"Type"=>"COUNTRY" ,"ValidValues"=>[], "TagNum" => "1258"}


,
"DerivativeStateOrProvinceOfIssue" => #{"TagNum" => "1259" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1259" => #{"Name"=>"DerivativeStateOrProvinceOfIssue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1259"}


,
"DerivativeLocaleOfIssue" => #{"TagNum" => "1260" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1260" => #{"Name"=>"DerivativeLocaleOfIssue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1260"}


,
"DerivativeStrikePrice" => #{"TagNum" => "1261" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1261" => #{"Name"=>"DerivativeStrikePrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1261"}


,
"DerivativeStrikeCurrency" => #{"TagNum" => "1262" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "1262" => #{"Name"=>"DerivativeStrikeCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "1262"}


,
"DerivativeStrikeMultiplier" => #{"TagNum" => "1263" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "1263" => #{"Name"=>"DerivativeStrikeMultiplier" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "1263"}


,
"DerivativeStrikeValue" => #{"TagNum" => "1264" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "1264" => #{"Name"=>"DerivativeStrikeValue" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "1264"}


,
"DerivativeOptAttribute" => #{"TagNum" => "1265" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "1265" => #{"Name"=>"DerivativeOptAttribute" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "1265"}


,
"DerivativeContractMultiplier" => #{"TagNum" => "1266" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "1266" => #{"Name"=>"DerivativeContractMultiplier" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "1266"}


,
"DerivativeMinPriceIncrement" => #{"TagNum" => "1267" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "1267" => #{"Name"=>"DerivativeMinPriceIncrement" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "1267"}


,
"DerivativeMinPriceIncrementAmount" => #{"TagNum" => "1268" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "1268" => #{"Name"=>"DerivativeMinPriceIncrementAmount" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "1268"}


,
"DerivativeUnitOfMeasure" => #{"TagNum" => "1269" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1269" => #{"Name"=>"DerivativeUnitOfMeasure" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1269"}


,
"DerivativeUnitOfMeasureQty" => #{"TagNum" => "1270" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1270" => #{"Name"=>"DerivativeUnitOfMeasureQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1270"}


,
"DerivativeTimeUnit" => #{"TagNum" => "1271" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1271" => #{"Name"=>"DerivativeTimeUnit" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1271"}


,
"DerivativeSecurityExchange" => #{"TagNum" => "1272" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "1272" => #{"Name"=>"DerivativeSecurityExchange" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "1272"}


,
"DerivativePositionLimit" => #{"TagNum" => "1273" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1273" => #{"Name"=>"DerivativePositionLimit" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1273"}


,
"DerivativeNTPositionLimit" => #{"TagNum" => "1274" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1274" => #{"Name"=>"DerivativeNTPositionLimit" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1274"}


,
"DerivativeIssuer" => #{"TagNum" => "1275" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1275" => #{"Name"=>"DerivativeIssuer" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1275"}


,
"DerivativeIssueDate" => #{"TagNum" => "1276" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "1276" => #{"Name"=>"DerivativeIssueDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "1276"}


,
"DerivativeEncodedIssuerLen" => #{"TagNum" => "1277" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "1277" => #{"Name"=>"DerivativeEncodedIssuerLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "1277"}


,
"DerivativeEncodedIssuer" => #{"TagNum" => "1278" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "1278" => #{"Name"=>"DerivativeEncodedIssuer" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "1278"}


,
"DerivativeSecurityDesc" => #{"TagNum" => "1279" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1279" => #{"Name"=>"DerivativeSecurityDesc" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1279"}


,
"DerivativeEncodedSecurityDescLen" => #{"TagNum" => "1280" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "1280" => #{"Name"=>"DerivativeEncodedSecurityDescLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "1280"}


,
"DerivativeEncodedSecurityDesc" => #{"TagNum" => "1281" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "1281" => #{"Name"=>"DerivativeEncodedSecurityDesc" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "1281"}


,
"DerivativeSecurityXMLLen" => #{"TagNum" => "1282" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "1282" => #{"Name"=>"DerivativeSecurityXMLLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "1282"}


,
"DerivativeSecurityXML" => #{"TagNum" => "1283" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "1283" => #{"Name"=>"DerivativeSecurityXML" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "1283"}


,
"DerivativeSecurityXMLSchema" => #{"TagNum" => "1284" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1284" => #{"Name"=>"DerivativeSecurityXMLSchema" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1284"}


,
"DerivativeContractSettlMonth" => #{"TagNum" => "1285" ,"Type" => "MONTHYEAR" ,"ValidValues" =>[]}
, "1285" => #{"Name"=>"DerivativeContractSettlMonth" ,"Type"=>"MONTHYEAR" ,"ValidValues"=>[], "TagNum" => "1285"}


,
"NoDerivativeEvents" => #{"TagNum" => "1286" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1286" => #{"Name"=>"NoDerivativeEvents" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1286"}


,
"DerivativeEventType" => #{"TagNum" => "1287" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1287" => #{"Name"=>"DerivativeEventType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1287"}


,
"DerivativeEventDate" => #{"TagNum" => "1288" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "1288" => #{"Name"=>"DerivativeEventDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "1288"}


,
"DerivativeEventTime" => #{"TagNum" => "1289" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "1289" => #{"Name"=>"DerivativeEventTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "1289"}


,
"DerivativeEventPx" => #{"TagNum" => "1290" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1290" => #{"Name"=>"DerivativeEventPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1290"}


,
"DerivativeEventText" => #{"TagNum" => "1291" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1291" => #{"Name"=>"DerivativeEventText" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1291"}


,
"NoDerivativeInstrumentParties" => #{"TagNum" => "1292" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1292" => #{"Name"=>"NoDerivativeInstrumentParties" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1292"}


,
"DerivativeInstrumentPartyID" => #{"TagNum" => "1293" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1293" => #{"Name"=>"DerivativeInstrumentPartyID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1293"}


,
"DerivativeInstrumentPartyIDSource" => #{"TagNum" => "1294" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1294" => #{"Name"=>"DerivativeInstrumentPartyIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1294"}


,
"DerivativeInstrumentPartyRole" => #{"TagNum" => "1295" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1295" => #{"Name"=>"DerivativeInstrumentPartyRole" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1295"}


,
"NoDerivativeInstrumentPartySubIDs" => #{"TagNum" => "1296" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1296" => #{"Name"=>"NoDerivativeInstrumentPartySubIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1296"}


,
"DerivativeInstrumentPartySubID" => #{"TagNum" => "1297" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1297" => #{"Name"=>"DerivativeInstrumentPartySubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1297"}


,
"DerivativeInstrumentPartySubIDType" => #{"TagNum" => "1298" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1298" => #{"Name"=>"DerivativeInstrumentPartySubIDType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1298"}


,
"DerivativeExerciseStyle" => #{"TagNum" => "1299" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "1299" => #{"Name"=>"DerivativeExerciseStyle" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "1299"}


,
"MarketSegmentID" => #{"TagNum" => "1300" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1300" => #{"Name"=>"MarketSegmentID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1300"}


,
"MarketID" => #{"TagNum" => "1301" ,"Type" => "EXCHANGE" ,"ValidValues" =>[]}
, "1301" => #{"Name"=>"MarketID" ,"Type"=>"EXCHANGE" ,"ValidValues"=>[], "TagNum" => "1301"}


,
"MaturityMonthYearIncrementUnits" => #{"TagNum" => "1302" ,"Type" => "INT" ,"ValidValues" =>[{"0", "MONTHS"},{"1", "DAYS"},{"2", "WEEKS"},{"3", "YEARS"}]}
, "1302" => #{"Name"=>"MaturityMonthYearIncrementUnits" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "MONTHS"},{"1", "DAYS"},{"2", "WEEKS"},{"3", "YEARS"}], "TagNum" => "1302"}


,
"MaturityMonthYearFormat" => #{"TagNum" => "1303" ,"Type" => "INT" ,"ValidValues" =>[{"0", "YEARMONTH_ONLY"},{"1", "YEARMONTHDAY"},{"2", "YEARMONTHWEEK"}]}
, "1303" => #{"Name"=>"MaturityMonthYearFormat" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "YEARMONTH_ONLY"},{"1", "YEARMONTHDAY"},{"2", "YEARMONTHWEEK"}], "TagNum" => "1303"}


,
"StrikeExerciseStyle" => #{"TagNum" => "1304" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1304" => #{"Name"=>"StrikeExerciseStyle" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1304"}


,
"SecondaryPriceLimitType" => #{"TagNum" => "1305" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1305" => #{"Name"=>"SecondaryPriceLimitType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1305"}


,
"PriceLimitType" => #{"TagNum" => "1306" ,"Type" => "INT" ,"ValidValues" =>[{"0", "PRICE"},{"1", "TICKS"},{"2", "PERCENTAGE"}]}
, "1306" => #{"Name"=>"PriceLimitType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "PRICE"},{"1", "TICKS"},{"2", "PERCENTAGE"}], "TagNum" => "1306"}


,
"ExecInstValue" => #{"TagNum" => "1308" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "1308" => #{"Name"=>"ExecInstValue" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "1308"}


,
"NoTradingSessionRules" => #{"TagNum" => "1309" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1309" => #{"Name"=>"NoTradingSessionRules" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1309"}


,
"NoMarketSegments" => #{"TagNum" => "1310" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1310" => #{"Name"=>"NoMarketSegments" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1310"}


,
"NoDerivativeInstrAttrib" => #{"TagNum" => "1311" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1311" => #{"Name"=>"NoDerivativeInstrAttrib" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1311"}


,
"NoNestedInstrAttrib" => #{"TagNum" => "1312" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1312" => #{"Name"=>"NoNestedInstrAttrib" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1312"}


,
"DerivativeInstrAttribType" => #{"TagNum" => "1313" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1313" => #{"Name"=>"DerivativeInstrAttribType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1313"}


,
"DerivativeInstrAttribValue" => #{"TagNum" => "1314" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1314" => #{"Name"=>"DerivativeInstrAttribValue" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1314"}


,
"DerivativePriceUnitOfMeasure" => #{"TagNum" => "1315" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1315" => #{"Name"=>"DerivativePriceUnitOfMeasure" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1315"}


,
"DerivativePriceUnitOfMeasureQty" => #{"TagNum" => "1316" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1316" => #{"Name"=>"DerivativePriceUnitOfMeasureQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1316"}


,
"DerivativeSettlMethod" => #{"TagNum" => "1317" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "1317" => #{"Name"=>"DerivativeSettlMethod" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "1317"}


,
"DerivativePriceQuoteMethod" => #{"TagNum" => "1318" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1318" => #{"Name"=>"DerivativePriceQuoteMethod" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1318"}


,
"DerivativeValuationMethod" => #{"TagNum" => "1319" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1319" => #{"Name"=>"DerivativeValuationMethod" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1319"}


,
"DerivativeListMethod" => #{"TagNum" => "1320" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1320" => #{"Name"=>"DerivativeListMethod" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1320"}


,
"DerivativeCapPrice" => #{"TagNum" => "1321" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1321" => #{"Name"=>"DerivativeCapPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1321"}


,
"DerivativeFloorPrice" => #{"TagNum" => "1322" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1322" => #{"Name"=>"DerivativeFloorPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1322"}


,
"DerivativePutOrCall" => #{"TagNum" => "1323" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1323" => #{"Name"=>"DerivativePutOrCall" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1323"}


,
"ListUpdateAction" => #{"TagNum" => "1324" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "1324" => #{"Name"=>"ListUpdateAction" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "1324"}


,
"ParentMktSegmID" => #{"TagNum" => "1325" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1325" => #{"Name"=>"ParentMktSegmID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1325"}


,
"TradingSessionDesc" => #{"TagNum" => "1326" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1326" => #{"Name"=>"TradingSessionDesc" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1326"}


,
"TradSesUpdateAction" => #{"TagNum" => "1327" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "1327" => #{"Name"=>"TradSesUpdateAction" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "1327"}


,
"RejectText" => #{"TagNum" => "1328" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1328" => #{"Name"=>"RejectText" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1328"}


,
"FeeMultiplier" => #{"TagNum" => "1329" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "1329" => #{"Name"=>"FeeMultiplier" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "1329"}


,
"UnderlyingLegSymbol" => #{"TagNum" => "1330" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1330" => #{"Name"=>"UnderlyingLegSymbol" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1330"}


,
"UnderlyingLegSymbolSfx" => #{"TagNum" => "1331" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1331" => #{"Name"=>"UnderlyingLegSymbolSfx" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1331"}


,
"UnderlyingLegSecurityID" => #{"TagNum" => "1332" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1332" => #{"Name"=>"UnderlyingLegSecurityID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1332"}


,
"UnderlyingLegSecurityIDSource" => #{"TagNum" => "1333" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1333" => #{"Name"=>"UnderlyingLegSecurityIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1333"}


,
"NoUnderlyingLegSecurityAltID" => #{"TagNum" => "1334" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1334" => #{"Name"=>"NoUnderlyingLegSecurityAltID" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1334"}


,
"UnderlyingLegSecurityAltID" => #{"TagNum" => "1335" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1335" => #{"Name"=>"UnderlyingLegSecurityAltID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1335"}


,
"UnderlyingLegSecurityAltIDSource" => #{"TagNum" => "1336" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1336" => #{"Name"=>"UnderlyingLegSecurityAltIDSource" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1336"}


,
"UnderlyingLegSecurityType" => #{"TagNum" => "1337" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1337" => #{"Name"=>"UnderlyingLegSecurityType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1337"}


,
"UnderlyingLegSecuritySubType" => #{"TagNum" => "1338" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1338" => #{"Name"=>"UnderlyingLegSecuritySubType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1338"}


,
"UnderlyingLegMaturityMonthYear" => #{"TagNum" => "1339" ,"Type" => "MONTHYEAR" ,"ValidValues" =>[]}
, "1339" => #{"Name"=>"UnderlyingLegMaturityMonthYear" ,"Type"=>"MONTHYEAR" ,"ValidValues"=>[], "TagNum" => "1339"}


,
"UnderlyingLegStrikePrice" => #{"TagNum" => "1340" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1340" => #{"Name"=>"UnderlyingLegStrikePrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1340"}


,
"UnderlyingLegSecurityExchange" => #{"TagNum" => "1341" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1341" => #{"Name"=>"UnderlyingLegSecurityExchange" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1341"}


,
"NoOfLegUnderlyings" => #{"TagNum" => "1342" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1342" => #{"Name"=>"NoOfLegUnderlyings" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1342"}


,
"UnderlyingLegPutOrCall" => #{"TagNum" => "1343" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1343" => #{"Name"=>"UnderlyingLegPutOrCall" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1343"}


,
"UnderlyingLegCFICode" => #{"TagNum" => "1344" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1344" => #{"Name"=>"UnderlyingLegCFICode" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1344"}


,
"UnderlyingLegMaturityDate" => #{"TagNum" => "1345" ,"Type" => "LOCALMKTDATE" ,"ValidValues" =>[]}
, "1345" => #{"Name"=>"UnderlyingLegMaturityDate" ,"Type"=>"LOCALMKTDATE" ,"ValidValues"=>[], "TagNum" => "1345"}


,
"ApplReqID" => #{"TagNum" => "1346" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1346" => #{"Name"=>"ApplReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1346"}


,
"ApplReqType" => #{"TagNum" => "1347" ,"Type" => "INT" ,"ValidValues" =>[{"0", "RETRANSMISSION_OF_APPLICATION_MESSAGES_FOR_THE_SPECIFIED_APPLICATIONS"},{"1", "SUBSCRIPTION_TO_THE_SPECIFIED_APPLICATIONS"},{"2", "REQUEST_FOR_THE_LAST_APPLLASTSEQNUM_PUBLISHED_FOR_THE_SPECIFIED_APPLICATIONS"},{"3", "REQUEST_VALID_SET_OF_APPLICATIONS"},{"4", "UNSUBSCRIBE_TO_THE_SPECIFIED_APPLICATIONS"},{"5", "CANCEL_RETRANSMISSION"},{"6", "CANCEL_RETRANSMISSION_AND_UNSUBSCRIBE_TO_THE_SPECIFIED_APPLICATIONS"}]}
, "1347" => #{"Name"=>"ApplReqType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "RETRANSMISSION_OF_APPLICATION_MESSAGES_FOR_THE_SPECIFIED_APPLICATIONS"},{"1", "SUBSCRIPTION_TO_THE_SPECIFIED_APPLICATIONS"},{"2", "REQUEST_FOR_THE_LAST_APPLLASTSEQNUM_PUBLISHED_FOR_THE_SPECIFIED_APPLICATIONS"},{"3", "REQUEST_VALID_SET_OF_APPLICATIONS"},{"4", "UNSUBSCRIBE_TO_THE_SPECIFIED_APPLICATIONS"},{"5", "CANCEL_RETRANSMISSION"},{"6", "CANCEL_RETRANSMISSION_AND_UNSUBSCRIBE_TO_THE_SPECIFIED_APPLICATIONS"}], "TagNum" => "1347"}


,
"ApplResponseType" => #{"TagNum" => "1348" ,"Type" => "INT" ,"ValidValues" =>[{"0", "REQUEST_SUCCESSFULLY_PROCESSED"},{"1", "APPLICATION_DOES_NOT_EXIST"},{"2", "MESSAGES_NOT_AVAILABLE"}]}
, "1348" => #{"Name"=>"ApplResponseType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "REQUEST_SUCCESSFULLY_PROCESSED"},{"1", "APPLICATION_DOES_NOT_EXIST"},{"2", "MESSAGES_NOT_AVAILABLE"}], "TagNum" => "1348"}


,
"ApplTotalMessageCount" => #{"TagNum" => "1349" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1349" => #{"Name"=>"ApplTotalMessageCount" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1349"}


,
"ApplLastSeqNum" => #{"TagNum" => "1350" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "1350" => #{"Name"=>"ApplLastSeqNum" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "1350"}


,
"NoApplIDs" => #{"TagNum" => "1351" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1351" => #{"Name"=>"NoApplIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1351"}


,
"ApplResendFlag" => #{"TagNum" => "1352" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "1352" => #{"Name"=>"ApplResendFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "1352"}


,
"ApplResponseID" => #{"TagNum" => "1353" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1353" => #{"Name"=>"ApplResponseID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1353"}


,
"ApplResponseError" => #{"TagNum" => "1354" ,"Type" => "INT" ,"ValidValues" =>[{"0", "APPLICATION_DOES_NOT_EXIST"},{"1", "MESSAGES_REQUESTED_ARE_NOT_AVAILABLE"},{"2", "USER_NOT_AUTHORIZED_FOR_APPLICATION"}]}
, "1354" => #{"Name"=>"ApplResponseError" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "APPLICATION_DOES_NOT_EXIST"},{"1", "MESSAGES_REQUESTED_ARE_NOT_AVAILABLE"},{"2", "USER_NOT_AUTHORIZED_FOR_APPLICATION"}], "TagNum" => "1354"}


,
"RefApplID" => #{"TagNum" => "1355" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1355" => #{"Name"=>"RefApplID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1355"}


,
"ApplReportID" => #{"TagNum" => "1356" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1356" => #{"Name"=>"ApplReportID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1356"}


,
"RefApplLastSeqNum" => #{"TagNum" => "1357" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "1357" => #{"Name"=>"RefApplLastSeqNum" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "1357"}


,
"LegPutOrCall" => #{"TagNum" => "1358" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1358" => #{"Name"=>"LegPutOrCall" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1358"}


,
"TotNoFills" => #{"TagNum" => "1361" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1361" => #{"Name"=>"TotNoFills" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1361"}


,
"NoFills" => #{"TagNum" => "1362" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1362" => #{"Name"=>"NoFills" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1362"}


,
"FillExecID" => #{"TagNum" => "1363" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1363" => #{"Name"=>"FillExecID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1363"}


,
"FillPx" => #{"TagNum" => "1364" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1364" => #{"Name"=>"FillPx" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1364"}


,
"FillQty" => #{"TagNum" => "1365" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1365" => #{"Name"=>"FillQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1365"}


,
"LegAllocID" => #{"TagNum" => "1366" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1366" => #{"Name"=>"LegAllocID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1366"}


,
"LegAllocSettlCurrency" => #{"TagNum" => "1367" ,"Type" => "CURRENCY" ,"ValidValues" =>[]}
, "1367" => #{"Name"=>"LegAllocSettlCurrency" ,"Type"=>"CURRENCY" ,"ValidValues"=>[], "TagNum" => "1367"}


,
"TradSesEvent" => #{"TagNum" => "1368" ,"Type" => "INT" ,"ValidValues" =>[{"0", "TRADING_RESUMES"},{"1", "CHANGE_OF_TRADING_SESSION"},{"2", "CHANGE_OF_TRADING_SUBSESSION"},{"3", "CHANGE_OF_TRADING_STATUS"}]}
, "1368" => #{"Name"=>"TradSesEvent" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "TRADING_RESUMES"},{"1", "CHANGE_OF_TRADING_SESSION"},{"2", "CHANGE_OF_TRADING_SUBSESSION"},{"3", "CHANGE_OF_TRADING_STATUS"}], "TagNum" => "1368"}


,
"MassActionReportID" => #{"TagNum" => "1369" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1369" => #{"Name"=>"MassActionReportID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1369"}


,
"NoNotAffectedOrders" => #{"TagNum" => "1370" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1370" => #{"Name"=>"NoNotAffectedOrders" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1370"}


,
"NotAffectedOrderID" => #{"TagNum" => "1371" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1371" => #{"Name"=>"NotAffectedOrderID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1371"}


,
"NotAffOrigClOrdID" => #{"TagNum" => "1372" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1372" => #{"Name"=>"NotAffOrigClOrdID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1372"}


,
"MassActionType" => #{"TagNum" => "1373" ,"Type" => "INT" ,"ValidValues" =>[{"1", "SUSPEND_ORDERS"},{"2", "RELEASE_ORDERS_FROM_SUSPENSION"},{"3", "CANCEL_ORDERS"}]}
, "1373" => #{"Name"=>"MassActionType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "SUSPEND_ORDERS"},{"2", "RELEASE_ORDERS_FROM_SUSPENSION"},{"3", "CANCEL_ORDERS"}], "TagNum" => "1373"}


,
"MassActionScope" => #{"TagNum" => "1374" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ALL_ORDERS_FOR_A_SECURITY"},{"2", "ALL_ORDERS_FOR_AN_UNDERLYING_SECURITY"},{"3", "ALL_ORDERS_FOR_A_PRODUCT"},{"4", "ALL_ORDERS_FOR_A_CFICODE"},{"5", "ALL_ORDERS_FOR_A_SECURITYTYPE"},{"6", "ALL_ORDERS_FOR_A_TRADING_SESSION"},{"7", "ALL_ORDERS"},{"8", "ALL_ORDERS_FOR_A_MARKET"},{"9", "ALL_ORDERS_FOR_A_MARKET_SEGMENT"},{"10", "ALL_ORDERS_FOR_A_SECURITY_GROUP"},{"11", "CANCEL_FOR_SECURITY_ISSUER"},{"12", "CANCEL_FOR_ISSUER_OF_UNDERLYING_SECURITY"}]}
, "1374" => #{"Name"=>"MassActionScope" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ALL_ORDERS_FOR_A_SECURITY"},{"2", "ALL_ORDERS_FOR_AN_UNDERLYING_SECURITY"},{"3", "ALL_ORDERS_FOR_A_PRODUCT"},{"4", "ALL_ORDERS_FOR_A_CFICODE"},{"5", "ALL_ORDERS_FOR_A_SECURITYTYPE"},{"6", "ALL_ORDERS_FOR_A_TRADING_SESSION"},{"7", "ALL_ORDERS"},{"8", "ALL_ORDERS_FOR_A_MARKET"},{"9", "ALL_ORDERS_FOR_A_MARKET_SEGMENT"},{"10", "ALL_ORDERS_FOR_A_SECURITY_GROUP"},{"11", "CANCEL_FOR_SECURITY_ISSUER"},{"12", "CANCEL_FOR_ISSUER_OF_UNDERLYING_SECURITY"}], "TagNum" => "1374"}


,
"MassActionResponse" => #{"TagNum" => "1375" ,"Type" => "INT" ,"ValidValues" =>[{"0", "REJECTED"},{"1", "ACCEPTED"}]}
, "1375" => #{"Name"=>"MassActionResponse" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "REJECTED"},{"1", "ACCEPTED"}], "TagNum" => "1375"}


,
"MassActionRejectReason" => #{"TagNum" => "1376" ,"Type" => "INT" ,"ValidValues" =>[{"0", "MASS_ACTION_NOT_SUPPORTED"},{"1", "INVALID_OR_UNKNOWN_SECURITY"},{"2", "INVALID_OR_UNKNOWN_UNDERLYING_SECURITY"},{"3", "INVALID_OR_UNKNOWN_PRODUCT"},{"4", "INVALID_OR_UNKNOWN_CFICODE"},{"5", "INVALID_OR_UNKNOWN_SECURITYTYPE"},{"6", "INVALID_OR_UNKNOWN_TRADING_SESSION"},{"7", "INVALID_OR_UNKNOWN_MARKET"},{"8", "INVALID_OR_UNKNOWN_MARKET_SEGMENT"},{"9", "INVALID_OR_UNKNOWN_SECURITY_GROUP"},{"99", "OTHER"},{"10", "INVALID_OR_UNKNOWN_SECURITY_ISSUER"},{"11", "INVALID_OR_UNKNOWN_ISSUER_OF_UNDERLYING_SECURITY"}]}
, "1376" => #{"Name"=>"MassActionRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "MASS_ACTION_NOT_SUPPORTED"},{"1", "INVALID_OR_UNKNOWN_SECURITY"},{"2", "INVALID_OR_UNKNOWN_UNDERLYING_SECURITY"},{"3", "INVALID_OR_UNKNOWN_PRODUCT"},{"4", "INVALID_OR_UNKNOWN_CFICODE"},{"5", "INVALID_OR_UNKNOWN_SECURITYTYPE"},{"6", "INVALID_OR_UNKNOWN_TRADING_SESSION"},{"7", "INVALID_OR_UNKNOWN_MARKET"},{"8", "INVALID_OR_UNKNOWN_MARKET_SEGMENT"},{"9", "INVALID_OR_UNKNOWN_SECURITY_GROUP"},{"99", "OTHER"},{"10", "INVALID_OR_UNKNOWN_SECURITY_ISSUER"},{"11", "INVALID_OR_UNKNOWN_ISSUER_OF_UNDERLYING_SECURITY"}], "TagNum" => "1376"}


,
"MultilegModel" => #{"TagNum" => "1377" ,"Type" => "INT" ,"ValidValues" =>[{"0", "PREDEFINED_MULTILEG_SECURITY"},{"1", "USER_DEFINED_MULTLEG_SECURITY"},{"2", "USER_DEFINED_NON_SECURITIZED_MULTILEG"}]}
, "1377" => #{"Name"=>"MultilegModel" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "PREDEFINED_MULTILEG_SECURITY"},{"1", "USER_DEFINED_MULTLEG_SECURITY"},{"2", "USER_DEFINED_NON_SECURITIZED_MULTILEG"}], "TagNum" => "1377"}


,
"MultilegPriceMethod" => #{"TagNum" => "1378" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NET_PRICE"},{"1", "REVERSED_NET_PRICE"},{"2", "YIELD_DIFFERENCE"},{"3", "INDIVIDUAL"},{"4", "CONTRACT_WEIGHTED_AVERAGE_PRICE"},{"5", "MULTIPLIED_PRICE"}]}
, "1378" => #{"Name"=>"MultilegPriceMethod" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NET_PRICE"},{"1", "REVERSED_NET_PRICE"},{"2", "YIELD_DIFFERENCE"},{"3", "INDIVIDUAL"},{"4", "CONTRACT_WEIGHTED_AVERAGE_PRICE"},{"5", "MULTIPLIED_PRICE"}], "TagNum" => "1378"}


,
"LegVolatility" => #{"TagNum" => "1379" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "1379" => #{"Name"=>"LegVolatility" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "1379"}


,
"DividendYield" => #{"TagNum" => "1380" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "1380" => #{"Name"=>"DividendYield" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "1380"}


,
"LegDividendYield" => #{"TagNum" => "1381" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "1381" => #{"Name"=>"LegDividendYield" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "1381"}


,
"CurrencyRatio" => #{"TagNum" => "1382" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "1382" => #{"Name"=>"CurrencyRatio" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "1382"}


,
"LegCurrencyRatio" => #{"TagNum" => "1383" ,"Type" => "FLOAT" ,"ValidValues" =>[]}
, "1383" => #{"Name"=>"LegCurrencyRatio" ,"Type"=>"FLOAT" ,"ValidValues"=>[], "TagNum" => "1383"}


,
"LegExecInst" => #{"TagNum" => "1384" ,"Type" => "MULTIPLECHARVALUE" ,"ValidValues" =>[]}
, "1384" => #{"Name"=>"LegExecInst" ,"Type"=>"MULTIPLECHARVALUE" ,"ValidValues"=>[], "TagNum" => "1384"}


,
"ContingencyType" => #{"TagNum" => "1385" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ONE_CANCELS_THE_OTHER"},{"2", "ONE_TRIGGERS_THE_OTHER"},{"3", "ONE_UPDATES_THE_OTHER_3"},{"4", "ONE_UPDATES_THE_OTHER_4"}]}
, "1385" => #{"Name"=>"ContingencyType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ONE_CANCELS_THE_OTHER"},{"2", "ONE_TRIGGERS_THE_OTHER"},{"3", "ONE_UPDATES_THE_OTHER_3"},{"4", "ONE_UPDATES_THE_OTHER_4"}], "TagNum" => "1385"}


,
"ListRejectReason" => #{"TagNum" => "1386" ,"Type" => "INT" ,"ValidValues" =>[{"0", "BROKER"},{"2", "EXCHANGE_CLOSED"},{"4", "TOO_LATE_TO_ENTER"},{"5", "UNKNOWN_ORDER"},{"6", "DUPLICATE_ORDER"},{"11", "UNSUPPORTED_ORDER_CHARACTERISTIC"},{"99", "OTHER"}]}
, "1386" => #{"Name"=>"ListRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "BROKER"},{"2", "EXCHANGE_CLOSED"},{"4", "TOO_LATE_TO_ENTER"},{"5", "UNKNOWN_ORDER"},{"6", "DUPLICATE_ORDER"},{"11", "UNSUPPORTED_ORDER_CHARACTERISTIC"},{"99", "OTHER"}], "TagNum" => "1386"}


,
"NoTrdRepIndicators" => #{"TagNum" => "1387" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1387" => #{"Name"=>"NoTrdRepIndicators" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1387"}


,
"TrdRepPartyRole" => #{"TagNum" => "1388" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1388" => #{"Name"=>"TrdRepPartyRole" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1388"}


,
"TrdRepIndicator" => #{"TagNum" => "1389" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "1389" => #{"Name"=>"TrdRepIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "1389"}


,
"TradePublishIndicator" => #{"TagNum" => "1390" ,"Type" => "INT" ,"ValidValues" =>[{"0", "DO_NOT_PUBLISH_TRADE"},{"1", "PUBLISH_TRADE"},{"2", "DEFERRED_PUBLICATION"}]}
, "1390" => #{"Name"=>"TradePublishIndicator" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "DO_NOT_PUBLISH_TRADE"},{"1", "PUBLISH_TRADE"},{"2", "DEFERRED_PUBLICATION"}], "TagNum" => "1390"}


,
"UnderlyingLegOptAttribute" => #{"TagNum" => "1391" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "1391" => #{"Name"=>"UnderlyingLegOptAttribute" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "1391"}


,
"UnderlyingLegSecurityDesc" => #{"TagNum" => "1392" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1392" => #{"Name"=>"UnderlyingLegSecurityDesc" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1392"}


,
"MarketReqID" => #{"TagNum" => "1393" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1393" => #{"Name"=>"MarketReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1393"}


,
"MarketReportID" => #{"TagNum" => "1394" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1394" => #{"Name"=>"MarketReportID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1394"}


,
"MarketUpdateAction" => #{"TagNum" => "1395" ,"Type" => "CHAR" ,"ValidValues" =>[{"A", "ADD"},{"D", "DELETE"},{"M", "MODIFY"}]}
, "1395" => #{"Name"=>"MarketUpdateAction" ,"Type"=>"CHAR" ,"ValidValues"=>[{"A", "ADD"},{"D", "DELETE"},{"M", "MODIFY"}], "TagNum" => "1395"}


,
"MarketSegmentDesc" => #{"TagNum" => "1396" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1396" => #{"Name"=>"MarketSegmentDesc" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1396"}


,
"EncodedMktSegmDescLen" => #{"TagNum" => "1397" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "1397" => #{"Name"=>"EncodedMktSegmDescLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "1397"}


,
"EncodedMktSegmDesc" => #{"TagNum" => "1398" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "1398" => #{"Name"=>"EncodedMktSegmDesc" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "1398"}


,
"ApplNewSeqNum" => #{"TagNum" => "1399" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "1399" => #{"Name"=>"ApplNewSeqNum" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "1399"}


,
"EncryptedPasswordMethod" => #{"TagNum" => "1400" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1400" => #{"Name"=>"EncryptedPasswordMethod" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1400"}


,
"EncryptedPasswordLen" => #{"TagNum" => "1401" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "1401" => #{"Name"=>"EncryptedPasswordLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "1401"}


,
"EncryptedPassword" => #{"TagNum" => "1402" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "1402" => #{"Name"=>"EncryptedPassword" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "1402"}


,
"EncryptedNewPasswordLen" => #{"TagNum" => "1403" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "1403" => #{"Name"=>"EncryptedNewPasswordLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "1403"}


,
"EncryptedNewPassword" => #{"TagNum" => "1404" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "1404" => #{"Name"=>"EncryptedNewPassword" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "1404"}


,
"UnderlyingLegMaturityTime" => #{"TagNum" => "1405" ,"Type" => "TZTIMEONLY" ,"ValidValues" =>[]}
, "1405" => #{"Name"=>"UnderlyingLegMaturityTime" ,"Type"=>"TZTIMEONLY" ,"ValidValues"=>[], "TagNum" => "1405"}


,
"RefApplExtID" => #{"TagNum" => "1406" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1406" => #{"Name"=>"RefApplExtID" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1406"}


,
"DefaultApplExtID" => #{"TagNum" => "1407" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1407" => #{"Name"=>"DefaultApplExtID" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1407"}


,
"DefaultCstmApplVerID" => #{"TagNum" => "1408" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1408" => #{"Name"=>"DefaultCstmApplVerID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1408"}


,
"SessionStatus" => #{"TagNum" => "1409" ,"Type" => "INT" ,"ValidValues" =>[{"0", "SESSION_ACTIVE"},{"1", "SESSION_PASSWORD_CHANGED"},{"2", "SESSION_PASSWORD_DUE_TO_EXPIRE"},{"3", "NEW_SESSION_PASSWORD_DOES_NOT_COMPLY_WITH_POLICY"},{"4", "SESSION_LOGOUT_COMPLETE"},{"5", "INVALID_USERNAME_OR_PASSWORD"},{"6", "ACCOUNT_LOCKED"},{"7", "LOGONS_ARE_NOT_ALLOWED_AT_THIS_TIME"},{"8", "PASSWORD_EXPIRED"}]}
, "1409" => #{"Name"=>"SessionStatus" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "SESSION_ACTIVE"},{"1", "SESSION_PASSWORD_CHANGED"},{"2", "SESSION_PASSWORD_DUE_TO_EXPIRE"},{"3", "NEW_SESSION_PASSWORD_DOES_NOT_COMPLY_WITH_POLICY"},{"4", "SESSION_LOGOUT_COMPLETE"},{"5", "INVALID_USERNAME_OR_PASSWORD"},{"6", "ACCOUNT_LOCKED"},{"7", "LOGONS_ARE_NOT_ALLOWED_AT_THIS_TIME"},{"8", "PASSWORD_EXPIRED"}], "TagNum" => "1409"}


,
"DefaultVerIndicator" => #{"TagNum" => "1410" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "1410" => #{"Name"=>"DefaultVerIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "1410"}


,
"Nested4PartySubIDType" => #{"TagNum" => "1411" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1411" => #{"Name"=>"Nested4PartySubIDType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1411"}


,
"Nested4PartySubID" => #{"TagNum" => "1412" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1412" => #{"Name"=>"Nested4PartySubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1412"}


,
"NoNested4PartySubIDs" => #{"TagNum" => "1413" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1413" => #{"Name"=>"NoNested4PartySubIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1413"}


,
"NoNested4PartyIDs" => #{"TagNum" => "1414" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1414" => #{"Name"=>"NoNested4PartyIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1414"}


,
"Nested4PartyID" => #{"TagNum" => "1415" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1415" => #{"Name"=>"Nested4PartyID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1415"}


,
"Nested4PartyIDSource" => #{"TagNum" => "1416" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "1416" => #{"Name"=>"Nested4PartyIDSource" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "1416"}


,
"Nested4PartyRole" => #{"TagNum" => "1417" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1417" => #{"Name"=>"Nested4PartyRole" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1417"}


,
"LegLastQty" => #{"TagNum" => "1418" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1418" => #{"Name"=>"LegLastQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1418"}


,
"UnderlyingExerciseStyle" => #{"TagNum" => "1419" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1419" => #{"Name"=>"UnderlyingExerciseStyle" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1419"}


,
"LegExerciseStyle" => #{"TagNum" => "1420" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1420" => #{"Name"=>"LegExerciseStyle" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1420"}


,
"LegPriceUnitOfMeasure" => #{"TagNum" => "1421" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1421" => #{"Name"=>"LegPriceUnitOfMeasure" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1421"}


,
"LegPriceUnitOfMeasureQty" => #{"TagNum" => "1422" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1422" => #{"Name"=>"LegPriceUnitOfMeasureQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1422"}


,
"UnderlyingUnitOfMeasureQty" => #{"TagNum" => "1423" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1423" => #{"Name"=>"UnderlyingUnitOfMeasureQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1423"}


,
"UnderlyingPriceUnitOfMeasure" => #{"TagNum" => "1424" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1424" => #{"Name"=>"UnderlyingPriceUnitOfMeasure" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1424"}


,
"UnderlyingPriceUnitOfMeasureQty" => #{"TagNum" => "1425" ,"Type" => "QTY" ,"ValidValues" =>[]}
, "1425" => #{"Name"=>"UnderlyingPriceUnitOfMeasureQty" ,"Type"=>"QTY" ,"ValidValues"=>[], "TagNum" => "1425"}


,
"ApplReportType" => #{"TagNum" => "1426" ,"Type" => "INT" ,"ValidValues" =>[{"0", "RESET_APPLSEQNUM_TO_NEW_VALUE_SPECIFIED_IN_APPLNEWSEQNUM"},{"1", "REPORTS_THAT_THE_LAST_MESSAGE_HAS_BEEN_SENT_FOR_THE_APPLIDS_REFER_TO_REFAPPLLASTSEQNUM"},{"2", "HEARTBEAT_MESSAGE_INDICATING_THAT_APPLICATION_IDENTIFIED_BY_REFAPPLID"},{"3", "APPLICATION_MESSAGE_RE_SEND_COMPLETED"}]}
, "1426" => #{"Name"=>"ApplReportType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "RESET_APPLSEQNUM_TO_NEW_VALUE_SPECIFIED_IN_APPLNEWSEQNUM"},{"1", "REPORTS_THAT_THE_LAST_MESSAGE_HAS_BEEN_SENT_FOR_THE_APPLIDS_REFER_TO_REFAPPLLASTSEQNUM"},{"2", "HEARTBEAT_MESSAGE_INDICATING_THAT_APPLICATION_IDENTIFIED_BY_REFAPPLID"},{"3", "APPLICATION_MESSAGE_RE_SEND_COMPLETED"}], "TagNum" => "1426"}


,
"SideExecID" => #{"TagNum" => "1427" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1427" => #{"Name"=>"SideExecID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1427"}


,
"OrderDelay" => #{"TagNum" => "1428" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1428" => #{"Name"=>"OrderDelay" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1428"}


,
"OrderDelayUnit" => #{"TagNum" => "1429" ,"Type" => "INT" ,"ValidValues" =>[{"0", "SECONDS"},{"1", "TENTHS_OF_A_SECOND"},{"2", "HUNDREDTHS_OF_A_SECOND"},{"3", "MILLISECONDS"},{"4", "MICROSECONDS"},{"5", "NANOSECONDS"},{"10", "MINUTES"},{"11", "HOURS"},{"12", "DAYS"},{"13", "WEEKS"},{"14", "MONTHS"},{"15", "YEARS"}]}
, "1429" => #{"Name"=>"OrderDelayUnit" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "SECONDS"},{"1", "TENTHS_OF_A_SECOND"},{"2", "HUNDREDTHS_OF_A_SECOND"},{"3", "MILLISECONDS"},{"4", "MICROSECONDS"},{"5", "NANOSECONDS"},{"10", "MINUTES"},{"11", "HOURS"},{"12", "DAYS"},{"13", "WEEKS"},{"14", "MONTHS"},{"15", "YEARS"}], "TagNum" => "1429"}


,
"VenueType" => #{"TagNum" => "1430" ,"Type" => "CHAR" ,"ValidValues" =>[{"E", "ELECTRONIC"},{"P", "PIT"},{"X", "EX_PIT"}]}
, "1430" => #{"Name"=>"VenueType" ,"Type"=>"CHAR" ,"ValidValues"=>[{"E", "ELECTRONIC"},{"P", "PIT"},{"X", "EX_PIT"}], "TagNum" => "1430"}


,
"RefOrdIDReason" => #{"TagNum" => "1431" ,"Type" => "INT" ,"ValidValues" =>[{"0", "GTC_FROM_PREVIOUS_DAY"},{"1", "PARTIAL_FILL_REMAINING"},{"2", "ORDER_CHANGED"}]}
, "1431" => #{"Name"=>"RefOrdIDReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "GTC_FROM_PREVIOUS_DAY"},{"1", "PARTIAL_FILL_REMAINING"},{"2", "ORDER_CHANGED"}], "TagNum" => "1431"}


,
"OrigCustOrderCapacity" => #{"TagNum" => "1432" ,"Type" => "INT" ,"ValidValues" =>[{"1", "MEMBER_TRADING_FOR_THEIR_OWN_ACCOUNT"},{"2", "CLEARING_FIRM_TRADING_FOR_ITS_PROPRIETARY_ACCOUNT"},{"3", "MEMBER_TRADING_FOR_ANOTHER_MEMBER"},{"4", "ALL_OTHER"}]}
, "1432" => #{"Name"=>"OrigCustOrderCapacity" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "MEMBER_TRADING_FOR_THEIR_OWN_ACCOUNT"},{"2", "CLEARING_FIRM_TRADING_FOR_ITS_PROPRIETARY_ACCOUNT"},{"3", "MEMBER_TRADING_FOR_ANOTHER_MEMBER"},{"4", "ALL_OTHER"}], "TagNum" => "1432"}


,
"RefApplReqID" => #{"TagNum" => "1433" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1433" => #{"Name"=>"RefApplReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1433"}


,
"ModelType" => #{"TagNum" => "1434" ,"Type" => "INT" ,"ValidValues" =>[{"0", "UTILITY_PROVIDED_STANDARD_MODEL"},{"1", "PROPRIETARY"}]}
, "1434" => #{"Name"=>"ModelType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "UTILITY_PROVIDED_STANDARD_MODEL"},{"1", "PROPRIETARY"}], "TagNum" => "1434"}


,
"ContractMultiplierUnit" => #{"TagNum" => "1435" ,"Type" => "INT" ,"ValidValues" =>[{"0", "SHARES"},{"1", "HOURS"},{"2", "DAYS"}]}
, "1435" => #{"Name"=>"ContractMultiplierUnit" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "SHARES"},{"1", "HOURS"},{"2", "DAYS"}], "TagNum" => "1435"}


,
"LegContractMultiplierUnit" => #{"TagNum" => "1436" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1436" => #{"Name"=>"LegContractMultiplierUnit" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1436"}


,
"UnderlyingContractMultiplierUnit" => #{"TagNum" => "1437" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1437" => #{"Name"=>"UnderlyingContractMultiplierUnit" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1437"}


,
"DerivativeContractMultiplierUnit" => #{"TagNum" => "1438" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1438" => #{"Name"=>"DerivativeContractMultiplierUnit" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1438"}


,
"FlowScheduleType" => #{"TagNum" => "1439" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NERC_EASTERN_OFF_PEAK"},{"1", "NERC_WESTERN_OFF_PEAK"},{"2", "NERC_CALENDAR_ALL_DAYS_IN_MONTH"},{"3", "NERC_EASTERN_PEAK"},{"4", "NERC_WESTERN_PEAK"}]}
, "1439" => #{"Name"=>"FlowScheduleType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NERC_EASTERN_OFF_PEAK"},{"1", "NERC_WESTERN_OFF_PEAK"},{"2", "NERC_CALENDAR_ALL_DAYS_IN_MONTH"},{"3", "NERC_EASTERN_PEAK"},{"4", "NERC_WESTERN_PEAK"}], "TagNum" => "1439"}


,
"LegFlowScheduleType" => #{"TagNum" => "1440" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1440" => #{"Name"=>"LegFlowScheduleType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1440"}


,
"UnderlyingFlowScheduleType" => #{"TagNum" => "1441" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1441" => #{"Name"=>"UnderlyingFlowScheduleType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1441"}


,
"DerivativeFlowScheduleType" => #{"TagNum" => "1442" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1442" => #{"Name"=>"DerivativeFlowScheduleType" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1442"}


,
"FillLiquidityInd" => #{"TagNum" => "1443" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1443" => #{"Name"=>"FillLiquidityInd" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1443"}


,
"SideLiquidityInd" => #{"TagNum" => "1444" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1444" => #{"Name"=>"SideLiquidityInd" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1444"}


,
"NoRateSources" => #{"TagNum" => "1445" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1445" => #{"Name"=>"NoRateSources" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1445"}


,
"RateSource" => #{"TagNum" => "1446" ,"Type" => "INT" ,"ValidValues" =>[{"0", "BLOOMBERG"},{"1", "REUTERS"},{"2", "TELERATE"},{"99", "OTHER"}]}
, "1446" => #{"Name"=>"RateSource" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "BLOOMBERG"},{"1", "REUTERS"},{"2", "TELERATE"},{"99", "OTHER"}], "TagNum" => "1446"}


,
"RateSourceType" => #{"TagNum" => "1447" ,"Type" => "INT" ,"ValidValues" =>[{"0", "PRIMARY"},{"1", "SECONDARY"}]}
, "1447" => #{"Name"=>"RateSourceType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "PRIMARY"},{"1", "SECONDARY"}], "TagNum" => "1447"}


,
"ReferencePage" => #{"TagNum" => "1448" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1448" => #{"Name"=>"ReferencePage" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1448"}


,
"RestructuringType" => #{"TagNum" => "1449" ,"Type" => "STRING" ,"ValidValues" =>[{"FR", "FULL_RESTRUCTURING"},{"MR", "MODIFIED_RESTRUCTURING"},{"MM", "MODIFIED_MOD_RESTRUCTURING"},{"XR", "NO_RESTRUCTURING_SPECIFIED"}]}
, "1449" => #{"Name"=>"RestructuringType" ,"Type"=>"STRING" ,"ValidValues"=>[{"FR", "FULL_RESTRUCTURING"},{"MR", "MODIFIED_RESTRUCTURING"},{"MM", "MODIFIED_MOD_RESTRUCTURING"},{"XR", "NO_RESTRUCTURING_SPECIFIED"}], "TagNum" => "1449"}


,
"Seniority" => #{"TagNum" => "1450" ,"Type" => "STRING" ,"ValidValues" =>[{"SD", "SENIOR_SECURED"},{"SR", "SENIOR"},{"SB", "SUBORDINATED"}]}
, "1450" => #{"Name"=>"Seniority" ,"Type"=>"STRING" ,"ValidValues"=>[{"SD", "SENIOR_SECURED"},{"SR", "SENIOR"},{"SB", "SUBORDINATED"}], "TagNum" => "1450"}


,
"NotionalPercentageOutstanding" => #{"TagNum" => "1451" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "1451" => #{"Name"=>"NotionalPercentageOutstanding" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "1451"}


,
"OriginalNotionalPercentageOutstanding" => #{"TagNum" => "1452" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "1452" => #{"Name"=>"OriginalNotionalPercentageOutstanding" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "1452"}


,
"UnderlyingRestructuringType" => #{"TagNum" => "1453" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1453" => #{"Name"=>"UnderlyingRestructuringType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1453"}


,
"UnderlyingSeniority" => #{"TagNum" => "1454" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1454" => #{"Name"=>"UnderlyingSeniority" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1454"}


,
"UnderlyingNotionalPercentageOutstanding" => #{"TagNum" => "1455" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "1455" => #{"Name"=>"UnderlyingNotionalPercentageOutstanding" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "1455"}


,
"UnderlyingOriginalNotionalPercentageOutstanding" => #{"TagNum" => "1456" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "1456" => #{"Name"=>"UnderlyingOriginalNotionalPercentageOutstanding" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "1456"}


,
"AttachmentPoint" => #{"TagNum" => "1457" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "1457" => #{"Name"=>"AttachmentPoint" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "1457"}


,
"DetachmentPoint" => #{"TagNum" => "1458" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "1458" => #{"Name"=>"DetachmentPoint" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "1458"}


,
"UnderlyingAttachmentPoint" => #{"TagNum" => "1459" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "1459" => #{"Name"=>"UnderlyingAttachmentPoint" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "1459"}


,
"UnderlyingDetachmentPoint" => #{"TagNum" => "1460" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "1460" => #{"Name"=>"UnderlyingDetachmentPoint" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "1460"}


,
"NoTargetPartyIDs" => #{"TagNum" => "1461" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1461" => #{"Name"=>"NoTargetPartyIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1461"}


,
"TargetPartyID" => #{"TagNum" => "1462" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1462" => #{"Name"=>"TargetPartyID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1462"}


,
"TargetPartyIDSource" => #{"TagNum" => "1463" ,"Type" => "CHAR" ,"ValidValues" =>[]}
, "1463" => #{"Name"=>"TargetPartyIDSource" ,"Type"=>"CHAR" ,"ValidValues"=>[], "TagNum" => "1463"}


,
"TargetPartyRole" => #{"TagNum" => "1464" ,"Type" => "INT" ,"ValidValues" =>[]}
, "1464" => #{"Name"=>"TargetPartyRole" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "1464"}


,
"SecurityListID" => #{"TagNum" => "1465" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1465" => #{"Name"=>"SecurityListID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1465"}


,
"SecurityListRefID" => #{"TagNum" => "1466" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1466" => #{"Name"=>"SecurityListRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1466"}


,
"SecurityListDesc" => #{"TagNum" => "1467" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1467" => #{"Name"=>"SecurityListDesc" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1467"}


,
"EncodedSecurityListDescLen" => #{"TagNum" => "1468" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "1468" => #{"Name"=>"EncodedSecurityListDescLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "1468"}


,
"EncodedSecurityListDesc" => #{"TagNum" => "1469" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "1469" => #{"Name"=>"EncodedSecurityListDesc" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "1469"}


,
"SecurityListType" => #{"TagNum" => "1470" ,"Type" => "INT" ,"ValidValues" =>[{"1", "INDUSTRY_CLASSIFICATION"},{"2", "TRADING_LIST"},{"3", "MARKET"},{"4", "NEWSPAPER_LIST"}]}
, "1470" => #{"Name"=>"SecurityListType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "INDUSTRY_CLASSIFICATION"},{"2", "TRADING_LIST"},{"3", "MARKET"},{"4", "NEWSPAPER_LIST"}], "TagNum" => "1470"}


,
"SecurityListTypeSource" => #{"TagNum" => "1471" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ICB"},{"2", "NAICS"},{"3", "GICS"}]}
, "1471" => #{"Name"=>"SecurityListTypeSource" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ICB"},{"2", "NAICS"},{"3", "GICS"}], "TagNum" => "1471"}


,
"NewsID" => #{"TagNum" => "1472" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1472" => #{"Name"=>"NewsID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1472"}


,
"NewsCategory" => #{"TagNum" => "1473" ,"Type" => "INT" ,"ValidValues" =>[{"0", "COMPANY_NEWS"},{"1", "MARKETPLACE_NEWS"},{"2", "FINANCIAL_MARKET_NEWS"},{"3", "TECHNICAL_NEWS"},{"99", "OTHER_NEWS"}]}
, "1473" => #{"Name"=>"NewsCategory" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "COMPANY_NEWS"},{"1", "MARKETPLACE_NEWS"},{"2", "FINANCIAL_MARKET_NEWS"},{"3", "TECHNICAL_NEWS"},{"99", "OTHER_NEWS"}], "TagNum" => "1473"}


,
"LanguageCode" => #{"TagNum" => "1474" ,"Type" => "LANGUAGE" ,"ValidValues" =>[]}
, "1474" => #{"Name"=>"LanguageCode" ,"Type"=>"LANGUAGE" ,"ValidValues"=>[], "TagNum" => "1474"}


,
"NoNewsRefIDs" => #{"TagNum" => "1475" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1475" => #{"Name"=>"NoNewsRefIDs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1475"}


,
"NewsRefID" => #{"TagNum" => "1476" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1476" => #{"Name"=>"NewsRefID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1476"}


,
"NewsRefType" => #{"TagNum" => "1477" ,"Type" => "INT" ,"ValidValues" =>[{"0", "REPLACEMENT"},{"1", "OTHER_LANGUAGE"},{"2", "COMPLIMENTARY"}]}
, "1477" => #{"Name"=>"NewsRefType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "REPLACEMENT"},{"1", "OTHER_LANGUAGE"},{"2", "COMPLIMENTARY"}], "TagNum" => "1477"}


,
"StrikePriceDeterminationMethod" => #{"TagNum" => "1478" ,"Type" => "INT" ,"ValidValues" =>[{"1", "FIXED_STRIKE"},{"2", "STRIKE_SET_AT_EXPIRATION_TO_UNDERLYING_OR_OTHER_VALUE"},{"3", "STRIKE_SET_TO_AVERAGE_OF_UNDERLYING_SETTLEMENT_PRICE_ACROSS_THE_LIFE_OF_THE_OPTION"},{"4", "STRIKE_SET_TO_OPTIMAL_VALUE"}]}
, "1478" => #{"Name"=>"StrikePriceDeterminationMethod" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "FIXED_STRIKE"},{"2", "STRIKE_SET_AT_EXPIRATION_TO_UNDERLYING_OR_OTHER_VALUE"},{"3", "STRIKE_SET_TO_AVERAGE_OF_UNDERLYING_SETTLEMENT_PRICE_ACROSS_THE_LIFE_OF_THE_OPTION"},{"4", "STRIKE_SET_TO_OPTIMAL_VALUE"}], "TagNum" => "1478"}


,
"StrikePriceBoundaryMethod" => #{"TagNum" => "1479" ,"Type" => "INT" ,"ValidValues" =>[{"1", "LESS_THAN_UNDERLYING_PRICE_IS_IN_THE_MONEY"},{"2", "LESS_THAN_OR_EQUAL_TO_THE_UNDERLYING_PRICE_IS_IN_THE_MONEY"},{"3", "EQUAL_TO_THE_UNDERLYING_PRICE_IS_IN_THE_MONEY"},{"4", "GREATER_THAN_OR_EQUAL_TO_UNDERLYING_PRICE_IS_IN_THE_MONEY"},{"5", "GREATER_THAN_UNDERLYING_IS_IN_THE_MONEY"}]}
, "1479" => #{"Name"=>"StrikePriceBoundaryMethod" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "LESS_THAN_UNDERLYING_PRICE_IS_IN_THE_MONEY"},{"2", "LESS_THAN_OR_EQUAL_TO_THE_UNDERLYING_PRICE_IS_IN_THE_MONEY"},{"3", "EQUAL_TO_THE_UNDERLYING_PRICE_IS_IN_THE_MONEY"},{"4", "GREATER_THAN_OR_EQUAL_TO_UNDERLYING_PRICE_IS_IN_THE_MONEY"},{"5", "GREATER_THAN_UNDERLYING_IS_IN_THE_MONEY"}], "TagNum" => "1479"}


,
"StrikePriceBoundaryPrecision" => #{"TagNum" => "1480" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "1480" => #{"Name"=>"StrikePriceBoundaryPrecision" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "1480"}


,
"UnderlyingPriceDeterminationMethod" => #{"TagNum" => "1481" ,"Type" => "INT" ,"ValidValues" =>[{"1", "REGULAR"},{"2", "SPECIAL_REFERENCE"},{"3", "OPTIMAL_VALUE"},{"4", "AVERAGE_VALUE"}]}
, "1481" => #{"Name"=>"UnderlyingPriceDeterminationMethod" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "REGULAR"},{"2", "SPECIAL_REFERENCE"},{"3", "OPTIMAL_VALUE"},{"4", "AVERAGE_VALUE"}], "TagNum" => "1481"}


,
"OptPayoutType" => #{"TagNum" => "1482" ,"Type" => "INT" ,"ValidValues" =>[{"1", "VANILLA"},{"2", "CAPPED"},{"3", "BINARY"}]}
, "1482" => #{"Name"=>"OptPayoutType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "VANILLA"},{"2", "CAPPED"},{"3", "BINARY"}], "TagNum" => "1482"}


,
"NoComplexEvents" => #{"TagNum" => "1483" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1483" => #{"Name"=>"NoComplexEvents" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1483"}


,
"ComplexEventType" => #{"TagNum" => "1484" ,"Type" => "INT" ,"ValidValues" =>[{"1", "CAPPED"},{"2", "TRIGGER"},{"3", "KNOCK_IN_UP"},{"4", "KOCK_IN_DOWN"},{"5", "KNOCK_OUT_UP"},{"6", "KNOCK_OUT_DOWN"},{"7", "UNDERLYING"},{"8", "RESET_BARRIER"},{"9", "ROLLING_BARRIER"}]}
, "1484" => #{"Name"=>"ComplexEventType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "CAPPED"},{"2", "TRIGGER"},{"3", "KNOCK_IN_UP"},{"4", "KOCK_IN_DOWN"},{"5", "KNOCK_OUT_UP"},{"6", "KNOCK_OUT_DOWN"},{"7", "UNDERLYING"},{"8", "RESET_BARRIER"},{"9", "ROLLING_BARRIER"}], "TagNum" => "1484"}


,
"ComplexOptPayoutAmount" => #{"TagNum" => "1485" ,"Type" => "AMT" ,"ValidValues" =>[]}
, "1485" => #{"Name"=>"ComplexOptPayoutAmount" ,"Type"=>"AMT" ,"ValidValues"=>[], "TagNum" => "1485"}


,
"ComplexEventPrice" => #{"TagNum" => "1486" ,"Type" => "PRICE" ,"ValidValues" =>[]}
, "1486" => #{"Name"=>"ComplexEventPrice" ,"Type"=>"PRICE" ,"ValidValues"=>[], "TagNum" => "1486"}


,
"ComplexEventPriceBoundaryMethod" => #{"TagNum" => "1487" ,"Type" => "INT" ,"ValidValues" =>[{"1", "LESS_THAN_COMPLEXEVENTPRICE"},{"2", "LESS_THAN_OR_EQUAL_TO_COMPLEXEVENTPRICE"},{"3", "EQUAL_TO_COMPLEXEVENTPRICE"},{"4", "GREATER_THAN_OR_EQUAL_TO_COMPLEXEVENTPRICE"},{"5", "GREATER_THAN_COMPLEXEVENTPRICE"}]}
, "1487" => #{"Name"=>"ComplexEventPriceBoundaryMethod" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "LESS_THAN_COMPLEXEVENTPRICE"},{"2", "LESS_THAN_OR_EQUAL_TO_COMPLEXEVENTPRICE"},{"3", "EQUAL_TO_COMPLEXEVENTPRICE"},{"4", "GREATER_THAN_OR_EQUAL_TO_COMPLEXEVENTPRICE"},{"5", "GREATER_THAN_COMPLEXEVENTPRICE"}], "TagNum" => "1487"}


,
"ComplexEventPriceBoundaryPrecision" => #{"TagNum" => "1488" ,"Type" => "PERCENTAGE" ,"ValidValues" =>[]}
, "1488" => #{"Name"=>"ComplexEventPriceBoundaryPrecision" ,"Type"=>"PERCENTAGE" ,"ValidValues"=>[], "TagNum" => "1488"}


,
"ComplexEventPriceTimeType" => #{"TagNum" => "1489" ,"Type" => "INT" ,"ValidValues" =>[{"1", "EXPIRATION"},{"2", "IMMEDIATE"},{"3", "SPECIFIED_DATE_TIME"}]}
, "1489" => #{"Name"=>"ComplexEventPriceTimeType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "EXPIRATION"},{"2", "IMMEDIATE"},{"3", "SPECIFIED_DATE_TIME"}], "TagNum" => "1489"}


,
"ComplexEventCondition" => #{"TagNum" => "1490" ,"Type" => "INT" ,"ValidValues" =>[{"1", "AND"},{"2", "OR"}]}
, "1490" => #{"Name"=>"ComplexEventCondition" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "AND"},{"2", "OR"}], "TagNum" => "1490"}


,
"NoComplexEventDates" => #{"TagNum" => "1491" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1491" => #{"Name"=>"NoComplexEventDates" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1491"}


,
"ComplexEventStartDate" => #{"TagNum" => "1492" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "1492" => #{"Name"=>"ComplexEventStartDate" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "1492"}


,
"ComplexEventEndDate" => #{"TagNum" => "1493" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "1493" => #{"Name"=>"ComplexEventEndDate" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "1493"}


,
"NoComplexEventTimes" => #{"TagNum" => "1494" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1494" => #{"Name"=>"NoComplexEventTimes" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1494"}


,
"ComplexEventStartTime" => #{"TagNum" => "1495" ,"Type" => "UTCTIMEONLY" ,"ValidValues" =>[]}
, "1495" => #{"Name"=>"ComplexEventStartTime" ,"Type"=>"UTCTIMEONLY" ,"ValidValues"=>[], "TagNum" => "1495"}


,
"ComplexEventEndTime" => #{"TagNum" => "1496" ,"Type" => "UTCTIMEONLY" ,"ValidValues" =>[]}
, "1496" => #{"Name"=>"ComplexEventEndTime" ,"Type"=>"UTCTIMEONLY" ,"ValidValues"=>[], "TagNum" => "1496"}


,
"StreamAsgnReqID" => #{"TagNum" => "1497" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1497" => #{"Name"=>"StreamAsgnReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1497"}


,
"StreamAsgnReqType" => #{"TagNum" => "1498" ,"Type" => "INT" ,"ValidValues" =>[{"1", "STREAM_ASSIGNMENT_FOR_NEW_CUSTOMER"},{"2", "STREAM_ASSIGNMENT_FOR_EXISTING_CUSTOMER"}]}
, "1498" => #{"Name"=>"StreamAsgnReqType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "STREAM_ASSIGNMENT_FOR_NEW_CUSTOMER"},{"2", "STREAM_ASSIGNMENT_FOR_EXISTING_CUSTOMER"}], "TagNum" => "1498"}


,
"NoAsgnReqs" => #{"TagNum" => "1499" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "1499" => #{"Name"=>"NoAsgnReqs" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "1499"}


,
"MDStreamID" => #{"TagNum" => "1500" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1500" => #{"Name"=>"MDStreamID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1500"}


,
"StreamAsgnRptID" => #{"TagNum" => "1501" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1501" => #{"Name"=>"StreamAsgnRptID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1501"}


,
"StreamAsgnRejReason" => #{"TagNum" => "1502" ,"Type" => "INT" ,"ValidValues" =>[{"0", "UNKNOWN_CLIENT"},{"1", "EXCEEDS_MAXIMUM_SIZE"},{"2", "UNKNOWN_OR_INVALID_CURRENCY_PAIR"},{"3", "NO_AVAILABLE_STREAM"},{"99", "OTHER"}]}
, "1502" => #{"Name"=>"StreamAsgnRejReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "UNKNOWN_CLIENT"},{"1", "EXCEEDS_MAXIMUM_SIZE"},{"2", "UNKNOWN_OR_INVALID_CURRENCY_PAIR"},{"3", "NO_AVAILABLE_STREAM"},{"99", "OTHER"}], "TagNum" => "1502"}


,
"StreamAsgnAckType" => #{"TagNum" => "1503" ,"Type" => "INT" ,"ValidValues" =>[{"0", "ASSIGNMENT_ACCEPTED"},{"1", "ASSIGNMENT_REJECTED"}]}
, "1503" => #{"Name"=>"StreamAsgnAckType" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "ASSIGNMENT_ACCEPTED"},{"1", "ASSIGNMENT_REJECTED"}], "TagNum" => "1503"}


,
"RelSymTransactTime" => #{"TagNum" => "1504" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "1504" => #{"Name"=>"RelSymTransactTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "1504"}


,
"StreamAsgnType" => #{"TagNum" => "1617" ,"Type" => "INT" ,"ValidValues" =>[{"1", "ASSIGNMENT"},{"2", "REJECTED"},{"3", "TERMINATE_UNASSIGN"}]}
, "1617" => #{"Name"=>"StreamAsgnType" ,"Type"=>"INT" ,"ValidValues"=>[{"1", "ASSIGNMENT"},{"2", "REJECTED"},{"3", "TERMINATE_UNASSIGN"}], "TagNum" => "1617"}


}.



components() ->
 #{"DerivativeSecurityXML" => #{
                              "Fields" => #{"DerivativeSecurityXMLLen" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeSecurityXML" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeSecurityXMLSchema" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"InstrumentLeg" => #{
                              "Fields" => #{"LegSymbol" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecurityIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"LegProduct" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCFICode" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecuritySubType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegMaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"LegMaturityDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegMaturityTime" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCouponPaymentDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegIssueDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRepoCollateralSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRepurchaseTerm" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRepurchaseRate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegFactor" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCreditRating" =>#{"Required" => "N", "Sequence" => undefined}
,"LegInstrRegistry" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCountryOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStateOrProvinceOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"LegLocaleOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRedemptionDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStrikeCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"LegContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"LegUnitOfMeasure" =>#{"Required" => "N", "Sequence" => undefined}
,"LegUnitOfMeasureQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPriceUnitOfMeasure" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPriceUnitOfMeasureQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegTimeUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"LegExerciseStyle" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"LegIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedLegIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedLegIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedLegSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedLegSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRatioQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSide" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPool" =>#{"Required" => "N", "Sequence" => undefined}
,"LegDatedDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegContractSettlMonth" =>#{"Required" => "N", "Sequence" => undefined}
,"LegInterestAccrualDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOptionRatio" =>#{"Required" => "N", "Sequence" => undefined}
,"LegContractMultiplierUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"LegFlowScheduleType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"LegSecAltIDGrp" =>#{"Required" => "N", "Sequence" => undefined}
}}
,"InstrmtLegExecGrp" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegLastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"LegLastForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCalculatedCcyLastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegGrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"LegVolatility" =>#{"Required" => "N", "Sequence" => undefined}
,"LegDividendYield" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCurrencyRatio" =>#{"Required" => "N", "Sequence" => undefined}
,"LegExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"LegLastQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegLastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"LegLastForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCalculatedCcyLastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegGrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"LegVolatility" =>#{"Required" => "N", "Sequence" => undefined}
,"LegDividendYield" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCurrencyRatio" =>#{"Required" => "N", "Sequence" => undefined}
,"LegExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"LegLastQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPreAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedParties3" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"TrdCapRptSideGrp" => #{
                              "Fields" => #{"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"SideLastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"SideTradeReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"SideFillStationCd" =>#{"Required" => "N", "Sequence" => undefined}
,"SideReasonCd" =>#{"Required" => "N", "Sequence" => undefined}
,"RptSeq" =>#{"Required" => "N", "Sequence" => undefined}
,"SideTrdSubTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"NetGrossInd" =>#{"Required" => "N", "Sequence" => undefined}
,"SideCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SideSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"OddLot" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputSource" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputDevice" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeBracket" =>#{"Required" => "N", "Sequence" => undefined}
,"NumDaysInterest" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"InterestAtMaturity" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"Concession" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalTakedown" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"SideMultiLegReportingType" =>#{"Required" => "N", "Sequence" => undefined}
,"ExchangeRule" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeAllocIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"SideGrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"AggressorIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"ExchangeSpecialInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCategory" =>#{"Required" => "N", "Sequence" => undefined}
,"SideExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderDelay" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderDelayUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"SideLiquidityInd" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSides" => #{
                              "Fields" => #{"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"SideLastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"SideTradeReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"SideFillStationCd" =>#{"Required" => "N", "Sequence" => undefined}
,"SideReasonCd" =>#{"Required" => "N", "Sequence" => undefined}
,"RptSeq" =>#{"Required" => "N", "Sequence" => undefined}
,"SideTrdSubTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"NetGrossInd" =>#{"Required" => "N", "Sequence" => undefined}
,"SideCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SideSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"OddLot" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputSource" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputDevice" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeBracket" =>#{"Required" => "N", "Sequence" => undefined}
,"NumDaysInterest" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"InterestAtMaturity" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"Concession" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalTakedown" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"SideMultiLegReportingType" =>#{"Required" => "N", "Sequence" => undefined}
,"ExchangeRule" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeAllocIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"SideGrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"AggressorIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"ExchangeSpecialInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCategory" =>#{"Required" => "N", "Sequence" => undefined}
,"SideExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderDelay" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderDelayUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"SideLiquidityInd" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"ClrInstGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SideTrdRegTS" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeReportOrderDetail" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"PositionAmountData" => #{
                              "Fields" => #{"PosAmtType" =>#{"Required" => "N", "Sequence" => undefined}
,"PosAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionCurrency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoPosAmt" => #{
                              "Fields" => #{"PosAmtType" =>#{"Required" => "N", "Sequence" => undefined}
,"PosAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionCurrency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"ApplicationSequenceControl" => #{
                              "Fields" => #{"ApplID" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplSeqNum" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplLastSeqNum" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplResendFlag" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"Stipulations" => #{
                              "Fields" => #{"StipulationType" =>#{"Required" => "N", "Sequence" => undefined}
,"StipulationValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoStipulations" => #{
                              "Fields" => #{"StipulationType" =>#{"Required" => "N", "Sequence" => undefined}
,"StipulationValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"MDReqGrp" => #{
                              "Fields" => #{"MDEntryType" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMDEntryTypes" => #{
                              "Fields" => #{"MDEntryType" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"TrdCapRptAckSideGrp" => #{
                              "Fields" => #{"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"OddLot" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputSource" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputDevice" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeBracket" =>#{"Required" => "N", "Sequence" => undefined}
,"NetGrossInd" =>#{"Required" => "N", "Sequence" => undefined}
,"SideCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SideSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"NumDaysInterest" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"InterestAtMaturity" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"Concession" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalTakedown" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"SideMultiLegReportingType" =>#{"Required" => "N", "Sequence" => undefined}
,"ExchangeRule" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeAllocIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"SideGrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"AggressorIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"SideLastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"SideTradeReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"SideFillStationCd" =>#{"Required" => "N", "Sequence" => undefined}
,"SideReasonCd" =>#{"Required" => "N", "Sequence" => undefined}
,"RptSeq" =>#{"Required" => "N", "Sequence" => undefined}
,"SideTrdSubTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"SideExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderDelay" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderDelayUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCategory" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSides" => #{
                              "Fields" => #{"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"OddLot" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputSource" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeInputDevice" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeBracket" =>#{"Required" => "N", "Sequence" => undefined}
,"NetGrossInd" =>#{"Required" => "N", "Sequence" => undefined}
,"SideCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SideSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"NumDaysInterest" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestRate" =>#{"Required" => "N", "Sequence" => undefined}
,"AccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"InterestAtMaturity" =>#{"Required" => "N", "Sequence" => undefined}
,"EndAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"StartCash" =>#{"Required" => "N", "Sequence" => undefined}
,"EndCash" =>#{"Required" => "N", "Sequence" => undefined}
,"Concession" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalTakedown" =>#{"Required" => "N", "Sequence" => undefined}
,"NetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"SideMultiLegReportingType" =>#{"Required" => "N", "Sequence" => undefined}
,"ExchangeRule" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeAllocIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"SideGrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"AggressorIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"SideLastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"SideTradeReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"SideFillStationCd" =>#{"Required" => "N", "Sequence" => undefined}
,"SideReasonCd" =>#{"Required" => "N", "Sequence" => undefined}
,"RptSeq" =>#{"Required" => "N", "Sequence" => undefined}
,"SideTrdSubTyp" =>#{"Required" => "N", "Sequence" => undefined}
,"SideExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderDelay" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderDelayUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCategory" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"ClrInstGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SideTrdRegTS" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeReportOrderDetail" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"SettlInstGrp" => #{
                              "Fields" => #{"SettlInstID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstTransType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Product" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"CFICode" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"LastUpdateTime" =>#{"Required" => "N", "Sequence" => undefined}
,"PaymentMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"PaymentRef" =>#{"Required" => "N", "Sequence" => undefined}
,"CardHolderName" =>#{"Required" => "N", "Sequence" => undefined}
,"CardNumber" =>#{"Required" => "N", "Sequence" => undefined}
,"CardStartDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CardExpDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CardIssNum" =>#{"Required" => "N", "Sequence" => undefined}
,"PaymentDate" =>#{"Required" => "N", "Sequence" => undefined}
,"PaymentRemitterID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSettlInst" => #{
                              "Fields" => #{"SettlInstID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstTransType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Product" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"CFICode" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"LastUpdateTime" =>#{"Required" => "N", "Sequence" => undefined}
,"PaymentMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"PaymentRef" =>#{"Required" => "N", "Sequence" => undefined}
,"CardHolderName" =>#{"Required" => "N", "Sequence" => undefined}
,"CardNumber" =>#{"Required" => "N", "Sequence" => undefined}
,"CardStartDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CardExpDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CardIssNum" =>#{"Required" => "N", "Sequence" => undefined}
,"PaymentDate" =>#{"Required" => "N", "Sequence" => undefined}
,"PaymentRemitterID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstructionsData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"SecLstUpdRelSymGrp" => #{
                              "Fields" => #{"ListUpdateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"RelSymTransactTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"ListUpdateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"RelSymTransactTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityTradingRules" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikeRules" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"SecLstUpdRelSymsLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"SecAltIDGrp" => #{
                              "Fields" => #{"SecurityAltID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityAltIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSecurityAltID" => #{
                              "Fields" => #{"SecurityAltID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityAltIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"DiscretionInstructions" => #{
                              "Fields" => #{"DiscretionInst" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionOffsetValue" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionMoveType" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionOffsetType" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionLimitType" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionRoundDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionScope" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"MatchRules" => #{
                              "Fields" => #{"MatchAlgorithm" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMatchRules" => #{
                              "Fields" => #{"MatchAlgorithm" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"LotTypeRules" => #{
                              "Fields" => #{"LotType" =>#{"Required" => "N", "Sequence" => undefined}
,"MinLotSize" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLotTypeRules" => #{
                              "Fields" => #{"LotType" =>#{"Required" => "N", "Sequence" => undefined}
,"MinLotSize" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"BidCompReqGrp" => #{
                              "Fields" => #{"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"NetGrossInd" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoBidComponents" => #{
                              "Fields" => #{"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"NetGrossInd" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"TargetParties" => #{
                              "Fields" => #{"TargetPartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetPartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetPartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoTargetPartyIDs" => #{
                              "Fields" => #{"TargetPartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetPartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetPartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"TimeInForceRules" => #{
                              "Fields" => #{"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoTimeInForceRules" => #{
                              "Fields" => #{"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"TrdSessLstGrp" => #{
                              "Fields" => #{"TradingSessionID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesMode" =>#{"Required" => "N", "Sequence" => undefined}
,"UnsolicitedIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradSesStatusRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesStartTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesOpenTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesPreCloseTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesCloseTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesEndTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalVolumeTraded" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesUpdateAction" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoTradingSessions" => #{
                              "Fields" => #{"TradingSessionID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesMode" =>#{"Required" => "N", "Sequence" => undefined}
,"UnsolicitedIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"TradSesStatusRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesStartTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesOpenTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesPreCloseTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesCloseTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesEndTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TotalVolumeTraded" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradSesUpdateAction" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"TradingSessionRules" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"YieldData" => #{
                              "Fields" => #{"YieldType" =>#{"Required" => "N", "Sequence" => undefined}
,"Yield" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldCalcDate" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldRedemptionDate" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldRedemptionPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldRedemptionPriceType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"ApplIDRequestAckGrp" => #{
                              "Fields" => #{"RefApplID" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplBegSeqNum" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplEndSeqNum" =>#{"Required" => "N", "Sequence" => undefined}
,"RefApplLastSeqNum" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplResponseError" =>#{"Required" => "N", "Sequence" => undefined}
,"RefApplReqID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoApplIDs" => #{
                              "Fields" => #{"RefApplID" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplBegSeqNum" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplEndSeqNum" =>#{"Required" => "N", "Sequence" => undefined}
,"RefApplLastSeqNum" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplResponseError" =>#{"Required" => "N", "Sequence" => undefined}
,"RefApplReqID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"UsernameGrp" => #{
                              "Fields" => #{"Username" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"FinancingDetails" => #{
                              "Fields" => #{"AgreementDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"AgreementID" =>#{"Required" => "N", "Sequence" => undefined}
,"AgreementDate" =>#{"Required" => "N", "Sequence" => undefined}
,"AgreementCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"TerminationType" =>#{"Required" => "N", "Sequence" => undefined}
,"StartDate" =>#{"Required" => "N", "Sequence" => undefined}
,"EndDate" =>#{"Required" => "N", "Sequence" => undefined}
,"DeliveryType" =>#{"Required" => "N", "Sequence" => undefined}
,"MarginRatio" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"SettlParties" => #{
                              "Fields" => #{"SettlPartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlPartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlPartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSettlPartyIDs" => #{
                              "Fields" => #{"SettlPartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlPartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlPartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"SettlPtysSubGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"LegPreAllocGrp" => #{
                              "Fields" => #{"LegAllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"LegIndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegAllocQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegAllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"LegAllocSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegAllocs" => #{
                              "Fields" => #{"LegAllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"LegIndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegAllocQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegAllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"LegAllocSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties2" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"PreAllocMlegGrp" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties3" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"InstrmtLegIOIGrp" => #{
                              "Fields" => #{"LegIOIQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{"LegIOIQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStipulations" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"BaseTradingRules" => #{
                              "Fields" => #{"ExpirationCycle" =>#{"Required" => "N", "Sequence" => undefined}
,"MinTradeVol" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxTradeVol" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxPriceVariation" =>#{"Required" => "N", "Sequence" => undefined}
,"ImpliedMarketIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"RoundLot" =>#{"Required" => "N", "Sequence" => undefined}
,"MultilegModel" =>#{"Required" => "N", "Sequence" => undefined}
,"MultilegPriceMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"TickRules" =>#{"Required" => "N", "Sequence" => undefined}
,"LotTypeRules" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceLimits" =>#{"Required" => "N", "Sequence" => undefined}
}}
,"InstrumentExtension" => #{
                              "Fields" => #{"DeliveryForm" =>#{"Required" => "N", "Sequence" => undefined}
,"PctAtRisk" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"AttrbGrp" =>#{"Required" => "N", "Sequence" => undefined}
}}
,"LinesOfTextGrp" => #{
                              "Fields" => #{"Text" =>#{"Required" => "Y", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLinesOfText" => #{
                              "Fields" => #{"Text" =>#{"Required" => "Y", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"BidDescReqGrp" => #{
                              "Fields" => #{"BidDescriptorType" =>#{"Required" => "N", "Sequence" => undefined}
,"BidDescriptor" =>#{"Required" => "N", "Sequence" => undefined}
,"SideValueInd" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityValue" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityNumSecurities" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityPctLow" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityPctHigh" =>#{"Required" => "N", "Sequence" => undefined}
,"EFPTrackingError" =>#{"Required" => "N", "Sequence" => undefined}
,"FairValue" =>#{"Required" => "N", "Sequence" => undefined}
,"OutsideIndexPct" =>#{"Required" => "N", "Sequence" => undefined}
,"ValueOfFutures" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoBidDescriptors" => #{
                              "Fields" => #{"BidDescriptorType" =>#{"Required" => "N", "Sequence" => undefined}
,"BidDescriptor" =>#{"Required" => "N", "Sequence" => undefined}
,"SideValueInd" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityValue" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityNumSecurities" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityPctLow" =>#{"Required" => "N", "Sequence" => undefined}
,"LiquidityPctHigh" =>#{"Required" => "N", "Sequence" => undefined}
,"EFPTrackingError" =>#{"Required" => "N", "Sequence" => undefined}
,"FairValue" =>#{"Required" => "N", "Sequence" => undefined}
,"OutsideIndexPct" =>#{"Required" => "N", "Sequence" => undefined}
,"ValueOfFutures" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"MaturityRules" => #{
                              "Fields" => #{"MaturityRuleID" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYearFormat" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYearIncrementUnits" =>#{"Required" => "N", "Sequence" => undefined}
,"StartMaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"EndMaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYearIncrement" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMaturityRules" => #{
                              "Fields" => #{"MaturityRuleID" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYearFormat" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYearIncrementUnits" =>#{"Required" => "N", "Sequence" => undefined}
,"StartMaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"EndMaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYearIncrement" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"StrikeRules" => #{
                              "Fields" => #{"StrikeRuleID" =>#{"Required" => "N", "Sequence" => undefined}
,"StartStrikePxRange" =>#{"Required" => "N", "Sequence" => undefined}
,"EndStrikePxRange" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikeIncrement" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikeExerciseStyle" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoStrikeRules" => #{
                              "Fields" => #{"StrikeRuleID" =>#{"Required" => "N", "Sequence" => undefined}
,"StartStrikePxRange" =>#{"Required" => "N", "Sequence" => undefined}
,"EndStrikePxRange" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikeIncrement" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikeExerciseStyle" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"MaturityRules" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"QuotQualGrp" => #{
                              "Fields" => #{"QuoteQualifier" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoQuoteQualifiers" => #{
                              "Fields" => #{"QuoteQualifier" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"PreAllocGrp" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"MDRjctGrp" => #{
                              "Fields" => #{"AltMDSourceID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAltMDSource" => #{
                              "Fields" => #{"AltMDSourceID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"Instrument" => #{
                              "Fields" => #{"Symbol" =>#{"Required" => "N", "Sequence" => undefined}
,"SymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"Product" =>#{"Required" => "N", "Sequence" => undefined}
,"ProductComplex" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityGroup" =>#{"Required" => "N", "Sequence" => undefined}
,"CFICode" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySubType" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MaturityTime" =>#{"Required" => "N", "Sequence" => undefined}
,"SettleOnOpenFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtAssignmentMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponPaymentDate" =>#{"Required" => "N", "Sequence" => undefined}
,"IssueDate" =>#{"Required" => "N", "Sequence" => undefined}
,"RepoCollateralSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"RepurchaseTerm" =>#{"Required" => "N", "Sequence" => undefined}
,"RepurchaseRate" =>#{"Required" => "N", "Sequence" => undefined}
,"Factor" =>#{"Required" => "N", "Sequence" => undefined}
,"CreditRating" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrRegistry" =>#{"Required" => "N", "Sequence" => undefined}
,"CountryOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"StateOrProvinceOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"LocaleOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"RedemptionDate" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikeCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikeMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikeValue" =>#{"Required" => "N", "Sequence" => undefined}
,"OptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"MinPriceIncrement" =>#{"Required" => "N", "Sequence" => undefined}
,"MinPriceIncrementAmount" =>#{"Required" => "N", "Sequence" => undefined}
,"UnitOfMeasure" =>#{"Required" => "N", "Sequence" => undefined}
,"UnitOfMeasureQty" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceUnitOfMeasure" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceUnitOfMeasureQty" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"ExerciseStyle" =>#{"Required" => "N", "Sequence" => undefined}
,"OptPayoutAmount" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceQuoteMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"ValuationMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"ListMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"CapPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"FloorPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"PutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"FlexibleIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"FlexProductEligibilityIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"CouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionLimit" =>#{"Required" => "N", "Sequence" => undefined}
,"NTPositionLimit" =>#{"Required" => "N", "Sequence" => undefined}
,"Issuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"Pool" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractSettlMonth" =>#{"Required" => "N", "Sequence" => undefined}
,"CPProgram" =>#{"Required" => "N", "Sequence" => undefined}
,"CPRegType" =>#{"Required" => "N", "Sequence" => undefined}
,"DatedDate" =>#{"Required" => "N", "Sequence" => undefined}
,"InterestAccrualDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ContractMultiplierUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"FlowScheduleType" =>#{"Required" => "N", "Sequence" => undefined}
,"RestructuringType" =>#{"Required" => "N", "Sequence" => undefined}
,"Seniority" =>#{"Required" => "N", "Sequence" => undefined}
,"NotionalPercentageOutstanding" =>#{"Required" => "N", "Sequence" => undefined}
,"OriginalNotionalPercentageOutstanding" =>#{"Required" => "N", "Sequence" => undefined}
,"AttachmentPoint" =>#{"Required" => "N", "Sequence" => undefined}
,"DetachmentPoint" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePriceDeterminationMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePriceBoundaryMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikePriceBoundaryPrecision" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingPriceDeterminationMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"OptPayoutType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"SecAltIDGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityXML" =>#{"Required" => "N", "Sequence" => undefined}
,"EvntGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrumentParties" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplexEvents" =>#{"Required" => "N", "Sequence" => undefined}
}}
,"TradeCapLegUnderlyingsGrp" => #{
                              "Fields" => #{}
                              ,"Groups" => #{
                              "NoOfLegUnderlyings" => #{
                              "Fields" => #{}
                              ,"Groups" => #{}
                              ,"Components" => #{"UnderlyingLegInstrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"HopGrp" => #{
                              "Fields" => #{"HopCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"HopSendingTime" =>#{"Required" => "N", "Sequence" => undefined}
,"HopRefID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoHops" => #{
                              "Fields" => #{"HopCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"HopSendingTime" =>#{"Required" => "N", "Sequence" => undefined}
,"HopRefID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"AllocAckGrp" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocPositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocRejCode" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryIndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocCustomerCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocType" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocPositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocRejCode" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryIndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocCustomerCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocType" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"QuotReqLegsGrp" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRefID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRefID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
,"LegBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"NestedParties2" => #{
                              "Fields" => #{"Nested2PartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested2PartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested2PartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoNested2PartyIDs" => #{
                              "Fields" => #{"Nested2PartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested2PartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested2PartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NstdPtys2SubGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"InstrmtMDReqGrp" => #{
                              "Fields" => #{"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySize" =>#{"Required" => "N", "Sequence" => undefined}
,"MDStreamID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySize" =>#{"Required" => "N", "Sequence" => undefined}
,"MDStreamID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"SideCrossOrdModGrp" => #{
                              "Fields" => #{"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"PreTradeAnonymity" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"SideComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SideTimeInForce" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSides" => #{
                              "Fields" => #{"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"PreTradeAnonymity" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"SideComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SideTimeInForce" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"PreAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"RootSubParties" => #{
                              "Fields" => #{"RootPartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"RootPartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRootPartySubIDs" => #{
                              "Fields" => #{"RootPartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"RootPartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"TrdCollGrp" => #{
                              "Fields" => #{"TradeReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTradeReportID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoTrades" => #{
                              "Fields" => #{"TradeReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTradeReportID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"NestedParties3" => #{
                              "Fields" => #{"Nested3PartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested3PartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested3PartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoNested3PartyIDs" => #{
                              "Fields" => #{"Nested3PartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested3PartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested3PartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NstdPtys3SubGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"StrategyParametersGrp" => #{
                              "Fields" => #{"StrategyParameterName" =>#{"Required" => "N", "Sequence" => undefined}
,"StrategyParameterType" =>#{"Required" => "N", "Sequence" => undefined}
,"StrategyParameterValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoStrategyParameters" => #{
                              "Fields" => #{"StrategyParameterName" =>#{"Required" => "N", "Sequence" => undefined}
,"StrategyParameterType" =>#{"Required" => "N", "Sequence" => undefined}
,"StrategyParameterValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"StrmAsgnRptInstrmtGrp" => #{
                              "Fields" => #{"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"StreamAsgnType" =>#{"Required" => "N", "Sequence" => undefined}
,"MDStreamID" =>#{"Required" => "N", "Sequence" => undefined}
,"StreamAsgnRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"StreamAsgnType" =>#{"Required" => "N", "Sequence" => undefined}
,"MDStreamID" =>#{"Required" => "N", "Sequence" => undefined}
,"StreamAsgnRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"StrmAsgnReqInstrmtGrp" => #{
                              "Fields" => #{"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySize" =>#{"Required" => "N", "Sequence" => undefined}
,"MDStreamID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySize" =>#{"Required" => "N", "Sequence" => undefined}
,"MDStreamID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"NestedParties4" => #{
                              "Fields" => #{"Nested4PartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested4PartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested4PartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoNested4PartyIDs" => #{
                              "Fields" => #{"Nested4PartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested4PartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested4PartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NstdPtys4SubGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"TrdRepIndicatorsGrp" => #{
                              "Fields" => #{"TrdRepPartyRole" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRepIndicator" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoTrdRepIndicators" => #{
                              "Fields" => #{"TrdRepPartyRole" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRepIndicator" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"RelSymDerivSecUpdGrp" => #{
                              "Fields" => #{"ListUpdateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"RelSymTransactTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"ListUpdateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"RelSymTransactTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryPriceLimits" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"DerivativeInstrumentParties" => #{
                              "Fields" => #{"DerivativeInstrumentPartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeInstrumentPartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeInstrumentPartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoDerivativeInstrumentParties" => #{
                              "Fields" => #{"DerivativeInstrumentPartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeInstrumentPartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeInstrumentPartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"DerivativeInstrumentPartySubIDsGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"MarketDataFeedTypes" => #{
                              "Fields" => #{"MDFeedType" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketDepth" =>#{"Required" => "N", "Sequence" => undefined}
,"MDBookType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMDFeedTypes" => #{
                              "Fields" => #{"MDFeedType" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketDepth" =>#{"Required" => "N", "Sequence" => undefined}
,"MDBookType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"TickRules" => #{
                              "Fields" => #{"StartTickPriceRange" =>#{"Required" => "N", "Sequence" => undefined}
,"EndTickPriceRange" =>#{"Required" => "N", "Sequence" => undefined}
,"TickIncrement" =>#{"Required" => "N", "Sequence" => undefined}
,"TickRuleType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoTickRules" => #{
                              "Fields" => #{"StartTickPriceRange" =>#{"Required" => "N", "Sequence" => undefined}
,"EndTickPriceRange" =>#{"Required" => "N", "Sequence" => undefined}
,"TickIncrement" =>#{"Required" => "N", "Sequence" => undefined}
,"TickRuleType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"RootParties" => #{
                              "Fields" => #{"RootPartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"RootPartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"RootPartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRootPartyIDs" => #{
                              "Fields" => #{"RootPartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"RootPartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"RootPartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"RootSubParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"UnderlyingInstrument" => #{
                              "Fields" => #{"UnderlyingSymbol" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingProduct" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCFICode" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecuritySubType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityDate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingMaturityTime" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCouponPaymentDate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingIssueDate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingRepoCollateralSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingRepurchaseTerm" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingRepurchaseRate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingFactor" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCreditRating" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingInstrRegistry" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCountryOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStateOrProvinceOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLocaleOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingRedemptionDate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStrikeCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingOptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingUnitOfMeasure" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingUnitOfMeasureQty" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingPriceUnitOfMeasure" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingPriceUnitOfMeasureQty" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingTimeUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingExerciseStyle" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCouponRate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedUnderlyingSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCPProgram" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCPRegType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingAllocationPercent" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingQty" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSettlementType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCashAmount" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCashType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingPx" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingDirtyPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingEndPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStartValue" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCurrentValue" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingEndValue" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingAdjustedQuantity" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingFXRate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingFXRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCapValue" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSettlMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingPutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingContractMultiplierUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingFlowScheduleType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingRestructuringType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSeniority" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingNotionalPercentageOutstanding" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingOriginalNotionalPercentageOutstanding" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingAttachmentPoint" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingDetachmentPoint" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"UndSecAltIDGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"UndlyInstrumentParties" =>#{"Required" => "N", "Sequence" => undefined}
}}
,"QuotSetGrp" => #{
                              "Fields" => #{"QuoteSetID" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteSetValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoQuoteEntries" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoQuoteSets" => #{
                              "Fields" => #{"QuoteSetID" =>#{"Required" => "Y", "Sequence" => undefined}
,"QuoteSetValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoQuoteEntries" =>#{"Required" => "Y", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotEntryGrp" =>#{"Required" => "Y", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"AllocGrp" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryIndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocCustomerCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocPositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocType" =>#{"Required" => "N", "Sequence" => undefined}
,"NotifyBrokerOfCredit" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocHandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocNetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocInterestAtMaturity" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlInstType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryIndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocCustomerCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocPositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocType" =>#{"Required" => "N", "Sequence" => undefined}
,"NotifyBrokerOfCredit" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocHandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedAllocText" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocNetMoney" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRateCalc" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAccruedInterestAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocInterestAtMaturity" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlInstType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"ClrInstGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstructionsData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"SettlPtysSubGrp" => #{
                              "Fields" => #{"SettlPartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlPartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSettlPartySubIDs" => #{
                              "Fields" => #{"SettlPartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlPartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"InstrumentPtysSubGrp" => #{
                              "Fields" => #{"InstrumentPartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrumentPartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoInstrumentPartySubIDs" => #{
                              "Fields" => #{"InstrumentPartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrumentPartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"SecLstUpdRelSymsLegGrp" => #{
                              "Fields" => #{"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"LegBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"QuotReqGrp" => #{
                              "Fields" => #{"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotePriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotePriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotReqLegsGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotQualGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"RateSource" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"QuotSetAckGrp" => #{
                              "Fields" => #{"QuoteSetID" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoQuoteEntries" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoCxldQuotes" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoAccQuotes" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoRejQuotes" =>#{"Required" => "N", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteSetValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoQuoteSets" => #{
                              "Fields" => #{"QuoteSetID" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoQuoteEntries" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoCxldQuotes" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoAccQuotes" =>#{"Required" => "N", "Sequence" => undefined}
,"TotNoRejQuotes" =>#{"Required" => "N", "Sequence" => undefined}
,"LastFragment" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteSetValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotEntryAckGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"RFQReqGrp" => #{
                              "Fields" => #{"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"ApplIDReportGrp" => #{
                              "Fields" => #{"RefApplID" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplNewSeqNum" =>#{"Required" => "N", "Sequence" => undefined}
,"RefApplLastSeqNum" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoApplIDs" => #{
                              "Fields" => #{"RefApplID" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplNewSeqNum" =>#{"Required" => "N", "Sequence" => undefined}
,"RefApplLastSeqNum" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"CpctyConfGrp" => #{
                              "Fields" => #{"OrderCapacity" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacityQty" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoCapacities" => #{
                              "Fields" => #{"OrderCapacity" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacityQty" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"UndInstrmtGrp" => #{
                              "Fields" => #{}
                              ,"Groups" => #{
                              "NoUnderlyings" => #{
                              "Fields" => #{}
                              ,"Groups" => #{}
                              ,"Components" => #{"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"Parties" => #{
                              "Fields" => #{"PartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"PartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"PartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoPartyIDs" => #{
                              "Fields" => #{"PartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"PartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"PartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"PtysSubGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"TradingSessionRulesGrp" => #{
                              "Fields" => #{"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoTradingSessionRules" => #{
                              "Fields" => #{"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"TradingSessionRules" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"UndlyInstrumentPtysSubGrp" => #{
                              "Fields" => #{"UnderlyingInstrumentPartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingInstrumentPartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoUndlyInstrumentPartySubIDs" => #{
                              "Fields" => #{"UnderlyingInstrumentPartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingInstrumentPartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"NestedParties" => #{
                              "Fields" => #{"NestedPartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedPartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedPartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoNestedPartyIDs" => #{
                              "Fields" => #{"NestedPartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedPartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedPartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NstdPtysSubGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"TrdgSesGrp" => #{
                              "Fields" => #{"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoTradingSessions" => #{
                              "Fields" => #{"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"AffectedOrdGrp" => #{
                              "Fields" => #{"OrigClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"AffectedOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"AffectedSecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAffectedOrders" => #{
                              "Fields" => #{"OrigClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"AffectedOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"AffectedSecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"CommissionData" => #{
                              "Fields" => #{"Commission" =>#{"Required" => "N", "Sequence" => undefined}
,"CommType" =>#{"Required" => "N", "Sequence" => undefined}
,"CommCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"FundRenewWaiv" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"FillsGrp" => #{
                              "Fields" => #{"FillExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"FillPx" =>#{"Required" => "N", "Sequence" => undefined}
,"FillQty" =>#{"Required" => "N", "Sequence" => undefined}
,"FillLiquidityInd" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoFills" => #{
                              "Fields" => #{"FillExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"FillPx" =>#{"Required" => "N", "Sequence" => undefined}
,"FillQty" =>#{"Required" => "N", "Sequence" => undefined}
,"FillLiquidityInd" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties4" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"NotAffectedOrdersGrp" => #{
                              "Fields" => #{"NotAffOrigClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"NotAffectedOrderID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoNotAffectedOrders" => #{
                              "Fields" => #{"NotAffOrigClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"NotAffectedOrderID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"DerivativeInstrument" => #{
                              "Fields" => #{"DerivativeSymbol" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeSymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeSecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeSecurityIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeProduct" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeProductComplex" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivFlexProductEligibilityIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeSecurityGroup" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeCFICode" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeSecuritySubType" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeMaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeMaturityDate" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeMaturityTime" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeSettleOnOpenFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeInstrmtAssignmentMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeSecurityStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeIssueDate" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeInstrRegistry" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeCountryOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeStateOrProvinceOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeLocaleOfIssue" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeStrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeStrikeCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeStrikeMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeStrikeValue" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeOptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeContractMultiplier" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeMinPriceIncrement" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeMinPriceIncrementAmount" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeUnitOfMeasure" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeUnitOfMeasureQty" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativePriceUnitOfMeasure" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativePriceUnitOfMeasureQty" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeSettlMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativePriceQuoteMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeValuationMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeListMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeCapPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeFloorPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativePutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeExerciseStyle" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeOptPayAmount" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeTimeUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeSecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativePositionLimit" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeNTPositionLimit" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeEncodedIssuerLen" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeEncodedIssuer" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeEncodedSecurityDescLen" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeEncodedSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeContractSettlMonth" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeContractMultiplierUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeFlowScheduleType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"DerivativeSecurityAltIDGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeSecurityXML" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeEventsGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeInstrumentParties" =>#{"Required" => "N", "Sequence" => undefined}
}}
,"LegOrdGrp" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegVolatility" =>#{"Required" => "N", "Sequence" => undefined}
,"LegDividendYield" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCurrencyRatio" =>#{"Required" => "N", "Sequence" => undefined}
,"LegExecInst" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegVolatility" =>#{"Required" => "N", "Sequence" => undefined}
,"LegDividendYield" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCurrencyRatio" =>#{"Required" => "N", "Sequence" => undefined}
,"LegExecInst" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPreAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"RelSymDerivSecGrp" => #{
                              "Fields" => #{"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"RelSymTransactTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"RelSymTransactTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryPriceLimits" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"DlvyInstGrp" => #{
                              "Fields" => #{"SettlInstSource" =>#{"Required" => "N", "Sequence" => undefined}
,"DlvyInstType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoDlvyInst" => #{
                              "Fields" => #{"SettlInstSource" =>#{"Required" => "N", "Sequence" => undefined}
,"DlvyInstType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"SettlParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"InstrmtStrkPxGrp" => #{
                              "Fields" => #{"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoStrikes" => #{
                              "Fields" => #{"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"ContraGrp" => #{
                              "Fields" => #{"ContraBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTrader" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTradeQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTradeTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraLegRefID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoContraBrokers" => #{
                              "Fields" => #{"ContraBroker" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTrader" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTradeQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraTradeTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ContraLegRefID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"DerivativeSecurityDefinition" => #{
                              "Fields" => #{}
                              ,"Groups" => #{}
                              ,"Components" => #{"DerivativeInstrument" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeInstrumentAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentGrp" =>#{"Required" => "N", "Sequence" => undefined}
}}
,"PriceLimits" => #{
                              "Fields" => #{"PriceLimitType" =>#{"Required" => "N", "Sequence" => undefined}
,"LowLimitPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"HighLimitPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingReferencePrice" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"InstrmtGrp" => #{
                              "Fields" => #{}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"SecListGrp" => #{
                              "Fields" => #{"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"RelSymTransactTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"RelSymTransactTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrumentExtension" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityTradingRules" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikeRules" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegSecListGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"UndInstrmtCollGrp" => #{
                              "Fields" => #{"CollAction" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoUnderlyings" => #{
                              "Fields" => #{"CollAction" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"CollInqQualGrp" => #{
                              "Fields" => #{"CollInquiryQualifier" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoCollInquiryQualifier" => #{
                              "Fields" => #{"CollInquiryQualifier" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"RateSource" => #{
                              "Fields" => #{"RateSource" =>#{"Required" => "N", "Sequence" => undefined}
,"RateSourceType" =>#{"Required" => "N", "Sequence" => undefined}
,"ReferencePage" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRateSources" => #{
                              "Fields" => #{"RateSource" =>#{"Required" => "N", "Sequence" => undefined}
,"RateSourceType" =>#{"Required" => "N", "Sequence" => undefined}
,"ReferencePage" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"TradeReportOrderDetail" => #{
                              "Fields" => #{"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefOrderIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"RefOrdIDReason" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"LeavesQty" =>#{"Required" => "N", "Sequence" => undefined}
,"CumQty" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigCustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderInputDevice" =>#{"Required" => "N", "Sequence" => undefined}
,"LotType" =>#{"Required" => "N", "Sequence" => undefined}
,"TransBkdTime" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigOrdModTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"OrderQtyData" =>#{"Required" => "N", "Sequence" => undefined}
,"DisplayInstruction" =>#{"Required" => "N", "Sequence" => undefined}
}}
,"CompIDStatGrp" => #{
                              "Fields" => #{"RefCompID" =>#{"Required" => "Y", "Sequence" => undefined}
,"RefSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}
,"StatusValue" =>#{"Required" => "Y", "Sequence" => undefined}
,"StatusText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoCompIDs" => #{
                              "Fields" => #{"RefCompID" =>#{"Required" => "Y", "Sequence" => undefined}
,"RefSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}
,"StatusValue" =>#{"Required" => "Y", "Sequence" => undefined}
,"StatusText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"LegStipulations" => #{
                              "Fields" => #{"LegStipulationType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStipulationValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegStipulations" => #{
                              "Fields" => #{"LegStipulationType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStipulationValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"ComplexEventTimes" => #{
                              "Fields" => #{"ComplexEventStartTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplexEventEndTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoComplexEventTimes" => #{
                              "Fields" => #{"ComplexEventStartTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplexEventEndTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"SpreadOrBenchmarkCurveData" => #{
                              "Fields" => #{"Spread" =>#{"Required" => "N", "Sequence" => undefined}
,"BenchmarkCurveCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"BenchmarkCurveName" =>#{"Required" => "N", "Sequence" => undefined}
,"BenchmarkCurvePoint" =>#{"Required" => "N", "Sequence" => undefined}
,"BenchmarkPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"BenchmarkPriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"BenchmarkSecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"BenchmarkSecurityIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"QuotCxlEntriesGrp" => #{
                              "Fields" => #{}
                              ,"Groups" => #{
                              "NoQuoteEntries" => #{
                              "Fields" => #{}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"ContAmtGrp" => #{
                              "Fields" => #{"ContAmtType" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtValue" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtCurr" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoContAmts" => #{
                              "Fields" => #{"ContAmtType" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtValue" =>#{"Required" => "N", "Sequence" => undefined}
,"ContAmtCurr" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"MDFullGrp" => #{
                              "Fields" => #{"MDEntryType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MDEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPx" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySize" =>#{"Required" => "N", "Sequence" => undefined}
,"LotType" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TickDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"MDMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityTradingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"HaltReasonInt" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryOriginator" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenCloseSettlFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"SellerDays" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryBuyer" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySeller" =>#{"Required" => "N", "Sequence" => undefined}
,"NumberOfOrders" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPositionNo" =>#{"Required" => "N", "Sequence" => undefined}
,"Scope" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceDelta" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"MDPriceLevel" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"MDOriginType" =>#{"Required" => "N", "Sequence" => undefined}
,"HighPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LowPx" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeVolume" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDQuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"RptSeq" =>#{"Required" => "N", "Sequence" => undefined}
,"DealingCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"FirstPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMDEntries" => #{
                              "Fields" => #{"MDEntryType" =>#{"Required" => "Y", "Sequence" => undefined}
,"MDEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPx" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySize" =>#{"Required" => "N", "Sequence" => undefined}
,"LotType" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TickDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"MDMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityTradingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"HaltReasonInt" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryOriginator" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenCloseSettlFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"SellerDays" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryBuyer" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySeller" =>#{"Required" => "N", "Sequence" => undefined}
,"NumberOfOrders" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPositionNo" =>#{"Required" => "N", "Sequence" => undefined}
,"Scope" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceDelta" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"MDPriceLevel" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"MDOriginType" =>#{"Required" => "N", "Sequence" => undefined}
,"HighPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LowPx" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeVolume" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDQuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"RptSeq" =>#{"Required" => "N", "Sequence" => undefined}
,"DealingCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"FirstPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"SecSizesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"RateSource" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"NstdPtys4SubGrp" => #{
                              "Fields" => #{"Nested4PartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested4PartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoNested4PartySubIDs" => #{
                              "Fields" => #{"Nested4PartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested4PartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"MDIncGrp" => #{
                              "Fields" => #{"MDUpdateAction" =>#{"Required" => "Y", "Sequence" => undefined}
,"DeleteReason" =>#{"Required" => "N", "Sequence" => undefined}
,"MDSubBookType" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketDepth" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryType" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancialStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPx" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySize" =>#{"Required" => "N", "Sequence" => undefined}
,"LotType" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TickDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"MDMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityTradingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"HaltReasonInt" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchType" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryOriginator" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenCloseSettlFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"SellerDays" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryBuyer" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySeller" =>#{"Required" => "N", "Sequence" => undefined}
,"NumberOfOrders" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPositionNo" =>#{"Required" => "N", "Sequence" => undefined}
,"Scope" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceDelta" =>#{"Required" => "N", "Sequence" => undefined}
,"NetChgPrevDay" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"MDPriceLevel" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"MDOriginType" =>#{"Required" => "N", "Sequence" => undefined}
,"HighPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LowPx" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeVolume" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransBkdTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"MDQuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"RptSeq" =>#{"Required" => "N", "Sequence" => undefined}
,"DealingCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"FirstPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MDStreamID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMDEntries" => #{
                              "Fields" => #{"MDUpdateAction" =>#{"Required" => "Y", "Sequence" => undefined}
,"DeleteReason" =>#{"Required" => "N", "Sequence" => undefined}
,"MDSubBookType" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketDepth" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryType" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"FinancialStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"CorporateAction" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPx" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySize" =>#{"Required" => "N", "Sequence" => undefined}
,"LotType" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryDate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TickDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"MDMkt" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityTradingStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"HaltReasonInt" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeCondition" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchType" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryOriginator" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}
,"OpenCloseSettlFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"SellerDays" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryBuyer" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySeller" =>#{"Required" => "N", "Sequence" => undefined}
,"NumberOfOrders" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryPositionNo" =>#{"Required" => "N", "Sequence" => undefined}
,"Scope" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceDelta" =>#{"Required" => "N", "Sequence" => undefined}
,"NetChgPrevDay" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"MDPriceLevel" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"MDOriginType" =>#{"Required" => "N", "Sequence" => undefined}
,"HighPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LowPx" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeVolume" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TransBkdTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"MDQuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"RptSeq" =>#{"Required" => "N", "Sequence" => undefined}
,"DealingCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntrySpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"MDEntryForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"FirstPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"MDStreamID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"SecSizesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"StatsIndGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"RateSource" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"UnderlyingAmount" => #{
                              "Fields" => #{"UnderlyingPayAmount" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCollectAmount" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSettlementDate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSettlementStatus" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoUnderlyingAmounts" => #{
                              "Fields" => #{"UnderlyingPayAmount" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingCollectAmount" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSettlementDate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSettlementStatus" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"TrdRegTimestamps" => #{
                              "Fields" => #{"TrdRegTimestamp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestampType" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestampOrigin" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskType" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskTypeSource" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskOrderHandlingInst" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoTrdRegTimestamps" => #{
                              "Fields" => #{"TrdRegTimestamp" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestampType" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdRegTimestampOrigin" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskType" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskTypeSource" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskOrderHandlingInst" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"ComplexEvents" => #{
                              "Fields" => #{"ComplexEventType" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplexOptPayoutAmount" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplexEventPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplexEventPriceBoundaryMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplexEventPriceBoundaryPrecision" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplexEventPriceTimeType" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplexEventCondition" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoComplexEvents" => #{
                              "Fields" => #{"ComplexEventType" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplexOptPayoutAmount" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplexEventPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplexEventPriceBoundaryMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplexEventPriceBoundaryPrecision" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplexEventPriceTimeType" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplexEventCondition" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ComplexEventDates" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"DerivativeInstrumentAttribute" => #{
                              "Fields" => #{"DerivativeInstrAttribType" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeInstrAttribValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoDerivativeInstrAttrib" => #{
                              "Fields" => #{"DerivativeInstrAttribType" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeInstrAttribValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"MarketSegmentGrp" => #{
                              "Fields" => #{"MarketID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMarketSegments" => #{
                              "Fields" => #{"MarketID" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketSegmentID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"SecurityTradingRules" =>#{"Required" => "N", "Sequence" => undefined}
,"StrikeRules" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"LegQuotStatGrp" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"InstrmtLegGrp" => #{
                              "Fields" => #{}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"QuotReqRjctGrp" => #{
                              "Fields" => #{"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotePriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRelatedSym" => #{
                              "Fields" => #{"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteRequestType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteType" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotePriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"FinancingDetails" =>#{"Required" => "N", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotReqLegsGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"QuotQualGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"PegInstructions" => #{
                              "Fields" => #{"PegOffsetValue" =>#{"Required" => "N", "Sequence" => undefined}
,"PegPriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"PegMoveType" =>#{"Required" => "N", "Sequence" => undefined}
,"PegOffsetType" =>#{"Required" => "N", "Sequence" => undefined}
,"PegLimitType" =>#{"Required" => "N", "Sequence" => undefined}
,"PegRoundDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"PegScope" =>#{"Required" => "N", "Sequence" => undefined}
,"PegSecurityIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"PegSecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"PegSymbol" =>#{"Required" => "N", "Sequence" => undefined}
,"PegSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"InstrumentParties" => #{
                              "Fields" => #{"InstrumentPartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrumentPartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrumentPartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoInstrumentParties" => #{
                              "Fields" => #{"InstrumentPartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrumentPartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrumentPartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentPtysSubGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"DerivativeSecurityAltIDGrp" => #{
                              "Fields" => #{"DerivativeSecurityAltID" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeSecurityAltIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoDerivativeSecurityAltID" => #{
                              "Fields" => #{"DerivativeSecurityAltID" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeSecurityAltIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"SettlObligationInstructions" => #{
                              "Fields" => #{"NetGrossInd" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlObligID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlObligTransType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlObligRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"CcyAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"LastUpdateTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSettlOblig" => #{
                              "Fields" => #{"NetGrossInd" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlObligID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlObligTransType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlObligRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"CcyAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrFxRate" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"LastUpdateTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDetails" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"LegQuotGrp" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegBidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegBidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegBidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegBidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"LegOfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
,"LegBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"ExecAllocGrp" => #{
                              "Fields" => #{"LastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastParPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"FirmTradeID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoExecs" => #{
                              "Fields" => #{"LastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryExecID" =>#{"Required" => "N", "Sequence" => undefined}
,"LastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastParPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LastCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeID" =>#{"Required" => "N", "Sequence" => undefined}
,"FirmTradeID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"TrdCapDtGrp" => #{
                              "Fields" => #{"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LastUpdateTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoDates" => #{
                              "Fields" => #{"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LastUpdateTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"ExecInstRules" => #{
                              "Fields" => #{"ExecInstValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoExecInstRules" => #{
                              "Fields" => #{"ExecInstValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"TriggeringInstruction" => #{
                              "Fields" => #{"TriggerType" =>#{"Required" => "N", "Sequence" => undefined}
,"TriggerAction" =>#{"Required" => "N", "Sequence" => undefined}
,"TriggerPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"TriggerSymbol" =>#{"Required" => "N", "Sequence" => undefined}
,"TriggerSecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"TriggerSecurityIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"TriggerSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}
,"TriggerPriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"TriggerPriceTypeScope" =>#{"Required" => "N", "Sequence" => undefined}
,"TriggerPriceDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"TriggerNewPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"TriggerOrderType" =>#{"Required" => "N", "Sequence" => undefined}
,"TriggerNewQty" =>#{"Required" => "N", "Sequence" => undefined}
,"TriggerTradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TriggerTradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"IOIQualGrp" => #{
                              "Fields" => #{"IOIQualifier" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoIOIQualifiers" => #{
                              "Fields" => #{"IOIQualifier" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"NstdPtysSubGrp" => #{
                              "Fields" => #{"NestedPartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedPartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoNestedPartySubIDs" => #{
                              "Fields" => #{"NestedPartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedPartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"PositionQty" => #{
                              "Fields" => #{"PosType" =>#{"Required" => "N", "Sequence" => undefined}
,"LongQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ShortQty" =>#{"Required" => "N", "Sequence" => undefined}
,"PosQtyStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"QuantityDate" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoPositions" => #{
                              "Fields" => #{"PosType" =>#{"Required" => "N", "Sequence" => undefined}
,"LongQty" =>#{"Required" => "N", "Sequence" => undefined}
,"ShortQty" =>#{"Required" => "N", "Sequence" => undefined}
,"PosQtyStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"QuantityDate" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"NstdPtys2SubGrp" => #{
                              "Fields" => #{"Nested2PartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested2PartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoNested2PartySubIDs" => #{
                              "Fields" => #{"Nested2PartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested2PartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"PosUndInstrmtGrp" => #{
                              "Fields" => #{"UnderlyingSettlPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSettlPriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingDeliveryAmount" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoUnderlyings" => #{
                              "Fields" => #{"UnderlyingSettlPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSettlPriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingDeliveryAmount" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"UnderlyingInstrument" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingAmount" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"SettlInstructionsData" => #{
                              "Fields" => #{"SettlDeliveryType" =>#{"Required" => "N", "Sequence" => undefined}
,"StandInstDbType" =>#{"Required" => "N", "Sequence" => undefined}
,"StandInstDbName" =>#{"Required" => "N", "Sequence" => undefined}
,"StandInstDbID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"DlvyInstGrp" =>#{"Required" => "N", "Sequence" => undefined}
}}
,"SideTrdRegTS" => #{
                              "Fields" => #{"SideTrdRegTimestamp" =>#{"Required" => "N", "Sequence" => undefined}
,"SideTrdRegTimestampType" =>#{"Required" => "N", "Sequence" => undefined}
,"SideTrdRegTimestampSrc" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSideTrdRegTS" => #{
                              "Fields" => #{"SideTrdRegTimestamp" =>#{"Required" => "N", "Sequence" => undefined}
,"SideTrdRegTimestampType" =>#{"Required" => "N", "Sequence" => undefined}
,"SideTrdRegTimestampSrc" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"StrmAsgnRptGrp" => #{
                              "Fields" => #{}
                              ,"Groups" => #{
                              "NoAsgnReqs" => #{
                              "Fields" => #{}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"StrmAsgnRptInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"TradingSessionRules" => #{
                              "Fields" => #{}
                              ,"Groups" => #{}
                              ,"Components" => #{"OrdTypeRules" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForceRules" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInstRules" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchRules" =>#{"Required" => "N", "Sequence" => undefined}
,"MarketDataFeedTypes" =>#{"Required" => "N", "Sequence" => undefined}
}}
,"MsgTypeGrp" => #{
                              "Fields" => #{"RefMsgType" =>#{"Required" => "N", "Sequence" => undefined}
,"MsgDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"RefApplVerID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefApplExtID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefCstmApplVerID" =>#{"Required" => "N", "Sequence" => undefined}
,"DefaultVerIndicator" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMsgTypes" => #{
                              "Fields" => #{"RefMsgType" =>#{"Required" => "N", "Sequence" => undefined}
,"MsgDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"RefApplVerID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefApplExtID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefCstmApplVerID" =>#{"Required" => "N", "Sequence" => undefined}
,"DefaultVerIndicator" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"UndSecAltIDGrp" => #{
                              "Fields" => #{"UnderlyingSecurityAltID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityAltIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoUnderlyingSecurityAltID" => #{
                              "Fields" => #{"UnderlyingSecurityAltID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingSecurityAltIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"ExecCollGrp" => #{
                              "Fields" => #{"ExecID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoExecs" => #{
                              "Fields" => #{"ExecID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"UnderlyingLegSecurityAltIDGrp" => #{
                              "Fields" => #{"UnderlyingLegSecurityAltID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLegSecurityAltIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoUnderlyingLegSecurityAltID" => #{
                              "Fields" => #{"UnderlyingLegSecurityAltID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLegSecurityAltIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"DisplayInstruction" => #{
                              "Fields" => #{"DisplayQty" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryDisplayQty" =>#{"Required" => "N", "Sequence" => undefined}
,"DisplayWhen" =>#{"Required" => "N", "Sequence" => undefined}
,"DisplayMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"DisplayLowQty" =>#{"Required" => "N", "Sequence" => undefined}
,"DisplayHighQty" =>#{"Required" => "N", "Sequence" => undefined}
,"DisplayMinIncr" =>#{"Required" => "N", "Sequence" => undefined}
,"RefreshQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"QuotEntryGrp" => #{
                              "Fields" => #{"QuoteEntryID" =>#{"Required" => "Y", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"MidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"MidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferYield" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoQuoteEntries" => #{
                              "Fields" => #{"QuoteEntryID" =>#{"Required" => "Y", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"MidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"MidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferYield" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"RgstDtlsGrp" => #{
                              "Fields" => #{"RegistDtls" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistEmail" =>#{"Required" => "N", "Sequence" => undefined}
,"MailingDtls" =>#{"Required" => "N", "Sequence" => undefined}
,"MailingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OwnerType" =>#{"Required" => "N", "Sequence" => undefined}
,"DateOfBirth" =>#{"Required" => "N", "Sequence" => undefined}
,"InvestorCountryOfResidence" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRegistDtls" => #{
                              "Fields" => #{"RegistDtls" =>#{"Required" => "N", "Sequence" => undefined}
,"RegistEmail" =>#{"Required" => "N", "Sequence" => undefined}
,"MailingDtls" =>#{"Required" => "N", "Sequence" => undefined}
,"MailingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OwnerType" =>#{"Required" => "N", "Sequence" => undefined}
,"DateOfBirth" =>#{"Required" => "N", "Sequence" => undefined}
,"InvestorCountryOfResidence" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"NstdPtys3SubGrp" => #{
                              "Fields" => #{"Nested3PartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested3PartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoNested3PartySubIDs" => #{
                              "Fields" => #{"Nested3PartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Nested3PartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"DerivativeEventsGrp" => #{
                              "Fields" => #{"DerivativeEventType" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeEventDate" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeEventTime" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeEventPx" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeEventText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoDerivativeEvents" => #{
                              "Fields" => #{"DerivativeEventType" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeEventDate" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeEventTime" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeEventPx" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeEventText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"NestedInstrumentAttribute" => #{
                              "Fields" => #{"NestedInstrAttribType" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedInstrAttribValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoNestedInstrAttrib" => #{
                              "Fields" => #{"NestedInstrAttribType" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedInstrAttribValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"SecondaryPriceLimits" => #{
                              "Fields" => #{"SecondaryPriceLimitType" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryLowLimitPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryHighLimitPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryTradingReferencePrice" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"SecurityXML" => #{
                              "Fields" => #{"SecurityXMLLen" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityXML" =>#{"Required" => "N", "Sequence" => undefined}
,"SecurityXMLSchema" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"EvntGrp" => #{
                              "Fields" => #{"EventType" =>#{"Required" => "N", "Sequence" => undefined}
,"EventDate" =>#{"Required" => "N", "Sequence" => undefined}
,"EventTime" =>#{"Required" => "N", "Sequence" => undefined}
,"EventPx" =>#{"Required" => "N", "Sequence" => undefined}
,"EventText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoEvents" => #{
                              "Fields" => #{"EventType" =>#{"Required" => "N", "Sequence" => undefined}
,"EventDate" =>#{"Required" => "N", "Sequence" => undefined}
,"EventTime" =>#{"Required" => "N", "Sequence" => undefined}
,"EventPx" =>#{"Required" => "N", "Sequence" => undefined}
,"EventText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"CompIDReqGrp" => #{
                              "Fields" => #{"RefCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoCompIDs" => #{
                              "Fields" => #{"RefCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"LocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeskID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"UnderlyingLegInstrument" => #{
                              "Fields" => #{"UnderlyingLegSymbol" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLegSymbolSfx" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLegSecurityID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLegSecurityIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLegCFICode" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLegSecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLegSecuritySubType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLegMaturityMonthYear" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLegMaturityDate" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLegMaturityTime" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLegStrikePrice" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLegOptAttribute" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLegPutOrCall" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLegSecurityExchange" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingLegSecurityDesc" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"UnderlyingLegSecurityAltIDGrp" =>#{"Required" => "N", "Sequence" => undefined}
}}
,"SecSizesGrp" => #{
                              "Fields" => #{"MDSecSizeType" =>#{"Required" => "N", "Sequence" => undefined}
,"MDSecSize" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoOfSecSizes" => #{
                              "Fields" => #{"MDSecSizeType" =>#{"Required" => "N", "Sequence" => undefined}
,"MDSecSize" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"SecTypesGrp" => #{
                              "Fields" => #{"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySubType" =>#{"Required" => "N", "Sequence" => undefined}
,"Product" =>#{"Required" => "N", "Sequence" => undefined}
,"CFICode" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSecurityTypes" => #{
                              "Fields" => #{"SecurityType" =>#{"Required" => "N", "Sequence" => undefined}
,"SecuritySubType" =>#{"Required" => "N", "Sequence" => undefined}
,"Product" =>#{"Required" => "N", "Sequence" => undefined}
,"CFICode" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"AttrbGrp" => #{
                              "Fields" => #{"InstrAttribType" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrAttribValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoInstrAttrib" => #{
                              "Fields" => #{"InstrAttribType" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrAttribValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"UnderlyingStipulations" => #{
                              "Fields" => #{"UnderlyingStipType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStipValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoUnderlyingStips" => #{
                              "Fields" => #{"UnderlyingStipType" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingStipValue" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"ComplexEventDates" => #{
                              "Fields" => #{"ComplexEventStartDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplexEventEndDate" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoComplexEventDates" => #{
                              "Fields" => #{"ComplexEventStartDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplexEventEndDate" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"ComplexEventTimes" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"MiscFeesGrp" => #{
                              "Fields" => #{"MiscFeeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeType" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeBasis" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMiscFees" => #{
                              "Fields" => #{"MiscFeeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeType" =>#{"Required" => "N", "Sequence" => undefined}
,"MiscFeeBasis" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"RoutingGrp" => #{
                              "Fields" => #{"RoutingType" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoRoutingIDs" => #{
                              "Fields" => #{"RoutingType" =>#{"Required" => "N", "Sequence" => undefined}
,"RoutingID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"RgstDistInstGrp" => #{
                              "Fields" => #{"DistribPaymentMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"DistribPercentage" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribAgentName" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribAgentCode" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribAgentAcctNumber" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribPayRef" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribAgentAcctName" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoDistribInsts" => #{
                              "Fields" => #{"DistribPaymentMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"DistribPercentage" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribCurr" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribAgentName" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribAgentCode" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribAgentAcctNumber" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribPayRef" =>#{"Required" => "N", "Sequence" => undefined}
,"CashDistribAgentAcctName" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"OrderQtyData" => #{
                              "Fields" => #{"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"CashOrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderPercent" =>#{"Required" => "N", "Sequence" => undefined}
,"RoundingDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"RoundingModulus" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"ListOrdGrp" => #{
                              "Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstMode" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchIncrement" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxPriceLevels" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestinationIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"SideValueInd" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceProtectionScope" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefOrderIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"PreTradeAnonymity" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategy" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategyParameters" =>#{"Required" => "N", "Sequence" => undefined}
,"ParticipationRate" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoOrders" => #{
                              "Fields" => #{"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlInstMode" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"Account" =>#{"Required" => "N", "Sequence" => undefined}
,"AcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AccountType" =>#{"Required" => "N", "Sequence" => undefined}
,"DayBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingUnit" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"PreallocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"CashMargin" =>#{"Required" => "N", "Sequence" => undefined}
,"ClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"HandlInst" =>#{"Required" => "N", "Sequence" => undefined}
,"ExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"MinQty" =>#{"Required" => "N", "Sequence" => undefined}
,"MatchIncrement" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxPriceLevels" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxFloor" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestination" =>#{"Required" => "N", "Sequence" => undefined}
,"ExDestinationIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"ProcessCode" =>#{"Required" => "N", "Sequence" => undefined}
,"PrevClosePx" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"SideValueInd" =>#{"Required" => "N", "Sequence" => undefined}
,"LocateReqd" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"QtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceProtectionScope" =>#{"Required" => "N", "Sequence" => undefined}
,"StopPx" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"SolicitedFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"IOIID" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefOrderIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"TimeInForce" =>#{"Required" => "N", "Sequence" => undefined}
,"EffectiveTime" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpireTime" =>#{"Required" => "N", "Sequence" => undefined}
,"GTBookingInst" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}
,"PreTradeAnonymity" =>#{"Required" => "N", "Sequence" => undefined}
,"CustOrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"ForexReq" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"Price2" =>#{"Required" => "N", "Sequence" => undefined}
,"PositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"CoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxShow" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategy" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetStrategyParameters" =>#{"Required" => "N", "Sequence" => undefined}
,"ParticipationRate" =>#{"Required" => "N", "Sequence" => undefined}
,"Designation" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"PreAllocGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"DisplayInstruction" =>#{"Required" => "N", "Sequence" => undefined}
,"TrdgSesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Instrument" =>#{"Required" => "Y", "Sequence" => undefined}
,"UndInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"Stipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
,"TriggeringInstruction" =>#{"Required" => "N", "Sequence" => undefined}
,"SpreadOrBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
,"YieldData" =>#{"Required" => "N", "Sequence" => undefined}
,"CommissionData" =>#{"Required" => "N", "Sequence" => undefined}
,"PegInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"DiscretionInstructions" =>#{"Required" => "N", "Sequence" => undefined}
,"StrategyParametersGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"PtysSubGrp" => #{
                              "Fields" => #{"PartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"PartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoPartySubIDs" => #{
                              "Fields" => #{"PartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"PartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"TrdInstrmtLegGrp" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegNumber" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegLastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"LegLastForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCalculatedCcyLastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegGrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"LegVolatility" =>#{"Required" => "N", "Sequence" => undefined}
,"LegDividendYield" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCurrencyRatio" =>#{"Required" => "N", "Sequence" => undefined}
,"LegExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"LegLastQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{"LegQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegReportID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegNumber" =>#{"Required" => "N", "Sequence" => undefined}
,"LegPositionEffect" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCoveredOrUncovered" =>#{"Required" => "N", "Sequence" => undefined}
,"LegRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"LegLastPx" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"LegLastForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCalculatedCcyLastQty" =>#{"Required" => "N", "Sequence" => undefined}
,"LegGrossTradeAmt" =>#{"Required" => "N", "Sequence" => undefined}
,"LegVolatility" =>#{"Required" => "N", "Sequence" => undefined}
,"LegDividendYield" =>#{"Required" => "N", "Sequence" => undefined}
,"LegCurrencyRatio" =>#{"Required" => "N", "Sequence" => undefined}
,"LegExecInst" =>#{"Required" => "N", "Sequence" => undefined}
,"LegLastQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeCapLegUnderlyingsGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"TrdAllocGrp" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocCustomerCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryIndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoAllocs" => #{
                              "Fields" => #{"AllocAccount" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocAcctIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocSettlCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"IndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocQty" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocCustomerCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocMethod" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryIndividualAllocID" =>#{"Required" => "N", "Sequence" => undefined}
,"AllocClearingFeeIndicator" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties2" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"StrmAsgnReqGrp" => #{
                              "Fields" => #{}
                              ,"Groups" => #{
                              "NoAsgnReqs" => #{
                              "Fields" => #{}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"StrmAsgnReqInstrmtGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"ApplIDRequestGrp" => #{
                              "Fields" => #{"RefApplID" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplBegSeqNum" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplEndSeqNum" =>#{"Required" => "N", "Sequence" => undefined}
,"RefApplReqID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoApplIDs" => #{
                              "Fields" => #{"RefApplID" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplBegSeqNum" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplEndSeqNum" =>#{"Required" => "N", "Sequence" => undefined}
,"RefApplReqID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"LegBenchmarkCurveData" => #{
                              "Fields" => #{"LegBenchmarkCurveCurrency" =>#{"Required" => "N", "Sequence" => undefined}
,"LegBenchmarkCurveName" =>#{"Required" => "N", "Sequence" => undefined}
,"LegBenchmarkCurvePoint" =>#{"Required" => "N", "Sequence" => undefined}
,"LegBenchmarkPrice" =>#{"Required" => "N", "Sequence" => undefined}
,"LegBenchmarkPriceType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}}
,"ClrInstGrp" => #{
                              "Fields" => #{"ClearingInstruction" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoClearingInstructions" => #{
                              "Fields" => #{"ClearingInstruction" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"OrdAllocGrp" => #{
                              "Fields" => #{"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderAvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderBookingQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoOrders" => #{
                              "Fields" => #{"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryOrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderAvgPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderBookingQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"NestedParties2" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"NewsRefGrp" => #{
                              "Fields" => #{"NewsRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"NewsRefType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoNewsRefIDs" => #{
                              "Fields" => #{"NewsRefID" =>#{"Required" => "N", "Sequence" => undefined}
,"NewsRefType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"SettlDetails" => #{
                              "Fields" => #{"SettlObligSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSettlDetails" => #{
                              "Fields" => #{"SettlObligSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"SettlParties" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"OrdListStatGrp" => #{
                              "Fields" => #{"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"CumQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"WorkingIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"LeavesQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"CxlQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoOrders" => #{
                              "Fields" => #{"ClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"CumQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdStatus" =>#{"Required" => "Y", "Sequence" => undefined}
,"WorkingIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"LeavesQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"CxlQty" =>#{"Required" => "Y", "Sequence" => undefined}
,"AvgPx" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrdRejReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"BidCompRspGrp" => #{
                              "Fields" => #{"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Country" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"FairValue" =>#{"Required" => "N", "Sequence" => undefined}
,"NetGrossInd" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoBidComponents" => #{
                              "Fields" => #{"ListID" =>#{"Required" => "N", "Sequence" => undefined}
,"Country" =>#{"Required" => "N", "Sequence" => undefined}
,"Side" =>#{"Required" => "N", "Sequence" => undefined}
,"Price" =>#{"Required" => "N", "Sequence" => undefined}
,"PriceType" =>#{"Required" => "N", "Sequence" => undefined}
,"FairValue" =>#{"Required" => "N", "Sequence" => undefined}
,"NetGrossInd" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"CommissionData" =>#{"Required" => "Y", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"OrdTypeRules" => #{
                              "Fields" => #{"OrdType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoOrdTypeRules" => #{
                              "Fields" => #{"OrdType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"SideCrossOrdCxlGrp" => #{
                              "Fields" => #{"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigOrdModTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoSides" => #{
                              "Fields" => #{"Side" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdID" =>#{"Required" => "Y", "Sequence" => undefined}
,"SecondaryClOrdID" =>#{"Required" => "N", "Sequence" => undefined}
,"ClOrdLinkID" =>#{"Required" => "N", "Sequence" => undefined}
,"OrigOrdModTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeOriginationDate" =>#{"Required" => "N", "Sequence" => undefined}
,"TradeDate" =>#{"Required" => "N", "Sequence" => undefined}
,"ComplianceID" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Parties" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQtyData" =>#{"Required" => "Y", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"ExpirationQty" => #{
                              "Fields" => #{"ExpirationQtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoExpiration" => #{
                              "Fields" => #{"ExpirationQtyType" =>#{"Required" => "N", "Sequence" => undefined}
,"ExpQty" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"QuotEntryAckGrp" => #{
                              "Fields" => #{"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"MidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"MidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferYield" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryRejectReason" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoQuoteEntries" => #{
                              "Fields" => #{"QuoteEntryID" =>#{"Required" => "N", "Sequence" => undefined}
,"BidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSize" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSize" =>#{"Required" => "N", "Sequence" => undefined}
,"ValidUntilTime" =>#{"Required" => "N", "Sequence" => undefined}
,"BidSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferSpotRate" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints" =>#{"Required" => "N", "Sequence" => undefined}
,"MidPx" =>#{"Required" => "N", "Sequence" => undefined}
,"BidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"MidYield" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferYield" =>#{"Required" => "N", "Sequence" => undefined}
,"TransactTime" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionID" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate" =>#{"Required" => "N", "Sequence" => undefined}
,"OrdType" =>#{"Required" => "N", "Sequence" => undefined}
,"SettlDate2" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderQty2" =>#{"Required" => "N", "Sequence" => undefined}
,"BidForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"OfferForwardPoints2" =>#{"Required" => "N", "Sequence" => undefined}
,"Currency" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryStatus" =>#{"Required" => "N", "Sequence" => undefined}
,"QuoteEntryRejectReason" =>#{"Required" => "N", "Sequence" => undefined}
,"BookingType" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderCapacity" =>#{"Required" => "N", "Sequence" => undefined}
,"OrderRestrictions" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"Instrument" =>#{"Required" => "N", "Sequence" => undefined}
,"InstrmtLegGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"DerivativeInstrumentPartySubIDsGrp" => #{
                              "Fields" => #{"DerivativeInstrumentPartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeInstrumentPartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoDerivativeInstrumentPartySubIDs" => #{
                              "Fields" => #{"DerivativeInstrumentPartySubID" =>#{"Required" => "N", "Sequence" => undefined}
,"DerivativeInstrumentPartySubIDType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"SecurityTradingRules" => #{
                              "Fields" => #{}
                              ,"Groups" => #{}
                              ,"Components" => #{"BaseTradingRules" =>#{"Required" => "N", "Sequence" => undefined}
,"TradingSessionRulesGrp" =>#{"Required" => "N", "Sequence" => undefined}
,"NestedInstrumentAttribute" =>#{"Required" => "N", "Sequence" => undefined}
}}
,"LegSecAltIDGrp" => #{
                              "Fields" => #{"LegSecurityAltID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecurityAltIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegSecurityAltID" => #{
                              "Fields" => #{"LegSecurityAltID" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSecurityAltIDSource" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"StatsIndGrp" => #{
                              "Fields" => #{"StatsType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoStatsIndicators" => #{
                              "Fields" => #{"StatsType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"InstrmtLegSecListGrp" => #{
                              "Fields" => #{"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoLegs" => #{
                              "Fields" => #{"LegSwapType" =>#{"Required" => "N", "Sequence" => undefined}
,"LegSettlType" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"InstrumentLeg" =>#{"Required" => "N", "Sequence" => undefined}
,"LegStipulations" =>#{"Required" => "N", "Sequence" => undefined}
,"LegBenchmarkCurveData" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
,"UndlyInstrumentParties" => #{
                              "Fields" => #{"UnderlyingInstrumentPartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingInstrumentPartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingInstrumentPartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoUndlyInstrumentParties" => #{
                              "Fields" => #{"UnderlyingInstrumentPartyID" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingInstrumentPartyIDSource" =>#{"Required" => "N", "Sequence" => undefined}
,"UnderlyingInstrumentPartyRole" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"UndlyInstrumentPtysSubGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}

}
                              ,"Components" => #{}}
}.



groups() ->
 #{}.



header()->
#{
"Fields" => #{}, 
"Groups" => #{}, 
 "Components" => #{}

}.



trailer()->
#{
"Fields" => #{}, 
"Groups" => #{}, 
 "Components" => #{}

}.


%%==============================================================================
%%   @author Tanmay Dutta (tanmay.datta86@gmail.com)
%%   @copyright  Tanmay Dutta
%%   @version  0.1.0
%%   @doc  The main driver function
%%   @doc The fix dictionary can be passed in 3 variables
%%   @doc 1. fix_version 2. fix_transport 3. user_defined_fix
%%==============================================================================
-module(fixparser).
-author(tanmay_dutta).
-export([
      encode/1
     ,encode/2
	 ,decode/2
	 ,encode/3
	 ,decode/3
	 ,get_msg/2
        ]).
-compile([{parse_transform, lager_transform}]).
-compile([export_all]).


%% ====================================================================
%% Function Description: The default encode function
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: encode/1
%% Returns: Map of fix message
%% --------------------------------------------------------------------

encode(Message) ->
    {ok, FixVersion} = get_fix_version(Message)
    ,encode(Message, #{fix_version=>FixVersion}).

%% ====================================================================
%% Function Description: The default encode function 
%% Takes a map with params 1. fix_version 2. user_defined_fix
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: encode/2
%% Returns: Map of fix message
%% --------------------------------------------------------------------

encode(Message, VersionMap) ->
	encode(Message, VersionMap, map).

%% ====================================================================
%% Function Description: The general encode function that supports, 
%% various formats [xml, json, map, proplist]
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: encode/3
%% Returns: fix message encoded according to the third argument
%% --------------------------------------------------------------------
encode(#{"MsgType":=MsgType}=Message,
       VersionMap, map) ->
     Sentinal = binary_to_list(<<1>>)
     %% Sentinal = "|"
    ,FixDefs = fixparser_utils:get_consolidated_defs(VersionMap)
    ,{ok, Message_Body} = query_fix({messages, MsgType}, FixDefs)
    ,Header = maps:get(header, FixDefs)
    ,TrailerF = maps:get(trailer, FixDefs)
    ,{HeaderTags, BodyNumericTrailerRest} = encode_block(Header,
                                                         maps:merge(Message,
                                                                    #{"MsgType" => maps:get("Type", 
                                                                                            Message_Body)}),
                                                         FixDefs)
    ,{BodyTags, NumericTrailerRest} = encode_block(Message_Body,
                                                   BodyNumericTrailerRest,
                                                  FixDefs)
    ,{NumericTags, TrailerRest} = encode_numerics(NumericTrailerRest)
    ,{TrailerTags, Rest} = encode_block(TrailerF, TrailerRest,
                                       FixDefs)
    ,case maps:size(Rest) of 
         0 ->
             successful_encoding; 
         _ ->
             lager:info("Unable to encode complete body ~p~n", [Rest])
     end
     ,FullMessage = [compute_tagnums(Tag,Val,FixDefs) || {Tag, Val} <- HeaderTags ++ BodyTags ++ NumericTags ++ TrailerTags]
     , MessageString = to_string(FullMessage, Sentinal)
    ,{FullMessage, MessageString};

encode(_Message, _Version, xml) ->
    ok;
encode(_Message, _Version, json) ->
    ok;
encode(_Message, _Version, proplist) ->
    ok.

%% ====================================================================
%% Function Description: computes the tag number for a field
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: compute_tagnums/1
%% Returns: string(integer())
%% --------------------------------------------------------------------

compute_tagnums(Tag, Val, FixDefs) ->
    case is_integer1(Tag) of
        true ->
            {Tag, Val};
        _->
            TagNum = maps:get("TagNum", maps:get(Tag, maps:get(fields, FixDefs))),
            case maps:get("ValidValues", maps:get(Tag, maps:get(fields,FixDefs))) of 
                [] ->
                    {TagNum, Val};
                ValidList ->
                    UpperTag = string:to_upper(Val),
                    case proplists:get_value(UpperTag, ValidList) of 
                        undefined ->
                            lager:info("**************The value for Tag ~p as --> ~p does not match any valid values ~p  ~n you will get reject message. ************ ~n",[Tag, UpperTag, ValidList]),
                            {TagNum, Val};
                        _ValidValue->
                    {TagNum, Val}
                                end
                     
            end
    end.

%% ====================================================================
%% Function Description: Default decode function takes a fix map and 
%% decodes into a string
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: decode/2
%% Returns: string()
%% --------------------------------------------------------------------

decode(BinStream, Version) ->
	decode(BinStream, Version, {to, map}).


%% ====================================================================
%% Function Description: Generalized decode function that takes another 
%% third argument that defines the Input type of message {from, [map, xml, json, proplist]},
%% and returns a string
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: decode/3
%% Returns: string()
%% --------------------------------------------------------------------

decode(BinStream, FixDefinitionMap, {to, map}) ->
    FixDefs = fixparser_utils:get_consolidated_defs(FixDefinitionMap)
        ,Fields = maps:get(fields, FixDefs)
        ,MessageTags = [{maps:get("Name",
                                maps:get(integer_to_list(Tag),
                                         Fields)),
                         Value} 
                        || {Tag, Value} <- tokenize(BinStream)
                       ]
        ,Header = maps:get(header, FixDefs)
        ,Trailer = maps:get(trailer, FixDefs)
        ,{HeaderMap, BodyTrailer} = decode_block(MessageTags, Header, FixDefs)
        ,{TrailerMap, Body} = decode_block(BodyTrailer, Trailer, FixDefs)
        ,AllMessages = maps:get(messages,FixDefs)
        ,MsgBlock = maps:get(maps:get("Name",
                                      maps:get(maps:get("MsgType",
                                                        HeaderMap),
                                               AllMessages)),
                             AllMessages)
        ,{BodyMap, Rest} = decode_block(Body, MsgBlock, FixDefs)
        ,{maps:merge(maps:merge(HeaderMap, BodyMap), TrailerMap), Rest};
    
    

decode(_BinStream,_Version, {to, xml}) ->
    ok;
decode(_BinStream,_Version, {to, json}) ->
    ok;
decode(_BinStream,_Version, {to, proplist}) ->
    ok.


%% ====================================================================
%% Function Description: takes a block and decode it
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: decode_block/3
%% Returns: map()
%% --------------------------------------------------------------------

decode_block(Message, Block, FixDefs) ->
    {FieldMap, ComponentGroup} = decode_fields(Message, maps:get("Fields",Block)),
    {GroupsMap, Components} = decode_groups(ComponentGroup, maps:get("Groups",Block), FixDefs),
    {ComponentMap, Rest} = decode_components(Components, maps:get("Components",Block), FixDefs),
    {maps:merge(maps:merge(FieldMap, GroupsMap), ComponentMap), Rest}.



%% ====================================================================
%% Function Description: Decodes the components in fix message
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: decode_components/3
%% Returns: tuple(map(), fixpl)
%% --------------------------------------------------------------------

decode_components(Message, ComponentMap, FixDefs) ->
        maps:fold(fun(Component, _Body, {ComponentsDecoded, MessageA})->
                          {ComponentDecoded, RestMessage} = decode_component(Component, MessageA, FixDefs)
                              ,{maps:merge(ComponentsDecoded, ComponentDecoded), RestMessage}
                              end,
                  {#{}, Message}, ComponentMap).


%% ====================================================================
%% Function Description: Decodes single component in fix message
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: decode_component/3
%% Returns: tuple(map, rest)
%% --------------------------------------------------------------------

decode_component(Component, Message, FixDefs) ->
        ComponentBlock = maps:get(Component,
                                  maps:get(components, FixDefs))
        ,decode_block(Message, ComponentBlock, FixDefs).



%% ====================================================================
%% Function Description: Decode groups in fix message
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: decode_groups/3
%% Returns: tuple(map, rest)
%% --------------------------------------------------------------------

decode_groups(Message, GroupMap, FixDefs) ->
    maps:fold(fun(Group, Body, {GroupsDecoded, MessageA}) ->
                      {GroupDecoded, RestMessage} = decode_group(Group, Body, MessageA, FixDefs)
                          ,{maps:merge(GroupsDecoded, GroupDecoded), RestMessage}
                          end,
              {#{}, Message}, GroupMap).


%% ====================================================================
%% Function Description: decode a single group in fix message
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: decode_group/4
%% Returns: tuple(map, rest)
%% --------------------------------------------------------------------

decode_group(Group, Body, Message, FixDefs) ->
        case proplists:get_value(Group, Message) of 
            undefined ->
                {#{}, Message};
            GroupSize ->
                lager:info("found group.. it is --> ~p~n", [Group]),
                lager:info("Body for this group is --> ~p~n", [Body]),
                {GroupBody, Rest} = decode_block(proplists:delete(Group, Message), Body, FixDefs),
                {maps:merge(#{Group=>GroupSize}, GroupBody), Rest}
        end.
                

%% ====================================================================
%% Function Description: Decode fields in a fix message
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: decode_fields/2
%% Returns: tuple(map, rest)
%% --------------------------------------------------------------------

decode_fields(Message, FieldMap) ->
        maps:fold(fun(Field, _Body, {FieldsDecoded, MessageA})->
                          {FieldDecoded, RestMessage} = decode_field(Field, MessageA)
                              ,{maps:merge(FieldsDecoded, FieldDecoded), RestMessage}
                              end,
                  {#{}, Message}, FieldMap).


%% ====================================================================
%% Function Description: Decoded a single fix field
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: decode_field/2
%% Returns: tuple(map,rest)
%% --------------------------------------------------------------------

decode_field(Field, Message) ->
    case proplists:get_value(Field, Message) of 
        undefined ->
            {#{}, Message};
        Value ->
            {#{Field=>Value}, proplists:delete(Field, Message)}
    end.


get_message(Body, BodyMap)->
    AllFields = maps:get("Fields", BodyMap)
    ,{FieldMap,Rest} = get_fields(Body,AllFields)
     ,{FieldMap,Rest}.
    
    
get_fields(Message, FieldMap)-> 
    get_fields(Message, FieldMap, [], #{}).


get_fields([], _FieldsDict, RestMessage, HeaderMap) ->
    {HeaderMap, RestMessage};

get_fields([{T,V}|Rest], FieldDict, RestMessage, FieldMap) ->
    case maps:find(T,FieldDict) of 
	{ok, _Value} ->
	    get_fields(Rest, FieldDict, RestMessage, maps:merge(FieldMap, #{T=>V}));
	error -> 
	    get_fields(Rest, FieldDict, RestMessage ++ [{T,V}], FieldMap)
    end.


%% ====================================================================
%% Function Description: Provides the msgType from the binary msg stream
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: get_msg_type/2
%% Returns: fix_msg_type()
%% --------------------------------------------------------------------

get_msg(MsgType, FixModuleMap) ->
    case query_fix({MsgType, messages}, FixModuleMap) of 
	error ->
            query_fix({maps:get("Name", query_fix({MsgType, messages}, FixModuleMap)), messages}, FixModuleMap);
	Value ->
	    query_fix({maps:get("Name", Value), messages}, FixModuleMap)
    end. 


%% ====================================================================
%% Function Description: To get the transport associated with a particular
%% fix version
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: get_transport/1
%% Returns: transport_module
%% --------------------------------------------------------------------

get_transport(Version) ->
  Fix_Version = maps:get(fix_version, Version)
	,maps:get(Fix_Version, #{
		    fix41 => fix41
		   ,fix42 => fix42
		   ,fix43 => fix43
		   ,fix44 => fix44
		   ,fix50 => fixt11
		   ,fix50sp1 => fixt11
		   ,fix50sp2 => fixt11}).


%% ====================================================================
%% Function Description: Provides the msgType from the binary msg stream
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: get_msg_type/2
%% Returns: fix_msg_type()
%% --------------------------------------------------------------------

query_fix({Type,Query}, FixDefs)->
    maps:find(Query, maps:get(Type, FixDefs)). 

%% ====================================================================
%% Function Description: Create proplist from the binary stream
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: tokenize/1
%% Returns: proplist()
%% --------------------------------------------------------------------
tokenize(BinaryStream)->
    tokenize(BinaryStream, [<<1>>]).

%% ====================================================================
%% Function Description: Tokenize with custom sentinel, rarely useful
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: tokenize/2
%% Returns: proplist()
%% --------------------------------------------------------------------
tokenize(BinaryStream, Sentinel)->
    [{list_to_integer(binary_to_list(T)), binary_to_list(V)} || [T,V] <- [binary:split(TVP, <<"=">>, [global, trim]) || TVP <- binary:split(BinaryStream, Sentinel, [global,trim])]].



%% ====================================================================
%% Function Description: The next function from tokenize that understands
%% fix-speak and converts the msg numbers to human redeable format
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: translate_token/1
%% Returns: String()
%% --------------------------------------------------------------------
translate_token()->
    ok.


%% ====================================================================
%% Function Description: Encodes the fix block in order 
%% Fields --> Components --> Groups
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: encode_block/2
%% Returns: list({tag, value})
%% --------------------------------------------------------------------
encode_block(Block, Message, FixDefs)->
    %% lager:info("Got block ~p~n", [Block]),
    {Fields, ComponentsGroups} = encode_fields(maps:get("Fields", Block),
                                               Message),
    {Groups, ComponentsRest} = encode_groups(maps:get("Groups", Block),
                                   ComponentsGroups, FixDefs),
    {Components, Rest} = encode_components(maps:get("Components", Block),
                                             ComponentsRest, FixDefs),
    {Fields ++ Components ++ Groups, Rest}.

%% ====================================================================
%% Function Description: Encode full fix map
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: encode_fields/2
%% Returns: fix-speak encoding of full message
%% --------------------------------------------------------------------

encode_fields(FieldMap, MessageMap) ->
    maps:fold(fun(K, #{"Required":=V}, {MsgStr, RestMap}) -> 
                   {S, Map} = encode_field(K, V, RestMap)
                   ,{MsgStr ++ S, Map} end
                   ,{[], MessageMap},
                          FieldMap).


%% ====================================================================
%% Function Description: Encode single field 
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: encode_field/
%% Returns: fix-speak encoding
%% --------------------------------------------------------------------
encode_field(Tag, Required, Message)->
    case maps:find(Tag, Message) of
        {ok, Value} ->
            {[{Tag, Value}], maps:remove(Tag, Message)};
        _->
            case Required of
                "Y" -> 
                    case (lists:member(Tag, ["BodyLength", "SendingTime", "CheckSum"])) of
                        true ->
                            never_mind;
                        _ ->
                            lager:info("Tag ~p was required, I did not find it.. It might be an error for some tags ~n", [Tag])
                    end,
                {[], Message};
                _-> 
                    %% ideally it should be undefined for strict evaluation
                    %% {[{Tag, "undefined"}], Message};
                    {[], Message}
            end
    end.


%% ====================================================================
%% Function Description: Encoding numeric numbers as it is
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: encode_numerics/1
%% Returns: {tag, value}
%% --------------------------------------------------------------------

encode_numerics(NumericMap) ->
    maps:fold(fun(K,V, {MsgT, RestMap})->
                      case is_integer1(K) of
                          true ->
                              {MsgT ++ [{K,V}], maps:remove(K, RestMap)};
                          false -> {MsgT, RestMap}
                      end
              end,
              {[], NumericMap}, %% Acc
              NumericMap).  %% map

%% ====================================================================
%% Function Description: Encode all groups
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: encode_groups/2
%% Returns: fix-speak encoding
%% --------------------------------------------------------------------
encode_groups(Groups, Message, _FixDefs) when map_size(Groups) == 0 ->
{[], Message};

encode_groups(_Groups, Message, _FixDefs) when map_size(Message) == 0 ->
{[], #{}};

encode_groups(Groups, Message, FixDefs)->
    maps:fold(fun(Group, Body, {MsgPl, MessageTillNow}) ->
                      {Pl, RestMessage} = encode_group(Group, Body, MessageTillNow, FixDefs),
                      {MsgPl ++ Pl, RestMessage} end,
              {[], Message}, Groups).


%% ====================================================================
%% Function Description: Encode one group
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: encode_group/4
%% Returns: {pl, string()}
%% --------------------------------------------------------------------

encode_group(Group, Body, Message, FixDefs) ->
    case maps:find(Group, Message) of
        error ->
            {[], Message};
        {ok, GroupPL} ->

            {GroupEncoding, RestMessage}  = encode_group_h(GroupPL,
                                                           FixDefs,
                                                           Body,
                                                           []),
            Size = integer_to_list(tuple_size(GroupPL)),
            {[{Group,Size}] ++ GroupEncoding, RestMessage}
    end.

encode_group_h({}, _FD, _B, Answer)->
   {Answer, #{}};

encode_group_h(GroupPL, FixDefs, Body, Accum)->
    BlockMessage = element(1,GroupPL),
    {PL, #{}} = encode_block(Body, BlockMessage, FixDefs), 
    encode_group_h(erlang:delete_element(1,GroupPL),
                   FixDefs,
                   Body,
                   Accum++PL).
%% ====================================================================
%% Function Description: Encode components
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: encode_components/c
%% Returns: fix-speak encoding
%% --------------------------------------------------------------------
encode_components(Components, Message, _FD) when map_size(Components) == 0 ->
    {[], Message};

encode_components(Components, Message, FixDefs)->
    maps:fold(fun(Component, _Body, {MsgPl, MessageTillNow})->
                    {Pl, RestMessage} = encode_component(Component,
                                                         MessageTillNow,
                                                        FixDefs),
                     {MsgPl ++ Pl, RestMessage}
             end, {[], Message}, Components).


%% ====================================================================
%% Function Description: Encode one component
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: encode_component/2
%% Returns: returns
%% --------------------------------------------------------------------

encode_component(C, Message, _FD)  when map_size(C)==0->
       {[], Message};

encode_component(ComponentName, Message, FixDefs)->
    case maps:find(ComponentName, Message) of
        error ->
            {[], Message};
        {ok, Component} ->
            {ok, ComponentBlock }= query_fix({components, ComponentName}, FixDefs),
             {BlockPL, _Rest} = encode_block(ComponentBlock, Component, FixDefs),
            {BlockPL, maps:remove(ComponentName, Message)}
    end.



%% ====================================================================
%% Function Description: Covert fix tags to string
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: to_string/2
%% Returns: string()
%% --------------------------------------------------------------------

%% to_string([{"8", BeginString}, {"9", _Undefined}|Rest], Sentinal) ->
to_string([{"8", BeginString}|Rest], Sentinal) ->
    %% Body = to_string_h(lists:droplast(Rest), Sentinal, ""),
    [Seq, MsgType|Tail] = Rest,
    Body = string:join([string:join([T,V],"=") ||
                          {T,V} <- [MsgType, Seq, {"52", current_timestamp()} | Tail]],
                      Sentinal) ++ Sentinal,
    Length = integer_to_list(length(Body)),
    HeaderBody = "8="++BeginString ++ Sentinal ++ "9=" ++ Length ++ Sentinal ++ Body,
    HeaderBody ++ "10=" ++ checksum(HeaderBody) ++ Sentinal.
    


%% ====================================================================
%% Function Description: Computes checksum as per fix guideline
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: checksum/1
%% Returns: fix checksum
%% --------------------------------------------------------------------

checksum(Body) ->
    lists:flatten(io_lib:format("~3..0w", [lists:sum(Body) rem 256])).


%% ====================================================================
%% Function Description: Returns the fix version implied in the message
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: get_fix_version/1
%% Returns: Version/not_well_formed_fix_message
%% --------------------------------------------------------------------

get_fix_version(Message)->
    case maps:find("BeginString", Message) of 
        {ok, Version} ->
            {ok, list_to_atom(lists:flatten([string:to_lower(T) || T <-string:tokens(Version, ".")]))};
        error ->
            not_well_formed_fix_message 
    end.


%% ====================================================================
%% Function Description: Gets the current time
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: current_timestamp/0
%% Returns: timestamp in string
%% --------------------------------------------------------------------

current_timestamp() ->
    {{Y, Mon, D}, {H, Min, S}} = erlang:universaltime()
   ,lists:flatten(io_lib:format("~w~2..0w~2..0w-~2..0w:~2..0w:~2..0w.000",[Y, Mon, D, H, Min, S])).    
    

is_integer1(S) ->
    try
        _ = list_to_integer(S),
        true
    catch error:badarg ->
        false
    end.




parser_name(FixMaps)->
    Keys = lists:map(fun(X)->
                             atom_to_list(X) end, lists:sort(maps:values(FixMaps))),
    Pname = string:join(Keys, "_") ++ ".pmap",
    filename:join([filename:dirname(filename:absname(?MODULE)),
                  "src",
                  "parsers",
                  Pname]). 

get_parser(ParserFullDest)->
   ParserName = lists:last(string:tokens(ParserFullDest, "/")),
    case ets:lookup(parsers, ParserName) of
        [{ParserName, Parser}] ->
            {found, Parser};
        [] ->
            case file:consult(ParserFullDest) of
                {ok, [Parser]} ->
                    _Status = ets:insert(parsers,{ParserName, Parser}),
                    {found, Parser};
                _->
                    not_found
            end
    end. 


merge_fix_modules(Mod1, Mod2) when is_map(Mod1) and is_map(Mod2)->
    maps:merge(Mod1, Mod2);

merge_fix_modules(Mod1, Mod2) when is_map(Mod1)->
    Funcs = [messages, fields, components, groups, header, trailer],
    case maps:size(Mod1) of 
        0 ->
lists:foldl(fun(Key, Dict) ->
                     maps:merge(Dict,
                                #{Key => erlang:apply(Mod2, Key, [])})
            end,
            #{}, Funcs);
_ ->
    lists:foldl(fun(Key, Dict) -> maps:merge(Dict, 
                                            #{Key => maps:merge(maps:get(Key, Mod1),
                                                               erlang:apply(Mod2, Key, []))}) end,
                #{}, Funcs)
        end;

merge_fix_modules(Mod1, Mod2)->
    Funcs = [messages, fields, components, groups, header, trailer],
    lists:foldl(fun(Key, Dict) -> maps:merge(Dict, #{Key =>
                                                    maps:merge(erlang:apply(Mod1, Key, []),
                                                              erlang:apply(Mod2, Key, []))}) end, #{}, Funcs).

save_parser({Name,Pmap})->
    file:write_file(Name, io_lib:fwrite("~p.\n", [Pmap])),
    Pmap.


refresh_parsers()->
    ParserDir = filename:join([filename:dirname(filename:absname(?MODULE)),
                              "src",
                               "parsers"]),
    {ok, Files} = file:list_dir(ParserDir),
    lists:foreach(fun(F) ->
                          file:delete(filename:join(ParserDir, F)) end, Files),
    ets:delete_all_objects(parsers).
                          

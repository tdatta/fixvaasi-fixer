%%==============================================================================
%%   @author Tanmay Dutta (tanmay.datta86@gmail.com)
%%   @copyright  Tanmay Dutta
%%   @version  0.1.0
%%   @doc  The utilities module for all fix function
%%==============================================================================
-module(fixparser_utils).
-author(tanmay_dutta).
-compile([{parse_transform, lager_transform}]).
-compile([export_all]).

%% ====================================================================
%% Function Description: Retuns a parser if (exists) otherwise create,save and returns it
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: get_consolidated_defs/1
%% Returns: map()
%% --------------------------------------------------------------------

get_consolidated_defs(FixMaps) ->
    ParserName = parser_name(FixMaps),
    lager:debug("Parser name is ~p ~n", [ParserName]),
    case get_parser(ParserName) of
        {found, Parser} ->
            Parser; 
        _ ->
            Version = maps:get(fix_version, FixMaps),
            case maps:find(fix_transport, FixMaps) of
                {ok, Transport} ->
                    Next = merge_fix_modules(Version, Transport);
                _ ->
                    Next = merge_fix_modules(#{}, Version)
            end,
            case maps:find(user_defined_fix, FixMaps) of
                {ok, UDF_Module} ->
                    Final = merge_fix_modules(Next, UDF_Module);
                _->
                    Final = merge_fix_modules(#{}, Next)
            end,
            save_parser({ParserName, Final})
    end.



%% ====================================================================
%% Function Description: Returns the name of fix parser given a fix map
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: parser_name/1
%% Returns: string
%% --------------------------------------------------------------------

parser_name(FixMaps)->
    Keys = lists:map(fun(X)->
                             atom_to_list(X) end,
                     lists:sort(maps:values(FixMaps))),
    Pname = string:join(Keys, "_") ++ ".pmap",
    filename:join([filename:dirname(filename:absname(?MODULE)),
                  "src",
                  "parsers",
                  Pname]). 


%% ====================================================================
%% Function Description: Given full path of a parser returns the parser
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: get_parser/1
%% Returns: parser
%% --------------------------------------------------------------------

get_parser(ParserFullDest)->
   ParserName = lists:last(string:tokens(ParserFullDest, "/")),
    case ets:lookup(parsers, ParserName) of
        [{ParserName, Parser}] ->
            {found, Parser};
        [] ->
            case file:consult(ParserFullDest) of
                {ok, [Parser]} ->
                    _Status = ets:insert(parsers,{ParserName, Parser}),
                    {found, Parser};
                _->
                    not_found
            end
    end. 

%% ====================================================================
%% Function Description: Utility function that does the merging from the fix map
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: merge_fix_modules/2
%% Returns: dict()
%% --------------------------------------------------------------------

merge_fix_modules(Mod1, Mod2) when is_map(Mod1) and is_map(Mod2)->
    maps:merge(Mod1, Mod2);

%% ====================================================================
%% ====================================================================
merge_fix_modules(Mod1, Mod2) when is_map(Mod1)->
    Funcs = [messages, fields, components, groups, header, trailer],
    case maps:size(Mod1) of 
        0 ->
lists:foldl(fun(Key, Dict) ->
                     maps:merge(Dict,
                                #{Key => erlang:apply(Mod2, Key, [])})
            end,
            #{}, Funcs);
_ ->
    lists:foldl(fun(Key, Dict) -> maps:merge(Dict, 
                                            #{Key => maps:merge(maps:get(Key, Mod1),
                                                               erlang:apply(Mod2, Key, []))}) end,
                #{}, Funcs)
        end;


%% ====================================================================
%% ====================================================================
merge_fix_modules(Mod1, Mod2)->
    Funcs = [messages, fields, components, groups, header, trailer],
    lists:foldl(fun(Key, Dict) -> maps:merge(Dict, #{Key =>
                                                    maps:merge(erlang:apply(Mod1, Key, []),
                                                              erlang:apply(Mod2, Key, []))}) end, #{}, Funcs).


%% ====================================================================
%% Function Description: Saves the parser in "parsers" folder
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: save_parser/1
%% Returns: side_effect of saving the parser
%% --------------------------------------------------------------------

save_parser({Name,Pmap})->
    file:write_file(Name, io_lib:fwrite("~p.\n", [Pmap])),
    Pmap.


%% ====================================================================
%% Function Description: This is to delete the parsers from both file and the ets cache. Very useful while debugging
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: refresh_parser/0
%% Returns: side effect of deleting files and emptying ets cache
%% --------------------------------------------------------------------

refresh_parsers()->
    ParserDir = filename:join([filename:dirname(filename:absname(?MODULE)),
                              "src",
                               "parsers"]),
    {ok, Files} = file:list_dir(ParserDir),
    lists:foreach(fun(F) ->
                          file:delete(filename:join(ParserDir, F)) end, Files),
    ets:delete_all_objects(parsers).
                          

%% ====================================================================
%% Function Description: Returns the fix version implied in the message
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: get_fix_version/1
%% Returns: Version/not_well_formed_fix_message
%% --------------------------------------------------------------------

get_fix_version(Message)->
    case maps:find("BeginString", Message) of 
        {ok, Version} ->
            {ok, list_to_atom(lists:flatten([string:to_lower(T) || T <-string:tokens(Version, ".")]))};
        error ->
            not_well_formed_fix_message 
    end.

%% ====================================================================
%% Function Description: Gets the current time
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: current_timestamp/0
%% Returns: timestamp in string
%% --------------------------------------------------------------------

current_timestamp() ->
    {{Y, Mon, D}, {H, Min, S}} = erlang:universaltime()
   ,lists:flatten(io_lib:format("~w~2..0w~2..0w-~2..0w:~2..0w:~2..0w.000",[Y, Mon, D, H, Min, S])).    
    

%% ====================================================================
%% Function Description: Safely check for an integer 
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: is_integer1/1
%% Returns: bool()
%% --------------------------------------------------------------------
is_integer1(S) ->
    try
        _ = list_to_integer(S),
        true
    catch error:badarg ->
        false
    end.

%% ====================================================================
%% Function Description: Covert fix tags to string
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: to_string/2
%% Returns: string()
%% --------------------------------------------------------------------

%% to_string([{"8", BeginString}, {"9", _Undefined}|Rest], Sentinal) ->
to_string([{"8", BeginString}|Rest], Sentinal) ->
    % last is 10 tag this is a solid and valid assumption
    %% Body = to_string_h(lists:droplast(Rest), Sentinal, ""),
    Body = to_string_h(Rest, Sentinal, ""),
    Length = integer_to_list(length(Body)),
    HeaderBody = "8="++BeginString ++ Sentinal ++ "9=" ++ Length ++ Sentinal ++ Body,
    HeaderBody ++ "10=" ++ checksum(HeaderBody) ++ Sentinal.
    
to_string_h([], _Sentinal, String) ->
    String;

to_string_h([{Tag, Value}|Rest], Sentinal, String) ->
    to_string_h(Rest, Sentinal, String ++ Tag ++ "=" ++ Value ++ Sentinal).


%% ====================================================================
%% Function Description: Provides the msgType from the binary msg stream
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: get_msg_type/2
%% Returns: fix_msg_type()
%% --------------------------------------------------------------------

query_fix({Type,Query}, FixDefs)->
    maps:find(Query, maps:get(Type, FixDefs)). 

%% ====================================================================
%% Function Description: computes the tag number for a field
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: compute_tagnums/1
%% Returns: string(integer())
%% --------------------------------------------------------------------

compute_tagnums(Tag, Val, FixDefs) ->
    case is_integer1(Tag) of
        true ->
            {Tag, Val};
        _->
            TagNum = maps:get("TagNum", maps:get(Tag, maps:get(fields, FixDefs))),
            case maps:get("ValidValues", maps:get(Tag, maps:get(fields,FixDefs))) of 
                [] ->
                    {TagNum, Val};
                ValidList ->
                    UpperTag = string:to_upper(Val),
                    lager:info("validlist ~p~n",[ValidList]),
                    case proplists:get_value(UpperTag, ValidList) of 
                        undefined ->
                            lager:info("**************The value for Tag ~p as --> ~p does not match any valid values you will get reject message. ************ ~n",[Tag, UpperTag]),
                            {TagNum, Val};
                        ValidValue->
                    {TagNum, ValidValue}
                                end
                     
            end
    end.

%% ====================================================================
%% Function Description: Provides the msgType from the binary msg stream
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: get_msg_type/2
%% Returns: fix_msg_type()
%% --------------------------------------------------------------------

get_msg(MsgType, FixModuleMap) ->
    case query_fix({MsgType, messages}, FixModuleMap) of 
	error ->
            query_fix({maps:get("Name", query_fix({MsgType, messages}, FixModuleMap)), messages}, FixModuleMap);
	Value ->
	    query_fix({maps:get("Name", Value), messages}, FixModuleMap)
    end. 

%% ====================================================================
%% Function Description: To get the transport associated with a particular
%% fix version
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: get_transport/1
%% Returns: transport_module
%% --------------------------------------------------------------------

get_transport(Version) ->
  Fix_Version = maps:get(fix_version, Version)
	,maps:get(Fix_Version, #{
		    fix41 => fix41
		   ,fix42 => fix42
		   ,fix43 => fix43
		   ,fix44 => fix44
		   ,fix50 => fixt11
		   ,fix50sp1 => fixt11
		   ,fix50sp2 => fixt11}).



%% ====================================================================
%% Function Description: Computes checksum as per fix guideline
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: checksum/1
%% Returns: fix checksum
%% --------------------------------------------------------------------

checksum(Body) ->
    lists:flatten(io_lib:format("~3..0w", [lists:sum(Body) rem 256])).





%% ====================================================================
%% Function Description: Create proplist from the binary stream
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: tokenize/1
%% Returns: proplist()
%% --------------------------------------------------------------------
tokenize(BinaryStream)->
    tokenize(BinaryStream, [<<1>>]).

%% ====================================================================
%% Function Description: Tokenize with custom sentinel, rarely useful
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: tokenize/2
%% Returns: proplist()
%% --------------------------------------------------------------------
tokenize(BinaryStream, Sentinel)->
    [{list_to_integer(binary_to_list(T)), binary_to_list(V)} || [T,V] <- [binary:split(TVP, <<"=">>, [global, trim]) || TVP <- binary:split(BinaryStream, Sentinel, [global,trim])]].



get_message(Body, BodyMap)->
    AllFields = maps:get("Fields", BodyMap)
    ,{FieldMap,Rest} = get_fields(Body,AllFields)
     ,{FieldMap,Rest}.
    
    
get_fields(Message, FieldMap)-> 
    get_fields(Message, FieldMap, [], #{}).


get_fields([], _FieldsDict, RestMessage, HeaderMap) ->
    {HeaderMap, RestMessage};

get_fields([{T,V}|Rest], FieldDict, RestMessage, FieldMap) ->
    case maps:find(T,FieldDict) of 
	{ok, _Value} ->
	    get_fields(Rest, FieldDict, RestMessage, maps:merge(FieldMap, #{T=>V}));
	error -> 
	    get_fields(Rest, FieldDict, RestMessage ++ [{T,V}], FieldMap)
    end.



%% ====================================================================
%% Function Description: Get Category for a message
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: get_msg_category/2
%% Returns: string()
%% --------------------------------------------------------------------

get_msg_category(MsgName, VersionMap) ->
        FixDefs = get_consolidated_defs(VersionMap)
        ,case query_fix({messages, MsgName}, FixDefs) of
             error ->
                 message_name_undefined;
             {ok, Value} ->
                 maps:get("Category", Value)
         end.

    

%% ====================================================================
%% Function Description: Get Human readable name for a message value
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: get_msg_name/2
%% Returns: string()
%% --------------------------------------------------------------------

get_msg_name(Tag, VersionMap) ->
    lager:debug("got request ~p for tag", [Tag]),
        FixDefs = get_consolidated_defs(VersionMap)
        ,case query_fix({messages, Tag}, FixDefs) of
             error ->
                 message_name_undefined;
             {ok, Value} ->
                 maps:get("Name", Value)
         end.



%% ====================================================================
%% Function Description: json fix message output
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: to_json/1
%% Returns: string()
%% --------------------------------------------------------------------

to_json(FixMap) ->
        jsx:encode(parse_fix_map(FixMap)).


%% ====================================================================
%% Function Description: fix message output to proplist
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: to_proplist/1
%% Returns: string()
%% --------------------------------------------------------------------
to_proplist(FixMap) when is_map(FixMap)->
    maps:fold(fun(K,V,Acc)->
                      Acc ++ [{list_to_binary(K), to_proplist(V)}] 
              end,
              [],FixMap);

to_proplist(Atom) when is_atom(Atom)->
    list_to_binary(atom_to_list(Atom));
to_proplist(EverythingElse)->
    case is_proplist(EverythingElse) of 
        true ->
            lager:info("i am try "),
            EverythingElse;
        _ ->
            list_to_binary(EverythingElse)
    end.




%% ====================================================================
%% Function Description: proplist to fix encodable map
%% ====================================================================
%% --------------------------------------------------------------------
%% Func: from_json/1
%% Returns: FixMap
%% --------------------------------------------------------------------

from_json(Json) ->
    JsonMap = jsx:decode(Json, [return_maps]),
    parse_json_map(JsonMap).

%% ====================================================================
%% Helper functions
%% ====================================================================
parse_json_map(FixMap) when is_map(FixMap) ->
    maps:fold(fun(K,V,Acc)->
                      maps:merge(Acc, #{binary_to_list(K)=>parse_fix_map(V)})
              end,
              #{},FixMap);

parse_json_map(FixMap)->
    binary_to_list(FixMap).


parse_fix_map(FixMap) when is_map(FixMap) ->
    maps:fold(fun(K,V,Acc)->
                      maps:merge(Acc, #{list_to_binary(K)=>parse_fix_map(V)})
              end,
              #{},FixMap);

parse_fix_map(FixMap)->
    list_to_binary(FixMap).

is_proplist([]) -> true;
is_proplist([{K,_}|L])-> is_proplist(L);
is_proplist(_) -> false.

-module(fixt11).
-export([messages/0,
         fields/0,
         components/0,
         groups/0,
         header/0,
         trailer/0]).

messages() ->
 #{"Heartbeat" => #{
                              "Category" => "admin"
                              ,"Type" => "0"
                              ,"Fields" => #{"TestReqID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"0" => #{"Category"=>"admin"
           ,"Name" => "Heartbeat"}

,"TestRequest" => #{
                              "Category" => "admin"
                              ,"Type" => "1"
                              ,"Fields" => #{"TestReqID" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"1" => #{"Category"=>"admin"
           ,"Name" => "TestRequest"}

,"ResendRequest" => #{
                              "Category" => "admin"
                              ,"Type" => "2"
                              ,"Fields" => #{"BeginSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}
,"EndSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"2" => #{"Category"=>"admin"
           ,"Name" => "ResendRequest"}

,"Reject" => #{
                              "Category" => "admin"
                              ,"Type" => "3"
                              ,"Fields" => #{"RefSeqNum" =>#{"Required" => "Y", "Sequence" => undefined}
,"RefTagID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefMsgType" =>#{"Required" => "N", "Sequence" => undefined}
,"SessionRejectReason" =>#{"Required" => "N", "Sequence" => undefined}
,"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"3" => #{"Category"=>"admin"
           ,"Name" => "Reject"}

,"SequenceReset" => #{
                              "Category" => "admin"
                              ,"Type" => "4"
                              ,"Fields" => #{"GapFillFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"NewSeqNo" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"4" => #{"Category"=>"admin"
           ,"Name" => "SequenceReset"}

,"Logout" => #{
                              "Category" => "admin"
                              ,"Type" => "5"
                              ,"Fields" => #{"Text" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedTextLen" =>#{"Required" => "N", "Sequence" => undefined}
,"EncodedText" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}
,"5" => #{"Category"=>"admin"
           ,"Name" => "Logout"}

,"Logon" => #{
                              "Category" => "admin"
                              ,"Type" => "A"
                              ,"Fields" => #{"EncryptMethod" =>#{"Required" => "Y", "Sequence" => undefined}
,"HeartBtInt" =>#{"Required" => "Y", "Sequence" => undefined}
,"RawDataLength" =>#{"Required" => "N", "Sequence" => undefined}
,"RawData" =>#{"Required" => "N", "Sequence" => undefined}
,"ResetSeqNumFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"NextExpectedMsgSeqNum" =>#{"Required" => "N", "Sequence" => undefined}
,"MaxMessageSize" =>#{"Required" => "N", "Sequence" => undefined}
,"TestMessageIndicator" =>#{"Required" => "N", "Sequence" => undefined}
,"Username" =>#{"Required" => "N", "Sequence" => undefined}
,"Password" =>#{"Required" => "N", "Sequence" => undefined}
,"DefaultApplVerID" =>#{"Required" => "Y", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{"MsgTypeGrp" =>#{"Required" => "N", "Sequence" => undefined}
}
}
,"A" => #{"Category"=>"admin"
           ,"Name" => "Logon"}
}.


fields() ->
#{
"BeginSeqNo" => #{"TagNum" => "7" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "7" => #{"Name"=>"BeginSeqNo" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "7"}


,
"BeginString" => #{"TagNum" => "8" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "8" => #{"Name"=>"BeginString" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "8"}


,
"BodyLength" => #{"TagNum" => "9" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "9" => #{"Name"=>"BodyLength" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "9"}


,
"CheckSum" => #{"TagNum" => "10" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "10" => #{"Name"=>"CheckSum" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "10"}


,
"EndSeqNo" => #{"TagNum" => "16" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "16" => #{"Name"=>"EndSeqNo" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "16"}


,
"MsgSeqNum" => #{"TagNum" => "34" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "34" => #{"Name"=>"MsgSeqNum" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "34"}


,
"MsgType" => #{"TagNum" => "35" ,"Type" => "STRING" ,"ValidValues" =>[{"0", "HEARTBEAT"},{"1", "TEST_REQUEST"},{"2", "RESEND_REQUEST"},{"3", "REJECT"},{"4", "SEQUENCE_RESET"},{"5", "LOGOUT"},{"6", "INDICATION_OF_INTEREST"},{"7", "ADVERTISEMENT"},{"8", "EXECUTION_REPORT"},{"9", "ORDER_CANCEL_REJECT"},{"A", "LOGON"},{"B", "NEWS"},{"C", "EMAIL"},{"D", "ORDER_SINGLE"},{"E", "ORDER_LIST"},{"F", "ORDER_CANCEL_REQUEST"},{"G", "ORDER_CANCEL_REPLACE_REQUEST"},{"H", "ORDER_STATUS_REQUEST"},{"J", "ALLOCATION_INSTRUCTION"},{"K", "LIST_CANCEL_REQUEST"},{"L", "LIST_EXECUTE"},{"M", "LIST_STATUS_REQUEST"},{"N", "LIST_STATUS"},{"P", "ALLOCATION_INSTRUCTION_ACK"},{"Q", "DONT_KNOW_TRADE"},{"R", "QUOTE_REQUEST"},{"S", "QUOTE"},{"T", "SETTLEMENT_INSTRUCTIONS"},{"V", "MARKET_DATA_REQUEST"},{"W", "MARKET_DATA_SNAPSHOT_FULL_REFRESH"},{"X", "MARKET_DATA_INCREMENTAL_REFRESH"},{"Y", "MARKET_DATA_REQUEST_REJECT"},{"Z", "QUOTE_CANCEL"},{"a", "QUOTE_STATUS_REQUEST"},{"b", "MASS_QUOTE_ACKNOWLEDGEMENT"},{"c", "SECURITY_DEFINITION_REQUEST"},{"d", "SECURITY_DEFINITION"},{"e", "SECURITY_STATUS_REQUEST"},{"f", "SECURITY_STATUS"},{"g", "TRADING_SESSION_STATUS_REQUEST"},{"h", "TRADING_SESSION_STATUS"},{"i", "MASS_QUOTE"},{"j", "BUSINESS_MESSAGE_REJECT"},{"k", "BID_REQUEST"},{"l", "BID_RESPONSE"},{"m", "LIST_STRIKE_PRICE"},{"n", "XML_MESSAGE"},{"o", "REGISTRATION_INSTRUCTIONS"},{"p", "REGISTRATION_INSTRUCTIONS_RESPONSE"},{"q", "ORDER_MASS_CANCEL_REQUEST"},{"r", "ORDER_MASS_CANCEL_REPORT"},{"s", "NEW_ORDER_CROSS"},{"t", "CROSS_ORDER_CANCEL_REPLACE_REQUEST"},{"u", "CROSS_ORDER_CANCEL_REQUEST"},{"v", "SECURITY_TYPE_REQUEST"},{"w", "SECURITY_TYPES"},{"x", "SECURITY_LIST_REQUEST"},{"y", "SECURITY_LIST"},{"z", "DERIVATIVE_SECURITY_LIST_REQUEST"},{"AA", "DERIVATIVE_SECURITY_LIST"},{"AB", "NEW_ORDER_MULTILEG"},{"AC", "MULTILEG_ORDER_CANCEL_REPLACE"},{"AD", "TRADE_CAPTURE_REPORT_REQUEST"},{"AE", "TRADE_CAPTURE_REPORT"},{"AF", "ORDER_MASS_STATUS_REQUEST"},{"AG", "QUOTE_REQUEST_REJECT"},{"AH", "RFQ_REQUEST"},{"AI", "QUOTE_STATUS_REPORT"},{"AJ", "QUOTE_RESPONSE"},{"AK", "CONFIRMATION"},{"AL", "POSITION_MAINTENANCE_REQUEST"},{"AM", "POSITION_MAINTENANCE_REPORT"},{"AN", "REQUEST_FOR_POSITIONS"},{"AO", "REQUEST_FOR_POSITIONS_ACK"},{"AP", "POSITION_REPORT"},{"AQ", "TRADE_CAPTURE_REPORT_REQUEST_ACK"},{"AR", "TRADE_CAPTURE_REPORT_ACK"},{"AS", "ALLOCATION_REPORT"},{"AT", "ALLOCATION_REPORT_ACK"},{"AU", "CONFIRMATION_ACK"},{"AV", "SETTLEMENT_INSTRUCTION_REQUEST"},{"AW", "ASSIGNMENT_REPORT"},{"AX", "COLLATERAL_REQUEST"},{"AY", "COLLATERAL_ASSIGNMENT"},{"AZ", "COLLATERAL_RESPONSE"},{"BA", "COLLATERAL_REPORT"},{"BB", "COLLATERAL_INQUIRY"},{"BC", "NETWORK_STATUS_REQUEST"},{"BD", "NETWORK_STATUS_RESPONSE"},{"BE", "USER_REQUEST"},{"BF", "USER_RESPONSE"},{"BG", "COLLATERAL_INQUIRY_ACK"},{"BH", "CONFIRMATION_REQUEST"},{"BI", "TRADING_SESSION_LIST_REQUEST"},{"BJ", "TRADING_SESSION_LIST"},{"BK", "SECURITY_LIST_UPDATE_REPORT"},{"BL", "ADJUSTED_POSITION_REPORT"},{"BM", "ALLOCATION_INSTRUCTION_ALERT"},{"BN", "EXECUTION_ACKNOWLEDGEMENT"},{"BO", "CONTRARY_INTENTION_REPORT"},{"BP", "SECURITY_DEFINITION_UPDATE_REPORT"}]}
, "35" => #{"Name"=>"MsgType" ,"Type"=>"STRING" ,"ValidValues"=>[{"0", "HEARTBEAT"},{"1", "TEST_REQUEST"},{"2", "RESEND_REQUEST"},{"3", "REJECT"},{"4", "SEQUENCE_RESET"},{"5", "LOGOUT"},{"6", "INDICATION_OF_INTEREST"},{"7", "ADVERTISEMENT"},{"8", "EXECUTION_REPORT"},{"9", "ORDER_CANCEL_REJECT"},{"A", "LOGON"},{"B", "NEWS"},{"C", "EMAIL"},{"D", "ORDER_SINGLE"},{"E", "ORDER_LIST"},{"F", "ORDER_CANCEL_REQUEST"},{"G", "ORDER_CANCEL_REPLACE_REQUEST"},{"H", "ORDER_STATUS_REQUEST"},{"J", "ALLOCATION_INSTRUCTION"},{"K", "LIST_CANCEL_REQUEST"},{"L", "LIST_EXECUTE"},{"M", "LIST_STATUS_REQUEST"},{"N", "LIST_STATUS"},{"P", "ALLOCATION_INSTRUCTION_ACK"},{"Q", "DONT_KNOW_TRADE"},{"R", "QUOTE_REQUEST"},{"S", "QUOTE"},{"T", "SETTLEMENT_INSTRUCTIONS"},{"V", "MARKET_DATA_REQUEST"},{"W", "MARKET_DATA_SNAPSHOT_FULL_REFRESH"},{"X", "MARKET_DATA_INCREMENTAL_REFRESH"},{"Y", "MARKET_DATA_REQUEST_REJECT"},{"Z", "QUOTE_CANCEL"},{"a", "QUOTE_STATUS_REQUEST"},{"b", "MASS_QUOTE_ACKNOWLEDGEMENT"},{"c", "SECURITY_DEFINITION_REQUEST"},{"d", "SECURITY_DEFINITION"},{"e", "SECURITY_STATUS_REQUEST"},{"f", "SECURITY_STATUS"},{"g", "TRADING_SESSION_STATUS_REQUEST"},{"h", "TRADING_SESSION_STATUS"},{"i", "MASS_QUOTE"},{"j", "BUSINESS_MESSAGE_REJECT"},{"k", "BID_REQUEST"},{"l", "BID_RESPONSE"},{"m", "LIST_STRIKE_PRICE"},{"n", "XML_MESSAGE"},{"o", "REGISTRATION_INSTRUCTIONS"},{"p", "REGISTRATION_INSTRUCTIONS_RESPONSE"},{"q", "ORDER_MASS_CANCEL_REQUEST"},{"r", "ORDER_MASS_CANCEL_REPORT"},{"s", "NEW_ORDER_CROSS"},{"t", "CROSS_ORDER_CANCEL_REPLACE_REQUEST"},{"u", "CROSS_ORDER_CANCEL_REQUEST"},{"v", "SECURITY_TYPE_REQUEST"},{"w", "SECURITY_TYPES"},{"x", "SECURITY_LIST_REQUEST"},{"y", "SECURITY_LIST"},{"z", "DERIVATIVE_SECURITY_LIST_REQUEST"},{"AA", "DERIVATIVE_SECURITY_LIST"},{"AB", "NEW_ORDER_MULTILEG"},{"AC", "MULTILEG_ORDER_CANCEL_REPLACE"},{"AD", "TRADE_CAPTURE_REPORT_REQUEST"},{"AE", "TRADE_CAPTURE_REPORT"},{"AF", "ORDER_MASS_STATUS_REQUEST"},{"AG", "QUOTE_REQUEST_REJECT"},{"AH", "RFQ_REQUEST"},{"AI", "QUOTE_STATUS_REPORT"},{"AJ", "QUOTE_RESPONSE"},{"AK", "CONFIRMATION"},{"AL", "POSITION_MAINTENANCE_REQUEST"},{"AM", "POSITION_MAINTENANCE_REPORT"},{"AN", "REQUEST_FOR_POSITIONS"},{"AO", "REQUEST_FOR_POSITIONS_ACK"},{"AP", "POSITION_REPORT"},{"AQ", "TRADE_CAPTURE_REPORT_REQUEST_ACK"},{"AR", "TRADE_CAPTURE_REPORT_ACK"},{"AS", "ALLOCATION_REPORT"},{"AT", "ALLOCATION_REPORT_ACK"},{"AU", "CONFIRMATION_ACK"},{"AV", "SETTLEMENT_INSTRUCTION_REQUEST"},{"AW", "ASSIGNMENT_REPORT"},{"AX", "COLLATERAL_REQUEST"},{"AY", "COLLATERAL_ASSIGNMENT"},{"AZ", "COLLATERAL_RESPONSE"},{"BA", "COLLATERAL_REPORT"},{"BB", "COLLATERAL_INQUIRY"},{"BC", "NETWORK_STATUS_REQUEST"},{"BD", "NETWORK_STATUS_RESPONSE"},{"BE", "USER_REQUEST"},{"BF", "USER_RESPONSE"},{"BG", "COLLATERAL_INQUIRY_ACK"},{"BH", "CONFIRMATION_REQUEST"},{"BI", "TRADING_SESSION_LIST_REQUEST"},{"BJ", "TRADING_SESSION_LIST"},{"BK", "SECURITY_LIST_UPDATE_REPORT"},{"BL", "ADJUSTED_POSITION_REPORT"},{"BM", "ALLOCATION_INSTRUCTION_ALERT"},{"BN", "EXECUTION_ACKNOWLEDGEMENT"},{"BO", "CONTRARY_INTENTION_REPORT"},{"BP", "SECURITY_DEFINITION_UPDATE_REPORT"}], "TagNum" => "35"}


,
"NewSeqNo" => #{"TagNum" => "36" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "36" => #{"Name"=>"NewSeqNo" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "36"}


,
"PossDupFlag" => #{"TagNum" => "43" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "43" => #{"Name"=>"PossDupFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "43"}


,
"RefSeqNum" => #{"TagNum" => "45" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "45" => #{"Name"=>"RefSeqNum" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "45"}


,
"SenderCompID" => #{"TagNum" => "49" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "49" => #{"Name"=>"SenderCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "49"}


,
"SenderSubID" => #{"TagNum" => "50" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "50" => #{"Name"=>"SenderSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "50"}


,
"SendingTime" => #{"TagNum" => "52" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "52" => #{"Name"=>"SendingTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "52"}


,
"TargetCompID" => #{"TagNum" => "56" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "56" => #{"Name"=>"TargetCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "56"}


,
"TargetSubID" => #{"TagNum" => "57" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "57" => #{"Name"=>"TargetSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "57"}


,
"Text" => #{"TagNum" => "58" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "58" => #{"Name"=>"Text" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "58"}


,
"Signature" => #{"TagNum" => "89" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "89" => #{"Name"=>"Signature" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "89"}


,
"SecureDataLen" => #{"TagNum" => "90" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "90" => #{"Name"=>"SecureDataLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "90"}


,
"SecureData" => #{"TagNum" => "91" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "91" => #{"Name"=>"SecureData" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "91"}


,
"SignatureLength" => #{"TagNum" => "93" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "93" => #{"Name"=>"SignatureLength" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "93"}


,
"RawDataLength" => #{"TagNum" => "95" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "95" => #{"Name"=>"RawDataLength" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "95"}


,
"RawData" => #{"TagNum" => "96" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "96" => #{"Name"=>"RawData" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "96"}


,
"PossResend" => #{"TagNum" => "97" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "97" => #{"Name"=>"PossResend" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "97"}


,
"EncryptMethod" => #{"TagNum" => "98" ,"Type" => "INT" ,"ValidValues" =>[{"0", "NONE_OTHER"},{"1", "PKCS"},{"2", "DES"},{"3", "PKCS_DES"},{"4", "PGP_DES"},{"5", "PGP_DES_MD5"},{"6", "PEM_DES_MD5"}]}
, "98" => #{"Name"=>"EncryptMethod" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "NONE_OTHER"},{"1", "PKCS"},{"2", "DES"},{"3", "PKCS_DES"},{"4", "PGP_DES"},{"5", "PGP_DES_MD5"},{"6", "PEM_DES_MD5"}], "TagNum" => "98"}


,
"HeartBtInt" => #{"TagNum" => "108" ,"Type" => "INT" ,"ValidValues" =>[]}
, "108" => #{"Name"=>"HeartBtInt" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "108"}


,
"TestReqID" => #{"TagNum" => "112" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "112" => #{"Name"=>"TestReqID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "112"}


,
"OnBehalfOfCompID" => #{"TagNum" => "115" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "115" => #{"Name"=>"OnBehalfOfCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "115"}


,
"OnBehalfOfSubID" => #{"TagNum" => "116" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "116" => #{"Name"=>"OnBehalfOfSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "116"}


,
"OrigSendingTime" => #{"TagNum" => "122" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "122" => #{"Name"=>"OrigSendingTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "122"}


,
"GapFillFlag" => #{"TagNum" => "123" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "123" => #{"Name"=>"GapFillFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "123"}


,
"DeliverToCompID" => #{"TagNum" => "128" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "128" => #{"Name"=>"DeliverToCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "128"}


,
"DeliverToSubID" => #{"TagNum" => "129" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "129" => #{"Name"=>"DeliverToSubID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "129"}


,
"ResetSeqNumFlag" => #{"TagNum" => "141" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "141" => #{"Name"=>"ResetSeqNumFlag" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "141"}


,
"SenderLocationID" => #{"TagNum" => "142" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "142" => #{"Name"=>"SenderLocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "142"}


,
"TargetLocationID" => #{"TagNum" => "143" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "143" => #{"Name"=>"TargetLocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "143"}


,
"OnBehalfOfLocationID" => #{"TagNum" => "144" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "144" => #{"Name"=>"OnBehalfOfLocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "144"}


,
"DeliverToLocationID" => #{"TagNum" => "145" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "145" => #{"Name"=>"DeliverToLocationID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "145"}


,
"XmlDataLen" => #{"TagNum" => "212" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "212" => #{"Name"=>"XmlDataLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "212"}


,
"XmlData" => #{"TagNum" => "213" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "213" => #{"Name"=>"XmlData" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "213"}


,
"MessageEncoding" => #{"TagNum" => "347" ,"Type" => "STRING" ,"ValidValues" =>[{"ISO-2022-JP", "ISO_2022_JP"},{"EUC-JP", "EUC_JP"},{"SHIFT_JIS", "SHIFT_JIS"},{"UTF-8", "UTF_8"}]}
, "347" => #{"Name"=>"MessageEncoding" ,"Type"=>"STRING" ,"ValidValues"=>[{"ISO-2022-JP", "ISO_2022_JP"},{"EUC-JP", "EUC_JP"},{"SHIFT_JIS", "SHIFT_JIS"},{"UTF-8", "UTF_8"}], "TagNum" => "347"}


,
"EncodedTextLen" => #{"TagNum" => "354" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "354" => #{"Name"=>"EncodedTextLen" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "354"}


,
"EncodedText" => #{"TagNum" => "355" ,"Type" => "DATA" ,"ValidValues" =>[]}
, "355" => #{"Name"=>"EncodedText" ,"Type"=>"DATA" ,"ValidValues"=>[], "TagNum" => "355"}


,
"LastMsgSeqNumProcessed" => #{"TagNum" => "369" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "369" => #{"Name"=>"LastMsgSeqNumProcessed" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "369"}


,
"RefTagID" => #{"TagNum" => "371" ,"Type" => "INT" ,"ValidValues" =>[]}
, "371" => #{"Name"=>"RefTagID" ,"Type"=>"INT" ,"ValidValues"=>[], "TagNum" => "371"}


,
"RefMsgType" => #{"TagNum" => "372" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "372" => #{"Name"=>"RefMsgType" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "372"}


,
"SessionRejectReason" => #{"TagNum" => "373" ,"Type" => "INT" ,"ValidValues" =>[{"0", "INVALID_TAG_NUMBER"},{"1", "REQUIRED_TAG_MISSING"},{"10", "SENDINGTIME_ACCURACY_PROBLEM"},{"11", "INVALID_MSGTYPE"},{"12", "XML_VALIDATION_ERROR"},{"13", "TAG_APPEARS_MORE_THAN_ONCE"},{"14", "TAG_SPECIFIED_OUT_OF_REQUIRED_ORDER"},{"15", "REPEATING_GROUP_FIELDS_OUT_OF_ORDER"},{"16", "INCORRECT_NUMINGROUP_COUNT_FOR_REPEATING_GROUP"},{"17", "NON_DATA_VALUE_INCLUDES_FIELD_DELIMITER"},{"2", "TAG_NOT_DEFINED_FOR_THIS_MESSAGE_TYPE"},{"3", "UNDEFINED_TAG"},{"4", "TAG_SPECIFIED_WITHOUT_A_VALUE"},{"5", "VALUE_IS_INCORRECT"},{"6", "INCORRECT_DATA_FORMAT_FOR_VALUE"},{"7", "DECRYPTION_PROBLEM"},{"8", "SIGNATURE_PROBLEM"},{"9", "COMPID_PROBLEM"},{"99", "OTHER"}]}
, "373" => #{"Name"=>"SessionRejectReason" ,"Type"=>"INT" ,"ValidValues"=>[{"0", "INVALID_TAG_NUMBER"},{"1", "REQUIRED_TAG_MISSING"},{"10", "SENDINGTIME_ACCURACY_PROBLEM"},{"11", "INVALID_MSGTYPE"},{"12", "XML_VALIDATION_ERROR"},{"13", "TAG_APPEARS_MORE_THAN_ONCE"},{"14", "TAG_SPECIFIED_OUT_OF_REQUIRED_ORDER"},{"15", "REPEATING_GROUP_FIELDS_OUT_OF_ORDER"},{"16", "INCORRECT_NUMINGROUP_COUNT_FOR_REPEATING_GROUP"},{"17", "NON_DATA_VALUE_INCLUDES_FIELD_DELIMITER"},{"2", "TAG_NOT_DEFINED_FOR_THIS_MESSAGE_TYPE"},{"3", "UNDEFINED_TAG"},{"4", "TAG_SPECIFIED_WITHOUT_A_VALUE"},{"5", "VALUE_IS_INCORRECT"},{"6", "INCORRECT_DATA_FORMAT_FOR_VALUE"},{"7", "DECRYPTION_PROBLEM"},{"8", "SIGNATURE_PROBLEM"},{"9", "COMPID_PROBLEM"},{"99", "OTHER"}], "TagNum" => "373"}


,
"MaxMessageSize" => #{"TagNum" => "383" ,"Type" => "LENGTH" ,"ValidValues" =>[]}
, "383" => #{"Name"=>"MaxMessageSize" ,"Type"=>"LENGTH" ,"ValidValues"=>[], "TagNum" => "383"}


,
"NoMsgTypes" => #{"TagNum" => "384" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "384" => #{"Name"=>"NoMsgTypes" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "384"}


,
"MsgDirection" => #{"TagNum" => "385" ,"Type" => "CHAR" ,"ValidValues" =>[{"R", "RECEIVE"},{"S", "SEND"}]}
, "385" => #{"Name"=>"MsgDirection" ,"Type"=>"CHAR" ,"ValidValues"=>[{"R", "RECEIVE"},{"S", "SEND"}], "TagNum" => "385"}


,
"TestMessageIndicator" => #{"TagNum" => "464" ,"Type" => "BOOLEAN" ,"ValidValues" =>[]}
, "464" => #{"Name"=>"TestMessageIndicator" ,"Type"=>"BOOLEAN" ,"ValidValues"=>[], "TagNum" => "464"}


,
"Username" => #{"TagNum" => "553" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "553" => #{"Name"=>"Username" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "553"}


,
"Password" => #{"TagNum" => "554" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "554" => #{"Name"=>"Password" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "554"}


,
"NoHops" => #{"TagNum" => "627" ,"Type" => "NUMINGROUP" ,"ValidValues" =>[]}
, "627" => #{"Name"=>"NoHops" ,"Type"=>"NUMINGROUP" ,"ValidValues"=>[], "TagNum" => "627"}


,
"HopCompID" => #{"TagNum" => "628" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "628" => #{"Name"=>"HopCompID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "628"}


,
"HopSendingTime" => #{"TagNum" => "629" ,"Type" => "UTCTIMESTAMP" ,"ValidValues" =>[]}
, "629" => #{"Name"=>"HopSendingTime" ,"Type"=>"UTCTIMESTAMP" ,"ValidValues"=>[], "TagNum" => "629"}


,
"HopRefID" => #{"TagNum" => "630" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "630" => #{"Name"=>"HopRefID" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "630"}


,
"NextExpectedMsgSeqNum" => #{"TagNum" => "789" ,"Type" => "SEQNUM" ,"ValidValues" =>[]}
, "789" => #{"Name"=>"NextExpectedMsgSeqNum" ,"Type"=>"SEQNUM" ,"ValidValues"=>[], "TagNum" => "789"}


,
"ApplVerID" => #{"TagNum" => "1128" ,"Type" => "STRING" ,"ValidValues" =>[{"0", "FIX27"},{"1", "FIX30"},{"2", "FIX40"},{"3", "FIX41"},{"4", "FIX42"},{"5", "FIX43"},{"6", "FIX44"},{"7", "FIX50"},{"8", "FIX50SP1"},{"9", "FIX50SP2"}]}
, "1128" => #{"Name"=>"ApplVerID" ,"Type"=>"STRING" ,"ValidValues"=>[{"0", "FIX27"},{"1", "FIX30"},{"2", "FIX40"},{"3", "FIX41"},{"4", "FIX42"},{"5", "FIX43"},{"6", "FIX44"},{"7", "FIX50"},{"8", "FIX50SP1"},{"9", "FIX50SP2"}], "TagNum" => "1128"}


,
"CstmApplVerID" => #{"TagNum" => "1129" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1129" => #{"Name"=>"CstmApplVerID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1129"}


,
"RefApplVerID" => #{"TagNum" => "1130" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1130" => #{"Name"=>"RefApplVerID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1130"}


,
"RefCstmApplVerID" => #{"TagNum" => "1131" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1131" => #{"Name"=>"RefCstmApplVerID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1131"}


,
"DefaultApplVerID" => #{"TagNum" => "1137" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "1137" => #{"Name"=>"DefaultApplVerID" ,"Type"=>"STRING" ,"ValidValues"=>[], "TagNum" => "1137"}


}.



components() ->
 #{"HopGrp" => #{
                              "Fields" => #{"HopCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"HopSendingTime" =>#{"Required" => "N", "Sequence" => undefined}
,"HopRefID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoHops" => #{
                              "Fields" => #{"HopCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"HopSendingTime" =>#{"Required" => "N", "Sequence" => undefined}
,"HopRefID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
,"MsgTypeGrp" => #{
                              "Fields" => #{"RefMsgType" =>#{"Required" => "N", "Sequence" => undefined}
,"MsgDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"RefApplVerID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefCstmApplVerID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{
                              "NoMsgTypes" => #{
                              "Fields" => #{"RefMsgType" =>#{"Required" => "N", "Sequence" => undefined}
,"MsgDirection" =>#{"Required" => "N", "Sequence" => undefined}
,"RefApplVerID" =>#{"Required" => "N", "Sequence" => undefined}
,"RefCstmApplVerID" =>#{"Required" => "N", "Sequence" => undefined}}
                              ,"Groups" => #{}
                              ,"Components" => #{}
}

}
                              ,"Components" => #{}}
}.



groups() ->
 #{}.



header()->
#{
"Fields" => #{"BeginString" =>#{"Required" => "Y", "Sequence" => undefined}
,"BodyLength" =>#{"Required" => "Y", "Sequence" => undefined}
,"MsgType" =>#{"Required" => "Y", "Sequence" => undefined}
,"SenderCompID" =>#{"Required" => "Y", "Sequence" => undefined}
,"TargetCompID" =>#{"Required" => "Y", "Sequence" => undefined}
,"OnBehalfOfCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeliverToCompID" =>#{"Required" => "N", "Sequence" => undefined}
,"SecureDataLen" =>#{"Required" => "N", "Sequence" => undefined}
,"SecureData" =>#{"Required" => "N", "Sequence" => undefined}
,"MsgSeqNum" =>#{"Required" => "Y", "Sequence" => undefined}
,"SenderSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"SenderLocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"TargetLocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"OnBehalfOfSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"OnBehalfOfLocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeliverToSubID" =>#{"Required" => "N", "Sequence" => undefined}
,"DeliverToLocationID" =>#{"Required" => "N", "Sequence" => undefined}
,"PossDupFlag" =>#{"Required" => "N", "Sequence" => undefined}
,"PossResend" =>#{"Required" => "N", "Sequence" => undefined}
,"SendingTime" =>#{"Required" => "Y", "Sequence" => undefined}
,"OrigSendingTime" =>#{"Required" => "N", "Sequence" => undefined}
,"XmlDataLen" =>#{"Required" => "N", "Sequence" => undefined}
,"XmlData" =>#{"Required" => "N", "Sequence" => undefined}
,"MessageEncoding" =>#{"Required" => "N", "Sequence" => undefined}
,"LastMsgSeqNumProcessed" =>#{"Required" => "N", "Sequence" => undefined}
,"ApplVerID" =>#{"Required" => "N", "Sequence" => undefined}
,"CstmApplVerID" =>#{"Required" => "N", "Sequence" => undefined}}, 
"Groups" => #{}, 
 "Components" => #{"HopGrp" =>#{"Required" => "N", "Sequence" => undefined}
}

}.



trailer()->
#{
"Fields" => #{"SignatureLength" =>#{"Required" => "N", "Sequence" => undefined}
,"Signature" =>#{"Required" => "N", "Sequence" => undefined}
,"CheckSum" =>#{"Required" => "Y", "Sequence" => undefined}}, 
"Groups" => #{}, 
 "Components" => #{}

}.


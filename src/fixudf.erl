-module(fixudf).
-export([messages/0,
         fields/0,
         components/0,
         groups/0,
         header/0,
         trailer/0]).

messages()->
    #{

}.

fields()->
    #{
"test1" => #{"Number" => "10077" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "10077" => #{"Name"=>"test1" ,"Type"=>"STRING" ,"ValidValues"=>[]}

,"test2" => #{"Number" => "10009" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "10009" => #{"Name"=>"test1" ,"Type"=>"STRING" ,"ValidValues"=>[]}

,"test7" => #{"Number" => "10002" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "10002" => #{"Name"=>"test7" ,"Type"=>"STRING" ,"ValidValues"=>[]}
       
,"test3" => #{"Number" => "10008" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "10008" => #{"Name"=>"test1" ,"Type"=>"STRING" ,"ValidValues"=>[]}
       
,"test5" => #{"Number" => "10005" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "10005" => #{"Name"=>"test1" ,"Type"=>"STRING" ,"ValidValues"=>[]}
       
,"test6" => #{"Number" => "10006" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "10006" => #{"Name"=>"test6" ,"Type"=>"STRING" ,"ValidValues"=>[]}

,"test3" => #{"Number" => "10078" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "10078" => #{"Name"=>"test1" ,"Type"=>"STRING" ,"ValidValues"=>[]}

,"test4" => #{"Number" => "10040" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "10040" => #{"Name"=>"test1" ,"Type"=>"STRING" ,"ValidValues"=>[]}

,"test8" => #{"Number" => "10024" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "10024" => #{"Name"=>"test6" ,"Type"=>"STRING" ,"ValidValues"=>[]}

,"test9" => #{"Number" => "10048" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "10048" => #{"Name"=>"test1" ,"Type"=>"STRING" ,"ValidValues"=>[]}

,"test11" => #{"Number" => "10401" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "10401" => #{"Name"=>"test1" ,"Type"=>"STRING" ,"ValidValues"=>[]}

,"test12" => #{"Number" => "10050" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "10050" => #{"Name"=>"test1" ,"Type"=>"STRING" ,"ValidValues"=>[]}

,"test13" => #{"Number" => "10018" ,"Type" => "STRING" ,"ValidValues" =>[]}
, "10018" => #{"Name"=>"test1" ,"Type"=>"STRING" ,"ValidValues"=>[]}
}.

components()->
    #{}.

groups()->
    #{}.

header()->
    #{}.

trailer()->
    #{}.

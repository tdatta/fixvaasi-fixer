-module(test).
-compile([export_all]).


test_logon()->
    Msg = #{"BeginString" => "FIX.4.4","EncryptMethod" => "0","HeartBtInt" => "30","MsgSeqNum" => "1","MsgType" => "Logon","ResetSeqNumFlag" => "Y","SenderCompID" => "CLIENT1","TargetCompID" => "EXECUTOR"},
    
    fixparser:encode(Msg, #{fix_version=>fix44}).

%%%%%%%%%%%%%%%%%%% -- Fixtures -- %%%%%%%%%%%%%%%%%%%%%%%%%%

fix44_udf()->
    #{fix_version=>fix44
     ,fix_transport=>fix44
     ,user_defined_fix=>fixudf}.

fix50sp1()->
    #{fix_version=>fix50sp1
     ,fix_transport=>fixt11
     }.
%%%%%%%%%%%%%%%%%%% -- Tests for single encode %%%%%%%%%%%%%%%%%%%%%%%%%%
%% as the field is number so I assume that you know better than the parser
test_encode_field1()->
   fixparser:encode_field("1002", "testing", 
                          #{fix_version=>fix44
                            ,fix_transport=>fix44
                            ,user_defined_fix=>fixjpm}, "|").

%%% This tag is not defined in version or transport 
test_encode_field2()->
   fixparser:encode_field("test1", "testing", 
                          #{fix_version=>fix44
                            ,fix_transport=>fix44
                            ,user_defined_fix=>fixudf}, "|").
% only two definition provided
test_encode_field3()->
   fixparser:encode_field("BeginString", "testing", #{fix_version=>fix44
                             ,user_defined_fix=>fixjpm}, "|").

% no need to provide all the three definitions just one will do as well
test_encode_field4()->
   fixparser:encode_field("BeginString", "testing", #{fix_version=>fix44}, "|").


%%%%%%%%%%%%%%%%%%% -- Tests for full message encode %%%%%%%%%%%%%%%%%%%%%%%%%%
test_encode1()->
    Msg = #{
      "BeginString" => "FIX.4.4"
      ,"MsgType" => "Logon",
      "SenderCompID" => "the sender",
      "TargetCompID" => "Target",
      "Username" => "testing",
      "Password" => "So silly",
       "1007" =>"testing"
},
    fixparser:encode(Msg, #{fix_version=>fix50sp2, fix_transport=>fixt11}).

test_encode2()->
    Msg = #{
      "BeginString" => "FIXT.1.1",
      "MsgType" => "MarketDataRequest",
      "MsgSeqNum" => "nn",
      "TargetCompID" => "Target",
      "SenderCompID" => "the sender",
      "SendingTime" => "zzzyhmmd",
      "InstrmtMDReqGrp" => #{"NoRelatedSym" => {
                               #{"Instrument" => #{"Symbol" => "AUD/NZD"}}
                              }},
      "MDReqID" => "requestid",
      "SubscriptionRequestType" => "SNAPSHOT_PLUS_UPDATES",
      "MarketDepth" => "0",
      "MDUpdateType" => "FULL_REFRESH",
      "MDReqGrp" => #{
        "NoMDEntryTypes" =>{
        #{"MDEntryType" => "BID"},
        #{"MDEntryType" => "OFFER"}
         }}
     },
    fixparser:encode(Msg, #{fix_version=>fix50sp2, fix_transport=>fixt11}).

%%%%%%%%%%%%%%%%%%% -- Tests for single field decode %%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%% -- Tests for full message decode %%%%%%%%%%%%%%%%%%%%%%%%%%

test_decode1()->
    fixparser:decode(list_to_binary(msg1()),#{fix_version => fix50sp2,
                                             fix_transport => fixt11}).

test_decode2()->
    fixparser:decode(list_to_binary(msg_col_rep()),#{fix_version=>fix44
                                                    ,user_defined_fix=>fixudf}).


%%%%%%%%%%%%%%%%%%% -- data for testing starts from here %%%%%%%%%%%%%%%%%%%%%%
msg()->
"8=FIX.4.19=11235=049=BRKR56=INVMGR34=23552=19980604-07:58:28112=19980604-07:58:2810=157".

msg1()->
"8=FIX.4.49=7035=A49=MBT56=TargetCompID34=152=20130624-04:09:59141=Y98=0108=3010=079".

msg_col_rep()->
    "8=FIX.4.49=32335=BA49=MBT56=TargetCompID34=452=20130624-06:58:5543=N1=AcctNumber10077=MBTF10009=MorningAcctvalue10008=RealizedPnL10078=MBTF10040=MorningExcessEquity10005=MBTAcctType10006=OvernightBuyingPower12=Commission15=Currency10002=MorningBuyingPower910=0909=UniqueID908=UniqueID263=010024=410048=OvernightExcess10401=FLID10050=BODOvernightExcessEq900=TotalNetValue899=MarginExcess10018=0898=10010=019".

msg_col_bal1()->
    "8=FIX.4.49=27835=BA49=MBT56=TargetCompID34=5052=20130624-05:01:3243=N1=AcctNumber10077=MBTF10009=010076=110008=010078=MBTF10040=010005=210006=012=015=USD10002=0910=0909=uniquecode908=2263=010024=410048=010401=FLID10050=0900=0899=010018=0898=100 10=031". 

msg_col_bal2()->
    "8=FIX.4.49=34435=BA49=MBT56=TargetCompID34=5252=20130624-05:01:3243=N1=AcctNumber10077=MBT10009=152184.3910076=110008=010078=MBT10040=118515.5710005=210006=237031.1412=015=USD10002=474062.28910=0909=uniquecode908=1263=010024=6553710048=118515.5710401=FLID10050=118515.57900=152184.39899=118515.5710018=0898=10010=064".


position_report()->
   "8=FIX.4.49=24435=AP49=MBT56=TargetCompID34=552=20130624-06:58:5543=N1=AcctNumber715=2013062410015=4400705=-210004=-53012=29.0410001=010069=77467010000=0640=1277.8510003=4400710=AN_175=20130624728=0730=1277.85325=Y55=/GCQ354=5721=22727=110=111".


buy_limit_order()->
    "8=FIX.4.49=28135=D49=IssuedSenderCompID56=MBT34=100452=20130624-05:38:55.9081=AcctNumber100=MBTX10906=3038=3000040=211=ClOrdID167=FOR47=A44=127.5553=IssuedSenderCompID21=155=EUR/JPY54=1114=N769=20130624-05:38:5559=110055=8170861860=20130624-05:38:5510=137".


buy_limit_order2()->
    "8=FIX.4.49=37235=849=MBT56=IssuedSenderCompID34=388552=20130624-05:38:5643=N129=MBTS1=AcctNumber6=077=O11=ClOrdID14=017=ExecID460=4150=A151=3000010017=2e0e70a:02g6100=MBTX38=3000039=A37=2e0e70a:02g640=2109=IssuedSenderCompID376=ComplianceID44=127.555=EUR/JPY54=259=1126=20130924-20:58:30.00010055=8170861860=20130624-05:38:56.03410=197". 

buy_limit_order1()->
    "8=FIX.4.49=39935=849=MBT56=IssuedSenderCompID34=418652=20130624-05:39:2443=N1=AcctNumber6=127.577=O198=103-226411=ClOrdID12=2.9414=3000017=ExecID460=4150=F31=127.5151=0.010017=2e0e70a:02g6100=MBTX32=3000038=3000039=237=2e0e70a:02g610004=040=2109=IssuedSenderCompID9730=A44=127.555=EUR/JPY54=2126=20130924-20:58:30.00060=20130624-05:39:24.22110055=8170861810=221".


cancel_request()->
    "8=FIX.4.49=14735=F49=IssuedSenderCompID56=MBT34=3552=20130624-18:20:021=AcctNumber55=EUR/GBP54=237=1u2ea2s:12yy11=ClOrdID41=310017=1u2ea2s:12yy60=20110708-17:57:0110=113".
